﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using YuFit.WebServices.Interface.WebService;
using YuFit.WebServices.Test.Helper;

namespace YuFit.WebServices.Test
{
    public class NotificationWebServiceTests
    {
		private INotificationWebService _notificationService;

		/// <summary>
		/// Establish a session and create the service under test.
		/// </summary>
		[TestFixtureSetUp]
		public void TestSetup()
		{
			var session = SessionHelper.EstablishSession().Result;
			_notificationService = SessionHelper.YuFitClient.NotificationService;
		}

		#region GetNotificationsForUser Tests

		[Test]
		public async Task TestGetNotificationsForUserSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------

			// ----------------------------------
			// Act
			// ----------------------------------
            var notifications = await _notificationService.GetNotificationsForUser(SessionHelper.User.Id, 0, 100);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(notifications);
			// Comment out the count assertion
			// Can be asserted when running against an environment with notifications
			//Assert.GreaterOrEqual(notifications.Count, 1);
		}

		#endregion
    }
}

