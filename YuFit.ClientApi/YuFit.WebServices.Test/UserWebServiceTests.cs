﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;
using YuFit.WebServices.Test.Helper;
using YuFit.WebServices.Test.Model;
using System.Collections.Generic;

namespace YuFit.WebServices.Test
{
	[TestFixture]
    public class UserWebServiceTests
    {
		IUserWebService _userService;
		IUser _authenticatedUser;

		[TestFixtureSetUp]
		public void TestSetup()
		{
			var session = SessionHelper.EstablishSession().Result;
			_authenticatedUser = session.AuthenticatedUser;
			_userService = SessionHelper.YuFitClient.UserService;
		}

		/// <summary>
		/// Method Under Test: UserWebService.GetUserInformation
		/// Condition: Get the authenticated user from the service
		/// Expected Result: A User is returned that matches the authenticated user
		/// </summary>
		[Test]
		public async Task TestGetUserInformationSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------

			// ----------------------------------
			// Act
			// ----------------------------------
			IUser user = await _userService.GetUserInformation(_authenticatedUser.Id);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(user);
			Assert.IsNotNull(user.Name);
//			Assert.IsNotNull(user.Email);
			Assert.IsNotNull(user.ServiceIdentities);
		}

		/// <summary>
		/// Method Under Test: UserWebService.AddUser
		/// Condition: Add device info for a user
		/// Expected Result: The device information is added to the user
		/// </summary>
//		[Test]
//		public async Task TestAddDeviceInformationSuccess()
//		{
//			// ----------------------------------
//			// Arrange
//			// ----------------------------------
//			IDeviceInformation deviceInformation = new DeviceInformation
//			{
//				DeviceOs = DeviceOs.IOS,
//				// Token for Jeff's iPhone
//				NotificationToken = "<a6d65064 6c69163c 4be34e75 e3029c16 3d8942b6 6c41ecd9 7a0a40ff 9d9c52c5>"
//			};
//
//			// ----------------------------------
//			// Act
//			// ----------------------------------
//			//IUser user = await _userService.AddDevice(_authenticatedUser.Id, deviceInformation);
//
//			// ----------------------------------
//			// Assert
//			// ----------------------------------
//			//sAssert.IsNotNull(user);
//			// Devices are no longer returned by the service
//			// Assert.IsTrue(user.Devices.Any(d => d.DeviceOs == deviceInformation.DeviceOs && d.NotificationToken == deviceInformation.NotificationToken));
//		}

		/// <summary>
		/// Method Under Test: UserWebService.SetEventFilter
		/// Condition: Set event filter for a user
		/// Expected Result: The event filter is set on the user
		/// </summary>
		[Test]
		public async Task TestSetEventFilterSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			IEventFilter eventFilter = new EventFilter
			{
				Radius = 40,
				WithinDays = 14,
				SportFilters = new List<ISportFilter>
				{
					new SportFilter { IsExcluded = true, Sport = "Swimming"},
					new SportFilter { Sport = "Running"},
					new SportFilter { Sport = "Yoga"},
					new SportFilter { Sport = "Cycling", SubSports = new []{"Road", "Mtb"}}
				}
			};

			// ----------------------------------
			// Act
			// ----------------------------------
			IUser user = await _userService.SetEventFilter(_authenticatedUser.Id, eventFilter);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(user);
			Assert.IsNotNull(user.EventFilter);
			Assert.AreEqual(eventFilter.Radius, user.EventFilter.Radius);
			Assert.AreEqual(eventFilter.WithinDays, user.EventFilter.WithinDays);
			Assert.AreEqual(eventFilter.SportFilters.Count, user.EventFilter.SportFilters.Count);
			Assert.IsTrue(user.EventFilter.SportFilters.Any(s => s.IsExcluded.Value && s.Sport == "Swimming"));
			Assert.IsTrue(user.EventFilter.SportFilters.Any(s => (!s.IsExcluded.HasValue || !s.IsExcluded.Value) && s.Sport == "Running"));
			Assert.IsTrue(user.EventFilter.SportFilters.Any(s =>
					(!s.IsExcluded.HasValue || !s.IsExcluded.Value) &&
				s.Sport == "Cycling" &&
				s.SubSports.Count == 2));
		}

		/// <summary>
		/// Method Under Test: UserWebService.Update
		/// Condition: Update user profile for a user
		/// Expected Result: The user profile information is added to the user
		/// </summary>
		[Test]
		public async Task TestUpdateUserSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			IUserProfile userProfile = new UserProfile
			{
				Radius = 25,
				Kilometers = true,
				FavoriteActivities = new [] {"Running", "Yoga"},
				Birthday = new DateTime(1967, 08, 19),
				Gender = Gender.Male,
				AvatarUri = new Uri("http://my.avater.com/snowdog"),
				Interests = "Romantic evenings, holding hands on the beach.",
				Bio = "I was born. Shit happened. I am here."
			};

			IUser user = new YuFit.WebServices.Model.User
			{
				Id = _authenticatedUser.Id,
				Email = "me@me.com",
				Name = "Me",
				UserProfile = userProfile
			};

			user.UserProfile = userProfile;

			// ----------------------------------
			// Act
			// ----------------------------------
			IUser updatedUser = await _userService.UpdateUserInformation(user);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(user);
			Assert.AreEqual(userProfile.Radius, updatedUser.UserProfile.Radius);
			Assert.AreEqual(userProfile.Kilometers, updatedUser.UserProfile.Kilometers);
			Assert.AreEqual(2, updatedUser.UserProfile.FavoriteActivities.Count);
			Assert.AreEqual(userProfile.Birthday, updatedUser.UserProfile.Birthday);
			Assert.AreEqual(userProfile.Interests, updatedUser.UserProfile.Interests);
			Assert.AreEqual(userProfile.Bio, updatedUser.UserProfile.Bio);
		}
    }
}

