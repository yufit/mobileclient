﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;
using YuFit.WebServices.Test.Helper;
using YuFit.WebServices.Test.Model;
using System.Collections.Generic;
using YuFit.WebServices.Exceptions;

namespace YuFit.WebServices.Test
{
	[TestFixture]
	public class UserGroupWebServiceTests
	{
		#pragma warning disable 0414
		IUserWebService _userService;
		IUser _authenticatedUser;
		#pragma warning restore 0414

		IUserGroupWebService _userGroupService;

		private static readonly List<IUserGroup> _createdGroups = new List<IUserGroup>();


		[TestFixtureSetUp]
		public void TestSetup()
		{
			var session = SessionHelper.EstablishSession().Result;
			_authenticatedUser = session.AuthenticatedUser;
			_userService = SessionHelper.YuFitClient.UserService;
			_userGroupService = SessionHelper.YuFitClient.UserGroupService;
		}

		[TestFixtureTearDown]
		public void TestTearDown()
		{
			var tasks = new List<Task>();

			foreach (var group in _createdGroups)
			{
                tasks.Add(_userGroupService.DeleteGroup(group.Id));
			}

			Task.WaitAll(tasks.ToArray());
		}

		[Test]
		public async Task TestGetAllGroups()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			for (int i=0; i<10; ++i) {
				IUserGroup newGroup = new UserGroup
				{
					Name = string.Format("DumbBut{0}", i),
					Description = "Bunch of morons",
					Website = new System.Uri("http://www.youporn.com"),
//                    Type = GroupType.SportClub
				};
				IUserGroup createdGroup = await _userGroupService.CreateGroup(newGroup);
				_createdGroups.Add(createdGroup);
			}

			// ----------------------------------
			// Act
			// ----------------------------------
            IList<IUserGroup> groups = await _userGroupService.GetGroups(0, 100);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(groups);
			Assert.AreEqual(_createdGroups.Count, groups.Count);
		}

		[Test]
		public async Task TestCreateGroup()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			IUserGroup newGroup = new UserGroup
			{
				Name = "DumbBut",
				Description = "Bunch of morons",
				Website = new System.Uri("http://www.youporn.com"),
//                Type = GroupType.SportClub
			};

			// ----------------------------------
			// Act
			// ----------------------------------
			IUserGroup createdGroup = await _userGroupService.CreateGroup(newGroup);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(createdGroup);
			_createdGroups.Add(createdGroup);

			Assert.AreEqual(createdGroup.Name, "DumbBut");
			Assert.AreEqual(createdGroup.Description, "Bunch of morons");
			Assert.AreEqual(createdGroup.Website, new System.Uri("http://www.youporn.com"));
			Assert.AreEqual(createdGroup.Type, "Public");
		}

		[Test]
		public async Task TestCreateGroupPassingInAGroupId()
		{
			// ----------------------------------
			// Arrange/Act/Assert
			// ----------------------------------
			await AssertEx.ThrowsAsync<ArgumentException>(() => _userGroupService.CreateGroup(new UserGroup {
				Id = "cocococo"
			}));
		}

		[Test]
		public async Task TestCreateGroupPassingInNull()
		{
			// ----------------------------------
			// Arrange/Act/Assert
			// ----------------------------------
			await AssertEx.ThrowsAsync<ArgumentNullException>(() => _userGroupService.CreateGroup(null));
		}

		[Test]
		public async Task TestUpdateGroup()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			IUserGroup newGroup = new UserGroup
			{
				Name = "DumbBut",
				Description = "Bunch of morons",
				Website = new System.Uri("http://www.youporn.com"),
//                Type = GroupType.SportClub
			};

			// ----------------------------------
			// Act
			// ----------------------------------
			IUserGroup createdGroup = await _userGroupService.CreateGroup(newGroup);

			createdGroup.Name = "Goonz of hazard";
			createdGroup.Description = "Glorified on your tv";
			createdGroup.Website = new System.Uri("http://www.pornhub.com");
//            createdGroup.Type = GroupType.SportClub;

			IUserGroup updatedGroup = await _userGroupService.UpdateGroup(createdGroup);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(updatedGroup);
			_createdGroups.Add(updatedGroup);

			Assert.AreEqual(updatedGroup.Name, "Goonz of hazard");
			Assert.AreEqual(updatedGroup.Description, "Glorified on your tv");
			Assert.AreEqual(updatedGroup.Website, new System.Uri("http://www.pornhub.com"));
			Assert.AreEqual(updatedGroup.Type, "Private");
		}

		[Test]
		public async Task TestUpdateGroupPassingInNull()
		{
			// ----------------------------------
			// Arrange/Act/Assert
			// ----------------------------------
			await AssertEx.ThrowsAsync<ArgumentNullException>(() => _userGroupService.UpdateGroup(null));
		}

		[Test]
		public async Task TestUpdateGroupPassingInNoId()
		{
			// ----------------------------------
			// Arrange/Act/Assert
			// ----------------------------------
			await AssertEx.ThrowsAsync<ArgumentNullException>(() => _userGroupService.UpdateGroup(new UserGroup {
			}));
		}

		[Test]
		public async Task TestUpdateGroupForInvalidGroupId()
		{
			// ----------------------------------
			// Arrange/Act/Assert
			// ----------------------------------
			await AssertEx.ThrowsAsync<NotFoundException>(() => _userGroupService.UpdateGroup(new UserGroup {
				Id = "cocococo"
			}));
		}
	}
}

