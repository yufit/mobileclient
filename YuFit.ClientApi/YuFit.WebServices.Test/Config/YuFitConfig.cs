﻿using System;
using YuFit.WebServices.Test.Helper;
using YuFit.WebServices.Interface;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Test.Model;
using YuFit.WebServices.Model;

namespace YuFit.WebServices.Test.Config
{
	public enum ConfigType
	{
		Development,
		Staging,
		Production
	}

	public class YuFitConfig : IYuFitConfig
    {
		const string DevelopmentUri = "http://localhost:3000";
		const string StagingUri = "https://staging.api.yu-fit.com:8443";
		const string ProductionUri = "https://production.api.yu-fit.com:8443";

		#region Constructor

		protected YuFitConfig(ConfigType configType, ISessionStore sessionStore)
		{
			switch (configType)
			{
				case ConfigType.Staging:
					ServiceEndpoint = new Uri(StagingUri);
					break;
				case ConfigType.Production:
					ServiceEndpoint = new Uri(ProductionUri);
					break;
				default:
					ServiceEndpoint = new Uri(DevelopmentUri);
					break;

			}

			SessionStore = sessionStore;
		}

		#endregion

		#region Factory Methods

		public static IYuFitConfig Create(ConfigType configType)
		{
			return new YuFitConfig(configType, new StaticSessionStore());
		}

		public static IYuFitConfig Create(ConfigType configType, ISessionStore sessionStore)
		{
			return new YuFitConfig(configType, sessionStore);
		}

		#endregion

		#region IYufitConfig Implementation

        public string ClientVersionNumber { get; private set; }
        public Uri ServiceEndpoint { get; private set; }
        public Uri EmailRegistrationServiceEndpoint { get; private set; }
		public ISessionStore SessionStore { get; private set; }
		public ILogger Logger { get { return null; } }

		public object Create(Type objectType)
		{
			if (objectType == typeof(IDeviceInformation)) {
				return new DeviceInformation();
			}

			if (objectType == typeof(IEventFilter)) {
				return new EventFilter();
			}

			if (objectType == typeof(IEventSchedule)) {
				return new EventSchedule();
			}

			if (objectType == typeof(IFitEvent)) {
				return new FitEvent();
			}

			if (objectType == typeof(IFitEventComment)) {
				return new FitEventComment();
			}

			if (objectType == typeof(IFitEventParticipant)) {
				return new FitEventParticipant();
			}

			if (objectType == typeof(ILocation)) {
				return new Location();
			}

			if (objectType == typeof(INotification)) {
				return new Notification();
			}

			if (objectType == typeof(IServiceIdentity)) {
				return new ServiceIdentity();
			}

			if (objectType == typeof(ISportFilter)) {
				return new SportFilter();
			}

			if (objectType == typeof(IUser)) {
				return new User();
			}

			if (objectType == typeof(IUserProfile)) {
				return new UserProfile();
			}

			if (objectType == typeof(IUserGroup)) {
				return new UserGroup();
			}

			return null;
		}

		#endregion
    }
}

