﻿using System;
using YuFit.WebServices.Interface;

namespace YuFit.WebServices.Test.Helper
{
	public class StaticSessionStore : ISessionStore
    {
		private static string _sessionInformation;

		#region ISessionStore implementation

		public void StoreSessionInformation(string information)
		{
			_sessionInformation = information;
		}

		public string LoadSessionInformation()
		{
			return _sessionInformation;
		}

		#endregion
    }
}

