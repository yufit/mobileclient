using System;
using NUnit.Framework;
using System.Threading.Tasks;

namespace YuFit.WebServices.Test.Helper
{
	public static class AssertEx
	{
		public static async Task ThrowsAsync<TException>(Func<Task> func) where TException : class
		{
			await ThrowsAsync<TException>(func, exception => { });
		} 

		public static async Task ThrowsAsync<TException>(Func<Task> func, Action<TException> action) where TException : class
		{
			var exception = default(TException);
			var expected = typeof(TException);
			Type actual = null;
			try
			{
				await func();
			}
			catch (Exception e)
			{
				exception = e as TException;
				actual = e.GetType();
			}

			Assert.AreEqual(expected, actual);
			action(exception);
		}

		public static async Task DoesNotThrowsAsync(Func<Task> func)
		{
			Exception exception = null;
			try
			{
				await func();
			}
			catch (Exception e)
			{
				exception = e;
			}

			Assert.IsNull(exception);
		}	
	}
	
}
