﻿using System;
using YuFit.WebServices.Interface;

namespace YuFit.WebServices.Test.Helper
{
	public class VoidSessionStore : ISessionStore
    {
		#region ISessionStore implementation

		public void StoreSessionInformation(string information)
		{
		}

		public string LoadSessionInformation()
		{
			return null;
		}

		#endregion
    }
}

