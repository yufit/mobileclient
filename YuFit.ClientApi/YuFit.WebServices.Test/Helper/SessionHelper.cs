﻿using System.Threading.Tasks;
using YuFit.WebServices.Interface;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Model;
using YuFit.WebServices.Test.Config;

namespace YuFit.WebServices.Test.Helper
{
    public static class SessionHelper
    {
		// Change this type to switch environments
        private const ConfigType CONFIG_TYPE = ConfigType.Staging;

		private static IYuFitConfig _config;
		private static IYuFitClient _client;

		private static IYuFitConfig Config
		{
			get 
			{
				if (_config == null)
				{
					_config = YuFitConfig.Create(CONFIG_TYPE);
				}
				return _config;
			}
		}

		public static IYuFitClient YuFitClient
		{
			get
			{
				if (_client == null)
				{
					_client = new YuFitClient(Config);
				}
				return _client;
			}
		}

		public static IUser User
		{
			get
			{
				return YuFitClient.Session.AuthenticatedUser;
			}
		}

		public async static Task<IYuFitSession> EstablishSession()
		{
			if (YuFitClient.Session != null)
			{
				return YuFitClient.Session;
			}
			else
			{
				return await YuFitClient.OpenSession(AuthenticationCredentials.CreateWithToken(ServiceType.Facebook, Tokens.Facebook));
			}
		}
    }
}

