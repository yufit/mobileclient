﻿using System;
using YuFit.WebServices.Interface.Model;

namespace YuFit.WebServices.Test.Model
{
	public class EventSchedule : IEventSchedule
    {
		#region IEventSchedule implementation

		public DateTime			StartTime { get; set; }
		public EventRecurrence	Recurrence { get; set; }
		public int?				Interval { get; set; }
		public DateTime?		NextOccurence { get; set; }
		public int?				NumberOfOccurences { get; set; }
		public DateTime?		ExpiresAt { get; set; }

		#endregion
    }
}

