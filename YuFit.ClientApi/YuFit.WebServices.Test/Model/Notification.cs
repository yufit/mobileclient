﻿using System;
using YuFit.WebServices.Interface.Model;

namespace YuFit.WebServices.Test.Model
{
	public class Notification : INotification
    {
		#region INotification implementation

		public string Id { get; set; }
		public dynamic Data { get; set; }
		public bool IsAcked { get; set; }
		public DateTime CreatedAt { get; set; }

		#endregion
    }
}

