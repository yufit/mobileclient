﻿using System.Collections.Generic;
using YuFit.WebServices.Interface.Model;

namespace YuFit.WebServices.Test.Model
{
    public class SportFilter : ISportFilter
    {
		public bool? 			IsExcluded { get; set; }
		public string 			Sport { get; set; }
		public IList<string> 	SubSports { get; set; }
    }
}

