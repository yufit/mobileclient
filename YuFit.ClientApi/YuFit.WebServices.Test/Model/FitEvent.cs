﻿using System;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Model;
using System.Collections.Generic;

namespace YuFit.WebServices.Test.Model
{
    public class FitEvent : IFitEvent
	{
		#region IFitEvent implementation

		public string			Id { get; set; }
		public string			Name { get; set; }
		public string			Description { get; set; }
		public IEventSchedule	EventSchedule { get; set; }
		public int?				Duration { get; set; }
		public int?				Distance { get; set; }
		public int?				IntensityLevel { get; set; }
		public string			Sport { get; set; }
		public string			SubSport { get; set; }
		public ILocation		Location { get; set; }
		public string			Visibility { get; set; }
		public IList<IFitEventParticipant> Participants { get; set; }
		public IUser			CreatedBy { get; set; }
		public DateTime			CreatedAt { get; set; }
		public DateTime			LastUpdate { get; set; }
        public Boolean?         IsCanceled { get; set; }
        public Boolean?         IsPrivate { get; set; }
        public string           UserGroupId     { get; set; }
        public string           UserGroupName   { get; set; }
        public string           UserGroupAvatarUrl   { get; set; }



        public DateTime? PromotionStartingOn
        {
            get {
                throw new NotImplementedException();
            }
            set {
                throw new NotImplementedException();
            }
        }

        public bool? IsPromoted
        {
            get {
                throw new NotImplementedException();
            }
            set {
                throw new NotImplementedException();
            }
        }

        public string IconUrl
        {
            get {
                throw new NotImplementedException();
            }
            set {
                throw new NotImplementedException();
            }
        }

        public string EventPhotoUrl
        {
            get {
                throw new NotImplementedException();
            }
            set {
                throw new NotImplementedException();
            }
        }

        public string FacebookUrl
        {
            get {
                throw new NotImplementedException();
            }
            set {
                throw new NotImplementedException();
            }
        }

        public string WebsiteUrl
        {
            get {
                throw new NotImplementedException();
            }
            set {
                throw new NotImplementedException();
            }
        }

        public int NumViews
        {
            get {
                throw new NotImplementedException();
            }
            set {
                throw new NotImplementedException();
            }
        }
		#endregion
	}
}

