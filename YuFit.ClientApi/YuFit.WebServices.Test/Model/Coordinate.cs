﻿using YuFit.WebServices.Interface.Model;

namespace YuFit.WebServices.Test.Model
{
    public class Coordinate : ICoordinate
    {
		public double Latitude { get; set; }
		public double Longitude { get; set; }
    }
}

