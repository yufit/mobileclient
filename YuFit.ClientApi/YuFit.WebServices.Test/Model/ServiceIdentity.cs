﻿using YuFit.WebServices.Interface.Model;

namespace YuFit.WebServices.Test.Model
{
    public class ServiceIdentity : IServiceIdentity
    {
		public ServiceType ServiceType { get; set; }
		public bool IsLoginIdentity { get; set; }
		public string UserId { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
    }
}

