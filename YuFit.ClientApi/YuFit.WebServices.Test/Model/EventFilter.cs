﻿using System.Collections.Generic;
using YuFit.WebServices.Interface.Model;

namespace YuFit.WebServices.Test.Model
{
    public class EventFilter : IEventFilter
    {
		public int 					Radius { get; set; }
		public int 					WithinDays { get; set; }
		public IList<ISportFilter> 	SportFilters { get; set; }
    }
}

