﻿using YuFit.WebServices.Interface.Model;

namespace YuFit.WebServices.Test.Model
{
    public class DeviceInformation : IDeviceInformation
    {
        public string DeviceId { get; set; }
		public DeviceOs DeviceOs { get; set; }
		public string NotificationToken { get; set; }
    }
}

