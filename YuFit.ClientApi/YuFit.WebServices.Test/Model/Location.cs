﻿using YuFit.WebServices.Interface.Model;

namespace YuFit.WebServices.Test.Model
{
    public class Location : ILocation
    {
		public	string 	 Name { get; set; }
		public	string 	 StreetAddress { get; set; }
		public	string 	 City { get; set; }
		public	string 	 StateProvince { get; set; }
		public	string 	 Country { get; set; }
		public	double[] Coordinates { get; set; }
    }
}

