﻿using YuFit.WebServices.Interface.Model;
using System.Collections.Generic;

namespace YuFit.WebServices.Test.Model
{
    public class FitEventParticipant : IFitEventParticipant
    {
		public string			UserId { get; set; }
		public string			Name { get; set; }
		public IUserProfile		UserProfile { get; set; }
		public IList<ILocation> Locations {get; set; }
		public bool?			Invited { get; set; }
		public bool?			Going { get; set; }
    }
}

