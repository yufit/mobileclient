﻿using System;
using System.Collections.Generic;
using YuFit.WebServices.Interface.Model;

namespace YuFit.WebServices.Test.Model
{
	public class UserProfile : IUserProfile
    {
		#region IUserProfile implementation

		public int? Radius { get; set; }
		public bool? Kilometers { get; set; }
		public IList<string> FavoriteActivities { get; set; }
		public DateTime? Birthday { get; set; }
		public Gender? Gender { get; set; }
		public Uri AvatarUri { get; set; }
		public string Interests { get; set; }
		public string Bio { get; set; }
        public IList<INotificationPreference> NotificationPreferences { get; set; }
		#endregion
    }
}

