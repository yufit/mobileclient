﻿using System;
using NUnit.Framework;
using YuFit.WebServices.Test.Helper;
using YuFit.WebServices.Interface.WebService;
using System.Collections.Generic;
using YuFit.WebServices.Interface.Model;
using System.Threading.Tasks;
using YuFit.WebServices.Test.Model;
using YuFit.WebServices.Exceptions;

namespace YuFit.WebServices.Test
{

	[TestFixture]
	public class FitEventCommentWebServiceTests
	{
		const string NOT_FOUND_EVENT_ID = "53de6ac8476f749870000000";
		readonly double[] MENDON_PARK_LOCATION =   { -77.5639410, 43.0225503 };

		private static List<IFitEventComment> _createdEventComments = new List<IFitEventComment>();
		private IFitEventCommentWebService _fitEventCommentService;
		private IFitEventWebService _fitEventService;

		IFitEvent _event1;

		/// <summary>
		/// Establish a session and create the service under test.
		/// </summary>
		[TestFixtureSetUp]
		public void TestSetup()
		{
			var session = SessionHelper.EstablishSession().Result;
			_fitEventService = SessionHelper.YuFitClient.FitEventService;
			_fitEventCommentService = SessionHelper.YuFitClient.FitEventCommentService;
		}

		/// <summary>
		/// Cleanup any items that were created by the tests.
		/// </summary>
		[TestFixtureTearDown]
		public void TestTearDown()
		{
			var tasks = new List<Task>();

			foreach (var fitEventComment in _createdEventComments)
			{
				tasks.Add(_fitEventCommentService.DeleteComment(_event1, fitEventComment));
			}

			tasks.Add(_fitEventService.DeleteEvent(_event1));

			Task.WaitAll(tasks.ToArray());
		}

		/// <summary>
		/// Method Under Test: FitEventCommentWebService.AddComment
		/// Condition: Valid event and valid comment information
		/// Expected Result: The created comment is returned
		/// </summary>
		[Test]
		public async Task TestCreateEventCommentForValidEventSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			await CreateEvent();

			// ----------------------------------
			// Act
			// ----------------------------------
			var createdComment = await CreateTrackedFitEventComment(_event1, new FitEventComment {
				Comment = "A comment"
			});

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(createdComment);
			Assert.AreEqual(createdComment.Comment, "A comment");
		}

		/// <summary>
		/// Method Under Test: FitEventCommentWebService.AddComment
		/// Condition: Invalid event and valid comment information
		/// Expected Result: Will throw a NotFoundException
		/// </summary>
		[Test]
		public async Task TestCreateEventCommentForUnknownEventSuccess()
		{
			// ----------------------------------
			// Arrange/Act/Assert
			// ----------------------------------
			await AssertEx.ThrowsAsync<NotFoundException>(() => _fitEventCommentService.AddComment(new FitEvent {
				Id = "unknown event"
			}, new FitEventComment {
				Comment = "A comment"
			}));
		}

		/// <summary>
		/// Method Under Test: FitEventCommentWebService.GetComments
		/// Condition: Valid event
		/// Expected Result: Will get the comments
		/// </summary>
		[Test]
		public async Task TestGetEventCommentsSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			await CreateEvent();

			// ----------------------------------
			// Act
			// ----------------------------------
			for (int i=0; i<10; ++i) {
				await CreateTrackedFitEventComment(_event1, new FitEventComment {
					Comment = string.Format("A comment {0}", i)
				});
			}

            var comments = await _fitEventCommentService.GetComments(_event1, 0, 100);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.AreEqual(comments.Count, 10);
		}

		/// <summary>
		/// Method Under Test: FitEventCommentWebService.GetComments
		/// Condition: Invalid event
		/// Expected Result: Will throw a NotFoundException
		/// </summary>
		[Test]
		public async Task TestGetEventCommentsForUnknownEventSuccess()
		{
			// ----------------------------------
			// Arrange/Act/Assert
			// ----------------------------------
			await AssertEx.ThrowsAsync<NotFoundException>(() => _fitEventCommentService.GetComments(new FitEvent {
				Id = "unknown event"
            }, 0, 100));
		}

		/// <summary>
		/// Method Under Test: FitEventCommentWebService.DeleteComment
		/// Condition: Valid event and valid comment information
		/// Expected Result: The comment is deleted
		/// </summary>
		[Test]
		public async Task TestDestroyEventCommentForValidEventSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			await CreateEvent();

			// ----------------------------------
			// Act
			// ----------------------------------
			var createdComment = await _fitEventCommentService.AddComment(_event1, new FitEventComment {
				Comment = "A comment"
			});

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(createdComment);
			await AssertEx.DoesNotThrowsAsync(() => _fitEventCommentService.DeleteComment(_event1, createdComment));
            var comments = await _fitEventCommentService.GetComments(_event1, 0, 100);
			Assert.AreEqual(comments.Count, 0);
		}

		/// <summary>
		/// Method Under Test: FitEventCommentWebService.DeleteComment
		/// Condition: Valid event and invalid comment id
		/// Expected Result: Will throw a NotFoundException
		/// </summary>
		[Test]
		public async Task TestDestroyEventCommentForUnknownCommentId()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			await CreateEvent();

			// ----------------------------------
			// Act
			// ----------------------------------
			var createdComment = await CreateTrackedFitEventComment(_event1, new FitEventComment {
				Comment = "A comment"
			});

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(createdComment);

			var badComment = new FitEventComment { Id = "unknown" };

			await AssertEx.ThrowsAsync<NotFoundException>(() => _fitEventCommentService.DeleteComment(_event1, badComment));
		}

		/// <summary>
		/// Method Under Test: FitEventCommentWebService.DeleteComment
		/// Condition: Invalid event
		/// Expected Result: Will throw a NotFoundException
		/// </summary>
		[Test]
		public async Task TestDestroyEventCommentForUnknownEventId()
		{
			// ----------------------------------
			// Arrange/Act/Assert
			// ----------------------------------
			await AssertEx.ThrowsAsync<NotFoundException>(() => _fitEventCommentService.DeleteComment(new FitEvent {
				Id = "unknown event"
			}, new FitEventComment {
				Id = "unknown comment"
			}));
		}

		#region Private Methods

		private async Task<IFitEventComment> CreateTrackedFitEventComment(IFitEvent fitEvent, FitEventComment fitEventComment)
		{
			var trackedEventComment = await _fitEventCommentService.AddComment(fitEvent, fitEventComment);
			_createdEventComments.Add(trackedEventComment);

			return trackedEventComment;
		}

		private async Task<IFitEvent> CreateEvent()
		{
			var fitEvent = new FitEvent {
				Name = "My Test Event",
				Description = "My Test Event Description",
				Duration = 2,
				IntensityLevel = 1,
				Sport = "Running",
				SubSport = "Trail",
				Location = new Location
				{
					Name = "Mendon Ponds Park",
					Coordinates = MENDON_PARK_LOCATION
				},
				EventSchedule = new EventSchedule
				{
					StartTime = DateTime.UtcNow.AddDays(2.0),
					Recurrence = EventRecurrence.Never
				},
				Visibility = "public",
                IsPrivate = false,
				Participants = new List<IFitEventParticipant>
				{
					new FitEventParticipant
					{
						UserId = SessionHelper.User.Id,
						Invited = true
					}
				}
			};

			_event1 = await _fitEventService.CreateEvent(fitEvent);
			return _event1;
		}

		#endregion
	}
}

