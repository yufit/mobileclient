﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using YuFit.WebServices.Exceptions;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;
using YuFit.WebServices.Model;
using YuFit.WebServices.WebService;
using YuFit.WebServices.Test.Config;
using YuFit.WebServices.Test.Helper;

namespace YuFit.WebServices.Test
{
    [TestFixture]
    public class AuthenticationServiceTests
    {
		IAuthenticationWebService _authService;

        #pragma warning disable 414
		YuFitClient _yuFitClient;
        #pragma warning restore 414

		[TestFixtureSetUp]
		public void TestSetup()
		{
			// Create a YuFitClient with the VoidSessionStore
			// to force authentication for each test.
            var config = YuFitConfig.Create(ConfigType.Staging, new VoidSessionStore());
            _yuFitClient = new YuFitClient(config); // Need this so Json.Net has access to the CustomCreationConverters
            _authService = new AuthenticationWebService(config.ServiceEndpoint);
		}

		/// <summary>
		/// Method Under Test: AuthenticationWebService.OpenSession
		/// Condition: Credentials created with valid Facebook token
		/// Expected Result: A User is returned with a populated ApiToken
		/// </summary>
        [Test]
        public async Task TestLoginWithFacebookSuccess()
        {
            // ----------------------------------
			// Arrange
			// ----------------------------------
			IAuthenticationCredentials creds = AuthenticationCredentials.CreateWithToken(ServiceType.Facebook, Tokens.Facebook);

			// ----------------------------------
			// Act
			// ----------------------------------
			User user = await _authService.AuthenticateUser(creds);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(user);
			Assert.IsNotNull(user.Token);
			Assert.IsNotNullOrEmpty(user.Token.ApiToken);
        }

		/// <summary>
		/// Method Under Test: AuthenticationWebService.OpenSession
		/// Condition: Credentials created with valid Facebook token
		/// Expected Result: A User is returned with a populated ApiToken
		/// </summary>
		[Test]
		public async Task TestLoginWithFacebookUnauthorized()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			IAuthenticationCredentials creds = AuthenticationCredentials.CreateWithToken(ServiceType.Facebook, "InvalidToken");

			// ----------------------------------
			// Act
			// ----------------------------------
			bool isUnauthorizedExceptionThrown = false;

			try
			{
				await _authService.AuthenticateUser(creds);
			}
			catch (UnauthorizedException)
			{
				isUnauthorizedExceptionThrown = true;
			}

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsTrue(isUnauthorizedExceptionThrown);
		}
    }
}

