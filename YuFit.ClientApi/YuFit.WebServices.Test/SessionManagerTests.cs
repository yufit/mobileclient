﻿using System;
using NUnit.Framework;
using Moq;
using YuFit.WebServices.Interface;
using YuFit.WebServices.Interface.WebService;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Model;
using YuFit.WebServices.Exceptions;
using System.Threading.Tasks;
using System.Collections.Generic;
using YuFit.WebServices.Test.Model;
using System.Linq;

namespace YuFit.WebServices.Test
{
	[TestFixture]
    public class SessionManagerTests
    {
		private User _authenticatedUser;
		private YuFitSession _testSession;
		private string _serializedTestSession;

		[TestFixtureSetUp]
		public void SetupTests()
		{
			_authenticatedUser = CreateUser("Dagon");
			_testSession = CreateSession(_authenticatedUser);
			_serializedTestSession = _testSession.Serialize();
		}

		/// <summary>
		/// Method Under Test: SessionManager Constructor
		/// Condition: Previous session exists
		/// Expected Result: Session is loaded and exposed as Session
		/// </summary>
		[Test]
		public void TestLoadPreviousSessionNotExists()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var mockAuthenticationService = new Mock<IAuthenticationWebService>();

			var mockSessionStore = new Mock<ISessionStore>();
			mockSessionStore.Setup(f => f.LoadSessionInformation());

			// ----------------------------------
			// Act
			// ----------------------------------
			var sessionManager = new SessionManager(mockAuthenticationService.Object, mockSessionStore.Object);

			// ----------------------------------
			// Assert
			// ----------------------------------
			mockSessionStore.VerifyAll();

			Assert.IsNull(sessionManager.Session);
		}

		/// <summary>
		/// Method Under Test: SessionManager Constructor
		/// Condition: Previous session exists
		/// Expected Result: Session is loaded and exposed as Session
		/// </summary>
		[Test]
		public void TestLoadPreviousSessionExists()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var mockAuthenticationService = new Mock<IAuthenticationWebService>();

			var mockSessionStore = new Mock<ISessionStore>();
			mockSessionStore.Setup(f => f.LoadSessionInformation()).Returns(_serializedTestSession);

			// ----------------------------------
			// Act
			// ----------------------------------
			var sessionManager = new SessionManager(mockAuthenticationService.Object, mockSessionStore.Object);

			// ----------------------------------
			// Assert
			// ----------------------------------
			mockSessionStore.VerifyAll();

			Assert.IsNotNull(sessionManager.Session);
			Assert.AreEqual(_testSession.AuthenticatedUser.ServiceIdentities.First().Name, sessionManager.Session.AuthenticatedUser.ServiceIdentities.First().Name);
		}

		/// <summary>
		/// Method Under Test: SessionManager.OpenSession
		/// Condition: Valid credentials provided
		/// Expected Result: Session is created, stored, and exposed as Session
		/// </summary>
		[Test]
		public async Task TestOpenSessionSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var mockAuthenticationService = new Mock<IAuthenticationWebService>();
			mockAuthenticationService.Setup(f => f.AuthenticateUser(It.IsAny<IAuthenticationCredentials>()))
				.ReturnsAsync(_authenticatedUser);

			var mockSessionStore = new Mock<ISessionStore>();
			mockSessionStore.Setup(f => f.StoreSessionInformation(It.IsAny<string>()));

			var sessionManager = new SessionManager(mockAuthenticationService.Object, mockSessionStore.Object);

			// ----------------------------------
			// Act
			// ----------------------------------
			IYuFitSession session = await sessionManager.OpenSession(_testSession.AuthenticationCredentials);

			// ----------------------------------
			// Assert
			// ----------------------------------
			mockAuthenticationService.VerifyAll();
			mockSessionStore.VerifyAll();

			Assert.IsNotNull(session);
			Assert.AreSame(session, sessionManager.Session);
		}

		/// <summary>
		/// Method Under Test: SessionManager.OpenSession
		/// Condition: Invalid credentials provided
		/// Expected Result: Session is null, UnauthorizedException is thrown
		/// </summary>
		[Test]
		public async Task TestOpenSessionUnauthorized()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var mockAuthenticationService = new Mock<IAuthenticationWebService>();
			mockAuthenticationService.Setup(f => f.AuthenticateUser(It.IsAny<IAuthenticationCredentials>()))
				.ThrowsAsync(new UnauthorizedException());

			 var mockSessionStore = new Mock<ISessionStore>();
			mockSessionStore.Setup(f => f.StoreSessionInformation(It.IsAny<string>()));

			var sessionManager = new SessionManager(mockAuthenticationService.Object, mockSessionStore.Object);

			bool isUnauthorizedExceptionThrown = false;

			// ----------------------------------
			// Act
			// ----------------------------------
			IYuFitSession session = null;
			try
			{
				session = await sessionManager.OpenSession(_testSession.AuthenticationCredentials);
			}
			catch (UnauthorizedException)
			{
				isUnauthorizedExceptionThrown = true;
			}

			// ----------------------------------
			// Assert
			// ----------------------------------
			mockAuthenticationService.VerifyAll();
			mockSessionStore.Verify(f => f.StoreSessionInformation(It.IsAny<string>()), Times.Never);
			Assert.IsTrue(isUnauthorizedExceptionThrown);

			Assert.IsNull(session);
			Assert.IsNull(sessionManager.Session);
		}

		/// <summary>
		/// Method Under Test: SessionManager.EnsureSession
		/// Condition: Session is null
		/// Expected Result: Returns false
		/// </summary>
		[Test]
		public async Task TestEnsureSessionNullSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var mockAuthenticationService = new Mock<IAuthenticationWebService>();
			var mockSessionStore = new Mock<ISessionStore>();

			var sessionManager = new SessionManager(mockAuthenticationService.Object, mockSessionStore.Object);

			// ----------------------------------
			// Act
			// ----------------------------------
			var result = await sessionManager.EnsureSession();

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsFalse(result);
			Assert.IsNull(sessionManager.Session);
		}

		/// <summary>
		/// Method Under Test: SessionManager.EnsureSession
		/// Condition: Session exists and valid
		/// Expected Result: Returns true, authentication service is not called
		/// </summary>
		[Test]
		public async Task TestEnsureSessionNotExpiredSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var mockAuthenticationService = new Mock<IAuthenticationWebService>();
			mockAuthenticationService.Setup(f => f.AuthenticateUser(It.IsAny<IAuthenticationCredentials>()));

			var mockSessionStore = new Mock<ISessionStore>();
			mockSessionStore.Setup(f => f.LoadSessionInformation()).Returns(_serializedTestSession);

			var sessionManager = new SessionManager(mockAuthenticationService.Object, mockSessionStore.Object);

			// ----------------------------------
			// Act
			// ----------------------------------
			var result = await sessionManager.EnsureSession();

			// ----------------------------------
			// Assert
			// ----------------------------------
			mockAuthenticationService.Verify(f => f.AuthenticateUser(It.IsAny<IAuthenticationCredentials>()), Times.Never);

			Assert.IsTrue(result);
			Assert.IsNotNull(sessionManager.Session);
			Assert.AreEqual(_testSession.AuthenticationToken.ApiToken, sessionManager.Session.AuthenticationToken.ApiToken);
		}

		/// <summary>
		/// Method Under Test: SessionManager.EnsureSession
		/// Condition: Session exists and is expired
		/// Expected Result: Returns true
		///                  Authentication service is called
		///                  Session is saved with new token
		/// </summary>
		[Test]
		public async Task TestEnsureSessionExpiredSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var session = CreateSession("Abbath", "EXPIREDTOKEN", DateTime.Now - TimeSpan.FromMinutes(1));
			var refreshedUser = CreateUser("Abbath", "NEWTOKEN", DateTime.Now + TimeSpan.FromDays(30));

			var mockAuthenticationService = new Mock<IAuthenticationWebService>();
			mockAuthenticationService.Setup(f => f.AuthenticateUser(It.IsAny<IAuthenticationCredentials>()))
				.ReturnsAsync(refreshedUser);

			var mockSessionStore = new Mock<ISessionStore>();
			mockSessionStore.Setup(f => f.StoreSessionInformation(It.IsAny<string>()));
			mockSessionStore.Setup(f => f.LoadSessionInformation()).Returns(session.Serialize());

			var sessionManager = new SessionManager(mockAuthenticationService.Object, mockSessionStore.Object);

			// ----------------------------------
			// Act
			// ----------------------------------
			var result = await sessionManager.EnsureSession();

			// ----------------------------------
			// Assert
			// ----------------------------------
			mockAuthenticationService.VerifyAll();
			mockSessionStore.VerifyAll();

			Assert.IsTrue(result);
			Assert.IsNotNull(sessionManager.Session);
			Assert.AreEqual(refreshedUser.Token.ApiToken, sessionManager.Session.AuthenticationToken.ApiToken);
		}

		/// <summary>
		/// Method Under Test: SessionManager.EnsureSession
		/// Condition: Session exists and is expired
		///            Reauthentication fails
		/// Expected Result: Returns false
		///                  Authentication service is called
		///                  Session is cleared
		/// </summary>
		[Test]
		public async Task TestEnsureSessionExpiredReauthFailed()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var session = CreateSession("Abbath", "EXPIREDTOKEN", DateTime.Now - TimeSpan.FromMinutes(1));

			var mockAuthenticationService = new Mock<IAuthenticationWebService>();
			mockAuthenticationService.Setup(f => f.AuthenticateUser(It.IsAny<IAuthenticationCredentials>()))
				.ThrowsAsync(new UnauthorizedException());

			var mockSessionStore = new Mock<ISessionStore>();
			mockSessionStore.Setup(f => f.StoreSessionInformation(It.IsAny<string>()));
			mockSessionStore.Setup(f => f.LoadSessionInformation()).Returns(session.Serialize());

			var sessionManager = new SessionManager(mockAuthenticationService.Object, mockSessionStore.Object);

			// ----------------------------------
			// Act
			// ----------------------------------
			bool isUnauthorizedExcptionThrown = false;
			try
			{
				await sessionManager.EnsureSession();
			}
			catch (UnauthorizedException)
			{
				isUnauthorizedExcptionThrown = true;
			}

			// ----------------------------------
			// Assert
			// ----------------------------------
			mockAuthenticationService.VerifyAll();
			mockSessionStore.VerifyAll();

			Assert.IsTrue(isUnauthorizedExcptionThrown);
			Assert.IsNull(sessionManager.Session);
		}

		#region Test Helper Methods

		private YuFitSession CreateSession(string firstName, string apiToken = null, DateTime? expiresAt = null)
		{
			return CreateSession(CreateUser(firstName, apiToken, expiresAt));
		}

		private YuFitSession CreateSession(User user)
		{
			var session = new YuFitSession
			{
				AuthenticationCredentials = AuthenticationCredentials.CreateWithToken(ServiceType.Facebook, "SomeToken"),
				AuthenticatedUser = user,
				AuthenticationToken = user.Token
			};

			return session;
		}

		private User CreateUser(string name, string apiToken = null, DateTime? expiresAt = null)
		{
			return new User
			{
				Token = new Token
				{
					ApiToken = apiToken ?? "APITOKEN",
					ExpiresAt = expiresAt ?? DateTime.Now + TimeSpan.FromDays(1)
				},
				ServiceIdentities = new List<IServiceIdentity> {
					new ServiceIdentity {
						ServiceType = ServiceType.Facebook,
						Name = name
					}
				}

			};
		}

		#endregion
    }
}

