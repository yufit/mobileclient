﻿
===============================
YuFit Web Services Test Project
===============================

This test project exercises tests against a running YuFit Server and
requires valid tokens in order to function. These tests are more
integration tests than unit tests as they are designed to ensure correct
behavior and type mapping to and from the YuFit server.

Before running tests, update Tokens.cs with a current token to
be used for the tests. In addition, SessionHelper should be checked to
make sure it is using the correct service for establishing the session
being used for the tests.

