﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using YuFit.WebServices.Exceptions;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;
using YuFit.WebServices.Model;
using YuFit.WebServices.Test.Helper;
using YuFit.WebServices.Test.Model;

namespace YuFit.WebServices.Test
{
	[TestFixture]
    public class FitEventWebServiceTests
    {
		const string NOT_FOUND_EVENT_ID = "53de6ac8476f749870000000";
		double[] PERINTON_PARK_LOCATION = { -77.4569960, 43.1015325 };
		double[] MENDON_PARK_LOCATION =   { -77.5639410, 43.0225503 };
		double[] TREMBLAY_PARK_LOCATION = { -73.4273720, 45.5463660 };
		double[] MONTREAL_LOCATION =      { -73.7120832, 45.5601451 };

		private static List<IFitEvent> _createdEvents = new List<IFitEvent>();
		private IFitEventWebService _fitEventService;

		/// <summary>
		/// Establish a session and create the service under test.
		/// </summary>
		[TestFixtureSetUp]
		public void TestSetup()
		{
			var session = SessionHelper.EstablishSession().Result;
			_fitEventService = SessionHelper.YuFitClient.FitEventService;
		}

		/// <summary>
		/// Cleanup any items that were created by the tests.
		/// </summary>
		[TestFixtureTearDown]
		public void TestTearDown()
		{
			var tasks = new List<Task>();

			foreach (var fitEvent in _createdEvents)
			{
				tasks.Add(_fitEventService.DeleteEvent(fitEvent));
			}

			Task.WaitAll(tasks.ToArray());
		}

		#region Create Event Tests

		/// <summary>
		/// Method Under Test: FitEventWebService.CreateEvent
		/// Condition: Valid event information
		/// Expected Result: An event is returned that was created by the authenticated user
		/// </summary>
		[Test]
		public async Task TestCreateEventSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var fitEvent = new FitEvent {
				Name = "My Test Event",
				Description = "My Test Event Description",
				Duration = 2,
				IntensityLevel = 1,
				Sport = "Running",
				SubSport = "Trail",
				Location = new Location
				{
					Name = "Mendon Ponds Park",
					Coordinates = MENDON_PARK_LOCATION
				},
				EventSchedule = new EventSchedule
				{
					StartTime = DateTime.UtcNow.AddDays(2.0),
					Recurrence = EventRecurrence.Never
				},
				Visibility = "public",
				Participants = new List<IFitEventParticipant>
				{
					new FitEventParticipant
					{
						UserId = SessionHelper.User.Id,
						Invited = true
					}
				}
			};

			// ----------------------------------
			// Act
			// ----------------------------------
			var createdEvent = await CreateTrackedFitEvent(fitEvent);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(createdEvent);
			Assert.AreEqual(fitEvent.Name,        createdEvent.Name);
			Assert.AreEqual(fitEvent.Description, createdEvent.Description);
			Assert.AreEqual(fitEvent.Duration,    createdEvent.Duration);
			Assert.AreEqual(fitEvent.Sport,       createdEvent.Sport);
			Assert.AreEqual(fitEvent.SubSport,    createdEvent.SubSport);
			Assert.AreEqual(fitEvent.Location.Coordinates[0], createdEvent.Location.Coordinates[0]);
			Assert.AreEqual(fitEvent.Location.Coordinates[1], createdEvent.Location.Coordinates[1]);
			Assert.That(createdEvent.EventSchedule.StartTime,   Is.EqualTo(fitEvent.EventSchedule.StartTime).Within(1).Minutes);
			Assert.IsNotNull(createdEvent.Participants);
			Assert.AreEqual(fitEvent.Participants.Count, createdEvent.Participants.Count);
            Assert.IsTrue(createdEvent.Participants.All(p => (p.Invited.HasValue && p.Invited.Value) && !p.Going.HasValue));
		}

		#endregion

		#region Update Event Tests

		/// <summary>
		/// Method Under Test: FitEventWebService.UpdateEvent
		/// Condition: Valid event information
		/// Expected Result: The updated event is returned
		/// </summary>
		[Test]
		public async Task TestUpdateEventSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var fitEvent = new FitEvent {
				Name = "My Original Event",
				Description = "My Original Event Description",
				Duration = 2,
				IntensityLevel = 1,
				Sport = "Running",
				Location = new Location
				{
					Name = "Mendon Ponds Park",
					Coordinates = MENDON_PARK_LOCATION
				},
				EventSchedule = new EventSchedule
				{
					StartTime = DateTime.UtcNow.AddDays(2.0),
					Recurrence = EventRecurrence.Never
				},
				Visibility = "public"
			};

			var createdEvent = await CreateTrackedFitEvent(fitEvent);

			createdEvent.Name = "My Updated Event";
			createdEvent.Description = "My Updated Event Description";
			createdEvent.Duration = 3;
			createdEvent.IntensityLevel = 2;
			createdEvent.Sport = "Cycling";
			createdEvent.SubSport = "Road";
			createdEvent.Location = new Location {
				Name = "Perinton Park",
				Coordinates = PERINTON_PARK_LOCATION
			};
			createdEvent.EventSchedule = new EventSchedule {
				StartTime = DateTime.Now.AddHours(8).ToUniversalTime(),
				Recurrence = EventRecurrence.Never
			};
			createdEvent.Visibility = "private";
			createdEvent.Participants = new List<IFitEventParticipant> {
				new FitEventParticipant {
					UserId = SessionHelper.User.Id,
					Invited = true
				}
			};

			// ----------------------------------
			// Act
			// ----------------------------------
			var updatedEvent = await _fitEventService.UpdateEvent(createdEvent);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(updatedEvent);
			Assert.AreEqual(createdEvent.Name,        updatedEvent.Name);
			Assert.AreEqual(createdEvent.Description, updatedEvent.Description);
			Assert.AreEqual(createdEvent.Duration,    updatedEvent.Duration);
			Assert.AreEqual(createdEvent.Sport,       updatedEvent.Sport);
			Assert.AreEqual(createdEvent.SubSport,    updatedEvent.SubSport);
			Assert.AreEqual(createdEvent.Location.Coordinates[0], updatedEvent.Location.Coordinates[0]);
			Assert.AreEqual(createdEvent.Location.Coordinates[1], updatedEvent.Location.Coordinates[1]);
			Assert.That(updatedEvent.EventSchedule.StartTime,   Is.EqualTo(createdEvent.EventSchedule.StartTime).Within(1).Minutes);
			Assert.IsNotNull(updatedEvent.Participants);
			Assert.AreEqual(1, updatedEvent.Participants.Count);
			Assert.AreEqual(SessionHelper.User.Id, updatedEvent.Participants.First().UserId);
            Assert.IsTrue(updatedEvent.Participants.First().Invited.HasValue && updatedEvent.Participants.First().Invited.Value);
		}
			
		/// <summary>
		/// Method Under Test: FitEventWebService.UpdateEvent
		/// Condition: Valid event information
		/// Expected Result: The updated event is returned
		/// </summary>
		[Test]
		public async Task TestUpdateEventNotFound()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var fitEvent = new FitEvent {
				Id = "53de6ac8476f749870000000",
				Name = "My Original Event",
				Description = "My Original Event Description",
				Duration = 2,
				IntensityLevel = 1,
				Sport = "Running",
				Location = new Location
				{
					Name = "Mendon Ponds Park",
					Coordinates = MENDON_PARK_LOCATION
				},
				EventSchedule = new EventSchedule
				{
					StartTime = DateTime.UtcNow.AddDays(2.0),
					Recurrence = EventRecurrence.Never
				},
				Visibility = "public"
			};

			// ----------------------------------
			// Act
			// ----------------------------------
			bool isNotFoundExceptionThrown = false;
			IFitEvent updatedEvent = null;
			try
			{
				updatedEvent = await _fitEventService.UpdateEvent(fitEvent);
			}
			catch (Exception ex)
			{
				isNotFoundExceptionThrown = (ex is NotFoundException);
			}

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsTrue(isNotFoundExceptionThrown);
			Assert.IsNull(updatedEvent);
		}

		#endregion

		#region Get Event Tests

		/// <summary>
		/// Method Under Test: FitEventWebService.GetEvent
		/// Condition: Create an event then get the event by ID
		/// Expected Result: Event should be equal to the created event
		/// </summary>
		[Test]
		public async Task TestGetEventSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var fitEvent = new FitEvent {
				Name = "Get Test Event",
				Description = "Get Test Event Description",
				Duration = 1,
				IntensityLevel = 3,
				Sport = "Hiking",
				SubSport = "Park",
				Location = new Location
				{
					Name = "Mendon Ponds Park",
					Coordinates = MENDON_PARK_LOCATION
				},
				EventSchedule = new EventSchedule
				{
					StartTime = DateTime.UtcNow.AddDays(5.0),
					Recurrence = EventRecurrence.Never
				},
				Visibility = "public",
				Participants = new List<IFitEventParticipant> {
					new FitEventParticipant {
						UserId = SessionHelper.User.Id,
						Invited = true
					}
				}
			};

			var newEvent = await CreateTrackedFitEvent(fitEvent);

			// ----------------------------------
			// Act
			// ----------------------------------
			var createdEvent = await _fitEventService.GetEvent(newEvent.Id);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(createdEvent);
			Assert.AreEqual(fitEvent.Name,        createdEvent.Name);
			Assert.AreEqual(fitEvent.Description, createdEvent.Description);
			Assert.AreEqual(fitEvent.Duration,    createdEvent.Duration);
			Assert.AreEqual(fitEvent.Sport,       createdEvent.Sport);
			Assert.AreEqual(fitEvent.SubSport,    createdEvent.SubSport);
			Assert.AreEqual(fitEvent.Location.Coordinates[0], createdEvent.Location.Coordinates[0]);
			Assert.AreEqual(fitEvent.Location.Coordinates[1], createdEvent.Location.Coordinates[1]);
			Assert.That(createdEvent.EventSchedule.StartTime,   Is.EqualTo(fitEvent.EventSchedule.StartTime).Within(1).Minutes);
			Assert.IsNotNull(createdEvent.Participants);
			Assert.AreEqual(fitEvent.Participants.Count, createdEvent.Participants.Count);
            Assert.IsTrue(createdEvent.Participants.All(p => (p.Invited.HasValue && p.Invited.Value) && !p.Going.HasValue));
		}

		/// <summary>
		/// Method Under Test: FitEventWebService.GetEvent
		/// Condition: Get an event with an unknown id
		/// Expected Result: NotFoundException is thrown
		/// </summary>
		[Test]
		public async Task TestGetEventNotFound()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------

			// ----------------------------------
			// Act
			// ----------------------------------
			bool isNotFoundExceptionThrown = false;
			IFitEvent fitEvent = null;
			try
			{
				fitEvent = await _fitEventService.GetEvent("53de6ac8476f749870000000");
			}
			catch (Exception ex)
			{
				isNotFoundExceptionThrown = (ex is NotFoundException);
			}

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsTrue(isNotFoundExceptionThrown);
			Assert.IsNull(fitEvent);
		}

		#endregion

		#region Search Event Tests

		/// <summary>
		/// Method Under Test: FitEventWebService.SearchForEvents
		/// Condition: Create 2 events (Perinton, Boise du Tremblay) search with Montreal coordinates
		/// Expected Result: Tremblay should be the returned event
		/// </summary>
		[Test]
		public async Task TestSearchForEventsSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var perintonEvent = new FitEvent {
				Name = "Perinton Run",
				Description = "Perinton Run Description",
				Duration = 1,
				IntensityLevel = 3,
				Sport = "Running",
				Location = new Location
				{
					Name = "Perinton Park",
					Coordinates = PERINTON_PARK_LOCATION
				},
				EventSchedule = new EventSchedule
				{
					StartTime = DateTime.UtcNow.AddDays(5.0),
					Recurrence = EventRecurrence.Never
				},
				Visibility = "public"
			};

			var tremblayEvent = new FitEvent {
				Name = "Tremblay Run",
				Description = "Tremblay Run Description",
				Duration = 1,
				IntensityLevel = 3,
				Sport = "Running",
				Location = new Location
				{
					Name = "Tremblay Park",
					Coordinates = TREMBLAY_PARK_LOCATION
				},
				EventSchedule = new EventSchedule
				{
					StartTime = DateTime.UtcNow.AddDays(5.0),
					Recurrence = EventRecurrence.Never
				},
				Visibility = "public"
			};
			await CreateTrackedFitEvent(perintonEvent);
			var newTremblayEvent = await CreateTrackedFitEvent(tremblayEvent);

			var searchCoordinates = new Coordinate {
				Longitude = MONTREAL_LOCATION[0],
				Latitude = MONTREAL_LOCATION[1]
			};

			// ----------------------------------
			// Act
			// ----------------------------------
            var foundEvents = await _fitEventService.SearchForEvents(0, 100, searchCoordinates, 50.0f);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(foundEvents);
			Assert.AreEqual(1, foundEvents.Count);
			Assert.AreEqual(newTremblayEvent.Id, foundEvents.First().Id);
		}

		/// <summary>
		/// Method Under Test: FitEventWebService.SearchForEvents
		/// Condition: Create 2 events (Perinton, Mendon)
		///            Search with Montreal coordinates
		/// Expected Result: No events should be returned
		/// </summary>
		[Test]
		public async Task TestSearchForEventsNotFound()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var perintonEvent = new FitEvent {
				Name = "Perinton Run",
				Description = "Perinton Run Description",
				Duration = 1,
				IntensityLevel = 3,
				Sport = "Running",
				Location = new Location
				{
					Name = "Perinton Park",
					Coordinates = PERINTON_PARK_LOCATION
				},
				EventSchedule = new EventSchedule
				{
					StartTime = DateTime.UtcNow.AddDays(5.0),
					Recurrence = EventRecurrence.Never
				},
				Visibility = "public"
			};

			var mendonEvent = new FitEvent {
				Name = "Mendon Run",
				Description = "Mendon Run Description",
				Duration = 1,
				IntensityLevel = 3,
				Sport = "Running",
				Location = new Location
				{
					Name = "Mendon Ponds Park",
					Coordinates = MENDON_PARK_LOCATION
				},
				EventSchedule = new EventSchedule
				{
					StartTime = DateTime.UtcNow.AddDays(5.0),
					Recurrence = EventRecurrence.Never
				},
				Visibility = "public"
			};
			await CreateTrackedFitEvent(perintonEvent);
			await CreateTrackedFitEvent(mendonEvent);

			var searchCoordinates = new Coordinate {
				Longitude = MONTREAL_LOCATION[0],
				Latitude = MONTREAL_LOCATION[1]
			};

			// ----------------------------------
			// Act
			// ----------------------------------
            var foundEvents = await _fitEventService.SearchForEvents(0, 100, searchCoordinates, 50.0f);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.IsNotNull(foundEvents);
			Assert.AreEqual(0, foundEvents.Count);
		}

		#endregion

		#region My Upcoming Events Tests

		[Test]
		public async Task TestMyUpcomingEventsSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var fitEvent1 = new FitEvent {
				Name = "My Invited Event",
				Description = "My Invited Event Description",
				Duration = 2,
				IntensityLevel = 1,
				Sport = "Running",
				SubSport = "Trail",
				Location = new Location
				{
					Name = "Mendon Ponds Park",
					Coordinates = MENDON_PARK_LOCATION
				},
				EventSchedule = new EventSchedule
				{
					StartTime = DateTime.UtcNow.AddDays(2.0),
					Recurrence = EventRecurrence.Never
				},
				Visibility = "public",
				Participants = new List<IFitEventParticipant> {
					new FitEventParticipant {
						UserId = SessionHelper.User.Id,
						Invited = true
					}
				}
			};
			var invitedEvent = await CreateTrackedFitEvent(fitEvent1);
			await _fitEventService.SetGoing(invitedEvent.Id, true);

			// ----------------------------------
			// Act
			// ----------------------------------
			var myUpcomingEvents = await _fitEventService.MyUpcomingEvents(0, 100);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.AreEqual(1, myUpcomingEvents.Count);
			Assert.AreEqual(invitedEvent.Id, myUpcomingEvents.First().Id);
		}
		#endregion

		#region My Invited Events Tests

		[Test]
		public async Task TestMyInvitedEventsSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var fitEvent1 = new FitEvent {
				Name = "My Invited Event",
				Description = "My Invited Event Description",
				Duration = 2,
				IntensityLevel = 1,
				Sport = "Running",
				SubSport = "Trail",
				Location = new Location
				{
					Name = "Mendon Ponds Park",
					Coordinates = MENDON_PARK_LOCATION
				},
				EventSchedule = new EventSchedule
				{
					StartTime = DateTime.UtcNow.AddDays(2.0),
					Recurrence = EventRecurrence.Never
				},
				Visibility = "public",
				Participants = new List<IFitEventParticipant> {
					new FitEventParticipant {
						UserId = SessionHelper.User.Id,
						Invited = true
					}
				}
			};
			var invitedEvent = await CreateTrackedFitEvent(fitEvent1);

			// ----------------------------------
			// Act
			// ----------------------------------
			var myInvitedEvents = await _fitEventService.MyInvitedEvents(0, 100);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.GreaterOrEqual(myInvitedEvents.Count, 1);
			Assert.IsTrue(myInvitedEvents.Any(e => e.Id == invitedEvent.Id));
		}

		#endregion

		#region My Created Events Tests

		[Test]
		public async Task TestMyCreatedEventsSuccess()
		{
			// ----------------------------------
			// Arrange
			// ----------------------------------
			var fitEvent1 = new FitEvent {
				Name = "My Invited Event",
				Description = "My Invited Event Description",
				Duration = 2,
				IntensityLevel = 1,
				Sport = "Running",
				SubSport = "Trail",
				Location = new Location
				{
					Name = "Mendon Ponds Park",
					Coordinates = MENDON_PARK_LOCATION
				},
				EventSchedule = new EventSchedule
				{
					StartTime = DateTime.UtcNow.AddDays(2.0),
					Recurrence = EventRecurrence.Never
				},
				Visibility = "public",
				Participants = new List<IFitEventParticipant> {
					new FitEventParticipant {
						UserId = SessionHelper.User.Id,
						Invited = true
					}
				}
			};
			var invitedEvent = await CreateTrackedFitEvent(fitEvent1);

			// ----------------------------------
			// Act
			// ----------------------------------
			var myCreatedEvents = await _fitEventService.MyCreatedEvents(0, 100);

			// ----------------------------------
			// Assert
			// ----------------------------------
			Assert.GreaterOrEqual(myCreatedEvents.Count, 1);
			Assert.IsTrue(myCreatedEvents.Any(e => e.Id == invitedEvent.Id));
		}

		#endregion

		#region Private Methods

		private async Task<IFitEvent> CreateTrackedFitEvent(FitEvent fitEvent)
		{
			var trackedEvent = await _fitEventService.CreateEvent(fitEvent);
			_createdEvents.Add(trackedEvent);

			return trackedEvent;
		}

		#endregion
    }
}

