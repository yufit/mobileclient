﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using YuFit.WebServices.Extensions;

namespace YuFit.WebServices.Helper
{
    public class UriPathBuilder
    {
		private readonly IList<string> _uriPathSegments;
		private readonly IDictionary _uriQueryParameters;

		public UriPathBuilder()
		{
			_uriPathSegments = new List<string>();
			_uriQueryParameters = new Dictionary<string, string>();
		}

		public UriPathBuilder AddSegment(string segmentValue)
		{
			_uriPathSegments.Add(segmentValue);

			return this;
		}

		public UriPathBuilder AddQueryParameter(string parameterName, string value)
		{
			_uriQueryParameters.Add(parameterName, value);

			return this;
		}

		public Uri Build()
		{
			var uriBuildResult = new StringBuilder();

			foreach (var uriSegment in _uriPathSegments)
			{
				uriBuildResult.Append(uriSegment.AppendSlash());
			}

			string uriPath = uriBuildResult.ToString().RemoveTrailingSlash();
			uriBuildResult.Clear();

			if (_uriQueryParameters.Count > 0)
			{
				int p = 0;
				foreach (var queryParam in _uriQueryParameters.Keys)
				{
					uriBuildResult.Append(String.Format("{0}{1}={2}", p == 0 ? "?" : "&",
						queryParam, 
						_uriQueryParameters[queryParam], p++));
				}
			}

			return new Uri(uriPath + uriBuildResult, UriKind.RelativeOrAbsolute);
		}
    }
}

