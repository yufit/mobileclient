﻿using System;

namespace YuFit.WebServices.Helper
{
	/// <summary>
	/// Utility class for validating method arguments.
	/// </summary>
    public static class ArgumentValidator
    {
		/// <summary>
		/// Validates the argument is not null.
		/// </summary>
		/// <param name="argument">Argument.</param>
		/// <param name="argumentName">Argument name.</param>
		public static void ValidateArgumentNotNull(object argument, string argumentName)
		{
			if (argument == null)
			{
				throw new ArgumentNullException(argumentName);
			}
		}

		/// <summary>
		/// Validates the argument is not null or empty.
		/// Empty validation works with String and Guid.
		/// </summary>
		/// <param name="argument">Argument.</param>
		/// <param name="argumentName">Argument name.</param>
		public static void ValidateArgumentNotNullOrEmpty(object argument, string argumentName)
		{
			if (argument == null)
			{
				throw new ArgumentNullException(argumentName);
			}

			string s = argument as String;
			if (s == String.Empty)
			{
				throw new ArgumentException("String cannot be empty.", argumentName);
			}

			Guid? g = argument as Guid?;
			if (g == Guid.Empty)
			{
				throw new ArgumentException("Guid cannot be empty.", argumentName);
			}
		}

		/// <summary>
		/// Validates the argument is null or empty.
		/// </summary>
		/// <param name="argument">Argument.</param>
		/// <param name="argumentName">Argument name.</param>
		public static void ValidateArgumentIsNullOrEmpty(object argument, string argumentName)
		{
			if (argument != null)
			{
				throw new ArgumentException("Argument must be null or empty.", argumentName);
			}

			string s = argument as String;
			if (s != null && s != String.Empty)
			{
				throw new ArgumentException("String must be empty.", argumentName);
			}

			Guid? g = argument as Guid?;
			if (g != null && g != Guid.Empty)
			{
				throw new ArgumentException("Guid must be empty.", argumentName);
			}
		}
    }
}

