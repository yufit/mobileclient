﻿using System;
using System.Threading.Tasks;
using YuFit.WebServices.Helper;
using YuFit.WebServices.Interface;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;
using YuFit.WebServices.WebService;

namespace YuFit.WebServices
{
    /// <summary>
    /// Yu fit client.
    /// </summary>
    /// ----------------------------------------------------------------------------------------
    public class YuFitClient : IYuFitClient
    {
        #region Data members

        private readonly string                         _clientVersion;
        private readonly Uri                            _serviceEndpoint;
        private readonly ISessionStore                  _sessionStore;
        private readonly ILogger                        _logger;
        private readonly AuthenticationWebService       _authenticationService;
        private readonly SessionManager                 _sessionManager;

        private static IYuFitConfig                     _yuFitConfig;

        #endregion

        #region IYuFitClient Properties Realisation

        public IYuFitSession Session { get { return _sessionManager.Session; } }

        private readonly Lazy<IFitEventWebService> _lazyFitEventWebService;
        public IFitEventWebService FitEventService { get { return _lazyFitEventWebService.Value; } }

        private readonly Lazy<IFitEventCommentWebService> _lazyFitEventCommentWebService;
        public IFitEventCommentWebService FitEventCommentService { get { return _lazyFitEventCommentWebService.Value; } }

        private readonly Lazy<INotificationWebService> _lazyNotificationWebService;
        public INotificationWebService NotificationService { get { return _lazyNotificationWebService.Value; } }

        private readonly Lazy<IUserWebService> _lazyUserWebService;
        public IUserWebService UserService { get { return _lazyUserWebService.Value; } }

        private readonly Lazy<IUserGroupWebService> _lazyUserGroupService;
        public IUserGroupWebService UserGroupService { get { return _lazyUserGroupService.Value; } }

        private readonly Lazy<IUserGroupMemberWebService> _lazyUserGroupMemberWebService;
        public IUserGroupMemberWebService UserGroupMemberService { get { return _lazyUserGroupMemberWebService.Value; } }

        private readonly Lazy<IDeviceWebService> _lazyDeviceWebService;
        public IDeviceWebService DeviceWebService { get { return _lazyDeviceWebService.Value; } }

        private readonly Lazy<IFriendsWebService> _lazyFriendsWebService;
        public IFriendsWebService FriendsWebService { get { return _lazyFriendsWebService.Value; } }

        #endregion

        #region Constructor

        public YuFitClient(IYuFitConfig config)
        {
            ArgumentValidator.ValidateArgumentNotNull(config, "config");

            // Initialize from configuration
            _clientVersion      = config.ClientVersionNumber;
            _serviceEndpoint    = config.ServiceEndpoint;
            _sessionStore       = config.SessionStore;
            _logger             = config.Logger;
            _yuFitConfig        = config;

            // Initialize SessionManager
            _authenticationService = new AuthenticationWebService(_serviceEndpoint);
            _sessionManager = new SessionManager(_authenticationService, _sessionStore);

            // Initialize the lazy loaded services
            _lazyFitEventWebService 		= new Lazy<IFitEventWebService>			(() => new FitEventWebService		    (_sessionManager, _serviceEndpoint) { Logger = _logger, ClientVersion = _clientVersion });
            _lazyFitEventCommentWebService 	= new Lazy<IFitEventCommentWebService>	(() => new FitEventCommentWebService    (_sessionManager, _serviceEndpoint) { Logger = _logger, ClientVersion = _clientVersion });
            _lazyNotificationWebService 	= new Lazy<INotificationWebService>		(() => new NotificationWebService	    (_sessionManager, _serviceEndpoint) { Logger = _logger, ClientVersion = _clientVersion });
            _lazyUserWebService             = new Lazy<IUserWebService>             (() => new UserWebService               (_sessionManager, _serviceEndpoint) { Logger = _logger, ClientVersion = _clientVersion });
            _lazyDeviceWebService           = new Lazy<IDeviceWebService>           (() => new DeviceWebService             (_sessionManager, _serviceEndpoint) { Logger = _logger, ClientVersion = _clientVersion });
            _lazyUserGroupService           = new Lazy<IUserGroupWebService>        (() => new UserGroupWebService          (_sessionManager, _serviceEndpoint) { Logger = _logger, ClientVersion = _clientVersion });
            _lazyUserGroupMemberWebService  = new Lazy<IUserGroupMemberWebService>  (() => new UserGroupMemberWebService    (_sessionManager, _serviceEndpoint) { Logger = _logger, ClientVersion = _clientVersion });
            _lazyFriendsWebService          = new Lazy<IFriendsWebService>          (() => new FriendsWebService            (_sessionManager, _serviceEndpoint) { Logger = _logger, ClientVersion = _clientVersion });
        }

        #endregion

        #region Public Methods

        public async Task<IYuFitSession> OpenSession(IAuthenticationCredentials credentials)
        {
            return await _sessionManager.OpenSession(credentials);
        }

        public void CloseActiveSession()
        {
            _sessionManager.CloseSession();
        }

        public static object Create(Type objectType)
        {
            if (_yuFitConfig != null)
            {
                return _yuFitConfig.Create(objectType);
            }

            return null;
        }

        #endregion
    }
}

