﻿using System;
using YuFit.WebServices.Interface.Model;

namespace YuFit.WebServices.Model
{
    public class AuthenticationCredentials : IAuthenticationCredentials
	{
		#region IAuthenticationCredentials implementation

		public ServiceType  ServiceType { get; set; }
		public string       AccessToken { get; set; }
		public string       SecretKey   { get; set; }
        public string       Username    { get; set; }
        public string       Name        { get; set; }
        public string       Email       { get; set; }
		public string       Password    { get; set; }

		#endregion

		AuthenticationCredentials()
		{
		}

		public static IAuthenticationCredentials CreateWithToken(ServiceType serviceType, string accessToken, string secretKey = null)
		{
			return new AuthenticationCredentials {
				ServiceType = serviceType,
				AccessToken = accessToken,
				SecretKey = secretKey
			};
		}

		public static IAuthenticationCredentials CreateWithCreds(ServiceType serviceType, string username, string password)
		{
			return new AuthenticationCredentials {
				ServiceType = serviceType,
                Email = username,
				Password = password
			};
		}
        public static IAuthenticationCredentials CreateWithCreds(ServiceType serviceType, string name, string username, string password)
        {
            return new AuthenticationCredentials {
                ServiceType = serviceType,
                Name = name,
                Email = username,
                Password = password
            };
        }
    }
}

