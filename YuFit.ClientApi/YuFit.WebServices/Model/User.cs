﻿using System;
using Newtonsoft.Json;
using YuFit.WebServices.Interface.Model;
using System.Collections.Generic;

namespace YuFit.WebServices.Model
{
	public class User : IUser
	{
		#region IUser Implementation

		public string Id { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public IUserProfile UserProfile { get; set; }
		public IList<IDeviceInformation> Devices { get; set; }
		public IList<IServiceIdentity> ServiceIdentities { get; set; }
		public IList<ILocation> Locations {get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime UpdatedAt { get; set; }
		public IEventFilter EventFilter { get; set; }
		public int? CreatedEventsCount { get; set; }
		public IList<IFitEvent> ShortUpcomingEventList { get; set; }
        public bool IsFriend { get; set; }

		#endregion

		[JsonProperty("yufit_token")] public Token    Token { get; set; }
	}
}

