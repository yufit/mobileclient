﻿using System;
using Newtonsoft.Json;

namespace YuFit.WebServices.Model
{
	public class Token
	{
		[JsonProperty("api_token")] public string ApiToken { get; set; }
		[JsonProperty("expires_at")] public DateTime ExpiresAt { get; set; }

		[JsonIgnore] public bool IsValid { get { return !String.IsNullOrEmpty (ApiToken); } }
		[JsonIgnore] public bool IsExpired { get { return (ExpiresAt <= DateTime.UtcNow); } }
		[JsonIgnore] public bool IsNotExpired { get { return (ExpiresAt > DateTime.UtcNow); } }
	}
}

