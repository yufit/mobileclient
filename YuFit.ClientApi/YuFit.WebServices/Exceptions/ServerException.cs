﻿using System;
using System.Net;

namespace YuFit.WebServices.Exceptions
{
    public class ServerException : Exception
    {
        public HttpStatusCode StatusCode { get; private set; }

        public ServerException (string message, HttpStatusCode statusCode, Exception innerException = null)
            : base(message, innerException)
        {
            StatusCode = statusCode;
        }
    }
}

