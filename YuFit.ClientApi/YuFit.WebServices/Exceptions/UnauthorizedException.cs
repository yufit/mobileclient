﻿using System;

namespace YuFit.WebServices.Exceptions
{
	/// <summary>
	/// Unauthorized exception.
	/// </summary>
	public class UnauthorizedException : Exception
	{
		public UnauthorizedException (Exception innerException = null) 
			: base ("User not authorized to perform the remote operation", innerException)
		{
		}
	}
}

