﻿using System;

namespace YuFit.WebServices.Exceptions
{
    public class NotFoundException : Exception
    {
		public NotFoundException(Exception innerException = null) 
			: base ("Remote resource not found.", innerException)
        {
        }
    }
}

