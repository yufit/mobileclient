﻿using System;

namespace YuFit.WebServices.Exceptions
{
	/// <summary>
	/// Connection timeout exception.
	/// </summary>
	public class ConnectionTimeoutException : Exception
	{
		public ConnectionTimeoutException (Exception innerException = null) 
			: base ("Connection Timed Out", innerException)
		{
		}
	}
}

