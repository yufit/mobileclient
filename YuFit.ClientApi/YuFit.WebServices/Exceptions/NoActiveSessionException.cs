﻿using System;

namespace YuFit.WebServices.Exceptions
{
	/// <summary>
	/// No active session exception.
	/// </summary>
	public class NoActiveSessionException : Exception
	{
		public NoActiveSessionException (Exception innerException = null) 
			: base ("There are no active session", innerException)
		{
		}
	}
}

