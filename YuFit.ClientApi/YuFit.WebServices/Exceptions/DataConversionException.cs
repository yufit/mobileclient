﻿using System;

namespace YuFit.WebServices.Exceptions
{
	/// <summary>
	/// Data conversion exception.
	/// </summary>
	public class DataConversionException : Exception
	{
		public DataConversionException (Exception innerException = null) 
			: base ("Json conversion error", innerException)
		{
		}
	}
}

