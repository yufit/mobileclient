﻿using System;

namespace YuFit.WebServices.Extensions
{
    public static class StringExtensions
    {
		public static string AppendSlash(this string str)
		{
			return str + "/";
		}
			
		public static string RemoveTrailingSlash(this string str)
		{
			return str?.TrimEnd(new char[] { '/' });
		}

		public static string RemoveLeadingSlash(this string str)
		{
			return str?.TrimStart(new char[] { '/' });
		}
    }
}

