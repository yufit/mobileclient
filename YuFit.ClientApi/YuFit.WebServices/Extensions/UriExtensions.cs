﻿using System;
using System.Text;

namespace YuFit.WebServices.Extensions
{
    public static class UriExtensions
    {
		public static Uri Combine(this Uri uri, Uri path)
		{
			return (path == null) ? uri : new Uri(String.Format("{0}/{1}", uri.ToString().RemoveTrailingSlash(), path.ToString().RemoveLeadingSlash()));
		}
    }
}

