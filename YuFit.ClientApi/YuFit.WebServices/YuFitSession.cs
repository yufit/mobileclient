﻿using Newtonsoft.Json;
using YuFit.WebServices.Interface;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Model;


namespace YuFit.WebServices
{
    /// <summary>
    /// Yu fit session.
    /// </summary>
    /// ----------------------------------------------------------------------------------------
    public class YuFitSession : IYuFitSession
    {
        #region IYuFitSession implementation

        [JsonIgnore] public bool IsAuthenticated { get { return AuthenticationToken != null; } }
        [JsonIgnore] public bool IsValid { get { return (AuthenticationToken != null && AuthenticationToken.IsValid); } }
        [JsonIgnore] public bool IsExpired { get { return (AuthenticationToken != null && AuthenticationToken.IsExpired); } }
        [JsonIgnore] public bool NeedsRenewal { get { return IsValid && IsAuthenticated && IsExpired; } }
        [JsonIgnore] public bool NeedsAuthentication { get { return !IsValid || !IsAuthenticated; } }

        public IAuthenticationCredentials AuthenticationCredentials { get; set; }
        public IUser AuthenticatedUser { get; set; }
        public Token AuthenticationToken { get; set; }

        #endregion

        // TODO: Remove this after refactor
        public void Invalidate ()
        {
            AuthenticationToken = null;
            AuthenticatedUser = null;
        }

        #region Serialization

        public string Serialize ()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings {
                TypeNameHandling = TypeNameHandling.All
            });
        }

        public static YuFitSession Deserialize (string json)
        {
            return JsonConvert.DeserializeObject<YuFitSession>(json, new JsonSerializerSettings {
                TypeNameHandling = TypeNameHandling.All
            });
        }

        #endregion
    }
}

