﻿using System;
using Newtonsoft.Json.Converters;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Model;

namespace YuFit.WebServices.Converters
{
    public class UserConverter : CustomCreationConverter<IUser>
    {
        public override IUser Create(Type objectType)
        {
            return (IUser)YuFitClient.Create(objectType) ?? new User();
        }
    }
}
