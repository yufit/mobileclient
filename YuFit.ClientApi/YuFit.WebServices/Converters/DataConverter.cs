﻿using System;
using Newtonsoft.Json.Converters;

namespace YuFit.WebServices.Converters
{
    public class DataConverter<T> :  CustomCreationConverter<T>
    {
        public override T Create(Type objectType)
        {
            return (T) YuFitClient.Create(objectType);
        }
    }
}

