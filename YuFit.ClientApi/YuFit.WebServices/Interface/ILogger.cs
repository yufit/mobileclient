﻿using System;

namespace YuFit.WebServices.Interface
{
    public interface ILogger
    {
		void LogDiagnostic(string message);
    }
}

