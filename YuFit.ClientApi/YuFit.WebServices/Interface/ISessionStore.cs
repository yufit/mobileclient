﻿using System;

namespace YuFit.WebServices.Interface
{
	public interface ISessionStore
	{
		void StoreSessionInformation(string information);
		string LoadSessionInformation();
	}
}

