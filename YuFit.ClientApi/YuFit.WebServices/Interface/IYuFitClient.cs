﻿using System;
using YuFit.WebServices.Interface.WebService;
using YuFit.WebServices.Model;
using System.Threading.Tasks;
using YuFit.WebServices.Interface.Model;

namespace YuFit.WebServices.Interface
{
	public interface IYuFitClient
	{
		#region Session Management

		IYuFitSession       Session { get; }
		Task<IYuFitSession> OpenSession(IAuthenticationCredentials creds);
		void                CloseActiveSession();

		#endregion

		#region Services

		IFitEventWebService             FitEventService                 { get; }
		IFitEventCommentWebService      FitEventCommentService          { get; }
		INotificationWebService         NotificationService             { get; }
        IUserWebService                 UserService                     { get; }
        IDeviceWebService               DeviceWebService                { get; }
        IUserGroupWebService            UserGroupService                { get; }
        IUserGroupMemberWebService      UserGroupMemberService          { get; }
        IFriendsWebService              FriendsWebService               { get; }

		#endregion
	}
}

