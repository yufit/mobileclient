﻿using System;

namespace YuFit.WebServices.Interface
{
    public interface IYuFitConfig
    {
        string          ClientVersionNumber { get; }
        Uri             ServiceEndpoint     { get; }
        ISessionStore   SessionStore        { get; }
        ILogger         Logger              { get; }

		/// <summary>
		/// Factory method for creating concrete objects implementing the service interfaces
		/// </summary>
		/// <param name="objectType">Object type.</param>
		object Create(Type objectType);
    }
}

