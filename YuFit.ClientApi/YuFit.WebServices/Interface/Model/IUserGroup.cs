﻿using System;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

using YuFit.WebServices.Attributes;
using YuFit.WebServices.Converters;

namespace YuFit.WebServices.Interface.Model
{
    public enum GroupType { SportClub, SportTeam, SportOrganisation };

    [JsonConverter(typeof(DataConverter<IUserGroup>))]
    [JsonWrapperAttribute("user_group")]
    [JsonObject(MemberSerialization.OptIn)]
    public interface IUserGroup
    {
        [JsonProperty("user_group_id")]             string          Id                          { get; set; }
        [JsonProperty("name")]                      string          Name                        { get; set; }
        [JsonProperty("description")]               string          Description                 { get; set; }
        [JsonProperty("website")]                   Uri	            Website                     { get; set; }
        [JsonProperty("created_at")]                DateTime        CreatedAt                   { get; set; }
        [JsonProperty("updated_at")]                DateTime        UpdatedAt                   { get; set; }
        [JsonProperty("avatar_url")]                Uri             GroupAvatar                 { get; set; }
        [JsonProperty("group_photo_url")]           Uri             GroupPhoto                  { get; set; }
        [JsonProperty("created_by")]                IUser           CreatedBy                   { get; set; }
        [JsonProperty("is_private")]                Boolean?        Private                     { get; set; }
        [JsonProperty("location")]                  ILocation       Location                    { get; set; }
        [JsonProperty("is_current_user_manager")]   Boolean?        IsCurrentUserManager        { get; set; }
        [JsonProperty("member_count")]              int             MemberCount                 { get; set; }
        [JsonProperty("is_current_user_member")]    Boolean?        IsCurrentUserAMember        { get; set; }
        [JsonProperty("is_current_user_pending")]   Boolean?        IsCurrentUserPending        { get; set; }
        [JsonProperty("current_user_member_id")]    string          CurrentMemberUserMemberId   { get; set; }

//        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("type")]                      string          Type                        { get; set; }

//        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("sport")]                     string          SportKind                   { get; set; }

        [JsonProperty("is_plus")]                   Boolean?        Plus                        { get; set; }
        [JsonProperty("plus_subscription_level")]   string          PlusSubscriptionLevel       { get; set; }
        [JsonProperty("plus_expires_at")]           DateTime        PlusExpiresAt               { get; set; }

    }
}

