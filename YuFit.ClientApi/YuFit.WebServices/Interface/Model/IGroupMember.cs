﻿using System;
using Newtonsoft.Json;
using YuFit.WebServices.Converters;

namespace YuFit.WebServices.Interface.Model
{
    [JsonConverter(typeof(DataConverter<IGroupMember>))]
    [JsonObject(MemberSerialization.OptIn)]
    public interface IGroupMember
    {
        [JsonProperty("user_group_member_id")]      string                  Id { get; set; }
        [JsonProperty("user")]                      IUser                   User { get; set; }
        [JsonProperty("is_manager")]                Boolean?                IsManager { get; set; }
    }
}

