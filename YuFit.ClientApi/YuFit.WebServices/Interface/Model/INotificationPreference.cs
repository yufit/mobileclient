﻿
using System;
using Newtonsoft.Json;
using YuFit.WebServices.Converters;

namespace YuFit.WebServices.Interface.Model
{
    [JsonConverter(typeof(DataConverter<INotificationPreference>))]
    [JsonObject(MemberSerialization.OptIn)]
    public interface INotificationPreference
    {
        [JsonProperty("category_name")]     string  CategoryName     { get; set; }
        [JsonProperty("disabled")]          bool    IsDisabled       { get; set; }
    }
}

