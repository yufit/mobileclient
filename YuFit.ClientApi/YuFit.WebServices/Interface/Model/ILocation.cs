﻿using Newtonsoft.Json;
using YuFit.WebServices.Converters;

namespace YuFit.WebServices.Interface.Model
{
    [JsonConverter(typeof(DataConverter<ILocation>))]
	[JsonObject(MemberSerialization.OptIn)]
    public interface ILocation
    {
		[JsonProperty("name")] 				string 	 Name { get; set; }
		[JsonProperty("street_address")]	string 	 StreetAddress { get; set; }
		[JsonProperty("city")] 				string 	 City { get; set; }
		[JsonProperty("state_prov")] 		string 	 StateProvince { get; set; }
		[JsonProperty("country")] 			string 	 Country { get; set; }
		[JsonProperty("coordinates")] 		double[] Coordinates { get; set; }
    }
}

