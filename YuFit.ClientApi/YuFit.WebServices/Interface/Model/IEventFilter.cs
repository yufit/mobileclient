﻿using System.Collections.Generic;
using Newtonsoft.Json;
using YuFit.WebServices.Converters;

namespace YuFit.WebServices.Interface.Model
{
    [JsonConverter(typeof(DataConverter<IEventFilter>))]
	[JsonObject(MemberSerialization.OptIn)]
	public interface IEventFilter
	{
		[JsonProperty("radius")]			int 					Radius { get; set; }
		[JsonProperty("within_days")]		int 					WithinDays { get; set; }
		[JsonProperty("sport_filters")]		IList<ISportFilter> 	SportFilters { get; set; }
	}
}

