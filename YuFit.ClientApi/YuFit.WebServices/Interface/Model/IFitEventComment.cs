﻿using System;
using Newtonsoft.Json;

using YuFit.WebServices.Attributes;
using YuFit.WebServices.Converters;

namespace YuFit.WebServices.Interface.Model
{
    [JsonConverter(typeof(DataConverter<IFitEventComment>))]
	[JsonWrapperAttribute("event_comment")]
	[JsonObject(MemberSerialization.OptIn)]
	public interface IFitEventComment
	{
		[JsonProperty("event_comment_id")] 		string			Id 			{ get; set; }
		[JsonProperty("comment")] 				string			Comment 	{ get; set; }
		[JsonProperty("updated_at")] 			DateTime		UpdatedAt 	{ get; set; }
		[JsonProperty("user_id")] 				string			UserId 		{ get; set; }
		[JsonProperty("user_name")] 			string			UserName 	{ get; set; }
		[JsonProperty("user_avatar")] 			Uri				UserAvatar	{ get; set; }
	}
}

