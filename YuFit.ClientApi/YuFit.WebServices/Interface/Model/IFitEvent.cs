﻿using System;
using Newtonsoft.Json;
using YuFit.WebServices.Converters;
using YuFit.WebServices.Attributes;
using System.Collections.Generic;

namespace YuFit.WebServices.Interface.Model
{
    [JsonConverter(typeof(DataConverter<IFitEvent>))]
	[JsonWrapperAttribute("event")]
	[JsonObject(MemberSerialization.OptIn)]
    public interface IFitEvent
    {
        [JsonProperty("event_id"                )]  string                      Id                  { get; set; }
        [JsonProperty("name"                    )]  string                      Name                { get; set; }
        [JsonProperty("description"             )]  string                      Description         { get; set; }
        [JsonProperty("event_schedule"          )]  IEventSchedule              EventSchedule       { get; set; }
        [JsonProperty("duration"                )]  int?                        Duration            { get; set; }
        [JsonProperty("distance"                )]  int?                        Distance            { get; set; }
        [JsonProperty("intensity_level"         )]  int?                        IntensityLevel      { get; set; }
        [JsonProperty("sport"                   )]  string                      Sport               { get; set; }
        [JsonProperty("sub_sport"               )]  string                      SubSport            { get; set; }
        [JsonProperty("visibility"              )]  string                      Visibility          { get; set; }
        [JsonProperty("location"                )] 	ILocation                   Location            { get; set; }
        [JsonProperty("event_participants"      )]  IList<IFitEventParticipant> Participants        { get; set; }
        [JsonProperty("created_by"              )]  IUser                       CreatedBy           { get; set; }
        [JsonProperty("created_at"              )]  DateTime                    CreatedAt           { get; set; }
        [JsonProperty("updated_at"              )]  DateTime                    LastUpdate          { get; set; }
        [JsonProperty("canceled"                )]  Boolean?                    IsCanceled          { get; set; }
        [JsonProperty("is_private"              )]  Boolean?                    IsPrivate           { get; set; }
        [JsonProperty("user_group_id"           )]  string                      UserGroupId         { get; set; }
        [JsonProperty("user_group_name"         )]  string                      UserGroupName       { get; set; }
        [JsonProperty("user_group_avatar_url"   )]  string                      UserGroupAvatarUrl  { get; set; }


        [JsonProperty("promotion_starts"        )]  DateTime?                   PromotionStartingOn { get; set; }
        [JsonProperty("is_promoted"             )]  Boolean?                    IsPromoted          { get; set; }
        [JsonProperty("icon_url"                )]  string                      IconUrl             { get; set; }
        [JsonProperty("event_photo_url"         )]  string                      EventPhotoUrl       { get; set; }
        [JsonProperty("facebook_url"            )]  string                      FacebookUrl         { get; set; }
        [JsonProperty("website_url"             )]  string                      WebsiteUrl          { get; set; }
        [JsonProperty("num_views"               )]  int                         NumViews            { get; set; }
    }
}

