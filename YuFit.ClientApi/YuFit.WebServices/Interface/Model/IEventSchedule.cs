﻿using System;
using Newtonsoft.Json;
using YuFit.WebServices.Converters;
using Newtonsoft.Json.Converters;

namespace YuFit.WebServices.Interface.Model
{
	public enum EventRecurrence
	{
		Never,
		Daily,
		Weekly,
		Monthly,
		Yearly
	}

    [JsonConverter(typeof(DataConverter<IEventSchedule>))]
	[JsonObject(MemberSerialization.OptIn)]
    public interface IEventSchedule
    {
		[JsonProperty("start_time")] 		DateTime		StartTime { get; set; }
		[JsonConverter(typeof(StringEnumConverter))]
		[JsonProperty("recurrence")] 		EventRecurrence	Recurrence { get; set; }
		[JsonProperty("interval")] 			int? 		 	Interval { get; set; }
		[JsonProperty("next_occurence")] 	DateTime? 		NextOccurence { get; set; }
		[JsonProperty("how_many")] 			int? 		 	NumberOfOccurences { get; set; }
		[JsonProperty("expires_at")] 		DateTime? 		ExpiresAt { get; set; }
    }
}

