﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using YuFit.WebServices.Attributes;
using YuFit.WebServices.Converters;


namespace YuFit.WebServices.Interface.Model
{
    public enum DeviceOs
    {
        IOS,
        DROID
    }

    [JsonConverter(typeof(DataConverter<IDeviceInformation>))]
    [JsonWrapperAttribute("device")]
    [JsonObject(MemberSerialization.OptIn)]
    public interface IDeviceInformation
    {
        [JsonProperty("device_id")]
        string DeviceId { get; set; }

        [JsonProperty("notification_token")]
        string NotificationToken { get; set; }

        [JsonProperty("device_os")]
        [JsonConverter(typeof(StringEnumConverter))]
        DeviceOs DeviceOs { get; set; }
    }
}

