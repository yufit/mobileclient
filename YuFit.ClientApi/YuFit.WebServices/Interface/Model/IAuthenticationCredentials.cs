﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace YuFit.WebServices.Interface.Model
{
    public enum ServiceType
    {
        Facebook,
        Twitter,
        Yufit
    }

    public interface IAuthenticationCredentials
    {
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("service_type")]  ServiceType ServiceType { get; set; }
        [JsonProperty("access_token")] 	string      AccessToken { get; set; }
        [JsonProperty("secret_key")]    string      SecretKey   { get; set; }
        [JsonProperty("username")]      string      Username    { get; set; }
        [JsonProperty("name")]          string      Name        { get; set; }
        [JsonProperty("password")]      string      Password    { get; set; }
        [JsonProperty("email")]         string      Email       { get; set; }
    }
}

