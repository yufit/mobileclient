﻿using System;

namespace YuFit.WebServices.Interface.Model
{
    public interface ICoordinate
    {
		double Latitude     { get; set; }
		double Longitude    { get; set; }
    }
}

