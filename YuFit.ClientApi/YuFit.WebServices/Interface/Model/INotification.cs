﻿using System;
using Newtonsoft.Json;
using YuFit.WebServices.Converters;

namespace YuFit.WebServices.Interface.Model
{
    [JsonConverter(typeof(DataConverter<INotification>))]
	[JsonObject(MemberSerialization.OptIn)]
    public interface INotification
    {
		[JsonProperty("notification_id")] 	string		Id { get; set; }
		[JsonProperty("data")] 				dynamic		Data { get; set; }
		[JsonProperty("is_acked")] 			bool		IsAcked { get; set; }
		[JsonProperty("created_at")] 		DateTime	CreatedAt { get; set; }
    }
}

