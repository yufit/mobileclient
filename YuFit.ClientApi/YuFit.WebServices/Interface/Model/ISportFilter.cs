﻿using System.Collections.Generic;
using Newtonsoft.Json;
using YuFit.WebServices.Converters;

namespace YuFit.WebServices.Interface.Model
{
    [JsonConverter(typeof(DataConverter<ISportFilter>))]
	[JsonObject(MemberSerialization.OptIn)]
	public interface ISportFilter
	{
		[JsonProperty("is_excluded")]		bool? 			IsExcluded { get; set; }
		[JsonProperty("sport")]				string 			Sport { get; set; }
		[JsonProperty("sub_sports")]		IList<string> 	SubSports { get; set; }
	}
}

