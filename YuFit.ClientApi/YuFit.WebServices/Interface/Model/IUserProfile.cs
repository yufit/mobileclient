﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using YuFit.WebServices.Converters;
using Newtonsoft.Json.Converters;

namespace YuFit.WebServices.Interface.Model
{
    public enum Gender
    {
        Male,
        Female
    }

    [JsonConverter(typeof(DataConverter<IUserProfile>))]
    [JsonObject(MemberSerialization.OptIn)]
    public interface IUserProfile
    {
        [JsonProperty("radius")]                    int?                            Radius                  { get; set; }
        [JsonProperty("kilometers")]                bool?                           Kilometers              { get; set; }
        [JsonProperty("favorite_activities")]       IList<string>                   FavoriteActivities      { get; set; }
        [JsonProperty("birthday")]                  DateTime?                       Birthday                { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("gender")]                    Gender?    	                    Gender                  { get; set; }
        [JsonProperty("avatar_url")]                Uri                             AvatarUri               { get; set; }
        [JsonProperty("interests")]	                string                          Interests               { get; set; }
        [JsonProperty("bio")]                       string                          Bio                     { get; set; }
        [JsonProperty("notification_preferences")]  IList<INotificationPreference>  NotificationPreferences { get; set; }
    }
}

