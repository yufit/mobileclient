﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using YuFit.WebServices.Converters;

namespace YuFit.WebServices.Interface.Model
{
    [JsonConverter(typeof(DataConverter<IServiceIdentity>))]
	[JsonObject(MemberSerialization.OptIn)]
	public interface IServiceIdentity
    {
		[JsonConverter(typeof(StringEnumConverter))]
		[JsonProperty("service_type")]		ServiceType ServiceType { get; set; }
		[JsonProperty("is_login_identity")]	bool IsLoginIdentity { get; set; }
		[JsonProperty("user_id")]			string UserId { get; set; }
		[JsonProperty("name")]				string Name { get; set; }
		[JsonProperty("email")]				string Email { get; set; }
	}
}

