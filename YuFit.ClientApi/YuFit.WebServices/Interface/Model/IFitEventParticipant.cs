﻿using Newtonsoft.Json;
using YuFit.WebServices.Converters;
using System.Collections.Generic;

namespace YuFit.WebServices.Interface.Model
{
    [JsonConverter(typeof(DataConverter<IFitEventParticipant>))]
    public interface IFitEventParticipant
    {
		[JsonProperty("user_id")]	   string UserId { get; set; }
		[JsonProperty("name")]		   string Name { get; set; }
		[JsonProperty("user_profile")] IUserProfile UserProfile { get; set; }
		[JsonProperty("locations")]	   IList<ILocation> Locations {get; set; }
		[JsonProperty("invited")]	   bool?  Invited { get; set; }
		[JsonProperty("going")]		   bool?  Going { get; set; }
    }
}

