﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

using YuFit.WebServices.Attributes;
using YuFit.WebServices.Converters;

namespace YuFit.WebServices.Interface.Model
{
    [JsonConverter(typeof(UserConverter))]
    [JsonWrapperAttribute("user")]
    [JsonObject(MemberSerialization.OptIn)]
    public interface IUser
    {
        [JsonProperty("user_id")]               string                      Id                      { get; set; }
        [JsonProperty("name")]                  string                      Name                    { get; set; }
        [JsonProperty("email")]                 string                      Email                   { get; set; }
        [JsonProperty("user_profile")]          IUserProfile                UserProfile             { get; set; }
        [JsonProperty("devices")]               IList<IDeviceInformation>   Devices                 { get; set; }
        [JsonProperty("service_identities")]    IList<IServiceIdentity>     ServiceIdentities       { get; set; }
        [JsonProperty("locations")]             IList<ILocation>            Locations               { get; set; }
        [JsonProperty("created_at")]            DateTime                    CreatedAt               { get; set; }
        [JsonProperty("updated_at")]            DateTime                    UpdatedAt               { get; set; }
        [JsonProperty("event_filter")]          IEventFilter                EventFilter             { get; set; }
        [JsonProperty("created_events_count")]  int?                        CreatedEventsCount      { get; set; }
        [JsonProperty("upcoming_events")]       IList<IFitEvent>            ShortUpcomingEventList  { get; set; }
        [JsonProperty("is_friend")]             bool                        IsFriend                { get; set; }
    }
}

