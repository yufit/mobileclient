﻿using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Model;

namespace YuFit.WebServices.Interface
{
	public interface IYuFitSession
	{
		bool IsAuthenticated { get; }
		bool IsValid { get; }
		bool IsExpired { get; }
		bool NeedsRenewal { get; }
		bool NeedsAuthentication { get; }

		IAuthenticationCredentials AuthenticationCredentials { get; }
		Token AuthenticationToken { get; }
		IUser AuthenticatedUser { get; }
	}
}

