﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using YuFit.WebServices.Interface.Model;
using System.Threading;

namespace YuFit.WebServices.Interface.WebService
{
    public interface IFriendsWebService
    {
        Task<IList<IUserGroup>>     GetGroupsOf         (string userId, int pageNum, int pageSize, CancellationToken token = default(CancellationToken));
        Task<IList<IUser>>          GetFriendsOf        (string userId, int pageNum, int pageSize, CancellationToken token = default(CancellationToken));
        Task<IList<IUser>>          MyFriends           (int pageNum, int pageSize, CancellationToken token = default(CancellationToken));
        Task<bool>                  AddFriend           (string friendId, CancellationToken token = default(CancellationToken));
        Task<bool>                  UnFriend            (string friendId, CancellationToken token = default(CancellationToken));
    }
}

