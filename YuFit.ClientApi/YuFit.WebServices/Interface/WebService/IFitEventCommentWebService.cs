﻿using System.Collections.Generic;
using System.Threading.Tasks;

using YuFit.WebServices.Interface.Model;
using System.Threading;

namespace YuFit.WebServices.Interface.WebService
{
	public interface IFitEventCommentWebService
	{
        Task<IList<IFitEventComment>>   GetComments     (IFitEvent fitEvent, int pageNum, int pageSize, CancellationToken token = default(CancellationToken));
        Task<IFitEventComment>          AddComment      (IFitEvent fitEvent, IFitEventComment fitEventComment, CancellationToken token = default(CancellationToken));
        Task<IFitEventComment>          UpdateComments  (IFitEvent fitEvent, IFitEventComment fitEventComment, CancellationToken token = default(CancellationToken));
        Task<bool>                      DeleteComment   (IFitEvent fitEvent, IFitEventComment fitEventComment, CancellationToken token = default(CancellationToken));
	}
}

