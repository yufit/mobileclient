﻿using System.Threading.Tasks;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Model;

namespace YuFit.WebServices.Interface.WebService
{
	public interface IAuthenticationWebService
	{
		Task<User> AuthenticateUser(IAuthenticationCredentials creds);
	}
}

