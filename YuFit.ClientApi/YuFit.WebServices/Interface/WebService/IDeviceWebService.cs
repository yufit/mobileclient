﻿using System.Threading.Tasks;

using YuFit.WebServices.Interface.Model;

namespace YuFit.WebServices.Interface.WebService
{
    public interface IDeviceWebService
    {
        Task<IDeviceInformation>    AddNewDevice    (string userId, IDeviceInformation deviceInformation);
        Task<IDeviceInformation>    UpdateDevice    (string userId, IDeviceInformation deviceInformation);
        Task<bool>                  RemoveDevice    (string userId, string serverDeviceId);
    }
}

