﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using YuFit.WebServices.Interface.Model;
using System.Threading;

namespace YuFit.WebServices.Interface.WebService
{
    public interface IUserWebService
    {
        Task<IList<IUser>>  GetAllUsers             (CancellationToken token = default(CancellationToken));

        Task<IUser>         GetUserInformation      (string userId, CancellationToken token = default(CancellationToken));
        Task<IUser>         UpdateUserInformation   (IUser user, CancellationToken token = default(CancellationToken));
        Task<IUser>         SetEventFilter          (string userId, IEventFilter filter, CancellationToken token = default(CancellationToken));
        Task<IUser>         UpdateAvatarImage       (string userId, string contentType, Stream image, CancellationToken token = default(CancellationToken));

        Task<IList<IUser>>  SearchUser              (string query, int pageNum, int pageSize, CancellationToken token = default(CancellationToken));
    }
}

