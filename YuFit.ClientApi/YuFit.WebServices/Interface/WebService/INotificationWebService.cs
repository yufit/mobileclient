﻿using System.Threading.Tasks;
using System.Collections.Generic;
using YuFit.WebServices.Interface.Model;
using System.Threading;

namespace YuFit.WebServices.Interface.WebService
{
    public interface INotificationWebService
    {
        Task<IList<INotification>>  GetNotificationsForUser     (string userId, int pageNum, int pageSize, CancellationToken token = default(CancellationToken));
        Task<bool>                  AckNotificationForUser      (string userId, string notificationId, CancellationToken token = default(CancellationToken));
        Task<bool>                  AckAllNotificationsForUser  (string userId, CancellationToken token = default(CancellationToken));
    }
}

