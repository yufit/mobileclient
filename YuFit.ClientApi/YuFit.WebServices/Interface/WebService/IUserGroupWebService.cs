﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using YuFit.WebServices.Interface.Model;
using System.Threading;

namespace YuFit.WebServices.Interface.WebService
{
    public interface IUserGroupWebService
    {
        Task<IList<IUserGroup>>     GetGroups           (int pageNum, int pageSize, CancellationToken token = default(CancellationToken));
        Task<IList<IUserGroup>>     GetMyGroups         (int pageNum, int pageSize, CancellationToken token = default(CancellationToken));
        Task<IUserGroup>            GetGroup            (string groupId, CancellationToken token = default(CancellationToken));

        Task<IUserGroup>            CreateGroup	        (IUserGroup group, CancellationToken token = default(CancellationToken));
        Task<IUserGroup>            UpdateGroup	        (IUserGroup group, CancellationToken token = default(CancellationToken));
        Task<bool> 	                DeleteGroup	        (string groupId, CancellationToken token = default(CancellationToken));

        Task<IUserGroup>            UpdateGroupImage    (IUserGroup group, string contentType, Stream image, CancellationToken token = default(CancellationToken));
        Task<IUserGroup>            UpdateAvatarImage   (IUserGroup group, string contentType, Stream image, CancellationToken token = default(CancellationToken));

        Task<IList<IUserGroup>>     SearchGroup         (string query, int pageNum, int pageSize, CancellationToken token = default(CancellationToken));

        Task                        JoinGroup           (string groupId, CancellationToken token = default(CancellationToken));
        Task                        LeaveGroup          (string groupId, string memberId, CancellationToken token = default(CancellationToken));

        Task<IUserGroup>            InviteUserToGroup   (string groupId, IList<string> usersId, CancellationToken token = default(CancellationToken));
        Task                        RemoveUserFromGroup (string groupId, string memberId, CancellationToken token = default(CancellationToken));

        Task<IList<IFitEvent>>      UpcomingEvents      (string groupId, int pageNum, int pageSize, CancellationToken token = default(CancellationToken));

    }
}

