﻿using System.Threading.Tasks;
using System.Collections.Generic;
using YuFit.WebServices.Interface.Model;
using System.Threading;

namespace YuFit.WebServices.Interface.WebService
{
	public interface IFitEventWebService
	{
        Task<IList<IFitEvent>>  GetAllEvents        (int pageNum, int pageSize, CancellationToken token = default(CancellationToken));
        Task<IFitEvent>         GetEvent            (string eventId, CancellationToken token = default(CancellationToken));
        Task<IFitEvent>         CreateEvent         (IFitEvent fitEvent, CancellationToken token = default(CancellationToken));
        Task<IFitEvent>         UpdateEvent         (IFitEvent fitEvent, CancellationToken token = default(CancellationToken));
        Task<bool>              DeleteEvent         (IFitEvent fitEvent, CancellationToken token = default(CancellationToken));

        Task<IList<IFitEvent>>  SearchForEvents     (int pageNum, int pageSize, ICoordinate coordinates, double range = 10.0f, CancellationToken token = default(CancellationToken));
        Task<IList<IFitEvent>>  MyUpcomingEvents    (int pageNum, int pageSize, CancellationToken token = default(CancellationToken));
        Task<IList<IFitEvent>>  MyInvitedEvents     (int pageNum, int pageSize, CancellationToken token = default(CancellationToken));
        Task<IList<IFitEvent>>  MyCreatedEvents     (int pageNum, int pageSize, CancellationToken token = default(CancellationToken));

        Task                    ToggleGoing         (string eventId, CancellationToken token = default(CancellationToken));
        Task                    SetGoing            (string eventId, bool going, CancellationToken token = default(CancellationToken));
	}
}

