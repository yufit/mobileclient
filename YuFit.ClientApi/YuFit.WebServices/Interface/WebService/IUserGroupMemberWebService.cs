﻿using System.Threading.Tasks;
using System.Collections.Generic;

using YuFit.WebServices.Interface.Model;
using System.Threading;

namespace YuFit.WebServices.Interface.WebService
{
    public interface IUserGroupMemberWebService
    {
        Task<IList<IGroupMember>>   GetGroupMembers         (string groupId, int pageNum, int pageSize, CancellationToken token = default(CancellationToken));
        Task                        SetMemberAdminStatus    (string groupId, string memberId, bool isAdmin, CancellationToken token = default(CancellationToken));
    }
}

