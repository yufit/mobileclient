﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using YuFit.WebServices.Attributes;
using YuFit.WebServices.Exceptions;
using YuFit.WebServices.Extensions;
using YuFit.WebServices.Helper;
using YuFit.WebServices.Interface;
using System.IO;
using System.Threading;
using ModernHttpClient;

namespace YuFit.WebServices.WebService
{
    public class RestService<T>
    {
        protected readonly SessionManager SessionManager;
        private readonly Uri _baseEndpoint;

        #region Properties

        public ILogger Logger { get; set; }

        public string ClientVersion { get; set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="YuFit.WebServices.WebService.RestService`1"/> class.
        /// </summary>
        /// <param name="sessionManager">Session manager.</param>
        /// <param name="serviceBaseUrl">Service base URL.</param>
        /// <param name="resourceName">Resource name.</param>
        public RestService (SessionManager sessionManager, Uri serviceBaseUrl, string resourceName) :
            this(sessionManager, serviceBaseUrl, new Uri(resourceName, UriKind.Relative))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="YuFit.WebServices.WebService.RestService`1"/> class.
        /// </summary>
        /// <param name="sessionManager">Session manager.</param>
        /// <param name="serviceBaseUrl">Service base URL.</param>
        /// <param name="resourcePath">Resource path.</param>
        public RestService (SessionManager sessionManager, Uri serviceBaseUrl, Uri resourcePath = null)
        {
            ArgumentValidator.ValidateArgumentNotNull(serviceBaseUrl, "serviceBaseUrl");

            SessionManager = sessionManager;
            _baseEndpoint = serviceBaseUrl.Combine(resourcePath);
        }

        #region REST methods

        protected async Task<T> Get (string id, CancellationToken token = default(CancellationToken))
        {
            return await Get(new Uri(id, UriKind.Relative));
        }

        protected async Task<T> Get (Uri uri = null, CancellationToken token = default(CancellationToken))
        {
            string responseContent;

            using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, _baseEndpoint.Combine(uri)))
            {
                responseContent = await SendAsync(requestMessage);
            }

            return DeserializeResponse(responseContent);
        }

        protected async Task<IList<T>> GetList (Uri uri = null, CancellationToken token = default(CancellationToken))
        {
            string responseContent;

            using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, _baseEndpoint.Combine(uri)))
            {
                responseContent = await SendAsync(requestMessage, token);
            }

            return DeserializeListResponse(responseContent);
        }

        protected async Task<IList<TResponse>> GetList<TResponse> (Uri uri = null, CancellationToken token = default(CancellationToken))
        {
            string responseContent;

            using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, _baseEndpoint.Combine(uri)))
            {
                responseContent = await SendAsync(requestMessage);
            }

            return DeserializeListResponse<TResponse>(responseContent);
        }

        protected async Task<T> Put (T obj, string id, CancellationToken token = default(CancellationToken))
        {
            return await Put(obj, new Uri(id, UriKind.Relative));
        }

        protected async Task<T> Put (T obj, Uri uri = null, CancellationToken token = default(CancellationToken))
        {
            string responseContent;

            using (var requestMessage = new HttpRequestMessage(HttpMethod.Put, _baseEndpoint.Combine(uri)))
            {
                requestMessage.Content = new StringContent(SerializeRequest(obj));
                responseContent = await SendAsync(requestMessage);
            }

            return (String.IsNullOrEmpty(responseContent)) ? default(T) : DeserializeResponse(responseContent);
        }

        protected async Task<T> Put (JObject obj, Uri uri = null, CancellationToken token = default(CancellationToken))
        {
            string responseContent;

            using (var requestMessage = new HttpRequestMessage(HttpMethod.Put, _baseEndpoint.Combine(uri)))
            {
                requestMessage.Content = new StringContent(SerializeRequest(obj));
                responseContent = await SendAsync(requestMessage);
            }

            return (String.IsNullOrEmpty(responseContent)) ? default(T) : DeserializeResponse(responseContent);
        }

        protected async Task Put (Uri uri = null, CancellationToken token = default(CancellationToken))
        {
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Put, _baseEndpoint.Combine(uri)))
            {
                await SendAsync(requestMessage);
            }
        }

        protected async Task<T> Post (T obj, Uri uri = null, CancellationToken token = default(CancellationToken))
        {
            string responseContent;

            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, _baseEndpoint.Combine(uri)))
            {
                requestMessage.Content = new StringContent(SerializeRequest(obj));
                responseContent = await SendAsync(requestMessage);
            }

            return (String.IsNullOrEmpty(responseContent)) ? default(T) : DeserializeResponse(responseContent);
        }

        protected async Task<T> Post (JObject obj, Uri uri = null, CancellationToken token = default(CancellationToken))
        {
            string responseContent;

            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, _baseEndpoint.Combine(uri)))
            {
                requestMessage.Content = new StringContent(SerializeRequest(obj));
                responseContent = await SendAsync(requestMessage);
            }

            return (String.IsNullOrEmpty(responseContent)) ? default(T) : DeserializeResponse(responseContent);
        }

        protected async Task Post (Uri uri = null, CancellationToken token = default(CancellationToken))
        {
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, _baseEndpoint.Combine(uri)))
            {
                await SendAsync(requestMessage);
            }
        }

        protected async Task<T> UploadFile (Stream fileStream, string segmentHeader, Uri uri = null, CancellationToken token = default(CancellationToken))
        {
            Uri webService = _baseEndpoint.Combine(uri);
            //Uri webService = new Uri("https://staging.api.yu-fit.com:8443/user_groups/561efa5469702d339a00001a/upload_avatar");



            using (HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, webService))
            {
                requestMessage.Headers.ExpectContinue = false;

                EnsureValidSession();
                UpdateHeader(requestMessage);

//                using (var streamReader = new MemoryStream()) {
//                using (fileStream) {
                //fileStream.CopyTo(streamReader);
                using (var httpClient = new HttpClient())
                {
                    MultipartFormDataContent multiPartContent = new MultipartFormDataContent("----MyGreatBoundary");
                    StreamContent byteArrayContent = new StreamContent(fileStream);
                    byteArrayContent.Headers.Add("Content-Type", "image/jpeg");
                    multiPartContent.Add(byteArrayContent, segmentHeader, segmentHeader);
                    requestMessage.Content = multiPartContent;

                    try
                    {
                        using (var response = await httpClient.SendAsync(requestMessage, HttpCompletionOption.ResponseContentRead, CancellationToken.None))
                        {
                            if (response == null)
                            {
                                throw new ServerNotReachableException();
                            }

//                            if (Logger != null) { Logger.LogDiagnostic(string.Format("response: {0}", response.Headers)); }
                            response.EnsureSuccessStatusCode();

                            string responseContent = await response.Content.ReadAsStringAsync();
                            return (String.IsNullOrEmpty(responseContent)) ? default(T) : DeserializeResponse(responseContent);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                        //Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        protected async Task<bool> Delete (string id, CancellationToken token = default(CancellationToken))
        {
            return await Delete(new Uri(id, UriKind.Relative));
        }

        protected async Task<bool> Delete (Uri uri = null, CancellationToken token = default(CancellationToken))
        {
            string responseContent;

            using (var requestMessage = new HttpRequestMessage(HttpMethod.Delete, _baseEndpoint.Combine(uri)))
            {
                responseContent = await SendAsync(requestMessage);
            }

            return (!String.IsNullOrEmpty(responseContent)) && DeserializeResponse<bool>(responseContent);
        }

        private async Task<string> SendAsync (HttpRequestMessage request, CancellationToken token = default(CancellationToken))
        {
            string responseContent = null;

            EnsureValidSession();
            UpdateHeader(request);
            var handler = new NativeMessageHandler();
            using (var httpClient = new HttpClient(handler))
            {
                httpClient.Timeout = new TimeSpan(0, 0, 15);
                HttpResponseMessage response = null;

                try
                {
                    response = await httpClient.SendAsync(request, token);

                    if (response == null)
                    {
                        throw new ServerNotReachableException();
                    }

//					if (Logger != null)
//					{
//						Logger.LogDiagnostic(string.Format("response: {0}", response.Headers));
//					}

                    response.EnsureSuccessStatusCode();

                    responseContent = await response.Content.ReadAsStringAsync();

//					if (Logger != null)
//					{
//						Logger.LogDiagnostic(string.Format("responseContent: {0}", responseContent));
//					}
                }
                catch (TaskCanceledException ex)
                {
                    if (!token.IsCancellationRequested) {
                        // Request timed out
                        throw new ConnectionTimeoutException(ex);
                    } else {
                        throw;
                    }
                }
                catch (Exception ex)
                {
                    if (response != null)
                    {
                        switch (response.StatusCode)
                        {
                            case HttpStatusCode.Unauthorized:
                                throw new UnauthorizedException(ex);
                            case HttpStatusCode.InternalServerError:
                                throw new InternalServerErrorException(ex);
                            case HttpStatusCode.NotFound:
                                throw new NotFoundException(ex);
                            default:
                                throw new ServerException(ex.Message, response.StatusCode, ex);
                        }
                    }

                    throw;
                } 

                return responseContent;
            }
        }

        #endregion

        #region Session Handling

        /// <summary>
        /// Ensures the session is valid and renew it if possible  It is possible for the SessionService
        /// to be null (such as for the authentication service).  If no session service is avail, simply
        /// assume  the  session  is  ok.  If  the  session  is not  null and not  ok, we  will throw an 
        /// AuthRequired exception.
        /// </summary>
        /// <exception cref="T:YuFit.WebServices.Exceptions.NoActiveSessionException"></exception>
        /// ------------------------------------------------------------------------------------
        private async void EnsureValidSession ()
        {
            try {
                if (SessionManager != null && !await SessionManager.EnsureSession()) {
                    throw new NoActiveSessionException();
                } 
            } catch (Exception /*exception*/) {
                
            }
        }

        /// <summary>
        /// Updates the header.
        /// </summary>
        /// <param name="clientRequest">Client request.</param>
        /// ------------------------------------------------------------------------------------
        protected virtual void UpdateHeader (HttpRequestMessage clientRequest)
        {
            #if DEBUG
            clientRequest.Headers.Add("Authorization", "8a9d29966c619ff2418a046f59b5268db87b64d819e347bfb77dcd52fbade5e8703631687372ac32e0c00c6606540f95e837e613ae12dfcf62eca703a6657368");
            #else
			clientRequest.Headers.Add ("Authorization", "8a9d29966c619ff2418a046f59b5268db87b64d819e347bfb77dcd52fbade5e8703631687372ac32e0c00c6606540f95e837e613ae12dfcf62eca703a6657368");
            #endif
            if (SessionManager != null && SessionManager.Session != null && SessionManager.Session.IsValid)
            {
                clientRequest.Headers.Add("yufit-token", SessionManager.Session.AuthenticationToken.ApiToken);
            }

            clientRequest.Headers.Add("yufit-client-version", ClientVersion);


            if ((clientRequest.Method == HttpMethod.Post ||
            clientRequest.Method == HttpMethod.Put) && clientRequest.Content != null)
            {
                clientRequest.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            }
        }

        #endregion

        #region JSON serialization

        /// <summary>
        /// Serializes the request.
        /// </summary>
        /// <returns>The request.</returns>
        /// <param name="request">Request.</param>
        private string SerializeRequest (T request)
        {
            string serializedObject = SerializeObject(request);
            return serializedObject;
        }

        /// <summary>
        /// Serializes the request.
        /// </summary>
        /// <returns>The request.</returns>
        /// <param name="request">Request.</param>
        private string SerializeRequest<TReq> (TReq request)
        {
            return SerializeObject(request);
        }

        /// <summary>
        /// Serializes the object.
        /// </summary>
        /// <returns>The object.</returns>
        /// <param name="value">Value.</param>
        private string SerializeObject (object value)
        {
            try
            {
                var serializerSettings = new JsonSerializerSettings {
                    Formatting = Formatting.None,
                    NullValueHandling = NullValueHandling.Ignore
                };

                var attribute = GetWrapperAttribute(value);

                if (attribute != null)
                {
                    return JsonConvert.SerializeObject(
                        new JObject(
                            new JProperty(
                                attribute.Name, 
                                JObject.FromObject(value, new JsonSerializer { NullValueHandling = NullValueHandling.Ignore })
                            )
                        ), serializerSettings
                    );
                }

                return JsonConvert.SerializeObject(value, serializerSettings);

            }
            catch (Exception ex)
            {
                throw new DataConversionException(ex);
            }
        }

        /// <summary>
        /// Deserializes the response.
        /// </summary>
        /// <returns>The response.</returns>
        /// <param name="response">Response.</param>
        private T DeserializeResponse (string response)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (Exception ex)
            {
                throw new DataConversionException(ex);
            }
        }

        /// <summary>
        /// Deserializes the response.
        /// </summary>
        /// <returns>The response.</returns>
        /// <param name="response">Response.</param>
        /// <typeparam name="TResp">The 1st type parameter.</typeparam>
        private TResp DeserializeResponse<TResp> (string response)
        {
            try
            {
                return JsonConvert.DeserializeObject<TResp>(response);
            }
            catch (Exception ex)
            {
                throw new DataConversionException(ex);
            }
        }

        /// <summary>
        /// Deserializes the list response.
        /// </summary>
        /// <returns>The list response.</returns>
        /// <param name="response">Response.</param>
        private IList<T> DeserializeListResponse (string response)
        {
            try
            {
                return JsonConvert.DeserializeObject<List<T>>(response);
            }
            catch (Exception ex)
            {
                if (Logger != null)
                {
                    Logger.LogDiagnostic(ex.ToString());
                }
                ;
                throw new DataConversionException(ex);
            }
        }

        private IList<TType> DeserializeListResponse<TType> (string response)
        {
            try
            {
                return JsonConvert.DeserializeObject<List<TType>>(response);
            }
            catch (Exception ex)
            {
                if (Logger != null)
                {
                    Logger.LogDiagnostic(ex.ToString());
                }
                ;
                throw new DataConversionException(ex);
            }
        }

        private static JsonWrapperAttribute GetWrapperAttribute (object value)
        {
            JsonWrapperAttribute attribute = null;

            TypeInfo typeInfo = value.GetType().GetTypeInfo();

            var attributes = typeInfo.GetCustomAttributes(typeof(JsonWrapperAttribute));
            attribute = (JsonWrapperAttribute) attributes.FirstOrDefault();

            if (attribute == null)
            {
                var ifaces = typeInfo.ImplementedInterfaces;

                foreach (var iface in ifaces)
                {
                    attribute = (JsonWrapperAttribute) iface.GetTypeInfo().GetCustomAttributes(typeof(JsonWrapperAttribute)).FirstOrDefault();

                    if (attribute != null)
                    {
                        break;
                    }
                }
            }

            return attribute;
        }

        #endregion
    }
}

