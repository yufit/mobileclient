﻿using System;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

using YuFit.WebServices.Helper;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;
using YuFit.WebServices.Exceptions;

namespace YuFit.WebServices.WebService
{
    public class DeviceWebService : RestService<IDeviceInformation>, IDeviceWebService
    {
        public DeviceWebService (SessionManager sessionManager, Uri serviceBaseAddress)
            : base(sessionManager,serviceBaseAddress, "user_devices")
        {
        }

        #region IDeviceWebService implementation

        /// <summary>
        /// Adds a new device to the user.
        /// To add a device: `POST http://localhost:3000/user_devices` This method is idempotent 
        /// and behind the scenes will determine what needs to happen. This cannot detect 
        /// notification token changes since that is what is used to find the device in the 
        /// first place.
        ///
        /// The `POST` will return the following so that the client can keep track of the
        /// object ID assigned to the device so a token change can be sent
        /// 
        /// {
        ///     "device_id": "567bfd0e676f741045000000",
        ///     "notification_token": "your device specific token"
        /// }:
        /// </summary>
        /// <returns>The device.</returns>
        /// <param name="userId">User identifier.</param>
        /// <param name="deviceInformation">Device information.</param>
        public async Task<IDeviceInformation> AddNewDevice (string userId, IDeviceInformation deviceInformation)
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(userId, "userId");
            ArgumentValidator.ValidateArgumentNotNull(deviceInformation, "deviceInformation");
            ArgumentValidator.ValidateArgumentIsNullOrEmpty(deviceInformation.DeviceId, "deviceInformation.DeviceId");

            var deviceInfo = new JObject (new JProperty ("device", JObject.FromObject(deviceInformation)));

            return await Post(deviceInfo);
        }

        /// <summary>
        /// Update the device notification id.
        /// 
        /// When the client notices a token change, use 
        /// PUT http://localhost:3000/user_devices/567bfd0e676f741045000000
        /// with the same schema as `POST`, using the updated token. Server will 
        /// update the token with SNS and re-enable the endpoint if it has been 
        /// disabled by Amazon.
        /// </summary>
        /// <returns>The device.</returns>
        /// <param name="userId">User identifier.</param>
        /// <param name="deviceInformation">Device information.</param>
        public async Task<IDeviceInformation> UpdateDevice (string userId, IDeviceInformation deviceInformation)
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(userId, "userId");
            ArgumentValidator.ValidateArgumentNotNull(deviceInformation, "deviceInformation");
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(deviceInformation.DeviceId, "deviceInformation.DeviceId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(deviceInformation.DeviceId);

            var deviceInfo = new JObject (new JProperty ("device", JObject.FromObject(deviceInformation)));

            try {
                return await Put(deviceInfo, uriPathBuilder.Build());
            } catch (NotFoundException /*ex*/) {
                deviceInformation.DeviceId = null;
                return await AddNewDevice(userId, deviceInformation);
            } catch (Exception ex) {
                throw ex;
            }
        }

        /// <summary>
        /// Finally, if a user logs out on a device, the client should send a 
        /// DELETE http://localhost:3000/user_devices/567bf9d5676f74102c000000. 
        /// This will delete the device from the user, thus disabling any new 
        /// notifications for that user (on that device).
        ///
        /// Remember to use the `device_id` returned from the server for `PUT` and `DELETE`!!
        /// It is ​*not*​ the notification token itself.
        /// </summary>
        /// <returns>The device.</returns>
        /// <param name="userId">User identifier.</param>
        /// <param name="serverDeviceId">Server device identifier.</param>
        public async Task<bool> RemoveDevice (string userId, string serverDeviceId)
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(userId, "userId");
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(serverDeviceId, "serverDeviceId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(serverDeviceId);

            return await Delete(uriPathBuilder.Build());
        }

        #endregion
    }
}

