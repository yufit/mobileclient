﻿using System;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

using YuFit.WebServices.Helper;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;
using YuFit.WebServices.Model;

namespace YuFit.WebServices.WebService
{
    /// <summary>
    /// Authentication service.
    /// </summary>
    /// ----------------------------------------------------------------------------------------
    public class AuthenticationWebService : RestService<User>, IAuthenticationWebService
    {
        #region Constructor/Factory

        /// <summary>
        /// Initializes a new instance of the <see cref="YuFit.WebServices.WebService.AuthenticationWebService"/> class.
        /// </summary>
        /// <param name = "serviceBaseAddress"></param>
        public AuthenticationWebService (Uri serviceBaseAddress) : 
            base(null, serviceBaseAddress, "users")
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Opens the session.
        /// </summary>
        /// <returns>The session.</returns>
        /// <param name = "credentials"></param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<User> AuthenticateUser(IAuthenticationCredentials credentials)
        {
            ArgumentValidator.ValidateArgumentNotNull(credentials, "credentials");

            JObject authenticationPackage = new JObject (new JProperty ("service_identity", JObject.FromObject(credentials)));
            User authenticatedUser = await Post(authenticationPackage);

            return authenticatedUser;
        }

        #endregion
    }
}

