﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using YuFit.WebServices.Helper;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;

namespace YuFit.WebServices.WebService
{
    public class FitEventCommentWebService : RestService<IFitEventComment>, IFitEventCommentWebService
    {
        public FitEventCommentWebService (SessionManager sessionManager, Uri serviceBaseAddress) :
            base(sessionManager, serviceBaseAddress, "events")
        {
        }

        #region Event Comments impl

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Retrieve event associated comments.
        /// </summary>
        /// <returns>The comments.</returns>
        /// <param name="fitEvent">Fit event.</param>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IList<IFitEventComment>> GetComments(IFitEvent fitEvent, int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNull(fitEvent, "fitEvent");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(fitEvent.Id);
            uriPathBuilder.AddSegment("event_comments");
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());

            return await GetList(uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
		/// <summary>
		/// Add a comment for an event.
		/// </summary>
		/// <returns>The added comment.</returns>
		/// <param name="fitEvent">Fit event.</param>
		/// <param name="fitEventComment">Fit event comment.</param>
        /// <param name="token">Cancelation token</param>
		/// ------------------------------------------------------------------------------------
		/// <exception cref="T:System.ArgumentException"></exception>
		/// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
		/// ------------------------------------------------------------------------------------
        public async Task<IFitEventComment> AddComment(IFitEvent fitEvent, IFitEventComment fitEventComment, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNull(fitEvent, "fitEvent");
            ArgumentValidator.ValidateArgumentNotNull(fitEventComment, "fitEventComment");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(fitEvent.Id);
            uriPathBuilder.AddSegment("event_comments");

            return await Post(fitEventComment, uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Update a comment for an event.
        /// </summary>
        /// <returns>The added comment.</returns>
        /// <param name="fitEvent">Fit event.</param>
        /// <param name="fitEventComment">Fit event comment.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IFitEventComment> UpdateComments(IFitEvent fitEvent, IFitEventComment fitEventComment, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNull(fitEvent, "fitEvent");
            ArgumentValidator.ValidateArgumentNotNull(fitEventComment, "fitEventComment");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(fitEvent.Id);
            uriPathBuilder.AddSegment("event_comments");
            uriPathBuilder.AddSegment(fitEventComment.Id);

            return await Put(fitEventComment, uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Delete a comment for an event.
        /// </summary>
        /// <returns>The added comment.</returns>
        /// <param name="fitEvent">Fit event.</param>
        /// <param name="fitEventComment">Fit event comment.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<bool> DeleteComment(IFitEvent fitEvent, IFitEventComment fitEventComment, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNull(fitEvent, "fitEvent");
            ArgumentValidator.ValidateArgumentNotNull(fitEventComment, "fitEventComment");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(fitEvent.Id);
            uriPathBuilder.AddSegment("event_comments");
            uriPathBuilder.AddSegment(fitEventComment.Id);

            return await Delete(uriPathBuilder.Build(), token: token);
        }

        #endregion
    }
}

