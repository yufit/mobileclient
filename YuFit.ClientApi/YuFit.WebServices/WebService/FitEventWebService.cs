﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

using YuFit.WebServices.Helper;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;

namespace YuFit.WebServices.WebService
{
    class FitEventWebService : RestService<IFitEvent>, IFitEventWebService
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="YuFit.WebServices.WebService.FitEventWebService"/> class.
        /// </summary>
        /// <param name="sessionManager">Session manager.</param>
        /// <param name="serviceBaseAddress">Service base address.</param>
        public FitEventWebService (SessionManager sessionManager, Uri serviceBaseAddress) :
            base(sessionManager, serviceBaseAddress, "events")
        {
        }

        #endregion

        #region IFitEventService implementation

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Gets all events.
        /// </summary>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// <returns>The list of all events.</returns>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IList<IFitEvent>> GetAllEvents (int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());

            return await GetList(uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the event.
        /// </summary>
        /// <returns>The event.</returns>
        /// <param name="eventId">Event identifier.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IFitEvent> GetEvent (string eventId, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(eventId, "eventId");

            return await Get(eventId, token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Creates the event.
        /// </summary>
        /// <returns>The event.</returns>
        /// <param name="fitEvent">Fit event.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IFitEvent> CreateEvent (IFitEvent fitEvent, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNull(fitEvent, "fitEvent");
            ArgumentValidator.ValidateArgumentIsNullOrEmpty(fitEvent.Id, "fitEvent.Id");

            return await Post(fitEvent, null, token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Updates the event.
        /// </summary>
        /// <returns>The event.</returns>
        /// <param name="fitEvent">Fit event.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IFitEvent> UpdateEvent (IFitEvent fitEvent, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNull(fitEvent, "fitEvent");
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(fitEvent.Id, "fitEvent.Id");

            return await Put(fitEvent, fitEvent.Id, token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Deletes the event.
        /// </summary>
        /// <returns>The event.</returns>
        /// <param name="fitEvent">Fit event.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<bool> DeleteEvent (IFitEvent fitEvent, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNull(fitEvent, "fitEvent");
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(fitEvent.Id, "fitEvent.Id");

            return await Delete(fitEvent.Id, token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Get the specified coordinates and range.
        /// </summary>
        /// <param name="coordinates">Coordinates.</param>
        /// <param name="range">Range.</param>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IList<IFitEvent>> SearchForEvents (int pageNum, int pageSize, ICoordinate coordinates, double range = 10.0f, CancellationToken token = default(CancellationToken))
        {
            string latString = coordinates.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string longString = coordinates.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddQueryParameter("latitude", latString);
            uriPathBuilder.AddQueryParameter("longitude", longString);
            uriPathBuilder.AddQueryParameter("range", string.Format("{0}", range));
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());

            var result = await GetList(uriPathBuilder.Build(), token);
            return result;
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the invited events for the current user.
        /// </summary>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// <returns>The invited events.</returns>
        public async Task<IList<IFitEvent>> MyInvitedEvents (int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment("my_invited");
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());
            return await GetList(uriPathBuilder.Build(), token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the upcoming events for the current user.
        /// </summary>
        /// <returns>The upcoming events.</returns>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        public async Task<IList<IFitEvent>> MyUpcomingEvents (int pageNum, int pageSize,  CancellationToken token = default(CancellationToken))
        {
            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment("my_upcoming");
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());
            return await GetList(uriPathBuilder.Build(), token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Gets events the authenticated user has created.
        /// </summary>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// <returns>The events.</returns>
        public async Task<IList<IFitEvent>> MyCreatedEvents (int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment("my_created");
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());
            return await GetList(uriPathBuilder.Build(), token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Toggle user going to activity bit.
        /// </summary>
        /// <param name="eventId">event interested in joining/leaving.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task ToggleGoing (string eventId, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(eventId, "eventId");
            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(eventId);
            uriPathBuilder.AddSegment("going");
            await Get(uriPathBuilder.Build(), token);

        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Set the going state of the user to that event.
        /// </summary>
        /// <returns>The going.</returns>
        /// <param name="eventId">Event identifier.</param>
        /// <param name="going"></param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task SetGoing (string eventId, bool going, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(eventId, "eventId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(eventId);
            uriPathBuilder.AddSegment("set_going");
            uriPathBuilder.AddQueryParameter("going", going.ToString());

            await Get(uriPathBuilder.Build(), token);
        }

        #endregion
    }
}

