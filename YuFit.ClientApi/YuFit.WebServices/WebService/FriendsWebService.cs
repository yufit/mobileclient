﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using YuFit.WebServices.Helper;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;

namespace YuFit.WebServices.WebService
{
    public class FriendsWebService : RestService<IUser>, IFriendsWebService
    {
        public FriendsWebService (SessionManager sessionManager, Uri serviceBaseAddress)
            : base(sessionManager,serviceBaseAddress, "friends")
        {
        }

        #region IFriendsWebService implementation

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the friends of a friend.
        /// </summary>
        /// <returns>The friends of the friend.</returns>
        /// <param name="userId">friend id to get info from</param>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IList<IUser>> GetFriendsOf(string userId, int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(userId, "friendId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(userId);
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());

            return await GetList(uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the groups of a friend.
        /// </summary>
        /// <returns>The groups of the friend.</returns>
        /// <param name="userId">friend id to get info from</param>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IList<IUserGroup>> GetGroupsOf(string userId, int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(userId, "friendId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(userId);
            uriPathBuilder.AddSegment("user_groups");
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());

            return await GetList<IUserGroup>(uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the current user list of friends.
        /// </summary>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <returns>The groups of the friend.</returns>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IList<IUser>> MyFriends(int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());
            return await GetList(token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Add the specified friend id to your friend list.
        /// </summary>
        /// <returns>none</returns>
        /// <param name="friendId">friend id to be friended</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<bool> AddFriend (string friendId, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(friendId, "friendId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(friendId);

            await Put(uriPathBuilder.Build(), token: token);
            return true;
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Remove the specified friend id from your friend list.
        /// </summary>
        /// <returns>none</returns>
        /// <param name="friendId">friend id to be unfriended</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<bool> UnFriend (string friendId, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(friendId, "friendId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(friendId);

            await Delete(uriPathBuilder.Build(), token: token);
            return true;
        }

        #endregion
    }
}

