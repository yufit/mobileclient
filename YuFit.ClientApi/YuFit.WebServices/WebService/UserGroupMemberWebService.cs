﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

using YuFit.WebServices.Helper;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;

namespace YuFit.WebServices.WebService
{
    public class UserGroupMemberWebService : RestService<IGroupMember>, IUserGroupMemberWebService
    {
        public UserGroupMemberWebService (SessionManager sessionManager, Uri serviceBaseAddress)
            : base(sessionManager, serviceBaseAddress, "user_groups")
        {
        }

        #region IUserGroupMemberWebService implementation

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Returns the members of a group
        /// </summary>
        /// <returns>The members of the requested group.</returns>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IList<IGroupMember>> GetGroupMembers (string groupId, int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(groupId, "groupId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(groupId);
            uriPathBuilder.AddSegment("user_group_members");
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());
            return await GetList(uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Sets the member admin status.  If this fails, it will throw an exception.
        /// </summary>
        /// <returns>The member admin status.</returns>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="memberId">Member identifier.</param>
        /// <param name="isAdmin">If set to <c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task SetMemberAdminStatus(string groupId, string memberId, bool isAdmin, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(groupId, "groupId");
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(groupId, "memberId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(groupId);
            uriPathBuilder.AddSegment("user_group_members");
            uriPathBuilder.AddSegment(memberId);

            var data = @"{'user_group_member': {'is_manager': " + string.Format("{0}", isAdmin).ToLower() + @" }}";
            JObject member = JObject.Parse(data);
            await Put(member, uriPathBuilder.Build(), token: token);
        }


        #endregion
    }
}

