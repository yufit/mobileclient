﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using YuFit.WebServices.Helper;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;

namespace YuFit.WebServices.WebService
{
    class NotificationWebService :RestService<INotification>, INotificationWebService
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="YuFit.WebServices.WebService.NotificationWebService"/> class.
        /// </summary>
        /// <param name="sessionManager">Session manager.</param>
        /// <param name="serviceBaseAddress">Service base address.</param>
        public NotificationWebService(SessionManager sessionManager, Uri serviceBaseAddress) :
        base(sessionManager, serviceBaseAddress, "users")
        {
        }

        #endregion

        #region INotificationService implementation

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the notifications for user.
        /// </summary>
        /// <returns>The notifications for user.</returns>
        /// <param name="userId">User identifier.</param>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IList<INotification>> GetNotificationsForUser(string userId, int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(userId, "userId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(userId);
            uriPathBuilder.AddSegment("notifications");
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());

            return await GetList(uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Acks the notification for user.
        /// </summary>
        /// <returns>The notification for user.</returns>
        /// <param name="userId">User identifier.</param>
        /// <param name="notificationId">Notification identifier.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<bool> AckNotificationForUser(string userId, string notificationId, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(userId, "userId");
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(notificationId, "notificationId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(userId);
            uriPathBuilder.AddSegment("notifications");
            uriPathBuilder.AddSegment(notificationId);

            return await Delete(uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Acks all notifications for user.
        /// </summary>
        /// <returns>The all notifications for user.</returns>
        /// <param name="userId">User identifier.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<bool> AckAllNotificationsForUser(string userId, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(userId, "userId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(userId);
            uriPathBuilder.AddSegment("notifications");

            return await Delete(uriPathBuilder.Build(), token: token);
        }

        #endregion
    }
}

