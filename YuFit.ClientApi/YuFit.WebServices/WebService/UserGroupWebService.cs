﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using YuFit.WebServices.Helper;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;

namespace YuFit.WebServices.WebService
{
    public class UserGroupWebService : RestService<IUserGroup>, IUserGroupWebService
    {
        public UserGroupWebService (SessionManager sessionManager, Uri serviceBaseAddress) 
            : base(sessionManager, serviceBaseAddress, "user_groups")
        {
        }

        #region IGroupWebService implementation

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Get the list of groups in the system.
        /// </summary>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// <returns>The list of groups.</returns>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IList<IUserGroup>> GetGroups(int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());

            return await GetList(uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Get my groups
        /// </summary>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// <returns>My groups.</returns>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IList<IUserGroup>> GetMyGroups(int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment("my");
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());
            return await GetList(uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Get info for a specific group.
        /// </summary>
        /// <returns>Group information.</returns>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IUserGroup> GetGroup(string groupId, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNull(groupId, "groupId");
            return await Get(groupId, token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Create a group.
        /// </summary>
        /// <returns>Created group.</returns>
        /// <param name="group">Group identifier.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IUserGroup> CreateGroup(IUserGroup group, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNull(group, "group");
            ArgumentValidator.ValidateArgumentIsNullOrEmpty(group.Id, "group.Id");

            return await Post(group, token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Update the group.
        /// </summary>
        /// <returns>Updated group info.</returns>
        /// <param name="group">Group to update.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IUserGroup> UpdateGroup(IUserGroup group, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNull(group, "group");
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(group.Id, "group.Id");
            return await Put(group, group.Id, token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Delete the specified group.
        /// </summary>
        /// <returns>Nada.</returns>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<bool> DeleteGroup(string groupId, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(groupId, "group.Id");
            return await Delete(groupId, token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Sets the banner image for a group.
        /// </summary>
        /// <returns>The member admin status.</returns>
        /// <param name="group">Group.</param>
        /// <param name="contentType">Stream mime content type.</param>
        /// <param name="image">image bits.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IUserGroup> UpdateGroupImage(IUserGroup group, string contentType, System.IO.Stream image, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNull(group, "group");
            ArgumentValidator.ValidateArgumentNotNull(image, "image");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(group.Id);
            uriPathBuilder.AddSegment("upload_group_photo");
            return await UploadFile(image, "group_photo", uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Sets the avatar image for a group.
        /// </summary>
        /// <returns>The member admin status.</returns>
        /// <param name="group">Group.</param>
        /// <param name="contentType">Stream mime content type.</param>
        /// <param name="image">image bits.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IUserGroup> UpdateAvatarImage(IUserGroup group, string contentType, System.IO.Stream image, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNull(group, "group");
            ArgumentValidator.ValidateArgumentNotNull(image, "image");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(group.Id);
            uriPathBuilder.AddSegment("upload_avatar");
            return await UploadFile(image, "avatar", uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Search for groups matching the query.
        /// </summary>
        /// <returns>Matching group(s).</returns>
        /// <param name="query">Query.</param>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IList<IUserGroup>> SearchGroup(string query, int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(query, "query");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddQueryParameter("name", query);
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());
            return await GetList(uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Add the current user as a member of the specified group.
        /// </summary>
        /// <returns>Nada</returns>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task JoinGroup(string groupId, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(groupId, "groupId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(groupId);
            uriPathBuilder.AddSegment("user_group_members");
            await Post(uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Remove the specified user as a member of the specified group.  This really remove
        /// the current user.  But we don't have the current user id in this so the caller must
        /// pass it.
        /// </summary>
        /// <returns>Nada</returns>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="memberId">Member to be removed.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task LeaveGroup(string groupId, string memberId, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(groupId, "groupId");
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(memberId, "memberId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(groupId);
            uriPathBuilder.AddSegment("user_group_members");
            uriPathBuilder.AddSegment(memberId);
            await Delete(uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Invite the specified users to the group.
        /// </summary>
        /// <returns>Created info</returns>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="usersId">List of member to be added.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IUserGroup> InviteUserToGroup(string groupId, IList<string> usersId, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(groupId, "groupId");
            ArgumentValidator.ValidateArgumentNotNull(usersId, "usersId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(groupId);

            var group               = await GetGroup(groupId);
            var serializerSettings  = new JsonSerializerSettings { Formatting = Formatting.None };
            var serializedGroup     = JsonConvert.SerializeObject(group, serializerSettings);
            var serializedIds       = JsonConvert.SerializeObject(usersId, serializerSettings);

            var data = "{\"user_group\": " + serializedGroup + ", \"invited_users\": " + serializedIds + "}";
            JObject request = JObject.Parse(data);
            return await Put(request, uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Remove the specified user as a member of the specified group.
        /// </summary>
        /// <returns>Nada</returns>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="memberId">Member to be removed.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task RemoveUserFromGroup(string groupId, string memberId, CancellationToken token = default(CancellationToken))
        {
            await LeaveGroup(groupId, memberId, token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Retrieve the list of upcoming events for the group.
        /// </summary>
        /// <returns>Nada</returns>
        /// <param name="groupId">Group identifier.</param>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many groups per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IList<IFitEvent>> UpcomingEvents(string groupId, int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(groupId, "groupId");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(groupId);
            uriPathBuilder.AddSegment("upcoming_events");
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());


            return await GetList<IFitEvent>(uriPathBuilder.Build(), token: token);
        }


        #endregion
    }
}

