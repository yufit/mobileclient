﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

using YuFit.WebServices.Helper;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;

namespace YuFit.WebServices.WebService
{
    /// ----------------------------------------------------------------------------------------
    class UserWebService : RestService<IUser>, IUserWebService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="YuFit.WebServices.WebService.UserWebService"/> class.
        /// </summary>
        /// <param name = "sessionManager"></param>
        /// <param name = "serviceBaseAddress"></param>
        /// ------------------------------------------------------------------------------------
        public UserWebService (SessionManager sessionManager, Uri serviceBaseAddress)
            : base(sessionManager,serviceBaseAddress, "users")
        {
        }

        #region IUserService implementation

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Gets the user information.
        /// </summary>
        /// <returns>The user information.</returns>
        /// <param name="userId">User identifier.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IUser> GetUserInformation (string userId, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(userId, "userId");

            return await Get(userId, token: token);
        }

        /// <summary>
        /// Updates the user information.
        /// </summary>
        /// <returns>The user information.</returns>
        /// <param name="user">User.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IUser> UpdateUserInformation (IUser user, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNull(user, "user");
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(user.Id, "user.Id");

            return await Put(user, user.Id, token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Sets the user filters.
        /// </summary>
        /// <returns>The updated user.</returns>
        /// <param name="userId">User identifier.</param>
        /// <param name="filter">Filters to be set.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IUser> SetEventFilter(string userId, IEventFilter filter, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(userId, "userId");
            ArgumentValidator.ValidateArgumentNotNull(filter, "filter");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(userId);
            uriPathBuilder.AddSegment("event_filter");

            var eventFilter = new JObject (new JProperty ("event_filter", JObject.FromObject(filter)));

            return await Put(eventFilter, uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Sets the avatar image for a user.
        /// </summary>
        /// <returns>The updated user.</returns>
        /// <param name="userId">User identifier.</param>
        /// <param name="contentType">Stream mime content type.</param>
        /// <param name="image">image bits.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IUser> UpdateAvatarImage(string userId, string contentType, System.IO.Stream image, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(userId, "userId");
            ArgumentValidator.ValidateArgumentNotNull(image, "image");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddSegment(userId);
            uriPathBuilder.AddSegment("upload_avatar");
            return await UploadFile(image, "avatar", uriPathBuilder.Build(), token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns>The list of all users.</returns>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IList<IUser>> GetAllUsers (CancellationToken token = default(CancellationToken))
        {
            return await GetList(token: token);
        }

        /// ------------------------------------------------------------------------------------
        /// <summary>
        /// Search for users matching the query.
        /// </summary>
        /// <returns>Matching user(s).</returns>
        /// <param name="query">Query.</param>
        /// <param name="pageNum">Page to start from.</param>
        /// <param name="pageSize">How many users per page.<c>true</c> is admin.</param>
        /// <param name="token">Cancelation token</param>
        /// ------------------------------------------------------------------------------------
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:YuFit.WebServices.Exceptions.DataConversionException"></exception>
        /// ------------------------------------------------------------------------------------
        public async Task<IList<IUser>> SearchUser(string query, int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            ArgumentValidator.ValidateArgumentNotNullOrEmpty(query, "query");

            var uriPathBuilder = new UriPathBuilder();
            uriPathBuilder.AddQueryParameter("name", query);
            uriPathBuilder.AddQueryParameter("page", pageNum.ToString());
            uriPathBuilder.AddQueryParameter("page_size", pageSize.ToString());

            return await GetList(uriPathBuilder.Build(), token: token);
        }


        #endregion
    }
}

