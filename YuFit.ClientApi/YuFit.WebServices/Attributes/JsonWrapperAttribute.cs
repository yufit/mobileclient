﻿using System;

namespace YuFit.WebServices.Attributes
{
	// Analysis disable once InconsistentNaming
    public class JsonWrapperAttribute : Attribute
    {
		public string Name { get; set; }
		public JsonWrapperAttribute(string name)
        {
			Name = name;
        }
    }
}

