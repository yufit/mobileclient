﻿using System;
using System.Threading.Tasks;

using YuFit.WebServices.Exceptions;
using YuFit.WebServices.Helper;
using YuFit.WebServices.Interface;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;

namespace YuFit.WebServices
{
    /// <summary>
    /// Session manager.
    /// </summary>
    public class SessionManager
    {
        readonly IAuthenticationWebService _authenticationService;
        readonly ISessionStore _sessionStore;
        readonly YuFitSession _session = new YuFitSession();
        readonly object _locker = new object();

        public IYuFitSession Session { get { return _session; } }

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="YuFit.WebServices.SessionManager"/> class.
        /// </summary>
        /// <param name="authenticationService">Authentication service.</param>
        /// <param name="sessionStore">Session store.</param>
        public SessionManager (IAuthenticationWebService authenticationService, ISessionStore sessionStore)
        {
            ArgumentValidator.ValidateArgumentNotNull(authenticationService, "authenticationService");
            ArgumentValidator.ValidateArgumentNotNull(sessionStore, "sessionStore");

            _authenticationService = authenticationService;
            _sessionStore = sessionStore;

            ReloadPreviousSession();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Opens a new session using the provided credentials.
        /// </summary>
        /// <returns>The opened session.</returns>
        /// <param name="credentials">Authentication credentials for opening the session.</param>
        public async Task<IYuFitSession> OpenSession (IAuthenticationCredentials credentials)
        {
            try
            {
                var user = await _authenticationService.AuthenticateUser(credentials);

                _session.AuthenticatedUser = user;
                _session.AuthenticationCredentials = credentials;
                _session.AuthenticationToken = user.Token;
                StoreSession();
            }
            catch (Exception ex)
            {
                _session.Invalidate();
                throw new UnauthorizedException(ex);
            }

            return _session;
        }

        public void CloseSession ()
        {
            ClearSession();
        }

        /// <summary>
        /// Ensures the session is valid. Will use stored credentials to attempt
        /// session renewal if necessary.
        /// </summary>
        /// <returns>Whether the session is valid or not.</returns>
        public async Task<bool> EnsureSession ()
        {
            return !_session.NeedsRenewal || await ReAuthenticate();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Reloads the previous session. If no session was previously available, 
        /// a new one will be created.
        /// 
        /// We trap any possible exception in case the session store provider decides
        /// to barf all over and not handle it's own exception.  We don't want to be
        /// responsible for an application crash.
        /// </summary>
        /// <returns>The previous session if available, otherwise a new one.</returns>
        /// ------------------------------------------------------------------------------------
        void ReloadPreviousSession ()
        {
            try
            {
                var sessionInfo = _sessionStore.LoadSessionInformation();
                lock (_locker)
                {
                    var storedSession = YuFitSession.Deserialize(sessionInfo);
                    _session.AuthenticatedUser = storedSession.AuthenticatedUser;
                    _session.AuthenticationCredentials = storedSession.AuthenticationCredentials;
                    _session.AuthenticationToken = storedSession.AuthenticationToken;
                }
            } 
            // Analysis disable once EmptyGeneralCatchClause
            catch (Exception /* ex */)
            {
            }
        }

        /// <summary>
        /// Stores the session.
        /// 
        /// We trap any possible exception in case the session store provider decides
        /// to barf all over and not handle it's own exception.  We don't want to be
        /// responsible for an application crash.
        /// </summary>
        /// ------------------------------------------------------------------------------------
        void StoreSession ()
        {
            // Analysis disable once EmptyGeneralCatchClause
            try { _sessionStore?.StoreSessionInformation(_session.Serialize()); } 
            catch (Exception) { }
        }

        /// <summary>
        /// Clears the session.
        /// </summary>
        void ClearSession ()
        {
            try
            {
                _sessionStore?.StoreSessionInformation(string.Empty);
                lock (_locker) { _session.Invalidate(); } 
            }
            // Analysis disable once EmptyGeneralCatchClause
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Re-authenticate with stored credentials.
        /// </summary>
        /// <returns>Whether the session has been authenticated.</returns>
        async Task<bool> ReAuthenticate ()
        {
            bool authenticated = false;
            if (_session.AuthenticationCredentials == null)
            {
                ClearSession();
                throw new UnauthorizedException();
            }

            try
            {
                var user = await _authenticationService.AuthenticateUser(_session.AuthenticationCredentials);
                if (user != null)
                {
                    lock (_locker)
                    {
                        _session.AuthenticatedUser = user;
                        _session.AuthenticationToken = user.Token;
                        StoreSession();
                    }
                    authenticated = true;
                }
            }
            catch (UnauthorizedException)
            {
                ClearSession();
                throw;
            }

            return authenticated;
        }

        #endregion
    }
}

