using System;
using UIKit;
using MapKit;

namespace YuFit.IOS.Interfaces
{
    public interface IFitActivityAnnotation
    {
        UIColor Color { get; }
        string AvatarUrl { get; }
    }
}

