﻿using System;
using UIKit;

namespace YuFit.IOS.Interfaces
{
    public interface IIOSPushNotificationRegistration
    {
        UIMutableUserNotificationCategory BuildIOSNotificationCategory();
    }
}

