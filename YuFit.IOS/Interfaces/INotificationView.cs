﻿using System;
using UIKit;

namespace YuFit.IOS.Interfaces
{
    public interface INotificationView
    {
        nfloat Height { get; }
        UIView View { get; }
    }
}

