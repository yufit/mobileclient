using System;
using UIKit;
using MvvmCross.iOS.Views;
using MvvmCross.Core.ViewModels;

namespace YuFit.IOS.Interfaces
{
    public interface IContainerViewPresenter
    {
        void PresentViewInsideContainer(IMvxIosView viewController, string containerID, MvxPresentationHint hint);
        void DismissViewAssociatedToViewModel(IMvxViewModel viewModel);
        bool IsPresenting(IMvxViewModel viewModel);
        bool IsPresenting(Type viewModelType);
    }
}

