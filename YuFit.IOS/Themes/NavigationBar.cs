using UIKit;

namespace YuFit.IOS.Themes
{
    public static class NavigationBar {
        public static UIFont TitleFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
        public static UIColor TintColor = YuFitStyleKit.Black;
        public static UIColor FontColor = YuFitStyleKit.LightGray;
        public static UIColor BackButtonColor = YuFitStyleKit.Red;
    }
}


