using System;
using CoreGraphics;
using System.Linq;
using System.Reflection;
using UIKit;

namespace YuFit.IOS.Themes
{
    public struct ActivityColors {
        public UIColor Base;
        public UIColor Dark;
        public UIColor Light;
    }

    public static class YuFitStyleKitExtension
    {
        static string ActivityTypeToStyleKitActivity(string activityType) {
            return activityType;
        }

        public static Action<CGRect, UIColor> GetDrawingMethod(string methodName) {

            try {
                Action<CGRect, UIColor> drawDelegate;
                
                drawDelegate = Assembly.GetExecutingAssembly ()
                    .GetTypes ()
                    .Where (x => x.Name.Equals ("YuFitStyleKit"))
                    .Select (t => (Action<CGRect, UIColor>)Delegate.CreateDelegate (
                        typeof(Action<CGRect, UIColor>), 
                        null, 
                        t.GetMethod (methodName, BindingFlags.Static | BindingFlags.Public)))
                    .FirstOrDefault ();
                return drawDelegate;
            } catch (Exception /*ex*/) {
                return null;
            }
        }

        public static UIColor GetColor(string color) {
            try
            {
                string key = color;
                PropertyInfo pi = Assembly.GetExecutingAssembly ()
                    .GetTypes ()
                    .Where (x => x.Name.Equals ("YuFitStyleKit"))
                    .Select (t => t.GetProperty (key))
                    .FirstOrDefault ();
                return (UIColor) pi.GetMethod.Invoke(null, null);
            }
            catch(Exception /*ex*/)
            {
                return UIColor.White;
            }
        }

        public static ActivityColors GetColorGroup(string color) {
            return new ActivityColors {
                Base = GetColor(color),
                Dark = GetColor(color + "Dark"),
                Light = GetColor(color + "Light")
            };
        }

        public static ActivityColors ActivityBackgroundColors(string activityType) {
            try
            {
                return new ActivityColors {
                    Base = GetColor(activityType + "BaseColor"),
                    Dark = GetColor(activityType + "DarkColor"),
                    Light = GetColor(activityType + "LightColor")
                };
            }
            catch (Exception /*ex*/)
            {
                return new ActivityColors {
                    Base = GetColor("CyclingBaseColor"),
                    Dark = GetColor("CyclingDarkColor"),
                    Light = GetColor("CyclingLightColor")
                };

            }
        }

        public static Action<CGRect, UIColor> ActivityDrawingMethod(string activityType) {
            string key = "Draw" + ActivityTypeToStyleKitActivity(activityType);
            return GetDrawingMethod(key);
        }
    }
}

