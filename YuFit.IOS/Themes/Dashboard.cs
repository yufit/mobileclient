using UIKit;

namespace YuFit.IOS.Themes
{
    public static class Dashboard
    {
        public static class CreateFitActivityButton {
            public static UIColor BackgroundColor = YuFitStyleKit.Black;
            public static UIColor PlusBackgroundColor = YuFitStyleKit.Yellow;
            public static UIColor PlusForegroundColor = YuFitStyleKit.White;

        }

        public static class SummaryView {
            public static class InvitesButton {
                public static UIFont CountLabelTextFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 32);
                public static UIFont OtherLabelTextFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 20);
            }

            public static class JoinButton {
                public static UIFont TextFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 24);
            }

            public static class SpeedLabels {
                public static UIFont SpeedTextFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 22);
                public static UIFont SpeedUnitTextFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 14);
            }

            public static UIFont SliderTextFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 18);
            public static float SliderTextOffset = 2.5f;
        }


        public static UIColor HomeIconColor = YuFitStyleKit.White;

        public static class FilterView {
            
            public static UIColor ViewBackgroundColor = UIColor.Clear;
            public static UIColor LabelTextColor = YuFitStyleKit.LightGray;
            public static UIFont LabelTextFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
            public static UIFont SliderTextFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 20);
            public static float SliderFontOffset = 2.5f;
            public static UIColor SliderColor = YuFitStyleKit.Yellow;
        }

    }
}

