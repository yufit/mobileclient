﻿using System;
using UIKit;

namespace YuFit.IOS.Themes
{
    public static class Invitations
    {
        public static class WhenView
        {
            public static UIColor TextColor = YuFitStyleKit.White;
            public static UIFont DayLabelTextFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 20);
            public static UIFont MonthAndDayLabelTextFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 14);
            public static UIFont TimeLabelTextFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 14);
        }

        public static class ViewInvitations
        {
            public static UIColor AcceptInvitationButtonColor = YuFitStyleKit.LightGray;
            public static UIColor IgnoreInvitationButtonColor = YuFitStyleKit.Red;
            public static UIColor ButtonTextColor = YuFitStyleKit.White;

            public static UIColor FilterControlColor = YuFitStyleKit.Yellow;
            public static UIFont FilterControlFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 18);

        }
    }
}

