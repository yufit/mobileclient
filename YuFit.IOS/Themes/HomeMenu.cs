using System;
using UIKit;

namespace YuFit.IOS.Themes
{
    public static class HomeMenu
    {
        public static UIColor ViewBackgroundColor = YuFitStyleKit.Black;
        public static UIColor ViewBackgroundColorAlpha = YuFitStyleKit.Black;
        public static UIColor LabelTextColor = YuFitStyleKit.LightGray;
        public static UIFont LabelTextFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
        public static UIFont NextTrainingTimeFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 32);
        public static UIFont NextTrainingDateFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
        public static UIColor NextTrainingColor = YuFitStyleKit.Green;
        public static UIFont NameFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 34);
        public static UIColor NameTextColor = YuFitStyleKit.White;

        public static class MenuItems
        {
            public static UIColor TextColor = YuFitStyleKit.LightGray;
            public static UIFont TextFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 24);
            public static UIColor SeparatorColor = YuFitStyleKit.Yellow;
        }
    }
}

