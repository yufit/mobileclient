using System;
using UIKit;

namespace YuFit.IOS.Themes
{
    public static class CrudFitEvent
    {
        public static UIColor ViewBackgroundColor = YuFitStyleKit.Charcoal;
        public static UIColor ViewBackgroundColorAlpha = YuFitStyleKit.Charcoal;
        public static UIColor LabelTextColor = YuFitStyleKit.LightGray;
        public static UIFont LabelTextFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
        public static UIFont SliderTextFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 20);

        public static class EmptyDiamond {
            public static UIColor BackgroundColor = YuFitStyleKit.Charcoal;
            public static UIColor StrokeColor = YuFitStyleKit.Black;
        }

        public static class WhenDiamond {
            public static UIColor TextColor = YuFitStyleKit.White;
            public static UIFont DayLabelTextFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 24);
            public static UIFont MonthAndDayLabelTextFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            public static UIFont TimeLabelTextFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
        }

        public static class EmptyDiamondCycling {
            public static UIColor BackgroundColor  = YuFitStyleKit.Charcoal;
        }

        public static class PlusSign {
            public static UIColor BackgroundColor = YuFitStyleKit.Black; 
            public static UIColor PlusColor = YuFitStyleKit.White;
        }

        public static class ItsOnView {
            public static UIColor TextColor = YuFitStyleKit.White;
            public static UIFont TextFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 18);
            public static UIColor TriangleBackgroundColor = YuFitStyleKit.YellowDark;
            public static UIColor TriangleFillColor = YuFitStyleKit.Yellow;
        }

        public static class ActivityTimeView {
            public static float DayFontOffset = 2.5f;
            public static float DayNameFontOffset = 2.5f;
            public static float MonthFontOffset = 2.5f;
            public static float TimeFontOffset = 10f;

            public static UIFont WeekendFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 22);           
            public static UIFont WeekDayFont = UIFont.FromName("HelveticaNeueLTStd-ThCn", 22);
            public static UIFont MonthFont = UIFont.FromName("HelveticaNeueLTStd-ThCn", 22);
            public static UIFont DayNameFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 22);
            public static UIFont SelectionFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 22);
            public static UIFont TimeLabelFont = UIFont.FromName("HelveticaNeueLTStd-ThCn", 22);
            public static UIFont TimeFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 60);
            public static UIFont AmPmFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 30);
            public static UIFont FrequencyFont = UIFont.FromName("HelveticaNeueLTStd-ThCn", 18);
        }

        public static class ConfigView {

            public static UIFont SliderFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 22);
            public static float SliderFontOffset = 2.5f;

            public static UIFont LabelTextFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 18);
            public static UIColor LabelTextColor = YuFitStyleKit.White;

            public static UIFont SpeedValueFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 28);
            public static UIColor SpeedValueColor = YuFitStyleKit.Yellow;

            public static UIFont SpeedUnitFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 14);
            public static UIColor SpeedUnitColor = YuFitStyleKit.White;

            public static UIFont IntensityFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 20);
            public static UIColor IntensityColor = YuFitStyleKit.White;

        }

        public static class ActivitySummaryControl { 

            public static UIFont SubSportFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 20);
            public static UIColor SubSportColor = YuFitStyleKit.White;

            public static UIFont AddressFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            public static UIColor AddressColor = YuFitStyleKit.White;

            public static UIFont CreatedByFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            public static UIColor CreatedByColor = YuFitStyleKit.White;

            public static UIFont CreatorFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            public static UIColor CreatorColor = YuFitStyleKit.Charcoal;

            public static UIFont DetailsFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            public static UIColor DetailsColor = YuFitStyleKit.White;
        }


    }

    public static class CancelledActivity
    {
        public static UIFont CancelTextFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 72);
        public static UIColor CancelTextColor = YuFitStyleKit.Black;
        public static UIColor InsetColor = YuFitStyleKit.RedDark;
        public static UIColor CancelledFrameColor = YuFitStyleKit.RedLight;
        public static nfloat CancelledFrameAlpha = .7f;

    }
}

