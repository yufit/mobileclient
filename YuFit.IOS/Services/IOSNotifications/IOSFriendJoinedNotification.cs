﻿using System;
using YuFit.Core.Services.Notifications;
using YuFit.IOS.Interfaces;
using UIKit;
using YuFit.Core.Interfaces.Services;
using MvvmCross.Platform;

namespace YuFit.IOS.Services.IOSNotifications
{
    public class IOSFriendJoinedNotification : FriendJoinedNotification, IIOSPushNotificationRegistration
    {
        const string inviteCategoryIdent    = @"friend_joined";
        const string inviteJoinAction       = @"AddFriend";
        const string inviteDismissAction    = @"Dismiss";

        public UIMutableUserNotificationCategory BuildIOSNotificationCategory()
        {
            UIMutableUserNotificationAction action1 = new UIMutableUserNotificationAction();
            action1.ActivationMode = UIUserNotificationActivationMode.Background;
            action1.Title = Mvx.Resolve<IStringLoaderService>().GetString("Add as friend");
            action1.Identifier = inviteJoinAction;
            action1.Destructive = false;
            action1.AuthenticationRequired = false;

            UIMutableUserNotificationAction action2 = new UIMutableUserNotificationAction();
            action2.ActivationMode = UIUserNotificationActivationMode.Background;
            action2.Title = Mvx.Resolve<IStringLoaderService>().GetString("Dismiss");
            action2.Identifier = inviteDismissAction;
            action2.Destructive = false;
            action2.AuthenticationRequired = false;

            UIMutableUserNotificationCategory actionCategory = new UIMutableUserNotificationCategory();
            actionCategory.Identifier = inviteCategoryIdent;
            actionCategory.SetActions(new [] {action1, action2}, UIUserNotificationActionContext.Default);

            return actionCategory;
        }
            }
}

