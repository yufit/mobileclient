﻿using YuFit.Core.Services.Notifications;

namespace YuFit.IOS.Services.IOSNotifications
{
    public static class IOSNotificationFactory
    {
        public static BasePushNotification Construct(string category)
        {
            return Construct(PushNotificationCategoryEx.GetValueFromStringValue(category));
        }

        public static BasePushNotification Construct(PushNotificationCategory category)
        {
            switch(category)
            {
                case PushNotificationCategory.Invitation:           return new IOSInvitedToEventNotification();
                case PushNotificationCategory.UserJoined:           return new UserJoinEventNotification();
                case PushNotificationCategory.UserLeft:             return new UserLeftEventNotification();
                case PushNotificationCategory.EventCommentPosted:   return new EventCommentPostedNotification();
                case PushNotificationCategory.EventModified:        return new EventModifiedNotification();
                case PushNotificationCategory.EventCancelled:       return new EventCanceledNotification();
                case PushNotificationCategory.EventPromoted:        return new EventPromotedNotification();
                case PushNotificationCategory.GeneralMessage:       return new GeneralMessageNotification();
                case PushNotificationCategory.UserJoinedGroup:      return new UserJoinedGroupNotification();
                case PushNotificationCategory.UserInvitedToGroup:   return new IOSInvitedToGroupNotification();
                case PushNotificationCategory.FriendAdded:          return new FriendAddedNotification();
                case PushNotificationCategory.FriendJoined:         return new IOSFriendJoinedNotification();
                default: return null;
            }
        }
    }
}

