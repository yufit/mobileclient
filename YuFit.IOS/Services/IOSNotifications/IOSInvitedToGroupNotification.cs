﻿
using UIKit;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Services.Notifications;

using YuFit.IOS.Interfaces;
using MvvmCross.Platform;

namespace YuFit.IOS.Services.IOSNotifications
{
    public class IOSInvitedToGroupNotification : UserInvitedToGroupNotification, IIOSPushNotificationRegistration
    {
        const string inviteCategoryIdent    = @"user_invited_to_group";

        public UIMutableUserNotificationCategory BuildIOSNotificationCategory()
        {
            UIMutableUserNotificationAction action1 = new UIMutableUserNotificationAction();
            action1.ActivationMode = UIUserNotificationActivationMode.Background;
            action1.Title = Mvx.Resolve<IStringLoaderService>().GetString("push.notifications.join.label");
            action1.Identifier = InviteJoinAction;
            action1.Destructive = false;
            action1.AuthenticationRequired = false;

            UIMutableUserNotificationAction action2 = new UIMutableUserNotificationAction();
            action2.ActivationMode = UIUserNotificationActivationMode.Background;
            action2.Title = Mvx.Resolve<IStringLoaderService>().GetString("push.notifications.dismiss.label");
            action2.Identifier = InviteDismissAction;
            action2.Destructive = false;
            action2.AuthenticationRequired = false;

            UIMutableUserNotificationCategory actionCategory = new UIMutableUserNotificationCategory();
            actionCategory.Identifier = inviteCategoryIdent;
            actionCategory.SetActions(new [] {action1, action2}, UIUserNotificationActionContext.Default);

            return actionCategory;
        }
    }
}

