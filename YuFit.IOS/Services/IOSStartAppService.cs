﻿using System;
using YuFit.Core.Interfaces.Services;
using UIKit;

namespace YuFit.IOS.Services
{
    public class IOSStartAppService : IStartAppService
    {
        #region IStartAppService implementation
        public void OpenUrl (Uri uri)
        {
            UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl(uri.AbsoluteUri));
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.google.com"]];
        }
        #endregion
    }
}

