using YuFit.Core.Interfaces.Services;
using Foundation;

namespace YuFit.IOS.Services
{
	public class IOSStringLoaderService : IStringLoaderService
	{
		#region IStringLoaderService implementation
		public string GetString(string baseString)
		{
            return NSBundle.MainBundle.LocalizedString(baseString, string.Empty);
		}
        public string GetString(string baseString, string table)
        {
            return NSBundle.MainBundle.LocalizedString(baseString, string.Empty, table);
        }
		#endregion
	}
}

