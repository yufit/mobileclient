﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Foundation;
using UIKit;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.Services;
using YuFit.Core.Services.Notifications;

using YuFit.IOS.Interfaces;
using YuFit.IOS.Services.IOSNotifications;
using YuFit.WebServices.Interface.Model;
using MvvmCross.Plugins.Messenger;
using MvvmCross.Platform;

namespace YuFit.IOS.Services
{
    public class IOSPushNotificationService : PushNotificationService
    {
//        const string inviteCategoryIdent        = @"invitation";
//        const string userLeftCategoryIdent      = @"user_left_event";
//        const string userJoinCategoryIdent      = @"user_joined_event";
//        const string eventModifiedCategoryIdent = @"event_modified";
//        const string eventCanceledCategoryIdent = @"event_canceled";
//        const string generalMessageCategoryIdent= @"general_message";
//
//        const string inviteJoinAction = @"Join";
//        const string inviteDismissAction = @"Dismiss";

        protected IMvxMessenger MessengerService { get; private set; }

        public IOSPushNotificationService(IMvxMessenger messenger)
        {
            MessengerService = messenger;
        }

        #region IPushNotificationService implementation

        public new string ActivityIdToDisplay { 
            get { return Mvx.Resolve<ISandboxService>().GetItem<string>("ActivityIdToDisplay"); } 
            set { Mvx.Resolve<ISandboxService>().SetItem<string>("ActivityIdToDisplay", value); } 
        }

        public override void SetupNotificationsHandler ()
        {
            var instances = from t in Assembly.GetExecutingAssembly().GetTypes()
                            where t.GetInterfaces().Contains(typeof(IIOSPushNotificationRegistration)) && t.GetConstructor(Type.EmptyTypes) != null
                            select Activator.CreateInstance(t) as IIOSPushNotificationRegistration;

            var notificationCategories = new List<UIMutableUserNotificationCategory>();
            foreach (var instance in instances)
            {
                notificationCategories.Add(instance.BuildIOSNotificationCategory());
            }

            const UIUserNotificationType NotificationTypes = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;
            UIUserNotificationSettings notifSettings = UIUserNotificationSettings.GetSettingsForTypes(NotificationTypes, new NSSet(notificationCategories.ToArray()));
            UIApplication.SharedApplication.RegisterUserNotificationSettings(notifSettings);
            UIApplication.SharedApplication.RegisterForRemoteNotifications();
        }

        #endregion

        public void RegisterForRemoteNotifications (UIApplication application, NSData deviceToken)
        {
            NSString newDeviceToken = (NSString) deviceToken.ToString();
            Console.WriteLine(string.Format(">>>>>>>>>>>>>>>>>> {0}", newDeviceToken));
            Mvx.Resolve<IDeviceTrackerService>().SetNotificationToken(DeviceOs.IOS, newDeviceToken);

//            IUserService userSvc = Mvx.Resolve<IUserService>();
//            DeviceInformation devInfo = new DeviceInformation();
//            devInfo.DeviceOs = YuFit.WebServices.Interface.Model.DeviceOs.IOS;
//            devInfo.NotificationToken = newDeviceToken;
//            userSvc.DeviceInfo = devInfo;
        }

        /// <summary>
        /// Receives the remote notification.  This gets called in 2 scenarios:
        ///     1) When the user tap in the notification outside the app.
        ///     2) When the app is running and the notification comes up.
        /// </summary>
        /// <param name="application">Application.</param>
        /// <param name="userInfo">User info.</param>
        /// <param name="completionHandler">Completion handler.</param>
        public void ReceiveRemoteNotification (UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            var notification = CreatePlatformNotificationFromIOSPush(userInfo);
            if (notification != null) { 
                notification.HandleReceivedNotificationWhileRunning(result =>
                {
                    switch (result)
                    {
                        case PushNotificationResult.Failed:  completionHandler(UIBackgroundFetchResult.Failed ); break;
                        case PushNotificationResult.NewData: completionHandler(UIBackgroundFetchResult.NewData); break;
                        case PushNotificationResult.NoData:  completionHandler(UIBackgroundFetchResult.NoData ); break;
                    }
                }); 
            }
        }

        /// <summary>
        /// Handles the incoming notification on app start.
        /// </summary>
        /// <param name="launchOptions">Launch options.</param>
        public void HandleIncomingNotificationOnAppStart (NSDictionary launchOptions)
        {
            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
            if (launchOptions == null) { return; }

            Console.WriteLine("HandleIncomingNotificationOnAppStart");

            try
            {
                NSObject result;
                if (!launchOptions.TryGetValue(UIApplication.LaunchOptionsRemoteNotificationKey, out result))
                {
                    return;
                }
                Console.WriteLine("++++++++++++++++++++++++ HandleIncomingNotificationOnAppStart got values");

                NSDictionary remoteNotification = launchOptions[UIApplication.LaunchOptionsRemoteNotificationKey] as NSDictionary;
                if (remoteNotification == null) { return; }

                Console.WriteLine("++++++++++++++++++++++++ Got notifications got values");

                var notification = CreatePlatformNotificationFromIOSPush(remoteNotification);
                if (notification != null) { 
                    Console.WriteLine("++++++++++++++++++++++++ Got notifications converted and handling it");
                    notification.HandleReceivedNotificationWhileStarting();
                }
            }
            // Analysis disable once EmptyGeneralCatchClause
            catch (Exception /*e*/)
            {
            }

        }

        /// <summary>
        /// Handles the background notification action.  This is called when the user select an option
        /// in the ios notification outside the app.
        /// </summary>
        /// <param name="application">Application.</param>
        /// <param name="actionIdentifier">Action identifier.</param>
        /// <param name="remoteNotificationInfo">Remote notification info.</param>
        /// <param name="completionHandler">Completion handler.</param>
        public void HandleBackgroundNotificationAction (UIApplication application, string actionIdentifier, NSDictionary remoteNotificationInfo, Action completionHandler)
        {
            var notification = CreatePlatformNotificationFromIOSPush(remoteNotificationInfo);
            if (notification != null) {
                notification.HandleNotificationAction(actionIdentifier, completionHandler);
            }
        }

        //------------------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------------------
        private BasePushNotification CreatePlatformNotificationFromIOSPush(NSDictionary notificationData)
        {
            if (notificationData == null) {
                Mvx.Trace("Invalid push received - no dictionary info");
                return null;
            }

            try {
                var pushData = NSDictionaryToDynamic(notificationData);

                var aps = pushData["aps"];
                var info = aps["info"];
                var category = aps["category"].ToString();

                var alert_values = (JArray) aps["alert"]["loc-args"];
                var alert_key = aps["alert"]["loc-key"].ToString();

                var badge = aps["badge"];

                var alertInformation = new AlertInformation {
                    Values = alert_values.Select(jv => (string)jv).ToArray(),
                    Key = alert_key
                };

                var notification = CreateNotificationForCategory(category, alertInformation, info);
                if (badge != null) {
                    notification.UpdatedBadgeCount = (int) badge.Value;
                }
                return notification;
            } catch (Exception /*ex*/) {
                // TODO LOG ERROR.
                return null;
            }
        }

        protected override IPushNotification CreatePlatformNotificationFromNotificationRecord(NotificationRecord notification)
        {
            try {
                var notificationData = notification.Data;

                var info = notificationData["info"];
                var category = notificationData["category_name"].ToString();

                var alert_values = (JArray) notificationData["msg_values"];
                var alert_key = notificationData["msg_key"].ToString();

                var alertInformation = new AlertInformation {
                    Values = alert_values.Select(jv => (string)jv).ToArray(),
                    Key = alert_key
                };

                return CreateNotificationForCategory(category, alertInformation, info);
            } catch (Exception /*ex*/) {
                // TODO LOG ERROR.
                return null;
            }

        }

        protected BasePushNotification CreateNotificationForCategory(string category, AlertInformation alertInformation, dynamic info)
        {
            BasePushNotification pushNotification = IOSNotificationFactory.Construct(category);
            if (pushNotification != null)
            {
                pushNotification.AlertInformation = alertInformation;
                pushNotification.Info = info;

                UIApplicationState state = UIApplication.SharedApplication.ApplicationState;
                pushNotification.AppIsInForeground = !(state == UIApplicationState.Inactive || state == UIApplicationState.Background);
            }

            return pushNotification;
        }

        //------------------------------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------------------------
        private dynamic NSDictionaryToDynamic(NSDictionary apsDictionary)
        {
            if (apsDictionary == null) { return null; }

            NSError error;
            NSData jsonData = NSJsonSerialization.Serialize(apsDictionary, 0,  out error);
            if (jsonData == null || error != null) { return null; }

            string dictString = jsonData.ToString();
            dynamic dict = JsonConvert.DeserializeObject<dynamic>(dictString);

            return dict;
        }


    }

//"aps": {
//    "alert": {
//        "loc-key": "USER_INVITED_TO_EVENT",
//        "loc-args": [
//            "PatBoneCrusher",
//            "Running - Street",
//            "Perinton",
//            "NY"
//        ]
//    },
//    "sound": "default",
//    "category": "invitation",
//    "info": {
//        "event_id": "556fafcc69702d4c4d1d0000",
//        "sport": "running"
//    }
//}


//"data": {
//    "info": {
//        "event_id": "55db9c04676f74103c000000",
//        "sport": "Spinning",
//        "sub_sport": "Spinning"
//    },
//    "msg_key": "USER_INVITED_TO_EVENT",
//    "category_name": "invitation",
//    "default_message": "You have been invited to Mendon Spinning in Mendon, NY.",
//    "msg_values": [
//        "Me",
//        "Mendon Spinning",
//        "Mendon",
//        "NY"
//    ]
//},




}

