﻿using System;
using YuFit.Core.Interfaces.Services;
using System.Threading.Tasks;
using System.IO;
using Coc.ImagePicker.Touch;
using CoreGraphics;
using UIKit;
using Foundation;
using System.Runtime.InteropServices;
using MvvmCross.Platform.iOS.Views;
using MvvmCross.Platform;

namespace YuFit.IOS.Services
{
    public class IOSImagePickerService : IImagePickerService, ImagePickerDelegate
    {
        private readonly IMvxIosModalHost _modalHost;
        private ImagePicker _imagePicker;

        private Action<Stream> _pictureAvailable;
        private Action _assumeCancelled;
        //private int _percentQuality;

        private string _uiHint;

        public IOSImagePickerService ()
        {
            _modalHost = Mvx.Resolve<IMvxIosModalHost>();
        }

        #region IImagePickerService implementation

        public void ChoosePictureFromLibrary(int percentQuality, object uiHint, Action<Stream> pictureAvailable, Action assumeCancelled)
        {
            //_picker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            ChoosePictureCommon(percentQuality, uiHint, pictureAvailable, assumeCancelled);
        }

        public void TakePicture(int percentQuality, object uiHint, Action<Stream> pictureAvailable, Action assumeCancelled)
        {
            //_picker.SourceType = UIImagePickerControllerSourceType.Camera;
            ChoosePictureCommon(percentQuality, uiHint, pictureAvailable, assumeCancelled);
        }

        public Task<Stream> ChoosePictureFromLibrary (object uiHint)
        {
            var task = new TaskCompletionSource<Stream>();
            ChoosePictureFromLibrary(90, uiHint, task.SetResult, () => task.SetResult(null));
            return task.Task;
        }

        public Task<Stream> TakePicture (object uiHint)
        {
            var task = new TaskCompletionSource<Stream>();
            TakePicture(90, uiHint, task.SetResult, () => task.SetResult(null));
            return task.Task;
        }

        #endregion

        private void ChoosePictureCommon(int percentQuality, object uiHint, Action<Stream> pictureAvailable, Action assumeCancelled)
        {
            _uiHint = (string) uiHint;
            if (string.IsNullOrEmpty(_uiHint)) { return; }

            //_percentQuality = percentQuality;
            _pictureAvailable = pictureAvailable;
            _assumeCancelled = assumeCancelled;

            ImageCropOverlayView icov = new ImageCropOverlayView();
            if (_uiHint == "groupPhoto") {
                var width = UIScreen.MainScreen.Bounds.Width;
                var height = width * 0.5531401f;

//                var icov = new ImageCropOverlayView();
                icov.CropSize = new CGSize(width, height);

            } else if (_uiHint == "avatar") {
                icov = new ImageDiamondCropOverlayView();
                icov.CropSize = new CGSize(225, 225);
            }

            _imagePicker = new ImagePicker(icov);
            _imagePicker.Delegate = this;

            _modalHost.PresentModalViewController(_imagePicker.ImagePickerController, true);
        }

        #region ImagePickerDelegate implementation

        public void ImagePicked (ImagePicker picker, UIImage pickedImage)
        {
            var resizedImage = pickedImage;
            if (_uiHint == "groupPhoto") {
                UIGraphics.BeginImageContext(new CGSize(828, 458));
                pickedImage.Draw(new CGRect(0, 0, 828, 458));
                resizedImage = UIGraphics.GetImageFromCurrentImageContext();;
                UIGraphics.EndImageContext();
            } else if (_uiHint == "avatar") {
                UIGraphics.BeginImageContext(new CGSize(512, 512));
                pickedImage.Draw(new CGRect(0, 0, 512, 512));
                resizedImage = UIGraphics.GetImageFromCurrentImageContext();;
                UIGraphics.EndImageContext();
            }

            if (resizedImage != null) {
                using (NSData data = resizedImage.AsJPEG(80f / 100f))
                {
                    var byteArray = new byte[data.Length];
                    Marshal.Copy(data.Bytes, byteArray, 0, Convert.ToInt32(data.Length));

                    var imageStream = new MemoryStream(byteArray, false);
                    if (_pictureAvailable != null)
                        _pictureAvailable(imageStream);
                }
            } else {
                if (_assumeCancelled != null) {
                    _assumeCancelled();
                }
            }

            _imagePicker.DismissController();
            _imagePicker.Delegate = null;
            _modalHost.NativeModalViewControllerDisappearedOnItsOwn();

            _pictureAvailable = null;
            _assumeCancelled = null;

        }

        public void ImagePickedDidCancel (ImagePicker picker)
        {
            _imagePicker.DismissController();
            _imagePicker.Delegate = null;
            _modalHost.NativeModalViewControllerDisappearedOnItsOwn();
        }

        #endregion

    }
}

