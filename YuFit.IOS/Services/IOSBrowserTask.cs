﻿using System;
using YuFit.Core.Interfaces.Services;
using UIKit;

namespace YuFit.IOS.Services
{
    public class IOSBrowserTask : IBrowserTask
    {
        public IOSBrowserTask ()
        {
        }

        #region IBrowserTask implementation

        public void ShowWebPage (string url)
        {
            UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl(url));
        }

        #endregion
    }
}

