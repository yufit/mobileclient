﻿using System;
using YuFit.Core.Interfaces.Services;
using UIKit;

namespace YuFit.IOS.Services
{
    public class IOSNetworkMonitoringService : INetworkMonitoringService
    {
        #region INetworkMonitoringService implementation

        public void NetworkIsGoingActive ()
        {
            UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
        }

        public void NetworkIsGoingIdle ()
        {
            UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
        }

        public void NetworkSeemsToBeSlow ()
        {
            throw new NotImplementedException();
        }

        public void NetworkSeemsToHaveRecovered ()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}

