using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using CoreLocation;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using System.Linq;
using Coc.MvvmCross.Plugins.Location;

namespace YuFit.IOS.Services
{
    public class IOSGeoLocationConverterService : IGeoLocationConverterService
    {
        #region IMvxGeoLocationConverter implementation

        public async Task<Placemark> ReverseGeocodeLocationAsync (CocLocation location, bool keepLocation = true)
        {
            return await ReverseGeocodeLocationAsync(
                new Coordinate(
                    location.Coordinates.Latitude, 
                    location.Coordinates.Longitude
                )
            );
        }

        public async Task<Placemark> ReverseGeocodeLocationAsync (Coordinate location, bool keepLocation = true)
        {
            CLLocation tmp = new CLLocation (location.Latitude, location.Longitude);
            CLPlacemark[] placemarks = await (new CLGeocoder()).ReverseGeocodeLocationAsync(tmp);
            var mvxPlacemark = CLPlacemarksToMvxPlacemarks(placemarks);
            if (keepLocation && mvxPlacemark.Count > 0) { 
                Placemark pl = mvxPlacemark[0];

                // Replace the returned coordinate with the one used for the search.  We want the place to be where
                // the pin was drop.  For instance, when dropping a pin in middle of lake ontario, the search will
                // return the lake ontario place but the pin will be somewhere else.
                pl.Coordinates = location;
            }
            return mvxPlacemark.FirstOrDefault();
        }

        public async Task<List<Placemark>> GeocodeAddressAsync (string address)
        {
            CLPlacemark[] placemarks = await (new CLGeocoder()).GeocodeAddressAsync(address);
            return CLPlacemarksToMvxPlacemarks(placemarks);
        }

        public async Task<List<Placemark>> GeocodeAddressAsync (string address, Region region)
        {
            CLLocationCoordinate2D coord = new CLLocationCoordinate2D(region.Center.Latitude, region.Center.Longitude);
            using (var clRegion = new CLCircularRegion(coord, region.Radius, region.Identifier))
            {
                CLPlacemark[] placemarks = await (new CLGeocoder()).GeocodeAddressAsync(address, clRegion);
                return CLPlacemarksToMvxPlacemarks(placemarks);
            }
        }

        List<Placemark> CLPlacemarksToMvxPlacemarks (CLPlacemark[] placemarks)
        {
            List<Placemark> result = new List<Placemark>();
            Array.ForEach(placemarks, pl => {
                // Analysis disable once ConvertToLambdaExpression
                result.Add(new Placemark {
                    SubThoroughfare         = pl.SubThoroughfare ?? string.Empty,
                    Thoroughfare            = pl.Thoroughfare ?? string.Empty,
                    Locality                = pl.Locality ?? string.Empty,
                    SubLocality             = pl.SubLocality ?? string.Empty,
                    PostalCode              = pl.PostalCode ?? string.Empty,
                    AdministrativeArea      = pl.AdministrativeArea ?? string.Empty,
                    SubAdministrativeArea   = pl.SubAdministrativeArea ?? string.Empty,
                    Country                 = pl.Country ?? string.Empty,
                    Coordinates             = new Coordinate { 
                        Latitude = pl.Location.Coordinate.Latitude, 
                        Longitude = pl.Location.Coordinate.Longitude }
                });
            });
            return result;
        }
        #endregion
    }
}

