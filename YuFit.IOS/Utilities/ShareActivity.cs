﻿using UIKit;
using Foundation;
using CoreGraphics;

namespace YuFit.IOS.Utilities
{
    public class ShareActivity
    {
        readonly UIViewController _parentController;
        readonly UIView _viewToShare;

        public ShareActivity (UIViewController parentController, UIView viewToShare)
        {
            _parentController = parentController;
            _viewToShare = viewToShare;
        }

        UIBarButtonItem leftItem;
        UIBarButtonItem rightItem;

        public void Present()
        {
            var screenshot = TakeSnapshotOfView(_viewToShare);

            // TODO EVENTUALLY ADD THE EVENT ID
            //NSUrl shareUrl = new NSUrl(@"https://fb.me/739255952862948?event=123");
            //NSUrl shareUrl = new NSUrl(@"https://fb.me/739255952862948");

            // BUG ALERT: THIS is a workaround sharing via email.... When doing
            // so, we must reset the nav bar tint color to black or you don't see
            // the send and cancel button.
            UIBarButtonItem.Appearance.TintColor = UIColor.Black;
            leftItem = _parentController.NavigationItem.LeftBarButtonItem;
            rightItem = _parentController.NavigationItem.RightBarButtonItem;

            var avc = new UIActivityViewController (new NSObject [] { new NSString(""), screenshot }, null);
            avc.ExcludedActivityTypes = new [] { UIActivityType.AirDrop, UIActivityType.AddToReadingList, UIActivityType.Print, UIActivityType.SaveToCameraRoll, UIActivityType.AssignToContact, UIActivityType.CopyToPasteboard };
            avc.SetCompletionHandler(HandleUIActivityViewControllerCompletion);
            avc.RestorationIdentifier = "Activity";
            _parentController.PresentViewController (avc, true, null);
        }

        void HandleUIActivityViewControllerCompletion (NSString activityType, bool completed, NSExtensionItem[] returnedItems, NSError error)
        {
            // BUG ALERT: THIS is a workaround sharing via email.... When doing
            // so, we must reset the nav bar tint color to black or you don't see
            // the send and cancel button.
            //
            // When coming back we must reset the item to white so that they display.
            // For some reason, setting the TintColor on appearance doesn't do it.
            // so we must readd everything.

            UIBarButtonItem.Appearance.TintColor = UIColor.White;

            if (leftItem != null) {
                leftItem.TintColor = Themes.Dashboard.HomeIconColor;
                _parentController.NavigationItem.LeftBarButtonItem = leftItem;
            }

            if (rightItem != null) {
                rightItem.TintColor = Themes.Dashboard.HomeIconColor;
                _parentController.NavigationItem.RightBarButtonItem = rightItem;
            }
        }

        private UIImage TakeSnapshotOfView(UIView viewToShare)
        {
            CGSize size = viewToShare.Frame.Size;
//            size.Width /= 2.0f;
//            size.Height /= 2.0f;

            UIGraphics.BeginImageContext(size);
            viewToShare.DrawViewHierarchy(new CGRect(0, 0, size.Width, size.Height), true);
            UIImage image = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            return image;
        }
    }
}

