using System;
using CoreGraphics;
using UIKit;

namespace YuFit.IOS.Utilities
{
    public class CGContextHelper : IDisposable
    {
        private readonly CGContext _context;
        private bool _disposed;

        public CGContextHelper(CGContext context) {
            _context = context;
            _context.SaveState();
        }

        public CGContextHelper() {
            _context = UIGraphics.GetCurrentContext();
            _context.SaveState();
        }

        #region IDisposable implementation

        public void Dispose ()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.RestoreState();
                }
            }
            _disposed = true;
        }

        #endregion


    }
}

