using MapKit;
using UIKit;
using YuFit.IOS.Controls.Annotations;

namespace YuFit.IOS.Utilities
{
    public class AnnotationViewHelper<T> where T : MapAnnotation 
    {
        /// <summary>
        /// Gets the view for annotation.
        /// </summary>
        /// <returns>The view for annotation.</returns>
        /// <param name="mapView">Map view.</param>
        /// <param name="annotation">Annotation.</param>
        public MKAnnotationView GetViewForAnnotation (MKMapView mapView, IMKAnnotation annotation)
        {
            var fitAnnotation = annotation as MapAnnotation;
            if (fitAnnotation == null) { return null; }

            // Analysis disable CanBeReplacedWithTryCastAndCheckForNull
            if (fitAnnotation is GroupFitActivityAnnotation) {
                return GetViewForGroupActivityAnnotation(mapView, fitAnnotation as GroupFitActivityAnnotation);
            } 

            if (fitAnnotation is EventAnnotation) {
                return GetViewForEventAnnotation(mapView, fitAnnotation as EventAnnotation);
            } 

            if (fitAnnotation is FitActivityAnnotation) {
                return GetViewForActivityAnnotation(mapView, fitAnnotation as FitActivityAnnotation);
            }
            // Analysis restore CanBeReplacedWithTryCastAndCheckForNull
            return null;
        }

        /// <summary>
        /// Gets the view for group activity annotation.
        /// </summary>
        /// <returns>The view for group activity annotation.</returns>
        /// <param name="mapView">Map view.</param>
        /// <param name="annotation">Annotation.</param>
        static MKAnnotationView GetViewForGroupActivityAnnotation (MKMapView mapView, GroupFitActivityAnnotation annotation)
        {
            const string annotationId = "GroupActivityPin";
            MKAnnotationView annotationView = mapView.DequeueReusableAnnotation(annotationId);

            if (annotationView == null) {
                annotationView = new GroupActivityAnnotationView (annotation, annotationId);
                annotationView.CanShowCallout = false;
            } else  {
                annotationView.Annotation = annotation;
            }

            ((GroupActivityAnnotationView)annotationView).DataContext = annotation.DataContext;
            return annotationView;
        }

        /// <summary>
        /// Gets the view for event annotation.
        /// </summary>
        /// <returns>The view for event annotation.</returns>
        /// <param name="mapView">Map view.</param>
        /// <param name="annotation">Annotation.</param>
        static MKAnnotationView GetViewForEventAnnotation (MKMapView mapView, EventAnnotation annotation)
        {
            const string annotationId = "EventPin";
            MKAnnotationView annotationView = mapView.DequeueReusableAnnotation(annotationId);

            if (annotationView == null) {
                annotationView = new EventAnnotationView (annotation, annotationId);
                annotationView.CanShowCallout = false;
            } else  {
                annotationView.Annotation = annotation;
            }

            ((EventAnnotationView)annotationView).DataContext = annotation.DataContext;
            return annotationView;
        }

        /// <summary>
        /// Gets the view for activity annotation.
        /// </summary>
        /// <returns>The view for activity annotation.</returns>
        /// <param name="mapView">Map view.</param>
        /// <param name="annotation">Annotation.</param>
        static MKAnnotationView GetViewForActivityAnnotation (MKMapView mapView, FitActivityAnnotation annotation)
        {
            const string annotationId = "ActivityPin";
            MKAnnotationView annotationView = mapView.DequeueReusableAnnotation(annotationId);

            if (annotationView == null) {
                annotationView = new ActivityAnnotationView (annotation, annotationId);
                annotationView.CanShowCallout = false;
            } else  {
                annotationView.Annotation = annotation;
            }

            ((ActivityAnnotationView)annotationView).DataContext = annotation.DataContext;
            return annotationView;
        }

        /// <summary>
        /// Dids the add annotation views.
        /// </summary>
        /// <param name="mapView">Map view.</param>
        /// <param name="views">Views.</param>
        public void DidAddAnnotationViews (MKMapView mapView, MKAnnotationView[] views)
        {
            foreach (MKAnnotationView av in views)
            {
                // Don't do the annotation drop if the annotation is the user location
                if (av.Annotation is MKUserLocation) { continue; }

                MKAnnotation annotation = (av.Annotation) as MKAnnotation;

                if (annotation != null) {
                    av.Layer.Opacity = 0.0f;
                    UIView.AnimateNotify(0.2f, () => { av.Layer.Opacity = 1.0f; }, finished => { });

                }
            }
        }

    }
}


// Reference coede to animate the pin dropping down from top of screen.
//                    // Get the map point for the annotation
//                    MKMapPoint point = MKMapPoint.FromCoordinate(annotation.Coordinate);
//
//                    // Make sure the current annotation is inside the visible map rect
//                    if (!mapView.VisibleMapRect.Contains(point)) { continue; }
//
//                    // we want the animation to end with the annotations actual frame (this is the frame on the map)
//                    CGRect endFrame = av.Frame;
//
//                    // Move annotation out of view
//                    //av.Frame = new CGRect(av.Frame.X, av.Frame.Y - mapView.Frame.Height, av.Frame.Width, av.Frame.Height);
//
//                    // we use the index of the annotation to change the timing for each of the annotations so they
//                    // dont all drop at once.  It looks better this way
//                    int index = Array.IndexOf(views, av); 
//
//                    // Animate the drop
//                    UIView.Animate(0.2, 0.04 * index, UIViewAnimationOptions.CurveLinear,
//                        () => { av.Frame = endFrame; }, 
//                        () => UIView.Animate(0.2, () => { av.Transform = CoreGraphics.CGAffineTransform.MakeScale(1.0f, 0.8f); }, 
//                            () => UIView.Animate(0.1, () => av.Transform = CoreGraphics.CGAffineTransform.MakeIdentity())));
