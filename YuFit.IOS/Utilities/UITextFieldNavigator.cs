using System;
using System.Collections.Generic;
using System.Linq;

using Cirrious.FluentLayouts.Touch;

using CoreGraphics;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Login;

using YuFit.IOS.Controls;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Utilities
{

    class UITextFieldNavigator
    {
        readonly List<UITextField> _views;

        public UITextFieldNavigator (List<UITextField> views)
        {
            _views = views;
        }

        public void AttachKeyboardMonitoring ()
        {
            _views.Each((v, index) => {
                v.ShouldReturn = new UITextFieldCondition(delegate
                {
                    if (index + 1 < _views.Count)
                    {
                        UITextField next = _views.ElementAt(index + 1);
                        if (next != null) { next.BecomeFirstResponder(); }
                        return true;
                    }

                    v.ResignFirstResponder();
                    return true;
                });

            });
        }

        public void DetachKeyboardMonitoring ()
        {
            _views.Each((v, index) => { v.ShouldReturn = null; });
        }
    }
    
}
