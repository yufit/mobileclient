using System;

namespace YuFit.IOS.Attributes
{
	/// <summary>
	/// Push transition attribute.  Specify what transition the navcontroller will use when pushing 
	/// the view controller using this attribute.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	public class PushTransitionAttribute : CustomTransitionAttribute
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="PushTransitionAttribute"/> class.
		/// </summary>
        /// <param name="transitionName">Transition type.</param>
        public PushTransitionAttribute(Type transitionName) : base(transitionName)
		{
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents the current <see cref="PushTransitionAttribute"/>.
		/// </summary>
		/// <returns>A <see cref="System.String"/> that represents the current <see cref="PushTransitionAttribute"/>.</returns>
		public override string ToString()
		{
			string value = "Push transition: " + Transition;
			return value;
		}
	}
}

