using System;

namespace YuFit.IOS.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class PresentViewInContainerAttribute : Attribute
    {
        public PresentViewInContainerAttribute (string containerID) {
            ContainerID = containerID;
        }

        public string ContainerID { get; private set; }
    }
}

