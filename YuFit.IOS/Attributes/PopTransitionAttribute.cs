using System;

namespace YuFit.IOS.Attributes
{
	/// <summary>
	/// Pop transition attribute.  Specify what transition the navcontroller will use when popping 
	/// the view controller using this attribute.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	public class PopTransitionAttribute : CustomTransitionAttribute
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="PopTransitionAttribute"/> class.
		/// </summary>
        /// <param name="transitionName">Transition type.</param>
        public PopTransitionAttribute(Type transitionName) : base(transitionName)
		{
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents the current <see cref="PopTransitionAttribute"/>.
		/// </summary>
		/// <returns>A <see cref="System.String"/> that represents the current <see cref="PopTransitionAttribute"/>.</returns>
		public override string ToString()
		{
            string value = "Push transition: " + Transition;
			return value;
		}
	}
}

