using System;
using YuFit.IOS.Presenters;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Attributes
{
    public abstract class ViewCreationAttribute : Attribute
    {
        public abstract IMvxIosView CreateView();
    }
}

