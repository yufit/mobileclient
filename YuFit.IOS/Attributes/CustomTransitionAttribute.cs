using System;

namespace YuFit.IOS.Attributes
{
	/// <summary>
	/// Push transition attribute.  Specify what transition the navcontroller will use when pushing 
	/// the view controller using this attribute.
	/// </summary>
	public class CustomTransitionAttribute : Attribute
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="PushTransitionAttribute"/> class.
		/// </summary>
        /// <param name="transitionType">Transition type.</param>
		public CustomTransitionAttribute(Type transitionType)
		{
            Transition = transitionType;
		}

		/// <summary>
		/// This property is readonly (it has no set accessor)
		/// so it cannot be used as a named argument to this attribute.
		/// </summary>
		public Type Transition { get; private set; }

		/// <summary>
		/// The duration of the transition.
		/// </summary>
		public float Duration;

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents the current <see cref="PushTransitionAttribute"/>.
		/// </summary>
		/// <returns>A <see cref="System.String"/> that represents the current <see cref="PushTransitionAttribute"/>.</returns>
		public override string ToString()
		{
            string value = "Transition: " + Transition.ToString();
			return value;
		}
	}
}

