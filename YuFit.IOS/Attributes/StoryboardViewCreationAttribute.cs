using System;
using UIKit;
using YuFit.IOS.Presenters;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Attributes
{
    public class StoryboardViewCreationAttribute : ViewCreationAttribute
    {
        protected string ViewName { get; private set; }
        public string XibFileName;

        public StoryboardViewCreationAttribute(string viewName)
        {
            ViewName = viewName;
        }

        public override IMvxIosView CreateView()
        {
            return (IMvxIosView) UIStoryboard.FromName(XibFileName, null).InstantiateViewController(ViewName);
        }


    }
}

