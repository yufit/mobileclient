﻿using System;

namespace YuFit.IOS.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class RaisePropertyChangedAttribute : HandlerAttribute
    {
        public override ICallHandler CreateHandler(IUnityContainer container)
        {
            return new RaisePropertyChangedCallHandler();
        }
    }
}

