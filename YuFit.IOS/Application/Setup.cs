
using MapKit;
using PHFComposeBarView;
using UIKit;

using YuFit.Core.Interfaces.Services;

using YuFit.IOS.Controls;
using YuFit.IOS.CustomBindings;
using YuFit.IOS.Extensions;
using YuFit.IOS.Services;
using YuFit.IOS.Views.Dashboard;
using YuFit.IOS.Controls.Annotations;
using YuFit.IOS.Interfaces;
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.Platform;
using MvvmCross.iOS.Views;
using MvvmCross.Platform;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Bindings.Target.Construction;

namespace YuFit.IOS.Application
{
    public class Setup : MvxIosSetup
    {
        public Setup(MvxApplicationDelegate applicationDelegate, UIWindow window)
            : base(applicationDelegate, window)
        {
        }

        public Setup(MvxApplicationDelegate applicationDelegate, MvxBaseIosViewPresenter presenter)
            : base(applicationDelegate, presenter)
        {
        }

        protected override IMvxApplication CreateApp ()
        {
            return new Core.App();
        }

        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override IMvxIosViewsContainer CreateIosViewsContainer ()
        {
            return new YuFit.IOS.Presenters.ViewsContainer();
        }

        protected override void InitializeFirstChance()
        {
            base.InitializeFirstChance();
            Mvx.LazyConstructAndRegisterSingleton<IImagePickerService, IOSImagePickerService>();
            Mvx.LazyConstructAndRegisterSingleton<IGeoLocationConverterService, IOSGeoLocationConverterService>();
            Mvx.LazyConstructAndRegisterSingleton<IPushNotificationService, IOSPushNotificationService>();
            Mvx.LazyConstructAndRegisterSingleton<IStartAppService, IOSStartAppService>();
            Mvx.LazyConstructAndRegisterSingleton<IStringLoaderService, IOSStringLoaderService>();
            Mvx.LazyConstructAndRegisterSingleton<IBrowserTask, IOSBrowserTask>();
            Mvx.LazyConstructAndRegisterSingleton<INetworkMonitoringService, IOSNetworkMonitoringService>();
        }

        protected override void FillBindingNames(IMvxBindingNameRegistry obj)
        {
            base.FillBindingNames(obj);
            BehaviorExtensions.RegisterExtension(obj);
        }

        protected override void FillTargetFactories (IMvxTargetBindingFactoryRegistry registry)
        {
            base.FillTargetFactories(registry);
            registry.RegisterCustomBindingFactory<UISearchBar>          ("SearchButtonClicked", view => new MvxUISearchBarSearchCommandTargetBinding(view));
            registry.RegisterCustomBindingFactory<ComposeBarView>       ("DidPressButton",      view => new MvxComposeBarViewCommandTargetBinding(view));
            registry.RegisterCustomBindingFactory<MKMapView>            ("Region",              view => new MvxMKMapViewRegionTargetBinding(view));
            registry.RegisterCustomBindingFactory<ActivityIconView>     ("DrawIcon",            view => new ActivityIconViewTargetBindings(view));
            registry.RegisterCustomBindingFactory<ActivityIconDiamondView>("Activity",          view => new ActivityIconDiamondViewTargetBindings(view));
            registry.RegisterCustomBindingFactory<IconView>             ("DrawIcon",            view => new IconViewTargetBindings(view));
            registry.RegisterCustomBindingFactory<UIView>               ("BackgroundColor",     view => new BackgroundColorTargetBindings(view));
            registry.RegisterCustomBindingFactory<UIView>               ("DarkBackgroundColor", view => new DarkBackgroundColorTargetBindings(view));
            registry.RegisterCustomBindingFactory<MapAnnotation>        ("Coordinate",          annotation => new MvxCoordinateFitActivityAnnotationTargetBinding(annotation));

            registry.RegisterPropertyInfoBindingFactory(typeof(MvxUISegmentedControlSelectedSegmentTargetBinding), typeof(UISegmentedControl), "SelectedSegment");
        }
	}
}