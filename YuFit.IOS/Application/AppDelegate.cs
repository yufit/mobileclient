using System.Net;


using Coc.MvvmCross.Plugins.Facebook;
using Coc.MvvmCross.Plugins.Facebook.Touch;

using Facebook.CoreKit;
using Foundation;
using UIKit;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;

using YuFit.IOS.Services;
using YuFit.IOS.Themes;
using MvvmCross.iOS.Platform;
using MvvmCross.Platform;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;


namespace YuFit.IOS.Application
{
    [Register ("AppDelegate")]
    public class AppDelegate : MvxApplicationDelegate
    {
        IOSPushNotificationService _pushNotificationService;
        public override UIWindow Window { get; set; }

        #region Application lifecycle

        /// <summary>
        /// For now, allow any ssl conncetion.
        /// </summary>
        /// <returns><c>true</c>, if finish launching was willed, <c>false</c> otherwise.</returns>
        /// <param name="application">Application.</param>
        /// <param name="launchOptions">Launch options.</param>
        public override bool WillFinishLaunching (UIApplication application, NSDictionary launchOptions)
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
            return true;
        }

        /// <summary>
        /// Finisheds the launching.
        /// </summary>
        /// <returns><c>true</c>, if launching was finisheded, <c>false</c> otherwise.</returns>
        /// <param name="application">Application.</param>
        /// <param name="launchOptions">Launch options.</param>
        public override bool FinishedLaunching (UIApplication application, NSDictionary launchOptions)
        {

            Xamarin.Insights.Initialize("239066cb351ed2d5ed2cfcf9c5875fb05ad27300");
            System.Console.WriteLine(string.Format("Bundle id : {0}", NSBundle.MainBundle.BundleIdentifier));
            application.ApplicationSupportsShakeToEdit = true;

            CreatePresenter();
            InitializeSocialServices();
            StartApplication();
            SetupGlobalAppearance();
            RegisterDefaultSettings();

            CheckAndHandleIncomingNotification(launchOptions);
            System.Console.WriteLine("FinishedLaunching");

            return true;
        }

        public override void WillEnterForeground (UIApplication application)
        {
            base.WillEnterForeground(application);
            Mvx.Resolve<IMvxMessenger>().Publish(new AppEnteredForegroundMessage(this));
            System.Console.WriteLine("WillEnterForeground");
        }

        /// <summary>
        /// Raises the activated event.
        /// </summary>
        /// <param name="application">Application.</param>
        public override void OnActivated (UIApplication application)
        {
            AppEvents.ActivateApp ();
            System.Console.WriteLine("OnActivated");
        }

        /// <summary>
        /// Opens the URL.
        /// </summary>
        /// <returns><c>true</c>, if URL was opened, <c>false</c> otherwise.</returns>
        /// <param name="application">Application.</param>
        /// <param name="url">URL.</param>
        /// <param name="sourceApplication">Source application.</param>
        /// <param name="annotation">Annotation.</param>
        public override bool OpenUrl (UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
        {
            // We need to handle URLs by passing them to their own OpenUrl in order to make the SSO authentication works.
            System.Console.WriteLine("URL IS {0}", url);
            System.Console.WriteLine("sourceApplication IS {0}", sourceApplication);
            return ApplicationDelegate.SharedInstance.OpenUrl (application, url, sourceApplication, annotation);
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Initializes the social services.
        /// </summary>
        void InitializeSocialServices ()
        {
            ((IOSFacebookService)Mvx.Resolve<IFacebookService>()).InitializeAppInfo("510471792408033", "YuFit");
        }

        /// <summary>
        /// Creates the presenter.
        /// </summary>
        void CreatePresenter ()
        {
            Window = new UIWindow(UIScreen.MainScreen.Bounds);

            MvxBaseIosViewPresenter presenter = new Coc.MvxAdvancedPresenter.Touch.PresenterDispatcher(this, Window); 
            var setup = new Setup(this, presenter);
            setup.Initialize();
        }

        /// <summary>
        /// Starts the application.
        /// </summary>
        void StartApplication ()
        {
            //bool settingValue = NSUserDefaults.StandardUserDefaults.BoolForKey("use_staging_server");
            Mvx.Resolve<IMvxAppStart>().Start("ios;yufit;" + GetVersionNumber());
            Window.MakeKeyAndVisible();

            _pushNotificationService = (IOSPushNotificationService) Mvx.Resolve<IPushNotificationService>();
        }

        /// <summary>
        /// Setups the global appearance.
        /// </summary>
        void SetupGlobalAppearance ()
        {
            UINavigationBar.Appearance.TintColor = Dashboard.HomeIconColor;

            UISegmentedControl.Appearance.SetContentPositionAdjustment(new UIOffset(0, 2), UISegmentedControlSegment.Any, UIBarMetrics.Default);
            UISegmentedControl.Appearance.TintColor = Invitations.ViewInvitations.FilterControlColor;

            // set the text color for selected state
//            UITextAttributes attributesTb = new UITextAttributes();
//            attributesTb.TextColor
//            UITabBarItem.Appearance.SetTitleTextAttributes()
//
//            SetTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, nil] forState:UIControlStateSelected];
//            // set the text color for unselected state
//            [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, nil] forState:UIControlStateNormal];


            UITextAttributes attributes = new UITextAttributes();
            attributes.Font = Invitations.ViewInvitations.FilterControlFont;
            UISegmentedControl.Appearance.SetTitleTextAttributes(attributes, UIControlState.Normal);
        }

        string GetVersionNumber() 
        {
            NSObject ver = NSBundle.MainBundle.InfoDictionary["CFBundleShortVersionString"];
            NSObject buildNum = NSBundle.MainBundle.InfoDictionary["CFBundleVersion"];

            return ver + "+" + buildNum;
        }
        #region Push notification

        /// <summary>
        /// Registereds for remote notifications.
        /// </summary>
        /// <param name="application">Application.</param>
        /// <param name="deviceToken">Device token.</param>
        public override void RegisteredForRemoteNotifications (UIApplication application, NSData deviceToken)
        {
            _pushNotificationService.RegisterForRemoteNotifications(application, deviceToken);
        }

        public override void FailedToRegisterForRemoteNotifications (UIApplication application, NSError error)
        {
            
        }

        /// <summary>
        /// Dids the receive remote notification.
        /// </summary>
        /// <param name="application">Application.</param>
        /// <param name="userInfo">User info.</param>
        /// <param name="completionHandler">Completion handler.</param>
        public override void DidReceiveRemoteNotification (UIApplication application, NSDictionary userInfo, System.Action<UIBackgroundFetchResult> completionHandler)
        {
            _pushNotificationService.ReceiveRemoteNotification(application, userInfo, completionHandler);
        }

        /// <summary>
        /// Checks the and handle incoming notification.
        /// </summary>
        /// <param name="launchOptions">Launch options.</param>
        void CheckAndHandleIncomingNotification(NSDictionary launchOptions)
        {
            if (launchOptions != null) {
                _pushNotificationService.HandleIncomingNotificationOnAppStart(launchOptions);
            }
        }

        public override void HandleAction (UIApplication application, string actionIdentifier, NSDictionary remoteNotificationInfo, System.Action completionHandler)
        {
            _pushNotificationService.HandleBackgroundNotificationAction(application, actionIdentifier, remoteNotificationInfo, completionHandler);
        }

        #endregion

        /// <summary>
        /// Registers the default settings.
        /// </summary>
        void RegisterDefaultSettings ()
        {
            NSUserDefaults userDefaults = NSUserDefaults.StandardUserDefaults;
            NSMutableDictionary appDefaults = new NSMutableDictionary(); 
            appDefaults.SetValueForKey(NSObject.FromObject(false), new NSString("use_staging_server"));
            userDefaults.RegisterDefaults(appDefaults);
            userDefaults.Synchronize();
        }

        #endregion
    }
}