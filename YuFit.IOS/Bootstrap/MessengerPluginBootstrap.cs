using MvvmCross.Platform.Plugins;

namespace YuFit.IOS.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}