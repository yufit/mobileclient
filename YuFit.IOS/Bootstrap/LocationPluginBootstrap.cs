using MvvmCross.Platform.Plugins;

namespace YuFit.IOS.Bootstrap
{
    public class LocationPluginBootstrap
        : MvxLoaderPluginBootstrapAction<Coc.MvvmCross.Plugins.Location.PluginLoader, Coc.MvvmCross.Plugins.Location.Touch.Plugin>
    {
    }
}