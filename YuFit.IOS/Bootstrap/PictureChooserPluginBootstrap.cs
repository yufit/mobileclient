﻿using Cirrious.CrossCore.Plugins;

namespace YuFit.IOS.Bootstrap
{
    public class PictureChooserPluginBootstrap
        : MvxLoaderPluginBootstrapAction<Coc.MvvmCross.Plugins.PictureChooser.PluginLoader, Coc.MvvmCross.Plugins.PictureChooser.Touch.Plugin>
    {
        public PictureChooserPluginBootstrap ()
        {
        }
    }
}
