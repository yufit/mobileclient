using MvvmCross.Platform.Plugins;

namespace YuFit.IOS.Bootstrap
{
    public class DownloadCachePluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.DownloadCache.PluginLoader, MvvmCross.Plugins.DownloadCache.iOS.Plugin>
    {
    }
}