using MvvmCross.Platform.Plugins;


namespace YuFit.IOS.Bootstrap
{
    public class FacebookPluginBootstrap
        : MvxLoaderPluginBootstrapAction<Coc.MvvmCross.Plugins.Facebook.PluginLoader, Coc.MvvmCross.Plugins.Facebook.Touch.Plugin>
    {
        public FacebookPluginBootstrap ()
        {
        }
    }
}
