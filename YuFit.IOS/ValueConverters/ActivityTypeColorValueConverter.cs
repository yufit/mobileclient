﻿using System;
using UIKit;
using YuFit.IOS.Themes;
using MvvmCross.Platform.Converters;

namespace YuFit.IOS.ValueConverters
{
    public class ActivityTypeColorValueConverter : MvxValueConverter<string, UIColor>
    {
        protected override UIColor Convert (string value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return YuFitStyleKitExtension.ActivityBackgroundColors(value).Base;
        }
    }
  
}

