using System;
using CoreLocation;
using MvvmCross.Platform.Converters;
using YuFit.Core.Models;

namespace YuFit.IOS.ValueConverters
{
	public class CoordinateToCLLocationCoordinate2DValueConverter : MvxValueConverter<Coordinate, CLLocationCoordinate2D>
	{
		protected override CLLocationCoordinate2D Convert (Coordinate value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return new CLLocationCoordinate2D 
			{
				Latitude = value.Latitude,
				Longitude = value.Longitude
			};
		}
	}
}

