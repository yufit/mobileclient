﻿using System;
using System.Globalization;

using MvvmCross.Platform.Converters;

using CoreLocation;
using MapKit;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.Utils;

namespace YuFit.IOS.ValueConverters
{
    public class RegionToMKCoordinateWithOffsetValueConverter : MvxValueConverter<Region, MKCoordinateRegion>
    {
        protected override MKCoordinateRegion Convert (Region value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null) {
                CLLocationCoordinate2D current = new CLLocationCoordinate2D {
                    Latitude = value.Center.Latitude,
                    Longitude = value.Center.Longitude
                };

                MKCoordinateSpan span = new MKCoordinateSpan(Mapping.MilesToLatitudeDegrees(value.Radius), Mapping.MilesToLongitudeDegrees(value.Radius, current.Latitude));
                MKCoordinateRegion region = new MKCoordinateRegion(current, span);
                region.Center.Latitude = region.Span.LatitudeDelta * -0.45;
                return region;
            }
            return new MKCoordinateRegion();
        }

        protected override Region ConvertBack (MKCoordinateRegion value, Type targetType, object parameter, CultureInfo culture)
        {
            return new Region {
                Center = new Coordinate {
                    Latitude = value.Center.Latitude,
                    Longitude = value.Center.Longitude
                },
                Radius = Mapping.LatitudeDegreesToMiles(value.Span.LatitudeDelta)
            };
        }
    }
}

