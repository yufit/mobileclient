using System;
using MvvmCross.Platform.Converters;

namespace YuFit.IOS.ValueConverters
{
    public class ToUppercaseValueConverter : MvxValueConverter<string, string>
    {
        protected override string Convert (string value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.ToUpperInvariant();
        }
    }
}

