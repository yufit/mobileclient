﻿using System;
using MvvmCross.Platform.Converters;
using UIKit;
using YuFit.IOS.Themes;
using System.Globalization;

namespace YuFit.IOS.ValueConverters
{
    public class AuthenticationMethodChosenToColorValueConverter : MvxValueConverter<bool, UIColor>
    {
        protected override UIColor Convert (bool value, Type targetType, object parameter, CultureInfo culture)
        {
            return value ? YuFitStyleKit.YellowDark : YuFitStyleKit.Gray;
        }

    }
}

