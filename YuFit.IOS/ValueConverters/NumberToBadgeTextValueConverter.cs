﻿using System;
using MvvmCross.Platform.Converters;

namespace YuFit.IOS.ValueConverters
{
    public class NumberToBadgeTextValueConverter : MvxValueConverter<int, string>
    {
        protected override string Convert (int value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value > 0)
            {
                return value.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }

}

