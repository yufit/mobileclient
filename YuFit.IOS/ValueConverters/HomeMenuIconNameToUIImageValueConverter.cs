using System;
using MvvmCross.Platform.Converters;
using UIKit;
using YuFit.IOS.Themes;
using CoreGraphics;

namespace YuFit.IOS.ValueConverters
{
    public class HomeMenuIconNameToUIImageValueConverter : MvxValueConverter<string, UIImage>
    {
        protected override UIImage Convert (string value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Action<CGRect, UIColor> drawMethod = YuFitStyleKit.DrawWhatIcon;

            switch (value)
            {
                case "Invitations":
                    drawMethod = YuFitStyleKit.DrawWhatIcon;
                    break;

                case "Messages":
                    drawMethod = YuFitStyleKit.DrawMessagesIcon;
                    break;

                case "MapFilters":
                    drawMethod = YuFitStyleKit.DrawWhereIcon;
                    break;

                case "Find":
                    drawMethod = YuFitStyleKit.DrawFindIcon;
                    break;

                case "EditProfile":
                    drawMethod = YuFitStyleKit.DrawEditProfileIcon;
                    break;
            }

            UIGraphics.BeginImageContextWithOptions(new CGSize(25f, 25f), false, 0);
            drawMethod(new CGRect(0, 0, 25f, 25f), Themes.HomeMenu.MenuItems.TextColor);
            var image = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            return image;
        }


    }
}

