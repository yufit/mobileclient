using System;
using CoreLocation;
using Coc.MvvmCross.Plugins.Location;
using MvvmCross.Platform.Converters;
using System.Globalization;

namespace YuFit.IOS.ValueConverters
{

    public class CoordinatesToCLLocationValueConverter : MvxValueConverter<CocLocation, CLLocationCoordinate2D>
    {
        protected override CLLocationCoordinate2D Convert (CocLocation value, Type targetType, object parameter, CultureInfo culture)
        {
            return new CLLocationCoordinate2D {
                Latitude = value.Coordinates.Latitude,
                Longitude = value.Coordinates.Longitude
            };
        }

        protected override CocLocation ConvertBack (CLLocationCoordinate2D value, Type targetType, object parameter, CultureInfo culture)
        {
            return new CocLocation {
                Coordinates = new CocCoordinates {
                    Latitude = value.Latitude,
                    Longitude = value.Longitude
                }
            };
        }
    }
}

