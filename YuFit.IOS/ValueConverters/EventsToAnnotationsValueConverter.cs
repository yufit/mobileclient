//using System;
//using System.Collections.Generic;
//using Cirrious.CrossCore.Converters;
//using MonoTouch.CoreLocation;
//using YuFit.IOS.Views;
//using YuFit.Core.Models;
//
//namespace YuFit.IOS.ValueConverters
//{
//    public class EventsToAnnotationsValueConverter : MvxValueConverter<List<FitEvent>, List<BindableAnnotation>>
//    {
//
//        protected override List<BindableAnnotation> Convert (List<FitEvent> value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
//        {
//            List<BindableAnnotation> annotations = new List<BindableAnnotation>();
//            value.ForEach(ev =>
//            {   
//                BindableAnnotation annotation = new BindableAnnotation
//                {
//                    Coordinate = new CLLocationCoordinate2D
//                    {
//                        Latitude = ev.Coordinates.Latitude,
//                        Longitude = ev.Coordinates.Longitude
//                    },
//                    Name = ev.Name
//
//
//                };
//                annotations.Add(annotation);
//            });
//            return annotations;
//        }
//    }
//}
//
