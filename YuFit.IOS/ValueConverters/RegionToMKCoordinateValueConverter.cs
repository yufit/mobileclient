using MvvmCross.Platform.Converters;
using MapKit;
using YuFit.Core.Interfaces.Services;
using System;
using System.Globalization;
using CoreLocation;
using YuFit.Core.Utils;
using YuFit.Core.Models;

namespace YuFit.IOS.ValueConverters
{
    public class RegionToMKCoordinateValueConverter : MvxValueConverter<Region, MKCoordinateRegion>
    {
        protected override MKCoordinateRegion Convert (Region value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null) {
                CLLocationCoordinate2D current = new CLLocationCoordinate2D {
                    Latitude = value.Center.Latitude,
                    Longitude = value.Center.Longitude
                };

                MKCoordinateSpan span = new MKCoordinateSpan(Mapping.MilesToLatitudeDegrees(value.Radius), Mapping.MilesToLongitudeDegrees(value.Radius, current.Latitude));
                return new MKCoordinateRegion(current, span);
            }
            return new MKCoordinateRegion();
        }

        protected override Region ConvertBack (MKCoordinateRegion value, Type targetType, object parameter, CultureInfo culture)
        {
            return new Region {
                Center = new Coordinate {
                    Latitude = value.Center.Latitude,
                    Longitude = value.Center.Longitude
                },
                Radius = Mapping.LatitudeDegreesToMiles(value.Span.LatitudeDelta)
            };
        }
    }
}

