﻿using System;
using MapKit;
using CoreLocation;
using UIKit;
using MvvmCross.Binding.BindingContext;

namespace YuFit.IOS.Controls.Annotations
{
    public class BindableAnnotation : MKAnnotation, IDisposable, IBindableAnnotation
    {
        private bool _disposed;
        private CLLocationCoordinate2D _coordinate;

        public string Id { get; set; }
        public string Name { get; set; }

        public override string Title { get { return Name; } }

        public override CLLocationCoordinate2D Coordinate { get { return _coordinate; } }
        public IMvxBindingContext BindingContext { get; set; }

        public virtual object DataContext {
            get { return BindingContext.DataContext; }
            set { BindingContext.DataContext = value; }
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public new void Dispose(bool disposing)
        {
            if (!_disposed) {
                if (disposing) {
                    OnBeingDisposed();
                }
            }

            _disposed = true;
        }

        #pragma warning disable 114
        public virtual void SetCoordinate(CLLocationCoordinate2D coordinate)
        {
            // call WillChangeValue and DidChangeValue to use KVO with
            // an MKAnnotation so that setting the coordinate on the
            // annotation instance causes the associated annotation
            // view to move to the new location.

            // We animate it as well for a smooth transition
            UIView.Animate(0.25, () => 
            {
                WillChangeValue ("coordinate");
                _coordinate = coordinate;
                DidChangeValue ("coordinate");
            });
        }
        #pragma warning restore 114

        protected virtual void OnBeingDisposed ()
        {
            // can be overridden in derived classes
        }
    }
}


