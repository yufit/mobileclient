﻿using YuFit.Core.ViewModels.MapAnnotations;
using YuFit.Core.Models;
using MvvmCross.Platform;
using MvvmCross.Binding.BindingContext;

namespace YuFit.IOS.Controls.Annotations
{
    public class MapAnnotation : BindableAnnotation
    {
        public MapAnnotation()
        {
            this.CreateBindingContext();
            this.DelayBind(() => {
                var set = this.CreateBindingSet<MapAnnotation, MapAnnotationViewModel>();
                set.Bind().For(my => my.Id).To(vm => vm.Id);
                set.Bind().For(my => my.Name).To(vm => vm.DisplayName);
                set.Bind().For("Coordinate").To(vm => vm.Location).WithConversion("CoordinateToCLLocationCoordinate2D");
                set.Apply();
            });
        }

        ~MapAnnotation ()
        {
            Mvx.Trace("Destroy MapAnnotation {0}", GetType().ToString());
        }

    }
}

