﻿using MapKit;
using YuFit.Core.ViewModels.MapAnnotations;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platform;


namespace YuFit.IOS.Controls.Annotations
{
    public class MapAnnotationView<T> : MKAnnotationView, IMvxBindingContextOwner
        where T : MapAnnotationViewModel
    {
        protected T ViewModel { get { return (T) DataContext; } }

        public IMvxBindingContext BindingContext { get; set; }
        public object DataContext {
            get { return BindingContext.DataContext; }
            set {  BindingContext.DataContext = value; }
        }

        public MapAnnotationView (IMKAnnotation annotation, string id) : base(annotation, id)
        {
            
        }

        ~MapAnnotationView ()
        {
            Mvx.Trace("Destroy MapAnnotationView {0}", GetType().ToString());
        }
    }
}

