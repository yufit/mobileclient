﻿
using CoreGraphics;
using MapKit;
using UIKit;

using YuFit.Core.Models;
using YuFit.Core.ViewModels.MapAnnotations;

using YuFit.IOS.Themes;
using MvvmCross.Binding.BindingContext;

namespace YuFit.IOS.Controls.Annotations
{
    public sealed class ActivityAnnotationView : MapAnnotationView<FitActivityMapAnnotationViewModel>
    {
        // Analysis disable ValueParameterNotUsed
        public ActivityType ActivityType {
            get { return ViewModel.ActivityType; }
            set { UpdateColors(); }
        }
        // Analysis restore ValueParameterNotUsed

        private void UpdateColors() 
        {
            var activityColors = YuFitStyleKitExtension.ActivityBackgroundColors(ViewModel.ActivityType.TypeName);

            UIColor color = activityColors.Base;
            if (ViewModel.Invited) { color = activityColors.Light; }
            Image = YuFitStyleKit.ImageOfDiamondAnnotation(color);
        }

        public ActivityAnnotationView (IMKAnnotation annotation, string id)
            : base(annotation, id)
        {
            this.CreateBindingContext();

            this.DelayBind(() => {
                var set = this.CreateBindingSet<ActivityAnnotationView, FitActivityMapAnnotationViewModel>();
                set.Bind().For(my => my.ActivityType).To(vm => vm.ActivityType);
                set.Apply();
            });
        }
    }
}

