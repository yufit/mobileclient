﻿
using CoreGraphics;
using MapKit;
using UIKit;

using YuFit.Core.Models;
using YuFit.Core.ViewModels.MapAnnotations;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;

namespace YuFit.IOS.Controls.Annotations
{
    public sealed class EventAnnotationView : MapAnnotationView<EventMapAnnotationViewModel>
    {
        public Colors Colors {
            get { return ViewModel.Colors; }
            set { UpdateColors(value); }
        }

        private void UpdateColors(Colors colors) 
        {
            _imageContainer.StrokeColor = new UIColor(colors.Base.Red/255.0f, colors.Base.Green/255.0f, colors.Base.Blue/255.0f, 1.0f);
            _imageContainer.SetNeedsDisplay();
        }

        readonly MvxPictureContainer _imageContainer = new MvxPictureContainer(new CGRect(0, 0, 50, 50));
        readonly MvxImageViewLoader _loader;

        public EventAnnotationView (IMKAnnotation annotation, string id)
            : base(annotation, id)
        {
            this.CreateBindingContext();

            AddSubview(_imageContainer);
            Frame = _imageContainer.Frame;

            _loader = new MvxImageViewLoader(() => _imageContainer.AvatarImage);

            this.DelayBind(() => {
                var set = this.CreateBindingSet<EventAnnotationView, EventMapAnnotationViewModel>();
                set.Bind().For(my => my.Colors).To(vm => vm.Colors);
                set.Bind(_loader).To(vm => vm.AvatarUrl);
                set.Apply();
            });
        }
    }
}

