﻿
using CoreGraphics;
using MapKit;
using UIKit;

using YuFit.Core.Models;
using YuFit.Core.ViewModels.MapAnnotations;

using YuFit.IOS.Themes;
using MvvmCross.Binding.BindingContext;

namespace YuFit.IOS.Controls.Annotations
{
    public sealed class GroupActivityAnnotationView : MapAnnotationView<GroupFitActivityMapAnnotationViewModel>
    {
        public ActivityType ActivityType {
            get { return ViewModel.ActivityType; }
            set { UpdateColors(); }
        }

        public ActivityColors ActivityColors { get; set; }
        public UIColor Color { 
            get {
                GroupFitActivityMapAnnotationViewModel viewModel = (GroupFitActivityMapAnnotationViewModel) DataContext;
                if (viewModel.Invited)
                {
                    return ActivityColors.Light;
                }

                return ActivityColors.Base;
            }
        }

        private void UpdateColors() 
        {
            var activityColors = YuFitStyleKitExtension.ActivityBackgroundColors(ViewModel.ActivityType.TypeName);

            UIColor color = activityColors.Base;
            if (ViewModel.Invited) { color = activityColors.Light; }
            Image = YuFitStyleKit.ImageOfDiamondAnnotation(color);

//            ActivityColors = YuFitStyleKitExtension.ActivityBackgroundColors(ViewModel.ActivityType.TypeName);
//            _imageContainer.StrokeColor = ActivityColors.Dark;
//            _imageContainer.SetNeedsDisplay();
        }

//        readonly MvxPictureContainer _imageContainer = new MvxPictureContainer(new CGRect(0, 0, 75, 75));
//        MvxImageViewLoader _loader;

        public GroupActivityAnnotationView (IMKAnnotation annotation, string id)
            : base(annotation, id)
        {
            this.CreateBindingContext();

//            AddSubview(_imageContainer);
//            Frame = _imageContainer.Frame;
//
//            _loader = new MvxImageViewLoader(() => _imageContainer.AvatarImage);

            this.DelayBind(() => {
                var set = this.CreateBindingSet<GroupActivityAnnotationView, GroupFitActivityMapAnnotationViewModel>();
                set.Bind().For(my => my.ActivityType).To(vm => vm.ActivityType);
//                set.Bind(_loader).To(vm => vm.AvatarUrl);
                set.Apply();
            });
        }
    }
}

