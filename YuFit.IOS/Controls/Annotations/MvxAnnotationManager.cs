﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

using MapKit;
using UIKit;
using MvvmCross.Binding.Attributes;
using MvvmCross.Binding;
using MvvmCross.Platform.WeakSubscription;
using MvvmCross.Platform.Platform;

namespace YuFit.IOS.Controls.Annotations
{
    // an abstract helper class
    public abstract class MvxAnnotationManager
    {
        #region Data members

        /// <summary>
        /// The map view whos annotations are being managed for
        /// </summary>
        private readonly MKMapView _mapView;

        /// <summary>
        /// The data source for the map annotations
        /// </summary>
        private IEnumerable _itemsSource;

        /// <summary>
        /// Subscription for collection changed events on the item source
        /// </summary>
        private IDisposable _subscription;

        /// <summary>
        /// the managed list of annotations
        /// </summary>
        private readonly Dictionary<object, MKAnnotation> _annotations = new Dictionary<object, MKAnnotation>();

        #endregion

        #region Properties

        // MvxSetToNullAfterBinding isn't strictly needed any more 
        // - but it's nice to have for when binding is torn down
        [MvxSetToNullAfterBinding]
        public virtual IEnumerable ItemsSource {
            get  { return _itemsSource; }
            set  { SetItemsSource(value); }
        }

        #endregion

        #region constructor

        protected MvxAnnotationManager(MKMapView mapView)
        {
            _mapView = mapView;
        }

        #endregion



        protected virtual void SetItemsSource(IEnumerable value)
        {
            if (_itemsSource == value)
            {
                return;
            }

            if (_subscription != null)
            {
                _subscription.Dispose();
                _subscription = null;
            }

            _itemsSource = value;

            if (_itemsSource != null && !(_itemsSource is IList))
            {
                MvxBindingTrace.Trace(MvxTraceLevel.Warning,
                    "Binding to IEnumerable rather than IList - this can be inefficient, especially for large lists");
            }

            ReloadAllAnnotations();

            var newObservable = _itemsSource as INotifyCollectionChanged;
            if (newObservable != null)
            {
                _subscription = newObservable.WeakSubscribe(OnItemsSourceCollectionChanged);
            }
        }

        protected virtual void OnItemsSourceCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    AddAnnotations(e.NewItems);
                    break;

                case NotifyCollectionChangedAction.Remove:
                    RemoveAnnotations(e.OldItems);
                    break;

                case NotifyCollectionChangedAction.Replace:
                    RemoveAnnotations(e.OldItems);
                    AddAnnotations(e.NewItems);
                    break;

                case NotifyCollectionChangedAction.Move:
                    // not interested in this
                    break;

                case NotifyCollectionChangedAction.Reset:
                    ReloadAllAnnotations();
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected virtual void ReloadAllAnnotations()
        {
            _mapView.RemoveAnnotations(_annotations.Values.Select(x => (IMKAnnotation)x).ToArray());
            _annotations.Clear();

            if (_itemsSource == null)
            {
                return;
            }

            AddAnnotations(_itemsSource);
        }

        protected abstract MKAnnotation CreateAnnotation(object item);

        protected virtual void RemoveAnnotations(IEnumerable oldItems)
        {
            foreach (var item in oldItems)
            {
                RemoveAnnotationFor(item);
            }
        }

        protected virtual void RemoveAnnotationFor(object item)
        {
            var annotation = _annotations[item];
            var v = _mapView.ViewForAnnotation(annotation);
            _annotations.Remove(item);

            if (v != null) {
                UIView.AnimateNotify(0.2f, () => { v.Layer.Opacity = 0.0f; }, finished => { _mapView.RemoveAnnotation(annotation);});
            } else {
                _mapView.RemoveAnnotation(annotation);
            }
        }

        protected virtual void AddAnnotations(IEnumerable newItems)
        {
            foreach (object item in newItems)
            {
                AddAnnotationFor(item);
            }
        }

        protected virtual void AddAnnotationFor(object item)
        {
            if (!_annotations.ContainsKey(item)) {
                var annotation = CreateAnnotation(item);
                _annotations[item] = annotation;
                _mapView.AddAnnotation(annotation);
            }
        }
    }


}

