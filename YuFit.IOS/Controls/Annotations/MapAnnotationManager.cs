﻿using MapKit;
using YuFit.Core.ViewModels.MapAnnotations;

namespace YuFit.IOS.Controls.Annotations
{
    public class MapAnnotationManager : MvxAnnotationManager
    {
        public MapAnnotationManager(MKMapView mapView) : base(mapView)
        {
        }

        protected override MKAnnotation CreateAnnotation(object item)
        {
            var annotationViewModel = item as MapAnnotationViewModel;

            if (annotationViewModel is GroupFitActivityMapAnnotationViewModel) {
                return new GroupFitActivityAnnotation { DataContext = annotationViewModel };
            }

            if (annotationViewModel is EventMapAnnotationViewModel) {
                return new EventAnnotation { DataContext = annotationViewModel };
            }

            if (annotationViewModel is FitActivityMapAnnotationViewModel) {
                return new FitActivityAnnotation { DataContext = annotationViewModel };
            }

            return null;
        }
    }
}

