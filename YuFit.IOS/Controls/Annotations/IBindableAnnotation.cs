﻿using MvvmCross.Binding.BindingContext;


namespace YuFit.IOS.Controls.Annotations
{
    /// <summary>
    /// Contract for Annotations so that they can be bound to a data context within a bindable map
    /// </summary>
    public interface IBindableAnnotation : IMvxBindingContextOwner
    {
        string Id           { get; set; }
        object DataContext  { get; set; }
    }
}


