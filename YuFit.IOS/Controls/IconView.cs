﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Controls
{
    [Register ("IconView")]
    public class IconView : UIView
    {
        public float Padding { get; set; }

        protected Action<CGRect, UIColor> _drawIcon;
        public Action<CGRect, UIColor> DrawIcon { 
            get { return _drawIcon; }
            set { _drawIcon = value; SetNeedsDisplay(); }
        }

        protected UIColor _strokeColor;
        public UIColor StrokeColor {
            get { return _strokeColor; }
            set { _strokeColor = value; SetNeedsDisplay(); }
        }

        public IconView () { DoConstruction(YuFitStyleKit.White); }
        public IconView (UIColor strokeColor, string iconName) { DoConstruction(strokeColor, iconName); }
        public IconView (IntPtr handle) : base(handle) { DoConstruction(YuFitStyleKit.White); }

        void DoConstruction(UIColor strokeColor, string iconName = "")
        {
            BackgroundColor = UIColor.Clear;
            Padding = 0.0f;
            _strokeColor = strokeColor;

            if (!string.IsNullOrEmpty(iconName)) {
                DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("Draw" + iconName);
            }
        }

        public override void Draw (CGRect rect)
        {
            base.Draw(rect);
            BackgroundColor = UIColor.Clear;

            if (DrawIcon != null) {
                using (var ctx = UIGraphics.GetCurrentContext()) {
                    CGRect imageRect = Bounds.Inset(Padding, Padding);
                    DrawIcon(imageRect, _strokeColor);
                }
            }
        }

    }

}

