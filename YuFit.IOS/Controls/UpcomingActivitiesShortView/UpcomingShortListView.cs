﻿using Cirrious.FluentLayouts.Touch;

using UIKit;

using YuFit.Core.Extensions;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using MvvmCross.iOS.Views;
using MvvmCross.Binding.BindingContext;

namespace YuFit.IOS.Controls.UpcomingActivitiesShortView
{
    public class UpcomingShortListView<TTableView, TTableViewCell> : UIView
        where TTableView : MvxTableViewController, new()
    {

        public readonly UILabel         UpcomingTitle      = new UILabel();
        public readonly UILabel         OptionalTitle      = new UILabel();
        public readonly SeparatorLine   UpcomingSeparator  = new SeparatorLine();
        public          TTableView      TableViewCtrl      = new TTableView();

        public UpcomingShortListView ()
        {
            BackgroundColor = UIColor.Clear;
            UpcomingSeparator.BackgroundColor = YuFitStyleKit.Yellow;


            UpcomingTitle.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 20);
            UpcomingTitle.TextColor = YuFitStyleKit.Gray;
            UpcomingTitle.TextAlignment = UITextAlignment.Left;

            OptionalTitle.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 20);
            OptionalTitle.TextColor = YuFitStyleKit.Gray;
            OptionalTitle.TextAlignment = UITextAlignment.Right;
        }

        public void SetBindingContext(IMvxBindingContext bindingContext)
        {
            // don't access the view before the data is bound to it.  or the vm gets created twice
            // once from the tvc and the other from the view using this class.
            TableViewCtrl.BindingContext = bindingContext;
            AddSubviews(new UIView[] {UpcomingTitle, OptionalTitle, UpcomingSeparator, TableViewCtrl.View});
        }

        public void SetLayoutConstraints()
        {
            this.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            //            TableViewCtrl.TableView.ScrollEnabled = false;
            //            TableViewCtrl.TableView.AllowsSelection = false;


            UpcomingSeparator.AddConstraints(UpcomingSeparator.Height().EqualTo(1.0f));

            this.AddConstraints(
                UpcomingTitle.WithSameWidth(this).WithMultiplier(0.5f),
                UpcomingTitle.Left().EqualTo().LeftOf(this),
                UpcomingTitle.Top().EqualTo(10.0f).TopOf(this),

                OptionalTitle.WithSameWidth(this).WithMultiplier(0.5f),
                OptionalTitle.Right().EqualTo().RightOf(this),
                OptionalTitle.Top().EqualTo(10.0f).TopOf(this),

                UpcomingSeparator.WithSameWidth(this),
                UpcomingSeparator.WithSameCenterX(this),
                UpcomingSeparator.Top().EqualTo(1.5f).BottomOf(UpcomingTitle),

                TableViewCtrl.View.WithSameWidth(this),
                TableViewCtrl.View.WithSameCenterX(this),
                TableViewCtrl.View.Top().EqualTo(5.0f).BottomOf(UpcomingSeparator),
                TableViewCtrl.View.Bottom().EqualTo().BottomOf(this)
            );
        }
    }
}

