using System;

using Cirrious.FluentLayouts.Touch;


using Foundation;
using UIKit;

using YuFit.Core.Extensions;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using YuFit.Core.Interfaces.Services;
using System.Linq;
using YuFit.Core.Models;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platform;

namespace YuFit.IOS.Controls.UpcomingActivitiesShortView
{

    public class UpcomingShortListTableViewCell<TViewModel> : MvxTableViewCell
        where TViewModel : YuFit.Core.ViewModels.Activity.UpcomingActivityViewModel
    {
        public readonly DiamondShapeView ActivityIcon = new DiamondShapeView();
        public readonly UILabel WhenLabel = new UILabel();

        readonly SeparatorLine _strikethrough = new SeparatorLine();

        private string _sport;
        public string Sport {
            get { return _sport; }
            set { _sport = value; UpdateColor(); }
        }

        private bool _isCanceled;
        public bool IsCanceled {
            get { return _isCanceled; }
            set { _isCanceled = value; UpdateCanceled(); }
        }

        private DateTime _when;
        public DateTime When {
            get { return _when; }
            set { _when = value; UpdateWhenLabel(); }
        }

        public UpcomingShortListTableViewCell ()
        {
            BackgroundColor = UIColor.Clear;

            ContentView.AddSubviews(new UIView[] {ActivityIcon, WhenLabel});

            SetupConstraints();

            this.DelayBind(() => {
                WhenLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 18);
                WhenLabel.TextColor = YuFitStyleKit.Yellow;

                var set = this.CreateBindingSet<UpcomingShortListTableViewCell<TViewModel>, TViewModel>();
                set.Bind().For(me => me.Sport).To(vm => vm.Sport);
                set.Bind().For(me => me.When).To(vm => vm.When);
                set.Bind().For(me => me.IsCanceled).To(vm => vm.IsCanceled);
                set.Apply ();
            });
        }

        void SetupConstraints ()
        {
            ContentView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            ActivityIcon.AddConstraints(
                ActivityIcon.Height().EqualTo(25.0f),
                ActivityIcon.Width().EqualTo(25.0f)
            );

            ContentView.AddConstraints(
                ActivityIcon.Left().EqualTo().LeftOf(ContentView),
                ActivityIcon.WithSameCenterY(ContentView),

                WhenLabel.Left().EqualTo(5.0f).RightOf(ActivityIcon),
                WhenLabel.Right().EqualTo(-5.0f).RightOf(ContentView),
                WhenLabel.CenterY().EqualTo(2.5f).CenterYOf(ContentView)
            );
        }

        void UpdateColor ()
        {
            if (!string.IsNullOrEmpty(Sport)) {

                UIColor colorBase = UIColor.White;
                UIColor colorLight = UIColor.White;

                var categories = Mvx.Resolve<IFitActivityService>().GetEventCategories();
                var category = categories.FirstOrDefault(c => c.Name == Sport);
                if (category != null) {
                    var ccolors = category.Colors;
                    colorBase = new UIColor(ccolors.Base.Red/255.0f, ccolors.Base.Green/255.0f, ccolors.Base.Blue/255.0f, 1.0f);
                    colorLight = new UIColor(ccolors.Light.Red/255.0f, ccolors.Light.Green/255.0f, ccolors.Light.Blue/255.0f, 1.0f);
                } else {
                    ActivityColors colors;
                    colors = YuFitStyleKitExtension.ActivityBackgroundColors(Sport);
                    colorBase = colors.Base;
                    colorLight = colors.Light;
                }

                WhenLabel.TextColor = colorBase;
                _strikethrough.BackgroundColor = colorLight;
                ActivityIcon.DiamondColor = colorBase;
                ActivityIcon.SetNeedsDisplay();
            }
        }

        void UpdateWhenLabel()
        {
            NSDateFormatter formatter = new NSDateFormatter();
            formatter.DateFormat = "EEEE";

            string fullDate = string.Empty;
            string dayString = formatter.WeekdaySymbols[(int)_when.DayOfWeek];
            fullDate += dayString + ", ";

            formatter.DateFormat = "MMMM";
            string monthString = formatter.MonthSymbols[(int) _when.Month - 1];
            var monthAndDateString = monthString + ' ' + _when.Day;
            fullDate += monthAndDateString + ", ";

            formatter.SetLocalizedDateFormatFromTemplate("jj:mm");
            NSDate tmp = (NSDate) DateTime.SpecifyKind(_when, DateTimeKind.Local);
            string timeString = formatter.StringFor(tmp);
            fullDate += timeString;

            WhenLabel.Text = fullDate;
            SetNeedsDisplay();
        }

        void UpdateCanceled()
        {
            if (_isCanceled) {
                this.AddSubview(_strikethrough);
                _strikethrough.TranslatesAutoresizingMaskIntoConstraints = false;
                _strikethrough.AddConstraints(_strikethrough.Height().EqualTo(1.0f));

                this.AddConstraints(
                    _strikethrough.Left().EqualTo(5.0f).LeftOf(this),
                    _strikethrough.Right().EqualTo(-5.0f).RightOf(this),
                    _strikethrough.WithSameCenterX(this),
                    _strikethrough.WithSameCenterY(this)
                );
            }
        }
    }
    
}
