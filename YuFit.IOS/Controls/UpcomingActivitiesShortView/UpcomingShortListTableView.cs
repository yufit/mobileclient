using UIKit;

using YuFit.IOS.Views;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Controls.UpcomingActivitiesShortView
{
    public class UpcomingShortListTableView<TViewModel, TCell> : MvxTableViewController where TCell : UITableViewCell, new()
    {
        protected new TViewModel ViewModel { get { return (TViewModel) base.ViewModel; } }
        public TableViewSource<TCell> Source { get; set;}

        string _key;
        public UpcomingShortListTableView (string key)
        {
            _key = key;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            TableView.BackgroundColor = UIColor.Clear;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.RowHeight = 25;

            Source = new TableViewSource<TCell>(TableView, _key);

            TableView.Source = Source;
        }
    }
    
}
