﻿using System;
using CoreGraphics;

namespace YuFit.IOS.Controls
{
    public class MapBringHomeButton : DiamondShapeView
    {
        public IconView Icon = new IconView();

        public MapBringHomeButton ()
        {
            AddSubview(Icon);
        }

        public override void LayoutSubviews ()
        {
            var frame = DiamondRect;
            frame = frame.Inset(10.0f, 10.0f);
            frame.Offset(new CGPoint(-1.5f, 0.0f));
            Icon.Frame = frame;
            base.LayoutSubviews();
        }
    }


}

