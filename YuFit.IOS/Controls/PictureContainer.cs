using System;


using CoreGraphics;
using UIKit;

using YuFit.IOS.Controls;
using YuFit.IOS.Utilities;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Controls
{
    public sealed class ActivityIconDiamondView : DiamondShapeView
    {
        public Action<CGRect, UIColor>      DrawIcon  { get; set; }
        public UIColor                      ActivityFillColor { get; set; }

        public ActivityIconDiamondView(CGRect rect) : base(rect) {}
        public ActivityIconDiamondView() {}

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            if (DrawIcon != null) {
                using (var ctx = new CGContextHelper(UIGraphics.GetCurrentContext())) {
                    CGRect imageRect = Bounds.Inset(0.32f*Bounds.Width, 0.32f*Bounds.Height);
                    DrawIcon(imageRect, ActivityFillColor);
                }
            }
        }
    }

    /// <summary>
    /// Picture container.
    /// </summary>
    public sealed class PictureContainer : DiamondShapeView
    {
        public UIImageView AvatarImage = new UIImageView();
        public UILabel Label = new UILabel(); 
        public DiamondShapeView FrameContainer = new DiamondShapeView();

        public MvxImageViewLoader ImageViewLoader { get; private set; }

        public PictureContainer (float pictureInset = 14.5f)
        {
            ImageViewLoader = new MvxImageViewLoader(() => AvatarImage);
            AddSubview(FrameContainer);
            FrameContainer.AddSubview(Label);
            FrameContainer.AddSubview(AvatarImage);

            TransparentBackground = true;
            StrokeWidth = 1.25f;
            StrokeColor = Themes.YuFitStyleKit.Yellow;

            Label.TextAlignment = UITextAlignment.Center;
        }

        public override void LayoutSubviews ()
        {
            FrameContainer.Frame = DiamondRect;

            AvatarImage.Frame = FrameContainer.Bounds;
            var rect = FrameContainer.Bounds;
            rect.Y += 4;
            Label.Frame = rect;
            base.LayoutSubviews();
        }

        protected override void Dispose (bool disposing)
        {
            if (disposing) {
                ImageViewLoader.Dispose();
                ImageViewLoader = null;

                AvatarImage.Dispose();
                AvatarImage = null;

                FrameContainer.Dispose();
                FrameContainer = null;
            }
            base.Dispose(disposing);
        }
    }

    public sealed class MvxPictureContainer : DiamondShapeView
    {
        public MvxImageView AvatarImage = new MvxImageView();
        public DiamondShapeView FrameContainer = new DiamondShapeView();

        public MvxPictureContainer (CGRect frame)
        {
            FrameContainer.Inset = 2.0f;
            Frame = frame;
            AddSubview(FrameContainer);
            FrameContainer.AddSubview(AvatarImage);

            TransparentBackground = false;
            StrokeWidth = 2.0f;
            StrokeColor = Themes.YuFitStyleKit.Yellow;
        }

        public override void LayoutSubviews ()
        {
            FrameContainer.Frame = DiamondRect;
            AvatarImage.Frame = FrameContainer.Bounds;
            base.LayoutSubviews();
        }

        protected override void Dispose (bool disposing)
        {
            if (disposing) {
                AvatarImage.Dispose();
                AvatarImage = null;

                FrameContainer.Dispose();
                FrameContainer = null;
            }
            base.Dispose(disposing);
        }
    }
}
