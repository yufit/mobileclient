

using UIKit;

using CoreGraphics;

namespace YuFit.IOS.Controls
{
    public class DateField : UITextField
    {
        public readonly UIDatePicker DatePicker = new UIDatePicker();

        public DateField ()
        {
            DatePicker.Mode = UIDatePickerMode.Date;
            InputView = DatePicker;
        }

        #region Overrides to prevent select, copy, paste and carret to show.

        public override CGRect GetCaretRectForPosition (UITextPosition position)
        {
            return CGRect.Empty;
        }

        public override UITextSelectionRect[] GetSelectionRects (UITextRange range)
        {
            return null;
        }

        public override bool ShouldChangeTextInRange (UITextRange inRange, string replacementText)
        {
            return false;
        }

        public override void AddGestureRecognizer (UIGestureRecognizer gestureRecognizer)
        {
            if (gestureRecognizer is UILongPressGestureRecognizer)
            {
                gestureRecognizer.Enabled = false;
            }
            base.AddGestureRecognizer(gestureRecognizer);
        }

        #endregion
    }
    
}
