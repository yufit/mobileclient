﻿using System;
using Cirrious.FluentLayouts.Touch;
using UIKit;
using YuFit.Core.Extensions;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using YuFit.Core.Models;
using System.Collections.Generic;

namespace YuFit.IOS.Controls
{
    public class CancelledActivityView : UIView
    {
        public UILabel CancelText = new UILabel();
        public CancelledActivityView ()
        {
            BackgroundColor = CancelledActivity.CancelledFrameColor;
            Alpha = CancelledActivity.CancelledFrameAlpha;

            UIView insetView = new UIView();
            insetView.Alpha = CancelledActivity.CancelledFrameAlpha;
            insetView.BackgroundColor = CancelledActivity.InsetColor;

            CancelText.Font = CancelledActivity.CancelTextFont;
            CancelText.TextColor = CancelledActivity.CancelTextColor;
            CancelText.TextAlignment = UITextAlignment.Center;
            CancelText.Alpha = CancelledActivity.CancelledFrameAlpha;
            CancelText.AdjustsFontSizeToFitWidth = true;

            CancelText.Text = "hadjskajsh dkjashd kajshd kjashd kjash kjdas";

            AddSubviews(new [] { insetView, CancelText });

            Subviews.ForEach(sub => sub.TranslatesAutoresizingMaskIntoConstraints = false);

            this.AddConstraints(
                insetView.Width().EqualTo().WidthOf(this),
                insetView.Height().EqualTo().HeightOf(this).WithMultiplier(.92f),
                insetView.WithSameCenterX(this),
                insetView.WithSameCenterY(this),
                CancelText.WithSameCenterX(this),
                CancelText.WithSameCenterY(this).WithMultiplier(1.1f),
                CancelText.Width().EqualTo().WidthOf(this).WithMultiplier(.95f));            
        }
    }
    public class ExpiredActivityView : UIView
    {
        public UILabel CancelText = new UILabel();
        public ExpiredActivityView ()
        {
            BackgroundColor = CancelledActivity.CancelledFrameColor;
            Alpha = CancelledActivity.CancelledFrameAlpha;

            UIView insetView = new UIView();
            insetView.Alpha = CancelledActivity.CancelledFrameAlpha;
            insetView.BackgroundColor = CancelledActivity.InsetColor;

            CancelText.Font = CancelledActivity.CancelTextFont;
            CancelText.TextColor = CancelledActivity.CancelTextColor;
            CancelText.TextAlignment = UITextAlignment.Center;
            CancelText.Alpha = CancelledActivity.CancelledFrameAlpha;
            CancelText.AdjustsFontSizeToFitWidth = true;

            CancelText.Text = "hadjskajsh dkjashd kajshd kjashd kjash kjdas";

            AddSubviews(new [] { insetView, CancelText });

            Subviews.ForEach(sub => sub.TranslatesAutoresizingMaskIntoConstraints = false);

            this.AddConstraints(
                insetView.Width().EqualTo().WidthOf(this),
                insetView.Height().EqualTo().HeightOf(this).WithMultiplier(.92f),
                insetView.WithSameCenterX(this),
                insetView.WithSameCenterY(this),
                CancelText.WithSameCenterX(this),
                CancelText.WithSameCenterY(this).WithMultiplier(1.1f),
                CancelText.Width().EqualTo().WidthOf(this).WithMultiplier(.95f));            
        }
    }
}

