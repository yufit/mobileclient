﻿using System;

using Cirrious.FluentLayouts.Touch;

using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;

namespace YuFit.IOS.Controls.UserTableView
{
    public class UserInfoTableViewCell<TViewModel> : MvxTableViewCell
        where TViewModel : UserInfoViewModel
    {
        public UILabel NameLabel = new UILabel();
        public UILabel LocationLabel = new UILabel();
        public PictureContainer PictureView = new PictureContainer(7.0f);
        public IconView LocationIcon = new IconView();

        UIColor _contentColor = YuFitStyleKit.Yellow;
        public UIColor ContentColor {
            get { return _contentColor; }
            set { _contentColor = value; UpdateContent(); }
        }

        private string _sport;
        public string Sport {
            get { return _sport; }
            set { _sport = value; UpdateColor(); }
        }

        public UserInfoTableViewCell ()
        {
            BackgroundColor = UIColor.Clear;

            ContentView.AddSubviews(new UIView[] {NameLabel, PictureView, LocationIcon, LocationLabel});
            LocationIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawWhere1");
            PictureView.TransparentBackground = true;

            SetupConstraints();

            this.DelayBind(() => {
                Console.WriteLine ("Binding for {0}", ((UserInfoViewModel)DataContext).Name);
                NameLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 18);
                NameLabel.TextColor = YuFitStyleKit.Yellow;
                LocationLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 14);
                LocationLabel.TextColor = YuFitStyleKit.Yellow;

                var set = this.CreateBindingSet<UserInfoTableViewCell<TViewModel>, TViewModel>();
                set.Bind().For(me => me.Sport).To(vm => vm.Sport);
                set.Bind(PictureView.ImageViewLoader).To(friend => friend.AvatarUrl);
                set.Bind(NameLabel).To(friend => friend.Name);
                set.Bind(LocationLabel).To(friend => friend.Location);
                set.Apply ();
            });
        }

        void SetupConstraints ()
        {
            ContentView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            LocationIcon.AddConstraints(
                LocationIcon.Height().EqualTo(10.0f),
                LocationIcon.Width().EqualTo(10.0f)
            );

            ContentView.AddConstraints(
                PictureView.Height().EqualTo().HeightOf(ContentView),
                PictureView.Width().EqualTo().HeightOf(ContentView),
                PictureView.WithSameCenterY(ContentView),
                PictureView.Left().EqualTo(36.0f).LeftOf(ContentView),

                NameLabel.Left().EqualTo(10.0f).RightOf(PictureView),
                NameLabel.Right().EqualTo(-36.0f).RightOf(ContentView),
                NameLabel.Bottom().EqualTo(2.0f).CenterYOf(ContentView),

                LocationIcon.Left().EqualTo(10.0f).RightOf(PictureView),
                LocationIcon.Top().EqualTo(4.0f).CenterYOf(ContentView),

                LocationLabel.Left().EqualTo(5.0f).RightOf(LocationIcon),
                LocationLabel.Right().EqualTo(-36.0f).RightOf(ContentView),
                LocationLabel.Top().EqualTo(4.0f).CenterYOf(ContentView)

            );
        }

        void UpdateColor ()
        {
            if (!string.IsNullOrEmpty(Sport)) {
                ActivityColors colors = YuFitStyleKitExtension.ActivityBackgroundColors(Sport);
                ContentColor = colors.Light;
            }
        }

        protected virtual void UpdateContent ()
        {
            LocationLabel.TextColor = ContentColor;
            NameLabel.TextColor = ContentColor;
            LocationIcon.StrokeColor = ContentColor;
            PictureView.StrokeColor = ContentColor;
            PictureView.SetNeedsDisplay();
        }

        protected override void Dispose (bool disposing)
        {
            #if DEBUG_LEAKS
            Console.WriteLine("Deleting cell");
            #endif
            PictureView.Dispose();
            PictureView = null;
            base.Dispose(disposing);
        }

    }
}

