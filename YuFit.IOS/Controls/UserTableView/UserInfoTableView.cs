﻿using UIKit;


using YuFit.IOS.Themes;
using YuFit.IOS.Views;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Controls.UserTableView
{
    public class UserInfoTableView<TViewModel, TCell> : MvxTableViewController where TCell : UITableViewCell, new()
    {
        protected new TViewModel ViewModel { get { return (TViewModel) base.ViewModel; } }
        public TableViewSource<TCell> Source { get; set;}

        readonly string _cellKey;

        public UserInfoTableView (string cellKey)
        {
            _cellKey = cellKey;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            TableView.BackgroundColor = UIColor.Clear;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.RowHeight = 60;

            Source = new TableViewSource<TCell>(TableView, _cellKey);

            TableView.Source = Source;
        }

//        public override void ViewWillAppear (bool animated)
//        {
//            TableView.DeselectRow (TableView.IndexPathForSelectedRow, true);
//            base.ViewDidAppear(animated);
//        }
    }
}

