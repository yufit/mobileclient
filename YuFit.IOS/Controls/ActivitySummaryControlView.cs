﻿using System;
using Cirrious.FluentLayouts.Touch;
using UIKit;
using YuFit.Core.Extensions;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using YuFit.Core.Models;
using System.Collections.Generic;
using YuFit.Core.Interfaces.Services;
using System.Linq;
using MvvmCross.Platform;

namespace YuFit.IOS.Controls
{
    public class ActivitySummaryControlView : UIView
    {
        readonly TopBoxContainer        _topBox         = new TopBoxContainer();
        readonly SeparatorLine          _separator      = new SeparatorLine();
        readonly BottomBoxContainer     _bottomBox      = new BottomBoxContainer();

        private ActivityType _activityType;
        public ActivityType ActivityType {
            get { return _activityType; }
            set { _activityType = value; UpdateContent(); }
        }

        public ActivityIconView IconView
        {
            get { return _topBox.IconView; }
            set { /* nothing to do here */ }
        }

        DateTime _startTime;
        public DateTime StartTime {
            get { return _startTime; }
            set { _startTime = value; _topBox.WhenView.Date = _startTime; }
        }

        public string CreatedByLabel {
            get { return _bottomBox.CreatedByLabel.Text; }
            set { _bottomBox.CreatedByLabel.Text = value; }
        }

        public string CreatedBy {
            get { return _bottomBox.CreatorLabel.Text; }
            set { _bottomBox.CreatorLabel.Text = value; }
        }

        public string StreetAddress {
            get { return _bottomBox.AddressLabel.Text; }
            set { _bottomBox.AddressLabel.Text = value; _bottomBox.LocationIcon.SetNeedsDisplay(); }
        }

        public string SportDescription
        {
            get { return _bottomBox.SubSportLabel.Text; }
            set { _bottomBox.SubSportLabel.Text = value; }
        }

        private List<string> _details;
        public List<string> Details
        {
            get { return _details; }
            set { _details = value; SetDetails(); }
        }

        public ActivitySummaryControlView ()
        {
            AddSubviews(new UIView[] { _topBox, _separator, _bottomBox });
            Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            _topBox.AddConstraints(_topBox.Height().EqualTo(80.0f));
            _bottomBox.AddConstraints(_bottomBox.Height().EqualTo(73.5f));

            _separator.AddConstraints(_separator.Height().EqualTo(0.5f));

            this.AddConstraints(
                _topBox.WithSameWidth(this)   , _topBox.WithSameCenterX(this)   , _topBox.Top().EqualTo().TopOf(this),
                _separator.WithSameWidth(this), _separator.WithSameCenterX(this), _separator.Top().EqualTo().BottomOf(_topBox),
                _bottomBox.WithSameWidth(this), _bottomBox.WithSameCenterX(this), _bottomBox.Top().EqualTo().BottomOf(_separator)
            );

            _topBox.WhenView.DayLabelFont = CrudFitEvent.WhenDiamond.DayLabelTextFont;
            _topBox.WhenView.MonthAndDateLabelFont = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
            _topBox.WhenView.TimeLabelFont = CrudFitEvent.WhenDiamond.TimeLabelTextFont;



        }

        void UpdateContent ()
        {
            if (_activityType == null) { return; }
            
            UIColor colorBase = UIColor.White;
            UIColor colorDark = UIColor.White;

            var categories = Mvx.Resolve<IFitActivityService>().GetEventCategories();
            var category = categories.FirstOrDefault(c => c.Name == _activityType.TypeName);
            if (category != null) {
                var ccolors = category?.Colors ?? new Colors { 
                    Base = new Color { Red=255,Blue=255,Green=255 },
                    Dark = new Color { Red=255,Blue=255,Green=255 },
                    Light = new Color { Red=255,Blue=255,Green=255 }
                };
                colorBase = new UIColor(ccolors.Base.Red/255.0f, ccolors.Base.Green/255.0f, ccolors.Base.Blue/255.0f, 1.0f);
                colorDark = new UIColor(ccolors.Dark.Red/255.0f, ccolors.Dark.Green/255.0f, ccolors.Dark.Blue/255.0f, 1.0f);
            } else {
                ActivityColors colors;
                colors = YuFitStyleKitExtension.ActivityBackgroundColors(_activityType.TypeName);
                colorBase = colors.Base;
                colorDark = colors.Dark;
            }

            IconView.DrawIcon = YuFitStyleKitExtension.ActivityDrawingMethod(_activityType.TypeName);

            _topBox.BackgroundColor = colorBase;
            _bottomBox.BackgroundColor = colorDark;
            _separator.BackgroundColor = YuFitStyleKit.Charcoal;
        }

        private void SetDetails()
        {
            List<UILabel> details = new List<UILabel>();

            if (_details != null)
            {
                _details.ForEach(detail =>
                {
                    var label = new UILabel();
                    label.Text = detail;
                    details.Add(label);
                    _topBox.Specifics.AddSubview(label);
                });
            }
            else
            {
                _topBox.Specifics.Subviews.ForEach(s => s.RemoveFromSuperview());
            }

            var prevView = _topBox.Specifics;
            var prevAttr = NSLayoutAttribute.Top;
            float buffer = 0f;

            var detailsCount = details.Count;
            var it = 0;
            details.ForEach(d => 
            {
                d.Font = CrudFitEvent.ActivitySummaryControl.DetailsFont;
                d.TextColor = CrudFitEvent.ActivitySummaryControl.DetailsColor;
                
                d.TranslatesAutoresizingMaskIntoConstraints = false;

                _topBox.Specifics.AddConstraint(NSLayoutConstraint.Create(
                    d, NSLayoutAttribute.Top, NSLayoutRelation.Equal, prevView, prevAttr, 1, buffer));
                    
                _topBox.Specifics.AddConstraints(
                    d.WithSameCenterX(_topBox.Specifics));

                prevView = d;
                buffer = 0f;
                prevAttr = NSLayoutAttribute.Bottom;

                if (++it == detailsCount) {
                    d.Font = CrudFitEvent.WhenDiamond.DayLabelTextFont;
                }
            });

            _topBox.Specifics.AddConstraints(_topBox.Specifics.Bottom().EqualTo().BottomOf(prevView));

        }

        #region Top Box

        class TopBoxContainer : UIView
        {
            public readonly WhenView           WhenView       = new WhenView();
            public readonly ActivityIconView   IconView       = new ActivityIconView();
            public readonly UIView             Specifics      = new UIView();

            public TopBoxContainer ()
            {
                AddSubviews(new UIView[] {WhenView, IconView, Specifics});
                Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

                this.AddConstraints(
                    WhenView.Top().EqualTo(3.0f).TopOf(this),
                    WhenView.WithSameBottom(this),
                    WhenView.WithSameLeft(this),
                    WhenView.Width().EqualTo().WidthOf(this).WithMultiplier(0.33f),

                    IconView.WithSameCenterX(this),
                    IconView.WithSameCenterY(this),
                    IconView.Height().EqualTo().HeightOf(this).WithMultiplier(0.9f),
                    IconView.Width().EqualTo().HeightOf(this).WithMultiplier(0.9f),

                    //Specifics.WithSameTop(this),
                    //Specifics.WithSameBottom(this),
                    Specifics.WithSameCenterY(this),
                    Specifics.WithSameRight(this),
                    Specifics.Width().EqualTo().WidthOf(this).WithMultiplier(0.33f)
                );
            }
        }

        #endregion

        #region Bottom Box

        class BottomBoxContainer : UIView
        {
            public readonly UILabel     SubSportLabel       = new UILabel();

            readonly UIView             _locationContainer  = new UIView();
            public readonly IconView    LocationIcon        = new IconView();
            public readonly UILabel     AddressLabel        = new UILabel();

            readonly UIView             _createdByContainer = new UIView();
            readonly IconView           _createdByIcon      = new IconView();
            public readonly UILabel     CreatedByLabel      = new UILabel();
            public readonly UILabel     CreatorLabel        = new UILabel();

            public BottomBoxContainer ()
            {
                AddSubviews(new UIView[] {SubSportLabel, _locationContainer, _createdByContainer});
                Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

                this.AddConstraints(
                    _locationContainer.WithSameCenterX(this),
                    _locationContainer.CenterY().EqualTo(3.0f).CenterYOf(this),

                    SubSportLabel.Top().EqualTo(5.0f).TopOf(this),
                    SubSportLabel.WithSameCenterX(this),

                    _createdByContainer.Top().EqualTo().BottomOf(_locationContainer),
                    _createdByContainer.WithSameCenterX(this)
                );

                ConfigureLocationContainer();
                ConfigureCreatedByContainer();

                SubSportLabel.Font = CrudFitEvent.ActivitySummaryControl.SubSportFont;
                SubSportLabel.TextColor = CrudFitEvent.ActivitySummaryControl.SubSportColor;
                AddressLabel.Font = CrudFitEvent.ActivitySummaryControl.AddressFont;
                AddressLabel.TextColor = CrudFitEvent.ActivitySummaryControl.AddressColor;
                CreatedByLabel.Font = CrudFitEvent.ActivitySummaryControl.CreatedByFont;
                CreatedByLabel.TextColor = CrudFitEvent.ActivitySummaryControl.CreatedByColor;
                CreatorLabel.Font = CrudFitEvent.ActivitySummaryControl.CreatorFont;
                CreatorLabel.TextColor = YuFitStyleKit.Charcoal;
            }

            void ConfigureLocationContainer ()
            {
                _locationContainer.AddSubviews(new UIView[] {LocationIcon, AddressLabel});
                _locationContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

                LocationIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawWhere1");

                _locationContainer.AddConstraints(
                    LocationIcon.Left().EqualTo().LeftOf(_locationContainer),
                    LocationIcon.WithSameCenterY(_locationContainer),
                    LocationIcon.Height().EqualTo(3.0f).HeightOf(AddressLabel),
                    LocationIcon.Width().EqualTo(3.0f).HeightOf(AddressLabel),

                    AddressLabel.Left().EqualTo(5.0f).RightOf(LocationIcon),
                    AddressLabel.CenterY().EqualTo(2.0f).CenterYOf(_locationContainer),

                    _locationContainer.Right().EqualTo().RightOf(AddressLabel),
                    _locationContainer.Bottom().EqualTo().BottomOf(LocationIcon)
                );
            }

            void ConfigureCreatedByContainer ()
            {
                _createdByContainer.AddSubviews(new UIView[] {_createdByIcon, CreatedByLabel, CreatorLabel});
                _createdByContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

                _createdByIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawWho2");

                _createdByContainer.AddConstraints(
                    _createdByIcon.Left().EqualTo().LeftOf(_createdByContainer),
                    _createdByIcon.WithSameCenterY(_createdByContainer),
                    _createdByIcon.Height().EqualTo(3.0f).HeightOf(CreatedByLabel),
                    _createdByIcon.Width().EqualTo(3.0f).HeightOf(CreatedByLabel),

                    CreatedByLabel.Left().EqualTo(5.0f).RightOf(_createdByIcon),
                    CreatedByLabel.CenterY().EqualTo(2.0f).CenterYOf(_createdByContainer),

                    CreatorLabel.Left().EqualTo(5.0f).RightOf(CreatedByLabel),
                    CreatorLabel.CenterY().EqualTo(2.0f).CenterYOf(_createdByContainer),

                    _createdByContainer.Right().EqualTo().RightOf(CreatorLabel),
                    _createdByContainer.Bottom().EqualTo().BottomOf(_createdByIcon)
                );
            }
        }

        #endregion 
    }
}

