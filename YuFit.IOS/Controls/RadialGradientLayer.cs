﻿using System;
using CoreGraphics;
using UIKit;
using YuFit.IOS.Utilities;
using Foundation;

namespace YuFit.IOS.Views
{
    [Register ("RadialGradientView")]
    public sealed class RadialGradientView : UIView
    {
        public RadialGradientView ()
        {
            SetNeedsDisplay();
            UserInteractionEnabled = false;
        }

        public override void Draw (CGRect rect)
        {
            nfloat[] gradLocations = new nfloat[] {1.0f, 0.0f};
            nfloat[] gradColors = new nfloat[] {14.0f/255.0f,12.0f/255.0f,0.0f,1.0f,42.0f/255.0f,33.0f/255.0f,0.0f,1.0f};

            CGColorSpace colorSpace = CGColorSpace.CreateDeviceRGB();
            CGGradient gradient = new CGGradient(colorSpace, gradColors, gradLocations);

            CGPoint gradCenter = new CGPoint(Bounds.Size.Width/2, Bounds.Size.Height/2);
            float gradRadius = (float) Math.Min(Bounds.Size.Width-50.0f, Bounds.Size.Height-200.0f) ;

            using (var ctx = new CGContextHelper(UIGraphics.GetCurrentContext())) {
                UIGraphics.GetCurrentContext().DrawRadialGradient (gradient, gradCenter, 0, gradCenter, gradRadius, CGGradientDrawingOptions.DrawsAfterEndLocation);
            }
            base.Draw(rect);
        }

//        UIImage CreateRadialGradientImage(CGSize size, UIColor start, UIColor end, CGPoint center, nfloat radius)
//        {
//            UIGraphics.BeginImageContextWithOptions(size, true, 1.0f);
//            int numLocation = 2;
//            nfloat[] gradLocations = new nfloat[] {0.0f, 1.0f};
//
//            nfloat startR, startG, startB, startA;
//            start.GetRGBA(out startR, out startG, out startB, out startA);
//
//            nfloat endR, endG, endB, endA;
//            end.GetRGBA(out endR, out endG, out endB, out endA);
//            nfloat[] gradColors = new nfloat[] {startR, startG, startB, startA, endR, endG, endB, endA};
//
//            CGColorSpace colorSpace = CGColorSpace.CreateDeviceRGB();
//            CGGradient gradient = new CGGradient(colorSpace, gradColors, gradLocations);
//
//            CGPoint gradCenter = new CGPoint(center.X * Bounds.Size.Width/2, center.Y * Bounds.Size.Height/2);
//            nfloat gradRadius = (nfloat) Math.Min(Bounds.Size.Width, Bounds.Size.Height) * radius;
//
//            UIGraphics.GetCurrentContext().DrawRadialGradient (gradient, gradCenter, 0, gradCenter, gradRadius, CGGradientDrawingOptions.DrawsAfterEndLocation);
//            UIImage image = UIGraphics.GetImageFromCurrentImageContext();
//
//            UIGraphics.EndImageContext();
//            return image;
//        }
    }
}

//    public sealed class RadialGradientLayer : CAGradientLayer
//    {
//        public RadialGradientLayer ()
//        {
//            SetNeedsDisplay();
//        }
//
//        public override void DrawInContext (CoreGraphics.CGContext ctx)
//        {
//            nfloat[] gradLocations = new nfloat[] {0.0f, 1.0f};
//            nfloat[] gradColors = new nfloat[] {0.0f,0.0f,0.0f,1.0f,1.0f,1.0f,1.0f,1.0f};
//
//            CGColorSpace colorSpace = CGColorSpace.CreateDeviceRGB();
//            CGGradient gradient = new CGGradient(colorSpace, gradColors, gradLocations);
//
//            CGPoint gradCenter = new CGPoint(Bounds.Size.Width/2, Bounds.Size.Height/2);
//            float gradRadius = (float) Math.Min(Bounds.Size.Width, Bounds.Size.Height) ;
//
//            ctx.DrawRadialGradient (gradient, gradCenter, 0, gradCenter, gradRadius, CGGradientDrawingOptions.DrawsAfterEndLocation);
//            base.DrawInContext(ctx);
//        }
//    }
//


//
//(UIImage *)radialGradientImage:(CGSize)size start:(float)start end:(float)end centre:(CGPoint)centre radius:(float)radius {
//    // Render a radial background
//    // http://developer.apple.com/library/ios/#documentation/GraphicsImaging/Conceptual/drawingwithquartz2d/dq_shadings/dq_shadings.html
//
//    // Initialise
//    UIGraphicsBeginImageContextWithOptions(size, YES, 1);
//
//    // Create the gradient's colours
//    size_t num_locations = 2;
//    CGFloat locations[2] = { 0.0, 1.0 };
//    CGFloat components[8] = { start,start,start, 1.0,  // Start color
//        end,end,end, 1.0 }; // End color
//
//    CGColorSpaceRef myColorspace = CGColorSpaceCreateDeviceRGB();
//    CGGradientRef myGradient = CGGradientCreateWithColorComponents (myColorspace, components, locations, num_locations);
//
//    // Normalise the 0-1 ranged inputs to the width of the image
//    CGPoint myCentrePoint = CGPointMake(centre.x * size.width, centre.y * size.height);
//    float myRadius = MIN(size.width, size.height) * radius;
//
//    // Draw it!
//    CGContextDrawRadialGradient (UIGraphicsGetCurrentContext(), myGradient, myCentrePoint,
//        0, myCentrePoint, myRadius,
//        kCGGradientDrawsAfterEndLocation);
//
//    // Grab it as an autoreleased image
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//
//    // Clean up
//    CGColorSpaceRelease(myColorspace); // Necessary?
//    CGGradientRelease(myGradient); // Necessary?
//    UIGraphicsEndImageContext(); // Clean up
//    return image;
//}
