using CoreGraphics;
using UIKit;
using YuFit.IOS.Utilities;
using System;

namespace YuFit.IOS.Controls
{
    public class LogoView : UIView
    {
        public float Inset { get; set; }
        public bool UseCenteredVersion { get; set; }
        protected CGRect InsetRect {
            get {
                //  Square it.
                var bounds = Bounds;
                if (bounds.Width > bounds.Height) { 
                    bounds.Width = bounds.Height; 
                    bounds.X = (Bounds.Width-bounds.Width)/2.0f;
                } else if (bounds.Height > bounds.Width) { 
                    bounds.Height = bounds.Width; 
                    bounds.Y = (Bounds.Height-bounds.Height)/2.0f;
                }

                if (Math.Abs(bounds.Width) < 0.1f || Math.Abs(bounds.Height) < 0.1f) {
                    return bounds;
                }
                return bounds.Inset(Inset, Inset);
            }
        }


        public LogoView ()
        {
            BackgroundColor = UIColor.Clear;
            Inset = 0.0f;
        }

        public override void Draw (CGRect rect)
        {
            using (new CGContextHelper(UIGraphics.GetCurrentContext())) {
                if (UseCenteredVersion) {
                    YuFit.IOS.Themes.YuFitStyleKit.DrawCenteredLogo(InsetRect);
                } else {
                    YuFit.IOS.Themes.YuFitStyleKit.DrawLogo(InsetRect);
                }
            }
        }
    }
}

