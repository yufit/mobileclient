﻿using System;
using System.Collections.Generic;
using UIKit;
using Foundation;
using CoreGraphics;
using SWTableViewCell;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Bindings;

namespace YuFit.IOS.Controls
{
    public class MvxSwipeTableViewCell : SWTableViewCell.SWTableViewCell, IMvxBindable  
    {
        #region Properties

        public IMvxBindingContext BindingContext { get; set; }

        public object DataContext
        {
            get { return BindingContext.DataContext; }
            set { BindingContext.DataContext = value; }
        }

        #endregion


        #region Construction 

        public MvxSwipeTableViewCell()
            : this(string.Empty)
        {
        }

        public MvxSwipeTableViewCell(string bindingText)
        {
            this.CreateBindingContext(bindingText);
        }

        public MvxSwipeTableViewCell(IEnumerable<MvxBindingDescription> bindingDescriptions)
        {
            this.CreateBindingContext(bindingDescriptions);
        }

        public MvxSwipeTableViewCell(IntPtr handle)
            : this(string.Empty, handle)
        {
        }

        public MvxSwipeTableViewCell(string bindingText, IntPtr handle)
            : base(handle)
        {
            this.CreateBindingContext(bindingText);
        }

        public MvxSwipeTableViewCell(IEnumerable<MvxBindingDescription> bindingDescriptions, IntPtr handle)
            : base(handle)
        {
            this.CreateBindingContext(bindingDescriptions);
        }

        public MvxSwipeTableViewCell(string bindingText, UITableViewCellStyle cellStyle, NSString cellIdentifier,
            UITableViewCellAccessory tableViewCellAccessory =
            UITableViewCellAccessory.None)
            : base(cellStyle, cellIdentifier)
        {
            Accessory = tableViewCellAccessory;
            this.CreateBindingContext(bindingText);
        }

        public MvxSwipeTableViewCell(IEnumerable<MvxBindingDescription> bindingDescriptions,
            UITableViewCellStyle cellStyle, NSString cellIdentifier,
            UITableViewCellAccessory tableViewCellAccessory =
            UITableViewCellAccessory.None)
            : base(cellStyle, cellIdentifier)
        {
            // note that we allow the virtual Accessory property to be set here - but do not seal
            // it. Previous `sealed` code caused odd, unexplained behaviour in MonoTouch
            // - see https://github.com/MvvmCross/MvvmCross/issues/524
            Accessory = tableViewCellAccessory;
            this.CreateBindingContext(bindingDescriptions);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                BindingContext.ClearAllBindings();
            }
            base.Dispose(disposing);
        }

        #endregion 


    }
}

