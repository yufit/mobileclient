using System;
using CoreGraphics;
using UIKit;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Controls
{
    public class ApplyTriangle : UIView
    {
        public UIColor TriangleBackgroundColor { get; set; }
        public UIColor TriangleForegroundColor { get; set; }
        public bool ShowDownTriangle { get; set; }

        private float _percentFill;
        public float PercentFill
        {
            get { return _percentFill; }
            set {
                if (Math.Abs(value - 1.0f) < 0.01f) {
                    _percentFill = value;
//                    SetNeedsDisplay();
                }
            }
        }

        public ApplyTriangle ()
        {
            BackgroundColor = UIColor.Clear;
            TriangleBackgroundColor = Themes.CrudFitEvent.ItsOnView.TriangleBackgroundColor;
            TriangleForegroundColor = Themes.CrudFitEvent.ItsOnView.TriangleFillColor;

            Layer.ShadowColor = UIColor.Black.CGColor;
            Layer.ShadowOpacity = 0.6f;
            Layer.RasterizationScale = UIScreen.MainScreen.Scale;
            Layer.ShouldRasterize = true;
            Layer.ShadowOffset = new CGSize(0.0f, 0.0f);
            Layer.ShadowRadius = 5.0f;

        }

        public override void Draw(CGRect rect)
        {
            using (UIGraphics.GetCurrentContext()) {
                var downArrowColor = YuFitStyleKit.White;
                if (!ShowDownTriangle) {
                    downArrowColor = TriangleForegroundColor;
                }


                YuFitStyleKit.DrawApplyTriangle(Bounds, downArrowColor, TriangleForegroundColor);
            }
        }
    }
}

