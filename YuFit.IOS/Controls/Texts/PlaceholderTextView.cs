﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;

namespace YuFit.IOS.Controls
{
    [Register("PlaceholderTextView")]
    public class PlaceholderTextView : UITextView
    {
        bool _editing = false;

        public override string Text
        {
            get { return base.Text; }
            set { 
                if (_editing || (!_editing && !string.IsNullOrEmpty(value))) {
                    base.Text = value;
                    Console.WriteLine("Text is now {0}", value);
                }
            }
        }

        // We use a custom keyboard on top of the   normal keyboard, where we provide
        // custom shortcut word.  When  the user select such word, they don't trigger
        // the UITextView Changed event which in turn doesn't updated the Mvx bounded
        // viewmodel string.  Therefore, we use this property instead to databind and
        // we manually fire the event when the user end editing.
        string _data;
        public string Data {
            get { return _data; }
            set { _data = value; Text = value; }
        }
        public event EventHandler DataChanged;

        private string _placeHolder;
        public string Placeholder {
            get { return _placeHolder; }
            set { _placeHolder = value;  UpdateValue(); }
        }

        public PlaceholderTextView () { Initialize (); }
        public PlaceholderTextView (CGRect frame) : base(frame) { Initialize (); }
        public PlaceholderTextView (IntPtr handle) : base(handle) { Initialize (); }

        private void Initialize()
        {
            ShouldBeginEditing = t => {
                _editing = true;
                if (Text == Placeholder) { 
                    Text = string.Empty; 
                }
                return true;
            };
            ShouldEndEditing = t => { 
                _editing = false;
                Data = Text;
                if (DataChanged != null) {
                    DataChanged(this, EventArgs.Empty);
                }
                UpdateValue(); 
                return true; 
            };
        }

        void UpdateValue ()
        {
            if (!_editing && string.IsNullOrEmpty (Text)) {
                Text = Placeholder;
            }
        }
    }
}

