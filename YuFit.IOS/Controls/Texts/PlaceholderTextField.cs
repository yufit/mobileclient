using Foundation;
using UIKit;

namespace YuFit.IOS.Controls
{
    public class PlaceholderTextField : UITextField
    {
        private UIFont _placeholderFont;
        public UIFont PlaceholderFont { 
            get { return _placeholderFont; }
            set { _placeholderFont = value; UpdatePlaceholder(); }
        }

        private UIColor _placeholderColor;
        public UIColor PlaceholderColor { 
            get { return _placeholderColor; }
            set { _placeholderColor = value; UpdatePlaceholder(); } 
        }

        public override string Placeholder
        {
            get { return base.Placeholder; }
            set { base.Placeholder = value; UpdatePlaceholder(); }
        }

        void UpdatePlaceholder ()
        {
            if (_placeholderFont != null && _placeholderColor != null && !string.IsNullOrEmpty(Placeholder)) {
                AttributedPlaceholder = new NSAttributedString(
                    (string) Placeholder, 
                    _placeholderFont,
                    _placeholderColor
                );
            }
        }
    }
    
}
