﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Controls
{
    [Register ("ActivityIconView")]
    public class ActivityIconView : UIView
    {
        public float Padding { get; set; }

        Action<CGRect, UIColor> _drawIcon;
        public Action<CGRect, UIColor> DrawIcon { 
            get { return _drawIcon; }
            set { _drawIcon = value; SetNeedsDisplay(); }
        }

        public UIColor IconColor { get; set; }

        public ActivityIconView () { DoConstruction(); }
        public ActivityIconView (CGRect rectangle) : base(rectangle) { DoConstruction(); }
        public ActivityIconView (IntPtr handle) : base(handle) { DoConstruction(); }

        void DoConstruction()
        {
            BackgroundColor = UIColor.Clear;
            Padding = 0.0f;
            IconColor = YuFitStyleKit.White;
        }

        public override void Draw (CGRect rect)
        {
            base.Draw(rect);

            if (DrawIcon != null) {
                using (var ctx = UIGraphics.GetCurrentContext()) {
                    CGRect imageRect = Bounds.Inset(Padding*Bounds.Width, Padding*Bounds.Height);
                    DrawIcon(imageRect, IconColor);
                }
            }
        }

    }

}

