using System;

using CoreAnimation;
using CoreGraphics;
using UIKit;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Controls
{

    public class PointyView : UIView 
    {
        private UIBezierPath _viewPath;
        private CAShapeLayer _viewShapeLayer;
        public PointyView (CGRect rect) : base(rect) {
            BackgroundColor = UIColor.Clear;
            ComputeViewPath();
        }

        public override void Draw (CGRect rect)
        {
            ComputeViewPath ();
            YuFitStyleKit.Black.SetFill();
            _viewPath.Fill();
        }

        void ComputeViewPath ()
        {
            nfloat diamondHalfHeight = Bounds.Width / 2.0f;
            nfloat diamondStartY = Bounds.Height - diamondHalfHeight;

            _viewPath = new UIBezierPath();
            _viewPath.MoveTo(new CGPoint(0.0f, 0.0f));
            _viewPath.AddLineTo(new CGPoint(Bounds.Width, 0.0f));
            _viewPath.AddLineTo(new CGPoint(Bounds.Width, diamondStartY));
            _viewPath.AddLineTo(new CGPoint(Bounds.Width/2.0f, Bounds.Height));
            _viewPath.AddLineTo(new CGPoint(0.0f, diamondStartY));
            _viewPath.AddLineTo(new CGPoint(0.0f, 0.0f));
            _viewPath.ClosePath();

            _viewShapeLayer = new CAShapeLayer();
            _viewShapeLayer.Path = _viewPath.CGPath;
            _viewShapeLayer.FillColor = YuFitStyleKit.Blue.CGColor;
            _viewShapeLayer.StrokeColor = YuFitStyleKit.Blue.CGColor;
            _viewShapeLayer.LineWidth = 0.0f;
            Layer.Mask = _viewShapeLayer;
        }
    }
    
}
