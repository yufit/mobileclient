using System;
using UIKit;
using Foundation;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Controls
{
    [Register("CheckBox")]
    public class CheckBox : UIView
    {
        public UIColor SelectedColor { get; set; }
        public UIColor UnselectedColor { get; set; }

        private bool _isSelected;
        public bool IsSelected {
            get { return _isSelected; }
            set { _isSelected = value; SetNeedsDisplay(); }
        }

        public CheckBox (IntPtr handle) : base(handle)
        {
            BackgroundColor = UIColor.Clear;
            SelectedColor = YuFitStyleKit.Green;
            UnselectedColor = YuFitStyleKit.Pink;
        }

        public CheckBox()
        {
            BackgroundColor = UIColor.Clear;
            SelectedColor = YuFitStyleKit.Green;
            UnselectedColor = YuFitStyleKit.Pink;
        }

        public override void Draw (CoreGraphics.CGRect rect)
        {
            if (IsSelected) { YuFitStyleKit.DrawSelected(SelectedColor); }
            else            { YuFitStyleKit.DrawNotSelected(UnselectedColor); }
        }
    }
}

