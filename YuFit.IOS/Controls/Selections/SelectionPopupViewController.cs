using System;
using UIKit;
using System.Collections.Generic;
using CoreGraphics;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Controls.Selections
{
    public class SelectionPopupViewController : UIViewController
    {
        const float LabelHeight = 30.0f;
        readonly UIScrollView _scrollView = new UIScrollView();
        readonly List<UIView> _labelContainers = new List<UIView>();
        readonly List<UILabel> _labels = new List<UILabel>();

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="YuFit.IOS.Views.CreateActivity.SelectActivity.Activities.Utils.SelectionPopupViewController"/> class.
        /// </summary>
        /// <param name="options">Options.</param>
        /// <param name="oddColor">Odd color.</param>
        /// <param name="evenColor">Even color.</param>
        /// <param name="selectedAction">Selected action.</param>
        public SelectionPopupViewController (
            List<string> options, 
            UIColor oddColor, UIColor evenColor, 
            Action<int> selectedAction)
        {
            for (int i=0; i<options.Count; ++i) {
                var option = options[i];
                UIView view = new UIView(new CGRect(0, i*LabelHeight, 400, LabelHeight));
                view.BackgroundColor = (i%2) == 1 ? oddColor : evenColor;

                // TODO Use a theme font option.
                UILabel label = new UILabel(new CGRect(0, 3, 400, LabelHeight));
                label.Text = option.ToUpper();
                label.TextAlignment = UITextAlignment.Center;
                label.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 18);
                label.TextColor = YuFitStyleKit.White;
                view.Add(label);

                _labels.Add(label);
                _labelContainers.Add(view);
            }

            View.Add(_scrollView);
            _scrollView.AddSubviews(_labelContainers.ToArray());
            _scrollView.ContentSize = new CGSize(0, options.Count * LabelHeight);

            // We need to set the content size in constructor for the popover to
            // pick it up right.  We  will adjust  the width when laying out our
            // subviews  to match  the proper  width specified by the popover to
            // ensure we are not larger than the screen itself.
            PreferredContentSize = new CGSize(400, options.Count * LabelHeight);

            View.AddGestureRecognizer(new UITapGestureRecognizer(recognizer => {
                CGPoint tap = recognizer.LocationInView(_scrollView);
                selectedAction((int) (tap.Y / LabelHeight));
            }));
        }

        /// <summary>
        /// Views the will appear.  In here we remove the corner radius setting
        /// that the ios uses to make the corner round.
        /// </summary>
        /// <param name="animated">If set to <c>true</c> animated.</param>
        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear(animated);
            View.Superview.Layer.CornerRadius = 0;
        }

        /// <summary>
        /// We resize our views to ensure we are not larger than the width given
        /// to us by the ios popover creation code.
        /// </summary>
        public override void ViewWillLayoutSubviews ()
        {
            base.ViewWillLayoutSubviews();
            _scrollView.Frame = new CGRect(0, 0, View.Frame.Width, View.Frame.Height);

            for (int i=0; i<_labelContainers.Count; ++i) {
                var labelContainer = _labelContainers[i];
                var label = _labels[i];
                labelContainer.Frame = new CGRect(0, i*LabelHeight, View.Frame.Width,LabelHeight);
                label.Frame = new CGRect(0, label.Frame.Y, View.Frame.Width,LabelHeight);
            }
        }
    }
}

