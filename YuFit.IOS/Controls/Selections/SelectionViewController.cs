using System;
using System.Collections.Generic;
using System.Linq;

using Cirrious.FluentLayouts.Touch;
using CoreGraphics;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Helpers;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Controls.Selections
{
    public class SelectionViewControllerView : UIView {}
    public class SelectionViewController<T, TEnum> : UIViewController
    {
        private readonly SeparatorLine    _topLine     = new SeparatorLine();
        private readonly UILabel          _label       = new UILabel();
        private readonly SeparatorLine    _bottomLine  = new SeparatorLine();

        public bool ShowSeparatorLine { get; set; }

        public UIFont LabelFont { get { return _label.Font; } set { _label.Font = value; } }
        public UIColor LabelTextColor { get { return _label.TextColor; } set { _label.TextColor = value; } }
        public UITextAlignment LabelTextAlignment { get { return _label.TextAlignment; } set { _label.TextAlignment = value; } }

        private UIViewController _popover;
        private UITapGestureRecognizer _tapGesture;

        public ActivityColors Colors { get; set; }

        private SelectItemInListCommandViewModel<T, TEnum> _selectItemInListCommand;
        public SelectItemInListCommandViewModel<T, TEnum> SelectItemInListCommand {
            get{
                return _selectItemInListCommand;
            }
            set {
                _selectItemInListCommand = value;
                if (_selectItemInListCommand != null)
                {
                    int index = (int) Enum.ToObject(typeof(TEnum), _selectItemInListCommand.DefaultSelection);
                    _label.Text = (string) (_selectItemInListCommand.SelectableItems[index] as object);
                }
            }
        }

        private TEnum _currentSelection;
        public TEnum CurrentSelection {
            get { return _currentSelection; }
            set { 
                _currentSelection = value;
                int index = (int) Enum.ToObject(typeof(TEnum), _currentSelection);
                _label.Text = (string) (_selectItemInListCommand.SelectableItems[index] as object);
            }
        }

        public SelectionViewController ()
        {
            LabelFont = CrudFitEvent.ConfigView.IntensityFont;
            LabelTextColor = YuFitStyleKit.White;
            LabelTextAlignment = UITextAlignment.Center;
        }

        public override void ViewDidLoad ()
        {
            View = new SelectionViewControllerView();
            base.ViewDidLoad();

            Add(_label);

            UIView[] views = { _topLine, _label, _bottomLine };
            View.AddSubviews(views);

            views.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;
                View.AddConstraints(v.WithSameCenterX(View));
            });

            views.Where(v => v is SeparatorLine).ForEach(v => {
                v.AddConstraints(v.Height().EqualTo(1.5f));
                View.AddConstraints(v.Width().EqualTo().WidthOf(View));
                v.BackgroundColor = ShowSeparatorLine ? Colors.Base : UIColor.Clear;
            });

            views.Where(v => v is UILabel).ForEach(v => {
//                (v as UILabel).Font = CrudFitEvent.ConfigView.IntensityFont;
//                (v as UILabel).TextColor = YuFitStyleKit.White;
//                (v as UILabel).TextAlignment = UITextAlignment.Center;
                View.AddConstraints(v.Width().EqualTo().WidthOf(View));
            });

            View.AddConstraints(
                _topLine.Top().EqualTo().TopOf(View),
                _label.Top().EqualTo(5.0f).BottomOf(_topLine),
                _bottomLine.Top().EqualTo(2.0f).BottomOf(_label),
                _bottomLine.Bottom().EqualTo().BottomOf(View)
            );

            View.UserInteractionEnabled = true;

            _tapGesture = new UITapGestureRecognizer(DisplaySelections);
        }

        public override void ViewDidAppear (bool animated)
        {
            View.AddGestureRecognizer(_tapGesture);
            base.ViewDidAppear(animated);
        }

        public override void ViewDidDisappear (bool animated)
        {
            View.RemoveGestureRecognizer(_tapGesture);
            if (_popover != null) {
                _popover.Dispose();
                _popover = null;
            }
            base.ViewDidDisappear(animated);
        }

        void DisplaySelections ()
        {
            List<string> data = new List<string>();
            SelectItemInListCommand.SelectableItems.ForEach(item => data.Add(item as string));
            _popover = new SelectionPopupViewController(data, Colors.Dark, Colors.Base, HandleSelectionChanged);
            _popover.View.BackgroundColor = Colors.Base;
            _popover.View.Frame = new CGRect(0, 0, 100, 100);
            _popover.ModalPresentationStyle = UIModalPresentationStyle.Popover;

            UIPopoverPresentationController popoverController = _popover.PopoverPresentationController;
            popoverController.Delegate = new PopoverPresentationControllerDelegate();
            popoverController.SourceRect = View.Frame;
            popoverController.SourceView = View.Superview;
            popoverController.BackgroundColor = Colors.Base;
            popoverController.PermittedArrowDirections = UIPopoverArrowDirection.Down;

            _popover.ModalPresentationStyle = UIModalPresentationStyle.Popover;
            ParentViewController.PresentViewController(_popover, true, null);
        }

        void HandleSelectionChanged (int selection)
        {
            SelectItemInListCommand.Command.Execute(selection);
            _label.Text = (string) (_selectItemInListCommand.SelectableItems[selection] as object);
            _popover.DismissViewController(true, null);
            _popover.Dispose();
            _popover = null;
        }
    }

    public class PopoverPresentationControllerDelegate : UIPopoverPresentationControllerDelegate
    {
        public override UIModalPresentationStyle GetAdaptivePresentationStyle(UIPresentationController forPresentationController)
        {
            return UIModalPresentationStyle.None;
        }
    }

    public class StringSelectionVC<TEnum> : SelectionViewController<string, TEnum> {}
}
