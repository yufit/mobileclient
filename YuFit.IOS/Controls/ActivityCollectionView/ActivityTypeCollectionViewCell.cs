using System;


using CoreGraphics;
using Foundation;
using UIKit;

using YuFit.Core.ViewModels.Activity;

using YuFit.IOS.Themes;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;

namespace YuFit.IOS.Controls.ActivityCollectionView
{
    public class ActivityTypeCollectionViewCell : MvxCollectionViewCell
    {
        public static readonly UINib Nib = UINib.FromName("ActivityTypeCollectionViewCell", NSBundle.MainBundle);
        public static readonly NSString Key = new NSString("ActivityTypeCollectionViewCell");

        protected ActivityTypeItemView _activityTypeItemView;

        private string _activityType;
        public string ActivityType {
            get { return _activityType; }
            set { _activityType = value; UpdateIcon(); }
        }

        private bool _isEnabled;
        public bool IsEnabled {
            get { return _isEnabled; }
            set { _isEnabled = value; UpdateColors(); }
        }

        private bool _isSelected;
        public bool IsSelected {
            get { return _isSelected; }
            set { _isSelected = value; UpdateColors(); }
        }

        public ActivityTypeCollectionViewCell (IntPtr handle) : base(handle)
        {

            ContentView.Frame = Bounds;

            _activityTypeItemView = new ActivityTypeItemView(ContentView.Frame);
            _activityTypeItemView.StrokeWidth = 0.0f;
            _activityTypeItemView.DiamondColor = UIColor.Clear;
            ContentView.Add(_activityTypeItemView);

            this.DelayBind(() => {
                var set = this.CreateBindingSet<ActivityTypeCollectionViewCell, ActivityTypeCellViewModel>();
                set.Bind(this).For(me => me.ActivityType).To(vm => vm.TypeName);
                set.Bind(this).For(me => me.IsEnabled).To(vm => vm.IsEnabled);
                set.Bind(this).For(me => me.IsSelected).To(vm => vm.IsSelected);
                set.Apply();
            });
        }

        public override void UpdateConstraints ()
        {
            base.UpdateConstraints();
            ContentView.Frame = Bounds;
            _activityTypeItemView.Frame = ContentView.Bounds;
            _activityTypeItemView.UpdateDiamondShapeLayerMask();
            _activityTypeItemView.SetNeedsDisplay();
        }

        public override void LayoutSubviews ()
        {
            ContentView.Frame = Bounds;
            _activityTypeItemView.Frame = ContentView.Bounds;
            _activityTypeItemView.UpdateDiamondShapeLayerMask();
            _activityTypeItemView.SetNeedsDisplay();
            base.LayoutSubviews();
        }

        public override UIView HitTest (CGPoint point, UIEvent uievent)
        {
            return _activityTypeItemView.HitTest(point, uievent);
        }

        public static ActivityTypeCollectionViewCell Create ()
        {
            return (ActivityTypeCollectionViewCell) Nib.Instantiate(null, null)[0];
        }

        protected virtual void UpdateIcon() 
        {
            _activityTypeItemView.DrawActivityIcon = YuFitStyleKitExtension.ActivityDrawingMethod(_activityType);

            _activityTypeItemView.UpdateDiamondShapeLayerMask();
            _activityTypeItemView.SetNeedsDisplay();
        }

        protected virtual void UpdateColors() 
        {
            if (IsEnabled && IsSelected) {
                ActivityColors colors = YuFitStyleKitExtension.ActivityBackgroundColors(_activityType);
                _activityTypeItemView.DiamondColor = colors.Base;
                _activityTypeItemView.StrokeWidth = 3.0f;
                _activityTypeItemView.StrokeColor = colors.Base;
                _activityTypeItemView.ActivityFillColor = YuFitStyleKit.White;
                _activityTypeItemView.SetNeedsDisplay();
            } else if (IsEnabled && !IsSelected) {
                ActivityColors colors = YuFitStyleKitExtension.ActivityBackgroundColors(_activityType);
                _activityTypeItemView.DiamondColor = colors.Base;
                _activityTypeItemView.StrokeWidth = 3.0f;
                _activityTypeItemView.StrokeColor = colors.Dark;
                _activityTypeItemView.ActivityFillColor = YuFitStyleKit.White;
                _activityTypeItemView.SetNeedsDisplay();
            } else {
                _activityTypeItemView.DiamondColor = YuFitStyleKit.Black;
                _activityTypeItemView.TransparentBackground = true;
                _activityTypeItemView.StrokeWidth = 1.0f;
                _activityTypeItemView.StrokeColor = YuFitStyleKit.ActivityDisabledColor;
                _activityTypeItemView.ActivityFillColor = YuFitStyleKit.ActivityDisabledColor;
                _activityTypeItemView.SetNeedsDisplay();
            }
        }
    }
}

