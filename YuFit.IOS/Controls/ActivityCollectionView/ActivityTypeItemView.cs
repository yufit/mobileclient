using System;
using CoreGraphics;
using UIKit;
using YuFit.IOS.Controls;
using YuFit.IOS.Utilities;

namespace YuFit.IOS.Controls.ActivityCollectionView
{
    public class ActivityTypeItemView : DiamondShapeView
    {
        public Action<CGRect, UIColor>      DrawActivityIcon  { get; set; }
        public UIColor                      ActivityFillColor { get; set; }

        public ActivityTypeItemView(CGRect rect) : base(rect) {}
        public ActivityTypeItemView() {}

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            if (DrawActivityIcon != null) {
                using (var ctx = new CGContextHelper(UIGraphics.GetCurrentContext())) {
                    CGRect imageRect = Bounds.Inset(0.32f*Bounds.Width, 0.32f*Bounds.Height);
                    DrawActivityIcon(imageRect, ActivityFillColor);
                }
            }
        }
    }
}

