using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;

namespace YuFit.IOS.Controls.ActivityCollectionView
{
    #if notes

    const int itemCountPerBlock = 5;
    const int firstRowCount = itemCountPerBlock / 2;
    const int secondRowCount = itemCountPerBlock - firstRowCount;
    const int totalItem = 24;

    for (int i=0; i<totalItem; i++)
    {
    int mod = i % itemCountPerBlock;
    int block = (int) Math.Floor ((double)(i / itemCountPerBlock));
    int row = block * 2 + (mod < firstRowCount ? 0 : 1);
    Console.WriteLine ("{2} {3} Item at pos {0} would be on line {1}", i, row, mod, block);
    }

    #endif


    public class ActivityTypeCollectionViewLayout : UICollectionViewLayout
    {
        #region Constants

        const int _itemCountPerBlock = 3;
        const int _lineCountPerBlock = 2;
        const int _firstRowCount = _itemCountPerBlock / _lineCountPerBlock;
        const int _secondRowCount = _itemCountPerBlock - _firstRowCount;

        #endregion

        List<NSIndexPath> deleteIndexPaths;
        List<NSIndexPath> insertIndexPaths;

        nint _cellCount;

        CGSize _itemSize;
        float _topOffset;

        public override CGSize CollectionViewContentSize { get 
            { 
                nint rowCount = (_cellCount / _itemCountPerBlock) * _lineCountPerBlock;
                nfloat height = (rowCount * (_itemSize.Height / 2));

                if ((_cellCount % _itemCountPerBlock) != 0)
                {
                    height += (_itemSize.Height);
                }

                return new CGSize(CollectionView.Frame.Size.Width, (_topOffset * 2) + height); 
            } 
        }

        #region Construction

        public ActivityTypeCollectionViewLayout ()
        {
        }

        #endregion

        public override void PrepareLayout ()
        {
            base.PrepareLayout();
            CGSize size = CollectionView.Frame.Size;
            _cellCount = CollectionView.NumberOfItemsInSection(0);

            nfloat adjustedWidth = size.Width - 40;
            _itemSize = new CGSize(adjustedWidth / _secondRowCount, adjustedWidth / _secondRowCount);
            _topOffset = 20.0f;
        }


        public override UICollectionViewLayoutAttributes LayoutAttributesForItem (NSIndexPath indexPath)
        {
            UICollectionViewLayoutAttributes attributes = UICollectionViewLayoutAttributes.CreateForCell(indexPath);
            attributes.Size = new CGSize(_itemSize);

            int itemIndex = indexPath.Row;
            int mod = itemIndex % _itemCountPerBlock;
            int block = (int) Math.Floor ((double)(itemIndex / _itemCountPerBlock));
            int row = block * 2 + (mod < _firstRowCount ? 0 : 1);

            int rowIndexInBlock = (row % _lineCountPerBlock);
            int itemCountInRow =  rowIndexInBlock == 0 ? _firstRowCount : _secondRowCount;
            int indexInRow = (row % _lineCountPerBlock) == 0 ? mod : mod - _firstRowCount;

            nfloat x = ((CollectionViewContentSize.Width - (itemCountInRow * _itemSize.Width)) / 2.0f) + (indexInRow * _itemSize.Width) + _itemSize.Width/2;
            nfloat y = _topOffset + (row * (_itemSize.Height/2)) + _itemSize.Height/2;

            attributes.Center = new CGPoint(x, y);

            return attributes;
        }

        public override UICollectionViewLayoutAttributes[] LayoutAttributesForElementsInRect (CGRect rect)
        {
            List<UICollectionViewLayoutAttributes> attributes = new List<UICollectionViewLayoutAttributes>();
            for (int i=0 ; i < _cellCount; i++) {
                NSIndexPath indexPath = NSIndexPath.FromItemSection(i, 0);
                attributes.Add(LayoutAttributesForItem(indexPath));
            }
            return attributes.ToArray();
        }

        public override void PrepareForCollectionViewUpdates (UICollectionViewUpdateItem[] updateItems)
        {
            base.PrepareForCollectionViewUpdates(updateItems);

            deleteIndexPaths = new List<NSIndexPath>();
            insertIndexPaths = new List<NSIndexPath>();

            Array.ForEach(updateItems, u => {
                if (u.UpdateAction == UICollectionUpdateAction.Delete) {
                    deleteIndexPaths.Add(u.IndexPathBeforeUpdate);
                } else if (u.UpdateAction == UICollectionUpdateAction.Insert) {
                    insertIndexPaths.Add(u.IndexPathBeforeUpdate);
                }
            });
        }

        public override void FinalizeCollectionViewUpdates ()
        {
            base.FinalizeCollectionViewUpdates();
            deleteIndexPaths = null;
            insertIndexPaths = null;
        }

//        public override UICollectionViewLayoutAttributes InitialLayoutAttributesForAppearingItem (NSIndexPath itemIndexPath)
//        {
//            UICollectionViewLayoutAttributes attributes = base.InitialLayoutAttributesForAppearingItem(itemIndexPath);
//
//            if (insertIndexPaths.Contains(itemIndexPath))
//            {
//                // only change attributes on inserted cells
//                if (attributes == null)
//                {
//                    attributes = LayoutAttributesForItem(itemIndexPath);
//                }
//
//                // Configure attributes ...
//                attributes.Alpha = 0.0f;
//            }
//
//            return attributes;
//        }

//        public override UICollectionViewLayoutAttributes FinalLayoutAttributesForDisappearingItem (NSIndexPath itemIndexPath)
//        {
//            UICollectionViewLayoutAttributes attributes = base.InitialLayoutAttributesForAppearingItem(itemIndexPath);
//            if (deleteIndexPaths.Contains(itemIndexPath))
//            {
//                // only change attributes on inserted cells
//                if (attributes == null)
//                {
//                    attributes = LayoutAttributesForItem(itemIndexPath);
//                }
//
//                // Configure attributes ...
//                attributes.Alpha = 0.0f;
//                attributes.Transform3D = CATransform3D.MakeScale(0.1f, 0.1f, 1.0f);
//            }
//
//            return attributes;
//        }    
    }
}

