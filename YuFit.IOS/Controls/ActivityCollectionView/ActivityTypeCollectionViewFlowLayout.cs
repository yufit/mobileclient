using System;
using UIKit;
using CoreGraphics;
using System.Collections.Generic;
using Foundation;
using CoreAnimation;

namespace YuFit.IOS.Controls.ActivityCollectionView
{
    public class ActivityTypeCollectionViewFlowLayout : UICollectionViewFlowLayout
    {
        CGSize _itemSize;
        nfloat _topOffset;
        nint _cellCount;
        nint _spacer = -50;
        nfloat _leftOffset;

        public override CGSize CollectionViewContentSize {
            get {
                return new CGSize((_cellCount * (_itemSize.Width + _spacer)) + (_leftOffset * 2) - _spacer, CollectionView.Frame.Size.Height);
            }
        }

        public override void PrepareLayout ()
        {
            base.PrepareLayout ();

            CGSize size = CollectionView.Frame.Size;
            _cellCount = CollectionView.NumberOfItemsInSection(0);

            nfloat adjustedHeight = size.Height - 60;
            nfloat adjustedWidth = size.Height - 60;

            _itemSize = new CGSize(adjustedWidth, adjustedHeight);
            _topOffset = (size.Height - adjustedHeight) / 2.0f;
            _leftOffset = (size.Width - adjustedWidth) / 2.0f;
        }

        public override UICollectionViewLayoutAttributes LayoutAttributesForItem (NSIndexPath indexPath)
        {
            UICollectionViewLayoutAttributes attributes = UICollectionViewLayoutAttributes.CreateForCell(indexPath);
            attributes.Size = new CGSize(_itemSize);
            nint itemIndex = indexPath.Row;

            nfloat x = (_itemSize.Width / 2.0f) + (itemIndex * (_itemSize.Width + _spacer)) + _leftOffset;
            nfloat y = (_itemSize.Height / 2.0f) + _topOffset;
            attributes.Center = new CGPoint(x, y);

            CGRect currentBounds = CollectionView.Bounds;
            nfloat viewCenter = currentBounds.Width / 2.0f;
            nfloat halfWidth = currentBounds.Width / 2.0f;
            nfloat adjX = attributes.Center.X - currentBounds.X;
            nfloat distanceToCenter = (nfloat) Math.Abs(viewCenter - adjX); // 0 == center, width/2 == edge
            distanceToCenter = distanceToCenter < 0 ? 0 : distanceToCenter;
            distanceToCenter = distanceToCenter > halfWidth ? halfWidth : distanceToCenter;
            nfloat ratioCenterOffset = (distanceToCenter / halfWidth); // 0 == Dead center, 1 equal, on edge.
            attributes.Alpha = 1.0f - (ratioCenterOffset * 0.50f);
            attributes.ZIndex =  (int)((1.0f - (ratioCenterOffset * 0.25f)) * 100.0f);
            //Console.WriteLine ("adjx {4}, distanceToCenter {2}, half {3}, alpha {0}, ratio-offset {1}, zindex {5}", attributes.Alpha, ratioCenterOffset, distanceToCenter, viewCenter, adjX, attributes.ZIndex);

            CATransform3D theTransform = CATransform3D.Identity;
            theTransform.m34 = 1.0f/-840.0f;
            theTransform.m44 = 0.85f;
            PrintMatrix(theTransform);
            theTransform = theTransform.Scale(1.0f - (ratioCenterOffset * 0.85f), 1.0f - (ratioCenterOffset * 0.85f), 1.0f);
            PrintMatrix(theTransform);
            theTransform = theTransform.Translate(0.5f, 0.5f, 0.0f);
            PrintMatrix(theTransform);
            attributes.Transform3D = theTransform;

            return attributes;
        }

        public override UICollectionViewLayoutAttributes[] LayoutAttributesForElementsInRect (CGRect rect)
        {
            nint startIndex = (nint) (rect.X / (_itemSize.Width + _spacer) - 1);
            nint endIndex = (nint) ((rect.X + rect.Width) / (_itemSize.Width + _spacer)) + 1;
            //int visibleCount = endIndex - startIndex;

            startIndex = startIndex < 0 ? 0 : startIndex;
            endIndex = endIndex > _cellCount ? _cellCount : endIndex;
            //Console.WriteLine ("{0} - {1}", startIndex, endIndex);

            List<UICollectionViewLayoutAttributes> attributes = new List<UICollectionViewLayoutAttributes>();
            for (nint i=startIndex; i < endIndex; i++) {
                NSIndexPath indexPath = NSIndexPath.FromItemSection(i, 0);
                UICollectionViewLayoutAttributes attr = LayoutAttributesForItem (indexPath);
                attributes.Add(attr);
            }
            return attributes.ToArray();
        }

        public override CGPoint TargetContentOffset (CGPoint proposedContentOffset, CGPoint scrollingVelocity)
        {
            nfloat f = proposedContentOffset.X + ((_itemSize.Width + _spacer)/2.0f);
            f = (f<0?0:f);
            int startIndex = (int) ((f / (_itemSize.Width + _spacer)) - 0);
            Console.WriteLine("{0}, {1}, Center item is {2}", f, _itemSize.Width + _spacer, startIndex);

            // Fire item selected
            CollectionView.Source.ItemSelected(CollectionView, NSIndexPath.FromRowSection(startIndex, 0));

            return new CGPoint(startIndex * (_itemSize.Width + _spacer), 0.0f);
        }

        private void PrintMatrix(CATransform3D transform){
//            Console.WriteLine("m1 {0:0.00} {1:0.00} {2:0.00} {3:0.00}", transform.m11, transform.m12, transform.m13, transform.m14);
//            Console.WriteLine("m2 {0:0.00} {1:0.00} {2:0.00} {3:0.00}", transform.m21, transform.m22, transform.m23, transform.m24);
//            Console.WriteLine("m3 {0:0.00} {1:0.00} {2:0.00} {3:0.00}", transform.m31, transform.m32, transform.m33, transform.m34);
//            Console.WriteLine("m4 {0:0.00} {1:0.00} {2:0.00} {3:0.00}", transform.m41, transform.m42, transform.m43, transform.m44);
        }



        public override void PrepareForCollectionViewUpdates (UICollectionViewUpdateItem[] updateItems)
        {
            base.PrepareForCollectionViewUpdates (updateItems);
        }

        public override void FinalizeCollectionViewUpdates ()
        {
            base.FinalizeCollectionViewUpdates ();
        }

        public override UICollectionViewLayoutAttributes InitialLayoutAttributesForAppearingItem (NSIndexPath itemIndexPath)
        {
            return base.InitialLayoutAttributesForAppearingItem (itemIndexPath);
        }

        public override UICollectionViewLayoutAttributes FinalLayoutAttributesForDisappearingItem (NSIndexPath itemIndexPath)
        {
            return base.FinalLayoutAttributesForDisappearingItem (itemIndexPath);
        }

        public override void FinalizeLayoutTransition ()
        {
            base.FinalizeLayoutTransition ();
        }
    }
}

