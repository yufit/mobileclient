﻿using System;
using UIKit;
using MvvmCross.iOS.Views;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Controls.ActivityCollectionView
{
    public class ActivityCollectionViewController<TViewModel, TCell> : MvxCollectionViewController
    {
        protected new TViewModel ViewModel { get { return (TViewModel) base.ViewModel; } }
        public MvxCollectionViewSource Source { get; private set; }
        public ActivityCollectionViewController () : base(new UICollectionViewLayout()) { }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            CollectionView.RegisterClassForCell(typeof(TCell), typeof(TCell).Name);
//            CollectionView.RegisterNibForCell(ActivityTypeCollectionViewCell.Nib, ActivityTypeCollectionViewCell.Key);
            Source = new MvxCollectionViewSource(CollectionView, new Foundation.NSString(typeof(TCell).Name));
            CollectionView.Source = Source;

            CollectionView.SetCollectionViewLayout(new ActivityTypeCollectionViewLayout(), false);

            CollectionView.BackgroundColor = UIColor.Clear;
        }
    }
}

