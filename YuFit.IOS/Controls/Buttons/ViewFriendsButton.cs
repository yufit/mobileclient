﻿using System;
using YuFit.Core.Extensions;
using Cirrious.FluentLayouts.Touch;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Controls
{
    public sealed class ViewFriendsButton : DiamondShapeView
    {
        readonly IconView _iconView = new IconView();

        public ViewFriendsButton ()
        {
            AddSubview(_iconView);
            StrokeColor = YuFitStyleKit.Yellow;
            StrokeWidth = 0.0f;
            DiamondColor = YuFitStyleKit.Yellow;

            _iconView.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawWhomIcon");

            Subviews.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;
                this.AddConstraints(
                    v.WithSameWidth(this).WithMultiplier(0.4f), v.WithSameHeight(this).WithMultiplier(0.4f),
                    v.WithSameCenterX(this), v.WithSameCenterY(this)
                );
            });
        }
    }
}

