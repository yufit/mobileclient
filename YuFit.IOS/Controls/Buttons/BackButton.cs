using UIKit;
using CoreGraphics;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Controls.Buttons
{
    public class BackButton : UIButton
    {
        private UIBezierPath _buttonShapePath;

        public UIColor StrokeColor          { get; set; }
        public UIColor ButtonColor          { get; set; }

        public BackButton(CGRect rect) : base(rect)
        {
            Opaque = false;
            BackgroundColor         = UIColor.Clear;
            StrokeColor             = YuFitStyleKit.White;
            ButtonColor             = YuFitStyleKit.Black;
        }

        private void DrawDiamond()
        {
            var context = UIGraphics.GetCurrentContext();
            context.SaveState();

            ButtonColor.SetFill();
            _buttonShapePath.Fill();

            context.RestoreState();            
        }

        private CGRect BackButtonAdjustedRect {
            get {
                return Bounds.Inset(0.15f * Bounds.Width, 0.1f * Bounds.Height);
            }
        }

        private void ComputeDiamondShapeBezierPath()
        {
            CGRect frame = Bounds;//BackButtonAdjustedRect;

            UIBezierPath bezierPath = new UIBezierPath();
            bezierPath.MoveTo(new CGPoint(frame.GetMinX() + 0.70852f * frame.Width, frame.GetMinY() + 0.07510f * frame.Height));
            bezierPath.AddLineTo(new CGPoint(frame.GetMinX() + 0.26002f * frame.Width, frame.GetMinY() + 0.52510f * frame.Height));
            bezierPath.AddLineTo(new CGPoint(frame.GetMinX() + 0.25954f * frame.Width, frame.GetMinY() + 0.47443f * frame.Height));
            bezierPath.AddLineTo(new CGPoint(frame.GetMinX() + 0.71221f * frame.Width, frame.GetMinY() + 0.91193f * frame.Height));
            bezierPath.AddLineTo(new CGPoint(frame.GetMinX() + 0.66279f * frame.Width, frame.GetMinY() + 0.96307f * frame.Height));
            bezierPath.AddLineTo(new CGPoint(frame.GetMinX() + 0.21013f * frame.Width, frame.GetMinY() + 0.52557f * frame.Height));
            bezierPath.AddLineTo(new CGPoint(frame.GetMinX() + 0.18416f * frame.Width, frame.GetMinY() + 0.50047f * frame.Height));
            bezierPath.AddLineTo(new CGPoint(frame.GetMinX() + 0.20965f * frame.Width, frame.GetMinY() + 0.47490f * frame.Height));
            bezierPath.AddLineTo(new CGPoint(frame.GetMinX() + 0.65815f * frame.Width, frame.GetMinY() + 0.02490f * frame.Height));
            bezierPath.AddLineTo(new CGPoint(frame.GetMinX() + 0.70852f * frame.Width, frame.GetMinY() + 0.07510f * frame.Height));
            bezierPath.ClosePath();


            _buttonShapePath = bezierPath;
        }

        public override void Draw(CGRect rect)
        {
            ComputeDiamondShapeBezierPath();
            DrawDiamond();

        }

//        public override UIView HitTest(PointF point, UIEvent uievent)
//        {
//            UIGraphics.BeginImageContext(Bounds.Size);
//            var context = UIGraphics.GetCurrentContext();
//            bool isHit = false;
//
//            CGPathDrawingMode mode = CGPathDrawingMode.Stroke;
//            if (_buttonShapePath.UsesEvenOddFillRule) {
//                mode = CGPathDrawingMode.EOFill;
//            } else {
//                mode = CGPathDrawingMode.Fill;
//            }
//
//            context.SaveState();
//            context.AddPath(_buttonShapePath.CGPath);
//
//            isHit = context.PathContainsPoint(point, mode);
//
//            context.RestoreState();      
//            UIGraphics.EndImageContext();
//            return isHit ? this : null;
//        }
    }

}

