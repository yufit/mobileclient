﻿using System;
using YuFit.Core.Extensions;
using Cirrious.FluentLayouts.Touch;
using YuFit.IOS.Themes;
using CoreGraphics;
using UIKit;

namespace YuFit.IOS.Controls.Buttons
{
    public sealed class DiamondButton : DiamondShapeView
    {
        readonly IconView _iconView = new IconView();

        public Action<CGRect, UIColor> DrawMethod { 
            get { return _iconView.DrawIcon; }
            set { _iconView.DrawIcon =value; SetNeedsDisplay(); }
        }

        public DiamondButton (Action<CGRect, UIColor> drawMethod = null)
        {
            AddSubview(_iconView);

            StrokeWidth = 0.0f;
            StrokeColor = YuFitStyleKit.Yellow;
            DiamondColor = YuFitStyleKit.Yellow;
            DrawMethod = drawMethod;

            Subviews.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;
                this.AddConstraints(
                    v.WithSameWidth(this).WithMultiplier(0.5f), v.WithSameHeight(this).WithMultiplier(0.5f),
                    v.WithSameCenterX(this), v.WithSameCenterY(this)
                );
            });
        }
    }
}

