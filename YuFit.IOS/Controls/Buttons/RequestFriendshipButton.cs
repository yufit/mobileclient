﻿using Cirrious.FluentLayouts.Touch;
using YuFit.Core.Extensions;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Controls.Buttons
{
    public sealed class RequestFriendshipButton : DiamondShapeView
    {
        readonly IconView _iconView = new IconView();

        public RequestFriendshipButton ()
        {
            AddSubview(_iconView);
            StrokeColor = YuFitStyleKit.Yellow;
            StrokeWidth = 0.0f;
            DiamondColor = YuFitStyleKit.Yellow;

            _iconView.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawAddPerson");

            Subviews.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;
                this.AddConstraints(
                    v.WithSameWidth(this).WithMultiplier(0.65f), v.WithSameHeight(this).WithMultiplier(0.65f),
                    v.WithSameCenterX(this), v.WithSameCenterY(this)
                );
            });
        }
    }
}

