﻿using Cirrious.FluentLayouts.Touch;

using YuFit.Core.Extensions;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Controls.Buttons
{
    public sealed class SettingsButton : DiamondShapeView
    {
        readonly IconView _iconView = new IconView();

        public SettingsButton ()
        {
            AddSubview(_iconView);
            StrokeColor = YuFitStyleKit.Olive;
            StrokeWidth = 0.0f;
            DiamondColor = YuFitStyleKit.Olive;

            _iconView.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawSettings2");

            Subviews.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;
                this.AddConstraints(
                    v.WithSameWidth(this).WithMultiplier(0.40f), v.WithSameHeight(this).WithMultiplier(0.40f),
                    v.WithSameCenterX(this), v.WithSameCenterY(this)
                );
            });
        }
    }

}

