﻿using System;
using YuFit.Core.Extensions;
using Cirrious.FluentLayouts.Touch;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Controls.Buttons
{
    public sealed class LeaveGroupButton : DiamondShapeView
    {
        readonly IconView _iconView = new IconView();

        public LeaveGroupButton ()
        {
            AddSubview(_iconView);
            StrokeColor = YuFitStyleKit.Yellow;
            StrokeWidth = 0.0f;
            DiamondColor = YuFitStyleKit.Yellow;

            _iconView.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawLeaveGroupIcon");

            Subviews.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;
                this.AddConstraints(
                    v.WithSameWidth(this).WithMultiplier(0.4f), v.WithSameHeight(this).WithMultiplier(0.4f),
                    v.WithSameCenterX(this), v.WithSameCenterY(this)
                );
            });
        }
    }
}

