using Cirrious.FluentLayouts.Touch;

using UIKit;

using YuFit.IOS.Controls;

namespace YuFit.IOS.Controls.Buttons
{
    public class ApplyView : UIView
    {
        #region Properties

        public readonly UILabel ApplyLabel = new UILabel();
        public readonly ApplyTriangle ApplyTriangle = new ApplyTriangle();

        #endregion

        #region Initialization

        public ApplyView ()
        {
            BackgroundColor = UIColor.Clear;
        }

        public void Setup (bool showLabel = false, bool addBlur = false)
        {
//            if (addBlur) {
////                var blur = UIBlurEffect.FromStyle (UIBlurEffectStyle.Light);
////                var blurView = new UIVisualEffectView (blur);
////                Add (blurView);
////                blurView.TranslatesAutoresizingMaskIntoConstraints = false;
////                this.AddConstraints(
////                    blurView.WithSameHeight(this),
////                    blurView.WithSameWidth(this),
////                    blurView.WithSameCenterX(this),
////                    blurView.WithSameCenterY(this)
////                );
//
//                BackgroundColor = YuFitStyleKit.Black.ColorWithAlpha(0.85f);
//            }

            if (showLabel) {
                ApplyLabel.TextColor = Themes.CrudFitEvent.ItsOnView.TextColor;
                ApplyLabel.Font = Themes.CrudFitEvent.ItsOnView.TextFont;
                Add(ApplyLabel);

                ApplyLabel.TranslatesAutoresizingMaskIntoConstraints = false;
                AddConstraint(NSLayoutConstraint.Create(ApplyLabel, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterX, 1.0f, 0));
            }

            Add(ApplyTriangle);
            ApplyTriangle.TranslatesAutoresizingMaskIntoConstraints = false;

            ApplyTriangle.AddConstraints(
                ApplyTriangle.Width().EqualTo(80),
                ApplyTriangle.Height().EqualTo(40)
            );

            this.AddConstraints(
                ApplyTriangle.CenterX().EqualTo().CenterXOf(this),
                ApplyTriangle.Bottom().EqualTo(-20).BottomOf(this)
            );

            if (showLabel) {
                this.AddConstraints(
                    ApplyLabel.CenterX().EqualTo().CenterXOf(this),
                    ApplyLabel.Bottom().EqualTo().TopOf(ApplyTriangle)
                );
            }
        }

        #endregion

        #region Methods

        #endregion
    }
}

