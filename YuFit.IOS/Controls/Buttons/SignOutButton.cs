﻿using Cirrious.FluentLayouts.Touch;

using YuFit.Core.Extensions;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Controls.Buttons
{
    public sealed class SignOutButton : DiamondShapeView
    {
        readonly IconView _iconView = new IconView();

        public SignOutButton ()
        {
            AddSubview(_iconView);
            StrokeColor = YuFitStyleKit.Red;
            StrokeWidth = 0.0f;
            DiamondColor = YuFitStyleKit.Red;

            _iconView.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawSignOut");

            Subviews.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;
                this.AddConstraints(
                    v.WithSameWidth(this).WithMultiplier(0.40f), v.WithSameHeight(this).WithMultiplier(0.40f),
                    v.WithSameCenterX(this), v.WithSameCenterY(this)
                );
            });
        }
    }

}

