﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;
using System.ComponentModel;
using YuFit.IOS.Themes;
using System.Linq;
using YuFit.Core.Extensions;
using Cirrious.FluentLayouts.Touch;

namespace YuFit.IOS.Controls
{
    [Register("WhenView"), DesignTimeVisible(true)]
    public class WhenView : UIView, IComponent
    {
        #region Required for the xamarin storyboard editor

        #pragma warning disable 0067
        public event EventHandler Disposed;

        public ISite Site { get; set; }
        #pragma warning restore 0067
        #endregion

        DateTime _date = DateTime.Now;

        readonly UILabel _dayLabel           = new UILabel();
        readonly UILabel _monthAndDateLabel  = new UILabel();
        readonly UILabel _timeLabel          = new UILabel();

        public DateTime Date {
            get { return _date; }
            set { if (_date != value) { _date = value; UpdateDate(); } }
        }

        public UIFont DayLabelFont {
            get { return _dayLabel.Font; }
            set { if (_dayLabel.Font != value) { 
                    _dayLabel.Font = value; 
                    SetNeedsUpdateConstraints();
                    LayoutIfNeeded();
                } 
            }
        }

        public UIFont MonthAndDateLabelFont {
            get { return _monthAndDateLabel.Font; }
            set { if (_monthAndDateLabel.Font != value) {
                    _monthAndDateLabel.Font = value;
                    SetNeedsUpdateConstraints();
                    LayoutIfNeeded();
                }
            }
        }

        public UIFont TimeLabelFont {
            get { return _timeLabel.Font; }
            set {
                if (_timeLabel.Font != value) {
                    _timeLabel.Font = value;
                    SetNeedsUpdateConstraints();
                    LayoutIfNeeded();
                }
            }
        }


        public WhenView () { Initialize(); }
        public WhenView (IntPtr handle) : base(handle) { }
        public WhenView (CGRect rect) : base(rect) { Initialize(); }

        public override void AwakeFromNib ()
        {
            base.AwakeFromNib ();
            Initialize ();
        }      

        private void Initialize()
        {
            AddSubviews(new UIView[] {_dayLabel, _monthAndDateLabel, _timeLabel});
            Subviews.Where(v => v is UILabel).ForEach(v => {
                var label = v as UILabel;
                label.TextColor = YuFitStyleKit.White;
                label.TextAlignment = UITextAlignment.Center;
                label.AdjustsFontSizeToFitWidth = true;
                label.TranslatesAutoresizingMaskIntoConstraints = false;
            });

            this.AddConstraints(
                _monthAndDateLabel.Width().EqualTo().WidthOf(this).WithMultiplier(0.8f),
                _monthAndDateLabel.WithSameCenterX(this),
                _monthAndDateLabel.CenterY().EqualTo(2.0f).CenterYOf(this),

                _dayLabel.Width().EqualTo().WidthOf(this).WithMultiplier(0.6f),
                _dayLabel.WithSameCenterX(this),
                _dayLabel.Bottom().EqualTo(2.0f).TopOf(_monthAndDateLabel),

                _timeLabel.Width().EqualTo().WidthOf(this).WithMultiplier(0.6f),
                _timeLabel.WithSameCenterX(this),
                _timeLabel.Top().EqualTo(-2.0f).BottomOf(_monthAndDateLabel)

            );
        }

        private void UpdateDate()
        {
            NSDateFormatter formatter = new NSDateFormatter();
            formatter.DateFormat = "EEEE";

            string dayString = formatter.ShortStandaloneWeekdaySymbols[(int)_date.DayOfWeek];
            _dayLabel.Text = dayString.ToUpper();

            formatter.DateFormat = "MMMM";
            string monthString = formatter.MonthSymbols[(int) _date.Month - 1];
            _monthAndDateLabel.Text = monthString.ToUpper() + ' ' + _date.Day;

            formatter.SetLocalizedDateFormatFromTemplate("jj:mm");

            NSDate tmp = (NSDate) DateTime.SpecifyKind(_date, DateTimeKind.Local);

            string timeString = formatter.StringFor(tmp);
            _timeLabel.Text = timeString;
            SetNeedsDisplay();
        }

//        public override void LayoutSubviews ()
//        {
//            base.LayoutSubviews();
//
//            nfloat desiredHeight = Bounds.Height/3.0f;
//            nfloat startY = 0.0f;
//
//            CGRect helperRect = Bounds;
//            helperRect.Height = desiredHeight + 3.0f;
//            helperRect.Y = startY + 6.0f + 0.0f;
//            helperRect.Width = Bounds.Width * 0.7f;
//            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
//            _dayLabel.Frame = helperRect;
//
//            helperRect.Y = startY + desiredHeight + 2.0f;
//            _monthAndDateLabel.Frame = helperRect;
//
//            helperRect.Y = startY + (2 * desiredHeight) - 4.0f;
//            helperRect.Width = Bounds.Width * 0.7f;
//            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
//            _timeLabel.Frame = helperRect;
//        }

    }
}

