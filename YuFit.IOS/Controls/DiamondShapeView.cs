using System;

using CoreAnimation;
using CoreGraphics;

using UIKit;

using YuFit.IOS.Themes;
using YuFit.IOS.Utilities;

namespace YuFit.IOS.Controls
{
    public enum PlusLocation { Left, Top, Right, Bottom, None }

    // TODO THIS CLASS NEEDS TO NOT INCLUDE THE PLUS STUFF LOGIC.  THAT IS SPECIFIC TO THE 
    // CREATION OF EVENT AND SHOULD BE MOVE IN A CLASS THAT WOULD DERIVE FROM THIS.
    public class DiamondShapeView : UIView
    {
        #region Data members

        private UIBezierPath _diamondShapePath;
        private CAShapeLayer _diamondShapeLayer;

        private UIBezierPath _plusShapePath;
        private UIBezierPath _plusPath;

        #endregion

        #region Properties

        // TODO HACK THIS IS A HACK BECAUSE I NEEDED A WAY TO TELL THE BEZIER PATH TO NOT FILL THE BACKGROUND.
        // MAYBE INSTEAD OF DIAMOND COLOR, WE COULD USE THE BACKGROUND COLOR INSTEAD.
        public bool TransparentBackground { get; set; }

        public UIColor      StrokeColor         { get; set; }
        public UIColor      DiamondColor        { get; set; }
        public float        StrokeWidth         { get; set; }
        public UIColor      PlusBackgroundColor { get; set; }
        public UIColor      PlusForegroundColor { get; set; }
        public PlusLocation PlusLocation        { get; set; }
        public float        PlusSizeFactor      { get; set; }
        public bool         ShouldDrawDownArrow { get; set; }
        public float        Inset               { get; set; }

        protected CGRect DiamondRect {
            get {
                var bounds = Bounds;
                if (bounds.Width > bounds.Height) { 
                    bounds.Width = bounds.Height; 
                    bounds.X = (Bounds.Width-bounds.Width)/2.0f;
                } else if (bounds.Height > bounds.Width) { 
                    bounds.Height = bounds.Width; 
                    bounds.Y = (Bounds.Height-bounds.Height)/2.0f;
                }

                if (Math.Abs(bounds.Width) < 0.1f || Math.Abs(bounds.Height) < 0.1f) {
                    return bounds;
                }
                return bounds.Inset(Inset + StrokeWidth/2.0f, Inset + StrokeWidth/2.0f);
            }
        }

        protected CGSize PlusSize {
            get {
                CGRect diamondRect = DiamondRect;
                return new CGSize(diamondRect.Width * 0.2f, diamondRect.Width * 0.2f);
            }
        }

        protected CGRect PlusOnTopRect {
            get {
                CGRect diamondRect = DiamondRect;
                CGRect plusRect = new CGRect(new CGPoint {
                    X = (diamondRect.Width / 2.0f - PlusSize.Width / 2) + diamondRect.X,
                    Y = diamondRect.Top + (StrokeWidth / 2.0f),
                }, PlusSize);
                return plusRect;
            }
        }

        protected CGRect PlusOnBottomRect {
            get {
                CGRect diamondRect = DiamondRect;
                CGRect plusRect = new CGRect(new CGPoint {
                    X = (diamondRect.Width / 2.0f - PlusSize.Width / 2) + diamondRect.X,
                    Y = diamondRect.Bottom - PlusSize.Width - (StrokeWidth / 2.0f)
                }, PlusSize);
                return plusRect;
            }
        }

        protected CGRect PlusOnLeftRect {
            get {
                CGRect diamondRect = DiamondRect;
                CGRect plusRect = new CGRect(new CGPoint {
                    X = diamondRect.Left + (StrokeWidth / 2.0f),
                    Y = (diamondRect.Width / 2.0f - PlusSize.Width / 2) + diamondRect.Y
                }, PlusSize);
                return plusRect;
            }
        }

        protected CGRect PlusOnRightRect {
            get {
                CGRect diamondRect = DiamondRect;
                CGRect plusRect = new CGRect(new CGPoint {
                    X = diamondRect.Right - PlusSize.Width - (StrokeWidth / 2.0f),
                    Y = (diamondRect.Width / 2.0f - PlusSize.Width / 2) + diamondRect.Y
                }, PlusSize);
                return plusRect;
            }
        }

        protected CGRect PlusRect {
            get {
                switch (PlusLocation) {
                    case PlusLocation.Top: return PlusOnTopRect;
                    case PlusLocation.Bottom: return PlusOnBottomRect;
                    case PlusLocation.Left: return PlusOnLeftRect;
                    case PlusLocation.Right: return PlusOnRightRect;
                }
                return CGRect.Empty;
            }
        }

        #endregion

        #region Construction/Initialization

        public DiamondShapeView(CGRect rect) : base(rect)
        {
            Opaque              = false;
            BackgroundColor     = UIColor.Clear;
            StrokeColor         = YuFitStyleKit.White;
            DiamondColor        = YuFitStyleKit.Black;
            StrokeWidth         = 0.0f;

            PlusSizeFactor      = 0.2f;
            PlusLocation        = PlusLocation.None;

            Inset               = 4.0f;

            UpdateDiamondShapeLayerMask();
        }

        public DiamondShapeView()
        {
            Opaque              = false;
            BackgroundColor     = UIColor.Clear;
            StrokeColor         = YuFitStyleKit.White;
            DiamondColor        = YuFitStyleKit.Black;
            StrokeWidth         = 0.0f;

            PlusSizeFactor      = 0.2f;
            PlusLocation        = PlusLocation.None;

            Inset               = 4.0f;
        }

        #endregion

        #region UIView overrides

        public override void UpdateConstraints ()
        {
            base.UpdateConstraints();
            UpdateDiamondShapeLayerMask();
        }

        public override void Draw(CGRect rect)
        {
            using (var ctx = new CGContextHelper(UIGraphics.GetCurrentContext())) {
                DrawDiamond();
                DrawPlus();

                if (ShouldDrawDownArrow) DrawDownArrow();
            }
        }

        public override UIView HitTest (CGPoint point, UIEvent uievent)
        {
            CGPoint layerPoint = Layer.ConvertPointToLayer(point, _diamondShapeLayer);
            return _diamondShapeLayer.Path.ContainsPoint(layerPoint, true) ? this : null;
        }

        #endregion

        #region API

        public void UpdateDiamondShapeLayerMask()
        {
            ComputeDiamondShapeBezierPath();

            _diamondShapeLayer = new CAShapeLayer();
            _diamondShapeLayer.Path = _diamondShapePath.CGPath;
            _diamondShapeLayer.FillColor = DiamondColor.CGColor;
            _diamondShapeLayer.StrokeColor = StrokeColor.CGColor;
            _diamondShapeLayer.LineWidth = StrokeWidth;
            Layer.Mask = _diamondShapeLayer;
        }

        #endregion

        #region Private helpers

        private void ComputeDiamondShapeBezierPath()
        {
            CGRect frame = DiamondRect;

            _diamondShapePath = new UIBezierPath();
            _diamondShapePath.MoveTo    (new CGPoint(frame.GetMinX() + 0.49942f * frame.Width, frame.GetMinY() + 1.00000f * frame.Height));
            _diamondShapePath.AddLineTo (new CGPoint(frame.GetMinX() + 1.00000f * frame.Width, frame.GetMinY() + 0.49942f * frame.Height));
            _diamondShapePath.AddLineTo (new CGPoint(frame.GetMinX() + 0.50058f * frame.Width, frame.GetMinY() + 0.00000f * frame.Height));
            _diamondShapePath.AddLineTo (new CGPoint(frame.GetMinX() + 0.00000f * frame.Width, frame.GetMinY() + 0.50058f * frame.Height));
            _diamondShapePath.AddLineTo (new CGPoint(frame.GetMinX() + 0.49942f * frame.Width, frame.GetMinY() + 1.00000f * frame.Height));
            _diamondShapePath.ClosePath();
        }

        private void ComputePlusShape()
        {
            CGRect plusFrame = PlusRect;

            _plusShapePath = new UIBezierPath();
            _plusShapePath.MoveTo       (new CGPoint(plusFrame.GetMinX() + 0.49942f * plusFrame.Width, plusFrame.GetMinY() + 1.00000f * plusFrame.Height));
            _plusShapePath.AddLineTo    (new CGPoint(plusFrame.GetMinX() + 1.00000f * plusFrame.Width, plusFrame.GetMinY() + 0.49942f * plusFrame.Height));
            _plusShapePath.AddLineTo    (new CGPoint(plusFrame.GetMinX() + 0.50058f * plusFrame.Width, plusFrame.GetMinY() + 0.00000f * plusFrame.Height));
            _plusShapePath.AddLineTo    (new CGPoint(plusFrame.GetMinX() + 0.00000f * plusFrame.Width, plusFrame.GetMinY() + 0.50058f * plusFrame.Height));
            _plusShapePath.AddLineTo    (new CGPoint(plusFrame.GetMinX() + 0.49942f * plusFrame.Width, plusFrame.GetMinY() + 1.00000f * plusFrame.Height));
            _plusShapePath.ClosePath();

            CGRect plusSignFrame = plusFrame.Inset(0.3f*PlusSize.Width, 0.3f*PlusSize.Width);

            _plusPath = new UIBezierPath();
            _plusPath.MoveTo(new CGPoint(plusSignFrame.GetMinX() + 0.55996f * plusSignFrame.Width, plusSignFrame.GetMinY() + 0.54690f * plusSignFrame.Height));
            _plusPath.AddLineTo(new CGPoint(plusSignFrame.GetMinX() + 0.55996f * plusSignFrame.Width, plusSignFrame.GetMinY() + 0.93967f * plusSignFrame.Height));
            _plusPath.AddLineTo(new CGPoint(plusSignFrame.GetMinX() + 0.45889f * plusSignFrame.Width, plusSignFrame.GetMinY() + 0.93967f * plusSignFrame.Height));
            _plusPath.AddLineTo(new CGPoint(plusSignFrame.GetMinX() + 0.45889f * plusSignFrame.Width, plusSignFrame.GetMinY() + 0.54690f * plusSignFrame.Height));
            _plusPath.AddLineTo(new CGPoint(plusSignFrame.GetMinX() + 0.07018f * plusSignFrame.Width, plusSignFrame.GetMinY() + 0.54690f * plusSignFrame.Height));
            _plusPath.AddLineTo(new CGPoint(plusSignFrame.GetMinX() + 0.07018f * plusSignFrame.Width, plusSignFrame.GetMinY() + 0.45942f * plusSignFrame.Height));
            _plusPath.AddLineTo(new CGPoint(plusSignFrame.GetMinX() + 0.45891f * plusSignFrame.Width, plusSignFrame.GetMinY() + 0.45942f * plusSignFrame.Height));
            _plusPath.AddLineTo(new CGPoint(plusSignFrame.GetMinX() + 0.45891f * plusSignFrame.Width, plusSignFrame.GetMinY() + 0.06667f * plusSignFrame.Height));
            _plusPath.AddLineTo(new CGPoint(plusSignFrame.GetMinX() + 0.55998f * plusSignFrame.Width, plusSignFrame.GetMinY() + 0.06667f * plusSignFrame.Height));
            _plusPath.AddLineTo(new CGPoint(plusSignFrame.GetMinX() + 0.55998f * plusSignFrame.Width, plusSignFrame.GetMinY() + 0.45942f * plusSignFrame.Height));
            _plusPath.AddLineTo(new CGPoint(plusSignFrame.GetMinX() + 0.94647f * plusSignFrame.Width, plusSignFrame.GetMinY() + 0.45942f * plusSignFrame.Height));
            _plusPath.AddLineTo(new CGPoint(plusSignFrame.GetMinX() + 0.94647f * plusSignFrame.Width, plusSignFrame.GetMinY() + 0.54690f * plusSignFrame.Height));
            _plusPath.AddLineTo(new CGPoint(plusSignFrame.GetMinX() + 0.55996f * plusSignFrame.Width, plusSignFrame.GetMinY() + 0.54690f * plusSignFrame.Height));
            _plusPath.ClosePath();
            _plusPath.MiterLimit = 4.0f;
        }

        private void DrawDiamond()
        {
            UpdateDiamondShapeLayerMask();
            if (!TransparentBackground) {
                DiamondColor.SetFill();
                _diamondShapePath.Fill();
            }

            StrokeColor.SetStroke();
            _diamondShapePath.LineWidth = StrokeWidth;
            _diamondShapePath.Stroke();
        }

        private void DrawPlus()
        {
            if (PlusLocation != PlusLocation.None) {
                ComputePlusShape();

                PlusBackgroundColor.SetFill();
                _plusShapePath.Fill();

                PlusForegroundColor.SetFill();
                _plusPath.Fill();
            }
        }

        private void DrawDownArrow()
        {
            CGRect framee = Bounds;
            framee.Y = framee.Height / 2.0f;
            framee.Height = framee.Height / 2.0f;
            UIBezierPath downArrowPath = new UIBezierPath();
            downArrowPath.MoveTo(new CGPoint(framee.GetMinX() + 0.35000f * framee.Width, framee.GetMinY() + 0.27000f * framee.Height));
            downArrowPath.AddCurveToPoint(new CGPoint(framee.GetMinX() + 0.50000f * framee.Width, framee.GetMinY() + 0.57000f * framee.Height), new CGPoint(framee.GetMinX() + 0.50052f * framee.Width, framee.GetMinY() + 0.57104f * framee.Height), new CGPoint(framee.GetMinX() + 0.50000f * framee.Width, framee.GetMinY() + 0.57000f * framee.Height));
            downArrowPath.AddLineTo(new CGPoint(framee.GetMinX() + 0.65000f * framee.Width, framee.GetMinY() + 0.27000f * framee.Height));
            downArrowPath.AddLineTo(new CGPoint(framee.GetMinX() + 0.61250f * framee.Width, framee.GetMinY() + 0.27000f * framee.Height));
            downArrowPath.AddLineTo(new CGPoint(framee.GetMinX() + 0.50000f * framee.Width, framee.GetMinY() + 0.49500f * framee.Height));
            downArrowPath.AddLineTo(new CGPoint(framee.GetMinX() + 0.38750f * framee.Width, framee.GetMinY() + 0.27000f * framee.Height));
            downArrowPath.AddLineTo(new CGPoint(framee.GetMinX() + 0.35000f * framee.Width, framee.GetMinY() + 0.27000f * framee.Height));            
            YuFitStyleKit.White.SetFill();
            downArrowPath.Fill();
        }

        #endregion

    }
}

