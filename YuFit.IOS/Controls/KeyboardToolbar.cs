using UIKit;


namespace YuFit.IOS.Controls
{

    /// <summary>
    /// Toolbar that plays input clicks
    /// </summary>
    public class KeyboardToolbar : UIToolbar
    {
        public KeyboardToolbar (CoreGraphics.CGRect rect) : base(rect)
        {

        }

//        public override bool EnableInputClicksWhenVisible {
//            get {
//                return true;
//            }
//        }
    }
}
