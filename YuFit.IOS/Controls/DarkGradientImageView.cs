using System;

using CoreAnimation;
using CoreGraphics;
using UIKit;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Controls
{
    public class DarkGradientImageView : UIView
    {
        public UIImageView ImageView = new UIImageView();
        public DarkGradientImageView (UIImage image, CGRect rect) : base(rect)
        {
            Add(ImageView);
            ImageView.Image = image;
            ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;

            ImageView.Frame = new CGRect {
                Width = rect.Width, Height = rect.Width * 0.5531401f,
                X = 0, Y = 0
            };

            CAGradientLayer gradient = new CAGradientLayer();
            gradient.Frame = ImageView.Frame;
            gradient.Colors = new [] { UIColor.Clear.CGColor, UIColor.Clear.CGColor, YuFitStyleKit.Black.CGColor };
            ImageView.Layer.AddSublayer(gradient);
        }

        public void ReplaceImage(UIImage image) 
        {
            ImageView.RemoveFromSuperview();

            ImageView = new UIImageView();
            Add(ImageView);
            ImageView.Image = image;
            ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;

            ImageView.Frame = new CGRect {
                Width = Bounds.Width, Height = Bounds.Width * 0.5531401f,
                X = 0, Y = 0
            };

            CAGradientLayer gradient = new CAGradientLayer();
            gradient.Frame = ImageView.Frame;
            gradient.Colors = new [] { UIColor.Clear.CGColor, UIColor.Clear.CGColor, YuFitStyleKit.Black.CGColor };
            ImageView.Layer.AddSublayer(gradient);
        }
    }
    
}
