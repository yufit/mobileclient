﻿using System;
using Foundation;
using UIKit;
using SWTableViewCell;
using YuFit.Core.ViewModels;
using YuFit.IOS.Views;

namespace YuFit.IOS.Controls
{
    public abstract class MvxSwipeTableViewSource<TCell> : TableViewSource<TCell> where TCell : UITableViewCell, new()
    {
        protected readonly BaseViewModel _viewModel;
        protected readonly UITableView _tableView;

        public MvxSwipeTableViewSource(BaseViewModel viewModel, UITableView tableView, string nibName)
            : base(tableView, nibName)
        {
            _viewModel = viewModel;
            _tableView = tableView;
        } 

        protected override UITableViewCell GetOrCreateCellFor (UITableView tableView, NSIndexPath indexPath, object item)
        {
            MvxSwipeTableViewCell cell = (MvxSwipeTableViewCell) base.GetOrCreateCellFor(tableView, indexPath, item);

            cell.Delegate = GetDelegate();

            UIButton[] rightButtons  = GetRightButtons();
            if (rightButtons != null)
            {
                cell.RightUtilityButtons = rightButtons;
            }

            UIButton[] leftButtons  = GetLeftButtons();
            if (leftButtons != null)
            {
                cell.LeftUtilityButtons = leftButtons;
            }

            return cell;
        }

        protected virtual UIButton[] GetLeftButtons () 
        {
            return null;
        }

        protected virtual UIButton[] GetRightButtons ()
        {
            return null;
        }

        protected abstract MvxSwipeTableViewCellDelegate GetDelegate();


    }
}

