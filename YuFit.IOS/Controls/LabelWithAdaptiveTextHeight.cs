using System;

using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Associations.Groups;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using Cirrious.FluentLayouts.Touch;
using StoreKit;
using System.Collections.Generic;
using Foundation;

namespace YuFit.IOS.Views.Associations.Groups
{
    public class LabelWithAdaptiveTextHeight : UILabel
    {
        public override void LayoutSubviews ()
        {
            base.LayoutSubviews();
            Font = FontToFitHeight();
        }

        UIFont FontToFitHeight ()
        {
            var minFontSize = 18;
            var maxFontSize = 67;
            var fontSizeAverage = 0;
            var textAndLabelHeightDiff = 0;

            while (minFontSize <= maxFontSize) {
                fontSizeAverage = minFontSize + (maxFontSize - minFontSize) / 2;
                var labelText = Text;
                var labelHeight = Frame.Height;

                var testStringHeight = labelText.StringSize(Font.WithSize(fontSizeAverage)).Height;
                textAndLabelHeightDiff = (int) (labelHeight - testStringHeight);

                if (fontSizeAverage == minFontSize || fontSizeAverage == maxFontSize) {
                    if (textAndLabelHeightDiff < 0) {
                        return Font.WithSize(fontSizeAverage - 1);
                    }
                    return Font.WithSize(fontSizeAverage);
                }

                if (textAndLabelHeightDiff < 0) {
                    maxFontSize = fontSizeAverage - 1;
                } else if (textAndLabelHeightDiff > 0) {
                    minFontSize = fontSizeAverage + 1;
                } else {
                    return Font.WithSize(fontSizeAverage);
                }
            }

            return Font.WithSize(fontSizeAverage);
        }
    }
    
}

//- (void)validateProductIdentifiers:(NSArray *)productIdentifiers
//{
//    SKProductsRequest *productsRequest = [[SKProductsRequest alloc]
//        initWithProductIdentifiers:[NSSet setWithArray:productIdentifiers]];
//
//    // Keep a strong reference to the request.
//    self.request = productsRequest;
//    productsRequest.delegate = self;
//    [productsRequest start];
//}
//
//// SKProductsRequestDelegate protocol method
//- (void)productsRequest:(SKProductsRequest *)request
//didReceiveResponse:(SKProductsResponse *)response
//{
//    self.products = response.products;
//
//    for (NSString *invalidIdentifier in response.invalidProductIdentifiers) {
//        // Handle any invalid product identifiers.
//    }
//
//    [self displayStoreUI]; // Custom method
//}