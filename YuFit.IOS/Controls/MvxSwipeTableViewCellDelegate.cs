﻿using System;
using SWTableViewCell;
using UIKit;

namespace YuFit.IOS.Controls
{
    public class MvxSwipeTableViewCellDelegate : SWTableViewCellDelegate
    {
        //private readonly UITableView _tableView;

        public event EventHandler UtilityButtonsClosed;
        public event EventHandler LeftUtilityButtonsOpen;
        public event EventHandler RightUtilityButtonsOpen;

        public override void ScrollingToState(SWTableViewCell.SWTableViewCell cell, SWCellState state)
        {
            switch (state)
            {
                case SWCellState.Center:
                    OnUtilityButtonsClosed();
                    break;

                case SWCellState.Left:
                    OnLeftUtilityButtonsOpen();
                    break;

                case SWCellState.Right:
                    OnRightUtilityButtonsOpen();
                    break;
            }
        }


        public MvxSwipeTableViewCellDelegate(UITableView tableView)
        {
            //_tableView = tableView;
        }

        protected virtual void OnUtilityButtonsClosed()
        {
            EventHandler handler = UtilityButtonsClosed;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        protected virtual void OnLeftUtilityButtonsOpen()
        {
            EventHandler handler = LeftUtilityButtonsOpen;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        protected virtual void OnRightUtilityButtonsOpen()
        {
            EventHandler handler = RightUtilityButtonsOpen;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }



        public override void DidTriggerLeftUtilityButton(SWTableViewCell.SWTableViewCell cell, nint index)
        {
            Console.WriteLine("Left button {0} was pressed.", index);
        }

        public override void DidTriggerRightUtilityButton(SWTableViewCell.SWTableViewCell cell, nint index)
        {
            Console.WriteLine("Right button {0} was pressed.", index);

        }

        public override bool ShouldHideUtilityButtonsOnSwipe(SWTableViewCell.SWTableViewCell cell)
        {
            // allow just one cell's utility button to be open at once
            return true;
        }

        public override bool CanSwipeToState(SWTableViewCell.SWTableViewCell cell, SWCellState state)
        {
            switch (state)
            {
                case SWCellState.Left:
                    // set to false to disable all left utility buttons appearing
                    return true;

                case SWCellState.Right:
                    // set to false to disable all right utility buttons appearing
                    return true;
            }
            return true;
        }
    }
}

