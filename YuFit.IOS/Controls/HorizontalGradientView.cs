﻿using System;
using UIKit;
using CoreGraphics;
using YuFit.IOS.Utilities;

namespace YuFit.IOS.Controls
{
    public class HorizontalGradientView : UIView
    {
        public UIColor StartColor { get; set; }
        public UIColor EndColor { get; set; }

        public HorizontalGradientView ()
        {
            UserInteractionEnabled = true;
        }

        public override void Draw (CGRect rect)
        {
            nfloat[] gradLocations = {1.0f, 0.0f};
            nfloat[] gradColors = new nfloat[8];
            StartColor.GetRGBA(out gradColors[0], out gradColors[1], out gradColors[2], out gradColors[3]);
            EndColor.GetRGBA(out gradColors[4], out gradColors[5], out gradColors[6], out gradColors[7]);

            CGColorSpace colorSpace = CGColorSpace.CreateDeviceRGB();
            CGGradient gradient = new CGGradient(colorSpace, gradColors, gradLocations);

            using (var ctx = new CGContextHelper(UIGraphics.GetCurrentContext())) {
                UIGraphics.GetCurrentContext().DrawLinearGradient(gradient, new CGPoint(0.0f, 0.0f), new CGPoint(0.0f, Frame.Height), CGGradientDrawingOptions.DrawsAfterEndLocation);
            }
            base.Draw(rect);
        }
    }
}

