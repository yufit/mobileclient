﻿using System;
using UIKit;
using Cirrious.FluentLayouts.Touch;

namespace YuFit.IOS.Controls
{
    public class PushNotificationControl : UIView
    {
        public IconView Icon { get; private set; }
        public UILabel MessageLabel { get; private set; }
        public UILabel DateTimeLabel { get; private set; }

        readonly UIView _labelContainer = new UIView();

        public PushNotificationControl ()
        {
            SetupSubviews();
            SetupConstraints();
        }

        void SetupSubviews()
        {
            MessageLabel = new UILabel();
            DateTimeLabel = new UILabel();
            Icon = new IconView();

            AddSubviews(new UIView []{ Icon, _labelContainer });
            _labelContainer.AddSubviews(new UIView []{ MessageLabel, DateTimeLabel });
        }

        void SetupConstraints()
        {
            MessageLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            DateTimeLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _labelContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            Icon.TranslatesAutoresizingMaskIntoConstraints = false;

            MessageLabel.Lines = 0;
            MessageLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 14);
            MessageLabel.TextColor = YuFit.IOS.Themes.YuFitStyleKit.White;

            DateTimeLabel.Lines = 0;
            DateTimeLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 12);
            DateTimeLabel.TextColor = YuFit.IOS.Themes.YuFitStyleKit.White;

            this.AddConstraints(
                Icon.Height().EqualTo().HeightOf(this).WithMultiplier(0.7f),
                Icon.Width().EqualTo().HeightOf(this).WithMultiplier(0.7f),
                Icon.WithSameCenterY(this),
                Icon.Left().EqualTo(20.0f).LeftOf(this),
                _labelContainer.CenterY().EqualTo(0.0f).CenterYOf(this),
                _labelContainer.Left().EqualTo(10.0f).RightOf(Icon),
                _labelContainer.Right().EqualTo(-20.0f).RightOf(this)
            );

            _labelContainer.AddConstraints(
                MessageLabel.WithSameWidth(_labelContainer),
                MessageLabel.WithSameCenterX(_labelContainer),
                MessageLabel.WithSameTop(_labelContainer),

                DateTimeLabel.WithSameWidth(_labelContainer),
                DateTimeLabel.WithSameCenterX(_labelContainer),
                DateTimeLabel.Below(MessageLabel, 2.0f)
            );

            this.AddConstraints( _labelContainer.Bottom().EqualTo().BottomOf(DateTimeLabel) );

            //this.AddConstraint(NSLayoutConstraint.Create(MessageLabel, NSLayoutAttribute.Height, NSLayoutRelation.LessThanOrEqual, this, NSLayoutAttribute.Height, 1.0f, 1.0f));
        }
    }
}

