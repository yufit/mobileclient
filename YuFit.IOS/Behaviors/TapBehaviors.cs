using UIKit;

using System;

// Analysis disable once InconsistentNaming

namespace YuFit.IOS.Behaviors
{
    public class TapBehavior
    {
        public System.Windows.Input.ICommand Command { get;set; }

        public TapBehavior(UIView view)
        {
            view.UserInteractionEnabled = true;
            var tap = new UITapGestureRecognizer(() => 
            {
                var command = Command;
                if (command != null) { command.Execute(null); }
            });

            view.AddGestureRecognizer(tap);
        }
    }

    public class SwipeBehavior
    {
        public System.Windows.Input.ICommand Command { get;set; }

        public SwipeBehavior(UIView view, UISwipeGestureRecognizerDirection direction)
        {
            var swipe = new UISwipeGestureRecognizer(() => 
            {
                var command = Command;
                if (command != null) { command.Execute(null); }
            });

            swipe.Direction=direction;
            view.AddGestureRecognizer(swipe);
        }
    }


    public class SwipeLeftBehavior : SwipeBehavior { public SwipeLeftBehavior(UIView view) : base(view, UISwipeGestureRecognizerDirection.Left) { } }
    public class SwipeRightBehavior : SwipeBehavior { public SwipeRightBehavior(UIView view) : base(view, UISwipeGestureRecognizerDirection.Right) { } }
    public class SwipeUpBehavior : SwipeBehavior { public SwipeUpBehavior(UIView view) : base(view, UISwipeGestureRecognizerDirection.Up) { } }
    public class SwipeDownBehavior : SwipeBehavior { public SwipeDownBehavior(UIView view) : base(view, UISwipeGestureRecognizerDirection.Down) { } }
}

