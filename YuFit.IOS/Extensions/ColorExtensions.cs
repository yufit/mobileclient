﻿using System;
using UIKit;
using YuFit.Core.Models;

namespace YuFit.IOS.Extensions
{
    public static class ColorExtensions
    {
        public static UIColor ToUIColor (this Color color)
        {
            return new UIColor(color.Red/255.0f, color.Green/255.0f, color.Blue/255.0f, 1.0f);
        }
    }
}

