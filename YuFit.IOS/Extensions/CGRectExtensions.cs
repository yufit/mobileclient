using System;
using CoreGraphics;

namespace YuFit.IOS.Extensions
{
	public static class CGRectExtensions
	{
		public static nfloat ComputeScaleFactor(this CGRect src, CGRect dest)
		{
			nfloat scaleFactor = 1.0f;

			nfloat sourceAspect = src.Width / src.Height;
			nfloat destAspect = dest.Width / dest.Height;

			if (sourceAspect > destAspect) {
				scaleFactor = dest.Width / src.Width;
			} else {
				scaleFactor = dest.Height / src.Height;
			}

			return scaleFactor;
		}

		public static nfloat ComputeScaleFactor(this CGRect src, CGSize dest)
		{
			nfloat scaleFactor = 1.0f;

			nfloat sourceAspect = src.Width / src.Height;
			nfloat destAspect = dest.Width / dest.Height;

			if (sourceAspect > destAspect) {
				scaleFactor = dest.Width / src.Width;
			} else {
				scaleFactor = dest.Height / src.Height;
			}

			return scaleFactor;
		}

		public static CGRect CenterInto(this CGRect src, CGRect dest)
		{
			return new CGRect {
				X = (dest.Width - src.Width) / 2.0f,
				Y = (dest.Height - src.Height) / 2.0f,
				Width = src.Width,
				Height = src.Height
			};
		}

		public static CGRect ScaleRect(this CGRect src, float factor)
		{
			return new CGRect {
				X = src.X,
				Y = src.Y,
				Width = src.Width * factor,
				Height = src.Height * factor
			};
		}

		public static CGRect ScaleRect(this CGRect src, CGRect dest, bool keepWidth, bool keepHeight)
		{
			CGRect destRect = new CGRect();

			nfloat sourceAspect = src.Width / src.Height;
			nfloat destAspect = dest.Width / dest.Height;

			if (sourceAspect > destAspect)
			{
				// wider than high keep the width and scale the height
				destRect.Width = dest.Width;
				destRect.Height = dest.Width / sourceAspect;

				if (keepHeight)
				{
					nfloat resizePerc = dest.Height / destRect.Height;
					destRect.Width = dest.Width * resizePerc;
					destRect.Height = dest.Height;
				}
			}
			else
			{
				// higher than wide – keep the height and scale the width
				destRect.Height = dest.Height;
				destRect.Width = dest.Height * sourceAspect;

				if (keepWidth)
				{
					nfloat resizePerc = dest.Width / destRect.Width;
					destRect.Width = dest.Width;
					destRect.Height = dest.Height * resizePerc;
				}

			}

			return destRect;
		}
	}
}

