//
// Lift off from online samples.
//

using System;
using System.Linq.Expressions;
using System.Reflection;
using BigTed;
using UIKit;
using YuFit.Core.ViewModels;
using MvvmCross.Platform.Exceptions;

namespace YuFit.IOS.Extensions
{
	public static class ViewModelExtensions
	{
		public static void BindLoadingMessage<TViewModel> (
			this TViewModel viewModel, 
			UIView view, 
			Expression<Func<TViewModel, bool>> property, 
            Expression<Func<TViewModel, string>> message, 
			ProgressHUD.MaskType maskType = ProgressHUD.MaskType.Black
		) where TViewModel : BaseViewModel 
		{
			// Before we can do aything, we need to get the property info object.  If 
			// it ain't valid, barf all over...
			MemberExpression expression = property.Body as MemberExpression;
			if (expression == null) { throw new MvxException ("Failed to creating binding"); }

			PropertyInfo propertyInfo = expression.Member as PropertyInfo;
			if (propertyInfo == null) { throw new MvxException ("Failed to creating binding"); }

            MemberExpression expressionM = message.Body as MemberExpression;
            if (expression == null) { throw new MvxException ("Failed to creating binding"); }

            PropertyInfo propertyInfoM = expressionM.Member as PropertyInfo;
            if (propertyInfoM == null) { throw new MvxException ("Failed to creating binding"); }

			// We can now create our hud.  We will set an action to provide reusability
			// of the function for later.
			ProgressHUD hud = new ProgressHUD ();
            Action<bool, string> setMessageVisibility = (visible, msg) => {
				if (visible) {
					hud.Show (msg, maskType: maskType);
				} else {
					hud.Dismiss ();
				}
			};

			// Lets initialize the binding with the existing property value.  And
			// let us watch for the property changed event so that we can show or
			// hide the hud.
            setMessageVisibility ((bool)propertyInfo.GetValue (viewModel, null), (string)propertyInfoM.GetValue(viewModel, null));
			viewModel.PropertyChanged += (sender, e) => {
                if (e.PropertyName == propertyInfo.Name || e.PropertyName == propertyInfoM.Name) {
                    setMessageVisibility ((bool)propertyInfo.GetValue (viewModel, null), (string)propertyInfoM.GetValue(viewModel, null));
                } 
			};
		}

	}
}

