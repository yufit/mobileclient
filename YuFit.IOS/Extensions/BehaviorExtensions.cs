using YuFit.IOS.Behaviors;
using UIKit;

// Analysis disable once InconsistentNaming
using MvvmCross.Binding.BindingContext;


namespace YuFit.IOS.Extensions
{
    public static class BehaviorExtensions
    {
        public static TapBehavior           Tap(this UIView view)        { return new TapBehavior(view); }
        public static SwipeLeftBehavior     SwipeLeft(this UIView view)  { return new SwipeLeftBehavior(view); }
        public static SwipeRightBehavior    SwipeRight(this UIView view) { return new SwipeRightBehavior(view); }
        public static SwipeUpBehavior       SwipeUp(this UIView view)    { return new SwipeUpBehavior(view); }
        public static SwipeDownBehavior     SwipeDown(this UIView view)  { return new SwipeDownBehavior(view); }

        public static void RegisterExtension(IMvxBindingNameRegistry registry) {
            registry.AddOrOverwrite(typeof (TapBehavior), "Command");
            registry.AddOrOverwrite(typeof (SwipeLeftBehavior), "Command");
            registry.AddOrOverwrite(typeof (SwipeRightBehavior), "Command");
            registry.AddOrOverwrite(typeof (SwipeUpBehavior), "Command");
            registry.AddOrOverwrite(typeof (SwipeDownBehavior), "Command");
        }
    }

}

