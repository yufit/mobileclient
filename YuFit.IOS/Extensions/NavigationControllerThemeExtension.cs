using System;
using UIKit;
using YuFit.IOS.Controls;
using CoreGraphics;

namespace YuFit.IOS.Extensions
{
    public static class NavigationControllerThemeExtension
    {
        public static void ApplyTheme(this UINavigationController navController)
        {
            navController.NavigationBar.BarTintColor = Themes.NavigationBar.TintColor;
            navController.NavigationBar.Translucent = false;
            navController.NavigationBar.BarStyle = UIBarStyle.Black;

            UINavigationBar.Appearance.SetBackgroundImage(new UIImage(), UIBarMetrics.Default);
            UINavigationBar.Appearance.ShadowImage = new UIImage();

            // HACK START TEMPORARY
            navController.NavigationBar.Subviews[0].Subviews[0].BackgroundColor = UIColor.Clear;
            // HACK END

            navController.NavigationBar.TitleTextAttributes = new UIStringAttributes { 
                Font = Themes.NavigationBar.TitleFont,
                ForegroundColor = Themes.NavigationBar.FontColor
            };
        }
    }
}

