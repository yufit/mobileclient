using UIKit;
using System.Globalization;

namespace YuFit.IOS.Extensions
{
    public static class UIDeviceExtension
    {
        public static bool IsAtLeastIOS8(this UIDevice device) { return UIDevice.CurrentDevice.CheckSystemVersion(8, 0); }
        public static bool IsAtLeastIOS7(this UIDevice device) { return UIDevice.CurrentDevice.CheckSystemVersion(7, 0); }
    }
}

