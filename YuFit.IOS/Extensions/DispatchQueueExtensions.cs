using System;
using CoreFoundation;
using UIKit;

namespace YuFit.IOS.Extensions
{
    public static class DispatchQueueExtension
    {
        public static void LaterOnMainThread(this DispatchQueue queue, long delay, Action action)
        {
            queue.DispatchAfter(
                new DispatchTime(DispatchTime.Now, delay), 
                () => UIApplication.SharedApplication.InvokeOnMainThread(action));
        }
    }
}

