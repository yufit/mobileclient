using System;
using MapKit;
using CoreLocation;
using CoreGraphics;
using System.Collections.Generic;
using System.Linq;

namespace YuFit.IOS.Extensions
{
	public static class MapViewExtensions
	{
		const double MERCATOR_OFFSET = 268435456;
		const double MERCATOR_RADIUS = 85445659.44705395;

		static double LongitudeToPixelSpaceX (double longitude)
		{
			return Math.Round (MERCATOR_OFFSET + MERCATOR_RADIUS * longitude * Math.PI / 180.0);
		}

		static double LatitudeToPixelSpaceY (double latitude)
		{
			return Math.Round (MERCATOR_OFFSET - MERCATOR_RADIUS * Math.Log ((1 + Math.Sin (latitude * Math.PI / 180.0)) / (1 - Math.Sin (latitude * Math.PI / 180.0))) / 2.0);
		}

		static double PixelSpaceXToLongitude (double pixelX)
		{
			return ((Math.Round (pixelX) - MERCATOR_OFFSET) / MERCATOR_RADIUS) * 180.0 / Math.PI;
		}

		static double PixelSpaceYToLatitude (double pixelY)
		{
			return (Math.PI / 2.0 - 2.0 * Math.Tan (Math.Exp ((Math.Round (pixelY) - MERCATOR_OFFSET) / MERCATOR_RADIUS))) * 180.0 / Math.PI;
		}


		public static MKCoordinateSpan GetCoordinateSpanForCoordinate (this MKMapView mapView, CLLocationCoordinate2D centerCoordinate, int zoomLevel)
		{
			// convert center coordiate to pixel space
			double centerPixelX = LongitudeToPixelSpaceX (centerCoordinate.Longitude);
			double centerPixelY = LatitudeToPixelSpaceY (centerCoordinate.Latitude);

			// determine the scale value from the zoom level
			int zoomExponent = 20 - zoomLevel;
			double zoomScale = Math.Pow (2, zoomExponent);

			// scale the map’s size in pixel space
			CGSize mapSizeInPixels = mapView.Bounds.Size;
			double scaledMapWidth = mapSizeInPixels.Width * zoomScale;
			double scaledMapHeight = mapSizeInPixels.Height;

			// figure out the position of the top-left pixel
			double topLeftPixelX = centerPixelX - (scaledMapWidth / 2);
			double topLeftPixelY = centerPixelY - (scaledMapHeight / 2);

			// find delta between left and right longitudes
			double minLng = PixelSpaceXToLongitude (topLeftPixelX);
			double maxLng = PixelSpaceXToLongitude (topLeftPixelX + scaledMapWidth);
			double longitudeDelta = maxLng - minLng;

			// find delta between top and bottom latitudes
			double minLat = PixelSpaceYToLatitude (topLeftPixelY);
			double maxLat = PixelSpaceYToLatitude (topLeftPixelY + scaledMapHeight);
			double latitudeDelta = -1 * (maxLat - minLat);

			// create and return the lat/lng span
			MKCoordinateSpan span = new MKCoordinateSpan (latitudeDelta, longitudeDelta);

			return span;
		}

		public static void CenterMapToAnnotations(this MKMapView Map, double latlongPadding)
		{
			if (Map == null || Map.Annotations == null)
				return;


			List<MKAnnotation> annotations = new List<MKAnnotation>();
			foreach (var a in Map.Annotations)
			{
				annotations.Add(a as MKAnnotation);
			}
			if (annotations.Count > 1)
			{

				double maxLat = annotations.OrderByDescending (e => e.Coordinate.Latitude).FirstOrDefault ().Coordinate.Latitude;
				double maxLong = annotations.OrderByDescending (e => e.Coordinate.Longitude).FirstOrDefault ().Coordinate.Longitude;

				double minLat = annotations.OrderBy (e => e.Coordinate.Latitude).FirstOrDefault ().Coordinate.Latitude;
				double minLong = annotations.OrderBy (e => e.Coordinate.Longitude).FirstOrDefault ().Coordinate.Longitude;
				MKCoordinateRegion region = new MKCoordinateRegion ();

				region.Center.Latitude = (maxLat + minLat) / 2;
				region.Center.Longitude = (maxLong + minLong) / 2;
				region.Span.LatitudeDelta = maxLat - minLat+latlongPadding ;
				region.Span.LongitudeDelta = maxLong - minLong+latlongPadding;

				Map.Region = region;
			}

		}	
	}
}

