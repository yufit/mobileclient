using System;
using CoreLocation;
using Coc.MvvmCross.Plugins.Location;

namespace YuFit.IOS.Extensions
{
    public static class MvxCoordinatesExtension
    {
        public static CLLocationCoordinate2D ToCLLocationCoordinate2D(this CocCoordinates that)
        {
            return new CLLocationCoordinate2D(that.Latitude, that.Longitude);
        }

    }

    public static class CLLocationCoordinate2DExtension
    {
        public static CocCoordinates ToMvxCoordinates(this CLLocationCoordinate2D coord) {
            return new CocCoordinates {
                Latitude = coord.Latitude,
                Longitude = coord.Longitude
            };
        }
    }
}

