using System;
using UIKit;
using System.Collections.Generic;

namespace YuFit.IOS.Extensions
{
    public static class UIViewExtensions
    {
        public static NSLayoutConstraint[] FitAndCenterInside(this UIView fittingView, UIView containerView)
        {
            List<NSLayoutConstraint> constraints = new List<NSLayoutConstraint>();
            constraints.Add(NSLayoutConstraint.Create(fittingView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.Width, 1.0f, 0));
            constraints.Add(NSLayoutConstraint.Create(fittingView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.Height, 1.0f, 0));
            constraints.Add(NSLayoutConstraint.Create(fittingView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.CenterY, 1.0f, 0));
            constraints.Add(NSLayoutConstraint.Create(fittingView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.CenterX, 1.0f, 0));
            return constraints.ToArray();
        }

        public static NSLayoutConstraint[] FitInside(this UIView fittingView, UIView containerView)
        {
            List<NSLayoutConstraint> constraints = new List<NSLayoutConstraint>();
            constraints.Add(NSLayoutConstraint.Create(fittingView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.Width, 1.0f, 0));
            constraints.Add(NSLayoutConstraint.Create(fittingView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.Height, 1.0f, 0));
            return constraints.ToArray();
        }

        public static NSLayoutConstraint EqualHeight(this UIView fittingView, UIView containerView, float percent = 1.0f)
        {
            return NSLayoutConstraint.Create(fittingView, NSLayoutAttribute.Height, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.Height, percent, 0);
        }

        public static NSLayoutConstraint EqualWidth(this UIView fittingView, UIView containerView, float percent = 1.0f)
        {
            return NSLayoutConstraint.Create(fittingView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.Width, percent, 0);
        }

        public static NSLayoutConstraint SetHeight(this UIView fittingView, float height)
        {
            return NSLayoutConstraint.Create(fittingView, NSLayoutAttribute.Height, 0, null, NSLayoutAttribute.NoAttribute, 1.0f, height);
        }

        public static NSLayoutConstraint SetWidth(this UIView fittingView, float width)
        {
            return NSLayoutConstraint.Create(fittingView, NSLayoutAttribute.Width, 0, null, NSLayoutAttribute.NoAttribute, 1.0f, width);
        }

        public static NSLayoutConstraint CenterVerticallyInside(this UIView fittingView, UIView containerView, float offset = 0.0f)
        {
            return NSLayoutConstraint.Create(fittingView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.CenterY, 1.0f, offset);
        }

        public static NSLayoutConstraint CenterHorizontallyInside(this UIView fittingView, UIView containerView, float offset = 0.0f)
        {
            return NSLayoutConstraint.Create(fittingView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.CenterX, 1.0f, offset);
        }

        public static NSLayoutConstraint AlignToTopOf(this UIView fittingView, UIView containerView, float offset = 0.0f)
        {
            return NSLayoutConstraint.Create(fittingView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.Top, 1.0f, offset);
        }

        public static NSLayoutConstraint AlignToBottomOf(this UIView fittingView, UIView containerView, float offset = 0.0f)
        {
            return NSLayoutConstraint.Create(fittingView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, containerView, NSLayoutAttribute.Bottom, 1.0f, offset);
        }

    }
}

