using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;
using MapKit;

namespace YuFit.IOS.CustomBindings
{
    public class MvxMKMapViewRegionTargetBinding : MvxConvertingTargetBinding
    {
        protected MKMapView MapView
        {
            get { return Target as MKMapView; }
        }

        private bool _subscribed;
        bool _selfUpdating;

        public MvxMKMapViewRegionTargetBinding(MKMapView mapView)
            : base(mapView)
        {
            if (mapView == null)
            {
                MvxBindingTrace.Trace(MvxTraceLevel.Error, "Error - MapView is null in MvxUIMapViewRegionTargetBinding");
            }
        }

        public override MvxBindingMode DefaultMode
        {
            get { return MvxBindingMode.TwoWay; }
        }

        public override System.Type TargetType
        {
            get { return typeof (MKCoordinateRegion); }
        }

        protected override void SetValueImpl(object target, object value)
        {
            if (!_selfUpdating) {
                _selfUpdating = true;
                ((MKMapView)target).SetRegion((MKCoordinateRegion) value, true);
                _selfUpdating = false;
            }
        }

        public override void SubscribeToEvents()
        {
            var target = MapView;
            if (target == null)
            {
                MvxBindingTrace.Trace(MvxTraceLevel.Error,
                    "Error - UITextField is null in MvxUITextFieldTextTargetBinding");
                return;
            }

            target.RegionChanged += HandleRegionChanged;
            _subscribed = true;
        }

        protected override void Dispose(bool isDisposing)
        {
            base.Dispose(isDisposing);
            if (isDisposing)
            {
                var map = MapView;
                if (map != null && _subscribed)
                {
                    map.RegionChanged -= HandleRegionChanged;
                    _subscribed = false;
                }
            }
        }

        private void HandleRegionChanged(object sender, System.EventArgs e)
        {
            var view = MapView;
            if (view == null)
                return;

            if (!_selfUpdating) {
                _selfUpdating = true;
                FireValueChanged(view.Region);
                _selfUpdating = false;
            }
        }

        public MKCoordinateRegion CurrentRegion
        { 
            get 
            { 
                var view = MapView;
                if (view == null)
                    return new MKCoordinateRegion();
                return view.Region;
            }
        }
    }
}