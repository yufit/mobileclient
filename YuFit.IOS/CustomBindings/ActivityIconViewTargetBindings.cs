﻿using System;

using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;

using CoreGraphics;
using UIKit;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;

namespace YuFit.IOS.CustomBindings
{
    public class ActivityIconViewTargetBindings : MvxConvertingTargetBinding
    {
        protected       ActivityIconView ActivityIconView { get { return Target as ActivityIconView; } }
        public override MvxBindingMode   DefaultMode      { get { return MvxBindingMode.OneWay; } }
        public override Type             TargetType       { get { return typeof (Action<CGRect, UIColor>); } }

        public ActivityIconViewTargetBindings(ActivityIconView target) : base(target)
        {
            if (target == null) {
                MvxBindingTrace.Trace(MvxTraceLevel.Error, 
                    "Error - iconView is null in MvxUIMapViewRegionTargetBinding");
            }
        }

        protected override void SetValueImpl(object target, object value)
        {
            if (value != null) {
                ActivityIconView.DrawIcon = YuFitStyleKitExtension.ActivityDrawingMethod((string) value);
            }
        }
    }

    public class IconViewTargetBindings : MvxConvertingTargetBinding
    {
        protected       IconView         IconView         { get { return Target as IconView; } }
        public override MvxBindingMode   DefaultMode      { get { return MvxBindingMode.OneWay; } }
        public override Type             TargetType       { get { return typeof (Action<CGRect, UIColor>); } }

        public IconViewTargetBindings(IconView target) : base(target)
        {
            if (target == null) {
                MvxBindingTrace.Trace(MvxTraceLevel.Error, 
                    "Error - iconView is null in MvxUIMapViewRegionTargetBinding");
            }
        }

        protected override void SetValueImpl(object target, object value)
        {
            if (value != null) {
                IconView.DrawIcon = YuFitStyleKitExtension.ActivityDrawingMethod((string) value);
            }
        }
    }

    public class BackgroundColorTargetBindings : MvxConvertingTargetBinding
    {
        protected       UIView           View             { get { return Target as UIView; } }
        public override MvxBindingMode   DefaultMode      { get { return MvxBindingMode.OneWay; } }
        public override Type             TargetType       { get { return typeof (UIColor); } }

        public BackgroundColorTargetBindings(UIView target) : base(target)
        {
            if (target == null) {
                MvxBindingTrace.Trace(MvxTraceLevel.Error, "Error - View is null in BackgroundColorTargetBindings");
            }
        }

        protected override void SetValueImpl(object target, object value)
        {
            if (value != null) {
                var colors = YuFitStyleKitExtension.ActivityBackgroundColors((string) value);
                View.BackgroundColor = colors.Base;
            }
        }
    }

    public class DarkBackgroundColorTargetBindings : MvxConvertingTargetBinding
    {
        protected       UIView           View             { get { return Target as UIView; } }
        public override MvxBindingMode   DefaultMode      { get { return MvxBindingMode.OneWay; } }
        public override Type             TargetType       { get { return typeof (UIColor); } }

        public DarkBackgroundColorTargetBindings(UIView target) : base(target)
        {
            if (target == null) {
                MvxBindingTrace.Trace(MvxTraceLevel.Error, "Error - View is null in BackgroundColorTargetBindings");
            }
        }

        protected override void SetValueImpl(object target, object value)
        {
            if (value != null) {
                var colors = YuFitStyleKitExtension.ActivityBackgroundColors((string) value);
                View.BackgroundColor = colors.Dark;
            }
        }
    }
}

