﻿using System;
using System.Windows.Input;
using PHFComposeBarView;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;
using MvvmCross.Platform.WeakSubscription;

namespace YuFit.IOS.CustomBindings
{
    public class MvxComposeBarViewCommandTargetBinding : MvxConvertingTargetBinding
    {
        private ICommand _command;
        private IDisposable _canExecuteSubscription;
        private readonly EventHandler<EventArgs> _canExecuteEventHandler;

        protected ComposeBarView Control { get { return Target as ComposeBarView; } }

        public MvxComposeBarViewCommandTargetBinding (ComposeBarView control) : base(control)
        {
            if (control == null) {
                MvxBindingTrace.Trace(MvxTraceLevel.Error, "Error - UIControl is null in MvxUIControlTouchUpInsideTargetBinding");
            } else {
                control.DidPressButton += DidPressButtonClicked;
            }

            _canExecuteEventHandler = new EventHandler<EventArgs>(OnCanExecuteChanged);
        }

        private void DidPressButtonClicked(object sender, EventArgs eventArgs)
        {
            if (_command == null) { return; }
            if (!_command.CanExecute(null)) { return; }

            var composeBar = sender as ComposeBarView;
            _command.Execute(composeBar.Text);
            composeBar.Text = string.Empty;
            composeBar.ResignFirstResponder ();
        }

        public override MvxBindingMode DefaultMode {get { return MvxBindingMode.OneWay; }}
        public override Type TargetType {get { return typeof(ICommand);} }

        protected override void SetValueImpl(object target, object value)
        {
            if (_canExecuteSubscription != null) {
                _canExecuteSubscription.Dispose();
                _canExecuteSubscription = null;
            }
            _command = value as ICommand;
            if (_command != null) {
                _canExecuteSubscription = _command.WeakSubscribe(_canExecuteEventHandler);
            }
            RefreshEnabledState();
        }

        private void RefreshEnabledState()
        {
            var view = Control;
            if (view == null) { return; }

            var shouldBeEnabled = false;
            if (_command != null) {
                shouldBeEnabled = _command.CanExecute(null);
            }
            view.Enabled = shouldBeEnabled;
        }

        private void OnCanExecuteChanged(object sender, EventArgs e)
        {
            RefreshEnabledState();
        }

        protected override void Dispose(bool isDisposing)
        {
            if (isDisposing) {
                if (Control != null) {
                    Control.DidPressButton -= DidPressButtonClicked;
                }
                if (_canExecuteSubscription != null) {
                    _canExecuteSubscription.Dispose();
                    _canExecuteSubscription = null;
                }
            }
            base.Dispose(isDisposing);
        }
    }
}

