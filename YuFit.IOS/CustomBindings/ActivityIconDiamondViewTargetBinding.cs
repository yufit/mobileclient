﻿using System;
using System.Linq;

using CoreGraphics;

using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;

using UIKit;
using YuFit.Core.Interfaces.Services;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;

namespace YuFit.IOS.CustomBindings
{
    public class ActivityIconDiamondViewTargetBindings : MvxConvertingTargetBinding
    {
        protected       ActivityIconDiamondView ActivityIconView { get { return Target as ActivityIconDiamondView; } }
        public override MvxBindingMode   DefaultMode      { get { return MvxBindingMode.OneWay; } }
        public override Type             TargetType       { get { return typeof (Action<CGRect, UIColor>); } }

        public ActivityIconDiamondViewTargetBindings(ActivityIconDiamondView target) : base(target)
        {
            if (target == null) {
                MvxBindingTrace.Trace(MvxTraceLevel.Error, 
                    "Error - iconView is null in MvxUIMapViewRegionTargetBinding");
            }
        }

        protected override void SetValueImpl(object target, object value)
        {
            if (value != null) {
                ActivityIconView.DrawIcon = YuFitStyleKitExtension.ActivityDrawingMethod((string) value);

                UIColor colorDark = UIColor.White;
                UIColor colorBase = UIColor.White;

                var sport = (string) value;
                var categories = Mvx.Resolve<IFitActivityService>().GetEventCategories();
                var category = categories.FirstOrDefault(c => c.Name == sport);
                if (category != null) {
                    var ccolors = category.Colors;
                    colorDark = new UIColor(ccolors.Dark.Red/255.0f, ccolors.Dark.Green/255.0f, ccolors.Dark.Blue/255.0f, 1.0f);
                    colorBase = new UIColor(ccolors.Base.Red/255.0f, ccolors.Base.Green/255.0f, ccolors.Base.Blue/255.0f, 1.0f);
                } else {
                    ActivityColors colors;
                    colors = YuFitStyleKitExtension.ActivityBackgroundColors(sport);
                    colorDark = colors.Dark;
                    colorBase = colors.Base;
                }
                ActivityIconView.DiamondColor = colorBase;
                ActivityIconView.ActivityFillColor = YuFitStyleKit.White;
            }
        }
    }
}

