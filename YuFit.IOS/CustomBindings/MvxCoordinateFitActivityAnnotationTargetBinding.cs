﻿using System;
using CoreLocation;
using YuFit.Core.Models;
using YuFit.IOS.Views.Dashboard;
using YuFit.IOS.Controls.Annotations;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target;
using MvvmCross.Platform;
using MvvmCross.Platform.Platform;

namespace YuFit.IOS.CustomBindings
{
    public class MvxCoordinateFitActivityAnnotationTargetBinding : MvxConvertingTargetBinding
    {
        public MvxCoordinateFitActivityAnnotationTargetBinding(MapAnnotation annotation)
            : base(annotation)
        {

        }

        public override MvxBindingMode DefaultMode
        {
            get
            {
                return MvxBindingMode.OneWay;
            }
        }

        public override Type TargetType
        {
            get
            {
                return typeof(CLLocationCoordinate2D);
            }
        }

        protected override void SetValueImpl (object target, object value)
        {
            var annotation = target as MapAnnotation;
            if (annotation == null)
            {
                return;
            }

            annotation.SetCoordinate((CLLocationCoordinate2D) value);
        }


    }
}

