using System;
using System.Linq;
using UIKit;
using YuFit.IOS.Attributes;
using YuFit.IOS.Presenters.Transitions;
using YuFit.IOS.Interfaces;
using System.Collections.Generic;

namespace YuFit.IOS.Presenters
{
	public class NavigationController : UINavigationController 
	{
		/// <summary>
		/// Helper cast delegate
		/// </summary>
		/// <value>The delegate.</value>
		public new NavigationControllerDelegate Delegate { get { return base.Delegate as NavigationControllerDelegate; } }

		/// <summary>
		/// Initializes a new instance of the <see cref="NavigationController"/> class.
		/// </summary>
		/// <param name="viewController">View controller.</param>
		public NavigationController(UIViewController viewController) : base(viewController) {
			base.Delegate = new NavigationControllerDelegate(this);
		}

		/// <summary>
		/// Pushes a view controller onto the UINavigationController's navigation stack.
		/// </summary>
		/// <see cref="T:MonoTouch.UIKit.UITabBarController"></see>
		/// <param name="viewController">View controller.</param>
		/// <param name="animated">If set to <c>true</c> animated.</param>
		public override void PushViewController(UIViewController viewController, bool animated)
		{
            if (animated) { SetupViewAnimation<PushTransitionAttribute>(viewController); }
            base.PushViewController(viewController, animated);
		}

		/// <summary>
		/// Pops to root view controller.
		/// </summary>
		/// <param name="animated">If set to <c>true</c> animated.</param>
		/// <returns>An array of view controller that were popped from the stack in the process of returning to the root controller.</returns>
		public override UIViewController[] PopToRootViewController(bool animated)
		{
			if (animated && ViewControllers.Length > 0) { SetupViewAnimation<PopTransitionAttribute>(ViewControllers[0]); }
			return base.PopToRootViewController(animated);
		}

		/// <summary>
		/// Pops the view controller animated.
		/// </summary>
		/// <param name="animated">If set to <c>true</c> animated.</param>
		/// <returns>The view controller popped from the stack</returns>
		public override UIViewController PopViewController(bool animated)
		{
			if (animated) { SetupViewAnimation<PopTransitionAttribute>(TopViewController); }
			return base.PopViewController(animated);
		}

        public override UIViewController[] PopToViewController (UIViewController viewController, bool animated)
        {
            if (animated) { SetupViewAnimation<PopTransitionAttribute>(TopViewController); }
            return base.PopToViewController(viewController, animated);
        }

		/// <summary>
		/// Setups the view animation for the given view based on the attribute specified on the view.
		/// </summary>
		/// <param name="view">View.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		private void SetupViewAnimation<T>(UIViewController view) where T : CustomTransitionAttribute
		{
			Attribute[] attributes = Attribute.GetCustomAttributes (view.GetType ());
			T attribute = (T) attributes.FirstOrDefault (a => a is T);
			if (attribute == null) { 
                Delegate.Animator = null;
                return; 
            }

            CustomAnimatedTransition transition = (CustomAnimatedTransition) attribute.Transition.GetConstructor(new Type[] {typeof(float)}).Invoke(new object[] {attribute.Duration});
			if (transition != null) {
				Delegate.Animator = transition;
			}
		}

		/// <summary>
		/// Construct an object from a type.
		/// </summary>
		/// <returns>The new object.</returns>
		/// <param name="t">T.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T GetNewObject<T>(Type t)
		{
			try   { return (T) t.GetConstructor(new Type[] {}).Invoke(new object[] {}); }
			catch { return default(T); }
		}
	}
}
