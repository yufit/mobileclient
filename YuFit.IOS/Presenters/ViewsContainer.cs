using System;
using System.Linq;
using UIKit;
using MvvmCross.iOS.Views;
using MvvmCross.Core.ViewModels;

namespace YuFit.IOS.Presenters
{
	public class ViewsContainer : MvxIosViewsContainer
	{
		protected override IMvxIosView CreateViewOfType (Type viewType, MvxViewModelRequest request)
		{
            Attribute[] attributes = Attribute.GetCustomAttributes (viewType);
            Attributes.StoryboardViewCreationAttribute attribute = (Attributes.StoryboardViewCreationAttribute) attributes.FirstOrDefault (a => a is Attributes.StoryboardViewCreationAttribute);
            return attribute != null ? attribute.CreateView() : base.CreateViewOfType(viewType, request);
		}
	}
}

