﻿using System;
using Coc.MvxAdvancedPresenter.Touch;
using UIKit;

namespace YuFit.IOS.Presenters
{
    public class LoginPresenter : NavigationViewPresenter
    {
        public LoginPresenter ()
        {
        }

        ~LoginPresenter ()
        {
            Console.WriteLine("Destroy LoginPresenter");
        }

        /// <summary>
        /// The way the presenter works, when swapping root window, views in the view controllers get yank
        /// leaving ViewWillDisappear not being called.  Our view approach is to remove anything that could
        /// keep a ref to the view in the ViewDidDisapear.  Without this, our loginview will leaks.
        /// 
        /// TODO I'd like to find a better mechanism to solve that problem.
        /// </summary>
        /// <param name="window">Window.</param>
        protected override void WillDetachedFromWindow (UIWindow window)
        {
            ((UINavigationController) RootViewController).ViewControllers[0].ViewWillDisappear(false);
            base.WillDetachedFromWindow(window);
        }
    }
}

