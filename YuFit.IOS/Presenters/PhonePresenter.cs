using System;
using System.Linq;


using Coc.MvxAdvancedPresenter.Touch;

using UIKit;

using YuFit.Core.ViewModels;
using YuFit.Core.ViewModels.Activity.Summary;
using YuFit.Core.ViewModels.CreateActivity;
using YuFit.Core.ViewModels.CreateActivity.ConfigureActivities;

using YuFit.IOS.Attributes;
using YuFit.IOS.Interfaces;
using Xamarin;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Presenters
{
    public class PhoneSingleViewPresenter : SingleViewPresenter
    {
        ~PhoneSingleViewPresenter ()
        {
            Console.WriteLine("Destroy PhoneSingleViewPresenter");
        }
    }

    public class PhonePresenter : NavigationViewPresenter
    {
        INotificationView _currentNotification;

        MvxPresentationHint _specifiedHint;
        protected override UINavigationController CreateNavController (IMvxIosView view)
        {
            return new NavigationController(view as UIViewController);
        }

        ~PhonePresenter ()
        {
            Console.WriteLine("Destroy PhonePresenter");
        }

        public override void Show (IMvxIosView view)
        {
            // SPECIAL CASE. CHECK TO SEE IF THE REQUEST IS FOR A SUMMARYVIEWMODEL, IN WHICH CASE
            // WE NEED TO POP TO THE DASHBOARD and TELL IT TO SHOW THE VIEW MODEL.
//            if (view.Request.ViewModelType == typeof(ActivitySummaryViewModel)) {
//                UINavigationController navController = RootViewController as UINavigationController;
//                navController.PopToRootViewController(true);
//            }
//            else
            if (view is INotificationView) {
                PresentNotification(view as INotificationView);
                return;
            }

            // SPECIAL CASE.  IT IS POSSIBLE, WHEN DOING A PRESS TO CREATE AN EVENT ON THE DASHBOARD
            // MAP THAT 2 CreateActivityViewModel GETS REQUESTED TO BE DISPLAYED.
            // FIX: IF THE TOP IS ALREADY THE CreateActivityViewModel, ignore the request.
            if (view.Request.ViewModelType == typeof(CreateFitActivityViewModel)) {
                if (IsPresentingViewModel(view.Request.ViewModelType)) {
                    Mvx.Trace("Presenting create view model twice");
                    return;
                }
            }

            if (ShouldPresentInViewControllerContainer(view)) {
                PresentInViewControllerContainer(view);
            } else {
                base.Show(view);
            }
        }

        public override void ChangePresentation (MvxPresentationHint hint)
        {
            if (!(hint is MvxClosePresentationHint)) {
                _specifiedHint = hint;
            } else if (hint is MvxClosePresentationHint) {
                _specifiedHint = null;
            }

            base.ChangePresentation(hint);
        }

        public override void Close (IMvxViewModel viewModel)
        {
            if (IsPresentingViewModel(viewModel)) {
                UINavigationController navController = RootViewController as UINavigationController;
                IContainerViewPresenter viewPresenter = navController.TopViewController as IContainerViewPresenter;
                viewPresenter.DismissViewAssociatedToViewModel(viewModel);
            } else {
                if (viewModel is BaseActivityConfigureVM)
                {
                    UINavigationController navController = (UINavigationController) RootViewController;
                    UIViewController toPopTo = navController.ViewControllers[navController.ViewControllers.Length - 3];
                    ((UINavigationController) RootViewController).PopToViewController(toPopTo, true);
                }
                else if (viewModel is DisplayReceivedNotificationViewModel)
                {
                    DismissCurrentNotification();  
                } else {
                    base.Close(viewModel);
                }
            }
        }

        private bool ShouldPresentInViewControllerContainer(MvvmCross.iOS.Views.IMvxIosView view)
        {
            try {
                Attribute[] attributes = Attribute.GetCustomAttributes (view.GetType ());
                PresentViewInContainerAttribute attribute = (PresentViewInContainerAttribute) attributes.FirstOrDefault (a => a is PresentViewInContainerAttribute);
                return attribute != null;
            } catch (Exception ex) {
                Insights.Report(ex);
                return false;
            }
        }

        private bool IsPresentingViewModel(IMvxViewModel viewModel)
        {
            try {
                UINavigationController navController = RootViewController as UINavigationController;
                if (navController.TopViewController is IContainerViewPresenter) {
                    IContainerViewPresenter viewPresenter = navController.TopViewController as IContainerViewPresenter;
                    return viewPresenter.IsPresenting(viewModel);
                }
                return false;
            } catch (Exception ex) {
                Insights.Report(ex);
                return false;
            }
        }

        private bool IsPresentingViewModel(Type viewModelType)
        {
            try {
                UINavigationController navController = RootViewController as UINavigationController;
                if (navController.TopViewController is IContainerViewPresenter)
                {
                    IContainerViewPresenter viewPresenter = navController.TopViewController as IContainerViewPresenter;
                    return viewPresenter.IsPresenting(viewModelType);
                }
                if (navController.TopViewController != null)
                {
                    var view = (IMvxIosView) navController.TopViewController;
                    return view.ViewModel.GetType() == viewModelType;
                }
                return false;
            } catch (Exception ex) {
                Insights.Report(ex);
                return false;
            }
        }

        private void PresentInViewControllerContainer(IMvxIosView view)
        {
            try {
                Attribute[] attributes = Attribute.GetCustomAttributes (view.GetType ());
                PresentViewInContainerAttribute attribute = (PresentViewInContainerAttribute) attributes?.FirstOrDefault (a => a is PresentViewInContainerAttribute);
                UINavigationController navController = RootViewController as UINavigationController;
                IContainerViewPresenter viewPresenter = navController?.TopViewController as IContainerViewPresenter;
                viewPresenter?.PresentViewInsideContainer(view, attribute?.ContainerID, _specifiedHint);
            } catch (Exception ex) {
                Insights.Report(ex);
            }
        }

        private void PresentNotification(INotificationView notification)
        {
            if (_currentNotification == null)
            {
                _currentNotification = notification;
                UIWindow window = UIApplication.SharedApplication.Windows.First();

                notification.View.Frame = new CoreGraphics.CGRect(0, -notification.Height, window.RootViewController.View.Bounds.Width, notification.Height);

                window.AddSubview(notification.View);

                //UIView.AnimateNotify(.25f, 0.0f, UIViewAnimationOptions.CurveEaseInOut, View.LayoutIfNeeded, null);

                UIView.AnimateNotify(.3f, 0.0f, UIViewAnimationOptions.CurveEaseInOut, () => {
                    notification.View.Frame = new CoreGraphics.CGRect(0, 0, window.RootViewController.View.Bounds.Width, notification.Height);

                }, result => {
                    
                });
            }
        }

        private void DismissCurrentNotification()
        {
            try {
                UIWindow window = UIApplication.SharedApplication.Windows.First();

                UIView.AnimateNotify(.3f, 0.0f, UIViewAnimationOptions.CurveEaseInOut, () => {
                    _currentNotification.View.Frame = new CoreGraphics.CGRect(0, -_currentNotification.Height, window.RootViewController.View.Bounds.Width, _currentNotification.Height);

                }, result => {
                    _currentNotification.View.RemoveFromSuperview();
                    _currentNotification = null;
                });
            } catch (Exception ex) {
                Insights.Report(ex);
            }
        }

        public override bool PresentModalViewController (UIViewController viewController, bool animated)
        {
            RootViewController.PresentViewController(viewController, true, null);
            return true;
        }

    }
}
