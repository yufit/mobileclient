using System;
using UIKit;

namespace YuFit.IOS.Presenters
{
	public class NavigationControllerDelegate : UINavigationControllerDelegate
	{
		/// <summary>
		/// The Detail Navigation Controller we're the delegate for
		/// </summary>
		//private readonly WeakReference _parent;

		/// <summary>
		/// The action to execute when a navigation animation is complete
		/// </summary>
		/// <value>The animation completed.</value>
		public Action AnimationCompleted;

		/// <summary>
		/// The custom transition (if any) applied to transitions
		/// </summary>
		/// <value>The animator.</value>
		public UIViewControllerAnimatedTransitioning Animator;

		/// <summary>
		/// Initializes a new instance of the <see cref="NavigationControllerDelegate"/> class.
		/// </summary>
		/// <param name="parent">Parent.</param>
		public NavigationControllerDelegate(UINavigationController parent)
		{
			//_parent = new WeakReference(parent);
		}

		/// <summary>
		///  Returns the custom navigation transition (if any)
		/// </summary>
		/// <returns>The animation controller for operation.</returns>
		/// <param name="navigationController">Navigation controller.</param>
		/// <param name="operation">Operation.</param>
		/// <param name = "fromViewController"></param>
		/// <param name="toViewController">To view controller.</param>
		public override IUIViewControllerAnimatedTransitioning GetAnimationControllerForOperation(
			UINavigationController navigationController, 
			UINavigationControllerOperation operation, 
			UIViewController fromViewController, 
			UIViewController toViewController
		)
		{
			return Animator;
		}


		/// <summary>
		/// Called when the nav controller is shown
		/// </summary>
		/// <param name="navigationController">Navigation controller.</param>
		/// <param name="viewController">View controller.</param>
		/// <param name = "animated"></param>
		public override void DidShowViewController(
			UINavigationController navigationController, 
			UIViewController viewController, 
			bool animated
		)
		{
			if (AnimationCompleted != null) {
				AnimationCompleted();
			}
		}
	}
}


