using UIKit;

namespace YuFit.IOS.Presenters.Transitions
{
	/// <summary>
	/// Custom animated transition.  Any of our transition must derived from this or there won't be any transition
	/// happening.
	/// </summary>
	public class CustomAnimatedTransition : UIViewControllerAnimatedTransitioning
	{
        public CustomAnimatedTransition (float duration)
        {
            Duration = duration;
        }

		public float Duration = 0.35f;
		public override double TransitionDuration(IUIViewControllerContextTransitioning transitionContext)
		{
			return Duration;
		}

		/// <summary>
		/// Animates the transition.  This is throwing on purposes.  We want the derived class to animate the
		/// transition.
		/// </summary>
		/// <param name="transitionContext">Transition context.</param>
		/// <exception cref="T:System.NotImplementedException"></exception>
		public override void AnimateTransition(IUIViewControllerContextTransitioning transitionContext)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
			throw new System.NotImplementedException();
		}

        public UIImage CaptureScreen()
        {
            YuFit.IOS.Application.AppDelegate appDelegate = (YuFit.IOS.Application.AppDelegate) UIApplication.SharedApplication.Delegate;
            if (UIScreen.MainScreen.RespondsToSelector(new ObjCRuntime.Selector("scale"))) {
                // Retina
                UIGraphics.BeginImageContextWithOptions(appDelegate.Window.Bounds.Size, true, UIScreen.MainScreen.Scale);
            } else {
                // Non retina
                UIGraphics.BeginImageContext(appDelegate.Window.Bounds.Size);
            }
            appDelegate.Window.DrawViewHierarchy(appDelegate.Window.Bounds, true);
            UIImage image = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();
            return image;
        }
	}
}

