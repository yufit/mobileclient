using UIKit;
using CoreGraphics;

namespace YuFit.IOS.Presenters.Transitions
{
	public class FadeTransition : CustomAnimatedTransition
	{
        public FadeTransition (float duration) : base(duration) {}

		public override void AnimateTransition(IUIViewControllerContextTransitioning transitionContext)
		{
			UIViewController toViewController = transitionContext.GetViewControllerForKey(UITransitionContext.ToViewControllerKey);
			UIViewController fromViewController = transitionContext.GetViewControllerForKey(UITransitionContext.FromViewControllerKey);
			transitionContext.ContainerView.AddSubview(toViewController.View);
			toViewController.View.Alpha = 0;

			UIView.Animate(TransitionDuration(transitionContext), () => {
				fromViewController.View.Alpha = 0;
				toViewController.View.Alpha = 1;
			}, () => {
				fromViewController.View.Transform = CGAffineTransform.MakeIdentity();
				transitionContext.CompleteTransition(!transitionContext.TransitionWasCancelled);
                fromViewController.View.Alpha = 1;
			});
		}
	}
}

