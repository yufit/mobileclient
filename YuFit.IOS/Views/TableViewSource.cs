using UIKit;
using Foundation;
using YuFit.IOS.CustomBindings;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Views
{
    public class TableViewSource<TCell> : MvxTableViewSource where TCell : UITableViewCell, new()
    {
        readonly string _key;
        public TableViewSource (UITableView tableView, string key) : base(tableView)
        {
            _key = key;
        }

        protected override UITableViewCell GetOrCreateCellFor (UITableView tableView, NSIndexPath indexPath, object item)
        {
            var cell = tableView.DequeueReusableCell(_key) ?? new TCell();

            cell.SetNeedsUpdateConstraints();
            cell.UpdateConstraintsIfNeeded();

            return cell;
        }
    }
}
