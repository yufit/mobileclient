using System;
using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Coc.MvvmCross.Plugins.Location;
using Coc.MvxAdvancedPresenter.Touch.Attributes;

using CoreGraphics;
using CoreLocation;
using MapKit;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Activity.Summary;
using YuFit.Core.ViewModels.Dashboard;

using YuFit.IOS.Controls;
using YuFit.IOS.Controls.Annotations;
using YuFit.IOS.Extensions;
using YuFit.IOS.Interfaces;
using YuFit.IOS.Presenters;
using YuFit.IOS.Themes;
using YuFit.IOS.Utilities;
using YuFit.IOS.Views.Dashboard;
using YuFit.IOS.Views.Activity.Summary;
using YuFit.IOS.Views.Event;
using YuFit.Core.ViewModels.Event;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Views.Dashboard
{
    /// <summary>
    /// Dashboard view.  We  disable  pitch and  rotation for  now as it causes
    /// issue with properly positioning the summary view.
    /// 
    /// I took of transition animation as it leaks the view.  A bug in the presenter
    /// stuff.
    /// </summary>
    [Presenter(typeof(PhonePresenter))]//, TransitionType = typeof(FadeTransition), Duration = 0.35f)]
    public class DashboardView : BaseViewController<DashboardViewModel>, IContainerViewPresenter
    {
        #region Data Members

        MapAnnotationManager _annotationManager;

        readonly AnnotationViewHelper<MapAnnotation> _annotationViewHelper = new AnnotationViewHelper<MapAnnotation>();

        readonly UIView _activityDetailContainerView = new UIView();
        NSLayoutConstraint _activityDetailContainerViewConstraint;

        UIBarButtonItem _homeButton;
        UIBarButtonItem _friendsButton;

        Toolbar _toolbar = new Toolbar();
        MKMapView _mapView = new MKMapView();
        MapBringHomeButton _bringBackButton = new MapBringHomeButton();
        UILongPressGestureRecognizer _longGesture;
        UITapGestureRecognizer _dismissSummaryTapGesture;

        #endregion

        #region Properties

        #endregion

        #region Construction/Destruction

        #endregion

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            SetupMapView();
            SetupToolbar();

            InitNavBarButtons();

            _mapView.ShowsUserLocation = true;

            _longGesture = new UILongPressGestureRecognizer(HandleAction);
            _longGesture.MinimumPressDuration = 0.4f;
            _mapView.PitchEnabled = false;
            _mapView.RotateEnabled = false;
            _mapView.GetViewForAnnotation = _annotationViewHelper.GetViewForAnnotation;

            var set = this.CreateBindingSet<DashboardView, DashboardViewModel>();
            set.Bind(_mapView).For("Region").To(vm => vm.CurrentRegion).WithConversion("RegionToMKCoordinate");
            set.Bind(_annotationManager).For(my => my.ItemsSource).To(vm => vm.FitActivities);
            set.Bind(_homeButton).To(vm => vm.ShowHomeMenuCommand.Command);
            set.Bind(_friendsButton).To(vm => vm.DisplayFriendsCommand.Command);
            set.Bind(_activityDetailContainerView.SwipeLeft()).To(vm => vm.DisplayPreviousActivitySummaryCommand.Command);
            set.Bind(_activityDetailContainerView.SwipeRight()).To(vm => vm.DisplayNextActivitySummaryCommand.Command);
            set.Bind(_toolbar.ResetLocationButton.Tap()).To(vm => vm.BringBackToWhereIAmCommand.Command);
            set.Bind(_toolbar.FilterButton.Tap()).To(vm => vm.DisplayFiltersCommand.Command);
            set.Bind(_toolbar.NotificationsButton.Tap()).To(vm => vm.DisplayNotificationsCommand.Command);
            set.Bind(_toolbar.InvitationsButton.Tap()).To(vm => vm.DisplayInvitesCommand.Command);
            set.Bind(_toolbar.CreateActivityButton).To(vm => vm.CreateFitActivityCommand.Command);
            set.Bind(_toolbar.InvitationsBadge).For(b => b.BadgeText).To(vm => vm.NumberOfNewInvitations).WithConversion("NumberToBadgeText");
            set.Bind(_toolbar.NotificationsBadge).For(b => b.BadgeText).To(vm => vm.NumberOfNewNotifications).WithConversion("NumberToBadgeText");
            set.Apply();
//            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage);

            _activityDetailContainerView.TranslatesAutoresizingMaskIntoConstraints = false;
            _activityDetailContainerView.BackgroundColor = UIColor.Clear;
            _dismissSummaryTapGesture = new UITapGestureRecognizer(DismissSummaryOnTap);

            Add(_activityDetailContainerView);

            View.AddConstraints( 
                _activityDetailContainerView.CenterX().EqualTo().CenterXOf(View),
                _activityDetailContainerView.Width().EqualTo().WidthOf(View),
                _activityDetailContainerView.Height().EqualTo().HeightOf(View)
            );

            _activityDetailContainerViewConstraint = _activityDetailContainerView.Bottom().EqualTo().TopOf(View).ToLayoutConstraints().FirstOrDefault();
            View.AddConstraint(_activityDetailContainerViewConstraint);
        }

        public override void ViewDidDisappear (bool animated)
        {
            _mapView.DidAddAnnotationViews -= HandleDidAddAnnotationViews;
            _mapView.DidSelectAnnotationView -= HandleDidSelectAnnotationView;
            _mapView.RemoveGestureRecognizer(_longGesture);
            _activityDetailContainerView.RemoveGestureRecognizer(_dismissSummaryTapGesture);
            base.ViewDidDisappear(animated);
        }

        public override void ViewDidAppear (bool animated)
        {
            var selected = _mapView.SelectedAnnotations;
            if (selected != null) {
                selected.ForEach(ann => _mapView.DeselectAnnotation(ann, false));
            }

            _mapView.DidAddAnnotationViews += HandleDidAddAnnotationViews;
            _mapView.DidSelectAnnotationView += HandleDidSelectAnnotationView;
            _mapView.AddGestureRecognizer(_longGesture);
            _activityDetailContainerView.AddGestureRecognizer(_dismissSummaryTapGesture);
            base.ViewDidAppear(animated);
        }

        void DismissSummaryOnTap(UITapGestureRecognizer touch)
        {
            if (_currentActivityDetailView is ActivitySummaryView) {
                ((ActivitySummaryView) _currentActivityDetailView).Dismiss();
                ViewModel.DismissActivitySummaryCommand.Command.Execute(this);
            }
        }

        void HandleAction (UILongPressGestureRecognizer touch)
        {
            if (touch.State != UIGestureRecognizerState.Began) { return; }

            CGPoint userTouchLocation = touch.LocationInView(_mapView);
            CLLocationCoordinate2D mapPoint = _mapView.ConvertPoint(userTouchLocation, _mapView);

            Console.WriteLine("{0}, {1}", mapPoint.Latitude, mapPoint.Longitude);
            ViewModel.CreateFitActivityAtCommand.Command.Execute(new CocLocation { Coordinates = mapPoint.ToMvxCoordinates() });
        }

        void SetupMapView ()
        {
            View.AddSubview(_mapView);
            _mapView.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddConstraints(
                _mapView.WithSameWidth(View),
                _mapView.WithSameHeight(View),
                _mapView.WithSameCenterX(View),
                _mapView.WithSameCenterY(View)
            );

            _annotationManager = new MapAnnotationManager(_mapView);
        }


        void SetupToolbar ()
        {
            
            View.AddSubview(_toolbar);

            _toolbar.TranslatesAutoresizingMaskIntoConstraints = false;
            _toolbar.AddConstraints(_toolbar.Height().EqualTo(45));

            View.AddConstraints(
                _toolbar.WithSameCenterX(View),
                _toolbar.Bottom().EqualTo().BottomOf(View),
                _toolbar.WithSameWidth(View)
            );

            _toolbar.BackgroundColor = YuFitStyleKit.Black;
        }

        void HandleDidSelectAnnotationView (object sender, MKAnnotationViewEventArgs e)
        {
            Console.WriteLine("selected");
            MapAnnotation annotation = e.View.Annotation as MapAnnotation;
            if (annotation == null) { return; }

            ViewModel.DisplayActivitySummaryCommand.Command.Execute(annotation.DataContext);
        }

        void HandleDidAddAnnotationViews (object sender, MKMapViewAnnotationEventArgs e)
        {
            _annotationViewHelper.DidAddAnnotationViews(_mapView, e.Views);
        }


        void InitNavBarButtons()
        {
            UIImage image; 

            UIGraphics.BeginImageContextWithOptions(new CGSize(29f, 29f), false, 0);
            YuFitStyleKit.DrawProfile2Icon (new CGRect(0, 0, 29F, 29F), Themes.Dashboard.HomeIconColor);

            image = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            _homeButton = new UIBarButtonItem(image, UIBarButtonItemStyle.Plain, null);
            _homeButton.TintColor = Themes.Dashboard.HomeIconColor;
            NavigationItem.RightBarButtonItem = _homeButton;

            UIGraphics.BeginImageContextWithOptions(new CGSize(27f, 27f), false, 0);
            YuFitStyleKit.DrawAssociationsIcon(new CGRect(0, 0, 27F, 27F), Themes.Dashboard.HomeIconColor);
            image = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            _friendsButton = new UIBarButtonItem(image, UIBarButtonItemStyle.Plain, null);
            _friendsButton.TintColor = Themes.Dashboard.HomeIconColor;
            NavigationItem.LeftBarButtonItem = _friendsButton;
        }

        #region IContainerViewPresenter implementation

        UIViewController _currentActivityDetailView;

        public void PresentViewInsideContainer (IMvxIosView viewController, string containerID, MvxPresentationHint hint)
        {
            if (viewController is ActivitySummaryView)
            {
                UIViewController vc = (UIViewController) viewController;

                if (_currentActivityDetailView == null) {

                    CLLocationCoordinate2D center2 = _mapView.CenterCoordinate;
                    center2.Latitude += _mapView.Region.Span.LatitudeDelta * -0.45;
                    CGPoint point = _mapView.ConvertCoordinate(center2, _mapView);


                    CLLocationCoordinate2D center = new CLLocationCoordinate2D{
                        Latitude = ViewModel.SelectedPinVM.Location.Latitude,
                        Longitude = ViewModel.SelectedPinVM.Location.Longitude
                    };
                    center.Latitude -= _mapView.Region.Span.LatitudeDelta * -0.45;
                    _mapView.SetCenterCoordinate(center, false);

                    _activityDetailContainerView.Add(vc.View);
                    AddChildViewController(vc);

                    nfloat height = point.Y;
                    vc.View.Frame = new CGRect {
                        Width = _mapView.Bounds.Width, Height = height, 
                        X = 0, Y = 0// - height
                    };

                    View.RemoveConstraint(_activityDetailContainerViewConstraint); 
                    _activityDetailContainerViewConstraint = _activityDetailContainerView.Bottom().EqualTo().BottomOf(View).ToLayoutConstraints().FirstOrDefault();
                    View.AddConstraint(_activityDetailContainerViewConstraint);
                    View.SetNeedsUpdateConstraints();

                    UIView.AnimateNotify(0.15f, 0.0f, UIViewAnimationOptions.CurveEaseInOut, () => {
                        _toolbar.Layer.Opacity = 0.0f;
                        View.LayoutIfNeeded();
                        NavigationItem.LeftBarButtonItem = null;
                        _bringBackButton.Layer.Opacity = 0.0f;
                    }, finished => {
                        _currentActivityDetailView = vc;
                    });
                } else {
                    CLLocationCoordinate2D center2 = _mapView.CenterCoordinate;
                    center2.Latitude += _mapView.Region.Span.LatitudeDelta * -0.45;

                    CLLocationCoordinate2D center = new CLLocationCoordinate2D{
                        Latitude = ViewModel.SelectedPinVM.Location.Latitude,
                        Longitude = ViewModel.SelectedPinVM.Location.Longitude
                    };
                    center.Latitude -= _mapView.Region.Span.LatitudeDelta * -0.45;
                    _mapView.SetCenterCoordinate(center, true);

                    if (hint is FlipFromLeftHint)
                    {
                        CGRect contentRect = _currentActivityDetailView.View.Frame;

                        CGRect outEndRect = contentRect;
                        outEndRect.X = 0 - contentRect.Width;

                        CGRect inStartRect = contentRect;
                        inStartRect.X = contentRect.Width;

                        _activityDetailContainerView.Add(vc.View);
                        AddChildViewController(vc);
                        vc.View.Frame = inStartRect;
                        UIView.AnimateNotify(0.15f, 0.0f, UIViewAnimationOptions.CurveEaseInOut, () => {
                            _toolbar.Layer.Opacity = 0.0f;
                            _currentActivityDetailView.View.Frame = outEndRect;
                            vc.View.Frame = contentRect;
                        }, finished => {
                            _currentActivityDetailView.View.RemoveFromSuperview();
                            _currentActivityDetailView.RemoveFromParentViewController();
                            //_currentActivityDetailView.Dispose();
                            _currentActivityDetailView = vc;
                        });
                    } else {
                        CGRect contentRect = _currentActivityDetailView.View.Frame;

                        CGRect outEndRect = contentRect;
                        outEndRect.X = contentRect.Width;

                        CGRect inStartRect = contentRect;
                        inStartRect.X = 0 - contentRect.Width;

                        _activityDetailContainerView.Add(vc.View);
                        AddChildViewController(vc);
                        vc.View.Frame = inStartRect;
                        UIView.AnimateNotify(0.15f, 0.0f, UIViewAnimationOptions.CurveEaseInOut, () => {
                            _toolbar.Layer.Opacity = 0.0f;
                            _currentActivityDetailView.View.Frame = outEndRect;
                            vc.View.Frame = contentRect;
                        }, finished => {
                            _currentActivityDetailView.View.RemoveFromSuperview();
                            _currentActivityDetailView.RemoveFromParentViewController();
                            //_currentActivityDetailView.Dispose();
                            _currentActivityDetailView = vc;
                        });
                    }
                }
            }

        }

        public void DismissViewAssociatedToViewModel(IMvxViewModel viewModel)
        {
            if (viewModel is ActivitySummaryViewModel) { // && 
                View.RemoveConstraint(_activityDetailContainerViewConstraint); 
                _activityDetailContainerViewConstraint = _activityDetailContainerView.Bottom().EqualTo().TopOf(View).ToLayoutConstraints().FirstOrDefault();
                View.AddConstraint(_activityDetailContainerViewConstraint);
                View.SetNeedsUpdateConstraints();

                UIView.AnimateNotify(0.15f, 0.0f, UIViewAnimationOptions.CurveEaseInOut, View.LayoutIfNeeded, finished => {
                    _toolbar.Layer.Opacity = 1.0f;
                    _currentActivityDetailView.View.RemoveFromSuperview();
                    _currentActivityDetailView.RemoveFromParentViewController();
                    _bringBackButton.Layer.Opacity = 1.0f;

                    NavigationItem.RightBarButtonItem = _homeButton;
                    NavigationItem.LeftBarButtonItem = _friendsButton;

                    CLLocationCoordinate2D center = new CLLocationCoordinate2D{
                        Latitude = ViewModel.SelectedPinVM.Location.Latitude,
                        Longitude = ViewModel.SelectedPinVM.Location.Longitude
                    };
                    _mapView.SetCenterCoordinate(center, true);

//                    _currentActivityDetailView.Dispose();
                    _currentActivityDetailView = null;

                    var selected = _mapView.SelectedAnnotations;
                    if (selected != null) {
                        selected.ForEach(ann => _mapView.DeselectAnnotation(ann, false));
                    }
                });
            }
        }

        public bool IsPresenting(IMvxViewModel viewModel)
        {
            return ChildViewControllers.Any(vc => ((IMvxIosView) vc).ViewModel == viewModel);
        }

        public bool IsPresenting(Type viewModelType)
        {
            return ChildViewControllers.Any(vc => ((IMvxIosView) vc).ViewModel.GetType() == viewModelType);
        }


        #endregion

//        protected override void FreeManagedResources ()
//        {
//            base.FreeManagedResources();
//            _mapView.Delegate = null;
//            _mapView = null;
//        }
    }
}

