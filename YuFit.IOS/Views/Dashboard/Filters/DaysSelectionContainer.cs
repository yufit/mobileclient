using System;
using System.Collections.Generic;

using UIKit;

using YuFit.IOS.Themes;
using Cirrious.FluentLayouts.Touch;
using Foundation;
using CoreGraphics;
using System.Linq;

namespace YuFit.IOS.Views.Dashboard.Filters
{
    class DayLabel : UILabel
    {
        public DayOfWeek DayOfWeek { get; private set; }

        bool _isEnabled;
        public bool IsEnabled {
            get { return _isEnabled; }
            set { _isEnabled = value; UpdateColor(); }
        }

        public DayLabel (DayOfWeek dayOfWeek)
        {
            DayOfWeek = dayOfWeek;
            TextAlignment = UITextAlignment.Center;
            Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 18);
            AdjustsFontSizeToFitWidth = true;
            UpdateColor();
        }

        void UpdateColor ()
        {
            TextColor = _isEnabled ? YuFitStyleKit.Yellow : YuFitStyleKit.White;
            Font = UIFont.FromName(_isEnabled ? "HelveticaNeueLTStd-BdCn" : "HelveticaNeueLTStd-LtCn", 18);
        }
    }

	sealed class DaysSelectionContainer : UIView
	{
        public readonly UILabel TitleLabel = new UILabel();
        public readonly List<DayLabel> DaysLabel = new List<DayLabel>();

        readonly List<UIView> _spacers = new List<UIView>();
        readonly UIView _daysContainer = new UIView();

        public DaysSelectionContainer ()
        {
            SetupTitle();
            SetupDaysContainer();

            this.AddConstraints(this.Bottom().EqualTo().BottomOf(_daysContainer));

        }

        void SetupTitle ()
        {
            TitleLabel.TextAlignment = UITextAlignment.Left;
            TitleLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            TitleLabel.TextColor = YuFitStyleKit.White;
            TitleLabel.AdjustsFontSizeToFitWidth = true;
            TitleLabel.Text = "day of the week";

            TitleLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            AddSubview(TitleLabel);

            this.AddConstraints(
                TitleLabel.Left().EqualTo().LeftOf(this),
                TitleLabel.Right().EqualTo().RightOf(this),
                TitleLabel.Bottom().EqualTo().CenterYOf(this)
            );
        }

        void SetupDaysContainer ()
        {
            NSDateFormatter dateFormatter = new NSDateFormatter();
            var symbols = dateFormatter.VeryShortWeekdaySymbols;
            for (int i=0; i<symbols.Length; ++i) {
                DayOfWeek[] arr = (DayOfWeek[]) Enum.GetValues(typeof(DayOfWeek));
                    
                var label = new DayLabel(arr[i]);

                label.TextAlignment = UITextAlignment.Center;
                label.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 18);
                label.TextColor = YuFitStyleKit.White;
                label.AdjustsFontSizeToFitWidth = true;
                label.Text = symbols[i];
                label.TranslatesAutoresizingMaskIntoConstraints = false;

                label.SetContentCompressionResistancePriority((float) UILayoutPriority.Required, UILayoutConstraintAxis.Horizontal);
                label.SetContentHuggingPriority((float) UILayoutPriority.Required, UILayoutConstraintAxis.Horizontal);

                DaysLabel.Add(label);
            }

            for (int i=0; i<DaysLabel.Count - 1; ++i) {
                var spacer = new UIView();
                spacer.TranslatesAutoresizingMaskIntoConstraints = false;
                spacer.SetContentCompressionResistancePriority((float) UILayoutPriority.DefaultLow, UILayoutConstraintAxis.Horizontal);
                spacer.SetContentHuggingPriority((float) UILayoutPriority.DefaultLow, UILayoutConstraintAxis.Horizontal);
                _spacers.Add(spacer);
            }

            _daysContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            AddSubview(_daysContainer);

            _daysContainer.AddSubviews(DaysLabel.ToArray());
            _daysContainer.AddSubviews(_spacers.ToArray());

            this.AddConstraints(
                _daysContainer.Left().EqualTo().LeftOf(this),
                _daysContainer.Right().EqualTo().RightOf(this),
                _daysContainer.Top().EqualTo().CenterYOf(this)
            );

            _daysContainer.AddConstraints(
                DaysLabel[0].Left().EqualTo().LeftOf(_daysContainer),
                DaysLabel[0].Top().EqualTo().TopOf(_daysContainer),
                _daysContainer.Bottom().EqualTo().BottomOf(DaysLabel[0])
            );

            for (int i=0; i<_spacers.Count; ++i) {
                _daysContainer.AddConstraints(
                    _spacers[i].Left().EqualTo().RightOf(DaysLabel[i]),
                    _spacers[i].Right().EqualTo().LeftOf(DaysLabel[i+1]),
                    _spacers[i].Top().EqualTo().TopOf(_daysContainer),
                    DaysLabel[i+1].Top().EqualTo().TopOf(_daysContainer)
                );

                if (i>0) {
                    _daysContainer.AddConstraints(
                        _spacers[i].Width().EqualTo().WidthOf(_spacers[0])
                    );
                }
            }

            _daysContainer.AddConstraints(
                DaysLabel[DaysLabel.Count-1].Right().EqualTo().RightOf(_daysContainer)
            );

            _daysContainer.AddGestureRecognizer(new UITapGestureRecognizer(() => {
                CGPoint pt = _daysContainer.GestureRecognizers[0].LocationOfTouch(0, _daysContainer);
                UIView tappedLabelView = DaysLabel.FirstOrDefault(label => {
                    CGPoint pointInSubview = label.ConvertPointFromView(pt, _daysContainer);
                    return label.Bounds.Contains(pointInSubview);
                });

                if (tappedLabelView != null) {
                    ((DayLabel) tappedLabelView).IsEnabled = !((DayLabel) tappedLabelView).IsEnabled;
                }
            }));
        }
	}

}

