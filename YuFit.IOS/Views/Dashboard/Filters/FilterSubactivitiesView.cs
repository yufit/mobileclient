﻿using System;
using YuFit.Core.ViewModels.Dashboard;
using UIKit;
using YuFit.IOS.Controls;
using YuFit.Core.Extensions;
using Cirrious.FluentLayouts.Touch;
using System.Linq;
using MvvmCross.Binding.BindingContext;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Dashboard.Filters
{
    public class FilterSubactivitiesView : BaseViewController<FilterSubactivitiesViewModel>
    {
        private readonly UILabel         _tableLabel        = new UILabel();
        private readonly SeparatorLine   _tableTopLine      = new SeparatorLine();
        private readonly UIView          _tableContainer    = new UIView();
        private readonly SeparatorLine   _tableBottomLine   = new SeparatorLine();
        private readonly SubactivitiesTableView _tableView         = new SubactivitiesTableView();
        private ActivityColors _colors;


        public FilterSubactivitiesView() 
        {          
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            _colors = YuFitStyleKitExtension.ActivityBackgroundColors(ViewModel.Activity);

            AddSubviews();
            SetupBindings();
        }

        void AddSubviews ()
        {
            UIView[] subviews = { _tableTopLine, _tableContainer, _tableBottomLine };

            View.AddSubviews(subviews);

            subviews.ForEach(v => 
            {
                v.TranslatesAutoresizingMaskIntoConstraints = false;

                View.AddConstraints(
                    v.WithSameCenterX(View),
                    v.Width().EqualTo().WidthOf(View).WithMultiplier(0.9f)
                );
            });

            subviews.Where(v => v is SeparatorLine).ForEach(v => 
            {
                v.AddConstraints(v.Height().EqualTo(1.5f));
                v.BackgroundColor = _colors.Base;
            });

            subviews.Where(v => v is UILabel).ForEach(v => {
                (v as UILabel).Font = Themes.CrudFitEvent.LabelTextFont;
                (v as UILabel).TextColor = Themes.CrudFitEvent.LabelTextColor;
            });

            View.AddConstraints(
                _tableTopLine.Top().EqualTo(22.0f).TopOf(View),
                _tableContainer.Top().EqualTo(5.0f).BottomOf(_tableTopLine),
                _tableContainer.Bottom().EqualTo(-5.0f).TopOf(_tableBottomLine),
                _tableBottomLine.Bottom().EqualTo(-22.0f).BottomOf(View)
            );


            _tableView.BindingContext = BindingContext;
            AddChildViewController(_tableView);
            _tableContainer.AddSubview(_tableView.View);

            _tableView.View.TranslatesAutoresizingMaskIntoConstraints = false;
            _tableContainer.AddConstraints(
                _tableView.View.Top().EqualTo().TopOf(_tableContainer),
                _tableView.View.Left().EqualTo().LeftOf(_tableContainer),
                _tableView.View.Bottom().EqualTo().BottomOf(_tableContainer),
                _tableView.View.Right().EqualTo().RightOf(_tableContainer)
            );
        }


        void SetupBindings ()
        {
            var set = this.CreateBindingSet<FilterSubactivitiesView, FilterSubactivitiesViewModel>();
            set.Bind(_tableLabel).To(vm => vm.TableLabelTitle);
            set.Apply();
            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );
        }
    }
}

