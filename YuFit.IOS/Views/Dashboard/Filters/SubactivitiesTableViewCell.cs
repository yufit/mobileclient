﻿using System;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using Foundation;
using UIKit;
using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.CreateActivity;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using YuFit.Core.ViewModels.Dashboard;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Views.Dashboard.Filters
{
    public class SubactivitiesTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("SubactivitiesTableViewCell");

        readonly UILabel _nameLabel = new UILabel();
        readonly CheckBox _checkbox = new CheckBox();
        private ActivityColors _colors;

        private SubactivityViewModel Viewmodel { get { return ((SubactivityViewModel) DataContext); } }

        public bool IsSelected
        {
            get { return ((SubactivityViewModel)DataContext).IsSelected; }
            set {
                UIView.Animate(0.25f, () => {
                    _nameLabel.TextColor = value ? _colors.Base : _colors.Dark;
                    _checkbox.IsSelected = value;
                });
            }
        }

        private string _sport;
        public string Sport {
            get { return _sport; }
            set { _sport = value; UpdateContent(); }
        }

        public SubactivitiesTableViewCell ()
        {
            BackgroundColor = UIColor.Clear;
            SelectionStyle = UITableViewCellSelectionStyle.None;

            ContentView.AddSubviews(new UIView[] {_checkbox, _nameLabel });

            SetupConstraints();

            this.DelayBind(() => {
                _colors = YuFitStyleKitExtension.ActivityBackgroundColors(Viewmodel.ParentActivity);
                _checkbox.SelectedColor = _colors.Base;
                _checkbox.UnselectedColor = _colors.Dark;

                _nameLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 18);
                _nameLabel.TextColor = _colors.Dark;

                var set = this.CreateBindingSet<SubactivitiesTableViewCell, SubactivityViewModel>();
                //set.Bind(_pictureView.ImageViewLoader).To(friend => friend.AvatarUrl);
                set.Bind(_nameLabel).To(sub => sub.DisplayName);
                set.Bind(this).For(me => me.IsSelected).To(friend => friend.IsSelected);
                set.Apply();
            });
        }

        void SetupConstraints ()
        {
            ContentView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            _checkbox.AddConstraints(
                _checkbox.Height().EqualTo(13.0f),
                _checkbox.Width().EqualTo(19.0f)
            );

            ContentView.AddConstraints(
                _checkbox.WithSameCenterY(ContentView),
                _checkbox.Left().EqualTo(5.0f).LeftOf(ContentView),

                _nameLabel.Left().EqualTo(10.0f).RightOf(_checkbox),
                _nameLabel.Right().EqualTo(-36.0f).RightOf(ContentView),
                _nameLabel.WithSameCenterY(ContentView)
            );
        }

        void UpdateContent ()
        {
            ActivityColors colors = YuFitStyleKitExtension.ActivityBackgroundColors(_sport);
            _nameLabel.TextColor = colors.Light;

        }

    }
}

