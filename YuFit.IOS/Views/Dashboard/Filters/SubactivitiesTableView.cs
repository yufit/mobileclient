﻿using System;
using YuFit.Core.ViewModels.Dashboard;
using UIKit;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Views.Dashboard.Filters
{
    public class SubactivitiesTableView : MvxTableViewController
    {
        protected new FilterSubactivitiesViewModel ViewModel { get { return (FilterSubactivitiesViewModel) base.ViewModel; } }

        public SubactivitiesTableView ()
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            TableView.BackgroundColor = UIColor.Clear;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.RowHeight = 40;

            var source = new TableViewSource<SubactivitiesTableViewCell>(TableView, SubactivitiesTableViewCell.Key);
            TableView.Source = source;

            var set = this.CreateBindingSet<SubactivitiesTableView, FilterSubactivitiesViewModel>();
            set.Bind(source).To(vm => vm.Subactivities);
            set.Bind(source).For(s => s.SelectionChangedCommand).To(vm => vm.SubactivitySelectedCommand.Command);
            set.Apply();
        }
    }
}

