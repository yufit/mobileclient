﻿
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Fcaico.Controls.ArrowSlider;

using UIKit;

using YuFit.Core.ViewModels.Dashboard;

namespace YuFit.IOS.Views.Dashboard.Filters
{
    public class MapFiltersView : BaseViewController<MapFiltersViewModel>
    {
        readonly UILabel _activityFilterLabel = new UILabel();
        readonly FilterActivityCollectionView _activityCollectionView = new FilterActivityCollectionView();
        readonly UILabel _timeFilterLabel = new UILabel();
        readonly ArrowSliderView _timeFilterSlider = new ArrowSliderView(); 

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            SetupViews();
            SetupConstraints();
            SetupBindings();
        }

        void SetupViews ()
        {
            View.BackgroundColor = Themes.Dashboard.FilterView.ViewBackgroundColor;

            _activityCollectionView.DataContext = ViewModel;
            AddChildViewController(_activityCollectionView);

            _activityFilterLabel.Lines = 0;
            _activityFilterLabel.TextAlignment = UITextAlignment.Center;

            View.Add(_activityFilterLabel);
            View.Add(_activityCollectionView.View);
            View.Add(_timeFilterLabel);
            View.Add(_timeFilterSlider);

            _timeFilterLabel.Font = Themes.Dashboard.FilterView.LabelTextFont;
            _timeFilterLabel.TextColor = Themes.Dashboard.FilterView.LabelTextColor;

            _activityFilterLabel.Font = Themes.Dashboard.FilterView.LabelTextFont;
            _activityFilterLabel.TextColor = Themes.Dashboard.FilterView.LabelTextColor;

            _timeFilterSlider.Values = ViewModel.WithinDaysOptionsList;
            _timeFilterSlider.Font = Themes.Dashboard.FilterView.SliderTextFont;
            _timeFilterSlider.FontBaselineOffset = Themes.Dashboard.FilterView.SliderFontOffset;
            _timeFilterSlider.ArrowColor = Themes.Dashboard.FilterView.SliderColor;
            _timeFilterSlider.BackgroundColor = UIColor.Clear;
            _timeFilterLabel.AdjustsFontSizeToFitWidth = true;
        }

        void SetupConstraints ()
        {
            _activityFilterLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _activityCollectionView.View.TranslatesAutoresizingMaskIntoConstraints = false;
            _timeFilterLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _timeFilterSlider.TranslatesAutoresizingMaskIntoConstraints = false;

            _activityFilterLabel.AddConstraints(
                _activityFilterLabel.Height().EqualTo(40)
            );

            View.AddConstraints(
                _timeFilterLabel.Top().EqualTo(5f).TopOf(View),
                _timeFilterLabel.Left().EqualTo(Spacing).LeftOf(View),
                _timeFilterLabel.Right().EqualTo(-Spacing).RightOf(View),

                _timeFilterSlider.Top().EqualTo().BottomOf(_timeFilterLabel),
//                _timeFilterSlider.WithSameCenterX(View),
//                _timeFilterSlider.WithSameWidth(_activityCollectionView.View),
                _timeFilterSlider.Height().EqualTo(30f),
                _timeFilterSlider.Left().EqualTo(Spacing).LeftOf(View),
                _timeFilterSlider.Right().EqualTo(-Spacing).RightOf(View),

                _activityFilterLabel.Left().EqualTo(Spacing).LeftOf(View),
                _activityFilterLabel.Top().EqualTo(5f).BottomOf(_timeFilterSlider),
                _activityFilterLabel.Right().EqualTo(-Spacing).RightOf(View),

                _activityCollectionView.View.Left().EqualTo(Spacing).LeftOf(View),
                _activityCollectionView.View.Right().EqualTo(-Spacing).RightOf(View),
                _activityCollectionView.View.Top().EqualTo().BottomOf(_activityFilterLabel),
                _activityCollectionView.View.Bottom().EqualTo().BottomOf(View)
            );
        }


        void SetupBindings ()
        {
            var set = this.CreateBindingSet<MapFiltersView, MapFiltersViewModel>();
            set.Bind(_activityFilterLabel).To(vm => vm.ActivityFilterLabel);
            set.Bind(_timeFilterSlider).For(slider => slider.CurrentValue).To(vm => vm.WithinDays);
            set.Bind(_timeFilterLabel).To(vm => vm.WithinDaysLabel);
            set.Apply();
        }
    }
}

