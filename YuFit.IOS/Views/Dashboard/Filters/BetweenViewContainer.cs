
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using YuFit.Core.ViewModels.Dashboard;
using UIKit;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Dashboard.Filters
{
    class BetweenViewContainer : UIView
    {
        public readonly UILabel TitleLabel = new UILabel();
        public readonly UILabel StartLabel = new UILabel();
        public readonly UILabel StopLabel  = new UILabel();

        public BetweenViewContainer ()
        {
            TitleLabel.TextAlignment = UITextAlignment.Left;
            TitleLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            TitleLabel.TextColor = YuFitStyleKit.White;
            TitleLabel.AdjustsFontSizeToFitWidth = true;
            TitleLabel.Text = "between";
            TitleLabel.TranslatesAutoresizingMaskIntoConstraints = false;

            StartLabel.TextAlignment = UITextAlignment.Left;
            StartLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            StartLabel.TextColor = YuFitStyleKit.White;
            StartLabel.AdjustsFontSizeToFitWidth = true;
            StartLabel.Text = "7:00AM";
            StartLabel.TranslatesAutoresizingMaskIntoConstraints = false;

            StopLabel.TextAlignment = UITextAlignment.Right;
            StopLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            StopLabel.TextColor = YuFitStyleKit.White;
            StopLabel.AdjustsFontSizeToFitWidth = true;
            StopLabel.Text = "11:00PM";
            StopLabel.TranslatesAutoresizingMaskIntoConstraints = false;

            AddSubviews(new UIView[] { TitleLabel, StartLabel, StopLabel });

            this.AddConstraints(
                TitleLabel.Left().EqualTo().LeftOf(this),
                TitleLabel.Right().EqualTo().RightOf(this),
                TitleLabel.Bottom().EqualTo().CenterYOf(this),

                StartLabel.Left().EqualTo().LeftOf(this),
                StartLabel.Top().EqualTo().CenterYOf(this),
                StartLabel.Width().EqualTo().WidthOf(this).WithMultiplier(0.5f),

                StopLabel.Left().EqualTo().RightOf(StartLabel),
                StopLabel.Top().EqualTo().CenterYOf(this),
                StopLabel.Width().EqualTo().WidthOf(this).WithMultiplier(0.5f),

                this.Bottom().EqualTo().BottomOf(StartLabel)
            );


        }
    }

}

