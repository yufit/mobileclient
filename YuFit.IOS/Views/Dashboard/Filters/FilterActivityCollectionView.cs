﻿using System;
using YuFit.IOS.Controls.ActivityCollectionView;
using YuFit.Core.ViewModels.Dashboard;
using MvvmCross.Binding.BindingContext;
using Foundation;
using UIKit;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Dashboard.Filters
{
    public class FilterActivityTypeCollectionViewCell : ActivityTypeCollectionViewCell
    {
        public FilterActivityTypeCollectionViewCell (IntPtr handle) : base(handle)
        {
            
        }

        protected override void UpdateColors ()
        {
            if (IsEnabled && IsSelected) {
                ActivityColors colors = YuFitStyleKitExtension.ActivityBackgroundColors(ActivityType);
                _activityTypeItemView.TransparentBackground = false;
                _activityTypeItemView.DiamondColor = colors.Base;
                _activityTypeItemView.StrokeWidth = 3.0f;
                _activityTypeItemView.StrokeColor = colors.Dark;
                _activityTypeItemView.ActivityFillColor = YuFitStyleKit.White;
                _activityTypeItemView.SetNeedsDisplay();
            } else {
                _activityTypeItemView.DiamondColor = YuFitStyleKit.Black;
                _activityTypeItemView.TransparentBackground = true;
                _activityTypeItemView.StrokeWidth = 1.0f;
                _activityTypeItemView.StrokeColor = YuFitStyleKit.ActivityDisabledColor;
                _activityTypeItemView.ActivityFillColor = YuFitStyleKit.ActivityDisabledColor;
                _activityTypeItemView.SetNeedsDisplay();
            }
        }
    }
    public class FilterActivityCollectionView : ActivityCollectionViewController<MapFiltersViewModel, FilterActivityTypeCollectionViewCell>
    {

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            var set = this.CreateBindingSet<FilterActivityCollectionView, MapFiltersViewModel>();
            set.Bind(Source).To(vm => vm.Activities);
            set.Bind(Source).For(s => s.SelectionChangedCommand).To(vm => vm.ShowSubActivitiesFilterCommand.Command);
            set.Apply();

            Source.SelectedItemChanged += (sender, e) => {
                NSIndexPath[] index = CollectionView.GetIndexPathsForSelectedItems();
                CollectionView.SelectItem(index[0], true, UICollectionViewScrollPosition.CenteredHorizontally);


            };

            CollectionView.ReloadData();
        }

         
    }
}

