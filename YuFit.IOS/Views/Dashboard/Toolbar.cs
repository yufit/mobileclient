﻿using System;
using UIKit;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using YuFit.IOS.Views.CreateActivity;
using CoreGraphics;
using YuFit.Core.Extensions;
using Cirrious.FluentLayouts.Touch;

namespace YuFit.IOS.Views.Dashboard
{
    public class Toolbar : UIView
    {
        public CreateFitActivityButton CreateActivityButton { get; private set; }
        public IconView ResetLocationButton { get; private set; }
        public IconView FilterButton { get; private set; }
        public IconView NotificationsButton { get; private set; }
        public IconView InvitationsButton { get; private set; }

        public BadgeView InvitationsBadge { get; private set; }
        public BadgeView NotificationsBadge { get; private set; }

        public Toolbar ()
        {
            SetupSubviews();
            SetupConstraints();
        }

        private void SetupSubviews()
        {
            ResetLocationButton = new IconView { DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawLocationIcon"), Padding = 10.5f };
            FilterButton = new IconView { DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawFiltersIcon"), Padding = 8f };
            NotificationsButton = new IconView { DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawNotificationsIcon"), Padding = 10f };
            InvitationsButton = new IconView { DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawActivitiesIcon"), Padding = 12f };
            InvitationsBadge = new BadgeView(InvitationsButton, BadgeView.Alignment.TopRight);
            NotificationsBadge = new BadgeView(NotificationsButton, BadgeView.Alignment.TopRight);

            InvitationsBadge.BadgeTextFont = UIFont.SystemFontOfSize(16f);
            InvitationsBadge.BadgePositionAdjustment = new CGPoint(-5f, 5f);

            NotificationsBadge.BadgeTextFont = UIFont.SystemFontOfSize(16f);
            NotificationsBadge.BadgePositionAdjustment = new CGPoint(-5f, 5f);

            CreateActivityButton = new CreateFitActivityButton();

            AddSubviews(new UIView [] { 
                ResetLocationButton,
                FilterButton,
                CreateActivityButton,
                NotificationsButton,
                InvitationsButton
            });
        }

        private void SetupConstraints()
        {
            Subviews.ForEach(sv => sv.TranslatesAutoresizingMaskIntoConstraints = false);



            this.AddConstraints(
                ResetLocationButton.WithSameHeight(this),
                ResetLocationButton.Width().EqualTo().HeightOf(ResetLocationButton),
                ResetLocationButton.Bottom().EqualTo().BottomOf(this),

                FilterButton.WithSameHeight(this),
                FilterButton.Width().EqualTo().HeightOf(FilterButton),
                FilterButton.Bottom().EqualTo().BottomOf(this),

                NotificationsButton.WithSameHeight(this),
                NotificationsButton.Width().EqualTo().HeightOf(NotificationsButton),
                NotificationsButton.Bottom().EqualTo().BottomOf(this),

                InvitationsButton.WithSameHeight(this),
                InvitationsButton.Width().EqualTo().HeightOf(InvitationsButton),
                InvitationsButton.Bottom().EqualTo().BottomOf(this),

                CreateActivityButton.Height().EqualTo(34),
                CreateActivityButton.Width().EqualTo(68),
                CreateActivityButton.WithSameCenterX(this),
                CreateActivityButton.Bottom().EqualTo(-25.0f).BottomOf(this),

                ResetLocationButton.WithSameCenterX(this).WithMultiplier(0.20f),
                FilterButton.WithSameCenterX(this).WithMultiplier(0.55f),
                NotificationsButton.WithSameCenterX(this).WithMultiplier(1.45f),
                InvitationsButton.WithSameCenterX(this).WithMultiplier(1.80f)
            );


        }
    }
}

