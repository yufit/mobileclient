using System;
using System.Collections.Generic;
using System.Drawing;

using Cirrious.FluentLayouts.Touch;

using Foundation;
using UIKit;

using YuFit.Core.ViewModels;
using YuFit.IOS.Extensions;
using YuFit.IOS.Controls.Buttons;
using CoreGraphics;
using YuFit.IOS.Themes;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Views
{
    /// <summary>
    /// Base view controller.
    /// </summary>
    public class BaseViewController<T> : MvxViewController where T : NamedViewModel
    {
        protected const float Spacing = 36.0f;
        protected new T ViewModel { get { return (T) base.ViewModel; } }

        BackButton _backButtonView;
        UIScreenEdgePanGestureRecognizer _screenEdgeSwap;

        protected bool IsRadialGradientVisible = true;
        protected bool ShowBackButton = true;

        private bool _showNavBar = true;
        protected bool ShowNavBar { 
            get { return _showNavBar; }
            set { 
                _showNavBar = value;
                if (NavigationController != null) { 
                    NavigationController.NavigationBarHidden = !_showNavBar; 
                }
             }
        }

        protected bool WatchKeyboard = false;

        readonly List<NSObject> _keyboardObservers = new List<NSObject>();

        public BaseViewController() {}
		public BaseViewController(string nibName, NSBundle bundle) : base(nibName, bundle) {}
		public BaseViewController(IntPtr handle) : base(handle) {}

        ~BaseViewController () { Console.WriteLine("Destroy {0}", GetType()); }

		// Analysis disable once RedundantOverridenMember
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
            //FreeManagedResources();
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

            if (IsRadialGradientVisible) { AddGradientView(); }
            if (NavigationItem != null && NavigationController != null) { SetupNavigationWidgets(); }
        }

        protected virtual void BackButtonTouchDown (object sender, EventArgs e)
        {
            NavigationController.PopViewController(true);
        }

        void HandleScreenEdgeSwap ()
        {
            if (_screenEdgeSwap != null && NavigationController != null && _screenEdgeSwap.State == UIGestureRecognizerState.Ended)
            {
                NavigationController.PopViewController(true);
                _screenEdgeSwap.Enabled = false;
            }
        }

        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear(animated);
            if (_screenEdgeSwap != null) { View.AddGestureRecognizer(_screenEdgeSwap); }
            if (_backButtonView != null) { _backButtonView.TouchDown += BackButtonTouchDown; }

            if (NavigationController != null) { 
                NavigationController.NavigationBarHidden = !ShowNavBar; 
            }

            if (WatchKeyboard) {
                _keyboardObservers.Add(NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, HandleKeyboardWillShow));
                _keyboardObservers.Add(NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, HandleKeyboardWillHide));
            }
        }

        public override void ViewWillDisappear (bool animated)
        {
            if (_screenEdgeSwap != null) { View.RemoveGestureRecognizer(_screenEdgeSwap); }
            if (_backButtonView != null) { _backButtonView.TouchDown -= BackButtonTouchDown; }

            if (WatchKeyboard) {
                _keyboardObservers.ForEach(NSNotificationCenter.DefaultCenter.RemoveObserver);
                _keyboardObservers.Clear();
            }

            base.ViewWillDisappear(animated);
        }

        protected void FireEvent(EventHandler theEvent)
        {
            var handler = theEvent;
            if (handler != null) { handler(this, EventArgs.Empty); }
        }

        public override void WillMoveToParentViewController (UIViewController parent)
        {
            base.WillMoveToParentViewController(parent);
            if (parent == null && ViewModel != null) {
                ViewModel.Close();
            }
        }

        public override void RemoveFromParentViewController ()
        {
            base.RemoveFromParentViewController();
            if (ViewModel != null) { ViewModel.Close(); }
        }

        void ApplyBackTheme()
        {
            if (ShowBackButton && NavigationController.ViewControllers.Length > 1) {
                _backButtonView = new BackButton(new RectangleF(0, 0, 20.0f, 20.0f));
                _backButtonView.ButtonColor = Themes.YuFitStyleKit.White;
                NavigationItem.LeftBarButtonItem = new UIBarButtonItem(_backButtonView);
            }
        }

        void AddGradientView ()
        {
            View.BackgroundColor = Themes.CrudFitEvent.ViewBackgroundColor;
            RadialGradientView v = new RadialGradientView();
            v.Layer.ZPosition = -100;
            Add(v);
            v.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddConstraints(
                v.WithSameWidth(View),
                v.WithSameHeight(View),
                v.WithSameCenterX(View),
                v.WithSameCenterY(View)
            );
        }

        void SetupNavigationWidgets ()
        {
            if (!ShowBackButton) {
                NavigationItem.SetHidesBackButton(true, true);
            } else {
                NavigationItem.SetHidesBackButton(false, true);
                ApplyBackTheme();

                _screenEdgeSwap = new UIScreenEdgePanGestureRecognizer(HandleScreenEdgeSwap);
                _screenEdgeSwap.Edges = UIRectEdge.Left;
            }

            Title = ViewModel.DisplayName;
            NavigationController.ApplyTheme();
        }

        public override void TouchesBegan (NSSet touches, UIEvent evt)
        {
            View.EndEditing(true);
            base.TouchesBegan(touches, evt);
        }

        #region Keyboard handling

        void HandleKeyboardWillShow (NSNotification notification)
        {
            NSDictionary info = notification.UserInfo;
            NSObject frameBeginInfo = info[UIKeyboard.FrameBeginUserInfoKey];
            CGSize kbSize = (frameBeginInfo as NSValue).RectangleFValue.Size;
            nfloat keyboardTop = View.Bounds.Height - kbSize.Height;

            var speedValue = info.ValueForKey(NSString.FromData("UIKeyboardAnimationDurationUserInfoKey", NSStringEncoding.ASCIIStringEncoding));
            var speed = ((NSNumber) speedValue).DoubleValue;

            KeyboardWillShow(keyboardTop, speed);
        }

        void HandleKeyboardWillHide (NSNotification notification)
        {
            NSDictionary info = notification.UserInfo;
            var speedValue = info.ValueForKey(NSString.FromData("UIKeyboardAnimationDurationUserInfoKey", NSStringEncoding.ASCIIStringEncoding));
            var speed = ((NSNumber) speedValue).DoubleValue;
            KeyboardWillHide(speed);
        }

        protected virtual void KeyboardWillShow (nfloat keyboardTop, double animationSpeed) {}
        protected virtual void KeyboardWillHide (double animationSpeed) {}

        #endregion


        #if DEBUG_LEAKS

        public override void MotionEnded (UIEventSubtype motion, UIEvent evt)
        {
            GC.Collect(); 
            GC.WaitForPendingFinalizers();

            base.MotionEnded(motion, evt);
        }
        #endif
	}
}

