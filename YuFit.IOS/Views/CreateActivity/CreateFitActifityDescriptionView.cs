﻿using System;
using UIKit;

using YuFit.IOS.Controls;
using YuFit.Core.Extensions;
using System.Linq;
using Cirrious.FluentLayouts.Touch;
using YuFit.IOS.Themes;
using CoreGraphics;

namespace YuFit.IOS.Views.CreateActivity
{
    public class CreateFitActifityDescriptionView : UIView
    {
        readonly SeparatorLine  _topLine         = new SeparatorLine();
        readonly SeparatorLine  _bottomLine      = new SeparatorLine();

        public readonly UILabel TitleLabel                   = new UILabel();
        public readonly PlaceholderTextView DescriptionLabel = new PlaceholderTextView();

        private ActivityColors _activityColors;
        public ActivityColors ActivityColors { 
            get { return _activityColors; }
            set { _activityColors = value; OnPaletteUpdated(); } 
        }

        public CreateFitActifityDescriptionView ()
        {
            BackgroundColor = UIColor.Clear;

            UIView[] subviews = { _topLine, DescriptionLabel, _bottomLine };
            AddSubviews(subviews);

            subviews.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;

                this.AddConstraints(
                    v.WithSameCenterX(this),
                    v.Width().EqualTo().WidthOf(this)
                );
            });

            subviews.Where(v => v is SeparatorLine).ForEach(v => {
                v.AddConstraints(v.Height().EqualTo(1.0f));
                v.BackgroundColor = YuFitStyleKit.White;
            });

            DescriptionLabel.Font = CrudFitEvent.ConfigView.LabelTextFont;
            DescriptionLabel.TextColor = YuFitStyleKit.White;
            DescriptionLabel.Editable = true;

            SetupConstraints();

            DescriptionLabel.KeyboardType = UIKeyboardType.Default;
            DescriptionLabel.AutocorrectionType = UITextAutocorrectionType.Yes;
            DescriptionLabel.AutocapitalizationType = UITextAutocapitalizationType.Sentences;
            DescriptionLabel.BackgroundColor = UIColor.Clear;

            KeyboardToolbar toolbar = new KeyboardToolbar (new CoreGraphics.CGRect(0.0f, 0.0f, Frame.Size.Width, 44.0f));

            toolbar.TintColor = YuFitStyleKit.White;
            toolbar.BarStyle = UIBarStyle.Black;

            toolbar.Translucent = true;

            toolbar.Items = new UIBarButtonItem[]{ 
                new UIBarButtonItem("R1", 
                    UIBarButtonItemStyle.Plain, AddBarButtonText),
                new UIBarButtonItem("R2", 
                    UIBarButtonItemStyle.Plain, AddBarButtonText),
                new UIBarButtonItem("R3", 
                    UIBarButtonItemStyle.Plain, AddBarButtonText),
                new UIBarButtonItem("R4", 
                    UIBarButtonItemStyle.Plain, AddBarButtonText),
                new UIBarButtonItem("Recup", 
                    UIBarButtonItemStyle.Plain, AddBarButtonText),
                new UIBarButtonItem("Min", 
                    UIBarButtonItemStyle.Plain, AddBarButtonText),
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate {
                    this.DescriptionLabel.ResignFirstResponder();
                })
            };

            DescriptionLabel.KeyboardAppearance = UIKeyboardAppearance.Dark;
            DescriptionLabel.InputAccessoryView = toolbar;

        }

        public void AddBarButtonText(object sender, EventArgs e)
        {
            var barButtonItem = sender as UIBarButtonItem;
            if (barButtonItem != null) {
                this.DescriptionLabel.Text += barButtonItem.Title + " ";
                UIDevice.CurrentDevice.PlayInputClick ();
            }

        }
        void OnPaletteUpdated ()
        {
            DescriptionLabel.TextColor = ActivityColors.Base;
            _topLine.BackgroundColor = ActivityColors.Base;
            _bottomLine.BackgroundColor = ActivityColors.Base;
        }

        void SetupConstraints ()
        {
            this.AddConstraints(
                _topLine.Top().EqualTo(1.0f).TopOf(this),
                DescriptionLabel.Top().EqualTo(8.0f).BottomOf(_topLine),
                DescriptionLabel.Bottom().EqualTo(-8.0f).TopOf(_bottomLine),
                _bottomLine.Bottom().EqualTo(-1.0f).BottomOf(this)
            );
        }

        public override CGSize SizeThatFits (CGSize size)
        {
            // TODO Replace the 20 with some math (textView.Y - this.Frame.Y) + (this.Frame.Bottom - thexView.Bottom)
            var textViewHeight = DescriptionLabel.SizeThatFits(size).Height;
            return new CGSize(size.Width, textViewHeight + 40);
        }
    }
}

