using System;
using System.Linq;

using Cirrious.FluentLayouts.Touch;

using CoreGraphics;
using Fcaico.Controls.ArrowSlider;
using Foundation;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.CreateActivity;

using YuFit.IOS.Controls;
using YuFit.IOS.Controls.Selections;
using YuFit.IOS.Themes;
using YuFit.IOS.Utilities;
using YuFit.Core.ViewModels.CreateActivity.ConfigureActivities;

namespace YuFit.IOS.Views.CreateActivity.ConfigureActivities
{
    public class HeaderIconView : DiamondShapeView
    {
        public Action<CGRect, UIColor>  DrawActivityIcon  { get; set; }
        public UIColor                      ActivityFillColor { get; set; }

        public HeaderIconView(CGRect rect) : base(rect) {}
        public HeaderIconView() {}

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            if (DrawActivityIcon != null) {
                using (var ctx = new CGContextHelper(UIGraphics.GetCurrentContext())) {
                    CGRect imageRect = Bounds.Inset(0.32f*Bounds.Width, 0.32f*Bounds.Height);
                    DrawActivityIcon(imageRect, ActivityFillColor);
                }
            }
        }
    }

    /// <summary>
    /// This is so that when we the user is using the slider, the scrollview
    /// doesn't intercepts it.  For it to work the following must be set:
    /// 
    /// DelaysContentTouches = false;
    /// CanCancelContentTouches = true;
    /// </summary>
    public class MyScrollView : UIScrollView
    {
        public MyScrollView ()
        {
            DelaysContentTouches = false;
            CanCancelContentTouches = true;
        }

        public override bool TouchesShouldCancelInContentView(UIView view)
        {
            return !(view is ArrowSliderView || view.Superview is ArrowSliderView);
        }
    }


    public abstract class BaseActivityConfigureView<T> : BaseViewController<T>
        where T : BaseActivityConfigureVM
    {
        protected string ActivityName { get; private set; }
        protected ActivityColors Colors { get; private set; }
        public readonly UIView DiamondContainerView = new UIView();
        public readonly HeaderIconView HeaderIcon = new HeaderIconView();
        public readonly UIScrollView ScrollView = new MyScrollView();

        private UITapGestureRecognizer _selectTypeGesture;

        protected BaseActivityConfigureView (string activityName)
        {
            ActivityName = activityName;

            Colors = YuFitStyleKitExtension.ActivityBackgroundColors(activityName);

            HeaderIcon.DiamondColor      = Colors.Base;
            HeaderIcon.StrokeColor       = Colors.Dark;
            HeaderIcon.StrokeWidth       = 3.0f;
            HeaderIcon.PlusLocation      = PlusLocation.None;
            HeaderIcon.DrawActivityIcon  = YuFitStyleKitExtension.GetDrawingMethod("Draw"+ActivityName);
            HeaderIcon.ActivityFillColor = YuFitStyleKit.White;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            View.Add(ScrollView);
            ScrollView.TranslatesAutoresizingMaskIntoConstraints = false;

            var viewsDict = new NSDictionary("scrollview", ScrollView);
            View.AddConstraints(NSLayoutConstraint.FromVisualFormat("H:|[scrollview]|", 0, null, viewsDict));

            SetupHeaderView(ScrollView);

            View.AddConstraints(
                ScrollView.Top().EqualTo().TopOf(View),
                ScrollView.Bottom().EqualTo().BottomOf(View)
            );

            UIView[] views = GetChildrenViews();
            ScrollView.AddSubviews(views);

            views.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;
                ScrollView.AddConstraints(v.WithSameCenterX(ScrollView));
            });

            views.Where(v => v is ArrowSliderView).ForEach(v => {
                // Analysis disable ConvertToLambdaExpression
                v.AddConstraints(v.Height().EqualTo(30.0f));
                ScrollView.AddConstraints(v.Width().EqualTo().WidthOf(ScrollView).WithMultiplier(0.75f));

                ArrowSliderView slider = (ArrowSliderView) v;
                slider.ArrowColor = Colors.Base;
                slider.ExclusiveTouch = true;
                slider.Font = CrudFitEvent.SliderTextFont;
                slider.BackgroundColor = UIColor.Clear;
                //((ArrowSliderView) v).Font... and text color
            });

            views.Where(v => v is SeparatorLine).ForEach(v => {
                v.AddConstraints(v.Height().EqualTo(1.5f));
                ScrollView.AddConstraints(v.Width().EqualTo().WidthOf(ScrollView).WithMultiplier(0.85f));
                v.BackgroundColor = Colors.Base;
            });

            views.Where(v => v is UILabel).ForEach(v => {
                (v as UILabel).Font = CrudFitEvent.ConfigView.LabelTextFont;
                (v as UILabel).TextColor = Colors.Base;
                (v as UILabel).TextAlignment = UITextAlignment.Center;
                ScrollView.AddConstraints(v.Width().EqualTo().WidthOf(ScrollView).WithMultiplier(0.85f));
            });

            views.Where(v => v is SelectionViewControllerView).ForEach(v => {
                ScrollView.AddConstraints(v.Width().EqualTo().WidthOf(ScrollView).WithMultiplier(0.85f));
            });

            _selectTypeGesture = new UITapGestureRecognizer(() => { NavigationController.PopViewController(true); });
        }

        public override void ViewDidAppear (bool animated)
        {
            base.ViewDidAppear(animated);

            ScrollView.IndicatorStyle = UIScrollViewIndicatorStyle.White;
            ScrollView.ShowsVerticalScrollIndicator = true;
            ScrollView.FlashScrollIndicators();
            ScrollView.ShowsHorizontalScrollIndicator = true;
            HeaderIcon.AddGestureRecognizer(_selectTypeGesture);
        }

        public override void ViewDidDisappear (bool animated)
        {
            HeaderIcon.RemoveGestureRecognizer(_selectTypeGesture);
            base.ViewDidDisappear(animated);
        }

        protected override void BackButtonTouchDown (object sender, EventArgs e)
        {
            int viewCount = NavigationController.ViewControllers.Count();
            var navTo = viewCount - 2;

            NavigationController.PopToViewController(NavigationController.ViewControllers[navTo-1], true);
            //base.BackButtonTouchDown(sender, e);
        }

        protected abstract UIView[] GetChildrenViews();

        void SetupHeaderView (UIView container)
        {
            container.Add(DiamondContainerView);
            DiamondContainerView.Add(HeaderIcon);

            DiamondContainerView.TranslatesAutoresizingMaskIntoConstraints = false;
            HeaderIcon.TranslatesAutoresizingMaskIntoConstraints = false;

            container.AddConstraints(
                DiamondContainerView.WithSameCenterX(container),
                DiamondContainerView.WithSameWidth(container),
                DiamondContainerView.Top().EqualTo(20).TopOf(container)
            );

            DiamondContainerView.AddConstraints(DiamondContainerView.Height().EqualTo(175.0f));

            DiamondContainerView.AddConstraints(
                HeaderIcon.CenterX().EqualTo().CenterXOf(DiamondContainerView),
                HeaderIcon.CenterY().EqualTo().CenterYOf(DiamondContainerView)
            );

            DiamondContainerView.AddConstraints(
                HeaderIcon.Height().EqualTo().HeightOf(DiamondContainerView),
                HeaderIcon.Width().EqualTo().HeightOf(DiamondContainerView)
            );

        }
    }
}

