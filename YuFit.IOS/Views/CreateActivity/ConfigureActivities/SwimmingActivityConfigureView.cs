using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using Fcaico.Controls.ArrowSlider;
using UIKit;
using YuFit.Core.Models.Activities;
using YuFit.Core.ViewModels.CreateActivity.ConfigureActivities;
using YuFit.IOS.Controls.Selections;
using YuFit.IOS.Themes;
using YuFit.IOS.Views.CreateActivity.Transitions;

namespace YuFit.IOS.Views.CreateActivity.ConfigureActivities
{
    [Attributes.PushTransition(typeof(CreateFitActivityPushTransition), Duration=0.25f)]
    [Attributes.PopTransition(typeof(CreateFitActivityPopTransition), Duration=0.25f)]
    public class SwimmingActivityConfigureView : BaseActivityConfigureView<SwimmingActivityConfigureViewModel>
    {
        public class TypeSelection : SelectionViewController<string, SwimmingType> {}
        public class IntensitySelection : SelectionViewController<string, SwimmingIntensity> {}

        readonly UILabel            _swimmingTypeTitle      = new UILabel();
        readonly TypeSelection      _selectSwimmingTypeView = new TypeSelection();

        readonly UILabel            _distanceLabel          = new UILabel();
        readonly ArrowSliderView    _distanceSlider         = new ArrowSliderView(); 

        readonly UILabel            _durationLabel          = new UILabel();
        readonly ArrowSliderView    _durationSlider         = new ArrowSliderView(); 

        readonly UILabel            _intensityTitleLabel    = new UILabel();
        readonly IntensitySelection _selectIntensityView    = new IntensitySelection();

        string _distanceUnit;
        public string DistanceUnit {
            get { return _distanceUnit; }
            set { _distanceUnit = value; InitializeSliders(); }
        }

        string _durationUnit;
        public string DurationUnit {
            get { return _durationUnit; }
            set { _durationUnit = value; InitializeSliders(); }
        }

        public SwimmingActivityConfigureView () : base("Swimming")
        {
            _selectIntensityView.Colors = Colors;
            _selectSwimmingTypeView.Colors = Colors;
        }

        protected override UIView[] GetChildrenViews ()
        {
            UIView[] views = {
                _swimmingTypeTitle, _selectSwimmingTypeView.View, 
                _distanceLabel, _distanceSlider,  
                _durationLabel, _durationSlider, 
                _intensityTitleLabel, _selectIntensityView.View
            };
            return views;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            AddChildViewController(_selectIntensityView);
            AddChildViewController(_selectSwimmingTypeView);

            InitializeSliders();
            SetupConstraints();

            SetupBindings();
        }

        void InitializeSliders()
        {
            _durationSlider.Values = ViewModel.DurationValues;
            _distanceSlider.Values = ViewModel.DistanceValues;

            _durationSlider.Font = CrudFitEvent.ConfigView.SliderFont;
            _durationSlider.FontBaselineOffset = CrudFitEvent.ConfigView.SliderFontOffset;

            _distanceSlider.Font = CrudFitEvent.ConfigView.SliderFont;
            _distanceSlider.FontBaselineOffset = CrudFitEvent.ConfigView.SliderFontOffset;
        }

        void SetupConstraints ()
        {
            View.AddConstraints(
                _swimmingTypeTitle.Top().EqualTo(15.0f).BottomOf(HeaderIcon),
                _selectSwimmingTypeView.View.Top().EqualTo(2.0f).BottomOf(_swimmingTypeTitle),
                _distanceLabel.Top().EqualTo(15.0f).BottomOf(_selectSwimmingTypeView.View),
                _distanceSlider.Top().EqualTo().BottomOf(_distanceLabel),
                _durationLabel.Top().EqualTo(15.0f).BottomOf(_distanceSlider),
                _durationSlider.Top().EqualTo().BottomOf(_durationLabel),
                _intensityTitleLabel.Top().EqualTo(15.0f).BottomOf(_durationSlider),
                _selectIntensityView.View.Top().EqualTo(2.0f).BottomOf(_intensityTitleLabel)
            );

            // Magic juju that makes the scroll figure out its content size.
            ScrollView.AddConstraints(
                _selectIntensityView.View.Bottom().EqualTo(-70).BottomOf(ScrollView)
            );
        }

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<SwimmingActivityConfigureView, SwimmingActivityConfigureViewModel>();
            set.Bind(_swimmingTypeTitle     ).To(vm => vm.SwimmingTypeTitle);
            set.Bind(_distanceLabel         ).To (vm => vm.DistanceTitle);
            set.Bind(_durationLabel         ).To(vm => vm.DurationTitle);
            set.Bind(_intensityTitleLabel   ).To(vm => vm.TrainingTypeTitle);
            set.Bind(_selectIntensityView   ).For(me => me.SelectItemInListCommand).To(vm => vm.SelectIntensityCommand);
            set.Bind(_selectSwimmingTypeView).For(me => me.SelectItemInListCommand).To(vm => vm.SelectTypeCommand);

            set.Bind(_distanceSlider).For(slider => slider.CurrentValue).To(vm => vm.Distance);
            set.Bind(_durationSlider).For(slider => slider.CurrentValue).To(vm => vm.Duration);
            set.Bind().For("DistanceUnit").To(vm => vm.DistanceUnit);
            set.Bind().For("DurationUnit").To(vm => vm.DurationUnit);

            set.Apply();
        }
    }
}

