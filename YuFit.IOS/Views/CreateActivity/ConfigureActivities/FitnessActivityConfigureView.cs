﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Fcaico.Controls.ArrowSlider;
using UIKit;

using YuFit.Core.Models.Activities;
using YuFit.Core.ViewModels.CreateActivity.ConfigureActivities;

using YuFit.IOS.Controls.Selections;
using YuFit.IOS.Themes;
using YuFit.IOS.Views.CreateActivity.Transitions;

namespace YuFit.IOS.Views.CreateActivity.ConfigureActivities
{
    [Attributes.PushTransition(typeof(CreateFitActivityPushTransition), Duration=0.25f)]
    [Attributes.PopTransition(typeof(CreateFitActivityPopTransition), Duration=0.25f)]
    public class FitnessActivityConfigureView : BaseActivityConfigureView<FitnessActivityConfigureViewModel>
    {
        public class TypeSelection : SelectionViewController<string, FitnessType> {}
        public class IntensitySelection : SelectionViewController<string, FitnessIntensity> {}

        readonly UILabel            _FitnessTypeTitle      = new UILabel();
        readonly TypeSelection      _selectFitnessTypeView = new TypeSelection();

        readonly UILabel            _durationLabel      = new UILabel();
        readonly ArrowSliderView    _durationSlider     = new ArrowSliderView(); 

        readonly UILabel            _intensityTitleLabel = new UILabel();
        readonly IntensitySelection _selectIntensityView = new IntensitySelection();

        string _durationUnit;
        public string DurationUnit {
            get { return _durationUnit; }
            set { _durationUnit = value; InitializeSliders(); }
        }

        public FitnessActivityConfigureView () : base("Fitness")
        {
            _selectIntensityView.Colors  = Colors;
            _selectFitnessTypeView.Colors   = Colors;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            AddChildViewController(_selectIntensityView);
            AddChildViewController(_selectFitnessTypeView);

            InitializeSliders();
            SetupConstraints();

            SetupBindings();
        }

        void InitializeSliders()
        {
            _durationSlider.Values = ViewModel.DurationValues;

            _durationSlider.Font = CrudFitEvent.ConfigView.SliderFont;
            _durationSlider.FontBaselineOffset = CrudFitEvent.ConfigView.SliderFontOffset;

        }

        protected override UIView[] GetChildrenViews ()
        {
            UIView[] views = {
                _FitnessTypeTitle, _selectFitnessTypeView.View, 
                _durationLabel, _durationSlider, 
                _intensityTitleLabel, _selectIntensityView.View
            };
            return views;
        }

        void SetupConstraints ()
        {
            View.AddConstraints(
                _FitnessTypeTitle.Top().EqualTo(15.0f).BottomOf(HeaderIcon),
                _selectFitnessTypeView.View.Top().EqualTo(2.0f).BottomOf(_FitnessTypeTitle),
                _durationLabel.Top().EqualTo(15.0f).BottomOf(_selectFitnessTypeView.View),
                _durationSlider.Top().EqualTo().BottomOf(_durationLabel),
                _intensityTitleLabel.Top().EqualTo(15.0f).BottomOf(_durationSlider),
                _selectIntensityView.View.Top().EqualTo(2.0f).BottomOf(_intensityTitleLabel)
            );

            // Magic juju that makes the scroll figure out its content size.
            ScrollView.AddConstraints(
                _selectIntensityView.View.Bottom().EqualTo(-70).BottomOf(ScrollView)
            );
        }

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<FitnessActivityConfigureView, FitnessActivityConfigureViewModel>();
            set.Bind(_FitnessTypeTitle      ).To(vm => vm.FitnessTypeTitle);
            set.Bind(_durationLabel         ).To(vm => vm.DurationTitle);
            set.Bind(_intensityTitleLabel   ).To(vm => vm.TrainingTypeTitle);
            set.Bind(_selectIntensityView   ).For(me => me.SelectItemInListCommand).To(vm => vm.SelectIntensityCommand);
            set.Bind(_selectFitnessTypeView ).For(me => me.SelectItemInListCommand).To(vm => vm.SelectTypeCommand);

            set.Bind(_durationSlider).For(slider => slider.CurrentValue).To(vm => vm.Duration);
            set.Bind().For("DurationUnit").To(vm => vm.DurationUnit);

            set.Apply();
        }
    }
}

