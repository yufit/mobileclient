using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Fcaico.Controls.ArrowSlider;
using UIKit;

using YuFit.Core.Models.Activities;
using YuFit.Core.ViewModels.CreateActivity.ConfigureActivities;

using YuFit.IOS.Controls.Selections;
using YuFit.IOS.Themes;
using YuFit.IOS.Views.CreateActivity.Transitions;

namespace YuFit.IOS.Views.CreateActivity.ConfigureActivities
{
    [Attributes.PushTransition(typeof(CreateFitActivityPushTransition), Duration=0.25f)]
    [Attributes.PopTransition(typeof(CreateFitActivityPopTransition), Duration=0.25f)]
    public class YogaActivityConfigureView : BaseActivityConfigureView<YogaActivityConfigureViewModel>
    {
        public class TypeSelection : SelectionViewController<string, YogaType> {}
        public class IntensitySelection : SelectionViewController<string, YogaIntensity> {}

        readonly UILabel            _yogaTypeTitle      = new UILabel();
        readonly TypeSelection      _selectYogaTypeView = new TypeSelection();

        readonly UILabel            _durationLabel      = new UILabel();
        readonly ArrowSliderView    _durationSlider     = new ArrowSliderView(); 

        readonly UILabel            _intensityTitleLabel = new UILabel();
        readonly IntensitySelection _selectIntensityView = new IntensitySelection();

        string _durationUnit;
        public string DurationUnit {
            get { return _durationUnit; }
            set { _durationUnit = value; InitializeSliders(); }
        }

        public YogaActivityConfigureView () : base("Yoga")
        {
            _selectIntensityView.Colors  = Colors;
            _selectYogaTypeView.Colors   = Colors;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            AddChildViewController(_selectIntensityView);
            AddChildViewController(_selectYogaTypeView);

            InitializeSliders();
            SetupConstraints();

            SetupBindings();
        }

        void InitializeSliders()
        {
            _durationSlider.Values = ViewModel.DurationValues;

            _durationSlider.Font = CrudFitEvent.ConfigView.SliderFont;
            _durationSlider.FontBaselineOffset = CrudFitEvent.ConfigView.SliderFontOffset;

        }

        protected override UIView[] GetChildrenViews ()
        {
            UIView[] views = {
                _yogaTypeTitle, _selectYogaTypeView.View, 
                _durationLabel, _durationSlider, 
                _intensityTitleLabel, _selectIntensityView.View
            };
            return views;
        }

        void SetupConstraints ()
        {
            View.AddConstraints(
                _yogaTypeTitle.Top().EqualTo(15.0f).BottomOf(HeaderIcon),
                _selectYogaTypeView.View.Top().EqualTo(2.0f).BottomOf(_yogaTypeTitle),
                _durationLabel.Top().EqualTo(15.0f).BottomOf(_selectYogaTypeView.View),
                _durationSlider.Top().EqualTo().BottomOf(_durationLabel),
                _intensityTitleLabel.Top().EqualTo(15.0f).BottomOf(_durationSlider),
                _selectIntensityView.View.Top().EqualTo(2.0f).BottomOf(_intensityTitleLabel)
            );

            // Magic juju that makes the scroll figure out its content size.
            ScrollView.AddConstraints(
                _selectIntensityView.View.Bottom().EqualTo(-70).BottomOf(ScrollView)
            );
        }

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<YogaActivityConfigureView, YogaActivityConfigureViewModel>();
            set.Bind(_yogaTypeTitle         ).To(vm => vm.YogaTypeTitle);
            set.Bind(_durationLabel         ).To(vm => vm.DurationTitle);
            set.Bind(_intensityTitleLabel   ).To(vm => vm.TrainingTypeTitle);
            set.Bind(_selectIntensityView   ).For(me => me.SelectItemInListCommand).To(vm => vm.SelectIntensityCommand);
            set.Bind(_selectYogaTypeView    ).For(me => me.SelectItemInListCommand).To(vm => vm.SelectTypeCommand);

            set.Bind(_durationSlider).For(slider => slider.CurrentValue).To(vm => vm.Duration);
            set.Bind().For("DurationUnit").To(vm => vm.DurationUnit);

            set.Apply();
        }
    }
}

