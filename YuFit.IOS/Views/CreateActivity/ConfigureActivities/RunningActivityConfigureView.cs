using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Fcaico.Controls.ArrowSlider;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.Models.Activities;
using YuFit.Core.ViewModels.CreateActivity.ConfigureActivities;

using YuFit.IOS.Controls.Selections;
using YuFit.IOS.Themes;
using YuFit.IOS.Views.CreateActivity.Transitions;

namespace YuFit.IOS.Views.CreateActivity.ConfigureActivities
{
    [Attributes.PushTransition(typeof(CreateFitActivityPushTransition), Duration=0.25f)]
    [Attributes.PopTransition(typeof(CreateFitActivityPopTransition), Duration=0.25f)]
    public class RunningActivityConfigureView : BaseActivityConfigureView<RunningActivityConfigureViewModel>
    {
        public class TypeSelection : SelectionViewController<string, RunningType> {}
        public class IntensitySelection : SelectionViewController<string, RunningIntensity> {}

        readonly UILabel            _runningTypeTitle       = new UILabel();
        readonly TypeSelection      _selectRunningTypeView  = new TypeSelection();

        readonly UILabel            _distanceLabel          = new UILabel();
        readonly ArrowSliderView    _distanceSlider         = new ArrowSliderView(); 

        readonly UILabel            _durationLabel          = new UILabel();
        readonly ArrowSliderView    _durationSlider         = new ArrowSliderView(); 

        readonly UIView             _speedContainer         = new UIView();
        readonly UILabel            _speedLabel             = new UILabel();
        readonly UILabel            _speedUnitLabel         = new UILabel();

        readonly UILabel            _intensityTitleLabel    = new UILabel();
        readonly IntensitySelection _selectIntensityView    = new IntensitySelection();

        string _distanceUnit;
        public string DistanceUnit {
            get { return _distanceUnit; }
            set { _distanceUnit = value; InitializeSliders(); }
        }

        string _durationUnit;
        public string DurationUnit {
            get { return _durationUnit; }
            set { _durationUnit = value; InitializeSliders(); }
        }

        public RunningActivityConfigureView () : base("Running")
        {
            _selectIntensityView.Colors = Colors;
            _selectRunningTypeView.Colors = Colors;
        }

        protected override UIView[] GetChildrenViews ()
        {
            UIView[] views = {
                _runningTypeTitle, _selectRunningTypeView.View, 
                _distanceLabel, _distanceSlider,  
                _durationLabel, _durationSlider, 
                _speedContainer, 
                _intensityTitleLabel, _selectIntensityView.View
            };
            return views;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            AddChildViewController(_selectIntensityView);
            AddChildViewController(_selectRunningTypeView);

            InitializeSliders();
            SetupConstraints(ScrollView);
            SetupSpeedContainerViews();

            SetupBindings();
        }

        void InitializeSliders()
        {
            _durationSlider.Values = ViewModel.DurationValues;
            _distanceSlider.Values = ViewModel.DistanceValues;

            _durationSlider.Font = CrudFitEvent.ConfigView.SliderFont;
            _durationSlider.FontBaselineOffset = CrudFitEvent.ConfigView.SliderFontOffset;

            _distanceSlider.Font = CrudFitEvent.ConfigView.SliderFont;
            _distanceSlider.FontBaselineOffset = CrudFitEvent.ConfigView.SliderFontOffset;
        }

        void SetupConstraints (UIView container)
        {
            _speedContainer.AddConstraints(_speedContainer.Height().EqualTo(30.0f));
            container.AddConstraints(_speedContainer.Width().EqualTo().WidthOf(container).WithMultiplier(0.85f));

            container.AddConstraints(
                _runningTypeTitle.Top().EqualTo(15.0f).BottomOf(DiamondContainerView),
                _selectRunningTypeView.View.Top().EqualTo(2.0f).BottomOf(_runningTypeTitle),
                _distanceLabel.Top().EqualTo(15.0f).BottomOf(_selectRunningTypeView.View),
                _distanceSlider.Top().EqualTo().BottomOf(_distanceLabel),
                _durationLabel.Top().EqualTo(15.0f).BottomOf(_distanceSlider),
                _durationSlider.Top().EqualTo().BottomOf(_durationLabel),
                _speedContainer.Top().EqualTo(15.0f).BottomOf(_durationSlider),
                _intensityTitleLabel.Top().EqualTo(15.0f).BottomOf(_speedContainer),
                _selectIntensityView.View.Top().EqualTo(2.0f).BottomOf(_intensityTitleLabel)
            );

            // Magic juju that makes the scroll figure out its content size.
            container.AddConstraints(
                _selectIntensityView.View.Bottom().EqualTo(-70).BottomOf(container)
            );
        }

        void SetupSpeedContainerViews ()
        {
            _speedContainer.AddSubviews(new UIView[] {_speedLabel, _speedUnitLabel});

            _speedContainer.Subviews.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;

                _speedContainer.AddConstraints(
                    v.Width().EqualTo().WidthOf(_speedContainer).WithMultiplier(0.5f),
                    v.WithSameHeight(_speedContainer)
                );
            });

            _speedLabel.Font = CrudFitEvent.ConfigView.SpeedValueFont;
            _speedLabel.TextColor = YuFitStyleKit.RunningDarkColor;
            _speedLabel.TextAlignment = UITextAlignment.Right;

            _speedUnitLabel.Font = CrudFitEvent.ConfigView.SpeedUnitFont;
            _speedUnitLabel.TextColor = CrudFitEvent.ConfigView.SpeedUnitColor;

            _speedContainer.AddConstraints(
                _speedLabel.CenterY().EqualTo(2.0f).CenterYOf(_speedContainer),
                _speedLabel.Right().EqualTo(-2.0f).CenterXOf(_speedContainer),
                _speedUnitLabel.CenterY().EqualTo(4.0f).CenterYOf(_speedContainer),
                _speedUnitLabel.Left().EqualTo(2.0f).CenterXOf(_speedContainer)
            );
        }

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<RunningActivityConfigureView, RunningActivityConfigureViewModel>();
            set.Bind(_runningTypeTitle      ).To(vm => vm.RunningTypeTitle);
            set.Bind(_distanceLabel         ).To(vm => vm.DistanceTitle);
            set.Bind(_durationLabel         ).To(vm => vm.DurationTitle);
            set.Bind(_intensityTitleLabel   ).To(vm => vm.TrainingTypeTitle);
            set.Bind(_speedLabel            ).To(vm => vm.Speed);
            set.Bind(_speedUnitLabel        ).To(vm => vm.SpeedUnit);
            set.Bind(_selectIntensityView   ).For(me => me.SelectItemInListCommand).To(vm => vm.SelectIntensityCommand);
            set.Bind(_selectRunningTypeView ).For(me => me.SelectItemInListCommand).To(vm => vm.SelectRunningTypeCommand);

            set.Bind(_distanceSlider).For(slider => slider.CurrentValue).To(vm => vm.Distance);
            set.Bind(_durationSlider).For(slider => slider.CurrentValue).To(vm => vm.Duration);
            set.Bind().For("DistanceUnit").To(vm => vm.DistanceUnit);
            set.Bind().For("DurationUnit").To(vm => vm.DurationUnit);

            set.Apply();
        }
    }
}

