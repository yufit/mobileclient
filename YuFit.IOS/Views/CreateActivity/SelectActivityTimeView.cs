using System;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using CoreGraphics;

using Fcaico.Controls.Calendar;
using Fcaico.iOS.Controls.TimePicker;

using Foundation;
using UIKit;

using YuFit.Core.ViewModels.CreateActivity;
using YuFit.IOS.Controls.Selections;
using YuFit.IOS.Themes;
using YuFit.WebServices.Interface.Model;

namespace YuFit.IOS.Views.CreateActivity
{
//    [Attributes.PushTransition(typeof(CreateFitActivityPushTransition), Duration=0.25f)]
//    [Attributes.PopTransition(typeof(CreateFitActivityPopTransition), Duration=0.25f)]
    public class SelectActivityTimeView : BaseCreateActivityViewController<SelectActivityTimeViewModel>
    {
        #region types
        public class RepeatSelection : SelectionViewController<string, EventRecurrence> {}
        #endregion

        #region Data Members
        //readonly RepeatSelection _selectRepeatView = new RepeatSelection();
        readonly UIScrollView _scrollView = new UIScrollView();
        readonly CalendarView _calendar = new CalendarView();
        readonly TimePickerView _timePicker = new TimePickerView();
        #endregion


        #region Constructor / Destructor

        #endregion

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad(); 

            View.Add(_scrollView);
            _scrollView.AddSubview(_calendar);
            _scrollView.AddSubview(_timePicker);
           // _scrollView.AddSubview(_selectRepeatView.View);
                
            SetupCalendar();
            SetupTimePicker();
            //SetupSelectRepeatView();

            SetupConstraints();
            SetupBinding();
        }

        public override void ViewDidAppear (bool animated)
        {
            base.ViewDidAppear(animated);
            _scrollView.IndicatorStyle = UIScrollViewIndicatorStyle.White;
            _scrollView.ShowsVerticalScrollIndicator = true;
            _scrollView.FlashScrollIndicators();
            _scrollView.ShowsHorizontalScrollIndicator = true;
        }

        private void SetupBinding()
        {
            var set = this.CreateBindingSet<SelectActivityTimeView, SelectActivityTimeViewModel>();

            set.Bind(_calendar          ).For(v => v.SelectedDate).To(vm => vm.SelectedDate);
            set.Bind(_calendar          ).For(v => v.Date).To(vm => vm.SelectedDate);
            set.Bind(_timePicker        ).For(v => v.Time).To(vm => vm.SelectedTime);
            set.Bind(_timePicker        ).For(v => v.MinTime).To(vm => vm.MinTime);
            set.Bind(_timePicker        ).For(v => v.MaxTime).To(vm => vm.MaxTime);
            //set.Bind(_selectRepeatView  ).For(me => me.SelectItemInListCommand).To(vm => vm.SelectRepeatIntervalCommand);

            set.Apply();
        }

        private void SetupConstraints() 
        { 
            _calendar.TranslatesAutoresizingMaskIntoConstraints = false;
            _timePicker.TranslatesAutoresizingMaskIntoConstraints = false;
            //_selectRepeatView.View.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.TranslatesAutoresizingMaskIntoConstraints = false;

            var viewsDict = new NSDictionary("scrollview", _scrollView);
            View.AddConstraints(NSLayoutConstraint.FromVisualFormat("H:|[scrollview]|", 0, null, viewsDict));

            View.AddConstraints(
                _scrollView.Top().EqualTo().TopOf(View),
                _scrollView.Bottom().EqualTo().BottomOf(View)
            );


            _calendar.AddConstraints(
                _calendar.Height().EqualTo(250));

            _timePicker.AddConstraints(
                _timePicker.Height().EqualTo(130));

//            _selectRepeatView.View.AddConstraints(
//                _selectRepeatView.View.Height().EqualTo(30f));

            View.AddConstraints(
                _calendar.Width().EqualTo().WidthOf(View).WithMultiplier(.95f),
                _calendar.CenterX().EqualTo().CenterXOf(View),

                _timePicker.Width().EqualTo().WidthOf(_calendar),
                _timePicker.CenterX().EqualTo().CenterXOf(View)//,

//                _selectRepeatView.View.Width().EqualTo().WidthOf(_calendar),
//                _selectRepeatView.View.CenterX().EqualTo().CenterXOf(View)
            );

            _scrollView.AddConstraints(
                _calendar.Top().EqualTo(20).TopOf(_scrollView),
                _timePicker.Top().EqualTo(35).BottomOf(_calendar)//,
//                _selectRepeatView.View.Top().EqualTo(30).BottomOf(_timePicker),
//                _selectRepeatView.View.Bottom().EqualTo(-80).BottomOf(_scrollView)
            );
            
        }

        private void SetupCalendar()
        {
            _calendar.BackgroundColor = UIColor.Clear;
            _calendar.UseDayInitials = true;
            _calendar.DisablePastDates = true;
            _calendar.HidePreviousAndNextMonthDays = true;
            _calendar.DayNamesColor = YuFitStyleKit.White;
            _calendar.DayNamesBackgroundColor = ColorPalette.Base;
            _calendar.SelectionColor = ColorPalette.Base;
            _calendar.RuleColor = ColorPalette.Base;
            _calendar.MonthColor = YuFitStyleKit.White;
            _calendar.WeekDaysColor = YuFitStyleKit.White;
            _calendar.WeekendDaysColor = YuFitStyleKit.White;
            _calendar.DayNameFont = CrudFitEvent.ActivityTimeView.DayNameFont;
            _calendar.MonthFont = CrudFitEvent.ActivityTimeView.MonthFont;
            _calendar.WeekDayFont = CrudFitEvent.ActivityTimeView.WeekDayFont;
            _calendar.WeekendFont = CrudFitEvent.ActivityTimeView.WeekendFont;
            _calendar.SelectionFont = CrudFitEvent.ActivityTimeView.SelectionFont;

            _calendar.MonthFontBaselineOffset = CrudFitEvent.ActivityTimeView.MonthFontOffset;
            _calendar.DayNameFontBaselineOffset = CrudFitEvent.ActivityTimeView.DayNameFontOffset;
            _calendar.DayFontBaselineOffset = CrudFitEvent.ActivityTimeView.DayFontOffset;

            _calendar.PreviousMonthImage = YuFitStyleKit.ImageOfPreviousMonth(
                new CGRect(0, 0, 30, 30), ColorPalette.Base);

            _calendar.NextMonthImage = YuFitStyleKit.ImageOfNextMonth(
                new CGRect(0, 0, 30, 30), ColorPalette.Base);
            _calendar.Date = DateTime.Now;
        }

        private void SetupTimePicker()
        {
            _timePicker.Time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 
                DateTime.Now.AddHours(1).Hour, 0, 0);

            _timePicker.MinTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 
                DateTime.Now.AddHours(1).Hour, 0, 0);

            _timePicker.RuleColor = ColorPalette.Base;
            _timePicker.TimeColor = YuFitStyleKit.White;
            _timePicker.AmPmColor = YuFitStyleKit.White;
            _timePicker.LabelFont = CrudFitEvent.ActivityTimeView.TimeLabelFont;
            _timePicker.TimeFont = CrudFitEvent.ActivityTimeView.TimeFont;
            _timePicker.AmPmFont = CrudFitEvent.ActivityTimeView.AmPmFont;
            _timePicker.TimeFontOffset = CrudFitEvent.ActivityTimeView.TimeFontOffset;

            _timePicker.BackImage = YuFitStyleKit.ImageOfDownArrow(
                new CGRect(0, 0, 30, 30), ColorPalette.Base);

            _timePicker.ForwardImage = YuFitStyleKit.ImageOfUpArrow(
                new CGRect(0, 0, 30, 30), ColorPalette.Base);            
        }

        private void SetupSelectRepeatView()
        {
//            _selectRepeatView.Colors = ColorPalette;
//            AddChildViewController(_selectRepeatView);
        }

        protected override void OnPaletteUpdated(ActivityColors colors)
        {
            _calendar.DayNamesBackgroundColor = colors.Base;
            _calendar.SelectionColor = colors.Base;

            _calendar.PreviousMonthImage = YuFitStyleKit.ImageOfPreviousMonth(
                new CGRect(0, 0, 30, 30), colors.Base);

            _calendar.NextMonthImage = YuFitStyleKit.ImageOfNextMonth(
                new CGRect(0, 0, 30, 30), colors.Base);

            _timePicker.BackImage = YuFitStyleKit.ImageOfDownArrow(
                new CGRect(0, 0, 30, 30), colors.Base);

            _timePicker.ForwardImage = YuFitStyleKit.ImageOfUpArrow(
                new CGRect(0, 0, 30, 30), colors.Base);
//
//            _selectRepeatView.Colors = colors;
        }
    }
}

