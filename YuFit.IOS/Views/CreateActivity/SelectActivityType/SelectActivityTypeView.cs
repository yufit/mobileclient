using MvvmCross.Binding.BindingContext;

using Foundation;
using UIKit;

using YuFit.Core.ViewModels.CreateActivity;

using YuFit.IOS.Controls.ActivityCollectionView;
using YuFit.IOS.Views.CreateActivity.Transitions;

namespace YuFit.IOS.Views.CreateActivity.SelectActivityType
{
    [Attributes.PushTransition(typeof(CreateFitActivityPushTransition), Duration=0.25f)]
    [Attributes.PopTransition(typeof(CreateFitActivityPopTransition), Duration=0.25f)]
    public class SelectActivityTypeView : BaseViewController<SelectActivityTypeViewModel>
    {
        class CollectionView : ActivityCollectionViewController<SelectActivityTypeViewModel, ActivityTypeCollectionViewCell> {}
        readonly CollectionView _collectionView = new CollectionView();

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            _collectionView.DataContext = ViewModel;
            _collectionView.View.TranslatesAutoresizingMaskIntoConstraints = true;
            AddChildViewController(_collectionView);
            View.Add(_collectionView.View);

            _collectionView.CollectionView.SetCollectionViewLayout(new ActivityTypeCollectionViewLayout(), false);
            var set = this.CreateBindingSet<SelectActivityTypeView, SelectActivityTypeViewModel>();
            set.Bind(_collectionView.Source).To(vm => vm.Activities);
            set.Bind(_collectionView.Source).For(s => s.SelectionChangedCommand).To(vm => vm.ConfigureActivityCommand.Command);
            set.Apply();

            _collectionView.CollectionView.ReloadData();
        }

        public override void ViewDidAppear (bool animated)
        {
            _collectionView.Source.SelectedItemChanged += SelectedItemChanged;
            base.ViewDidAppear(animated);
        }

        public override void ViewDidDisappear (bool animated)
        {
            _collectionView.Source.SelectedItemChanged -= SelectedItemChanged;
            base.ViewDidDisappear(animated);
        }

        private void SelectedItemChanged(object sender, System.EventArgs e) {
            NSIndexPath[] index = _collectionView.CollectionView.GetIndexPathsForSelectedItems();
            _collectionView.CollectionView.SelectItem(index[0], true, UICollectionViewScrollPosition.CenteredHorizontally);
        }
    }
}

