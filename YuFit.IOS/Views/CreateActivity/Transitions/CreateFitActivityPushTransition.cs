using CoreGraphics;
using UIKit;
using YuFit.IOS.Presenters.Transitions;

namespace YuFit.IOS.Views.CreateActivity.Transitions
{
    public class CreateFitActivityPushTransition : CustomAnimatedTransition
    {
        public CreateFitActivityPushTransition (float duration) : base(duration) {}

        public override void AnimateTransition(IUIViewControllerContextTransitioning transitionContext)
        {
            if (transitionContext != null) {
                UIViewController toViewController = transitionContext.GetViewControllerForKey(UITransitionContext.ToViewControllerKey);
                UIViewController fromViewController = transitionContext.GetViewControllerForKey(UITransitionContext.FromViewControllerKey);

                transitionContext.ContainerView.AddSubview(toViewController.View);
                toViewController.View.Frame = fromViewController.View.Frame;
                toViewController.View.Center = new CGPoint(toViewController.View.Center.X, fromViewController.View.Center.Y + fromViewController.View.Frame.Height);

                UIView.AnimateNotify(TransitionDuration(transitionContext), 0.0d, UIViewAnimationOptions.CurveEaseOut, () => {
                    toViewController.View.Center = fromViewController.View.Center;
                }, finished => transitionContext.CompleteTransition(!transitionContext.TransitionWasCancelled));
            }
        }
    }
}


