using CoreGraphics;
using UIKit;
using YuFit.IOS.Presenters.Transitions;

namespace YuFit.IOS.Views.CreateActivity.Transitions
{
    public class CreateFitActivityPopTransition : CustomAnimatedTransition
    {
        public CreateFitActivityPopTransition (float duration) : base(duration) {}

        public override void AnimateTransition(IUIViewControllerContextTransitioning transitionContext)
        {
            if (transitionContext != null) {
                UIViewController toViewController = transitionContext.GetViewControllerForKey(UITransitionContext.ToViewControllerKey);
                UIViewController fromViewController = transitionContext.GetViewControllerForKey(UITransitionContext.FromViewControllerKey);
                transitionContext.ContainerView.AddSubview(toViewController.View);

                fromViewController.View.Layer.ZPosition = toViewController.View.Layer.ZPosition + 10;
                toViewController.View.Frame = fromViewController.View.Frame;
                toViewController.View.Center = new CGPoint(fromViewController.View.Center.X, fromViewController.View.Center.Y);
                toViewController.View.Layer.Opacity = 1.0f;

                UIView.AnimateNotify(TransitionDuration(transitionContext), 0.0d, UIViewAnimationOptions.CurveEaseIn, () => {
                    fromViewController.View.Center = new CGPoint(toViewController.View.Center.X, toViewController.View.Center.Y + toViewController.View.Frame.Height);
                }, finished => transitionContext.CompleteTransition(!transitionContext.TransitionWasCancelled));
            }
        }
    }
}

