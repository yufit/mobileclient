using System;

using System.Collections.Generic;
using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using CoreGraphics;
using Foundation;
using UIKit;

using YuFit.Core.ViewModels.CreateActivity;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;
using YuFit.IOS.Views.CreateActivity.Transitions;
using YuFit.IOS.Controls;

namespace YuFit.IOS.Views.CreateActivity
{
    [Attributes.PushTransition(typeof(CreateFitActivityPushTransition), Duration=0.25f)]
    [Attributes.PopTransition(typeof(CreateFitActivityPopTransition), Duration=0.25f)]
//    public class BaseCreateFitActivityView<T> : BaseCreateActivityViewController<CreateFitActivityViewModel>
    public class BaseCreateFitActivityView<T> : BaseCreateActivityViewController<T>
        where T : BaseCreateActivityViewModel
    {
        readonly UIScrollView                       _scrollView         = new UIScrollView();
        readonly CreateFitActivityContentView       _contentView        = new CreateFitActivityContentView();
        readonly CreateFitActifityDescriptionView   _descriptionView    = new CreateFitActifityDescriptionView();
        readonly CreateFitActivityButton            _fitActivityButton  = new CreateFitActivityButton();
        readonly DiamondShapeView                   _privateView        = new DiamondShapeView();
        readonly UILabel                            _privateLabel       = new UILabel();

        NSLayoutConstraint _descriptionHeightConstraint;

        private List<NSObject> _keyboardObservers = new List<NSObject>();
        CGPoint _previousOffset;

        string _description;
        public string ActivityDescription {
            get { return _description; }
            set { 
                _description = value;

                if (!_descriptionView.DescriptionLabel.IsFirstResponder) {
                    _descriptionView.RemoveConstraint(_descriptionHeightConstraint);
                    var _desiredHeight = _descriptionView.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
                    _descriptionHeightConstraint = _descriptionView.Height().EqualTo(_desiredHeight).ToLayoutConstraints().ToArray()[0];
                    _descriptionView.AddConstraint(_descriptionHeightConstraint);

                    View.SetNeedsUpdateConstraints();
                    View.LayoutIfNeeded();
                }
            }
        }

        bool _isPrivate;
        public bool IsPrivate {
            get { return _isPrivate; }
            set { _isPrivate = value; UpdatePrivateView(); }
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            SetupSubviews();
            SetupBindings();

            UpdateBar();
        }

        public override void ViewWillAppear (bool animated)
        {
            _keyboardObservers.Add(NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, HandleKeyboardWillShow));
            _keyboardObservers.Add(NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, HandleKeyboardWillHide));

            base.ViewWillAppear(animated);
        }

        public override void ViewWillDisappear (bool animated)
        {
            _keyboardObservers.ForEach(NSNotificationCenter.DefaultCenter.RemoveObserver);

            base.ViewWillDisappear(animated);
        }

        void SetupSubviews()
        {
            AddApplyView(false, true, true);

            SetupScrollView();
            SetupContentView();
            SetupDescriptionView();
            //SetupFitActivityButton();  // taken out as it looks like crap when creating activity from a group.
            SetupPrivateView();

            AddApplyViewConstraint();

            _scrollView.AddConstraints(
                _descriptionView.Bottom().EqualTo(-Spacing).BottomOf(_scrollView)
            );
        }

        void SetupScrollView ()
        {
            _scrollView.TranslatesAutoresizingMaskIntoConstraints = false;
            Add(_scrollView);

            View.AddConstraints(
                _scrollView.Top().EqualTo().TopOf(View),
                _scrollView.Bottom().EqualTo().TopOf(ApplyView),
                _scrollView.WithSameWidth(View),
                _scrollView.WithSameCenterX(View)
            );
        }

        void SetupContentView()
        {
            _contentView.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_contentView);

            View.AddConstraints(
                _contentView.WithSameWidth(View),
                _contentView.Height().EqualTo().WidthOf(View),
                _contentView.WithSameCenterX(View)
            );

            _scrollView.AddConstraints(
                _contentView.Top().EqualTo().TopOf(_scrollView)
            );
        }

        void SetupDescriptionView()
        {
            _descriptionView.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_descriptionView);

            var _desiredHeight = _descriptionView.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
            _descriptionHeightConstraint = _descriptionView.Height().EqualTo(_desiredHeight).ToLayoutConstraints().ToArray()[0];
            _descriptionView.AddConstraint(_descriptionHeightConstraint);

            View.AddConstraints(
                _descriptionView.Left().EqualTo(Spacing).LeftOf(View),
                _descriptionView.Right().EqualTo(-Spacing).RightOf(View)
            );
            _scrollView.AddConstraints(
                _descriptionView.Top().EqualTo(21.0f).BottomOf(_contentView)
            );
        }


        void SetupPrivateView()
        {
            _privateView.TranslatesAutoresizingMaskIntoConstraints = false;
            _privateLabel.TranslatesAutoresizingMaskIntoConstraints = false;

            _scrollView.Add(_privateLabel);
            _scrollView.Add(_privateView);

            _privateView.DiamondColor = YuFitStyleKit.Red;

            View.AddConstraints(
                _privateView.Width().EqualTo(46.0f),
                _privateView.Height().EqualTo().WidthOf(_privateView),
                _privateView.Right().EqualTo(-50.0f).RightOf(View),
                _privateView.Bottom().EqualTo().TopOf(_privateLabel),
                _privateLabel.Bottom().EqualTo(-20f).BottomOf(_contentView),
                _privateLabel.CenterX().EqualTo().CenterXOf(_privateView)
            );


            View.AddConstraints(
                
            );
            _privateLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 14);
        }

        void UpdatePrivateView()
        {
            if (IsPrivate) {
                _privateLabel.Text = ViewModel.PrivateText;
                _privateLabel.TextColor = YuFitStyleKit.Red;
                _privateView.DiamondColor = YuFitStyleKit.Red;
            } else {
                _privateLabel.Text = ViewModel.PublicText;
                _privateLabel.TextColor = YuFitStyleKit.Green;
                _privateView.DiamondColor = YuFitStyleKit.Green;
            }
            _privateView.SetNeedsDisplay();
        }

        void SetupFitActivityButton()
        {
            _fitActivityButton.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddSubview(_fitActivityButton);

            _fitActivityButton.AddConstraints(
                _fitActivityButton.Width().EqualTo(80.0f),
                _fitActivityButton.Height().EqualTo(40.0f)
            );

            View.AddConstraints(
                _fitActivityButton.CenterX().EqualTo().CenterXOf(View),
                _fitActivityButton.Bottom().EqualTo().TopOf(View)
            );
        }

        void SetupBindings()
        {
            var set = this.CreateBindingSet<BaseCreateFitActivityView<T>, CreateFitActivityViewModel>();
            BindActivityButton(set);
            BindWhenButton(set);
            BindWhereButton(set);
            BindWhomButton(set);

            set.Bind(_descriptionView.TitleLabel).To(vm => vm.Title);
            set.Bind(_descriptionView.DescriptionLabel).For(l => l.Data).To(vm => vm.Description);
            set.Bind(_descriptionView.DescriptionLabel).For(v => v.Placeholder).To(vm => vm.DescriptionPlaceholder);

//            set.Bind().For(me => me.IsEditing).To(vm => vm.IsEditing);
            set.Bind().For(me => me.ActivityDescription).To(vm => vm.Description);

            set.Bind(_privateView.Tap()).To(vm => vm.ToggleVisibilityCommand.Command);
            set.Bind().For(me => me.IsPrivate).To(vm => vm.Private);

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage);

            set.Apply();
        }

        void BindActivityButton (MvxFluentBindingDescriptionSet<BaseCreateFitActivityView<T>, CreateFitActivityViewModel> set)
        {
            set.Bind(_contentView.SelectActivityButton.Tap()).To(vm => vm.SelectActivityTypeCommand.Command);
            set.Bind(_contentView.SelectActivityButton).For(a => a.ActivityDisplayViewModel).To(vm => vm.ActivityDisplayViewModel);
        }

        void BindWhenButton (MvxFluentBindingDescriptionSet<BaseCreateFitActivityView<T>, CreateFitActivityViewModel> set)
        {
            set.Bind(_contentView.SelectWhenButton.Tap()).To(vm => vm.SelectTimeDateCommand.Command);
            set.Bind(_contentView.SelectWhenButton).For(b => b.StartTime).To(vm => vm.StartTime);
        }

        void BindWhereButton (MvxFluentBindingDescriptionSet<BaseCreateFitActivityView<T>, CreateFitActivityViewModel> set)
        {
            set.Bind(_contentView.SelectWhereButton.Tap()).To(vm => vm.SelectLocationCommand.Command);
            set.Bind(_contentView.SelectWhereButton).For(w => w.Where).To(vm => vm.Location);
        }

        void BindWhomButton (MvxFluentBindingDescriptionSet<BaseCreateFitActivityView<T>, CreateFitActivityViewModel> set)
        {
            set.Bind(_contentView.SelectWhomButton.Tap()).To(vm => vm.SelectParticipantsCommand.Command);
            set.Bind(_contentView.SelectWhomButton).For(w => w.Participants).To(vm => vm.Participants);
            set.Bind(_contentView.SelectWhomButton.CountLabel).To(vm => vm.ParticipantsCount);
            set.Bind(_contentView.SelectWhomButton.TopLabel).To(vm => vm.FriendText);
            set.Bind(_contentView.SelectWhomButton.BottomLabel).To(vm => vm.InvitedText);
        }

        void HandleKeyboardWillShow(NSNotification notification)
        {
            // Retrieve keyboard size information.
            NSDictionary info = notification.UserInfo;
            NSObject frameBeginInfo = info[UIKeyboard.FrameBeginUserInfoKey];
            CGSize kbSize = (frameBeginInfo as NSValue).RectangleFValue.Size;

            nfloat keyboardTop = View.Bounds.Height - kbSize.Height;

            _previousOffset = _scrollView.ContentOffset;

            _descriptionView.DescriptionLabel.ScrollEnabled = true;

            _descriptionView.RemoveConstraint(_descriptionHeightConstraint);
            _descriptionHeightConstraint = _descriptionView.Height().EqualTo(keyboardTop).ToLayoutConstraints().ToArray()[0];
            _descriptionView.AddConstraint(_descriptionHeightConstraint);

            View.SetNeedsUpdateConstraints();
            View.LayoutIfNeeded();

            Console.WriteLine("{0}", _descriptionView.Frame.Top);
            _scrollView.SetContentOffset(new CGPoint(0, _descriptionView.Frame.Top), true);
            _scrollView.ScrollEnabled = false;
        }

        void HandleKeyboardWillHide(NSNotification notification)
        {
            _descriptionView.DescriptionLabel.ScrollEnabled = false;


            _descriptionView.RemoveConstraint(_descriptionHeightConstraint);
            var _desiredHeight = _descriptionView.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
            _descriptionHeightConstraint = _descriptionView.Height().EqualTo(_desiredHeight).ToLayoutConstraints().ToArray()[0];
            _descriptionView.AddConstraint(_descriptionHeightConstraint);

            View.LayoutIfNeeded();
            _scrollView.SetContentOffset(_previousOffset, true);

            if (_scrollView.ContentSize.Height > View.Frame.Height) {
                _scrollView.SetContentOffset(_previousOffset, true);
            } else {
                _scrollView.SetContentOffset(new CGPoint(0.0f, 0.0f), true);
            }

            _scrollView.ScrollEnabled = true;
        }

        public override void MotionEnded (UIEventSubtype motion, UIEvent evt)
        {
//            if (evt.Subtype == UIEventSubtype.MotionShake && !IsEditing)
//            {
//                ViewModel.DeleteActivityCommand.Command.Execute(this);
//            }
            base.MotionEnded(motion, evt);
        }

        void UpdateBar ()
        {
        }

        protected override void OnPaletteUpdated (ActivityColors colors)
        {
            _contentView.SelectActivityButton.ActivityColors = colors;
            _contentView.SelectWhenButton.ActivityColors = colors;
            _contentView.SelectWhereButton.ActivityColors = colors;
            _contentView.SelectWhomButton.ActivityColors = colors;
            _descriptionView.ActivityColors = colors;
            base.OnPaletteUpdated(colors);
        }
    }

    public class CreateFitActivityView : BaseCreateFitActivityView<CreateFitActivityViewModel>
    {
        public CreateFitActivityView ()
        {
        }
    }

}

