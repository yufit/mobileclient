using System;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using UIKit;
using YuFit.IOS.Themes;
using YuFit.IOS.Utilities;

namespace YuFit.IOS.Views.CreateActivity
{
    [Register ("CreateFitActivityButton")]
    public class CreateFitActivityButton : UIButton
    {
        private UIBezierPath _buttonShapePath;
        private CAShapeLayer _diamondShapeLayer;

        public UIColor StrokeColor          { get; set; }
        public UIColor ButtonColor          { get; set; }
        public UIColor PlusBackgroundColor  { get; set; }
        public UIColor PlusForegroundColor  { get; set; }

        public CreateFitActivityButton ()
        {
            DoConstruction();
        }

        public CreateFitActivityButton(CGRect rect) : base(rect)
        {
            DoConstruction();
        }

        public CreateFitActivityButton(IntPtr handle) : base(handle) 
        {
            DoConstruction();
        }

        private void DoConstruction()
        {
            Opaque = false;
            BackgroundColor = UIColor.Clear;
            StrokeColor     = YuFitStyleKit.White;
            ButtonColor     = Themes.Dashboard.CreateFitActivityButton.BackgroundColor;
        }

        private void ComputeShapes()
        {
            CGRect frame = Bounds;
            _buttonShapePath = new UIBezierPath();
            _buttonShapePath.MoveTo(new CGPoint(frame.GetMinX() + 0.00000f * frame.Width, frame.GetMinY() + 1.00000f * frame.Height));
            _buttonShapePath.AddLineTo(new CGPoint(frame.GetMinX() + 0.50000f * frame.Width, frame.GetMinY() + 0.00000f * frame.Height));
            _buttonShapePath.AddLineTo(new CGPoint(frame.GetMinX() + 1.00000f * frame.Width, frame.GetMinY() + 1.00000f * frame.Height));
            _buttonShapePath.AddLineTo(new CGPoint(frame.GetMinX() + 0.00000f * frame.Width, frame.GetMinY() + 1.00000f * frame.Height));
            _buttonShapePath.ClosePath();

            _diamondShapeLayer = new CAShapeLayer();
            _diamondShapeLayer.Path = _buttonShapePath.CGPath;
            _diamondShapeLayer.FillColor = ButtonColor.CGColor;
            Layer.Mask = _diamondShapeLayer;
        }

        public override void Draw(CGRect rect)
        {
            ComputeShapes();

            using (new CGContextHelper()) {
                YuFitStyleKit.DrawCreatePlusButton(Bounds);
            }
        }

        public override UIView HitTest (CGPoint point, UIEvent uievent)
        {
            CGPoint layerPoint = Layer.ConvertPointToLayer(point, _diamondShapeLayer);
            return _diamondShapeLayer.Path.ContainsPoint(layerPoint, true) ? this : null;
        }

    }
}

