using System;

using CoreGraphics;
using UIKit;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.CreateActivity.ConfigActivityButtons
{
    public class ConfigActivityTimeButton : BaseConfigActivityButton
    {
        #region Data Members

        private readonly WhenView _whenView = new WhenView();

        #endregion

        #region Properties

        public DateTime StartTime 
        {
            get { return _whenView.Date; }
            set { _whenView.Date = value; UpdateButton(); }
        }

        #endregion

        #region Construction / Destruction

        public ConfigActivityTimeButton () { DoConstruction(); }
        public ConfigActivityTimeButton (CGRect frame) : base(frame) { DoConstruction(); }

        void DoConstruction ()
        {
            _whenView.Date = DateTime.MinValue;
            _whenView.DayLabelFont = CrudFitEvent.WhenDiamond.DayLabelTextFont;
            _whenView.MonthAndDateLabelFont = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
            _whenView.TimeLabelFont = CrudFitEvent.WhenDiamond.TimeLabelTextFont;
            AddSubview(_whenView);
        }

        #endregion

        /// <summary>
        /// Lays out subviews.
        /// </summary>
        public override void LayoutSubviews ()
        {
            // make the "when" view take up half of the diamond's height.  This looks nice.
            CGRect whenFrame = new CGRect(Bounds.X, Bounds.Height / 4.0f, Bounds.Width, Bounds.Height);
            whenFrame.Height = whenFrame.Height / 2.0f;
            _whenView.Frame = Bounds;//whenFrame;

            base.LayoutSubviews();
        }

        private void UpdateButton()
        {
            if (_whenView.Date < DateTime.Now)  {
                DiamondButton.PlusLocation = PlusLocation.Left;
                DiamondButton.DefaultIconDrawMethod = YuFitStyleKitExtension.GetDrawingMethod("DrawWhenIcon");
                _whenView.Hidden = true;
            } else {
                DiamondButton.PlusLocation = PlusLocation.None;
                DiamondButton.DefaultIconDrawMethod = null;
                _whenView.Hidden = false;
            }

            DiamondButton.SetNeedsDisplay();
            OnPaletteUpdated();
        }

        protected override void OnPaletteUpdated ()
        {
            if (!IsFilledIn()) {
                DiamondButton.DiamondColor = YuFitStyleKit.Charcoal;
                DiamondButton.StrokeColor = ActivityColors.Base;
                DiamondButton.StrokeWidth = 2.0f;
                DiamondButton.PlusBackgroundColor = ActivityColors.Base;
                DiamondButton.PlusForegroundColor = YuFitStyleKit.White;
            } else  {
                DiamondButton.DiamondColor = ActivityColors.Base;
                DiamondButton.StrokeWidth = 0.0f;
            }

            DiamondButton.SetNeedsDisplay();
        }

        protected override bool IsFilledIn ()
        {
            return _whenView.Date > DateTime.Now;
        }

    }
}
