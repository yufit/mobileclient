using System;
using UIKit;
using CoreGraphics;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.CreateActivity.ConfigActivityButtons
{
    public abstract class BaseConfigActivityButton : UIView
    {
        private ActivityColors _activityColors;
        public ActivityColors ActivityColors { 
            get { return _activityColors; }
            set { _activityColors = value; OnPaletteUpdated(); } 
        }

        public ConfigActivityDiamondView DiamondButton { get; private set; }

        protected BaseConfigActivityButton () { DoConstruction(); }
        protected BaseConfigActivityButton (CGRect frame) : base(frame) { DoConstruction(); }

        void DoConstruction ()
        {
            BackgroundColor = UIColor.Clear;
            DiamondButton = new ConfigActivityDiamondView();
            Add(DiamondButton);
        }

        public override UIView HitTest (CGPoint point, UIEvent uievent)
        {
            return DiamondButton.HitTest(point, uievent);
        }

        public override void LayoutSubviews ()
        {
            DiamondButton.Frame = Bounds;
            base.LayoutSubviews();
        }

        protected abstract void OnPaletteUpdated();
        protected abstract bool IsFilledIn();
    }
}

