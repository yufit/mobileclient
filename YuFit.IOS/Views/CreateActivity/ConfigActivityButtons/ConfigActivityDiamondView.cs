using System;
using CoreGraphics;
using UIKit;
using YuFit.IOS.Controls;
using YuFit.IOS.Utilities;

namespace YuFit.IOS.Views.CreateActivity.ConfigActivityButtons
{
    public class ConfigActivityDiamondView : DiamondShapeView
    {
        public Action<CGRect, UIColor> DefaultIconDrawMethod { get; set; }

        public ConfigActivityDiamondView() : base() { Padding = 0.37f; }
        public ConfigActivityDiamondView(CGRect rect) : base(rect) { Padding = 0.37f; }

        public float Padding { get; set; }

        public override void Draw(CGRect rect)
        {
            base.Draw(Bounds);

            if (DefaultIconDrawMethod != null) {
                using (var ctx = new CGContextHelper(UIGraphics.GetCurrentContext())) {
                    CGRect imageRect = Bounds.Inset(Padding*Bounds.Width, Padding*Bounds.Height);
                    DefaultIconDrawMethod(imageRect, StrokeColor);
                }
            }
        }
    }
}

