using System;
using CoreGraphics;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using UIKit;
using YuFit.Core.Models;
using System.Linq;
using YuFit.Core.Extensions;

namespace YuFit.IOS.Views.CreateActivity.ConfigActivityButtons
{
    public class ConfigActivityWhereButton : BaseConfigActivityButton
    {
        readonly ActivityIconView   IconView     = new ActivityIconView();
        readonly UILabel            CityLabel    = new UILabel();
        readonly UILabel            CountryLabel = new UILabel();

        private Location _where;
        public Location Where {
            get { return _where; }
            set { _where = value; UpdateButton(); }
        }

        public ConfigActivityWhereButton ()
        {
            
        }

        public ConfigActivityWhereButton (CGRect frame) : base(frame)
        {
        }

        private void UpdateButton()
        {
            if (_where != null && _where.Coordinates == null) {
                DiamondButton.PlusLocation = PlusLocation.Right;
                DiamondButton.DefaultIconDrawMethod = YuFitStyleKitExtension.GetDrawingMethod("DrawWhereIcon");
            } else {
                DiamondButton.PlusLocation = PlusLocation.None;
                DiamondButton.DefaultIconDrawMethod = null;
                IconView.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawIconMap");
                AddSubviews(new UIView[] {IconView, CityLabel, CountryLabel});

                Subviews.Where(v => v is UILabel).ForEach(v => {
                    UILabel label = (UILabel) v;
                    label.BackgroundColor = UIColor.Clear;
                    label.TextAlignment = UITextAlignment.Center;
                    label.TextColor = CrudFitEvent.WhenDiamond.TextColor;
                });

                CityLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
                CountryLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
                CityLabel.Text = Where.City;
                if (!string.IsNullOrEmpty(Where.StateProvince)) {
                    CityLabel.Text += ", " + Where.StateProvince;
                }
                CountryLabel.Text = Where.Country;

                CityLabel.AdjustsFontSizeToFitWidth = true;
                CountryLabel.AdjustsFontSizeToFitWidth = true;
            }
            OnPaletteUpdated();
        }
        protected override void OnPaletteUpdated ()
        {
            if (_where != null && _where.Coordinates == null) {
                DiamondButton.DiamondColor = YuFitStyleKit.Charcoal;
                DiamondButton.StrokeColor = ActivityColors.Base;
                DiamondButton.StrokeWidth = 2.0f;
                DiamondButton.PlusBackgroundColor = ActivityColors.Base;
                DiamondButton.PlusForegroundColor = YuFitStyleKit.White;
            } else {
                DiamondButton.DiamondColor = ActivityColors.Base;
                DiamondButton.StrokeColor = YuFitStyleKit.White;
                DiamondButton.StrokeWidth = 0.0f;
            }
            DiamondButton.SetNeedsDisplay();
        }

        public override void LayoutSubviews ()
        {
            const float startY = 0.0f;

            CGRect helperRect = Bounds;
            helperRect.Height = Bounds.Height / 3.5f;
            helperRect.Width = Bounds.Width / 3.5f;
            helperRect.Y = Bounds.Height/2.0f - helperRect.Height - 2.0f;
            helperRect.X = (Bounds.Width/2.0f) - helperRect.Width / 2.0f;
            IconView.Frame = helperRect;

            float desiredHeight = 16;//(Bounds.Height/2.0f/3.0f);
            helperRect = Bounds;
            helperRect.Y = Bounds.Height / 2.0f + 2.0f;
            helperRect.Height = desiredHeight;
            helperRect.Width = Bounds.Width * 0.7f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            CityLabel.Frame = helperRect;

            helperRect.Y += desiredHeight;
            helperRect.Width = Bounds.Width * 0.45f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            CountryLabel.Frame = helperRect;

            base.LayoutSubviews();
        }

        protected override bool IsFilledIn ()
        {
            return _where != null;
        }
    }
}

