using CoreGraphics;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;
using YuFit.Core.Extensions;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using YuFit.Core.Models;
using System;

namespace YuFit.IOS.Views.CreateActivity.ConfigActivityButtons
{
    public class ConfigActivityWhomButton : BaseConfigActivityButton
    {
        public readonly UIView  LabelContainer = new UIView();
        public readonly UILabel CountLabel     = new UILabel();
        public readonly UILabel TopLabel       = new UILabel();
        public readonly UILabel BottomLabel    = new UILabel();

        private List<FitActivityParticipant> _participants;
        public List<FitActivityParticipant> Participants {
            get { return _participants; }
            set { _participants = value; UpdateButton(); }
        }

        public ConfigActivityWhomButton () { DoConstruction(); }
        public ConfigActivityWhomButton (CGRect frame) : base(frame) { DoConstruction(); }

        void DoConstruction()
        {
            LabelContainer.AddSubviews(new [] {CountLabel, TopLabel, BottomLabel});
            AddSubview(LabelContainer);

            LabelContainer.Subviews.Where(v => v is UILabel).ForEach(v => {
                UILabel label = (UILabel) v;
                label.BackgroundColor = UIColor.Clear;
                label.TextAlignment = UITextAlignment.Center;
                label.TextColor = CrudFitEvent.WhenDiamond.TextColor;
                label.AdjustsFontSizeToFitWidth = true;
            });

            CountLabel.Font = CrudFitEvent.WhenDiamond.DayLabelTextFont;
            TopLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
            BottomLabel.Font = CrudFitEvent.WhenDiamond.TimeLabelTextFont;
        }

        /// <Docs>Lays out subviews.</Docs>
        /// <summary>
        /// TODO : Change this so it is not based on magic numbers
        /// </summary>
        public override void LayoutSubviews ()
        {
            LabelContainer.Frame = Bounds;

            nfloat desiredHeight = (Bounds.Height/2.0f)/3.0f;
            nfloat startY = (Bounds.Height/2.0f) - (Bounds.Height/4.0f);

            CGRect helperRect = LabelContainer.Frame;
            helperRect.Height = desiredHeight + 3.0f;
            helperRect.Y = startY + 8.0f + 0.0f;
            CountLabel.Frame = helperRect;

            helperRect.Y = startY + desiredHeight + 2.0f;
            helperRect.Width = Bounds.Width * 0.7f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            TopLabel.Frame = helperRect;

            helperRect.Y = startY + (2 * desiredHeight) - 6.0f;
            helperRect.Width = Bounds.Width * 0.45f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            BottomLabel.Frame = helperRect;

            base.LayoutSubviews();
        }

        private void UpdateButton()
        {
            if (_participants == null || _participants.Count == 0) {
                DiamondButton.PlusLocation = PlusLocation.Bottom;
                DiamondButton.DefaultIconDrawMethod = YuFitStyleKitExtension.GetDrawingMethod("DrawWhomIcon");
                LabelContainer.Hidden = true;
            } else {
                DiamondButton.PlusLocation = PlusLocation.None;
                DiamondButton.DefaultIconDrawMethod = null;

//                CountLabel.Text = _participants.Count.ToString();
//                TopLabel.Text = "FRIENDS";
//                BottomLabel.Text = "INVITED";
                LabelContainer.Hidden = false;
            }
            DiamondButton.SetNeedsDisplay();
            OnPaletteUpdated();
        }

        protected override void OnPaletteUpdated ()
        {
            if (_participants == null || _participants.Count == 0) {
                DiamondButton.DiamondColor = YuFitStyleKit.Charcoal;
                DiamondButton.StrokeColor = ActivityColors.Dark;
                DiamondButton.StrokeWidth = 2.0f;
                DiamondButton.PlusBackgroundColor = ActivityColors.Dark;
                DiamondButton.PlusForegroundColor = YuFitStyleKit.White;
            } else {
                DiamondButton.DiamondColor = ActivityColors.Dark;
                DiamondButton.StrokeWidth = 0.0f;
            }
            DiamondButton.SetNeedsDisplay();
        }

        protected override bool IsFilledIn ()
        {
            return _participants != null && _participants.Count > 0;
        }
    }
}

