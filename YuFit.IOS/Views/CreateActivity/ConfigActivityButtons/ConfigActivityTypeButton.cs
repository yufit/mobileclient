using System;
using CoreGraphics;
using System.Linq;
using MvvmCross.Binding.BindingContext;
using UIKit;
using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.DisplayActivities;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using YuFit.IOS.Utilities;
using YuFit.Core.Models.Activities;

namespace YuFit.IOS.Views.CreateActivity.ConfigActivityButtons
{
    public class ConfigActivityTypeButton : BaseConfigActivityButton
    {
        private UIView detailView;

        private BaseActivityDisplayViewModelRaw _activityDisplayViewModel;
        public BaseActivityDisplayViewModelRaw ActivityDisplayViewModel {
            get { return _activityDisplayViewModel; }
            set { _activityDisplayViewModel = value; UpdateButton(); }
        }

        private string _activityType;
        public string ActivityType {
            get { return _activityType; }
            set { _activityType = value; UpdateButton(); }
        }

        public ConfigActivityTypeButton () { }
        public ConfigActivityTypeButton (CGRect frame) : base(frame) { }

        public override void LayoutSubviews ()
        {
            if (detailView != null) {
                detailView.Frame = Bounds;//new RectangleF(0, Bounds.Height/2.0f, Bounds.Width, Bounds.Height/2.0f);
                detailView.LayoutIfNeeded();
            }
            base.LayoutSubviews();
        }

        private void UpdateButton()
        {
            // TODO This should be done differently instead of checking which activity is enabled or not
            if (_activityDisplayViewModel == null) {
                DiamondButton.PlusLocation = PlusLocation.Top;
                DiamondButton.DefaultIconDrawMethod = YuFitStyleKitExtension.GetDrawingMethod("DrawWhatIcon");
                if (detailView != null) { detailView.RemoveFromSuperview(); }
                detailView = null;
                SetNeedsLayout();
            } else {
                DiamondButton.PlusLocation = PlusLocation.None;
                DiamondButton.DefaultIconDrawMethod = null;

                if (detailView != null) { detailView.RemoveFromSuperview(); }

                if (_activityDisplayViewModel is CyclingActivityDisplayViewModel) {
                    detailView = new CyclingActivitySummaryView(_activityDisplayViewModel);
                } else if (_activityDisplayViewModel is RunningActivityDisplayViewModel) {
                    detailView = new RunningActivitySummaryView(_activityDisplayViewModel);
                } else if (_activityDisplayViewModel is SwimmingActivityDisplayViewModel) {
                    detailView = new SwimmingActivitySummaryView(_activityDisplayViewModel);
                } else if (_activityDisplayViewModel is YogaActivityDisplayViewModel) {
                    detailView = new YogaActivitySummaryView(_activityDisplayViewModel);
                } else if (_activityDisplayViewModel is FitnessActivityDisplayViewModel) {
                    detailView = new FitnessActivitySummaryView(_activityDisplayViewModel);
                } else if (_activityDisplayViewModel is MountainsActivityDisplayViewModel) {
                    detailView = new MountainsActivitySummaryView(_activityDisplayViewModel);
                }

                AddSubview(detailView);
                SetNeedsLayout();
            }
            DiamondButton.SetNeedsDisplay();
            OnPaletteUpdated();
        }

        protected override void OnPaletteUpdated ()
        {
            if (!IsFilledIn()) {
                DiamondButton.DiamondColor = YuFitStyleKit.Charcoal;
                DiamondButton.StrokeColor = ActivityColors.Light;
                DiamondButton.StrokeWidth = 2.0f;
                DiamondButton.PlusBackgroundColor = ActivityColors.Light;
                DiamondButton.PlusForegroundColor = YuFitStyleKit.White;
            } else {
                DiamondButton.DiamondColor = ActivityColors.Light;
                DiamondButton.StrokeWidth = 0.0f;
            }
            DiamondButton.SetNeedsDisplay();
        }

        protected override bool IsFilledIn ()
        {
            return _activityDisplayViewModel != null;
        }
    }

    public class ActivitySummaryView : UIView, IMvxBindingContextOwner {
        protected readonly ActivityIconView IconView = new ActivityIconView();

        #region IMvxBindingContextOwner implementation

        public IMvxBindingContext BindingContext { get; set; }

        /// <summary>
        /// The data context for the current cell (the view model)
        /// </summary>
        public virtual object DataContext {
            get { return BindingContext.DataContext; }
            set { BindingContext.DataContext = value; }
        }

        #endregion

        public ActivitySummaryView (BaseActivityDisplayViewModelRaw activityViewModel) { 
            this.CreateBindingContext (); 
            DataContext = activityViewModel;
        }
    }

    public class CyclingActivitySummaryView : ActivitySummaryView {
        private readonly UILabel DurationLabel  = new UILabel();
        private readonly UILabel DistanceLabel  = new UILabel();

        public CyclingActivitySummaryView (BaseActivityDisplayViewModelRaw activityViewModel) : base(activityViewModel) 
        {
            AddSubviews(new UIView[] {IconView, DurationLabel, DistanceLabel});

            Subviews.Where(v => v is UILabel).ForEach(v => {
                UILabel label = (UILabel) v;
                label.BackgroundColor = UIColor.Clear;
                label.TextAlignment = UITextAlignment.Center;
                label.TextColor = CrudFitEvent.WhenDiamond.TextColor;
                label.AdjustsFontSizeToFitWidth = true;

            });

            DurationLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
            DistanceLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;

            var set = this.CreateBindingSet<CyclingActivitySummaryView, CyclingActivityDisplayViewModel>();
            set.Bind(DistanceLabel).To (vm => vm.Distance);
            set.Bind(DurationLabel).To (vm => vm.Duration);
            set.Bind(IconView).For(sv => sv.DrawIcon).To(vm => vm.ActivitySettings.Sport);
            set.Apply ();
        }

        public override void LayoutSubviews ()
        {
            const float startY = 0.0f;

            CGRect helperRect = Bounds;
            helperRect.Height = Bounds.Height / 3.5f;
            helperRect.Width = Bounds.Width / 3.5f;
            helperRect.Y = Bounds.Height/2.0f - helperRect.Height - 2.0f;
            helperRect.X = (Bounds.Width/2.0f) - helperRect.Width / 2.0f;
            IconView.Frame = helperRect;

            float desiredHeight = 16;//(Bounds.Height/2.0f/3.0f);
            helperRect = Bounds;
            helperRect.Y = Bounds.Height / 2.0f + 2.0f;
            helperRect.Height = desiredHeight;
            helperRect.Width = Bounds.Width * 0.7f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            DistanceLabel.Frame = helperRect;

            helperRect.Y += desiredHeight;
            helperRect.Width = Bounds.Width * 0.45f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            DurationLabel.Frame = helperRect;

            base.LayoutSubviews();
        }


    }

    public class RunningActivitySummaryView : ActivitySummaryView {
        private readonly UILabel DurationLabel     = new UILabel();
        private readonly UILabel DistanceLabel  = new UILabel();

        public RunningActivitySummaryView (BaseActivityDisplayViewModelRaw activityViewModel) : base(activityViewModel)
        {
            AddSubviews(new UIView[] {IconView, DurationLabel, DistanceLabel});

            Subviews.Where(v => v is UILabel).ForEach(v => {
                UILabel label = (UILabel) v;
                label.BackgroundColor = UIColor.Clear;
                label.TextAlignment = UITextAlignment.Center;
                label.TextColor = CrudFitEvent.WhenDiamond.TextColor;
                label.AdjustsFontSizeToFitWidth = true;
            });

            DurationLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
            DistanceLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;

            var set = this.CreateBindingSet<RunningActivitySummaryView, RunningActivityDisplayViewModel>();
            set.Bind(DistanceLabel).To (vm => vm.Distance);
            set.Bind(DurationLabel).To (vm => vm.Duration);
            set.Bind(IconView).For(sv => sv.DrawIcon).To(vm => vm.ActivitySettings.Sport);
            set.Apply ();
        }

        public override void LayoutSubviews ()
        {
            CGRect helperRect = Bounds;
            helperRect.Height = Bounds.Height / 3.5f;
            helperRect.Width = Bounds.Width / 3.5f;
            helperRect.Y = Bounds.Height/2.0f - helperRect.Height - 2.0f;
            helperRect.X = (Bounds.Width/2.0f) - helperRect.Width / 2.0f;
            IconView.Frame = helperRect;

            float desiredHeight = 16;//(Bounds.Height/2.0f/3.0f);
            helperRect = Bounds;
            helperRect.Y = Bounds.Height / 2.0f + 2.0f;
            helperRect.Height = desiredHeight;
            helperRect.Width = Bounds.Width * 0.7f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            DistanceLabel.Frame = helperRect;

            helperRect.Y += desiredHeight;
            helperRect.Width = Bounds.Width * 0.45f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            DurationLabel.Frame = helperRect;

            base.LayoutSubviews();
        }


    }

    public class MountainsActivitySummaryView : ActivitySummaryView {
        private readonly UILabel DurationLabel     = new UILabel();
        private readonly UILabel DistanceLabel  = new UILabel();

        public MountainsActivitySummaryView (BaseActivityDisplayViewModelRaw activityViewModel) : base(activityViewModel)
        {
            AddSubviews(new UIView[] {IconView, DurationLabel, DistanceLabel});

            Subviews.Where(v => v is UILabel).ForEach(v => {
                UILabel label = (UILabel) v;
                label.BackgroundColor = UIColor.Clear;
                label.TextAlignment = UITextAlignment.Center;
                label.TextColor = CrudFitEvent.WhenDiamond.TextColor;
                label.AdjustsFontSizeToFitWidth = true;
            });

            DurationLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
            DistanceLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;

            var set = this.CreateBindingSet<MountainsActivitySummaryView, MountainsActivityDisplayViewModel>();
            set.Bind(DistanceLabel).To (vm => vm.Distance);
            set.Bind(DurationLabel).To (vm => vm.Duration);
            set.Bind(IconView).For(sv => sv.DrawIcon).To(vm => vm.ActivitySettings.Sport);
            set.Apply ();
        }

        public override void LayoutSubviews ()
        {
            CGRect helperRect = Bounds;
            helperRect.Height = Bounds.Height / 3.5f;
            helperRect.Width = Bounds.Width / 3.5f;
            helperRect.Y = Bounds.Height/2.0f - helperRect.Height - 2.0f;
            helperRect.X = (Bounds.Width/2.0f) - helperRect.Width / 2.0f;
            IconView.Frame = helperRect;

            float desiredHeight = 16;//(Bounds.Height/2.0f/3.0f);
            helperRect = Bounds;
            helperRect.Y = Bounds.Height / 2.0f + 2.0f;
            helperRect.Height = desiredHeight;
            helperRect.Width = Bounds.Width * 0.7f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            DistanceLabel.Frame = helperRect;

            helperRect.Y += desiredHeight;
            helperRect.Width = Bounds.Width * 0.45f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            DurationLabel.Frame = helperRect;

            base.LayoutSubviews();
        }


    }
    public class SwimmingActivitySummaryView : ActivitySummaryView {
        private readonly UILabel DurationLabel  = new UILabel();
        private readonly UILabel DistanceLabel  = new UILabel();

        public SwimmingActivitySummaryView (BaseActivityDisplayViewModelRaw activityViewModel) : base(activityViewModel)
        {
            AddSubviews(new UIView[] {IconView, DurationLabel, DistanceLabel});

            Subviews.Where(v => v is UILabel).ForEach(v => {
                UILabel label = (UILabel) v;
                label.BackgroundColor = UIColor.Clear;
                label.TextAlignment = UITextAlignment.Center;
                label.TextColor = CrudFitEvent.WhenDiamond.TextColor;
                label.AdjustsFontSizeToFitWidth = true;
            });

            DurationLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
            DistanceLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;

            var set = this.CreateBindingSet<SwimmingActivitySummaryView, SwimmingActivityDisplayViewModel>();
            set.Bind(DistanceLabel).To (vm => vm.Distance);
            set.Bind(DurationLabel).To (vm => vm.Duration);
            set.Bind(IconView).For(sv => sv.DrawIcon).To(vm => vm.ActivitySettings.Sport);
            set.Apply ();
        }

        public override void LayoutSubviews ()
        {
            CGRect helperRect = Bounds;
            helperRect.Height = Bounds.Height / 3.5f;
            helperRect.Width = Bounds.Width / 3.5f;
            helperRect.Y = Bounds.Height/2.0f - helperRect.Height - 2.0f;
            helperRect.X = (Bounds.Width/2.0f) - helperRect.Width / 2.0f;
            IconView.Frame = helperRect;

            float desiredHeight = 16;//(Bounds.Height/2.0f/3.0f);
            helperRect = Bounds;
            helperRect.Y = Bounds.Height / 2.0f + 2.0f;
            helperRect.Height = desiredHeight;
            helperRect.Width = Bounds.Width * 0.7f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            DistanceLabel.Frame = helperRect;

            helperRect.Y += desiredHeight;
            helperRect.Width = Bounds.Width * 0.45f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            DurationLabel.Frame = helperRect;

            base.LayoutSubviews();
        }
    }

    public class YogaActivitySummaryView : ActivitySummaryView {
        private readonly UILabel DurationLabel     = new UILabel();
        private readonly UILabel DistanceLabel  = new UILabel();

        public YogaActivitySummaryView (BaseActivityDisplayViewModelRaw activityViewModel) : base(activityViewModel)
        {
            AddSubviews(new UIView[] {IconView, DurationLabel, DistanceLabel});

            Subviews.Where(v => v is UILabel).ForEach(v => {
                UILabel label = (UILabel) v;
                label.BackgroundColor = UIColor.Clear;
                label.TextAlignment = UITextAlignment.Center;
                label.TextColor = CrudFitEvent.WhenDiamond.TextColor;
                label.AdjustsFontSizeToFitWidth = true;
            });

            DurationLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
            DistanceLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;

            var set = this.CreateBindingSet<YogaActivitySummaryView, YogaActivityDisplayViewModel>();
            set.Bind(DistanceLabel).To (vm => vm.Distance);
            set.Bind(DurationLabel).To (vm => vm.Duration);
            set.Bind(IconView).For(sv => sv.DrawIcon).To(vm => vm.ActivitySettings.Sport);
            set.Apply ();
        }

        public override void LayoutSubviews ()
        {
            CGRect helperRect = Bounds;
            helperRect.Height = Bounds.Height / 3.5f;
            helperRect.Width = Bounds.Width / 3.5f;
            helperRect.Y = Bounds.Height/2.0f - helperRect.Height - 2.0f;
            helperRect.X = (Bounds.Width/2.0f) - helperRect.Width / 2.0f;
            IconView.Frame = helperRect;

            float desiredHeight = 16;//(Bounds.Height/2.0f/3.0f);
            helperRect = Bounds;
            helperRect.Y = Bounds.Height / 2.0f + 2.0f;
            helperRect.Height = desiredHeight;
            helperRect.Width = Bounds.Width * 0.7f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            DistanceLabel.Frame = helperRect;

            helperRect.Y += desiredHeight;
            helperRect.Width = Bounds.Width * 0.45f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            DurationLabel.Frame = helperRect;

            base.LayoutSubviews();
        }


    }
    public class FitnessActivitySummaryView : ActivitySummaryView {
        private readonly UILabel DurationLabel     = new UILabel();
        private readonly UILabel DistanceLabel  = new UILabel();

        public FitnessActivitySummaryView (BaseActivityDisplayViewModelRaw activityViewModel) : base(activityViewModel)
        {
            AddSubviews(new UIView[] {IconView, DurationLabel, DistanceLabel});

            Subviews.Where(v => v is UILabel).ForEach(v => {
                UILabel label = (UILabel) v;
                label.BackgroundColor = UIColor.Clear;
                label.TextAlignment = UITextAlignment.Center;
                label.TextColor = CrudFitEvent.WhenDiamond.TextColor;
                label.AdjustsFontSizeToFitWidth = true;
            });

            DurationLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
            DistanceLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;

            var set = this.CreateBindingSet<FitnessActivitySummaryView, FitnessActivityDisplayViewModel>();
            set.Bind(DistanceLabel).To (vm => vm.Distance);
            set.Bind(DurationLabel).To (vm => vm.Duration);
            set.Bind(IconView).For(sv => sv.DrawIcon).To(vm => vm.ActivitySettings.Sport);
            set.Apply ();
        }

        public override void LayoutSubviews ()
        {
            CGRect helperRect = Bounds;
            helperRect.Height = Bounds.Height / 3.5f;
            helperRect.Width = Bounds.Width / 3.5f;
            helperRect.Y = Bounds.Height/2.0f - helperRect.Height - 2.0f;
            helperRect.X = (Bounds.Width/2.0f) - helperRect.Width / 2.0f;
            IconView.Frame = helperRect;

            float desiredHeight = 16;//(Bounds.Height/2.0f/3.0f);
            helperRect = Bounds;
            helperRect.Y = Bounds.Height / 2.0f + 2.0f;
            helperRect.Height = desiredHeight;
            helperRect.Width = Bounds.Width * 0.7f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            DistanceLabel.Frame = helperRect;

            helperRect.Y += desiredHeight;
            helperRect.Width = Bounds.Width * 0.45f;
            helperRect.X = Bounds.Width/2.0f - helperRect.Width/2.0f;
            DurationLabel.Frame = helperRect;

            base.LayoutSubviews();
        }


    }
}

