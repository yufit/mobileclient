﻿using UIKit;
using MvvmCross.Binding.BindingContext;
using YuFit.Core.ViewModels.CreateActivity;

namespace YuFit.IOS.Views.CreateActivity
{
    public class EditFitActivityView : BaseCreateFitActivityView<EditFitActivityViewModel>
    {
        UIBarButtonItem _deleteButton;

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            _deleteButton = new UIBarButtonItem(UIBarButtonSystemItem.Trash);
            UITextAttributes attributes = new UITextAttributes();
            attributes.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 19);
            _deleteButton.SetTitleTextAttributes(attributes, UIControlState.Normal);
            NavigationItem.RightBarButtonItem = _deleteButton;
            SetupBindings();
        }

        void SetupBindings()
        {
            var set = this.CreateBindingSet<EditFitActivityView, EditFitActivityViewModel>();
            set.Bind(_deleteButton).To(vm => vm.DeleteActivityCommand.Command);
            set.Apply();
        }

    }
}

