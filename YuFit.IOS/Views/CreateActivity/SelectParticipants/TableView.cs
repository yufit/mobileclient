using UIKit;

using MvvmCross.Binding.BindingContext;

using YuFit.Core.ViewModels.CreateActivity;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Views.CreateActivity.SelectParticipants
{
    public class TableView : MvxTableViewController
    {
        protected new SelectParticipantsViewModel ViewModel { get { return (SelectParticipantsViewModel) base.ViewModel; } }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            TableView.BackgroundColor = UIColor.Clear;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.RowHeight = 60;

            var source = new TableViewSource<ParticipantTableViewCell>(TableView, ParticipantTableViewCell.Key);
            TableView.Source = source;

            var set = this.CreateBindingSet<TableView, SelectParticipantsViewModel>();
            set.Bind(source).To(vm => vm.YuFitFriends);
            set.Bind(source).For(s => s.SelectionChangedCommand).To(vm => vm.ParticipantSelectedCommand.Command);
            set.Apply();
        }

        public override void ViewDidDisappear (bool animated)
        {
            base.ViewDidDisappear(animated);

            if (TableView != null) {
                if (TableView.Source != null) {
                    TableView.Source.Dispose();
                    TableView.Source = null;
                }
                TableView.Dispose ();
            }
        }
    }
}

