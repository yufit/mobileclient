using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using YuFit.Core.Models;
using YuFit.Core.ViewModels;
using YuFit.Core.ViewModels.CreateActivity;

using YuFit.IOS.Controls.Buttons;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.CreateActivity
{
    public class BaseCreateActivityViewController<T> : BaseViewController<T>
        where T : NamedViewModel
    {

        public ActivityColors ColorPalette { get; private set; }

        private ActivityType _activityType;
        public ActivityType ActivityType {
            get { return _activityType; }
            set { _activityType = value; UpdatePalette(); }
        }

        public ApplyView ApplyView = new ApplyView();

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            var set = this.CreateBindingSet<BaseCreateActivityViewController<T>, BaseCreateActivityViewModel>();
            set.Bind(ApplyView.Tap()).To(vm => vm.SaveCommand.Command);
            set.Bind().For(me => me.ActivityType).To(vm => vm.ActivityType);
            set.Apply();
            UpdatePalette();
        }

        protected void AddApplyView(bool alwaysEnabled = true, bool showLabel = false, bool blur = false)
        {
            ApplyView.Layer.ZPosition = 2000;
            Add(ApplyView);

            ApplyView.Setup(showLabel, blur);

            var set = this.CreateBindingSet<BaseCreateActivityViewController<T>, BaseCreateActivityViewModel>();
            if (showLabel) {set.Bind(ApplyView.ApplyLabel).To(vm => vm.SaveCommand.DisplayName).WithConversion("ToUppercase");}
            set.Apply();

            if (alwaysEnabled) { 
                ApplyView.ApplyTriangle.PercentFill = 1.0f; 
                ApplyView.ApplyTriangle.ShowDownTriangle = true; 
            }
            ApplyView.ApplyLabel.Hidden = !showLabel;
        }

        private void UpdatePalette()
        {
            ActivityColors colors;
            if (_activityType != null && _activityType.Enabled) {
                colors = YuFitStyleKitExtension.ActivityBackgroundColors(_activityType.TypeName);
            } else {
                colors = YuFitStyleKitExtension.ActivityBackgroundColors("Cycling");
            }
            ApplyView.ApplyTriangle.TriangleBackgroundColor = YuFitStyleKit.Black;
            ApplyView.ApplyTriangle.TriangleForegroundColor = colors.Base;
            ApplyView.ApplyTriangle.SetNeedsDisplay();
            ColorPalette = colors;
            OnPaletteUpdated(colors);
        }

        protected virtual void OnPaletteUpdated(ActivityColors colors) {}

        protected void AddApplyViewConstraint()
        {
            ApplyView.TranslatesAutoresizingMaskIntoConstraints = false;
            ApplyView.AddConstraints(
                ApplyView.Height().EqualTo(ApplyView.ApplyLabel.Hidden?65.0f:90.0f)
            );
            View.AddConstraints(
                ApplyView.WithSameCenterX(View),
                ApplyView.WithSameWidth(View),
                ApplyView.Bottom().EqualTo().BottomOf(View)
            );
        }
    }
}

