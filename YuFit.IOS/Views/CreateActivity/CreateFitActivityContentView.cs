using CoreGraphics;
using UIKit;
using YuFit.IOS.Views.CreateActivity.ConfigActivityButtons;
using System;


namespace YuFit.IOS.Views.CreateActivity
{
    public class CreateFitActivityContentView : UIView
    {
        public readonly ConfigActivityTypeButton  SelectActivityButton  = new ConfigActivityTypeButton();
        public readonly ConfigActivityWhereButton SelectWhereButton     = new ConfigActivityWhereButton();
        public readonly ConfigActivityTimeButton  SelectWhenButton      = new ConfigActivityTimeButton();
        public readonly ConfigActivityWhomButton  SelectWhomButton      = new ConfigActivityWhomButton();

        public CreateFitActivityContentView ()
        {
            BackgroundColor = UIColor.Clear;

            UIView[] subviews = {
                SelectActivityButton, SelectWhenButton, 
                SelectWhereButton, SelectWhomButton
            };

            AddSubviews(subviews);
        }

        public override void LayoutSubviews ()
        {
            base.LayoutSubviews();

            nfloat maxHeight     = Bounds.Width < Bounds.Height ? Bounds.Width : Bounds.Height;

            nfloat buttonHeight  = (maxHeight - 40) / 2.0f;
            nfloat buttonWidth   = buttonHeight;
            nfloat buttonTop     = (Bounds.Height - (buttonHeight*2)) / 2.0f;

            SelectActivityButton.Frame  = new CGRect(Bounds.Width / 2.0f - buttonWidth/2, buttonTop, buttonWidth, buttonWidth);
            SelectWhereButton.Frame     = new CGRect(SelectActivityButton.Frame.Right - buttonWidth/2, SelectActivityButton.Frame.Height/2.0f + buttonTop, buttonWidth, buttonWidth);
            SelectWhenButton.Frame      = new CGRect(SelectActivityButton.Frame.X - buttonWidth/2, SelectActivityButton.Frame.Height/2.0f + buttonTop, buttonWidth, buttonWidth);
            SelectWhomButton.Frame      = new CGRect(SelectActivityButton.Frame.X, SelectActivityButton.Frame.Height + buttonTop, buttonWidth, buttonWidth);
        }
    }
}

