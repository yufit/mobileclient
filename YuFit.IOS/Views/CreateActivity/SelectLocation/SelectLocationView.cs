using System;
using CoreGraphics;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using Coc.MvvmCross.Plugins.Location;

using CoreLocation;
using UIKit;
using MapKit;

using YuFit.Core.ViewModels.CreateActivity;
using YuFit.IOS.Extensions;
using YuFit.IOS.Utilities;
using YuFit.IOS.Views.CreateActivity.Transitions;
using YuFit.IOS.Controls.Annotations;

namespace YuFit.IOS.Views.CreateActivity.SelectLocation
{
    /// <summary>
    /// Select location view.  This is the view that a user use to select where
    /// his/her activity is to take place.  The activity can be  selected via a
    /// long press on the map, or by entering a valid address in the search bar.
    /// </summary>
    [Attributes.PushTransition(typeof(CreateFitActivityPushTransition), Duration=0.25f)]
    [Attributes.PopTransition(typeof(CreateFitActivityPopTransition), Duration=0.25f)]
    public partial class SelectLocationView : BaseCreateActivityViewController<SelectLocationViewModel>
    {

        #region Data members

        MapAnnotationManager _annotationManager;

        private readonly AnnotationViewHelper<MapAnnotation> _annotationViewHelper = new AnnotationViewHelper<MapAnnotation>();
        private readonly UISearchBar _searchBar = new UISearchBar();
        readonly MKMapView MapView = new MKMapView();

        UILongPressGestureRecognizer _selectLocationGesture;
        UITapGestureRecognizer _closeKeyboardGesture;

        #endregion

        #region Properties

        /// <summary>
        /// The  selected location.  We use  a local  property to bind with our
        /// view model.  It is a TwoWay binding. Doing a longpress in this view
        /// will update the selected location  in the  view model.  And doing a
        /// search will trigger the  view model to update the selected location
        /// to the found location if any.
        /// </summary>
        private CocLocation _selectedCoordinates;
        public CocLocation SelectedCoordinates {
            get { return _selectedCoordinates; }
            set { _selectedCoordinates = value; RefreshSelectedLocation(); }
        }
        public event EventHandler SelectedCoordinatesChanged;

        #endregion

        #region Methods

        #region UIView Pverrodes

        /// <summary>
        /// Called when the view did load.
        /// </summary>
        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            SetupMapView();
            SetupSubviews();
            SetupGestures();
            SetupBinding();
            SetupConstraints();
        }

        public override void ViewDidAppear (bool animated)
        {
            MapView.AddGestureRecognizer(_selectLocationGesture);
            MapView.AddGestureRecognizer(_closeKeyboardGesture);
            base.ViewDidAppear(animated);
        }

        public override void ViewDidDisappear (bool animated)
        {
            MapView.RemoveGestureRecognizer(_selectLocationGesture);
            MapView.RemoveGestureRecognizer(_closeKeyboardGesture);
            base.ViewDidDisappear(animated);
        }

        #endregion

        #region View Initialization

        private void SetupMapView()
        {
            View.Add(MapView);
            MapView.TranslatesAutoresizingMaskIntoConstraints = false;

            View.AddConstraints(
                MapView.WithSameWidth(View),
                MapView.WithSameHeight(View),
                MapView.WithSameCenterX(View),
                MapView.WithSameCenterY(View)
            );
            _annotationManager = new MapAnnotationManager(MapView);
        }

        /// <summary>
        /// Setups the subviews.
        /// </summary>
        private void SetupSubviews ()
        {
            Add(_searchBar);

            MapView.PitchEnabled = false;
            MapView.ShowsUserLocation = true;
            MapView.GetViewForAnnotation = _annotationViewHelper.GetViewForAnnotation;

            _searchBar.BarStyle = UIBarStyle.Black;
            _searchBar.SearchBarStyle = UISearchBarStyle.Default;

            _searchBar.Layer.Opacity = 0.8f;
            _searchBar.Layer.CornerRadius = 6.0f;
            _searchBar.ClipsToBounds = true;
            _searchBar.Layer.BorderWidth = 1.0f;
            _searchBar.KeyboardAppearance = UIKeyboardAppearance.Dark;

        }

        /// <summary>
        /// Setups the gestures.  We use a long tap as the mechanism to specify
        /// the activity selected location.
        /// TODO We should move the duration in a constant file somewhere.
        /// </summary>
        private void SetupGestures ()
        {
            _selectLocationGesture = new UILongPressGestureRecognizer(OnSelectLocation);
            _selectLocationGesture.MinimumPressDuration = 0.15f;

            _closeKeyboardGesture = new UITapGestureRecognizer(() => _searchBar.ResignFirstResponder());
        }

        /// <summary>
        /// Setups the binding.
        /// </summary>
        private void SetupBinding ()
        {
            var set = this.CreateBindingSet<SelectLocationView, SelectLocationViewModel>();
            set.Bind(MapView).For("Region").To(vm => vm.CurrentRegion).WithConversion("RegionToMKCoordinate");

            set.Bind().For(me => me.SelectedCoordinates).To(vm => vm.SelectedCoordinates).TwoWay();
            set.Bind(_annotationManager).For(my => my.ItemsSource).To(vm => vm.Annotations);

            set.Bind(_searchBar).To(vm => vm.SearchTerm);
            set.Bind(_searchBar).For("SearchButtonClicked").To(vm => vm.SearchCommand.Command);
            set.Apply();
        }

        /// <summary>
        /// Setups the auto layout constraints.  We are not setting  them up in
        /// the ui tool as it  is too  confusing.  We rely on the fluent layout
        /// extension from stuart lodge.
        /// </summary>
        private void SetupConstraints ()
        {
            View.RemoveConstraints(View.Constraints);

            MapView.TranslatesAutoresizingMaskIntoConstraints = false;
            _searchBar.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddConstraints(
                MapView.Height().EqualTo().HeightOf(View),
                MapView.Width().EqualTo().WidthOf(View),
                MapView.WithSameCenterX(View),
                MapView.WithSameCenterY(View),
                _searchBar.Left().EqualTo(5.0f).LeftOf(View),
                _searchBar.Right().EqualTo(-5.0f).RightOf(View),
                _searchBar.Top().EqualTo(5.0f).TopOf(View)
            );
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Called when the user taps for a long time on the map.  This selects
        /// the activity location.  We don't add the pin yet, we do it once the
        /// view model has been updated.
        /// </summary>
        /// <param name="touch">Touch.</param>
        void OnSelectLocation (UILongPressGestureRecognizer touch)
        {
            if (touch.State != UIGestureRecognizerState.Began) { return; }

            CGPoint userTouchLocation = touch.LocationInView(MapView);
            CLLocationCoordinate2D mapPoint = MapView.ConvertPoint(userTouchLocation, MapView);
            Console.WriteLine("{0}, {1}", mapPoint.Latitude, mapPoint.Longitude);
            SelectedCoordinates = new CocLocation { Coordinates = mapPoint.ToMvxCoordinates() };

            FireEvent(SelectedCoordinatesChanged);
        }

        /// <summary>
        /// Refreshs the selected location.  This gets called when the selected
        /// location in the view model gets  updated.  We remove any pin if any
        /// were visible and add the  new one.  This doesn't center on the pin,
        /// the viewmodel is responsible to set the map region accordingly.
        /// </summary>
        void RefreshSelectedLocation ()
        {
            _searchBar.ResignFirstResponder();
        }

        #endregion

        #endregion
    }
}