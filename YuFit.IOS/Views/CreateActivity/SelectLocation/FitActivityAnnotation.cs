//using CoreLocation;
//using MapKit;
//using UIKit;
//
//using YuFit.IOS.Interfaces;
//
//namespace YuFit.IOS.Views.CreateActivity.SelectLocation
//{
//    public class FitActivityAnnotation : MKAnnotation, IFitActivityAnnotation
//    {
//        private CLLocationCoordinate2D _coordinate;
//        public override CLLocationCoordinate2D Coordinate { get { return _coordinate; } }
//        public UIColor Color { get; set; }
//        public UIImage Image { get; set; }
//        public new string Title { get; set; }
//        public new string Subtitle { get; set; }
//        public string AvatarUrl { get; set; }
//
//        public FitActivityAnnotation(CLLocationCoordinate2D newCoordinate, UIColor color) {
//            _coordinate = newCoordinate;
//            Color = color;
//        }
//    }
//}
//
