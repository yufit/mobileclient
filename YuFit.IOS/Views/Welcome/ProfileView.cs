﻿
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Welcome;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Welcome
{
    class PublicInfoContainer : UIView
    {
        public readonly UILabel     NameLabel           = new UILabel();

        readonly UIView             _locationContainer  = new UIView();
        readonly IconView           _locationIcon       = new IconView();
        public readonly UILabel     AddressLabel        = new UILabel();

        public PublicInfoContainer ()
        {
            AddSubviews(new UIView[] {NameLabel, _locationContainer});
            Subviews.ForEach(v => { v.TranslatesAutoresizingMaskIntoConstraints = false; });

            this.AddConstraints(
                _locationContainer.WithSameCenterX(this),
                _locationContainer.Top().EqualTo(5.0f).CenterYOf(this),

                NameLabel.Bottom().EqualTo(0.0f).CenterYOf(this),
                NameLabel.WithSameCenterX(this),
                this.Bottom().EqualTo().BottomOf(_locationContainer)

            );

            ConfigureLocationContainer();

            NameLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 22);
            NameLabel.TextColor = YuFitStyleKit.White;
            AddressLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 20);
            AddressLabel.TextColor = YuFitStyleKit.Gray;
        }

        void ConfigureLocationContainer ()
        {
            _locationContainer.AddSubviews(new UIView[] {_locationIcon, AddressLabel});
            _locationContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            _locationIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawWhere1");

            _locationContainer.AddConstraints(
                _locationIcon.Left().EqualTo().LeftOf(_locationContainer),
                _locationIcon.WithSameCenterY(_locationContainer),
                _locationIcon.Height().EqualTo(-1.0f).HeightOf(AddressLabel),
                _locationIcon.Width().EqualTo(-1.0f).HeightOf(AddressLabel),

                AddressLabel.Left().EqualTo(5.0f).RightOf(_locationIcon),
                AddressLabel.CenterY().EqualTo(2.0f).CenterYOf(_locationContainer),

                _locationContainer.Right().EqualTo().RightOf(AddressLabel),
                _locationContainer.Bottom().EqualTo().BottomOf(_locationIcon)
            );
        }

    }

    public class ProfileView : BasePageView<ProfileViewModel>
    {
        readonly PictureContainer       _pictureContainer     = new PictureContainer();
        readonly PublicInfoContainer    _publicInfoContainer  = new PublicInfoContainer();
        readonly UILabel                _bioLabel             = new UILabel();

        private ProfileViewModel viewModel;
        public new ProfileViewModel ViewModel
        {
            get { return viewModel ?? (viewModel = base.ViewModel as ProfileViewModel); }
        }

        public ProfileView () : base(YuFitStyleKitExtension.GetColorGroup("Purple"), 1)
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            SetupPublicInfoContainer();
            SetupBioContainer();
            SetupPictureContainer();
            SetupBindings();
        }

        void SetupPictureContainer ()
        {
            _pictureContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            View.Add(_pictureContainer);
            _pictureContainer.StrokeWidth = 7.0f;
            _pictureContainer.StrokeColor = Colors.Base;

            _pictureContainer.AvatarImage.Image = UIImage.FromFile ("Media/AvatarVincent.jpg");

            View.AddConstraints(
                _pictureContainer.Top().EqualTo(25.0f).BottomOf(SubTitle3Label),
                _pictureContainer.Bottom().EqualTo(-25.0f).TopOf(_publicInfoContainer),
                _pictureContainer.WithSameCenterX(View),
                _pictureContainer.WithSameWidth(View)
            );
        }

        void SetupPublicInfoContainer ()
        {
            _publicInfoContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            View.Add(_publicInfoContainer);

            View.AddConstraints(
                _publicInfoContainer.Left().EqualTo(Spacing).LeftOf(View),
                _publicInfoContainer.Right().EqualTo(-Spacing).RightOf(View),
                _publicInfoContainer.Top().EqualTo(25.0f).CenterYOf(View)
            );
        }

        void SetupBioContainer()
        {
            _bioLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddSubviews(_bioLabel);

            _bioLabel.TextColor = YuFitStyleKit.Gray;
            _bioLabel.TextAlignment = UITextAlignment.Center;
            _bioLabel.Font = CrudFitEvent.WhenDiamond.TimeLabelTextFont;
            _bioLabel.Lines = 0;

            View.AddConstraints(
                _bioLabel.Top().EqualTo(25).BottomOf(_publicInfoContainer),
                _bioLabel.WithSameCenterX(View),
                _bioLabel.Width().EqualTo().WidthOf(View).WithMultiplier(0.6f)
           );
        }

        void SetupBindings()
        {
            var set = this.CreateBindingSet<ProfileView, ProfileViewModel>();
            set.Bind(_publicInfoContainer.NameLabel).To(vm => vm.RealName);
            set.Bind(_publicInfoContainer.AddressLabel).To(vm => vm.HomeAddress);
            set.Bind(_bioLabel).To(vm => vm.Bio);
            set.Apply ();
        }

    }
}

