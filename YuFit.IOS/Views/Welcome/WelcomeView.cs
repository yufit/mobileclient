﻿using System;
using System.Collections.Generic;
using MvvmCross.Binding.BindingContext;
using Coc.MvxAdvancedPresenter.Touch.Attributes;
using CoreGraphics;
using UIKit;
using YuFit.Core.ViewModels.Welcome;
using YuFit.IOS.Extensions;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Views.Welcome
{
    [Presenter(typeof(Coc.MvxAdvancedPresenter.Touch.SingleViewPresenter))]
    public class WelcomeView : BaseViewController<WelcomeViewModel>
    {
        readonly List<UIViewController> _pages = new List<UIViewController>();

        private UIScrollView scrollView;

        readonly UIPageControl pageControl = new UIPageControl
        {
            PageIndicatorTintColor = YuFit.IOS.Themes.YuFitStyleKit.Gray,
            CurrentPageIndicatorTintColor = YuFit.IOS.Themes.YuFitStyleKit.Yellow
        };

        public override bool PrefersStatusBarHidden ()
        {
            return true;
        }

        int _currentPage;
        public int CurrentPageIndex {
            get { return _currentPage; }
            set { Page = (nint) value; }
        }
        public event EventHandler CurrentPageIndexChanged;

        private nint page;
        public nint Page
        {
            get { return page; }
            set
            {
                pageControl.CurrentPage = value;
                page = value;
                scrollView.SetContentOffset(new CGPoint((value * UIScreen.MainScreen.Bounds.Width), 0), true);
            }
        }

        public WelcomeView()
        {
            IsRadialGradientVisible = false;
            ShowBackButton = false;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            pageControl.ValueChanged += HandlePageControlValueChanged;
            scrollView = new UIScrollView
            {
                ShowsHorizontalScrollIndicator = false,
                ShowsVerticalScrollIndicator = false,
                Bounces = true,
                PagingEnabled = true,
                Frame = View.Frame
            };

            scrollView.Scrolled += HandleScrollViewDecelerationEnded;
            pageControl.Frame = new CGRect(0, scrollView.Bounds.Bottom - 50, scrollView.Bounds.Width, 10);
            View.AddSubviews(scrollView, pageControl);

            int i;
            for (i = 0; i < ViewModel.Pages.Count; i++)
            {
                var pageViewController = CreatePage(ViewModel.Pages[i]);
                pageViewController.View.Frame = new CGRect(View.Bounds.Width * i, 0, View.Bounds.Width, View.Bounds.Height);
                scrollView.AddSubview(pageViewController.View);
            }

            scrollView.ContentSize = new CGSize(UIScreen.MainScreen.Bounds.Width * (i == 0 ? 1 : i), 0);
            pageControl.Pages = i;

            var currentPageView = _pages[(int)Page];
            pageControl.CurrentPageIndicatorTintColor = currentPageView.View.TintColor;

            var set = this.CreateBindingSet<WelcomeView, WelcomeViewModel>();
            set.Bind(View.Tap()).To(me => me.NextPageCommand.Command);
            set.Bind(this).For(me => me.CurrentPageIndex).To(vm => vm.CurrentPage).TwoWay();
            set.Apply();
        }

        public override void ViewWillDisappear (bool animated)
        {
            pageControl.ValueChanged -= HandlePageControlValueChanged;
            scrollView.Scrolled -= HandleScrollViewDecelerationEnded;
            base.ViewWillDisappear(animated);
        }

        /// <summary>
        /// Given a ViewModel create a UIViewController for the UIPageViewController
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        private UIViewController CreatePage(IMvxViewModel viewModel)
        {
            var screen = this.CreateViewControllerFor(viewModel) as UIViewController;
            _pages.Add(screen);
            return screen;
        }

        /// <summary>
        /// Handles when the user touches the pageControl
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void HandlePageControlValueChanged(object sender, EventArgs e)
        {
            Page = pageControl.CurrentPage;
        }

        /// <summary>
        /// Fires after the user has scrolled the scroll view
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        void HandleScrollViewDecelerationEnded(object sender, EventArgs e)
        {
            var pageScrolledTo = (int)Math.Floor((scrollView.ContentOffset.X - scrollView.Frame.Width / 2) / scrollView.Frame.Width) + 1;

            if (pageScrolledTo < _pages.Count)
            {
                page = pageScrolledTo;
                pageControl.CurrentPage = pageScrolledTo;

                var currentPageView = _pages[(int) Page];
                pageControl.CurrentPageIndicatorTintColor = currentPageView.View.TintColor;

                _currentPage = pageScrolledTo;
                CurrentPageIndexChanged(this, EventArgs.Empty);
            }
        }
    }
}

