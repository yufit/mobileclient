﻿using System;
using YuFit.Core.ViewModels.Welcome;
using YuFit.IOS.Themes;
using Fcaico.Controls.Calendar;
using Fcaico.iOS.Controls.TimePicker;
using UIKit;
using CoreGraphics;
using Cirrious.FluentLayouts.Touch;

namespace YuFit.IOS.Views.Welcome
{
    public class WhenView: BasePageView<WhenViewModel>
    {
        readonly UIView _container = new UIView();
        readonly CalendarView _calendar = new CalendarView();
        readonly TimePickerView _timePicker = new TimePickerView();

        public WhenView () : base(YuFitStyleKitExtension.GetColorGroup("Yellow"), 5) {}

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            SetupContainer();
            SetupCalendar();
            SetupTimePicker();
        }

        public static UIFont WeekendFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 16);
        public static UIFont WeekDayFont = UIFont.FromName("HelveticaNeueLTStd-ThCn", 16);
        public static UIFont MonthFont = UIFont.FromName("HelveticaNeueLTStd-ThCn", 16);
        public static UIFont DayNameFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 16);
        public static UIFont SelectionFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 16);
        public static UIFont TimeLabelFont = UIFont.FromName("HelveticaNeueLTStd-ThCn", 14);
        public static UIFont TimeFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 14);
        public static UIFont AmPmFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 24);

        private void SetupContainer()
        {
            View.Add(_container);
            _container.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddConstraints(
                _container.Width().EqualTo().WidthOf(View).WithMultiplier(.6f),
                _container.CenterX().EqualTo(15.0f).CenterXOf(View),
                _container.Top().EqualTo(10.0f).BottomOf(SubTitle3Label),
                _container.Bottom().EqualTo(-65.0f).BottomOf(View)
            );
        }

        private void SetupCalendar()
        {
            _calendar.BackgroundColor = UIColor.Clear;
            _calendar.UseDayInitials = true;
            _calendar.DisablePastDates = true;
            _calendar.HidePreviousAndNextMonthDays = true;
            _calendar.DayNamesColor = YuFitStyleKit.White;
            _calendar.DayNamesBackgroundColor = YuFitStyleKit.Yellow;
            _calendar.SelectionColor = YuFitStyleKit.Yellow;
            _calendar.RuleColor = YuFitStyleKit.Yellow;
            _calendar.MonthColor = YuFitStyleKit.White;
            _calendar.WeekDaysColor = YuFitStyleKit.White;
            _calendar.WeekendDaysColor = YuFitStyleKit.White;
            _calendar.DayNameFont = DayNameFont;
            _calendar.MonthFont = MonthFont;
            _calendar.WeekDayFont = WeekDayFont;
            _calendar.WeekendFont = WeekendFont;
            _calendar.SelectionFont = SelectionFont;

            _calendar.MonthFontBaselineOffset = CrudFitEvent.ActivityTimeView.MonthFontOffset;
            _calendar.DayNameFontBaselineOffset = CrudFitEvent.ActivityTimeView.DayNameFontOffset;
            _calendar.DayFontBaselineOffset = CrudFitEvent.ActivityTimeView.DayFontOffset;

            _calendar.PreviousMonthImage = YuFitStyleKit.ImageOfPreviousMonth(
                new CGRect(0, 0, 15, 15), YuFitStyleKit.Yellow);

            _calendar.NextMonthImage = YuFitStyleKit.ImageOfNextMonth(
                new CGRect(0, 0, 15, 15), YuFitStyleKit.Yellow);

            _calendar.TranslatesAutoresizingMaskIntoConstraints = false;
            _container.Add(_calendar);
            _container.AddConstraints(
                _calendar.Width().EqualTo().WidthOf(_container),
                _calendar.Height().EqualTo().HeightOf(_container).WithMultiplier(0.6f),
                _calendar.CenterX().EqualTo().CenterXOf(_container),
                _calendar.Top().EqualTo().TopOf(_container)
            );
            
        }

        private void SetupTimePicker()
        {
            _timePicker.Time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 
                DateTime.Now.AddHours(1).Hour, 0, 0);

            _timePicker.MinTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 
                DateTime.Now.AddHours(1).Hour, 0, 0);

            _timePicker.RuleColor = YuFitStyleKit.Yellow;
            _timePicker.TimeColor = YuFitStyleKit.White;
            _timePicker.AmPmColor = YuFitStyleKit.White;
            _timePicker.LabelFont = CrudFitEvent.ActivityTimeView.TimeLabelFont;
            _timePicker.TimeFont = CrudFitEvent.ActivityTimeView.TimeFont;
            _timePicker.AmPmFont = CrudFitEvent.ActivityTimeView.AmPmFont;
            _timePicker.TimeFontOffset = CrudFitEvent.ActivityTimeView.TimeFontOffset;

            _timePicker.BackImage = YuFitStyleKit.ImageOfDownArrow(
                new CGRect(0, 0, 30, 30), YuFitStyleKit.Yellow);

            _timePicker.ForwardImage = YuFitStyleKit.ImageOfUpArrow(
                new CGRect(0, 0, 30, 30), YuFitStyleKit.Yellow);

            _timePicker.TranslatesAutoresizingMaskIntoConstraints = false;
            _container.Add(_timePicker);

            _container.AddConstraints(
                _timePicker.Width().EqualTo().WidthOf(_container),
                _timePicker.Left().EqualTo().LeftOf(_container),
                _timePicker.Top().EqualTo(10.0f).BottomOf(_calendar),
                _timePicker.Bottom().EqualTo().BottomOf(_container)
            );
            
        }
    }
}

