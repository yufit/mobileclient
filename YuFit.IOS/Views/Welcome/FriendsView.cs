﻿using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Welcome;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using Foundation;
using MvvmCross.iOS.Views;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Views.Welcome
{

    class TableView : MvxTableViewController
    {
        protected new WelcomeFriendsViewModel ViewModel { get { return (WelcomeFriendsViewModel) base.ViewModel; } }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            TableView.BackgroundColor = UIColor.Clear;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.RowHeight = 60;
            TableView.ScrollEnabled = false;

            var source = new TableViewSource<ParticipantTableViewCell>(TableView, ParticipantTableViewCell.Key);
            TableView.Source = source;

            var set = this.CreateBindingSet<TableView, WelcomeFriendsViewModel>();
            set.Bind(source).To(vm => vm.YuFitFriends);
            set.Apply();
        }

        public override void ViewDidDisappear (bool animated)
        {
            base.ViewDidDisappear(animated);

            TableView.Source.Dispose();
            TableView.Source = null;

            TableView.Dispose ();
        }
    }

    public class ParticipantTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString ("ParticipantTableViewCell");

        readonly UILabel _nameLabel = new UILabel();
        readonly UILabel _locationLabel = new UILabel();
        readonly PictureContainer _pictureView = new PictureContainer(7.0f);
        readonly IconView _locationIcon = new IconView();
        readonly CheckBox _checkbox = new CheckBox();

        public bool IsSelected
        {
            get { return ((ParticipantViewModel)DataContext).IsSelected; }
            set {
                UIView.Animate(0.25f, () => {
                    UIColor green = YuFitStyleKit.Green;
                    _locationLabel.TextColor = value ? green : YuFitStyleKit.White;
                    _nameLabel.TextColor = value ? green : YuFitStyleKit.White;
                    _checkbox.IsSelected = value;
                    _locationIcon.StrokeColor = value ? green : YuFitStyleKit.White;
                    _pictureView.StrokeColor = value ? green : YuFitStyleKit.Pink;
                    _pictureView.SetNeedsDisplay();
                });
            }
        }

        public ParticipantTableViewCell ()
        {
            BackgroundColor = UIColor.Clear;
            SelectionStyle = UITableViewCellSelectionStyle.None;

            ContentView.AddSubviews(new UIView[] {_checkbox, _nameLabel, _locationIcon, _locationLabel, _pictureView});
            _locationIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawWhere1");
            _pictureView.TransparentBackground = true;

            SetupConstraints();

            this.DelayBind(() => {
                _nameLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 18);
                _nameLabel.TextColor = YuFitStyleKit.Yellow;
                _locationLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 14);
                _locationLabel.TextColor = YuFitStyleKit.Yellow;

                var set = this.CreateBindingSet<ParticipantTableViewCell, ParticipantViewModel>();
                set.Bind(_nameLabel).To(friend => friend.Name);
                set.Bind(_locationLabel).To(friend => friend.Location);
                set.Bind(this).For(me => me.IsSelected).To(friend => friend.IsSelected);
                set.Apply();
                _pictureView.AvatarImage.Image = UIImage.FromFile ("Media/" + ((ParticipantViewModel)DataContext).AvatarUrl);
            });
        }

        void SetupConstraints ()
        {
            ContentView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _locationIcon.AddConstraints(
                _locationIcon.Height().EqualTo(10.0f),
                _locationIcon.Width().EqualTo(10.0f)
            );

            _checkbox.AddConstraints(
                _checkbox.Height().EqualTo(13.0f),
                _checkbox.Width().EqualTo(19.0f)
            );

            ContentView.AddConstraints(
                _checkbox.WithSameCenterY(ContentView),
                _checkbox.Left().EqualTo(5.0f).LeftOf(ContentView),

                _pictureView.Height().EqualTo().HeightOf(ContentView),
                _pictureView.Width().EqualTo().HeightOf(ContentView),
                _pictureView.WithSameCenterY(ContentView),
                _pictureView.Left().EqualTo(6.0f).RightOf(_checkbox),

                _nameLabel.Left().EqualTo(10.0f).RightOf(_pictureView),
                _nameLabel.Right().EqualTo().RightOf(ContentView),
                _nameLabel.Bottom().EqualTo(2.0f).CenterYOf(ContentView),

                _locationIcon.Left().EqualTo(10.0f).RightOf(_pictureView),
                _locationIcon.Top().EqualTo(4.0f).CenterYOf(ContentView),

                _locationLabel.Left().EqualTo(5.0f).RightOf(_locationIcon),
                _locationLabel.Right().EqualTo().RightOf(ContentView),
                _locationLabel.Top().EqualTo(4.0f).CenterYOf(ContentView)
            );
        }
    }

    public class WelcomeFriendsView : BasePageView<WelcomeFriendsViewModel>
    {
        readonly UIView          _container         = new UIView();

        readonly SeparatorLine   _tableTopLine      = new SeparatorLine();
        readonly UIView          _tableContainer    = new UIView();
        readonly SeparatorLine   _tableBottomLine   = new SeparatorLine();
        readonly TableView       _tableView         = new TableView();

        public WelcomeFriendsView () : base(YuFitStyleKitExtension.GetColorGroup("Pink"), 4)
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            View.AddSubview(_container);
            _container.TranslatesAutoresizingMaskIntoConstraints = false;

            View.AddConstraints(
                _container.Top().EqualTo(25.0f).BottomOf(SubTitle3Label),
                _container.Left().EqualTo(25.0f).CenterXOf(WelcomeHalfDiamond),
                _container.Right().EqualTo(-25.0f).RightOf(View),
                _container.Bottom().EqualTo(-75.0f).BottomOf(View)
            );

            AddFirstLevelViews();
            AddTableView();
        }

        void AddFirstLevelViews ()
        {
            UIView[] firstLevelViews = {
                //_searchTopLine, _searchContainer, _searchBottomLine, 
                _tableTopLine, _tableContainer, _tableBottomLine
            };

            _container.AddSubviews(firstLevelViews);

            // Every views are 80% the width of the parent View
            // Every views are centered within the parent View 
            firstLevelViews.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;
                _container.AddConstraints(
                    v.WithSameCenterX(_container),
                    v.Width().EqualTo().WidthOf(_container)
                );
            });

            firstLevelViews.Where(v => v is SeparatorLine).ForEach(v => {
                v.AddConstraints(v.Height().EqualTo(1.5f));
                v.BackgroundColor = YuFitStyleKit.Pink;
            });

            firstLevelViews.Where(v => v is UILabel).ForEach(v => {
                (v as UILabel).Font = CrudFitEvent.LabelTextFont;
                (v as UILabel).TextColor = CrudFitEvent.LabelTextColor;
            });


            _container.AddConstraints(
                _tableTopLine.Top().EqualTo().TopOf(_container),
                _tableContainer.Top().EqualTo(5.0f).BottomOf(_tableTopLine),
                _tableContainer.Bottom().EqualTo(-5.0f).TopOf(_tableBottomLine),
                _tableBottomLine.Bottom().EqualTo().BottomOf(_container)
            );
        }

        private void AddTableView()
        {
            _tableView.BindingContext = BindingContext;
            AddChildViewController(_tableView);
            _tableContainer.AddSubview(_tableView.View);

            _tableView.View.TranslatesAutoresizingMaskIntoConstraints = false;
            _tableContainer.AddConstraints(
                _tableView.View.Top().EqualTo().TopOf(_tableContainer),
                _tableView.View.Left().EqualTo().LeftOf(_tableContainer),
                _tableView.View.Bottom().EqualTo().BottomOf(_tableContainer),
                _tableView.View.Right().EqualTo().RightOf(_tableContainer)
            );
        }

    }
}

