﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using UIKit;

using YuFit.Core.ViewModels.Welcome;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Welcome
{
    public class IntroView : BaseViewController<IntroViewModel>
    {
        readonly LogoView _logoView = new LogoView();
        readonly UILabel _welcomeLabel = new UILabel();

        public IntroView ()
        {
            IsRadialGradientVisible = false;
            ShowBackButton = false;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            View.BackgroundColor = UIColor.Black;
            View.TintColor = UIColor.White;

            SetupWelcomeLabel();
            SetupLogoView();

            var set = this.CreateBindingSet<IntroView, IntroViewModel>();
            set.Bind(_welcomeLabel).To(vm => vm.Messages);
            set.Apply();

        }

        void SetupLogoView ()
        {
            View.Add(_logoView);

            _logoView.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddConstraints(
                _logoView.Width().EqualTo().WidthOf(View).WithMultiplier(0.6f),
                _logoView.WithSameCenterX(View),
                _logoView.Top().EqualTo(35.0f).TopOf(View),
                _logoView.Bottom().EqualTo(-35.0f).TopOf(_welcomeLabel)
            );
        }

        void SetupWelcomeLabel ()
        {
            View.Add(_welcomeLabel);

            _welcomeLabel.Lines = 0;
            _welcomeLabel.TextColor = YuFitStyleKit.White;
            _welcomeLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 18);
            _welcomeLabel.TextAlignment = UITextAlignment.Center;

            _welcomeLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddConstraints(
                _welcomeLabel.Width().EqualTo().WidthOf(View).WithMultiplier(0.7f),
                _welcomeLabel.WithSameCenterX(View),
                _welcomeLabel.Top().EqualTo(10.0f).CenterYOf(View)
            );
        }

    }
}

