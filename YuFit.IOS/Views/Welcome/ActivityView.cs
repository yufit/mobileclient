﻿using System;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using CoreGraphics;

using Fcaico.Controls.ArrowSlider;

using UIKit;

using YuFit.Core.ViewModels.Welcome;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;

using ControlWhenView = YuFit.IOS.Controls.WhenView;

// THIS VIEW IS HAVING LAYOUT ISSUES.... PROBLEM IS RELATED WITH THE ARROWS
// THAT DOESN'T MEAN THE PROBLEM IS IN THE ARROWS LAYOUT THEMSELF.
//
// IF YOU REMOVE THE INSIDE VIEW CONSTRAINTS, IT IS MUCH HAPPIER, YOU WILL END
// UP WITH ONLY 4 CONSTRAINTS ISSUES.
//
// THE REMAINING 4 ISSUES ARE RELATED WITH THE WIDTH OF THE ARROW BEING DYNAMIC
// TO THE VIEW ITSELF.  NOT SURE WHY.

namespace YuFit.IOS.Views.Welcome
{
    sealed class InsideCropView : UIView
    {
        public readonly UIView InsideView = new UIView();
        public InsideCropView ()
        {
            AddSubview(InsideView);
            InsideView.TranslatesAutoresizingMaskIntoConstraints = false;

            this.AddConstraints(
//                InsideView.Right().EqualTo().RightOf(this),
//                InsideView.Top().EqualTo().TopOf(this),
//                InsideView.Left().EqualTo().LeftOf(this),
//                InsideView.Bottom().EqualTo().BottomOf(this)
                InsideView.Right().EqualTo(-2.5f).RightOf(this),
                InsideView.Top().EqualTo(2.5f).TopOf(this),
                InsideView.Left().EqualTo(2.5f).LeftOf(this),
                InsideView.Bottom().EqualTo(-2.5f).BottomOf(this)
            );
        }
    }

    class DiamondBoxesContainer : UIView
    {
        readonly InsideCropView             _detailsDiamond = new InsideCropView();
        readonly InsideCropView             _joinDiamond    = new InsideCropView();
        readonly InsideCropView             _goingDiamond   = new InsideCropView();
        readonly InsideCropView             _sliderContainer= new InsideCropView();

        public readonly UILabel             JoinLabel       = new UILabel();
        public readonly ControlWhenView     WhenBox         = new ControlWhenView();
        public readonly IconView            SportIcon       = new IconView();

        public readonly UILabel             GoingCountLabel = new UILabel();
        public readonly UILabel             GoingLabel      = new UILabel();
                                          
        public ArrowSliderView              TopSlider       = new ArrowSliderView(); 
        public ArrowSliderView              BottomSlider    = new ArrowSliderView();
                                          
        public UITextField                  TopLabel        = new UITextField();
        public UITextField                  BottomLabel     = new UITextField();


        public DiamondBoxesContainer ()
        {
            SetupDetailsDiamond();
            SetupJoinDiamond();
            SetupGoingDiamond();
            SetupSliderContainer();
        }

        void SetupSliderContainer ()
        {
            Add(_sliderContainer);
            _sliderContainer.TranslatesAutoresizingMaskIntoConstraints = false;

            this.AddConstraints(
                _sliderContainer.Width().EqualTo().WidthOf(this),
                _sliderContainer.Height().EqualTo().WidthOf(_sliderContainer).WithMultiplier(0.25f),
                _sliderContainer.Left().EqualTo().LeftOf(this),
                _sliderContainer.Bottom().EqualTo().TopOf(_detailsDiamond)
            );

            var sliderBlock = new UIView();
            sliderBlock.TranslatesAutoresizingMaskIntoConstraints = false;
            _sliderContainer.InsideView.AddSubview(sliderBlock);

            _sliderContainer.InsideView.AddConstraints(
                sliderBlock.Width().EqualTo().WidthOf(_sliderContainer.InsideView).WithMultiplier(0.66666666f),
                sliderBlock.Height().EqualTo().HeightOf(_sliderContainer.InsideView),
                sliderBlock.Left().EqualTo().LeftOf(_sliderContainer.InsideView),
                sliderBlock.Top().EqualTo().TopOf(_sliderContainer.InsideView)
            );

            TopSlider.TranslatesAutoresizingMaskIntoConstraints = false;
            BottomSlider.TranslatesAutoresizingMaskIntoConstraints = false;
            sliderBlock.AddSubview(TopSlider);
            sliderBlock.AddSubview(BottomSlider);

            sliderBlock.AddConstraints(
                TopSlider.Width().EqualTo().WidthOf(sliderBlock),
                TopSlider.Height().EqualTo().HeightOf(sliderBlock).WithMultiplier(0.5f),
                TopSlider.Left().EqualTo(-TopSlider.Padding - 1.0f).LeftOf(sliderBlock),
                TopSlider.Top().EqualTo(2.5f).TopOf(sliderBlock),

                BottomSlider.Width().EqualTo().WidthOf(sliderBlock),
                BottomSlider.Height().EqualTo().HeightOf(sliderBlock).WithMultiplier(0.5f),
                BottomSlider.Left().EqualTo(-TopSlider.Padding - 1.0f).LeftOf(sliderBlock),
                BottomSlider.Bottom().EqualTo(2.5f).BottomOf(sliderBlock)
            );

            var smallBlock = new UIView();
            smallBlock.TranslatesAutoresizingMaskIntoConstraints = false;
            _sliderContainer.InsideView.AddSubview(smallBlock);

            _sliderContainer.AddConstraints(
                smallBlock.Width().EqualTo().WidthOf(_sliderContainer.InsideView).WithMultiplier(0.3333333333f),
                smallBlock.Height().EqualTo().HeightOf(_sliderContainer.InsideView),
                smallBlock.Right().EqualTo().RightOf(_sliderContainer.InsideView),
                smallBlock.Top().EqualTo().TopOf(_sliderContainer.InsideView)
            );

            TopLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            BottomLabel.TranslatesAutoresizingMaskIntoConstraints = false;

            smallBlock.AddSubview(TopLabel);
            smallBlock.AddSubview(BottomLabel);

            smallBlock.AddConstraints(
                TopLabel.Width().EqualTo().WidthOf(smallBlock),
                TopLabel.Height().EqualTo().WidthOf(smallBlock).WithMultiplier(0.5f),
                TopLabel.Left().EqualTo(3.5f).LeftOf(smallBlock),
                TopLabel.Bottom().EqualTo(7.5f).CenterYOf(smallBlock),

                BottomLabel.Width().EqualTo().WidthOf(smallBlock),
                BottomLabel.Height().EqualTo().WidthOf(smallBlock).WithMultiplier(0.5f),
                BottomLabel.Left().EqualTo(3.5f).LeftOf(smallBlock),
                BottomLabel.Top().EqualTo(4.5f).CenterYOf(smallBlock)
            );

            TopLabel.VerticalAlignment = UIControlContentVerticalAlignment.Bottom;
            TopLabel.UserInteractionEnabled = false;
            TopLabel.AllowsEditingTextAttributes = false;

            BottomLabel.VerticalAlignment = UIControlContentVerticalAlignment.Top;
            BottomLabel.UserInteractionEnabled = false;
            BottomLabel.AllowsEditingTextAttributes = false;

            TopLabel.BackgroundColor = UIColor.Clear;
            TopLabel.TextColor = YuFitStyleKit.Red;
            TopLabel.Font = Themes.Dashboard.SummaryView.SpeedLabels.SpeedTextFont;

            BottomLabel.BackgroundColor = UIColor.Clear;
            BottomLabel.TextColor = YuFitStyleKit.White;
            BottomLabel.Font = Themes.Dashboard.SummaryView.SpeedLabels.SpeedUnitTextFont;


            TopSlider.IsEnabled = false;
            TopSlider.ArrowColor = YuFitStyleKit.Red;
            TopSlider.BackgroundColor = UIColor.Clear;
            TopSlider.Font = Themes.Dashboard.SummaryView.SliderTextFont;
            TopSlider.FontBaselineOffset = Themes.Dashboard.SummaryView.SliderTextOffset;

            BottomSlider.IsEnabled = false;
            BottomSlider.ArrowColor = YuFitStyleKit.Red;
            BottomSlider.BackgroundColor = UIColor.Clear;
            BottomSlider.Font = Themes.Dashboard.SummaryView.SliderTextFont;
            BottomSlider.FontBaselineOffset = Themes.Dashboard.SummaryView.SliderTextOffset;
        }

        void SetupGoingDiamond ()
        {
            Add(_goingDiamond);
            _goingDiamond.TranslatesAutoresizingMaskIntoConstraints = false;
            _goingDiamond.InsideView.BackgroundColor = UIColor.Clear;
            _goingDiamond.InsideView.Layer.BorderColor = YuFitStyleKit.Red.CGColor;
            _goingDiamond.InsideView.Layer.BorderWidth = 1.0f;

            var tmpContainer = new UIView();
            Add(tmpContainer);
            tmpContainer.TranslatesAutoresizingMaskIntoConstraints = false;

            this.AddConstraints(
                _goingDiamond.Width().EqualTo().WidthOf(this).WithMultiplier(0.3333333333f),
                _goingDiamond.Height().EqualTo().WidthOf(_goingDiamond),
                _goingDiamond.Left().EqualTo().RightOf(_detailsDiamond),
                _goingDiamond.Top().EqualTo().TopOf(_detailsDiamond),

                tmpContainer.WithSameWidth(_goingDiamond),
                tmpContainer.WithSameHeight(_goingDiamond),
                tmpContainer.WithSameCenterX(_goingDiamond),
                tmpContainer.WithSameCenterY(_goingDiamond)
            );

            tmpContainer.Add(GoingCountLabel);
            GoingCountLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            GoingCountLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 28);
            GoingCountLabel.TextColor = YuFitStyleKit.Red;

            tmpContainer.Add(GoingLabel);
            GoingLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            GoingLabel.Font =  UIFont.FromName("HelveticaNeueLTStd-LtCn", 18);
            GoingLabel.TextColor = YuFitStyleKit.White;

            tmpContainer.AddConstraints(
                GoingCountLabel.WithSameCenterX(tmpContainer),
                GoingCountLabel.Bottom().EqualTo(8.0f).CenterYOf(tmpContainer),

                GoingLabel.WithSameCenterX(tmpContainer),
                GoingLabel.Top().EqualTo(2.0f).CenterYOf(tmpContainer)
            );


            tmpContainer.Transform = CGAffineTransform.MakeRotation((float) (Math.PI/4.0f));
        }

        void SetupJoinDiamond ()
        {
            Add(_joinDiamond);
            _joinDiamond.TranslatesAutoresizingMaskIntoConstraints = false;
            _joinDiamond.InsideView.BackgroundColor = YuFitStyleKit.Red;

            Add(JoinLabel);
            JoinLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            JoinLabel.Font = Themes.Dashboard.SummaryView.JoinButton.TextFont;
            JoinLabel.TextColor = YuFitStyleKit.White;

            this.AddConstraints(
                _joinDiamond.Width().EqualTo().WidthOf(this).WithMultiplier(0.33333333333f),
                _joinDiamond.Height().EqualTo().WidthOf(_joinDiamond),
                _joinDiamond.Left().EqualTo().RightOf(_detailsDiamond),
                _joinDiamond.Bottom().EqualTo().BottomOf(_detailsDiamond),

                JoinLabel.WithSameCenterX(_joinDiamond),
                JoinLabel.WithSameCenterY(_joinDiamond)
            );

            JoinLabel.Transform = CGAffineTransform.MakeRotation((float) (Math.PI/4.0f));

        }

        void SetupDetailsDiamond ()
        {
            Add(_detailsDiamond);
            _detailsDiamond.TranslatesAutoresizingMaskIntoConstraints = false;
            _detailsDiamond.InsideView.BackgroundColor = YuFitStyleKit.Red;

            Add(WhenBox);
            WhenBox.TranslatesAutoresizingMaskIntoConstraints = false;
            WhenBox.DayLabelFont = UIFont.FromName("HelveticaNeueLTStd-BdCn", 22);
            WhenBox.MonthAndDateLabelFont = UIFont.FromName("HelveticaNeueLTStd-Cn", 18);
            WhenBox.TimeLabelFont = UIFont.FromName("HelveticaNeueLTStd-Cn", 18);

            Add(SportIcon);
            SportIcon.TranslatesAutoresizingMaskIntoConstraints = false;

            this.AddConstraints(
                _detailsDiamond.Width().EqualTo().WidthOf(this).WithMultiplier(0.666666666f),
                _detailsDiamond.Height().EqualTo().WidthOf(_detailsDiamond),
                _detailsDiamond.Left().EqualTo().LeftOf(this),
                _detailsDiamond.Bottom().EqualTo().BottomOf(this),

                WhenBox.Width().EqualTo().WidthOf(_detailsDiamond).WithMultiplier(0.5f),
                WhenBox.Height().EqualTo().WidthOf(WhenBox),
                WhenBox.Left().EqualTo(10.0f).LeftOf(_detailsDiamond),
                WhenBox.Bottom().EqualTo(-10.0f).BottomOf(_detailsDiamond),

                SportIcon.Width().EqualTo().WidthOf(_detailsDiamond).WithMultiplier(0.4f),
                SportIcon.Height().EqualTo().WidthOf(SportIcon),
                SportIcon.Right().EqualTo(-15.0f).RightOf(_detailsDiamond),
                SportIcon.Top().EqualTo(15.0f).TopOf(_detailsDiamond)

            );

            WhenBox.Transform = CGAffineTransform.MakeRotation((float) (Math.PI/4.0f));
            SportIcon.Transform = CGAffineTransform.MakeRotation((float) (Math.PI/4.0f));

        }
    }

    public sealed class ActivityView : BasePageView<ActivityViewModel>
    {
        readonly DiamondBoxesContainer _container = new DiamondBoxesContainer();

        public ActivityView () : base(YuFitStyleKitExtension.GetColorGroup("Red"), 2)
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            SetupDiamondBoxesContainer();
        }

        void SetupDiamondBoxesContainer() 
        {
            View.Add(_container);
            _container.TranslatesAutoresizingMaskIntoConstraints = false;

            View.AddConstraints(
                _container.Width().EqualTo().WidthOf(View).WithMultiplier(0.6f),
                _container.Height().EqualTo().WidthOf(_container),
                _container.WithSameCenterX(View),
                _container.Top().EqualTo(30.0f).BottomOf(SubTitle3Label)
            );

            _container.Transform = CGAffineTransform.MakeRotation((float) (-Math.PI/4.0f));

            _container.TopSlider.Values = ViewModel.DistanceValues;
            _container.BottomSlider.Values = ViewModel.DurationValues;

            var set = this.CreateBindingSet<ActivityView, ActivityViewModel>();
            set.Bind(_container.JoinLabel).To(me => me.JoinTitle);
            set.Bind(_container.GoingCountLabel).To(me => me.ParticipantCount);
            set.Bind(_container.GoingLabel).To(me => me.GoingTitle);
            set.Bind(_container.TopLabel).To(me => me.Speed);
            set.Bind(_container.BottomLabel).To(me => me.SpeedUnit);
            set.Bind(_container.TopSlider).For(slider => slider.CurrentValue).To(vm => vm.Distance);
            set.Bind(_container.BottomSlider).For(slider => slider.CurrentValue).To(vm => vm.Duration);
            set.Bind(_container.WhenBox).For("Date").To(me => me.StartTime);
            set.Apply();

            _container.SportIcon.DrawIcon = YuFitStyleKitExtension.ActivityDrawingMethod("Running");

        }
    }
}

