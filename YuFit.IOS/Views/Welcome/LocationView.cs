﻿using YuFit.Core.ViewModels.Welcome;
using YuFit.IOS.Themes;
using UIKit;
using CoreGraphics;
using Cirrious.FluentLayouts.Touch;

namespace YuFit.IOS.Views.Welcome
{
    public sealed class MapBackgroundView : UIView
    {
        public MapBackgroundView () 
        {
            BackgroundColor = UIColor.Clear;
        }

        public override void Draw (CGRect rect)
        {
            base.Draw(rect);

            YuFitStyleKit.DrawWelcomeLocation(Bounds, YuFitStyleKit.Aqua);
        }

    }
    public class LocationView: BasePageView<LocationViewModel>
    {
        readonly MapBackgroundView _mapView = new MapBackgroundView();
        public LocationView () : base(YuFitStyleKitExtension.GetColorGroup("Aqua"), 3)
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            View.Add(_mapView);

            _mapView.TranslatesAutoresizingMaskIntoConstraints = false;
            _mapView.AddConstraints(
                _mapView.Height().EqualTo().WidthOf(_mapView).WithMultiplier(1.20376176f)
            );

            View.AddConstraints(
                _mapView.Width().EqualTo().WidthOf(View),
                _mapView.WithSameCenterX(View),
                _mapView.Top().EqualTo(10.0f).CenterYOf(WelcomeHalfDiamond)
            );

        }
    }
}

