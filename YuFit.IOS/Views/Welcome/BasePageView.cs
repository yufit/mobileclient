﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using CoreGraphics;

using UIKit;

using YuFit.Core.ViewModels.Welcome;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Welcome
{

    public sealed class WelcomeHalfDiamond : UIView
    {
        public readonly UILabel NumberLabel = new UILabel();

        public ActivityColors Colors { get; private set; }
        public int PageNumber { get; private set; }

        public WelcomeHalfDiamond (ActivityColors colors, int pageNumber) 
        {
            BackgroundColor = UIColor.Clear;
            Colors = colors;
            PageNumber = pageNumber;

            NumberLabel.TextColor = colors.Dark;

            NumberLabel.Font = UIFont.FromName("HelveticaNeueLTStd-Cn", 116);
            NumberLabel.Text = pageNumber.ToString();
            NumberLabel.TranslatesAutoresizingMaskIntoConstraints = false;

            AddSubview(NumberLabel);

            this.AddConstraints(
                NumberLabel.CenterX().EqualTo(-15.0f).CenterXOf(this),
                NumberLabel.CenterY().EqualTo(15.0f).CenterYOf(this)
            );
        }

        public override void Draw (CGRect rect)
        {
            base.Draw(rect);

            YuFitStyleKit.DrawWelcomeHalfDiamond(Bounds, Colors.Base, Colors.Dark);
        }
    }


    public class BasePageView<T> : BaseViewController<T>
        where T : BasePageViewModel
    {
        protected readonly WelcomeHalfDiamond WelcomeHalfDiamond;
        protected readonly ActivityColors     Colors;
        protected readonly UILabel            TitleLabel = new UILabel();
        protected readonly UILabel            SubTitle1Label = new UILabel();
        protected readonly UILabel            SubTitle2Label = new UILabel();
        protected readonly UILabel            SubTitle3Label = new UILabel();

        public BasePageView(ActivityColors colors, int pageNumber)
        {
            IsRadialGradientVisible = false;
            ShowBackButton = false;
            Colors = colors;

            WelcomeHalfDiamond = new WelcomeHalfDiamond(colors, pageNumber);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            View.TintColor = Colors.Dark;
            View.BackgroundColor = YuFitStyleKit.Black;

            View.AddSubviews(new UIView[] {WelcomeHalfDiamond});

            WelcomeHalfDiamond.TranslatesAutoresizingMaskIntoConstraints = false;

            WelcomeHalfDiamond.AddConstraints(
                WelcomeHalfDiamond.Width().EqualTo().HeightOf(WelcomeHalfDiamond).WithMultiplier(0.5f)
            );

            View.AddConstraints(
                WelcomeHalfDiamond.Height().EqualTo().HeightOf(View).WithMultiplier(0.39f),
                WelcomeHalfDiamond.Top().EqualTo().TopOf(View),
                WelcomeHalfDiamond.Left().EqualTo().LeftOf(View)
            );

            SetupTitles();

            var set = this.CreateBindingSet<BasePageView<T>, T>();
            set.Bind(TitleLabel).To(vm => vm.DisplayName);
            set.Bind(SubTitle1Label).To(vm => vm.SubTitle1);
            set.Bind(SubTitle2Label).To(vm => vm.SubTitle2);
            set.Bind(SubTitle3Label).To(vm => vm.SubTitle3);
            set.Apply();
        }

        void SetupTitles()
        {
            View.AddSubviews(new UIView[] {TitleLabel, SubTitle1Label, SubTitle2Label, SubTitle3Label});

            TitleLabel.TextColor = Colors.Light;
            TitleLabel.Font = UIFont.FromName("HelveticaNeueLTStd-UltLtCn", 68);
            TitleLabel.AdjustsFontSizeToFitWidth = true;

            SubTitle1Label.TextColor = Colors.Light;
            SubTitle1Label.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 26);
            SubTitle1Label.AdjustsFontSizeToFitWidth = true;

            SubTitle2Label.TextColor = Colors.Light;
            SubTitle2Label.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 26);
            SubTitle2Label.AdjustsFontSizeToFitWidth = true;

            SubTitle3Label.TextColor = Colors.Light;
            SubTitle3Label.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 26);
            SubTitle3Label.AdjustsFontSizeToFitWidth = true;

            TitleLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            SubTitle1Label.TranslatesAutoresizingMaskIntoConstraints = false;
            SubTitle2Label.TranslatesAutoresizingMaskIntoConstraints = false;
            SubTitle3Label.TranslatesAutoresizingMaskIntoConstraints = false;

            View.AddConstraints(
                TitleLabel.Left().EqualTo(-2.0f).LeftOf(SubTitle1Label),
                TitleLabel.Right().EqualTo(-25.0f).RightOf(View),
                TitleLabel.Bottom().EqualTo(5).TopOf(SubTitle1Label),

                SubTitle1Label.Left().EqualTo(17.0f).RightOf(WelcomeHalfDiamond),
                SubTitle1Label.Right().EqualTo(-25.0f).RightOf(View),
                SubTitle1Label.CenterY().EqualTo(1.5f).CenterYOf(WelcomeHalfDiamond),

                SubTitle2Label.Left().EqualTo().LeftOf(SubTitle1Label),
                SubTitle2Label.Right().EqualTo(-25.0f).RightOf(View),
                SubTitle2Label.Top().EqualTo(1.5f).BottomOf(SubTitle1Label),

                SubTitle3Label.Left().EqualTo().LeftOf(SubTitle1Label),
                SubTitle3Label.Right().EqualTo(-25.0f).RightOf(View),
                SubTitle3Label.Top().EqualTo(1.5f).BottomOf(SubTitle2Label)
            );
        }

    }
}

