﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Welcome;

using YuFit.IOS.Controls;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Welcome
{
    public class OutroView : BaseViewController<OutroViewModel>
    {
        readonly LogoView       _logoView       = new LogoView();
        readonly UILabel        _label1         = new UILabel();
        readonly UILabel        _label2         = new UILabel();
        readonly UILabel        _label3         = new UILabel();
        readonly ApplyTriangle  _applyTriangle  = new ApplyTriangle();

        public OutroView ()
        {
            IsRadialGradientVisible = false;
            ShowBackButton = false;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            View.BackgroundColor = UIColor.Black;
            View.TintColor = YuFitStyleKit.Yellow;

            SetupWelcomeLabel();
            SetupApplyTriangle();
            SetupLogoView();

            var set = this.CreateBindingSet<OutroView, OutroViewModel>();
            set.Bind(View.Tap()).To(vm => vm.ContinueCommand.Command);
            set.Bind(_label1).To(vm => vm.Label1);
            set.Bind(_label2).To(vm => vm.Label2);
            set.Bind(_label3).To(vm => vm.Label3);
            set.Apply();
        }

        public override void ViewDidDisappear (bool animated)
        {
            View.GestureRecognizers.ForEach(View.RemoveGestureRecognizer);
            base.ViewDidDisappear(animated);
        }

        void SetupLogoView ()
        {
            View.Add(_logoView);

            _logoView.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddConstraints(
                _logoView.Width().EqualTo().WidthOf(View).WithMultiplier(0.6f),
                _logoView.WithSameCenterX(View),
                _logoView.Top().EqualTo(35.0f).TopOf(View),
                _logoView.Bottom().EqualTo(-35.0f).TopOf(_label1)
            );
        }

        void SetupWelcomeLabel ()
        {
            View.AddSubviews(new UIView[] {_label1, _label2, _label3});

            _label1.Lines = 0;
            _label1.TextColor = YuFitStyleKit.Yellow;
            _label1.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
            _label1.TextAlignment = UITextAlignment.Center;

            _label2.Lines = 0;
            _label2.TextColor = YuFitStyleKit.Yellow;
            _label2.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
            _label2.TextAlignment = UITextAlignment.Center;

            _label3.Lines = 0;
            _label3.TextColor = YuFitStyleKit.Yellow;
            _label3.Font = UIFont.FromName("HelveticaNeueLTStd-UltLtCn", 68);
            _label3.TextAlignment = UITextAlignment.Center;

            _label1.TranslatesAutoresizingMaskIntoConstraints = false;
            _label2.TranslatesAutoresizingMaskIntoConstraints = false;
            _label3.TranslatesAutoresizingMaskIntoConstraints = false;

            View.AddConstraints(
                _label1.WithSameCenterX(View),
                _label1.Bottom().EqualTo(5.0f).TopOf(_label2),

                _label2.WithSameCenterX(View),
                _label2.CenterY().EqualTo(-5.0f).CenterYOf(View),

                _label3.WithSameCenterX(View),
                _label3.Top().EqualTo(35.0f).BottomOf(_label2)
            );
        }

        void SetupApplyTriangle ()
        {
            View.Add(_applyTriangle);
            _applyTriangle.TranslatesAutoresizingMaskIntoConstraints = false;

            _applyTriangle.AddConstraints(
                _applyTriangle.Width().EqualTo(48),
                _applyTriangle.Height().EqualTo(25)
            );

            View.AddConstraints(
                _applyTriangle.CenterX().EqualTo().CenterXOf(View),
                _applyTriangle.Top().EqualTo(10.0f).BottomOf(_label3)
            );
        }
    }
}

