﻿using System;
using YuFit.IOS.Controls;
using UIKit;
using YuFit.Core.ViewModels;

namespace YuFit.IOS.Views.ViewInvitations
{
    public class InvitationsTableViewCellDelegate : MvxSwipeTableViewCellDelegate   
    { 
        private ViewInvitationsViewModel _viewModel;

        public InvitationsTableViewCellDelegate(UITableView tableView, ViewInvitationsViewModel viewModel) : base (tableView)
        {
            _viewModel = viewModel;
        }

        public override void DidTriggerRightUtilityButton (SWTableViewCell.SWTableViewCell cell, nint index)
        {
            InvitationTableViewCell invitation = cell as InvitationTableViewCell;

            if (cell == null)
            {
                return;
            }

            switch (index)
            {
                case 0:
                    _viewModel.ContextCommand1.Command.Execute(invitation.InvitationId);
                    cell.HideUtilityButtons(true);
                    break;

                case 1:
                    _viewModel.ContextCommand2.Command.Execute(invitation.InvitationId);
                    cell.HideUtilityButtons(true);
                    break;                   
            }
        }
    }
}

