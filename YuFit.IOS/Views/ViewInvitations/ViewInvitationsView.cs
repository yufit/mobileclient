using System;
using YuFit.Core.ViewModels;
using Cirrious.FluentLayouts.Touch;
using UIKit;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using YuFit.Core.Extensions;
using YuFit.IOS.Extensions;
using CoreGraphics;
using MvvmCross.Binding.BindingContext;

namespace YuFit.IOS.Views.ViewInvitations
{
    public class ViewInvitationsView : BaseViewController<ViewInvitationsViewModel>
	{
        private readonly UISegmentedControl _segmentedControl = new UISegmentedControl();
        private readonly UIView _invitationTableViewContainer = new UIView();
        private readonly InvitationsTableView  _invitationsTableView  = new InvitationsTableView();

        private UILabel _viewInformation = new UILabel();

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            View.BackgroundColor = UIColor.Clear;

            _invitationTableViewContainer.BackgroundColor = UIColor.Clear;

            SetupSubviews();
            SetupBindings();
        }

        void SetupSubviews()
        {
            SetupCategories();

            _invitationsTableView.BindingContext = BindingContext;
            View.AddSubview(_invitationTableViewContainer);

            UIView segmentView = new UIView();
            segmentView.AddSubview(_segmentedControl);
            _invitationTableViewContainer.AddSubview(segmentView);
            _segmentedControl.TranslatesAutoresizingMaskIntoConstraints = false;

            AddChildViewController(_invitationsTableView);
            _invitationTableViewContainer.AddSubview(_invitationsTableView.View);

            _invitationTableViewContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            _invitationsTableView.View.TranslatesAutoresizingMaskIntoConstraints = false;
            segmentView.TranslatesAutoresizingMaskIntoConstraints = false;

            View.AddConstraints(
                segmentView.Top().EqualTo().TopOf(_invitationTableViewContainer),
                segmentView.Left().EqualTo().LeftOf(_invitationTableViewContainer),
                segmentView.Right().EqualTo().RightOf(_invitationTableViewContainer),
                segmentView.Height().EqualTo(50f),
                _invitationsTableView.View.Top().EqualTo().BottomOf(segmentView),
                _invitationsTableView.View.Left().EqualTo().LeftOf(_invitationTableViewContainer),
                _invitationsTableView.View.Bottom().EqualTo().BottomOf(_invitationTableViewContainer),
                _invitationsTableView.View.Right().EqualTo().RightOf(_invitationTableViewContainer),
                _invitationTableViewContainer.WithSameHeight(View),
                _invitationTableViewContainer.WithSameWidth(View),
                _invitationTableViewContainer.WithSameTop(View),
                _invitationTableViewContainer.WithSameLeft(View));

            segmentView.AddConstraints(
                _segmentedControl.WithSameWidth(segmentView).Minus(30f),
                _segmentedControl.WithSameHeight(segmentView).Minus(20f),
                _segmentedControl.WithSameCenterX(segmentView),
                _segmentedControl.WithSameCenterY(segmentView));
            
            if (View.Frame.Width >= 375)
            {
                var emptyButton = new UIBarButtonItem(UIBarButtonSystemItem.Add);
                NavigationItem.RightBarButtonItem = emptyButton;
                emptyButton.TintColor = UIColor.Black;
            }

        }

        void SetupBindings()
        {
            var set = MvxBindingContextOwnerExtensions.CreateBindingSet<ViewInvitationsView, ViewInvitationsViewModel>(this);
            set.Bind(_segmentedControl).For("SelectedSegment").To(vm => vm.WhichInvitations);
            set.Bind(_viewInformation).To(vm => vm.InformationText);
            set.Apply ();
        }

        void SetupCategories()
        {
            int pos = 0;
            ViewModel.Categories.ForEach(category => _segmentedControl.InsertSegment(category, pos++, false));

            _segmentedControl.TintColor = Invitations.ViewInvitations.FilterControlColor;

            UITextAttributes attributes = new UITextAttributes();
            attributes.Font = Invitations.ViewInvitations.FilterControlFont;
            _segmentedControl.SetTitleTextAttributes(attributes, UIControlState.Normal);

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage);
        }
	}
}
