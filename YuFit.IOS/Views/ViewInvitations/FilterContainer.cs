﻿using System;
using UIKit;
using Cirrious.FluentLayouts.Touch;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.ViewInvitations
{
    public class FilterContainer: UIView
    {
        public readonly UISegmentedControl SegmentedControl = new UISegmentedControl();

        public FilterContainer ()
        {
            AddSubviews(new UIView[] {SegmentedControl});

            SegmentedControl.InsertSegment("Invitations", 0, false);
            SegmentedControl.InsertSegment("Around Me", 1, false);
            SegmentedControl.InsertSegment("Mine", 2, false);

            SegmentedControl.TranslatesAutoresizingMaskIntoConstraints = false;

            SegmentedControl.TintColor = Invitations.ViewInvitations.FilterControlColor;

            UITextAttributes attributes = new UITextAttributes();
            attributes.Font = Invitations.ViewInvitations.FilterControlFont;
            SegmentedControl.SetTitleTextAttributes(attributes, UIControlState.Normal);

            this.AddConstraints(
                SegmentedControl.WithSameHeight(this),
                SegmentedControl.WithSameCenterY(this),
                SegmentedControl.Left().EqualTo(0.0f).LeftOf(this),
                SegmentedControl.Right().EqualTo().RightOf(this)
            );

        }
    }
}

