﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Foundation;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.Models;
using YuFit.Core.ViewModels;

using YuFit.IOS.Controls;

namespace YuFit.IOS.Views.ViewInvitations
{
    public class InvitationTableViewCell : MvxSwipeTableViewCell
    {
        public static readonly NSString Key = new NSString("InvitationTableViewCell");

        readonly ActivitySummaryControlView Activity = new ActivitySummaryControlView();

        private InvitationTableItemViewModel ViewModel {
            get {
                return (InvitationTableItemViewModel) BindingContext.DataContext;
            }
        }


        public string InvitationId { get; set; }

        public ActivityType ActivityType
        {
            get { return Activity.ActivityType; }
            set { Activity.ActivityType = value; }
        }

        CancelledActivityView _cancelledView = new CancelledActivityView();

        private bool _isCanceled;
        public bool IsCanceled {
            get { return _isCanceled; }
            set { _isCanceled = value; ShowCanceledView(value); }
        }


        public InvitationTableViewCell ()
        {
            InvitationId = string.Empty;
            SelectionStyle = UITableViewCellSelectionStyle.None;
            BackgroundColor = UIColor.Black;
            ContentView.BackgroundColor = UIColor.Clear;
            ContentView.AddSubview(Activity);
            SetupConstraints();


            this.DelayBind(() => {

                // Analysis disable once InvokeAsExtensionMethod
                var set = MvxBindingContextOwnerExtensions.CreateBindingSet<InvitationTableViewCell, InvitationTableItemViewModel>(this);
                set.Bind().For(me => me.ActivityType).To(vm => vm.ActivityType);
                set.Bind(Activity).For(a => a.StartTime).To(vm => vm.When);
                set.Bind(Activity).For(a => a.StreetAddress).To(vm => vm.StreetAddress);
                set.Bind(Activity).For(a => a.SportDescription).To(vm => vm.DisplayName);
                set.Bind(Activity).For(a => a.Details).To(vm => vm.Details);
                set.Bind(Activity).For(a => a.CreatedByLabel).To(vm => vm.CreatedByLabel);
                set.Bind(Activity).For(a => a.CreatedBy).To(vm => vm.CreatedBy);
                set.Bind().For(me => me.InvitationId).To(vm => vm.InvitationId);
                set.Bind().For(me => me.IsCanceled).To(vm => vm.IsCanceled);
                set.Bind(_cancelledView.CancelText).To(vm => vm.CancelString);
                set.Apply ();
            });
        }

        void SetupConstraints ()
        {
            ContentView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            ContentView.AddConstraints(
                Activity.Top().EqualTo().TopOf(ContentView),
                Activity.Height().EqualTo(154),
                Activity.CenterX().EqualTo().CenterXOf(ContentView),
                Activity.Width().EqualTo().WidthOf(ContentView));
        }

        void ShowCanceledView (bool canceled)
        {
            if (canceled) {
                var container = new UIView();
                container.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0.4f);
                container.UserInteractionEnabled = true;
                ContentView.AddSubview(container);

                container.TranslatesAutoresizingMaskIntoConstraints = false;
                ContentView.AddConstraints(
                    container.WithSameHeight(ContentView),
                    container.WithSameCenterY(ContentView),
                    container.WithSameCenterX(ContentView),
                    container.WithSameWidth(ContentView)
                );

                container.SetNeedsUpdateConstraints();

                _cancelledView.Alpha = .8f;
                _cancelledView.UserInteractionEnabled = true;

                _cancelledView.TranslatesAutoresizingMaskIntoConstraints = false;
                container.AddSubview(_cancelledView);

                container.AddConstraints(
                    _cancelledView.Top().EqualTo(20.0f).TopOf(container),
                    _cancelledView.Height().EqualTo(114),
                    _cancelledView.CenterX().EqualTo().CenterXOf(container),
                    _cancelledView.Width().EqualTo().WidthOf(container));
                
                _cancelledView.SetNeedsUpdateConstraints();
            }
        }

    }
}

