﻿using MvvmCross.Binding.BindingContext;

using Foundation;

using SWTableViewCell;

using UIKit;

using YuFit.Core.ViewModels;
using YuFit.IOS.Themes;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Views.ViewInvitations
{
    public class InvitationsTableView : MvxTableViewController
    {
        protected new ViewInvitationsViewModel ViewModel { get { return (ViewInvitationsViewModel) base.ViewModel; } }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            TableView.BackgroundColor = UIColor.Clear;

            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.DoubleLineEtched;
            TableView.SeparatorColor = YuFitStyleKit.Black;

            TableView.RowHeight = 159;

            var source = new InvitationsTableViewSource<InvitationTableViewCell>(ViewModel, TableView, InvitationTableViewCell.Key);

            TableView.Source = source;

            var set = this.CreateBindingSet<InvitationsTableView, ViewInvitationsViewModel>();
            set.Bind(source).To(vm => vm.Invitations);
            set.Bind(source).For(s => s.SelectionChangedCommand).To(vm => vm.ShowActivitySummaryCommand.Command);
            set.Apply();
        }

        private UIButton[] RightButtons()
        {
            NSMutableArray rightUtilityButtons = new NSMutableArray();
            rightUtilityButtons.AddUtilityButton(Themes.Invitations.ViewInvitations.AcceptInvitationButtonColor, "Accept");
            rightUtilityButtons.AddUtilityButton(Themes.Invitations.ViewInvitations.IgnoreInvitationButtonColor, "Ignore");
            return NSArray.FromArray<UIButton>(rightUtilityButtons);
        }
    }
}

