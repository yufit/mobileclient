﻿using System;
using YuFit.IOS.Controls;
using UIKit;
using YuFit.Core.ViewModels;
using YuFit.Core.Interfaces;
using Foundation;
using SWTableViewCell;

namespace YuFit.IOS.Views.ViewInvitations
{
    public class InvitationsTableViewSource<TCell> : MvxSwipeTableViewSource<TCell> where TCell : UITableViewCell, new()
    {
        public ViewInvitationsViewModel ViewModel { get {
                return (ViewInvitationsViewModel) _viewModel;
            }
        }

        public InvitationsTableViewSource(BaseViewModel viewModel, UITableView tableView, string nibName)
            : base(viewModel, tableView, nibName)
        {
        }

        protected override MvxSwipeTableViewCellDelegate GetDelegate ()
        {
            return new InvitationsTableViewCellDelegate(_tableView, ViewModel);
        }

        protected override UIButton[] GetRightButtons ()
        {
            NSMutableArray rightUtilityButtons = new NSMutableArray();
            rightUtilityButtons.AddUtilityButton(Themes.Invitations.ViewInvitations.AcceptInvitationButtonColor, ViewModel.ContextCommand1.DisplayName);
            rightUtilityButtons.AddUtilityButton(Themes.Invitations.ViewInvitations.IgnoreInvitationButtonColor, ViewModel.ContextCommand2.DisplayName);
            return NSArray.FromArray<UIButton>(rightUtilityButtons);
        }

    }
}

