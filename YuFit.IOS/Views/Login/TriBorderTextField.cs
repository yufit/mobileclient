using System.Linq;
using Cirrious.FluentLayouts.Touch;
using UIKit;
using YuFit.Core.Extensions;
using YuFit.IOS.Controls;

namespace YuFit.IOS.Views.Login
{
    public class TriBorderTextField : UIView
    {
        public PlaceholderTextField TextField { get; private set; }
        protected readonly SeparatorLine      _bottomSeparator    = new SeparatorLine();
        protected readonly SeparatorLine      _leftSeparator      = new SeparatorLine();
        protected readonly SeparatorLine      _rightSeparator     = new SeparatorLine();

        public TriBorderTextField (bool secureEntry = false)
        {
            TextField = new PlaceholderTextField();
            AddSubviews(new UIView[] {TextField, _bottomSeparator, _leftSeparator, _rightSeparator});

            Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            Subviews.Where(v => v is SeparatorLine).ForEach(v => v.BackgroundColor = UIColor.FromRGBA(171.0f/255.0f, 91.0f/255.0f, 8.0f/255.0f, 1.0f));

            this.AddConstraints(
                TextField.WithSameCenterX(this),
                TextField.Top().EqualTo().TopOf(this),
                TextField.Left().EqualTo(12.0f).LeftOf(this),
                TextField.Right().EqualTo(-12.0f).RightOf(this),
                TextField.Height().EqualTo().HeightOf(this),

                _bottomSeparator.WithSameCenterX(this),
                _bottomSeparator.Top().EqualTo().BottomOf(this),
                _bottomSeparator.Width().EqualTo().WidthOf(this),
                _bottomSeparator.Height().EqualTo(0.5f),

                _rightSeparator.Bottom().EqualTo().TopOf(_bottomSeparator),
                _rightSeparator.Right().EqualTo().RightOf(_bottomSeparator),
                _rightSeparator.Width().EqualTo(0.5f),
                _rightSeparator.Height().EqualTo().HeightOf(this),

                _leftSeparator.Bottom().EqualTo().TopOf(_bottomSeparator),
                _leftSeparator.Left().EqualTo().LeftOf(_bottomSeparator),
                _leftSeparator.Width().EqualTo(0.5f),
                _leftSeparator.Height().EqualTo().HeightOf(this)
            );

            TextField.KeyboardAppearance        = UIKeyboardAppearance.Dark;
            TextField.ClearButtonMode           = UITextFieldViewMode.Always;
            TextField.ReturnKeyType             = UIReturnKeyType.Next;
            TextField.AutocorrectionType        = UITextAutocorrectionType.No;
            TextField.AutocapitalizationType    = UITextAutocapitalizationType.None;
            TextField.KeyboardType              = UIKeyboardType.Default;
            TextField.SecureTextEntry           = secureEntry;
        }
    }
    
}
