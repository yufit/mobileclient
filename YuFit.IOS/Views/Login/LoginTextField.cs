﻿using System;
using UIKit;
using YuFit.IOS.Themes;
using System.Linq;
using YuFit.IOS.Controls;
using YuFit.Core.Extensions;

namespace YuFit.IOS.Views.Login
{
    public class LoginTextField : TriBorderTextField
    {
        public LoginTextField (bool secure = false) : base(secure)
        {
            TextField.Font              = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
            TextField.TextColor         = YuFitStyleKit.Yellow;
            TextField.PlaceholderFont   = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
            TextField.PlaceholderColor  = YuFitStyleKit.Gray;

            Subviews.Where(v => v is SeparatorLine).ForEach(v => v.BackgroundColor = YuFitStyleKit.YellowDark);


        }
    }


}

