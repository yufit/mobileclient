﻿using System;
using System.Collections.Generic;
using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using CoreGraphics;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Login;

using YuFit.IOS.Controls;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;
using YuFit.IOS.Utilities;

namespace YuFit.IOS.Views.Login
{
    public class SignupTextField : TriBorderTextField
    {
        public SignupTextField (bool secure = false) : base(secure)
        {
            TextField.Font                      = UIFont.FromName("HelveticaNeueLTStd-LtCn", 14);
            TextField.TextColor                 = UIColor.FromRGBA(171.0f/255.0f, 91.0f/255.0f, 8.0f/255.0f, 1.0f);
            TextField.PlaceholderFont           = UIFont.FromName("HelveticaNeueLTStd-LtCn", 14);
            TextField.PlaceholderColor          = UIColor.FromRGBA(150.0f/255.0f, 105.0f/255.0f, 7.0f/255.0f, 1.0f);
        }
    }

    public class SignupView : BaseViewController<SignupViewModel>
    {
        readonly HorizontalGradientView   _topView              = new HorizontalGradientView();
        readonly HorizontalGradientView   _fillInfoContainer    = new HorizontalGradientView();

        readonly UILabel                  _signupTitleLabel     = new UILabel();
        readonly UILabel                  _fillThisLabel        = new UILabel();

        readonly SignupTextField          _nameTextField        = new SignupTextField();
        readonly SignupTextField          _emailTextField       = new SignupTextField();
        readonly SignupTextField          _passwordTextField    = new SignupTextField(true);
        readonly SignupTextField          _password2TextField   = new SignupTextField(true);

        readonly UIView                  _createMyAccountView   = new UIView();
        readonly UILabel                 _createMyAccountLabel  = new UILabel();
        readonly SeparatorLine           _createMyAccountLine   = new SeparatorLine();

        UITextFieldNavigator             _fieldNavigator;
        NSLayoutConstraint               _bottomConstraint;
        NSLayoutConstraint               _topConstraint;

        public SignupView ()
        {
            IsRadialGradientVisible = false;
            ShowNavBar = true;
            NavigationBar.TintColor = UIColor.FromRGBA(0.96f, 0.79f, 0.00f, 1.0f);
            WatchKeyboard = true;
        }

        #region View stuff

        /// <summary>
        /// Hide the status bar showing the battery status and time.
        /// </summary>
        /// <returns><c>true</c>, if status bar hidden was prefersed, <c>false</c> otherwise.</returns>
        public override bool PrefersStatusBarHidden ()
        {
            return true;
        }

        /// <summary>
        /// Setup our views, constraints and bindings.
        /// </summary>
        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            CreateViewHierarchy();
            ConfigureViewSpecifics();
            SetupConstraints();
            SetupKeyboard();
            SetupBindings();
        }

        public override void ViewWillAppear (bool animated)
        {
            _fieldNavigator.AttachKeyboardMonitoring();
            base.ViewWillAppear(animated);
        }

        /// <summary>
        /// Without this, the view is retained and end up leaking.
        /// </summary>
        /// <param name="animated">If set to <c>true</c> animated.</param>
        public override void ViewWillDisappear (bool animated)
        {
            _fieldNavigator.DetachKeyboardMonitoring();
            base.ViewWillDisappear(animated);
            NavigationBar.TintColor = YuFitStyleKit.Black;
        }

        void CreateViewHierarchy ()
        {
            View.AddSubviews(new UIView[] {_fillInfoContainer, _topView});
            _fillInfoContainer.AddSubviews(new UIView[] {_fillThisLabel, _password2TextField, _passwordTextField, _emailTextField, _nameTextField, _createMyAccountView});
            _createMyAccountView.AddSubviews(new UIView[] {_createMyAccountLine, _createMyAccountLabel});
            _topView.AddSubviews(new UIView[] {_signupTitleLabel});
        }

        void ConfigureViewSpecifics ()
        {
            // Our gradiant views.
            _topView.EndColor                       = UIColor.FromRGBA(0.96f, 0.79f, 0.00f, 1.0f);
            _topView.StartColor                     = UIColor.FromRGBA(0.93f, 0.73f, 0.00f, 1.0f);
            _fillInfoContainer.EndColor             = UIColor.FromRGBA(232.0f/255.0f, 174.0f/255.0f, 3.0f/255.0f, 1.0f);
            _fillInfoContainer.StartColor           = UIColor.FromRGBA(196.0f/255.0f, 149.0f/255.0f, 6.0f/255.0f, 1.0f);

            _signupTitleLabel.Font                  = UIFont.FromName("HelveticaNeueLTStd-UltLtCn", 96);
            _fillThisLabel.Font                     = UIFont.FromName("HelveticaNeueLTStd-UltLtCn", 66);
            _createMyAccountLabel.Font              = UIFont.FromName("HelveticaNeueLTStd-UltLtCn", 26);

            _signupTitleLabel.TextColor             = YuFitStyleKit.White;
            _fillThisLabel.TextColor                = YuFitStyleKit.White;
            _createMyAccountLabel.TextColor         = YuFitStyleKit.White;
            _createMyAccountView.BackgroundColor    = UIColor.FromRGBA(171.0f/255.0f, 91.0f/255.0f, 8.0f/255.0f, 1.0f);
            _createMyAccountLine.BackgroundColor    = UIColor.FromRGBA(159.0f/255.0f, 78.0f/255.0f, 8.0f/255.0f, 1.0f);

            _createMyAccountLabel.TextAlignment     = UITextAlignment.Center;

            _nameTextField.TextField.AutocapitalizationType      = UITextAutocapitalizationType.Words;
            _emailTextField.TextField.KeyboardType               = UIKeyboardType.EmailAddress;
            _password2TextField.TextField.ReturnKeyType          = UIReturnKeyType.Done;
        }

        #endregion

        #region Constraints

        void SetupConstraints ()
        {
            View.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            View.AddConstraints(
                _fillInfoContainer.Width().EqualTo().WidthOf(View),
                _fillInfoContainer.Height().EqualTo(428.0f),
                _fillInfoContainer.CenterX().EqualTo().CenterXOf(View),

                _topView.Width().EqualTo().WidthOf(View),
                _topView.Bottom().EqualTo().TopOf(_fillInfoContainer),
                _topView.CenterX().EqualTo().CenterXOf(View)
            );

            _bottomConstraint = _fillInfoContainer.Bottom().EqualTo().BottomOf(View).ToLayoutConstraints().ToArray()[0];
            _topConstraint = _topView.Top().EqualTo().TopOf(View).ToLayoutConstraints().ToArray()[0];

            View.AddConstraint(_topConstraint);
            View.AddConstraint(_bottomConstraint);

            SetupBottomViewConstraints();
            SetupTopViewConstraints();
        }

        void SetupCreateMyAccountViewConstraints ()
        {
            _createMyAccountView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _createMyAccountView.AddConstraints(
                _createMyAccountLine.Width().EqualTo().WidthOf(_createMyAccountView),
                _createMyAccountLine.CenterX().EqualTo().CenterXOf(_createMyAccountView),
                _createMyAccountLine.Bottom().EqualTo().BottomOf(_createMyAccountView),
                _createMyAccountLine.Height().EqualTo(3.0f),

                _createMyAccountLabel.Width().EqualTo().WidthOf(_createMyAccountView),
                _createMyAccountLabel.CenterX().EqualTo().CenterXOf(_createMyAccountView),
                _createMyAccountLabel.CenterY().EqualTo().CenterYOf(_createMyAccountView),
                _createMyAccountLabel.Height().EqualTo().HeightOf(_createMyAccountView)
            );
        }

        void SetupBottomViewConstraints ()
        {
            SetupCreateMyAccountViewConstraints();

            _fillInfoContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            _fillInfoContainer.AddConstraints(
                _createMyAccountView.Width().EqualTo(188.0f),
                _createMyAccountView.Height().EqualTo(36.0f),
                _createMyAccountView.CenterX().EqualTo().CenterXOf(_fillInfoContainer),
                _createMyAccountView.Bottom().EqualTo(-54.0f).BottomOf(_fillInfoContainer),

                _fillThisLabel.CenterX().EqualTo().CenterXOf(_fillInfoContainer),
                _fillThisLabel.Top().EqualTo(31).TopOf(_fillInfoContainer),

                _password2TextField.CenterX().EqualTo().CenterXOf(_fillInfoContainer),
                _password2TextField.Width().EqualTo(286),
                _password2TextField.Height().EqualTo(19.5f),
                _password2TextField.Bottom().EqualTo(-41.5f).TopOf(_createMyAccountView),

                _passwordTextField.CenterX().EqualTo().CenterXOf(_fillInfoContainer),
                _passwordTextField.Width().EqualTo(286),
                _passwordTextField.Height().EqualTo(19.5f),
                _passwordTextField.Bottom().EqualTo(-23.0f).TopOf(_password2TextField),

                _emailTextField.CenterX().EqualTo().CenterXOf(_fillInfoContainer),
                _emailTextField.Width().EqualTo(286),
                _emailTextField.Height().EqualTo(19.5f),
                _emailTextField.Bottom().EqualTo(-23.0f).TopOf(_passwordTextField),

                _nameTextField.CenterX().EqualTo().CenterXOf(_fillInfoContainer),
                _nameTextField.Width().EqualTo(286),
                _nameTextField.Height().EqualTo(19.5f),
                _nameTextField.Bottom().EqualTo(-23.0f).TopOf(_emailTextField)
            );
        }

        void SetupTopViewConstraints ()
        {
            _topView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _topView.AddConstraints(
                _signupTitleLabel.CenterY().EqualTo().CenterYOf(_topView),
                _signupTitleLabel.CenterX().EqualTo().CenterXOf(_topView)
            );
        }

        #endregion
            
        #region Keyboard

        void SetupKeyboard ()
        {
            _fieldNavigator = new UITextFieldNavigator(new List<UITextField> {
                _nameTextField.TextField, _emailTextField.TextField, _passwordTextField.TextField, _password2TextField.TextField
            });
        }
            
        protected override void KeyboardWillShow (nfloat keyboardTop, double animationSpeed)
        {
            base.KeyboardWillShow(keyboardTop, animationSpeed);
            ShowNavBar = false;

            var frame = _password2TextField.Frame;
            var convertedFrame = View.ConvertRectFromView(frame, _fillInfoContainer);

            nfloat passwordBottom = convertedFrame.Bottom + 5.0f;
            if (passwordBottom > keyboardTop)
            {
                _bottomConstraint.Constant = 0 - (passwordBottom - keyboardTop);
                _topConstraint.Constant = 0 - (passwordBottom - keyboardTop);
                View.SetNeedsUpdateConstraints();
                UIView.AnimateNotify(animationSpeed, 0.0f, UIViewAnimationOptions.CurveEaseInOut, View.LayoutIfNeeded, null);
            }
        }

        protected override void KeyboardWillHide (double animationSpeed)
        {
            base.KeyboardWillHide(animationSpeed);
            ShowNavBar = true;

            _bottomConstraint.Constant = 0;
            _topConstraint.Constant = 0;
            View.SetNeedsUpdateConstraints();
            UIView.AnimateNotify(animationSpeed, 0.0f, UIViewAnimationOptions.CurveEaseInOut, View.LayoutIfNeeded, null);
        }

        #endregion

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<SignupView, SignupViewModel>();
            set.Bind(_signupTitleLabel).To(vm => vm.SignUpTitleString);
            set.Bind(_fillThisLabel).To(vm => vm.FillThisString);

            set.Bind(_nameTextField.TextField).For("Placeholder").To(vm => vm.NameLabelPlaceholder);
            set.Bind(_nameTextField.TextField).To(vm => vm.Name);

            set.Bind(_emailTextField.TextField).For("Placeholder").To(vm => vm.EmailLabelPlaceholder);
            set.Bind(_emailTextField.TextField).To(vm => vm.Email);

            set.Bind(_passwordTextField.TextField).For("Placeholder").To(vm => vm.PasswordLabelPlaceholder);
            set.Bind(_passwordTextField.TextField).To(vm => vm.Password);

            set.Bind(_password2TextField.TextField).For("Placeholder").To(vm => vm.PasswordAgainLabelPlaceholder);
            set.Bind(_password2TextField.TextField).To(vm => vm.PasswordRepeat);

            set.Bind(_createMyAccountLabel).To(vm => vm.CreateMyAccountCommand.DisplayName);
            set.Bind(_createMyAccountView.Tap()).To(vm => vm.CreateMyAccountCommand.Command);
            set.Apply();
        }
    }
}

