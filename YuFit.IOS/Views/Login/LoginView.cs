﻿using System;
using System.Collections.Generic;
using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Coc.MvxAdvancedPresenter.Touch.Attributes;

using CoreGraphics;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Login;

using YuFit.IOS.Controls;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;
using YuFit.IOS.Presenters;
using YuFit.IOS.Utilities;

namespace YuFit.IOS.Views.Login
{
    [Presenter(typeof(LoginPresenter))]
    public class LoginView : BaseViewController<LoginViewModel>
    {
        readonly UIView             _topView                = new UIView();
        readonly LogoView           _yufitLogoView          = new LogoView();
        readonly UILabel            _logInLabel             = new UILabel();

        readonly UIView             _loginMethodContainer   = new UIView();
        readonly UIView             _loginLabelsContainer   = new UIView();
        readonly UIView             _iconContainer          = new UIView();
        readonly UIView             _signUpContainer        = new UIView();

        readonly IconView           _yufitIcon              = new IconView();
        readonly IconView           _facebookIcon           = new IconView();
//        readonly IconView           _twitterIcon            = new IconView();
//        readonly IconView           _instagramIcon          = new IconView();

        readonly UILabel            _loginMethodLabel       = new UILabel();

        readonly LoginTextField     _loginName              = new LoginTextField();
        readonly LoginTextField     _loginPassword          = new LoginTextField(true);

        readonly DiamondShapeView   _yellowBottomDiamond    = new DiamondShapeView();
        readonly UILabel            _goLabel                = new UILabel();

        readonly UIView             _bottomView             = new UIView();

        readonly UILabel            _orLabel                = new UILabel();
        readonly UILabel            _signUpLabel            = new UILabel();
        readonly UILabel            _ifYouDontHaveOneLabel  = new UILabel();

        readonly UILabel            _forgotPassword         = new UILabel();


        UITextFieldNavigator        _fieldNavigator;
        NSLayoutConstraint          _bottomConstraint;
        NSLayoutConstraint          _topConstraint;
        NSLayoutConstraint          _loginContainerConstraint;
        NSLayoutConstraint          _loginLabelContainerConstraint;

        public LoginView ()
        {
            ShowNavBar = false;
            WatchKeyboard = true;
        }

        private bool _yufitChosen = false;
        public bool YufitChosen 
        {
            get
            {
                return _yufitChosen;
            }
            set
            {
                nfloat labelContainerHeight = 0;
                if (value)
                {
                    labelContainerHeight = 155;
                }
                nfloat logincontainerHeight = labelContainerHeight + 150;

                _loginContainerConstraint.Constant = logincontainerHeight;
                _loginLabelContainerConstraint.Constant = labelContainerHeight;
                View.SetNeedsUpdateConstraints();

                if (_yufitChosen != value) {
                    UIView.AnimateNotify(.25f, 0.0f, UIViewAnimationOptions.CurveEaseInOut, View.LayoutIfNeeded, null);
                }
                _yufitChosen = value;
            }
        }
                

        #region View Lifecycle

        /// <summary>
        /// Hide the status bar showing the battery status and time.
        /// </summary>
        /// <returns><c>true</c>, if status bar hidden was prefersed, <c>false</c> otherwise.</returns>
        public override bool PrefersStatusBarHidden ()
        {
            return true;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            CreateViewHierarchy();
            ConfigureViewSpecifics();
            SetupConstraints();
            SetupKeyboard();
            SetupBindings();
        }

        public override void ViewWillAppear (bool animated)
        {
            if (_fieldNavigator != null) {
                _fieldNavigator.AttachKeyboardMonitoring();
            }
            base.ViewWillAppear(animated);
        }

        public override void ViewWillDisappear (bool animated)
        {
            if (_fieldNavigator != null) {
                _fieldNavigator.DetachKeyboardMonitoring();
            }
            base.ViewWillDisappear(animated);
        }

        void CreateViewHierarchy ()
        {
            View.AddSubviews(new UIView[] { _loginMethodContainer, _topView, _bottomView, _yellowBottomDiamond });
            _loginMethodContainer.AddSubviews(new UIView[] { _iconContainer, _loginMethodLabel, _loginLabelsContainer });
            _loginLabelsContainer.AddSubviews(new UIView[] { _loginName, _loginPassword, _signUpContainer, _forgotPassword });
            _signUpContainer.AddSubviews(new UIView[] { _orLabel, _signUpLabel, _ifYouDontHaveOneLabel });
            _iconContainer.AddSubviews(new UIView [] { _yufitIcon, _facebookIcon /*, _twitterIcon, _instagramIcon */ });
            _topView.AddSubviews(new UIView[] { _logInLabel, _yufitLogoView });
            _yellowBottomDiamond.AddSubviews(new UIView[] { _goLabel });
        }

        void ConfigureViewSpecifics ()
        {
            _loginMethodContainer.BackgroundColor = UIColor.Black;
            _topView.BackgroundColor = UIColor.Clear;
            _bottomView.BackgroundColor = UIColor.Clear;
            _yellowBottomDiamond.DiamondColor = YuFitStyleKit.YellowDark;
            _loginMethodLabel.TextColor = YuFitStyleKit.Gray;
            _goLabel.TextColor = YuFitStyleKit.YellowLight;
            _logInLabel.TextColor = YuFitStyleKit.YellowLight;
            _loginLabelsContainer.ClipsToBounds = true;

            _goLabel.TextColor = YuFitStyleKit.YellowLight;
            _orLabel.TextColor = YuFitStyleKit.Gray;
            _signUpLabel.TextColor = YuFitStyleKit.YellowDark;
            _ifYouDontHaveOneLabel.TextColor = YuFitStyleKit.Gray;
            _forgotPassword.TextColor = YuFitStyleKit.YellowDark;

            _logInLabel.Font = UIFont.FromName("HelveticaNeueLTStd-UltLtCn", 36);
            _loginMethodLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 17);

            _goLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 22);
            _orLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 17);
            _signUpLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 22);
            _goLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 22);
            _ifYouDontHaveOneLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 17);
            _forgotPassword.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 17);

            _loginMethodLabel.Lines = 0;
            _loginMethodLabel.LineBreakMode = UILineBreakMode.WordWrap;
            _loginMethodLabel.TextAlignment = UITextAlignment.Center;

            _yufitIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawYufit");
            _facebookIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawFacebook");
//            _twitterIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawTwitter");
//            _instagramIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawInstagram");

            _yellowBottomDiamond.Inset = 0.0f;
            _yufitLogoView.Inset = 0.0f;

            _loginName.TextField.KeyboardType = UIKeyboardType.EmailAddress;
            _loginPassword.TextField.ReturnKeyType = UIReturnKeyType.Done;
        }

        #endregion

        #region Constraints

        void SetupConstraints ()
        {
            View.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            View.AddConstraints(
                _loginMethodContainer.Bottom().EqualTo().TopOf(_bottomView),
                _loginMethodContainer.Width().EqualTo().WidthOf(View),
                _loginMethodContainer.CenterX().EqualTo().CenterXOf(View),

                _topView.Width().EqualTo().WidthOf(View),
                _topView.Bottom().EqualTo().TopOf(_loginMethodContainer),
                _topView.CenterX().EqualTo().CenterXOf(View),

                _bottomView.Width().EqualTo().WidthOf(View),
                _bottomView.Height().EqualTo(80.0f),
                _bottomView.CenterX().EqualTo().CenterXOf(View),

                _yellowBottomDiamond.CenterY().EqualTo().TopOf(_bottomView),
                _yellowBottomDiamond.CenterX().EqualTo().CenterXOf(_bottomView),
                _yellowBottomDiamond.Width().EqualTo(72.0f),
                _yellowBottomDiamond.Height().EqualTo().WidthOf(_yellowBottomDiamond)
            );


            _loginContainerConstraint = _loginMethodContainer.Height().EqualTo(150.0f).ToLayoutConstraints().ToArray()[0];
            _bottomConstraint = _bottomView.Bottom().EqualTo().BottomOf(View).ToLayoutConstraints().ToArray()[0];
            _topConstraint = _topView.Top().EqualTo().TopOf(View).ToLayoutConstraints().ToArray()[0];

            View.AddConstraint(_topConstraint);
            View.AddConstraint(_bottomConstraint);
            View.AddConstraint(_loginContainerConstraint);

            SetupLoginMethodContainerViewConstraints();
            SetupTopViewConstraints();
            SetupBottomViewConstraints();
            SetupYellowDiamondViewConstraints();
        }

        void SetupLoginMethodContainerViewConstraints ()
        {
            _loginMethodContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _loginMethodContainer.AddConstraints(
                _iconContainer.Top().EqualTo(20).TopOf(_loginMethodContainer),
                _iconContainer.Width().EqualTo().WidthOf(_loginMethodContainer).WithMultiplier(.80f),
                _iconContainer.WithSameCenterX(_loginMethodContainer),
                _iconContainer.Height().EqualTo(30.0f),

                _loginMethodLabel.WithSameCenterX(_loginMethodContainer),
                _loginMethodLabel.Width().EqualTo().WidthOf(_loginMethodContainer).WithMultiplier(.95f),
                _loginMethodLabel.Top().EqualTo(20).BottomOf(_iconContainer),

                _loginLabelsContainer.Top().EqualTo(20).BottomOf(_loginMethodLabel),
                _loginLabelsContainer.Width().EqualTo().WidthOf(_loginMethodContainer).WithMultiplier(.80f),
                _loginLabelsContainer.WithSameCenterX(_loginMethodContainer)
            );

            _loginLabelContainerConstraint = _loginLabelsContainer.Height().EqualTo(0).ToLayoutConstraints().ToArray()[0];

            _loginMethodContainer.AddConstraint(_loginLabelContainerConstraint);

            SetupIconContainerViewConstraints();
            SetupLoginLabelContainerViewConstraints();

        }

        void SetupLoginLabelContainerViewConstraints ()
        {
            _loginLabelsContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            _loginLabelsContainer.AddConstraints(
                
                _loginName.WithSameCenterX(_loginLabelsContainer),
                _loginName.Top().EqualTo().TopOf(_loginLabelsContainer),
                _loginName.Width().EqualTo().WidthOf(_loginLabelsContainer),
                _loginName.Height().EqualTo(30),

                _loginPassword.WithSameCenterX(_loginLabelsContainer),
                _loginPassword.Top().EqualTo(20).BottomOf(_loginName),
                _loginPassword.Width().EqualTo().WidthOf(_loginLabelsContainer),
                _loginPassword.Height().EqualTo(30),

                _signUpContainer.WithSameCenterX(_loginLabelsContainer),
                _signUpContainer.Top().EqualTo(15).BottomOf(_loginPassword),

                _forgotPassword.WithSameCenterX(_loginLabelsContainer),
                _forgotPassword.Top().EqualTo(15).BottomOf(_signUpContainer)
            );

            _forgotPassword.Text = "FORGOT";
            SetupSignUpContainerViewConstraints();
        }

        void SetupSignUpContainerViewConstraints()
        {
            _signUpContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            _signUpContainer.AddConstraints(
                
                _orLabel.Left().EqualTo().LeftOf(_signUpContainer),
                _orLabel.WithSameCenterY(_signUpContainer),

                _signUpLabel.Left().EqualTo(5.0f).RightOf(_orLabel),
                _signUpLabel.WithSameCenterY(_signUpContainer),

                _ifYouDontHaveOneLabel.Left().EqualTo(5.0f).RightOf(_signUpLabel),
                _ifYouDontHaveOneLabel.WithSameCenterY(_signUpContainer)
            );


            _loginLabelsContainer.AddConstraints(
                _signUpContainer.Right().EqualTo().RightOf(_ifYouDontHaveOneLabel),
                _signUpContainer.Bottom().EqualTo().BottomOf(_signUpLabel)
            );
        
        }

        void SetupIconContainerViewConstraints ()
        {
            _iconContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _facebookIcon.AddConstraints(_facebookIcon.Height().EqualTo(30), _facebookIcon.Width().EqualTo(30));
//            _twitterIcon.AddConstraints(_twitterIcon.Height().EqualTo(33), _twitterIcon.Width().EqualTo(33));
//            _instagramIcon.AddConstraints(_instagramIcon.Height().EqualTo(30), _instagramIcon.Width().EqualTo(30));
            _yufitIcon.AddConstraints(_yufitIcon.Height().EqualTo(30), _yufitIcon.Width().EqualTo(40));

            _iconContainer.AddConstraints(
                _yufitIcon.WithSameCenterY(_iconContainer),
                _yufitIcon.WithSameCenterX(_iconContainer).WithMultiplier(0.6f),
                _facebookIcon.WithSameCenterY(_iconContainer),
                _facebookIcon.WithSameCenterX(_iconContainer).WithMultiplier(1.4f)
//                _twitterIcon.WithSameCenterY(_iconContainer),
//                _twitterIcon.WithSameCenterX(_iconContainer).WithMultiplier(1.25f),
//                _instagramIcon.WithSameCenterY(_iconContainer),
//                _instagramIcon.WithSameCenterX(_iconContainer).WithMultiplier(1.75f)
            );
        }

        void SetupTopViewConstraints ()
        {
            _topView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _topView.AddConstraints(
                _logInLabel.Bottom().EqualTo(-5.0f).BottomOf(_topView),
                _logInLabel.CenterX().EqualTo().CenterXOf(_topView),

                _yufitLogoView.Bottom().EqualTo(-50.0f).BottomOf(_topView),
                _yufitLogoView.Top().EqualTo(30.0f).TopOf(_topView),
                _yufitLogoView.Width().EqualTo().HeightOf(_yufitLogoView),
                _yufitLogoView.CenterX().EqualTo().CenterXOf(_topView)
            );
        }

        void SetupBottomViewConstraints ()
        {
        }

        void SetupYellowDiamondViewConstraints ()
        {
            _yellowBottomDiamond.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _yellowBottomDiamond.AddConstraints(
                _goLabel.CenterY().EqualTo(2.0f).CenterYOf(_yellowBottomDiamond),
                _goLabel.CenterX().EqualTo().CenterXOf(_yellowBottomDiamond)
            );
        }

        #endregion

        #region Keyboard

        void SetupKeyboard ()
        {
            _fieldNavigator = new UITextFieldNavigator(new List<UITextField> {
                _loginName.TextField, _loginPassword.TextField
            });
        }

        protected override void KeyboardWillShow (nfloat keyboardTop, double animationSpeed)
        {
            base.KeyboardWillShow(keyboardTop, animationSpeed);

            var frame = _loginPassword.Frame;
            var convertedFrame = View.ConvertRectFromView(frame, _loginLabelsContainer);

            nfloat passwordBottom = convertedFrame.Bottom + 5.0f;
            if (passwordBottom > keyboardTop)
            {
                _bottomConstraint.Constant = 0 - (passwordBottom - keyboardTop);
                _topConstraint.Constant = 0 - (passwordBottom - keyboardTop);
                View.SetNeedsUpdateConstraints();
                UIView.AnimateNotify(animationSpeed, 0.0f, UIViewAnimationOptions.CurveEaseInOut, View.LayoutIfNeeded, null);
            }
        }

        protected override void KeyboardWillHide (double animationSpeed)
        {
            base.KeyboardWillHide(animationSpeed);

            _bottomConstraint.Constant = 0;
            _topConstraint.Constant = 0;
            View.SetNeedsUpdateConstraints();
            UIView.AnimateNotify(animationSpeed, 0.0f, UIViewAnimationOptions.CurveEaseInOut, View.LayoutIfNeeded, null);
        }

        #endregion

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<LoginView, LoginViewModel>();
            set.Bind().For(t => t.YufitChosen).To(vm => vm.YufitChosen);
            set.Bind(_facebookIcon.Tap()).To(vm => vm.ChooseFacebookCommand.Command);
            set.Bind(_facebookIcon).For(icon => icon.StrokeColor).To(vm => vm.FacebookChosen).WithConversion("AuthenticationMethodChosenToColor");
//            set.Bind(_twitterIcon.Tap()).To(vm => vm.ChooseTwitterCommand.Command);
//            set.Bind(_twitterIcon).For(icon => icon.StrokeColor).To(vm => vm.TwitterChosen).WithConversion("AuthenticationMethodChosenToColor");
//            set.Bind(_instagramIcon.Tap()).To(vm => vm.ChooseInstagramCommand.Command);
//            set.Bind(_instagramIcon).For(icon => icon.StrokeColor).To(vm => vm.InstagramChosen).WithConversion("AuthenticationMethodChosenToColor");
            set.Bind(_yufitIcon.Tap()).To(vm => vm.ChooseYufitCommand.Command);
            set.Bind(_yufitIcon).For(icon => icon.StrokeColor).To(vm => vm.YufitChosen).WithConversion("AuthenticationMethodChosenToColor");
            set.Bind(_yellowBottomDiamond.Tap()).To(vm => vm.AuthenticateCommand.Command);

            set.Bind(_signUpLabel.Tap()).To(vm => vm.SignUpCommand.Command);

            set.Bind(_logInLabel).To(vm => vm.LoginTitleString);
            set.Bind(_loginMethodLabel).To(vm => vm.LoginMethodString);
            set.Bind(_goLabel).To(vm => vm.GoString);
            set.Bind(_orLabel).To(vm => vm.OrString);
            set.Bind(_signUpLabel).To(vm => vm.SignUpString);
            set.Bind(_ifYouDontHaveOneLabel).To(vm => vm.IfYouDontHaveAnAccountString);
            set.Bind(_forgotPassword).To(vm => vm.ResetPasswordString);
            set.Bind(_forgotPassword.Tap()).To(vm => vm.ResetPasswordCommand.Command);

            set.Bind(_loginName.TextField).For("Placeholder").To(vm => vm.EmailLabelPlaceholder);
            set.Bind(_loginPassword.TextField).For("Placeholder").To(vm => vm.PasswordLabelPlaceholder);
            set.Bind(_loginName.TextField).To(vm => vm.Email);
            set.Bind(_loginPassword.TextField).To(vm => vm.Password);
            set.Apply();

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage);
        }

        public override void MotionEnded (UIEventSubtype motion, UIEvent evt)
        {
            if (evt.Subtype == UIEventSubtype.MotionShake) {
                //ViewModel.ShowWelcomeScreen();
            }
            base.MotionEnded(motion, evt);
        }

    }
}

