using System;
using System.Linq;

using Cirrious.FluentLayouts.Touch;

using MvvmCross.Binding.BindingContext;
using CoreGraphics;
using UIKit;

using YuFit.Core.ViewModels.Profiles.Edit;

using YuFit.IOS.Controls;
using YuFit.IOS.Extensions;
using YuFit.IOS.Views;
using YuFit.IOS.Views.Profiles.Edit;

namespace YuFit.IOS.Views.Profiles
{
    public class MyScrollView : UIScrollView
    {
        public MyScrollView ()
        {
            DelaysContentTouches = false;
            CanCancelContentTouches = true;
        }

        public override bool TouchesShouldCancelInContentView(UIView view)
        {
            //return !(view is ArrowSliderView || view.Superview is ArrowSliderView);
            return true;
        }
    }

    /// <summary>
    /// Edit profile view. 
    /// 
    /// The autolayout is done in a way that width is align with the View, but 
    /// height is added as constraints to the ScrollView.  If we add the width
    /// constraints to the ScrollView, stuff gets all fucked up and alignement
    /// gets all messed up.
    /// </summary>
    class EditProfileView : BaseViewController<EditProfileViewModel>
    {

        readonly UIScrollView             _scrollView            = new MyScrollView();
        readonly PictureContainer         _pictureContainer      = new PictureContainer();
        readonly BirthdayContainer        _birthdayContainer     = new BirthdayContainer();
        readonly PersonalInfoContainer    _personalInfoContainer = new PersonalInfoContainer();
        readonly SpecifyInterestContainer _interestContainer     = new SpecifyInterestContainer();
        //readonly MetricOptionContainer    _metricContainer       = new MetricOptionContainer();
        readonly BioContainer             _bioContainer          = new BioContainer();

        private nfloat _keyboardDelta;
        CGPoint _previousOffset;

        NSLayoutConstraint _bioHeightConstraint;

        bool _isDirty;
        public bool IsDirty {
            get { return _isDirty; }
            set { _isDirty = value; }// UpdateSaveButton(_isDirty); }
        }

        string _bio;
        public string Bio {
            get { return _bio; }
            set { 
                _bio = value;

                if (!_bioContainer.DescriptionLabel.IsFirstResponder) {
                    _bioContainer.RemoveConstraint(_bioHeightConstraint);
                    var _desiredHeight = _bioContainer.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
                    _bioHeightConstraint = _bioContainer.Height().EqualTo(_desiredHeight).ToLayoutConstraints().ToArray()[0];
                    _bioContainer.AddConstraint(_bioHeightConstraint);

                    View.SetNeedsUpdateConstraints();
                    View.LayoutIfNeeded();
                }
            }
        }

        public EditProfileView ()
        {
            WatchKeyboard = true;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            SetupSubviews();
            SetupBindings();
        }

        void SetupSubviews()
        {
            SetupScrollView();
            SetupPictureContainer();
            SetupBirthdayContainer();
            SetupPersonalInfoContainer();
            SetupInterestContainer();
            SetupMetricContainer();
            SetupBioContainer();

            // Once everything is added, we can adjust the scrollview bottom
            // constraint which will devine the amount of scrollable content.
            _scrollView.AddConstraints(
                _bioContainer.Bottom().EqualTo(-Spacing).BottomOf(_scrollView)
            );

//            var saveButton = new UIBarButtonItem(UIBarButtonSystemItem.Save, (s, e) => { ViewModel.SaveCommand.Command.Execute(null); });
//
//            UITextAttributes attributes = new UITextAttributes();
//            attributes.Font = Themes.NavigationBar.TitleFont; //UIFont.FromName("HelveticaNeueLTStd-LtCn", 19);
//            saveButton.SetTitleTextAttributes(attributes, UIControlState.Normal);
//            NavigationItem.RightBarButtonItem = saveButton;
        }

        void SetupScrollView ()
        {
            _scrollView.TranslatesAutoresizingMaskIntoConstraints = false;
            Add(_scrollView);

            View.AddConstraints(
                _scrollView.WithSameWidth(View),
                _scrollView.WithSameHeight(View),
                _scrollView.WithSameCenterX(View),
                _scrollView.WithSameCenterY(View)
            );
        }

        void SetupPictureContainer ()
        {
            // Should be half the width of the view (height and width)
            // Should be Right align with the rest of the content
            _pictureContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_pictureContainer);


            View.AddConstraints(
                _pictureContainer.Width().EqualTo().WidthOf(View).WithMultiplier(0.53f),
                _pictureContainer.Height().EqualTo().WidthOf(View).WithMultiplier(0.53f),
                _pictureContainer.Right().EqualTo(-Spacing).RightOf(View)
            );
            _scrollView.AddConstraints(
                _pictureContainer.Top().EqualTo(Spacing-2.0f).TopOf(_scrollView)
            );
        }

        void SetupBirthdayContainer ()
        {
            _birthdayContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_birthdayContainer);

            _birthdayContainer.AddConstraints(
                _birthdayContainer.Height().EqualTo(50.0f)
            );
            View.AddConstraints(
                _birthdayContainer.Left().EqualTo(Spacing).LeftOf(View),
                _birthdayContainer.Right().EqualTo(-Spacing).RightOf(View)
            );
            _scrollView.AddConstraints(
                _birthdayContainer.Bottom().EqualTo(1.0f).BottomOf(_pictureContainer)
            );
        }

        void SetupPersonalInfoContainer ()
        {
            _personalInfoContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_personalInfoContainer);

            View.AddConstraints(
                _personalInfoContainer.Left().EqualTo(Spacing).LeftOf(View),
                _personalInfoContainer.Right().EqualTo(-Spacing).RightOf(View)
            );
            _scrollView.AddConstraints(
                _personalInfoContainer.Top().EqualTo(21.0f).BottomOf(_birthdayContainer)
            );

            // TEMP HACK
            _personalInfoContainer.VM = ViewModel;
        }

        void SetupInterestContainer ()
        {
            _interestContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_interestContainer);

            _interestContainer.AddConstraints(
                _interestContainer.Height().EqualTo(40.0f)
            );

            View.AddConstraints(
                _interestContainer.Left().EqualTo(Spacing).LeftOf(View),
                _interestContainer.Right().EqualTo(-Spacing).RightOf(View)
            );
            _scrollView.AddConstraints(
                _interestContainer.Top().EqualTo(21.0f).BottomOf(_personalInfoContainer)
            );
        }

        void SetupMetricContainer ()
        {
//            _metricContainer.TranslatesAutoresizingMaskIntoConstraints = false;
//            _scrollView.Add(_metricContainer);
//
//            _metricContainer.AddConstraints(
//                _metricContainer.Height().EqualTo(30.0f)
//            );
//
//            View.AddConstraints(
//                _metricContainer.Left().EqualTo(Spacing).LeftOf(View),
//                _metricContainer.Right().EqualTo(-Spacing).RightOf(View)
//            );
//            _scrollView.AddConstraints(
//                _metricContainer.Top().EqualTo(21.0f).BottomOf(_interestContainer)
//            );
        }

        void SetupBioContainer ()
        {
            _bioContainer.DescriptionLabel.ScrollEnabled = false;

            _bioContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_bioContainer);

            var _desiredHeight = _bioContainer.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
            _bioHeightConstraint = _bioContainer.Height().EqualTo(_desiredHeight).ToLayoutConstraints().ToArray()[0];
            _bioContainer.AddConstraint(_bioHeightConstraint);

            View.AddConstraints(
                _bioContainer.Left().EqualTo(Spacing).LeftOf(View),
                _bioContainer.Right().EqualTo(-Spacing).RightOf(View)
            );
            _scrollView.AddConstraints(
                _bioContainer.Top().EqualTo(21.0f).BottomOf(_interestContainer)
            );
        }

        void SetupBindings()
        {
            var set = this.CreateBindingSet<EditProfileView, EditProfileViewModel>();
            set.Bind(_pictureContainer.Tap()).To(vm => vm.AddAvatarCommand.Command);
            set.Bind(_pictureContainer.AvatarImage).For(p => p.Image).To(vm => vm.AvatarData).WithConversion("InMemoryImage");
            set.Bind(_pictureContainer.ImageViewLoader).To(vm => vm.AvatarUrl);

            set.Bind(_birthdayContainer.Label).To(vm => vm.BirthdayPlaceholderText);
            set.Bind(_birthdayContainer.BirthdayLabel.DatePicker).To(vm => vm.BirthDay);
            set.Bind(_birthdayContainer.BirthdayLabel).To("Format('{0:dd /MMM /yyyy}', BirthDay)");

            set.Bind(_personalInfoContainer.Name).For("Placeholder").To(vm => vm.NamePlaceholderText);
            set.Bind(_personalInfoContainer.Email).For("Placeholder").To(vm => vm.EmailPlaceholderText);
            set.Bind(_personalInfoContainer.Address).For("Placeholder").To(vm => vm.AddressPlaceholderText);
            set.Bind(_personalInfoContainer.Name).To(vm => vm.RealName);
            set.Bind(_personalInfoContainer.Email).To(vm => vm.EmailAddress);
            set.Bind(_personalInfoContainer.Address).To(vm => vm.Hometown);

            set.Bind(_interestContainer.Label).To(vm => vm.InterestPlaceholderText);
            set.Bind(_interestContainer.Source).To(vm => vm.Activities);
            set.Bind(_interestContainer.Source).For(s => s.SelectionChangedCommand).To(vm => vm.ToggleActivityCommand.Command);

//            set.Bind(_metricContainer.SegmentedControl).For(sm=>sm.SelectedSegment).To(vm=>vm.SelectedSegment);

            set.Bind(_bioContainer.DescriptionLabel).For(l => l.Data).To(vm => vm.Bio);
            set.Bind(_bioContainer.DescriptionLabel).For("Placeholder").To(vm => vm.BioPlaceholderText);
            set.Bind().For(me => me.Bio).To(vm => vm.Bio);

            set.Bind().For(me => me.IsDirty).To(vm => vm.IsChanged);
            set.Apply ();

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage);
        }

        protected override void KeyboardWillShow (nfloat keyboardTop, double animationSpeed)
        {
            base.KeyboardWillShow(keyboardTop, animationSpeed);
            _previousOffset = _scrollView.ContentOffset;
            nfloat realContainerBottom = 0.0f;
            if (_personalInfoContainer.IsEditing) {
                realContainerBottom = _personalInfoContainer.Frame.Bottom - _scrollView.ContentOffset.Y;

                _keyboardDelta = realContainerBottom - keyboardTop;
                Console.WriteLine("{0} = {1} - {2}", _keyboardDelta, realContainerBottom, keyboardTop);
                if (_keyboardDelta > 0) {
                    _scrollView.SetContentOffset(new CGPoint(0, _scrollView.ContentOffset.Y + _keyboardDelta), true);
                    //_scrollView.ScrollEnabled = false;
                } else {
                    _keyboardDelta = 0.0f;
                }
            } else if (_bioContainer.IsEditing) {

                _bioContainer.DescriptionLabel.ScrollEnabled = true;
                realContainerBottom = (_bioContainer.Frame.Top + keyboardTop);// - _scrollView.ContentOffset.Y;

                _bioContainer.RemoveConstraint(_bioHeightConstraint);
                _bioHeightConstraint = _bioContainer.Height().EqualTo(keyboardTop).ToLayoutConstraints().ToArray()[0];
                _bioContainer.AddConstraint(_bioHeightConstraint);

                View.SetNeedsUpdateConstraints();
                View.LayoutIfNeeded();

                Console.WriteLine("{0}", _bioContainer.Frame.Top);
                _scrollView.SetContentOffset(new CGPoint(0, _bioContainer.Frame.Top), true);
                _scrollView.ScrollEnabled = false;
            }
        }

        protected override void KeyboardWillHide (double animationSpeed)
        {
            base.KeyboardWillHide(animationSpeed);
            _bioContainer.DescriptionLabel.ScrollEnabled = false;


            _bioContainer.RemoveConstraint(_bioHeightConstraint);
            var _desiredHeight = _bioContainer.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
            _bioHeightConstraint = _bioContainer.Height().EqualTo(_desiredHeight).ToLayoutConstraints().ToArray()[0];
            _bioContainer.AddConstraint(_bioHeightConstraint);

            View.LayoutIfNeeded();
            _scrollView.SetContentOffset(_previousOffset, true);

            if (_scrollView.ContentSize.Height > View.Frame.Height) {
                _scrollView.SetContentOffset(_previousOffset, true);
            } else {
                _scrollView.SetContentOffset(new CGPoint(0.0f, 0.0f), true);
            }

            _keyboardDelta = 0.0f;
            _scrollView.ScrollEnabled = true;
        }

        void UpdateSaveButton (bool enabled)
        {
            UIView.Animate(0.25f, () => {
                NavigationItem.RightBarButtonItem.Enabled = enabled; 
                NavigationItem.RightBarButtonItem.TintColor = enabled ? Themes.Dashboard.HomeIconColor : UIColor.Clear; 
            });
        }
    }
}
