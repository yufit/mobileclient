﻿using System;
using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using CoreGraphics;
using Foundation;
using UIKit;

using YuFit.Core.ViewModels.Activity;
using YuFit.Core.ViewModels.Profiles;

using YuFit.IOS.Controls;
using YuFit.IOS.Controls.Buttons;
using YuFit.IOS.Controls.UpcomingActivitiesShortView;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Profiles
{
    public class FriendProfileView : BaseViewController<FriendProfileViewModel>
    {
        const float TableViewHeight = 170.0f;

        // Analysis disable once ConvertToStaticType
        class TableViewCell : UpcomingShortListTableViewCell<UpcomingActivityViewModel> {
            public static readonly NSString Key = new NSString("UpcomingActivitiesTableViewCell");
        }
        class TableView : UpcomingShortListTableView<UpcomingActivityViewModel, TableViewCell> {
            public TableView () : base(TableViewCell.Key) {  }
        }

        class ShortListView : UpcomingShortListView<TableView, TableViewCell> {}

        readonly UIScrollView           _scrollView         = new UIScrollView();
        readonly PictureContainer       _pictureContainer   = new PictureContainer();
        readonly UILabel                _nameLabel          = new UILabel();
        readonly UILabel                _homeLabel          = new UILabel();
        readonly ViewFriendsButton      _viewFriendsButton  = new ViewFriendsButton();
        readonly PlaceholderTextView    _bioTextView        = new PlaceholderTextView();

        readonly JoinGroupButton        _addFriendButton    = new JoinGroupButton();
        readonly LeaveGroupButton       _unFriendButton     = new LeaveGroupButton();


        readonly DiamondShapeView       _countDiamondView   = new DiamondShapeView();
        readonly UILabel                _countTitleLabel    = new UILabel();
        readonly UILabel                _countLabel         = new UILabel();

        readonly ShortListView          _upcomingListView   = new ShortListView();

        NSLayoutConstraint _bioHeightConstraint;
        NSLayoutConstraint _nameLabelLeftConstraint;
        NSLayoutConstraint _homeLabelLeftConstraint;
        NSLayoutConstraint _unFriendButtonLeftConstraint;

        string _bio;
        public string Bio {
            get { return _bio; }
            set { 
                _bio = value;
                _bioTextView.Text = value;

                _bioTextView.RemoveConstraint(_bioHeightConstraint);

                var _desiredHeight = _bioTextView.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
                _bioHeightConstraint = _bioTextView.Height().EqualTo(_desiredHeight).ToLayoutConstraints().ToArray()[0];

                _bioTextView.AddConstraint(_bioHeightConstraint);

                View.SetNeedsUpdateConstraints();
                View.LayoutIfNeeded();
            }
        }

        string _name;
        public string Name {
            get { return _name; }
            set { 
                _name = value;
                _nameLabel.Text = value;

                var _desiredHeight = _nameLabel.SizeThatFits(new CGSize(_nameLabel.Frame.Width, nfloat.MaxValue)).Height;
                _nameLabelLeftConstraint.Constant = _desiredHeight;

                View.SetNeedsUpdateConstraints();
                View.LayoutIfNeeded();
            }
        }

        string _home;
        public string Home {
            get { return _home; }
            set { 
                _home = value;
                _homeLabel.Text = value;

                var _desiredHeight = _homeLabel.SizeThatFits(new CGSize(_homeLabel.Frame.Width, nfloat.MaxValue)).Height;
                _homeLabelLeftConstraint.Constant = _desiredHeight;

                View.SetNeedsUpdateConstraints();
                View.LayoutIfNeeded();
            }
        }

        public bool ThisIsMe {
            get { return ViewModel.ThisIsMe; }
            set { 
                _addFriendButton.Hidden = value;
                _unFriendButton.Hidden = value;
            }
        }

        // Analysis disable ValueParameterNotUsed
        public bool IsFriend        { get { return ViewModel.IsFriend;        } set { ResetFriendshipButton(); } }
        // Analysis restore ValueParameterNotUsed

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            SetupSubviews();
            SetupBindings();
        }


        void SetupSubviews()
        {
            SetupScrollView();
            SetupPictureContainer();
            SetupNameLabel();
            SetupHomeLabel();
            SetupFriendshipButton();
            SetupViewFriendsButton();
            SetupCountDiamondView();
            SetupBioContainer();
            SetupBottomContainer();

            View.AddConstraints(
                _scrollView.Bottom().EqualTo(-TableViewHeight).BottomOf(View)
            );

            // Once everything is added, we can adjust the scrollview bottom
            // constraint which will devine the amount of scrollable content.
            _scrollView.AddConstraints(
                _bioTextView.Bottom().EqualTo(-Spacing).BottomOf(_scrollView)
            );


        }

        void SetupScrollView ()
        {
            _scrollView.TranslatesAutoresizingMaskIntoConstraints = false;
            Add(_scrollView);
            View.AddConstraints(
                _scrollView.WithSameWidth(View),
                _scrollView.WithSameCenterX(View),
                _scrollView.Top().EqualTo().TopOf(View)
            );
        }

        void SetupPictureContainer ()
        {
            _pictureContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_pictureContainer);
            _pictureContainer.StrokeWidth = 1.0f;
            _pictureContainer.StrokeColor = YuFitStyleKit.YellowDark;

            _pictureContainer.AddConstraints(
                _pictureContainer.Height().EqualTo().WidthOf(_pictureContainer)
            );

            View.AddConstraints(
                _pictureContainer.Width().EqualTo(-110.0f).WidthOf(View),
                _pictureContainer.Left().EqualTo(-55.0f).LeftOf(View)
            );
            _scrollView.AddConstraints(
                _pictureContainer.Top().EqualTo(5.0f).TopOf(_scrollView)
            );
        }

        void SetupNameLabel ()
        {
            _nameLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_nameLabel);
            _nameLabel.TextAlignment = UITextAlignment.Left;
            _nameLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 26);
            _nameLabel.TextColor = YuFitStyleKit.White;
            _nameLabel.AdjustsFontSizeToFitWidth = true;

            _nameLabelLeftConstraint = _nameLabel.Left().EqualTo().CenterXOf(_pictureContainer).ToLayoutConstraints().ToList()[0];
            View.AddConstraint(_nameLabelLeftConstraint);

            View.AddConstraints(
                _nameLabel.Top().EqualTo(2.0f).TopOf(_pictureContainer),
                _nameLabel.Right().EqualTo(-Spacing).RightOf(View)
            );
        }

        void SetupHomeLabel ()
        {
            _homeLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_homeLabel);
            _homeLabel.TextAlignment = UITextAlignment.Left;
            _homeLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 20);
            _homeLabel.TextColor = YuFitStyleKit.Gray;
            _homeLabel.AdjustsFontSizeToFitWidth = true;

            _homeLabelLeftConstraint = _homeLabel.Left().EqualTo().LeftOf(_nameLabel).ToLayoutConstraints().ToList()[0];
            View.AddConstraint(_homeLabelLeftConstraint);

            View.AddConstraints(
                _homeLabel.Top().EqualTo().BottomOf(_nameLabel),
                _homeLabel.Right().EqualTo(-Spacing).RightOf(View)
            );
        }

        void SetupFriendshipButton ()
        {
            _addFriendButton.TranslatesAutoresizingMaskIntoConstraints = false;
            _unFriendButton.TranslatesAutoresizingMaskIntoConstraints = false;

            _scrollView.Add(_addFriendButton);
            _scrollView.Add(_unFriendButton);

            _addFriendButton.AddConstraints(_addFriendButton.Height().EqualTo().WidthOf(_addFriendButton));
            _unFriendButton.AddConstraints(_unFriendButton.Height().EqualTo().WidthOf(_unFriendButton));

            View.AddConstraints(
                _addFriendButton.CenterY().EqualTo().CenterYOf(_pictureContainer),
                _addFriendButton.Width().EqualTo().WidthOf(_pictureContainer).WithMultiplier(0.25f),
                _addFriendButton.Left().EqualTo(2.5f).RightOf(_pictureContainer),

                _unFriendButton.CenterY().EqualTo().CenterYOf(_pictureContainer),
                _unFriendButton.Width().EqualTo().WidthOf(_pictureContainer).WithMultiplier(0.25f)
            );

            _unFriendButtonLeftConstraint = _unFriendButton.Left().EqualTo(2.5f).RightOf(_pictureContainer).ToLayoutConstraints().ToList()[0];
            View.AddConstraint(_unFriendButtonLeftConstraint);

            _addFriendButton.Hidden = true;
            _unFriendButton.Hidden = true;
        }

        void SetupViewFriendsButton ()
        {
            _viewFriendsButton.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_viewFriendsButton);

            _viewFriendsButton.AddConstraints(
                _viewFriendsButton.Height().EqualTo().WidthOf(_viewFriendsButton)
            );

            View.AddConstraints(
                _viewFriendsButton.Bottom().EqualTo(-1.25f).CenterYOf(_pictureContainer),
                _viewFriendsButton.Width().EqualTo().WidthOf(_pictureContainer).WithMultiplier(0.25f),
                _viewFriendsButton.CenterX().EqualTo(2.5f/2.0f).RightOf(_pictureContainer)
            );

        }

        void SetupCountDiamondView ()
        {
            _countDiamondView.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_countDiamondView);
            _countDiamondView.StrokeColor = YuFitStyleKit.Yellow;
            _countDiamondView.StrokeWidth = 3.0f;
            _countDiamondView.TransparentBackground = true;

            _countDiamondView.AddConstraints(
                _countDiamondView.Height().EqualTo().WidthOf(_countDiamondView)
            );


            View.AddConstraints(
                _countDiamondView.Top().EqualTo(1.25f).CenterYOf(_pictureContainer),
                _countDiamondView.Width().EqualTo().WidthOf(_pictureContainer).WithMultiplier(0.5f),
                _countDiamondView.CenterX().EqualTo(1.25f).RightOf(_pictureContainer)
            );

            _countTitleLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _countDiamondView.Add(_countTitleLabel);
            _countLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _countDiamondView.Add(_countLabel);


            _countTitleLabel.TextAlignment = UITextAlignment.Center;
            _countTitleLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            _countTitleLabel.TextColor = YuFitStyleKit.YellowDark;
            _countTitleLabel.AdjustsFontSizeToFitWidth = true;

            _countDiamondView.AddConstraints(
                _countTitleLabel.CenterX().EqualTo().CenterXOf(_countDiamondView),
                _countTitleLabel.Bottom().EqualTo(-2.0f).TopOf(_countLabel),
                _countTitleLabel.Width().EqualTo().WidthOf(_countDiamondView).WithMultiplier(0.5f)
            );

            _countTitleLabel.Text = "been in";

            _countLabel.TextAlignment = UITextAlignment.Center;
            _countLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 32);
            _countLabel.TextColor = YuFitStyleKit.White;
            _countLabel.AdjustsFontSizeToFitWidth = true;

            _countLabel.Text = "18";
            //var _desiredHeight = _countLabel.SizeThatFits(new CGSize(_countLabel.Frame.Width, nfloat.MaxValue)).Height;

            _countDiamondView.AddConstraints(
                _countLabel.CenterX().EqualTo().CenterXOf(_countDiamondView),
                _countLabel.CenterY().EqualTo().CenterYOf(_countDiamondView).WithMultiplier(1.25f),
                _countLabel.Width().EqualTo().WidthOf(_countDiamondView).WithMultiplier(0.6f),
                _countLabel.Height().EqualTo().HeightOf(_countDiamondView).WithMultiplier(0.3f)
            );
        }

        void SetupBioContainer()
        {
            _bioTextView.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_bioTextView);

            _bioTextView.TextColor = YuFitStyleKit.Gray;
            _bioTextView.TextAlignment = UITextAlignment.Center;
            _bioTextView.Font = CrudFitEvent.WhenDiamond.TimeLabelTextFont;
            _bioTextView.Editable = false;
            _bioTextView.BackgroundColor = UIColor.Clear;
            _bioTextView.ScrollEnabled = false;

            var _desiredHeight = _bioTextView.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
            _bioHeightConstraint = _bioTextView.Height().EqualTo(_desiredHeight).ToLayoutConstraints().ToArray()[0];
            _bioTextView.AddConstraint(_bioHeightConstraint);

            View.AddConstraints(
                _bioTextView.Left().EqualTo(Spacing).LeftOf(View),
                _bioTextView.Right().EqualTo(-Spacing).RightOf(View)
            );
            _scrollView.AddConstraints(
                _bioTextView.Top().EqualTo(20.0f).BottomOf(_pictureContainer)
            );
        }

        void SetupBottomContainer()
        {
            //FIGURE OUT HOW TO AVOID THIS
            _upcomingListView.SetBindingContext(BindingContext);
            AddChildViewController(_upcomingListView.TableViewCtrl);
            _upcomingListView.SetLayoutConstraints();

            _upcomingListView.TranslatesAutoresizingMaskIntoConstraints = false;

            View.AddSubview(_upcomingListView);
            _upcomingListView.AddConstraints(
                _upcomingListView.Height().EqualTo(TableViewHeight)
            );
            View.AddConstraints(
                _upcomingListView.Left().EqualTo(Spacing).LeftOf(View),
                _upcomingListView.Bottom().EqualTo(-8.0f).BottomOf(View),
                _upcomingListView.Right().EqualTo(-Spacing).RightOf(View)
            );
        }

        public override void ViewDidDisappear (bool animated)
        {
            _upcomingListView.SetBindingContext(null);
            base.ViewDidDisappear(animated);
        }

        void SetupBindings()
        {
            var set = this.CreateBindingSet<FriendProfileView, FriendProfileViewModel>();
            set.Bind(_pictureContainer.ImageViewLoader).To(vm => vm.AvatarUrl);
            set.Bind().For(me => me.Title).To(vm => vm.DisplayName);
            set.Bind().For(me => me.Name).To(vm => vm.RealName);
            set.Bind().For(me => me.Home).To(vm => vm.HomeAddress);
            set.Bind().For(me => me.ThisIsMe).To(vm => vm.ThisIsMe);
            set.Bind().For(me => me.IsFriend).To(vm => vm.IsFriend);

            set.Bind(_countTitleLabel).To(vm => vm.CreatedCountLabel);
            set.Bind(_countLabel).To(vm => vm.ActivityCreatedCount);

            set.Bind().For(me => me.Bio).To(vm => vm.Bio);

            set.Bind(_upcomingListView.UpcomingTitle).To(vm => vm.UpcomingActivitiesTitle);
            set.Bind(_upcomingListView.TableViewCtrl.Source).To(vm => vm.UpcomingActivities);
            set.Bind(_upcomingListView.TableViewCtrl.Source).For(s => s.SelectionChangedCommand).To(vm => vm.ShowActivitySummaryCommand.Command);

            set.Bind(_viewFriendsButton.Tap()).To(vm => vm.ViewFriendsCommand.Command);
            set.Bind(_addFriendButton.Tap()).To(vm => vm.AddAsFriendCommand.Command);
            set.Bind(_unFriendButton.Tap()).To(vm => vm.UnFriendCommand.Command);
            set.Apply ();

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage);
        }

        void ResetFriendshipButton ()
        {
            _addFriendButton.Hidden = IsFriend || ThisIsMe;
            _unFriendButton.Hidden = !IsFriend || ThisIsMe;

            if (IsFriend && !ThisIsMe) {
                _unFriendButtonLeftConstraint.Constant = 2.5f;
                View.NeedsUpdateConstraints();
            } else if (!ThisIsMe){
                _unFriendButtonLeftConstraint.Constant = -1000;
                View.NeedsUpdateConstraints();
            }
        }


    }
}

