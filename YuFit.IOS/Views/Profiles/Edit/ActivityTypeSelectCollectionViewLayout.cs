﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;


namespace YuFit.IOS.Views.Profiles.Edit
{
    #if notes

    const int itemCountPerBlock = 5;
    const int firstRowCount = itemCountPerBlock / 2;
    const int secondRowCount = itemCountPerBlock - firstRowCount;
    const int totalItem = 24;

    for (int i=0; i<totalItem; i++)
    {
    int mod = i % itemCountPerBlock;
    int block = (int) Math.Floor ((double)(i / itemCountPerBlock));
    int row = block * 2 + (mod < firstRowCount ? 0 : 1);
    Console.WriteLine ("{2} {3} Item at pos {0} would be on line {1}", i, row, mod, block);
    }

    #endif


    public class ActivityTypeSelectCollectionViewLayout : UICollectionViewLayout
    {
        #region Constants

        const int _itemCountPerBlock = 7;
        const int _lineCountPerBlock = 1;
        const int _firstRowCount = _itemCountPerBlock / _lineCountPerBlock;
        const int _secondRowCount = _itemCountPerBlock - _firstRowCount;

        #endregion

        List<NSIndexPath> deleteIndexPaths;
        List<NSIndexPath> insertIndexPaths;

        nint _cellCount;

        CGSize _itemSize;

        public override CGSize CollectionViewContentSize { 
            get { 
                CGSize size = CollectionView.Frame.Size;
                _itemSize = new CGSize(size.Width / _cellCount, size.Height);
                return new CGSize(CollectionView.Frame.Size.Width, CollectionView.Frame.Size.Height); 
            } 
        }

        #region Construction

        public ActivityTypeSelectCollectionViewLayout ()
        {
        }

        #endregion

        public override void PrepareLayout ()
        {
            base.PrepareLayout();

            _cellCount = CollectionView.NumberOfItemsInSection(0);

            CGSize size = CollectionView.Frame.Size;
            _itemSize = new CGSize(size.Width / _cellCount, size.Height);
        }

        public override UICollectionViewLayoutAttributes LayoutAttributesForItem (NSIndexPath indexPath)
        {
            UICollectionViewLayoutAttributes attributes = UICollectionViewLayoutAttributes.CreateForCell(indexPath);
            attributes.Size = new CGSize(_itemSize);

            int itemIndex = indexPath.Row;
            nfloat xOff = itemIndex * _itemSize.Width;
            nfloat yOff = 0.0f;

            attributes.Center = new CGPoint(xOff + (_itemSize.Width/2.0f), yOff + (_itemSize.Height/2.0f));
            return attributes;
        }

        public override UICollectionViewLayoutAttributes[] LayoutAttributesForElementsInRect (CGRect rect)
        {
            List<UICollectionViewLayoutAttributes> attributes = new List<UICollectionViewLayoutAttributes>();
            for (int i=0 ; i < _cellCount; i++) {
                NSIndexPath indexPath = NSIndexPath.FromItemSection(i, 0);
                attributes.Add(LayoutAttributesForItem(indexPath));
            }
            return attributes.ToArray();
        }

        public override void PrepareForCollectionViewUpdates (UICollectionViewUpdateItem[] updateItems)
        {
            base.PrepareForCollectionViewUpdates(updateItems);

            deleteIndexPaths = new List<NSIndexPath>();
            insertIndexPaths = new List<NSIndexPath>();

            Array.ForEach(updateItems, u => {
                if (u.UpdateAction == UICollectionUpdateAction.Delete) {
                    deleteIndexPaths.Add(u.IndexPathBeforeUpdate);
                } else if (u.UpdateAction == UICollectionUpdateAction.Insert) {
                    insertIndexPaths.Add(u.IndexPathBeforeUpdate);
                }
            });
        }

        public override void FinalizeCollectionViewUpdates ()
        {
            base.FinalizeCollectionViewUpdates();
            deleteIndexPaths = null;
            insertIndexPaths = null;
        }

        //        public override UICollectionViewLayoutAttributes InitialLayoutAttributesForAppearingItem (NSIndexPath itemIndexPath)
        //        {
        //            UICollectionViewLayoutAttributes attributes = base.InitialLayoutAttributesForAppearingItem(itemIndexPath);
        //
        //            if (insertIndexPaths.Contains(itemIndexPath))
        //            {
        //                // only change attributes on inserted cells
        //                if (attributes == null)
        //                {
        //                    attributes = LayoutAttributesForItem(itemIndexPath);
        //                }
        //
        //                // Configure attributes ...
        //                attributes.Alpha = 0.0f;
        //            }
        //
        //            return attributes;
        //        }

        //        public override UICollectionViewLayoutAttributes FinalLayoutAttributesForDisappearingItem (NSIndexPath itemIndexPath)
        //        {
        //            UICollectionViewLayoutAttributes attributes = base.InitialLayoutAttributesForAppearingItem(itemIndexPath);
        //            if (deleteIndexPaths.Contains(itemIndexPath))
        //            {
        //                // only change attributes on inserted cells
        //                if (attributes == null)
        //                {
        //                    attributes = LayoutAttributesForItem(itemIndexPath);
        //                }
        //
        //                // Configure attributes ...
        //                attributes.Alpha = 0.0f;
        //                attributes.Transform3D = CATransform3D.MakeScale(0.1f, 0.1f, 1.0f);
        //            }
        //
        //            return attributes;
        //        }    
    }
}

