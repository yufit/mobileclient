﻿using System;
using System.Linq;

using Cirrious.FluentLayouts.Touch;

using CoreGraphics;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Profiles.Edit
{
    public class BioContainer : UIView
    {
        readonly SeparatorLine  _topLine         = new SeparatorLine();
        readonly SeparatorLine  _bottomLine      = new SeparatorLine();

        public readonly PlaceholderTextView DescriptionLabel = new PlaceholderTextView();

        public bool IsEditing { get { return DescriptionLabel.IsFirstResponder; } }

        public BioContainer ()
        {
            UIView[] subviews = { _topLine, DescriptionLabel, _bottomLine };
            AddSubviews(subviews);

            Subviews.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;

                this.AddConstraints(
                    v.Width().EqualTo().WidthOf(this),
                    v.WithSameCenterX(this)
                );
            });

            Subviews.Where(v => v is SeparatorLine).ForEach(v => {
                v.AddConstraints(v.Height().EqualTo(1.0f));
                v.BackgroundColor = YuFitStyleKit.Yellow;
            });

            DescriptionLabel.Font = CrudFitEvent.ConfigView.LabelTextFont;
            DescriptionLabel.TextColor = YuFitStyleKit.Yellow;
            DescriptionLabel.Editable = true;

            DescriptionLabel.KeyboardType = UIKeyboardType.Default;
            DescriptionLabel.AutocorrectionType = UITextAutocorrectionType.Yes;
            DescriptionLabel.AutocapitalizationType = UITextAutocapitalizationType.Sentences;
            DescriptionLabel.BackgroundColor = UIColor.Clear;

            KeyboardToolbar toolbar = new KeyboardToolbar (new CoreGraphics.CGRect(0.0f, 0.0f, Frame.Size.Width, 44.0f));

            toolbar.TintColor = YuFitStyleKit.White;
            toolbar.BarStyle = UIBarStyle.Black;

            toolbar.Translucent = true;

            SetupConstraints();

            var myButton = new UIBarButtonItem (":-)", UIBarButtonItemStyle.Plain, AddBarButtonText);


            toolbar.Items = new UIBarButtonItem[]{ 
                myButton,
                new UIBarButtonItem(":-)", 
                    UIBarButtonItemStyle.Plain, AddBarButtonText),
                new UIBarButtonItem(":-(", 
                    UIBarButtonItemStyle.Plain, AddBarButtonText),
                new UIBarButtonItem(";-)", 
                    UIBarButtonItemStyle.Plain, AddBarButtonText),
                new UIBarButtonItem(":-P", 
                    UIBarButtonItemStyle.Plain, AddBarButtonText),
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate {
                    this.DescriptionLabel.ResignFirstResponder();
                })
            };

            DescriptionLabel.KeyboardAppearance = UIKeyboardAppearance.Dark;
            DescriptionLabel.InputAccessoryView = toolbar;
        }

        public void AddBarButtonText(object sender, EventArgs e)
        {
            var barButtonItem = sender as UIBarButtonItem;
            if (barButtonItem != null) {
                DescriptionLabel.Text += barButtonItem.Title;
                UIDevice.CurrentDevice.PlayInputClick ();
            }
        }

        void SetupConstraints ()
        {
            this.AddConstraints(
                _topLine.Top().EqualTo(1.0f).TopOf(this),
                DescriptionLabel.Top().EqualTo(8.0f).BottomOf(_topLine),
                DescriptionLabel.Bottom().EqualTo(-8.0f).TopOf(_bottomLine),
                _bottomLine.Bottom().EqualTo(-1.0f).BottomOf(this)
            );
        }

        public override CoreGraphics.CGSize SizeThatFits (CoreGraphics.CGSize size)
        {
            // TODO Replace the 20 with some math (textView.Y - this.Frame.Y) + (this.Frame.Bottom - thexView.Bottom)
            var textViewHeight = DescriptionLabel.SizeThatFits(size).Height;
            return new CGSize(size.Width, textViewHeight + 50);
        }
    }
}

