﻿
using System;

using Foundation;
using UIKit;
using MvvmCross.Binding.BindingContext;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using YuFit.Core.ViewModels.Profiles.Edit;
using CoreGraphics;
using YuFit.Core.Models;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Views.Profiles.Edit
{
    public partial class ActivityTypeSelectCollectionViewCell : MvxCollectionViewCell
    {
        public static readonly UINib Nib = UINib.FromName("ActivityTypeSelectCollectionViewCell", NSBundle.MainBundle);
        public static readonly NSString Key = new NSString("ActivityTypeSelectCollectionViewCell");

        readonly ActivityIconView _iconView = new ActivityIconView();

        private string _activityType;
        public string ActivityType {
            get { return _activityType; }
            set { _activityType = value; UpdateColors(); }
        }

        private bool _isEnabled;
        public bool IsEnabled {
            get { return _isEnabled; }
            set { _isEnabled = value; UpdateColors(); }
        }

        private bool _isSelected;
        public bool IsSelected {
            get { return _isSelected; }
            set { _isSelected = value; UpdateColors(); }
        }

        public ActivityTypeSelectCollectionViewCell (IntPtr handle) : base(handle)
        {
            ContentView.Frame = Bounds;
            _iconView = new ActivityIconView(ContentView.Frame);
            Add(_iconView);

            this.DelayBind(() => {
                var set = this.CreateBindingSet<ActivityTypeSelectCollectionViewCell, ActivitySelectViewModel>();
                set.Bind(_iconView).For(me => me.DrawIcon).To(vm => vm.ActivityType);
                set.Bind(this).For(me => me.ActivityType).To(vm => vm.ActivityType);
                set.Bind(this).For(me => me.IsEnabled).To(vm => vm.IsEnabled);
                set.Bind(this).For(me => me.IsSelected).To(vm => vm.IsSelected);
                set.Apply();
            });
            _iconView.SetNeedsDisplay();
        }

        public static ActivityTypeSelectCollectionViewCell Create ()
        {
            return (ActivityTypeSelectCollectionViewCell) Nib.Instantiate(null, null)[0];
        }

        void UpdateColors ()
        {
            if (!_isSelected) {
                _iconView.IconColor = YuFitStyleKit.ActivityDisabledColor;
            } else {
                var colors = YuFitStyleKitExtension.ActivityBackgroundColors(_activityType);
                _iconView.IconColor = colors.Base;
            }
            _iconView.SetNeedsDisplay();
        }

        public override void LayoutSubviews ()
        {
            ContentView.Frame = Bounds;

            CGRect rect = ContentView.Bounds;
            rect = rect.Inset(5.0f, 5.0f);

            if (rect.Height > rect.Width) {
                rect.Height = rect.Width;
            } else {
                rect.Width = rect.Height;
            }

            rect.X = (ContentView.Bounds.Width/2.0f) - (rect.Width/2.0f);
            rect.Y = (ContentView.Bounds.Height/2.0f) - (rect.Height/2.0f);

            _iconView.Frame = rect;
            _iconView.SetNeedsDisplay();
            base.LayoutSubviews();
        }
    }
}

