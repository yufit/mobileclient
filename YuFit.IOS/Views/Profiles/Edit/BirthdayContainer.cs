﻿using Cirrious.FluentLayouts.Touch;

using UIKit;

using YuFit.Core.Extensions;

using YuFit.IOS.Themes;
using YuFit.IOS.Controls;

namespace YuFit.IOS.Views.Profiles.Edit
{
    public class BirthdayContainer : UIView
    {
        public readonly UILabel Label    = new UILabel();
        readonly UITapGestureRecognizer _resignFirstResponderTap;

        public readonly DateField BirthdayLabel = new DateField();

        public BirthdayContainer ()
        {
            AddSubviews(new UIView[] { Label, BirthdayLabel });
            Label.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 19);
            Label.TextColor = YuFitStyleKit.White;
            BirthdayLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
            BirthdayLabel.TextColor = YuFitStyleKit.Yellow;

            Label.Text = "birthday";
            BirthdayLabel.Started += (sender, e) => Superview.AddGestureRecognizer(_resignFirstResponderTap);
            BirthdayLabel.Ended += (sender, e) => Superview.RemoveGestureRecognizer(_resignFirstResponderTap);

            _resignFirstResponderTap = new UITapGestureRecognizer(() => BirthdayLabel.ResignFirstResponder());
        }

        public override void MovedToSuperview ()
        {
            base.MovedToSuperview();

            RemoveConstraints(Constraints);
            Subviews.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;
                this.AddConstraints(
                    Label.Bottom().EqualTo(-5.0f).TopOf(BirthdayLabel),
                    BirthdayLabel.Bottom().EqualTo(3.0f).BottomOf(this),
                    Label.Left().EqualTo().LeftOf(this),
                    BirthdayLabel.Left().EqualTo().LeftOf(this)
                );
            });
        }
    }
}

