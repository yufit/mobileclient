﻿
using System.Collections.Generic;
using System.Linq;

using Cirrious.FluentLayouts.Touch;

using UIKit;

using YuFit.Core.Extensions;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Profiles.Edit
{
    
    public class PersonalInfoContainer : UIView
    {
        readonly List<SeparatorLine> _separatorsLine = new List<SeparatorLine>();
        public readonly PlaceholderTextField Name    = new PlaceholderTextField();
        public readonly PlaceholderTextField Email   = new PlaceholderTextField();
        public readonly PlaceholderTextField Address = new PlaceholderTextField();

        public bool IsEditing {
            get {
                return Name.IsFirstResponder || Email.IsFirstResponder || Address.IsFirstResponder;
            }
        }

        public YuFit.Core.ViewModels.Profiles.Edit.EditProfileViewModel VM { get; set; }

        public PersonalInfoContainer ()
        {
            for (int i=0; i<4; ++i) { _separatorsLine.Add(new SeparatorLine()); }
            AddSubviews(_separatorsLine.ToArray());
            AddSubviews(new UIView[] {Name, Email, Address});

            Subviews.ForEach(v => { v.TranslatesAutoresizingMaskIntoConstraints = false; });

            Subviews.Where(v => v is SeparatorLine).ForEach(v => {
                v.AddConstraints(v.Height().EqualTo(1.0f));
                v.BackgroundColor = YuFitStyleKit.Yellow;

                this.AddConstraints(
                    v.Width().EqualTo().WidthOf(this),
                    v.WithSameCenterX(this)
                );
            });

            Subviews.Where(v => v is PlaceholderTextField).ForEach(v => {
                var textField               = v as PlaceholderTextField;
                textField.Font              = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
                textField.TextColor         = YuFitStyleKit.Yellow;
                textField.PlaceholderFont   = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
                textField.PlaceholderColor  = YuFitStyleKit.YellowDark;

                this.AddConstraints(
                    v.Width().EqualTo().WidthOf(this),
                    v.WithSameCenterX(this)
                );
            });

            this.AddConstraints(
                _separatorsLine[0]  .Top().EqualTo(     ).TopOf   ( this ),
                Name                .Top().EqualTo(9.75f).BottomOf( _separatorsLine[0] ),
                _separatorsLine[1]  .Top().EqualTo(5.75f).BottomOf( Name ),
                Email               .Top().EqualTo(9.75f).BottomOf( _separatorsLine[1] ),
                _separatorsLine[2]  .Top().EqualTo(5.75f).BottomOf( Email ),
                Address             .Top().EqualTo(9.75f).BottomOf( _separatorsLine[2] ),
                _separatorsLine[3]  .Top().EqualTo(5.75f).BottomOf( Address)
            );

            Name.KeyboardAppearance     = UIKeyboardAppearance.Dark;
            Email.KeyboardAppearance    = UIKeyboardAppearance.Dark;
            Address.KeyboardAppearance  = UIKeyboardAppearance.Dark;

            SetupFields();
        }

        void SetupFields()
        {
            Name.ClearButtonMode            = UITextFieldViewMode.Always;
            Name.ReturnKeyType              = UIReturnKeyType.Next;
            Name.AutocorrectionType         = UITextAutocorrectionType.No;
            Name.AutocapitalizationType     = UITextAutocapitalizationType.None;
            Email.ClearButtonMode           = UITextFieldViewMode.Always;
            Email.ReturnKeyType             = UIReturnKeyType.Next;
            Email.AutocorrectionType        = UITextAutocorrectionType.No;
            Email.AutocapitalizationType    = UITextAutocapitalizationType.None;
            Address.ClearButtonMode         = UITextFieldViewMode.Always;
            Address.ReturnKeyType           = UIReturnKeyType.Search;
            Address.AutocorrectionType      = UITextAutocorrectionType.No;
            Address.AutocapitalizationType  = UITextAutocapitalizationType.None;

            Name.ShouldReturn = new UITextFieldCondition(delegate {
                Email.BecomeFirstResponder();
                return true;
            });

            Email.ShouldReturn = new UITextFieldCondition(delegate {
                Address.BecomeFirstResponder();
                return true;
            });

            Address.ShouldReturn = new UITextFieldCondition(delegate {
                Address.ResignFirstResponder();
                VM.SearchHometownCommand.Command.Execute(null);
                return true;
            });
        }

        public override void MovedToSuperview ()
        {
            base.MovedToSuperview();
            if (Superview != null) {
                Superview.AddConstraints(
                    this.Bottom().EqualTo().BottomOf(_separatorsLine[3])
                );
            }
        }

    }
}

