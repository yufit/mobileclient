﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using UIKit;

using YuFit.IOS.Themes;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Views.Profiles.Edit
{
    public class SpecifyInterestContainer : UIView
    {
        public UILabel Label { get; private set; } 
        public UICollectionView ActivityCollectionView { get; private set; }
        public MvxCollectionViewSource Source { get; private set; }

        public SpecifyInterestContainer ()
        {
            ActivityCollectionView = new UICollectionView(Bounds, new UICollectionViewLayout());
            ActivityCollectionView.RegisterNibForCell(ActivityTypeSelectCollectionViewCell.Nib, ActivityTypeSelectCollectionViewCell.Key);

            Source = new MvxCollectionViewSource(ActivityCollectionView, ActivityTypeSelectCollectionViewCell.Key);
            ActivityCollectionView.Source = Source;
            ActivityCollectionView.SetCollectionViewLayout(new ActivityTypeSelectCollectionViewLayout(), false);

            Label = new UILabel();
            AddSubviews(new UIView[] {Label, ActivityCollectionView});

            ActivityCollectionView.TranslatesAutoresizingMaskIntoConstraints = false;
            Label.TranslatesAutoresizingMaskIntoConstraints = false;

            Label.Lines = 0;
            Label.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 19);
            Label.TextColor = YuFitStyleKit.White;

            this.AddConstraints(
                Label.WithSameHeight(this),
                Label.WithSameCenterY(this),
                Label.AtLeftOf(this, 0.0f),

                ActivityCollectionView.WithSameHeight(this),
                ActivityCollectionView.WithSameCenterY(this),
                ActivityCollectionView.Left().EqualTo(10.0f).RightOf(Label),
                ActivityCollectionView.Right().EqualTo().RightOf(this)
            );

            ActivityCollectionView.BackgroundColor = UIColor.Clear;
        }
    }
}

