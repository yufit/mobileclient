﻿using Cirrious.FluentLayouts.Touch;

using UIKit;

using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Profiles.Edit
{
    public class MetricOptionContainer : UIView
    {
        public readonly UISegmentedControl SegmentedControl = new UISegmentedControl();

        public MetricOptionContainer ()
        {
            AddSubviews(new UIView[] {SegmentedControl});

            SegmentedControl.InsertSegment("metrics", 0, false);
            SegmentedControl.InsertSegment("imperial", 1, false);

            SegmentedControl.TranslatesAutoresizingMaskIntoConstraints = false;

            SegmentedControl.TintColor = YuFitStyleKit.Yellow;

            UITextAttributes attributes = new UITextAttributes();
            attributes.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 19);
            SegmentedControl.SetTitleTextAttributes(attributes, UIControlState.Normal);

            this.AddConstraints(
                SegmentedControl.WithSameHeight(this),
                SegmentedControl.WithSameCenterY(this),
                SegmentedControl.Left().EqualTo(0.0f).LeftOf(this),
                SegmentedControl.Right().EqualTo().RightOf(this)
            );

        }
    }
}

