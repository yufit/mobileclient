﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using YuFit.Core.ViewModels.Notifications;
using YuFit.IOS.Extensions;

namespace YuFit.IOS.Views.Notifications
{
    public class NotificationsView : BaseViewController<NotificationsViewModel>
    {
        readonly NotificationTableView _tableView = new NotificationTableView(NotificationsTableViewCell.Key);

        public NotificationsView ()
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            AddTableView();
            SetupBindings();
        }

        void AddTableView()
        {
            _tableView.BindingContext = BindingContext;
            View.AddSubview(_tableView.View);
            //AddChildViewController(_tableView);

            _tableView.View.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddConstraints(
                _tableView.View.Top().EqualTo(0.0f).TopOf(View),
                _tableView.View.Left().EqualTo().LeftOf(View),
                _tableView.View.Bottom().EqualTo(0.0f).BottomOf(View),
                _tableView.View.Right().EqualTo().RightOf(View)
            );
        }

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<NotificationsView, NotificationsViewModel>();
            set.Bind().For(me => me.Title).To(vm => vm.DisplayName);
            set.Bind(_tableView.Source).To(vm => vm.Notifications);
            set.Bind(_tableView.Source).For(s => s.SelectionChangedCommand).To(vm => vm.HandleNotificationCommand.Command);
            set.Apply();
            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );
        }

    }
}

