﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using YuFit.Core.ViewModels;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using UIKit;
using YuFit.Core.Interfaces.Services;
using System.Linq;
using MvvmCross.Platform;

namespace YuFit.IOS.Views.Notifications
{
    public class PushNotificationView : DisplayReceivedNotificationView<PushNotificationViewModel>
    {
        readonly PushNotificationControl _pushControl = new PushNotificationControl();

        public PushNotificationView ()
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            View.BackgroundColor = Themes.YuFitStyleKit.Charcoal;

            _pushControl.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddSubview(_pushControl);

            View.AddConstraints(
                _pushControl.WithSameCenterX(this.View),
                _pushControl.Top().EqualTo(20).TopOf(this.View),
                _pushControl.WithSameWidth(this.View),
                _pushControl.WithSameBottom(this.View)
            );

            if (!string.IsNullOrEmpty(ViewModel.Sport)) {
                UIColor colorDark = UIColor.White;

                var categories = Mvx.Resolve<IFitActivityService>().GetEventCategories();
                var category = categories.FirstOrDefault(c => c.Name == ViewModel.Sport);
                if (category != null) {
                    var ccolors = category.Colors;
                    colorDark = new UIColor(ccolors.Dark.Red/255.0f, ccolors.Dark.Green/255.0f, ccolors.Dark.Blue/255.0f, 1.0f);
                } else {
                    ActivityColors colors;
                    colors = YuFitStyleKitExtension.ActivityBackgroundColors(ViewModel.Sport);
                    colorDark = colors.Dark;
                }

                View.BackgroundColor = colorDark;

                _pushControl.Icon.DrawIcon = YuFitStyleKitExtension.ActivityDrawingMethod(ViewModel.Sport);
            }

            var set = this.CreateBindingSet<PushNotificationView, PushNotificationViewModel>();
            set.Bind(_pushControl.MessageLabel).To(vm => vm.Message);
            set.Apply();
        }
    }
}

