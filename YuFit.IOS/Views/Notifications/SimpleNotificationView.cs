﻿using YuFit.Core.ViewModels;
using YuFit.IOS.Controls;

namespace YuFit.IOS.Views.Notifications
{
    public class SimpleNotificationView : DisplayReceivedNotificationView<SimpleNotificationViewModel>
    {
        public SimpleNotificationView ()
        {
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            View.BackgroundColor = Themes.YuFitStyleKit.Blue;
        }
    }
}

