using UIKit;
using YuFit.Core.ViewModels.Notifications;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Views.Notifications
{
    public class NotificationTableView : MvxTableViewController
    {
        protected new NotificationsViewModel ViewModel { get { return (NotificationsViewModel) base.ViewModel; } }
        public TableViewSource<NotificationsTableViewCell> Source { get; set;}

        readonly string _cellKey;

        public NotificationTableView (string cellKey)
        {
            _cellKey = cellKey;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            TableView.BackgroundColor = UIColor.Clear;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.RowHeight = 100;
            TableView.EstimatedRowHeight = 100;
            TableView.AllowsSelection = true;

            Source = new TableViewSource<NotificationsTableViewCell>(TableView, _cellKey);
            TableView.Source = Source;

            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
        }

        public override void ViewWillAppear (bool animated)
        {
            TableView.DeselectRow (TableView.IndexPathForSelectedRow, true);
            base.ViewDidAppear(animated);
        }
    }
    
}
