﻿using System;

using MvvmCross.Binding.BindingContext;

using UIKit;

using YuFit.Core.ViewModels;

using YuFit.IOS.Extensions;
using YuFit.IOS.Interfaces;

namespace YuFit.IOS.Views.Notifications
{
    public class DisplayReceivedNotificationView<Type> : BaseViewController<Type>, INotificationView where Type : BaseViewModel
    {
        public virtual nfloat Height
        {
            get { return 100f; }
        }


        public virtual new UIView View
        {
            get { return base.View; }
        }
           

        public DisplayReceivedNotificationView ()
        {
            ShowBackButton = false;
            IsRadialGradientVisible = false;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            var set = this.CreateBindingSet<DisplayReceivedNotificationView<Type>, DisplayReceivedNotificationViewModel>();
            set.Bind(View.Tap()).To(vm => vm.DismissCommand.Command);
            set.Bind(View.SwipeUp()).To(vm => vm.DismissCommand.Command);
            set.Apply();
        }


    }
}

