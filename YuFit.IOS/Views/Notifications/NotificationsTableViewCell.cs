using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Foundation;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Notifications;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using YuFit.Core.Interfaces.Services;
using System.Linq;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Platform;

namespace YuFit.IOS.Views.Notifications
{
    public class NotificationsTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString ("NotificationsTableViewCell");

        readonly PushNotificationControl    _pushControl        = new PushNotificationControl();
        readonly SeparatorLine              _bottomSeparator    = new SeparatorLine();
        readonly PictureContainer           _avatar             = new PictureContainer();
        readonly ActivityIconDiamondView    _activityIcon       = new ActivityIconDiamondView();

        public bool IsAcked {
            get { return ViewModel.IsAcked; }
            set { SetupIsAcked(value); }
        }

        NotificationViewModel ViewModel { get { return DataContext as NotificationViewModel; } }

        public NotificationsTableViewCell ()
        {
            _pushControl.TranslatesAutoresizingMaskIntoConstraints = false;
            _bottomSeparator.TranslatesAutoresizingMaskIntoConstraints = false;

            this.SelectionStyle = UITableViewCellSelectionStyle.None;

            ContentView.AddSubviews(new UIView[] {_pushControl, _bottomSeparator});

            ContentView.AddConstraints(
                _pushControl.WithSameCenterX    (ContentView),
                _pushControl.WithSameTop        (ContentView),
                _pushControl.WithSameWidth      (ContentView),
  
                _pushControl.Bottom().EqualTo(-1.0f).BottomOf(ContentView),

                _bottomSeparator.WithSameWidth  (ContentView),
                _bottomSeparator.WithSameCenterX(ContentView),
                _bottomSeparator.Height().EqualTo(1.0f),
                _bottomSeparator.Bottom().EqualTo().BottomOf(ContentView)
            );

            _bottomSeparator.BackgroundColor = YuFitStyleKit.Gray;

            this.DelayBind(() => {
                if (ViewModel is PromotedEventNotificationViewModel) {
                    if (_avatar.Superview == null) {
                        _pushControl.AddSubview(_avatar);
                        _avatar.TranslatesAutoresizingMaskIntoConstraints = false;
                        _pushControl.AddConstraints(
                            _avatar.WithSameWidth(_pushControl.Icon),
                            _avatar.WithSameHeight(_pushControl.Icon),
                            _avatar.WithSameCenterX(_pushControl.Icon),
                            _avatar.WithSameCenterY(_pushControl.Icon)
                        );
                    }

                    _avatar.Hidden = false;
                    _activityIcon.Hidden = true;

                    var set2 = this.CreateBindingSet<NotificationsTableViewCell, PromotedEventNotificationViewModel>();
                    set2.Bind(_avatar.ImageViewLoader).To(vm => vm.EventIconUrl);
                    set2.Bind(_pushControl.DateTimeLabel).To(vm => vm.WhenAsString);
                    set2.Bind(_pushControl.MessageLabel).To(vm => vm.Message);
                    set2.Bind().For(me => me.IsAcked).To(vm => vm.IsAcked);
                    set2.Apply();
                } else if (ViewModel is EventNotificationViewModel) {
                    var set2 = this.CreateBindingSet<NotificationsTableViewCell, EventNotificationViewModel>();
                    if (_activityIcon.Superview == null) {
                        _pushControl.AddSubview(_activityIcon);
                        _activityIcon.TranslatesAutoresizingMaskIntoConstraints = false;
                        _pushControl.AddConstraints(
                            _activityIcon.WithSameWidth(_pushControl.Icon),
                            _activityIcon.WithSameHeight(_pushControl.Icon),
                            _activityIcon.WithSameCenterX(_pushControl.Icon),
                            _activityIcon.WithSameCenterY(_pushControl.Icon)
                        );
                        //_activityIcon.Padding = 1.0f;
                    }
                    _avatar.Hidden = true;
                    _activityIcon.Hidden = false;

                    set2.Bind(_activityIcon).For("Activity").To(vm => vm.Sport);


//                    set2.Bind(ContentView).For(sv => sv.BackgroundColor).To(vm => vm.Sport);
//                    set2.Bind(_bottomSeparator).For("DarkBackgroundColor").To(vm => vm.Sport);
                    set2.Bind(_pushControl.DateTimeLabel).To(vm => vm.WhenAsString);
                    set2.Bind(_pushControl.MessageLabel).To(vm => vm.Message);
                    set2.Bind().For(me => me.IsAcked).To(vm => vm.IsAcked);
                    set2.Apply();

                } else if (ViewModel is GroupNotificationViewModel) {
                    if (_avatar.Superview == null) {
                        _pushControl.AddSubview(_avatar);
                        _avatar.TranslatesAutoresizingMaskIntoConstraints = false;
                        _pushControl.AddConstraints(
                            _avatar.WithSameWidth(_pushControl.Icon),
                            _avatar.WithSameHeight(_pushControl.Icon),
                            _avatar.WithSameCenterX(_pushControl.Icon),
                            _avatar.WithSameCenterY(_pushControl.Icon)
                        );
                    }

                    _avatar.Hidden = false;
                    _activityIcon.Hidden = true;

                    var set2 = this.CreateBindingSet<NotificationsTableViewCell, GroupNotificationViewModel>();
//                        set2.Bind(ContentView).For(sv => sv.BackgroundColor).To(vm => vm.Sport);
//                        set2.Bind(_bottomSeparator).For("DarkBackgroundColor").To(vm => vm.Sport);
                    set2.Bind(_avatar.ImageViewLoader).To(vm => vm.GroupAvatarUri);
                    set2.Bind(_pushControl.DateTimeLabel).To(vm => vm.WhenAsString);
                    set2.Bind(_pushControl.MessageLabel).To(vm => vm.Message);
                    set2.Bind().For(me => me.IsAcked).To(vm => vm.IsAcked);
                    set2.Apply();
                } else if (ViewModel is FriendNotificationViewModel) {
                    if (_avatar.Superview == null) {
                        _pushControl.AddSubview(_avatar);
                        _avatar.TranslatesAutoresizingMaskIntoConstraints = false;
                        _pushControl.AddConstraints(
                            _avatar.WithSameWidth(_pushControl.Icon),
                            _avatar.WithSameHeight(_pushControl.Icon),
                            _avatar.WithSameCenterX(_pushControl.Icon),
                            _avatar.WithSameCenterY(_pushControl.Icon)
                        );
                    }

                    _avatar.Hidden = false;
                    _activityIcon.Hidden = true;

                    var set2 = this.CreateBindingSet<NotificationsTableViewCell, FriendNotificationViewModel>();
                    //                        set2.Bind(ContentView).For(sv => sv.BackgroundColor).To(vm => vm.Sport);
                    //                        set2.Bind(_bottomSeparator).For("DarkBackgroundColor").To(vm => vm.Sport);
                    set2.Bind(_avatar.ImageViewLoader).To(vm => vm.AvatarUri);
                    set2.Bind(_pushControl.DateTimeLabel).To(vm => vm.WhenAsString);
                    set2.Bind(_pushControl.MessageLabel).To(vm => vm.Message);
                    set2.Bind().For(me => me.IsAcked).To(vm => vm.IsAcked);
                    set2.Apply();
                } else {
                    var set = this.CreateBindingSet<NotificationsTableViewCell, NotificationViewModel>();
                    set.Bind(_pushControl.MessageLabel).To(vm => vm.Message);
                    set.Bind(_pushControl.DateTimeLabel).To(vm => vm.WhenAsString);
                    set.Bind().For(me => me.IsAcked).To(vm => vm.IsAcked);
                    set.Apply();
                }
            });
        }

        void SetupIsAcked (bool isAcked)
        {
            UIColor colorDark = UIColor.White;
            UIColor colorBase = UIColor.White;

            var eventNotificationViewModel = ViewModel as EventNotificationViewModel;
            if (eventNotificationViewModel != null && !string.IsNullOrEmpty(eventNotificationViewModel.Sport)) {
                var categories = Mvx.Resolve<IFitActivityService>().GetEventCategories();
                var category = categories.FirstOrDefault(c => c.Name == eventNotificationViewModel.Sport);
                if (category != null) {
                    var ccolors = category.Colors;
                    colorDark = new UIColor(ccolors.Dark.Red/255.0f, ccolors.Dark.Green/255.0f, ccolors.Dark.Blue/255.0f, 1.0f);
                    colorBase = new UIColor(ccolors.Base.Red/255.0f, ccolors.Base.Green/255.0f, ccolors.Base.Blue/255.0f, 1.0f);
                } else {
                    ActivityColors colors;
                    colors = YuFitStyleKitExtension.ActivityBackgroundColors(eventNotificationViewModel.Sport);
                    colorDark = colors.Dark;
                    colorBase = colors.Base;
                }
            }

            _bottomSeparator.BackgroundColor = colorDark;
            ContentView.BackgroundColor = YuFitStyleKit.Black;
            _pushControl.MessageLabel.TextColor = colorDark;
            _pushControl.DateTimeLabel.TextColor = colorDark;

            _activityIcon.BackgroundColor = colorDark;
            _activityIcon.SetNeedsDisplay();
        }
    }
    
}
