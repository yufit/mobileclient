﻿using System;
using YuFit.Core.ViewModels;
using UIKit;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

namespace YuFit.IOS.Views
{
    public class CommingSoonView : BaseViewController<CommingSoonViewModel>
    {
        UILabel _comingSoonMessage = new UILabel();

        public CommingSoonView ()
        {
            
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            _comingSoonMessage.TranslatesAutoresizingMaskIntoConstraints = false;
            _comingSoonMessage.TextColor = Themes.YuFitStyleKit.White;
            _comingSoonMessage.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
            _comingSoonMessage.Lines = 0;
            _comingSoonMessage.TextAlignment = UITextAlignment.Center;
            View.Add(_comingSoonMessage);
            View.AddConstraints(
                _comingSoonMessage.WithSameCenterX(View),
                _comingSoonMessage.WithSameCenterY(View),
                _comingSoonMessage.WithSameWidth(View).WithMultiplier(0.7f)
            );

            var set = this.CreateBindingSet<CommingSoonView, CommingSoonViewModel>();
            set.Bind(_comingSoonMessage).To(vm => vm.ComingSoonMessage);
            set.Apply();

        }
    }
}

