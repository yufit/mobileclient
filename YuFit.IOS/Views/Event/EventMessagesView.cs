﻿using System;
using System.Collections.Generic;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using CoreFoundation;
using CoreGraphics;
using Foundation;
using PHFComposeBarView;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.Messages;
using YuFit.Core.ViewModels.Event;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;
using MvvmCross.Plugins.Messenger;
using MvvmCross.Platform;

namespace YuFit.IOS.Views.Event
{
    public class EventMessagesView : BaseViewController<EventMessagesViewModel>
    {
        readonly MessagesTableView _commentsTableView    = new MessagesTableView();
        private List<NSObject> _keyboardObservers = new List<NSObject>();
        ComposeBarView composeBarView;
        readonly UIBarButtonItem _addCommentButton = new UIBarButtonItem(UIBarButtonSystemItem.Add, null);

        #pragma warning disable 414
        readonly MvxSubscriptionToken _commentAddedMessageToken;
        #pragma warning restore 414

        public EventMessagesView ()
        {
            var messenger = Mvx.Resolve<IMvxMessenger>();
            _commentAddedMessageToken = messenger.Subscribe<EventCommentAddedMessage>(OnCommentAdded);
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();
            _commentsTableView.BindingContext = BindingContext;

            View.AddSubviews(new UIView[] {_commentsTableView.View});

            View.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            View.AddConstraints(
                _commentsTableView.View.WithSameWidth(View),
                _commentsTableView.View.WithSameHeight(View),
                _commentsTableView.View.WithSameCenterX(View),
                _commentsTableView.View.WithSameCenterY(View)
            );

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );

            composeBarView = new ComposeBarView (new System.Drawing.RectangleF (0, (float) View.Bounds.Height, (float) View.Bounds.Width, ComposeBarView.InitialHeight)) {
                MaxCharCount = 160,
                MaxLinesCount = 6,
                Placeholder = "Type something...",
                UtilityButtonImage = UIImage.FromBundle ("Camera")
            };

            composeBarView.Hidden = true;
            View.AddSubview (composeBarView);

            var set = this.CreateBindingSet<EventMessagesView, EventMessagesViewModel>();
            set.Bind(_commentsTableView.Source).To(vm => vm.Comments);
            set.Bind().For(me => me.Title).To(vm => vm.DisplayName);

            set.Bind(composeBarView.PlaceholderLabel).To(vm => vm.AddCommentPlaceholder);
            set.Bind(composeBarView).For("DidPressButton").To(vm => vm.SendCommentCommand.Command);

            set.Apply();

            NavigationItem.RightBarButtonItem = _addCommentButton;
            _addCommentButton.TintColor = YuFitStyleKit.White;

        }

        public override void ViewDidAppear (bool animated)
        {
            _addCommentButton.Clicked += HandleAddComment;

            _keyboardObservers.Add(NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, HandleKeyboardWillShow));
            _keyboardObservers.Add(NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, HandleKeyboardWillHide));
            base.ViewDidAppear(animated);
        }

        public override void ViewDidDisappear (bool animated)
        {
            _keyboardObservers.ForEach (NSNotificationCenter.DefaultCenter.RemoveObserver);
            _addCommentButton.Clicked -= HandleAddComment;

            var messenger = Mvx.Resolve<IMvxMessenger>();
            messenger.Unsubscribe<EventCommentAddedMessage>(_commentAddedMessageToken);
            base.ViewDidDisappear(animated);
        }

        void HandleAddComment (object sender, EventArgs e)
        {
            composeBarView.BecomeFirstResponder();
        }

        void HandleKeyboardWillShow(NSNotification notification)
        {
            // Retrieve keyboard size information.
            NSDictionary info = notification.UserInfo;
            NSObject frameBeginInfo = info[UIKeyboard.FrameBeginUserInfoKey];
            CGSize kbSize = (frameBeginInfo as NSValue).RectangleFValue.Size;

            nfloat keyboardTop = View.Bounds.Height - kbSize.Height;

            CGRect frame = composeBarView.Frame;
            frame.Y = keyboardTop - composeBarView.Frame.Height;
            composeBarView.Frame = frame;
            composeBarView.Hidden = false;
        }

        void HandleKeyboardWillHide(NSNotification notification)
        {
            CGRect frame = composeBarView.Frame;
            frame.Y = View.Frame.Height;
            composeBarView.Frame = frame;
            composeBarView.Hidden = true;
        }

        void OnCommentAdded (EventCommentAddedMessage obj)
        {
            // NOTE: It is necessary to delay the scroll operation or it doesn't scroll
            // properly.
            DispatchQueue.DefaultGlobalQueue.DispatchAfter(
                new DispatchTime(DispatchTime.Now, 50000000), 
                () => BeginInvokeOnMainThread(
                    () => _commentsTableView.TableView.ScrollToRow(
                        NSIndexPath.FromItemSection(ViewModel.Comments.Count - 1, 0), 
                        UITableViewScrollPosition.Bottom, 
                        true
                    )
                )
            );
        }
    }
}

