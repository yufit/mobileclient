﻿using System;
using UIKit;
using Foundation;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Views.Event
{
    public class TableViewSource<TCell> : MvxTableViewSource where TCell : UITableViewCell, new()
    {
        readonly string _key;
        public TableViewSource (UITableView tableView, string key) : base(tableView)
        {
            _key = key;
        }

        protected override UITableViewCell GetOrCreateCellFor (UITableView tableView, NSIndexPath indexPath, object item)
        {
            var cell = tableView.DequeueReusableCell(_key) ?? new TCell();

            cell.SetNeedsUpdateConstraints();
            cell.UpdateConstraintsIfNeeded();

            return cell;
        }
    }
    public class MessagesTableView : MvxTableViewController
    {


        public TableViewSource<MessagesTableViewCell> Source { get; set; }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            TableView.BackgroundColor = UIColor.Clear;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.RowHeight = UITableView.AutomaticDimension;
            TableView.EstimatedRowHeight = 100;
            TableView.AllowsSelection = false;

            Source = new TableViewSource<MessagesTableViewCell>(TableView, MessagesTableViewCell.Key);
            TableView.Source = Source;
        }
    }
}

