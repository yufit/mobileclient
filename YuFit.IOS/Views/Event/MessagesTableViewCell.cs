﻿using System;
using Foundation;
using UIKit;
using YuFit.IOS.Controls;
using YuFit.Core.Models;
using YuFit.Core.ViewModels.Event;
using YuFit.IOS.Themes;
using MvvmCross.Binding.BindingContext;
using YuFit.IOS.Extensions;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Views.Event
{
    public class MessagesTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString ("CommentTableViewCell");

        readonly UILabel        _userNameLabel      = new UILabel();
        readonly UILabel        _postedWhenLabel    = new UILabel();
        readonly UILabel        _commentLabel       = new UILabel();
        readonly IconView       _deleteIcon         = new IconView();

        readonly SeparatorLine  _bottomSeparator    = new SeparatorLine();

        public Colors Colors {
            get { return ((EventMessageViewModel)DataContext).Colors; }
            set { UpdateContent(value); }
        }

        private bool _iAmTheOwner;
        public bool IAmTheOwner {
            get { return _iAmTheOwner; }
            set { _iAmTheOwner = value; UpdateDeleteButton(); }
        }

        ~MessagesTableViewCell ()
        {
            System.Console.WriteLine("DELETING CELLS");
        }
        public MessagesTableViewCell ()
        {
            BackgroundColor = UIColor.Clear;

            _userNameLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _userNameLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 16);
            _userNameLabel.Lines = 0;
            _userNameLabel.LineBreakMode = UILineBreakMode.TailTruncation;
            ContentView.AddSubview(_userNameLabel);

            _commentLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _commentLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            _commentLabel.Lines = 0;
            _commentLabel.LineBreakMode = UILineBreakMode.TailTruncation;
            ContentView.AddSubview(_commentLabel);

            _postedWhenLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _postedWhenLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 10);
            _postedWhenLabel.Lines = 0;
            _postedWhenLabel.LineBreakMode = UILineBreakMode.TailTruncation;
            _postedWhenLabel.TextAlignment = UITextAlignment.Right;
            ContentView.AddSubview(_postedWhenLabel);

            _bottomSeparator.TranslatesAutoresizingMaskIntoConstraints = false;
            ContentView.AddSubview(_bottomSeparator);

            _deleteIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawClose2");
            _deleteIcon.TranslatesAutoresizingMaskIntoConstraints = false;
            _deleteIcon.Hidden = true;
            ContentView.AddSubview(_deleteIcon);

            SetupConstraints();

            this.DelayBind(() => {
                var set = this.CreateBindingSet<MessagesTableViewCell, EventMessageViewModel>();
                set.Bind().For(me => me.Colors).To(vm => vm.Colors);
                set.Bind().For(me => me.IAmTheOwner).To(vm => vm.IAmTheOwner);
                set.Bind(_commentLabel).To(vm => vm.Comment);
                set.Bind(_userNameLabel).To(vm => vm.UserName);
                set.Bind(_postedWhenLabel).To(vm => vm.PostedWhen);
                set.Bind(_deleteIcon.Tap()).To(vm => vm.DeleteCommand.Command);
                set.Apply();
            });
        }

        void SetupConstraints ()
        {
            _bottomSeparator.AddConstraints(_bottomSeparator.Height().EqualTo(0.5f));

            ContentView.AddConstraints(
                _userNameLabel.Top().EqualTo(5.0f).TopOf(ContentView),
                _userNameLabel.Left().EqualTo(20.0f).LeftOf(ContentView),

                _postedWhenLabel.Top().EqualTo(-1.5f).BottomOf(_userNameLabel),
                _postedWhenLabel.Left().EqualTo(20.0f).LeftOf(ContentView),

                _deleteIcon.Height().EqualTo().HeightOf(_userNameLabel),
                _deleteIcon.Width().EqualTo().HeightOf(_userNameLabel),
                _deleteIcon.Right().EqualTo(-20.0f).RightOf(ContentView),
                _deleteIcon.CenterY().EqualTo(-2.5f).CenterYOf(_userNameLabel),

                _commentLabel.Top().EqualTo(5.0f).BottomOf(_postedWhenLabel),
                _commentLabel.Left().EqualTo(20.0f).LeftOf(ContentView),
                _commentLabel.Right().EqualTo(-20.0f).RightOf(ContentView),

                _bottomSeparator.Top().EqualTo(5.0f).BottomOf(_commentLabel),
                _bottomSeparator.Left().EqualTo(20.0f).LeftOf(ContentView),
                _bottomSeparator.Right().EqualTo(-20.0f).RightOf(ContentView)
            );

            ContentView.AddConstraints(
                ContentView.Bottom().EqualTo(15.0f).BottomOf(_bottomSeparator)
            );
        }

        void UpdateContent (Colors colors)
        {
            if (colors != null) {
                _userNameLabel.TextColor = colors.Light.ToUIColor();
                _commentLabel.TextColor = colors.Light.ToUIColor();
                _postedWhenLabel.TextColor = colors.Light.ToUIColor();
                _bottomSeparator.BackgroundColor = colors.Light.ToUIColor();
                _deleteIcon.StrokeColor = colors.Light.ToUIColor();
            }
        }

        void UpdateDeleteButton ()
        {
            _deleteIcon.Hidden = !_iAmTheOwner;
            _deleteIcon.SetNeedsDisplay();
        }
    }
}

