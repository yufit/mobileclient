﻿using System;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Coc.MvvmCross.Plugins.Location;

using CoreGraphics;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.Models;
using YuFit.Core.ViewModels.Activity.Summary;
using YuFit.Core.ViewModels.Dashboard.ActivitySummary;

using YuFit.IOS.Controls;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;
using YuFit.IOS.Views.Activity.Summary;
using YuFit.Core.ViewModels.Event;
using System.Linq;
using CoreLocation;
using MapKit;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Views.Event
{
    public class EventView : BaseViewController<EventViewModel>
    {
        private Colors _colors;
        public Colors Colors {
            get { return _colors; }
            set { _colors = value; UpdateContent(); }
        }

        private bool _isExpired;
        public bool IsExpired {
            get { return _isExpired; }
            set { _isExpired = value; ShowExpiredView(value); }
        }

        private bool _isCanceled;
        public bool IsCanceled {
            get { return _isCanceled; }
            set { _isCanceled = value; ShowCanceledView(value); }
        }

        private bool _isLoaded;
        public bool IsLoaded {
            get { return _isLoaded; }
            set { _isLoaded = value; if (_isLoaded) { LoadUI(); } }
        }

        UITapGestureRecognizer _addressTap;

        // Analysis disable ValueParameterNotUsed
        public string Bio           { get { return ViewModel.Description;     } set { UpdateDescription();      } }
        // Analysis restore ValueParameterNotUsed

        readonly UIScrollView           _topContainer       = new UIScrollView();
        readonly UIView                 _photoContainerView = new UIView();
        readonly UIImageView            _photoView          = new UIImageView();
        readonly PictureContainer       _avatarView         = new PictureContainer();
        readonly PlaceholderTextView    _descriptionLabel   = new PlaceholderTextView();

        readonly UIView                 _infoContainer      = new UIView();

        readonly UIView                 _topInfoContainer   = new UIView();
        readonly UIView                 _dateContainer      = new UIView();
        readonly WhenView               _whenLabels         = new WhenView();
        readonly SeparatorLine          _firstLine          = new SeparatorLine();
        readonly UIView                 _participantsView   = new UIView();
        readonly UIView                 _participantLabelContainer = new UIView();
        readonly UILabel                _countLabel         = new UILabel();
        readonly UILabel                _inscriptionLabel   = new UILabel();
        readonly SeparatorLine          _secondLine         = new SeparatorLine();
        readonly UIView                 _joinButton         = new UIView();
        readonly UILabel                _joinLabel          = new UILabel();

        readonly UIView                 _bottomInfoContainer= new UIView();
        readonly UIView                 _locationContainer  = new UIView();
        readonly IconView               _locationIcon       = new IconView(YuFitStyleKit.White, "Where1");
        readonly UILabel                _addressLabel       = new UILabel();

        readonly UIView                 _toolbarContainer   = new UIView();
        readonly UIView                 _btnsContainer      = new UIView();
        readonly IconView               _websiteButton      = new IconView(YuFitStyleKit.White, "Globe");
        readonly IconView               _facebookButton     = new IconView(YuFitStyleKit.White, "Facebook");
        readonly IconView               _messagesButton     = new IconView(YuFitStyleKit.White, "MessagesIcon");

        readonly UIBarButtonItem        _shareButton        = new UIBarButtonItem(UIBarButtonSystemItem.Action);
        readonly CancelledActivityView  _cancelledView      = new CancelledActivityView();
        readonly ExpiredActivityView    _expiredView        = new ExpiredActivityView();
        readonly MvxImageViewLoader     _imageViewLoader;

        NSLayoutConstraint _descriptionHeightConstraint;

        public EventView ()
        {
            _imageViewLoader = new MvxImageViewLoader(() => _photoView);
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            var set = this.CreateBindingSet<EventView, EventViewModel>();
            set.Bind().For(me => me.IsLoaded).To(vm => vm.IsLoaded);
            set.Apply();

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );

            _addressTap = new UITapGestureRecognizer(() => {
                var location = (Location) ViewModel.Location;
                var latitude = location.GeoCoordinates.Latitude;
                var longitude = location.GeoCoordinates.Longitude;
                var coordinates = new CLLocationCoordinate2D(latitude, longitude);

                var placemark = new MKPlacemark(coordinates, new MKPlacemarkAddress());
                var mapItem = new MKMapItem(placemark);
                mapItem.Name = ViewModel.DisplayName;

                var launchOptions = new MKLaunchOptions();
                mapItem.OpenInMaps(launchOptions);
            });
        }

        public override void ViewWillAppear (bool animated)
        {
            _bottomInfoContainer.AddGestureRecognizer(_addressTap);
            base.ViewWillAppear(animated);
        }

        public override void ViewWillDisappear (bool animated)
        {
            _bottomInfoContainer.RemoveGestureRecognizer(_addressTap);
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidAppear (bool animated)
        {
            base.ViewDidAppear(animated);
            _shareButton.Clicked += HandleShareAction;
        }

        public override void ViewDidDisappear (bool animated)
        {
            base.ViewDidDisappear(animated);
            _shareButton.Clicked -= HandleShareAction;
        }

        void HandleShareAction (object sender, EventArgs e)
        {
            var dummy = new YuFit.IOS.Utilities.ShareActivity(ParentViewController, NavigationController.ViewControllers[0].View);
            dummy.Present();
        }

        void LoadUI() 
        {
            CreateViewHierarchy();
            ConfigureViewSpecifics();
            SetupConstraints();
            SetupBindings();
        }

        void CreateViewHierarchy ()
        {
            View.AddSubviews(new UIView[] {_topContainer, _infoContainer, _toolbarContainer});
            _topContainer.AddSubviews(new UIView[] {_photoContainerView, _avatarView, _descriptionLabel});
            _photoContainerView.AddSubview(_photoView);
            _infoContainer.AddSubviews(new [] {_topInfoContainer, _bottomInfoContainer});
            _topInfoContainer.AddSubviews(new [] {_dateContainer, _firstLine, _participantsView, _secondLine, _joinButton});
            _dateContainer.AddSubview(_whenLabels);
            _participantsView.AddSubviews(new [] {_participantLabelContainer});
            _participantLabelContainer.AddSubviews(new [] {_countLabel, _inscriptionLabel});
            _joinButton.AddSubview(_joinLabel);
            _bottomInfoContainer.AddSubview(_locationContainer);
            _locationContainer.AddSubviews(new UIView[] {_locationIcon, _addressLabel});
            _toolbarContainer.AddSubviews(new [] {_btnsContainer});
            _btnsContainer.AddSubviews(new UIView[] {_messagesButton, _websiteButton, _facebookButton});
        }

        void ConfigureViewSpecifics ()
        {
            _toolbarContainer.BackgroundColor = UIColor.Black;
            _bottomInfoContainer.BackgroundColor = UIColor.Red;
            _topContainer.BackgroundColor = UIColor.Black;

            _photoContainerView.BackgroundColor = YuFitStyleKit.Black;
            _photoContainerView.AddSubview(_photoView);
            _photoView.ContentMode = UIViewContentMode.ScaleAspectFit;
            _photoView.ClipsToBounds = true;
            _photoContainerView.ClipsToBounds = true;

            _descriptionLabel.TextColor = YuFitStyleKit.White;
            _descriptionLabel.TextAlignment = UITextAlignment.Center;
            _descriptionLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 20);
            _descriptionLabel.Editable = false;
            _descriptionLabel.BackgroundColor = UIColor.Clear;
            _descriptionLabel.ScrollEnabled = false;

            _whenLabels.DayLabelFont = CrudFitEvent.WhenDiamond.DayLabelTextFont;
            _whenLabels.MonthAndDateLabelFont = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
            _whenLabels.TimeLabelFont = CrudFitEvent.WhenDiamond.TimeLabelTextFont;

            _countLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 24);
            _countLabel.TextColor = UIColor.White;

            _inscriptionLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            _inscriptionLabel.TextColor = UIColor.White;

            _joinLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 24);
            _joinLabel.Lines = 0;
            _joinLabel.TextAlignment = UITextAlignment.Center;
            _joinLabel.TextColor = UIColor.White;

            _addressLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            _addressLabel.TextColor = UIColor.White;

            _firstLine.BackgroundColor = UIColor.Black;
            _secondLine.BackgroundColor = UIColor.Black;
        }

        /// <summary>
        /// Setups the constraints.
        /// </summary>
        /// ----------------------------------------------------------------------------------------------------
        void SetupConstraints ()
        {
            View.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            View.AddConstraints(
                _topContainer.WithSameLeft(View),
                _topContainer.WithSameRight(View),
                _topContainer.Top().EqualTo().TopOf(View),

                _toolbarContainer.WithSameLeft(View),
                _toolbarContainer.WithSameRight(View),
                _toolbarContainer.Height().EqualTo(76.0f),
                _toolbarContainer.WithSameBottom(View),

                _infoContainer.WithSameWidth(View),
                _infoContainer.WithSameCenterX(View),
                _infoContainer.Height().EqualTo(128.0f),
                _infoContainer.Bottom().EqualTo().TopOf(_toolbarContainer),

                _topContainer.Bottom().EqualTo().TopOf(_infoContainer)
            );

            SetupTopContainerConstraints();
            SetupInfoContainerConstraints();
            SetupToolbalContainerConstraints();
        }

        /// <summary>
        /// Setups the top container constraints.
        /// </summary>
        /// ----------------------------------------------------------------------------------------------------
        void SetupTopContainerConstraints ()
        {
            _topContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            View.AddConstraints(
                _photoContainerView.Width().EqualTo().WidthOf(View),
                _photoContainerView.Height().EqualTo().WidthOf(View).WithMultiplier(0.5531401f),
                _photoContainerView.WithSameCenterX(View),

                _avatarView.Width().EqualTo(132.0f),
                _avatarView.Height().EqualTo().WidthOf(_avatarView),
                _avatarView.CenterY().EqualTo().BottomOf(_photoContainerView),
                _avatarView.CenterX().EqualTo().CenterXOf(View),

                _descriptionLabel.Left().EqualTo(20).LeftOf(View),
                _descriptionLabel.Right().EqualTo(-20).RightOf(View),
                _descriptionLabel.Top().EqualTo(20.0f).BottomOf(_avatarView)
            );

            SetupPictureContainerConstraints();
            SetupDescriptionConstraints();

            _topContainer.AddConstraints(
                _photoContainerView.Top().EqualTo().TopOf(_topContainer)
            );

            _topContainer.AddConstraints(
                _descriptionLabel.Bottom().EqualTo(-Spacing).BottomOf(_topContainer)
            );
        }

        /// <summary>
        /// Setups the picture container constraints.
        /// </summary>
        /// ----------------------------------------------------------------------------------------------------
        void SetupPictureContainerConstraints ()
        {
            _photoContainerView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _photoContainerView.AddConstraints(
                _photoView.WithSameWidth(_photoContainerView),
                _photoView.WithSameTop(_photoContainerView),
                _photoView.WithSameCenterX(_photoContainerView),
                _photoView.WithSameHeight(_photoContainerView)
            );
        }

        /// <summary>
        /// Setups the group description constraints.
        /// </summary>
        /// ----------------------------------------------------------------------------------------------------
        void SetupDescriptionConstraints ()
        {
            var _desiredHeight = _descriptionLabel.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
            _descriptionHeightConstraint = _descriptionLabel.Height().EqualTo(_desiredHeight).ToLayoutConstraints().First();
            _descriptionLabel.AddConstraint(_descriptionHeightConstraint);
        }

        /// <summary>
        /// Setups the info container constraints.
        /// </summary>
        /// ----------------------------------------------------------------------------------------------------
        void SetupInfoContainerConstraints ()
        {
            _infoContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _infoContainer.AddConstraints(
                _topInfoContainer.WithSameLeft(_infoContainer),
                _topInfoContainer.WithSameRight(_infoContainer),
                _topInfoContainer.Top().EqualTo().TopOf(_infoContainer),
                _topInfoContainer.Height().EqualTo(80.0f),

                _bottomInfoContainer.WithSameWidth(_infoContainer),
                _bottomInfoContainer.WithSameCenterX(_infoContainer),
                _bottomInfoContainer.Height().EqualTo(48.0f),
                _bottomInfoContainer.Bottom().EqualTo().BottomOf(_infoContainer)
            );
            SetupTopInfoContainerConstraints();
            SetupLocationContainerConstraints();
        }

        /// <summary>
        /// Setups the top info container constraints.
        /// </summary>
        /// ----------------------------------------------------------------------------------------------------
        void SetupTopInfoContainerConstraints ()
        {
            _topInfoContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _topInfoContainer.AddConstraints(
                _joinButton.WithSameHeight(_topInfoContainer),
                _joinButton.WithSameRight(_topInfoContainer),
                _joinButton.WithSameCenterY(_topInfoContainer),
                _joinButton.Width().EqualTo(95.0f),

                _secondLine.WithSameCenterY(_topInfoContainer),
                _secondLine.WithSameHeight(_topInfoContainer),
                _secondLine.Width().EqualTo(2.0f),
                _secondLine.Right().EqualTo().LeftOf(_joinButton),

                _participantsView.WithSameHeight(_topInfoContainer),
                _participantsView.WithSameCenterY(_topInfoContainer),
                _participantsView.Right().EqualTo().LeftOf(_secondLine),
                _participantsView.Width().EqualTo(95.0f),

                _firstLine.WithSameCenterY(_topInfoContainer),
                _firstLine.WithSameHeight(_topInfoContainer),
                _firstLine.Width().EqualTo(2.0f),
                _firstLine.Right().EqualTo().LeftOf(_participantsView),

                _dateContainer.WithSameHeight(_topInfoContainer),
                _dateContainer.WithSameCenterY(_topInfoContainer),
                _dateContainer.Right().EqualTo().LeftOf(_firstLine),
                _dateContainer.Left().EqualTo().LeftOf(_topInfoContainer)
            );

            SetupJoinButtonConstraints();
            SetupParticipantsContainerConstraints();
            SetupWhenContainerConstraints();
        }

        void SetupWhenContainerConstraints ()
        {
            _dateContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _dateContainer.AddConstraints(
                _whenLabels.WithSameCenterX(_dateContainer),
                _whenLabels.CenterY().EqualTo(3.0f).CenterYOf(_dateContainer),
                _whenLabels.WithSameHeight(_dateContainer),
                _whenLabels.WithSameWidth(_dateContainer)
            );
        }

        /// <summary>
        /// Setups the participants container constraints.
        /// </summary>
        /// ----------------------------------------------------------------------------------------------------
        void SetupParticipantsContainerConstraints ()
        {
            _participantsView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _participantsView.AddConstraints(
                _participantLabelContainer.WithSameCenterX(_participantsView),
                _participantLabelContainer.WithSameCenterY(_participantsView)
            );

            _participantLabelContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _participantLabelContainer.AddConstraints(
                _countLabel.Top().EqualTo().TopOf(_participantLabelContainer),
                _countLabel.WithSameCenterX(_participantLabelContainer),

                _inscriptionLabel.Top().EqualTo().BottomOf(_countLabel),
                _inscriptionLabel.WithSameCenterX(_participantLabelContainer),

                _participantLabelContainer.Right().EqualTo().RightOf(_inscriptionLabel),
                _participantLabelContainer.Bottom().EqualTo().BottomOf(_inscriptionLabel)
            );

        }

        /// <summary>
        /// Setups the join button constraints.
        /// </summary>
        /// ----------------------------------------------------------------------------------------------------
        void SetupJoinButtonConstraints()
        {
            _joinButton.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _joinButton.AddConstraints(
                _joinLabel.WithSameCenterX(_joinButton),
                _joinLabel.CenterY().EqualTo(3.0f).CenterYOf(_joinButton)
            );
        }

        /// <summary>
        /// Setups the location container constraints.
        /// </summary>
        /// ----------------------------------------------------------------------------------------------------
        void SetupLocationContainerConstraints()
        {
            _bottomInfoContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _bottomInfoContainer.AddConstraints(
                _locationContainer.WithSameCenterX(_bottomInfoContainer),
                _locationContainer.WithSameCenterY(_bottomInfoContainer)
            );

            _locationContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _locationContainer.AddConstraints(
                _locationIcon.Left().EqualTo().LeftOf(_locationContainer),
                _locationIcon.WithSameCenterY(_locationContainer),
                _locationIcon.Height().EqualTo(3.0f).HeightOf(_addressLabel),
                _locationIcon.Width().EqualTo(3.0f).HeightOf(_addressLabel),

                _addressLabel.Left().EqualTo(5.0f).RightOf(_locationIcon),
                _addressLabel.CenterY().EqualTo(2.0f).CenterYOf(_locationContainer),

                _locationContainer.Right().EqualTo().RightOf(_addressLabel),
                _locationContainer.Bottom().EqualTo().BottomOf(_locationIcon)
            );
        }

        /// <summary>
        /// Setups the toolbal container constraints.
        /// </summary>
        /// ----------------------------------------------------------------------------------------------------
        void SetupToolbalContainerConstraints ()
        {
            //_toolbarContainer.ClipsToBounds = true;
            _toolbarContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _toolbarContainer.AddConstraints(
                _btnsContainer.WithSameCenterX(_toolbarContainer),
                _btnsContainer.WithSameCenterY(_toolbarContainer),
                _btnsContainer.Height().EqualTo(34.0f),
                _btnsContainer.Width().EqualTo().WidthOf(_toolbarContainer)
            );

            var _spacer0 = new UIView();
            var _spacer1 = new UIView();
            var _spacer2 = new UIView();
            var _spacer3 = new UIView();

            _btnsContainer.AddSubviews(new [] {_spacer0, _spacer1, _spacer2, _spacer3});

            _btnsContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _btnsContainer.AddConstraints(
                _spacer0.Left().EqualTo().LeftOf(_btnsContainer),
                _spacer0.Right().EqualTo().LeftOf(_messagesButton),
                _spacer0.WithSameCenterY(_btnsContainer),
                _spacer0.WithSameHeight(_btnsContainer),

                _messagesButton.Left().EqualTo().RightOf(_spacer0),
                _messagesButton.WithSameHeight(_btnsContainer),
                _messagesButton.WithSameCenterY(_btnsContainer),
                _messagesButton.Width().EqualTo().HeightOf(_messagesButton),

                _spacer1.Left().EqualTo().RightOf(_messagesButton),
                _spacer1.Right().EqualTo().LeftOf(_websiteButton),
                _spacer1.WithSameCenterY(_btnsContainer),
                _spacer1.WithSameHeight(_btnsContainer),
                _spacer1.WithSameWidth(_spacer0),

                _websiteButton.Left().EqualTo().RightOf(_spacer1),
                _websiteButton.WithSameHeight(_btnsContainer),
                _websiteButton.WithSameCenterY(_btnsContainer),
                _websiteButton.Width().EqualTo().HeightOf(_websiteButton),

                _spacer2.Left().EqualTo().RightOf(_websiteButton),
                _spacer2.Right().EqualTo().LeftOf(_facebookButton),
                _spacer2.WithSameCenterY(_btnsContainer),
                _spacer2.WithSameHeight(_btnsContainer),
                _spacer2.WithSameWidth(_spacer1),

                _facebookButton.Left().EqualTo().RightOf(_spacer2),
                _facebookButton.WithSameHeight(_btnsContainer),
                _facebookButton.WithSameCenterY(_btnsContainer),
                _facebookButton.Width().EqualTo().HeightOf(_facebookButton),

                _spacer3.Left().EqualTo().RightOf(_facebookButton),
                _spacer3.Right().EqualTo().RightOf(_btnsContainer),
                _spacer3.WithSameCenterY(_btnsContainer),
                _spacer3.WithSameHeight(_btnsContainer),
                _spacer3.WithSameWidth(_spacer2)
            );
        }

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<EventView, EventViewModel>();
            set.Bind().For(me => me.Title).To(vm => vm.DisplayName);

            set.Bind(_avatarView.ImageViewLoader).To(vm => vm.Avatar);
            set.Bind(_imageViewLoader).To(vm => vm.Image);

            set.Bind().For(me => me.Bio).To(vm => vm.Description);

            set.Bind(_countLabel).To(vm => vm.GoingCount);
            set.Bind(_inscriptionLabel).To(vm => vm.InvitedTitle);
            set.Bind(_whenLabels).For(w => w.Date).To(vm => vm.StartTime);

            set.Bind(_messagesButton.Tap()).To(vm => vm.ShowCommentsCommand.Command);
            set.Bind(_websiteButton.Tap()).To(vm => vm.OpenWebSiteCommand.Command);
            set.Bind(_facebookButton.Tap()).To(vm => vm.OpenFacebookCommand.Command);

            set.Bind(_joinButton.Tap()).To(vm => vm.JoinCommand.Command);
            set.Bind(_joinLabel).To(vm => vm.IamInString);

            set.Bind(_addressLabel).To(vm => vm.Address);
            set.Bind().For(me => me.Colors).To(vm => vm.Colors);

            set.Bind(_cancelledView.CancelText).To(vm => vm.CancelString);
            set.Bind(_expiredView.CancelText).To(vm => vm.ExpiredString);

            set.Bind().For(me => me.IsCanceled).To(vm => vm.IsCanceled);
            set.Bind().For(me => me.IsExpired).To(vm => vm.IsExpired);

            set.Apply();
        }

        void UpdateDescription()
        {
            _descriptionLabel.Text = ViewModel.Description;

            _descriptionLabel.RemoveConstraint(_descriptionHeightConstraint);

            var _desiredHeight = _descriptionLabel.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
            _descriptionHeightConstraint = _descriptionLabel.Height().EqualTo(_desiredHeight).ToLayoutConstraints().ToArray()[0];

            _descriptionLabel.AddConstraint(_descriptionHeightConstraint);

            View.SetNeedsUpdateConstraints();
            View.LayoutIfNeeded();
        }

        void UpdateContent ()
        {
            UIColor baseColor = UIColor.Clear;
            UIColor darkColor = UIColor.Clear;
            if (Colors != null) {
                baseColor = new UIColor(Colors.Base.Red/255.0f, Colors.Base.Green/255.0f, Colors.Base.Blue/255.0f, 1.0f);
                darkColor = new UIColor(Colors.Dark.Red/255.0f, Colors.Dark.Green/255.0f, Colors.Dark.Blue/255.0f, 1.0f);
            }

            _bottomInfoContainer.BackgroundColor = darkColor;
            _topInfoContainer.BackgroundColor = baseColor;
            _avatarView.StrokeColor = darkColor;
            _avatarView.SetNeedsDisplay();

//            _summaryView.DiamondButton.DiamondColor = baseColor;
//            _summaryView.DiamondButton.SetNeedsDisplay();
//
//            _joinButton.DiamondButton.DiamondColor = baseColor;
//            _joinButton.DiamondButton.SetNeedsDisplay();
//
//            _facebookButton.DiamondButton.DiamondColor = baseColor;
//            _facebookButton.DiamondButton.SetNeedsDisplay();
//
//            _websiteButton.DiamondButton.DiamondColor = baseColor;
//            _websiteButton.DiamondButton.SetNeedsDisplay();
//
//            _invitesButton.DiamondButton.StrokeColor = baseColor;
//            _invitesButton.DiamondButton.SetNeedsDisplay();
//            Console.WriteLine("Color will be : " + baseColor);
//
//            ParentViewController.NavigationItem.LeftBarButtonItem = _shareButton;
//            _shareButton.TintColor = Themes.Dashboard.HomeIconColor;
//
//            if (!ViewModel.IsCanceled) {
//                UIImage image; 
//                UIGraphics.BeginImageContextWithOptions(new CGSize(22f, 22f), false, 0);
//                YuFitStyleKit.DrawMore(new CGRect(0, 0, 22F, 22F), Themes.Dashboard.HomeIconColor);
//                image = UIGraphics.GetImageFromCurrentImageContext();
//                UIGraphics.EndImageContext();
//                var editButton = new UIBarButtonItem(image, UIBarButtonItemStyle.Plain, (s, e) => ViewModel.ShowActivityDetailCommand.Command.Execute(null));
//                ParentViewController.NavigationItem.RightBarButtonItem = editButton;
//            } else {
//                ParentViewController.NavigationItem.RightBarButtonItem = null;
//            }
//
//            _invitesButton.CountLabelColor = baseColor;
        }

        void ShowCanceledView (bool canceled)
        {
            if (canceled) 
            {
                var container = new UIView(View.Frame);
                container.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0.4f);
                container.UserInteractionEnabled = true;
                View.AddSubview(container);

                _cancelledView.TranslatesAutoresizingMaskIntoConstraints = false;
                container.AddSubview(_cancelledView);

                container.AddConstraints(
                    _cancelledView.Height().EqualTo().HeightOf(container).WithMultiplier(.25f),
                    _cancelledView.Width().EqualTo().WidthOf(container),
                    _cancelledView.WithSameCenterX(container),
                    _cancelledView.WithSameCenterY(container).WithMultiplier(.5f));

                ParentViewController.NavigationItem.RightBarButtonItem = null;
            }
        }

        void ShowExpiredView (bool expired)
        {
            if (expired && !ViewModel.IsCanceled) 
            {
                var container = new UIView(View.Frame);
                container.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0.4f);
                container.UserInteractionEnabled = true;
                View.AddSubview(container);

                _expiredView.TranslatesAutoresizingMaskIntoConstraints = false;
                container.AddSubview(_expiredView);

                container.AddConstraints(
                    _expiredView.Height().EqualTo().HeightOf(container).WithMultiplier(.25f),
                    _expiredView.Width().EqualTo().WidthOf(container),
                    _expiredView.WithSameCenterX(container),
                    _expiredView.WithSameCenterY(container).WithMultiplier(.5f));

                ParentViewController.NavigationItem.RightBarButtonItem = null;
            }
        }

    }
}

