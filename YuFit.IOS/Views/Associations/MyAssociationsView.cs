﻿using System;
using System.Collections.Generic;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using CoreGraphics;
using UIKit;

using YuFit.Core.ViewModels.Associations;
using MvvmCross.Core.ViewModels;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Views.Associations
{
    public class MyAssociationsView : BaseViewController<MyAssociationsViewModel>
    {
        readonly UISegmentedControl _segmentedControl = new UISegmentedControl();
        readonly UIBarButtonItem _addButton = new UIBarButtonItem(UIBarButtonSystemItem.Add);
        readonly UIBarButtonItem _searchButton = new UIBarButtonItem(UIBarButtonSystemItem.Search);

        readonly List<UIViewController> _pages = new List<UIViewController>();
        readonly UIScrollView _scrollView = new UIScrollView();

        private int _page;
        public int Page
        {
            get { return _page; }
            set
            {
                _segmentedControl.SelectedSegment = value;
                _page = value;
                _scrollView.SetContentOffset(new CGPoint((value * UIScreen.MainScreen.Bounds.Width), 0), true);
            }
        }

        public bool AddButtonVisible
        {
            get { return _addButton.Enabled; }
            set 
            { 
                _addButton.Enabled = value;
                _addButton.TintColor = value ? Themes.Dashboard.HomeIconColor : UIColor.Clear;
            }
        }

        public bool SearchButtonVisible
        {
            get { return _searchButton.Enabled; }
            set 
            { 
                _searchButton.Enabled = value;
                _searchButton.TintColor = value ? Themes.Dashboard.HomeIconColor : UIColor.Clear;
            }
        }

        public override void ViewDidLoad() 
        {
            base.ViewDidLoad();

            UIView segmentView = new UIView();
            UIView containerView = new UIView();

            View.Add(segmentView);
            View.Add(containerView);

            segmentView.TranslatesAutoresizingMaskIntoConstraints = false;
            containerView.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddConstraints(
                segmentView.WithSameWidth(View),
                segmentView.WithSameCenterX(View),
                segmentView.WithSameTop(View),
                segmentView.Height().EqualTo(40.0f),

                containerView.WithSameWidth(View),
                containerView.WithSameCenterX(View),
                containerView.Top().EqualTo().BottomOf(segmentView),
                containerView.WithSameBottom(View)
            );

            segmentView.Add(_segmentedControl);
            containerView.Add(_scrollView);

            SetupNavigationButtons();

            _scrollView.BackgroundColor = UIColor.Blue;
            _scrollView.ShowsHorizontalScrollIndicator = false;
            _scrollView.ShowsVerticalScrollIndicator = false;
            _scrollView.PagingEnabled = true;
            _scrollView.ScrollEnabled = false;
            _scrollView.Frame = containerView.Frame;

            _scrollView.TranslatesAutoresizingMaskIntoConstraints = false;
            containerView.AddConstraints(
                _scrollView.WithSameWidth(containerView),
                _scrollView.WithSameHeight(containerView),
                _scrollView.WithSameCenterX(containerView),
                _scrollView.WithSameCenterY(containerView)
            );

            _segmentedControl.TranslatesAutoresizingMaskIntoConstraints = false;
            segmentView.AddConstraints(
                _segmentedControl.WithSameWidth(segmentView).Minus(30f),
                _segmentedControl.WithSameHeight(segmentView).Minus(9f),
                _segmentedControl.WithSameCenterX(segmentView),
                _segmentedControl.WithSameBottom(segmentView).Minus(1f));

            int i;
            UIView previousView = null;
            for (i = 0; i < ViewModel.Pages.Count; i++)
            {
                _segmentedControl.InsertSegment(ViewModel.Pages[i].DisplayName, i, false);
             
                var pageViewController = CreatePage(ViewModel.Pages[i]);
                _scrollView.AddSubview(pageViewController.View);

                pageViewController.View.TranslatesAutoresizingMaskIntoConstraints = false;
                _scrollView.AddConstraints(
                    pageViewController.View.WithSameWidth(_scrollView),
                    pageViewController.View.WithSameHeight(_scrollView),
                    pageViewController.View.WithSameCenterY(_scrollView)
                );

                if (previousView == null) {
                    _scrollView.AddConstraints(
                        pageViewController.View.Left().EqualTo().LeftOf(_scrollView)
                    );
                } else {
                    _scrollView.AddConstraints(
                        pageViewController.View.Left().EqualTo().RightOf(previousView)
                    );
                }

                AddChildViewController(pageViewController);
                previousView = pageViewController.View;
                ViewModel.Pages[i].Start();
            }

            _scrollView.AddConstraints(
                previousView.Right().EqualTo().RightOf(_scrollView)
            );

            var currentPageView = _pages[Page];
            _segmentedControl.SelectedSegment = 0;

            SetupBindings();
            SetupNavigationButtons();
        }

        private void SetupNavigationButtons()
        {
            NavigationItem.RightBarButtonItems = new []{ _addButton, _searchButton };
        }
           

        private UIViewController CreatePage(IMvxViewModel viewModel)
        {
            var screen = this.CreateViewControllerFor(viewModel) as UIViewController;
            _pages.Add(screen);
            return screen;
        }

        void SetupBindings()
        {
            var set = this.CreateBindingSet<MyAssociationsView, MyAssociationsViewModel>();
            set.Bind(_segmentedControl).For("SelectedSegment").To(vm => vm.SelectedPage);
            set.Bind().For(me => me.Page).To(vm => vm.SelectedPage);
            set.Bind(_addButton).For("Clicked").To(vm => vm.AddCommand.Command);
            set.Bind().For(me => me.AddButtonVisible).To(vm => vm.CanAdd);
            set.Bind().For(me => me.SearchButtonVisible).To(vm => vm.CanSearch);
            set.Bind(_searchButton).For("Clicked").To(vm => vm.SearchCommand.Command);
            set.Apply ();
        }
    }
}

