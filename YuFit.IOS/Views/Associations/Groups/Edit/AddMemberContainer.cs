﻿using System;
using System.Linq;
using YuFit.IOS.Controls;
using YuFit.Core.Extensions;
using UIKit;
using Cirrious.FluentLayouts.Touch;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Associations.Groups.Edit
{
    public class AddMemberContainer : UIView
    {
        public readonly IconView AddMemberDiamondView = new IconView();
        public readonly UILabel AddMemberLabel = new UILabel();

        public AddMemberContainer ()
        {
            AddMemberLabel.TextColor = YuFitStyleKit.Yellow;
            AddMemberLabel.Font = CrudFitEvent.ConfigView.LabelTextFont;

            AddMemberDiamondView.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawAddFriendsIcon");

            AddSubviews(new UIView[] { AddMemberDiamondView, AddMemberLabel });
            Subviews.ForEach(v => { v.TranslatesAutoresizingMaskIntoConstraints = false; });


            AddMemberDiamondView.AddConstraints(
                AddMemberDiamondView.Width().EqualTo(75.0f),
                AddMemberDiamondView.Height().EqualTo().WidthOf(AddMemberDiamondView)
            );

            this.AddConstraints(
                AddMemberDiamondView.Top().EqualTo().TopOf(this),
                AddMemberDiamondView.CenterX().EqualTo().CenterXOf(this),
                AddMemberLabel.Top().EqualTo(10.0f).BottomOf(AddMemberDiamondView),
                AddMemberLabel.CenterX().EqualTo().CenterXOf(this),
                this.Bottom().EqualTo().BottomOf(AddMemberLabel)
            );

        }


    }
}

