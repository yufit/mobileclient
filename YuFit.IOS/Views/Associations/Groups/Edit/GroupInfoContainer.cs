﻿using System.Collections.Generic;
using System.Linq;

using Cirrious.FluentLayouts.Touch;

using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Associations.Groups;

using YuFit.IOS.Controls;
using YuFit.IOS.Controls.Selections;
using YuFit.IOS.Themes;
using YuFit.IOS.Utilities;

using YuFit.WebServices.Interface.Model;

namespace YuFit.IOS.Views.Associations.Groups.Edit
{
    public class GroupInfoContainer : UIView
    {
        //public class TypeSelection : SelectionViewController<string, GroupType> {}
        //public class KindSelection : SelectionViewController<string, SportKind> {}

        readonly List<SeparatorLine> _separatorsLine    = new List<SeparatorLine>();
        public readonly PlaceholderTextField Name       = new PlaceholderTextField();
        public readonly PlaceholderTextField Address    = new PlaceholderTextField();

        //public TypeSelection SelectGroupTypeView        = new TypeSelection();
        //public KindSelection SelectSportKindView        = new KindSelection();

        public bool IsEditing {
            get {
                return Name.IsFirstResponder || Address.IsFirstResponder;
            }
        }

        public GroupInfoContainer ()
        {
            UserInteractionEnabled = true;
            for (int i = 0; i < 2; ++i) 
            { 
                _separatorsLine.Add(new SeparatorLine()); 
            }

//            SelectGroupTypeView.Colors = YuFitStyleKitExtension.ActivityBackgroundColors("Cycling");
//            SelectSportKindView.Colors = YuFitStyleKitExtension.ActivityBackgroundColors("Cycling");
//            SelectGroupTypeView.ShowSeparatorLine = false;
//            SelectSportKindView.ShowSeparatorLine = false;
//
//            SelectGroupTypeView.LabelFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 20);
//            SelectGroupTypeView.LabelTextColor = SelectGroupTypeView.Colors.Base;
//            SelectGroupTypeView.LabelTextAlignment = UITextAlignment.Left;
//            SelectSportKindView.LabelFont = UIFont.FromName("HelveticaNeueLTStd-LtCn", 20);
//            SelectSportKindView.LabelTextColor = SelectSportKindView.Colors.Base;
//            SelectSportKindView.LabelTextAlignment = UITextAlignment.Right;

            AddSubviews(_separatorsLine.ToArray());
            AddSubviews(new UIView[] { Name, Address /*S electGroupTypeView.View, SelectSportKindView.View */ });

            Subviews.ForEach(v => { v.TranslatesAutoresizingMaskIntoConstraints = false; });

            Subviews.Where(v => v is SeparatorLine).ForEach(v => {
                v.AddConstraints(v.Height().EqualTo(0.5f));
                v.BackgroundColor = YuFitStyleKit.YellowDark;

                this.AddConstraints(
                    v.Width().EqualTo().WidthOf(this),
                    v.WithSameCenterX(this)
                );
            });

            Subviews.Where(v => v is PlaceholderTextField).ForEach(v => {
                var textField               = v as PlaceholderTextField;
                textField.Font              = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
                textField.TextColor         = YuFitStyleKit.Yellow;
                textField.PlaceholderFont   = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);
                textField.PlaceholderColor  = YuFitStyleKit.YellowDark;

                this.AddConstraints(
                    v.Width().EqualTo().WidthOf(this),
                    v.WithSameCenterX(this)
                );
            });

            this.AddConstraints(
                _separatorsLine[0]      .Top().EqualTo(     ).TopOf   ( this ),
                Name                    .Top().EqualTo(9.00f).BottomOf( _separatorsLine[0] ),
                _separatorsLine[1]      .Top().EqualTo(3.75f).BottomOf( Name ),
                Address                 .Top().EqualTo(9.00f).BottomOf( _separatorsLine[1] )

//                SelectGroupTypeView.View.Top().EqualTo(2.0f).BottomOf( _separatorsLine[2] ),
//                SelectSportKindView.View.Top().EqualTo(2.0f).BottomOf( _separatorsLine[2] ),
//
//                SelectGroupTypeView.View.Width().EqualTo().WidthOf(this).WithMultiplier(0.5f),
//                SelectSportKindView.View.Width().EqualTo().WidthOf(this).WithMultiplier(0.5f),
//
//                SelectGroupTypeView.View.Left().EqualTo().LeftOf(this),
//                SelectSportKindView.View.Right().EqualTo().RightOf(this)
            );

            Name.KeyboardAppearance     = UIKeyboardAppearance.Dark;
            Address.KeyboardAppearance    = UIKeyboardAppearance.Dark;

            SetupFields();
        }

        public void AttachKeyboardMonitoring(EditGroupViewModel vm)
        {
            Name.ShouldReturn = new UITextFieldCondition(delegate {
                Address.BecomeFirstResponder();
                return true;
            });

            Address.ShouldReturn = new UITextFieldCondition(delegate {
                Address.BecomeFirstResponder();
                vm.SearchLocationCommand.Command.Execute(null);
                return true;
            });
        }

        public void DetachKeyboardMonitoring()
        {
            Name.ShouldReturn = null;
            Address.ShouldReturn = null;
        }

        void SetupFields()
        {
            Name.ClearButtonMode            = UITextFieldViewMode.Always;
            Name.ReturnKeyType              = UIReturnKeyType.Next;
            Name.AutocorrectionType         = UITextAutocorrectionType.No;
            Name.AutocapitalizationType     = UITextAutocapitalizationType.None;
            Address.ClearButtonMode           = UITextFieldViewMode.Always;
            Address.ReturnKeyType             = UIReturnKeyType.Search;
            Address.AutocorrectionType        = UITextAutocorrectionType.No;
            Address.AutocapitalizationType    = UITextAutocapitalizationType.None;
        }

        public override void MovedToSuperview ()
        {
            base.MovedToSuperview();
            if (Superview != null) {
                Superview.AddConstraints(
                    this.Bottom().EqualTo().BottomOf(Address)
                );
            }
        }
    }
}

