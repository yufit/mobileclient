﻿using System;
using System.Linq;
using UIKit;
using YuFit.Core.Extensions;
using YuFit.IOS.Controls;
using Cirrious.FluentLayouts.Touch;
using YuFit.IOS.Themes;
using CoreGraphics;

namespace YuFit.IOS.Views.Associations.Groups.Edit
{
    public class GroupDescriptionContainer : UIView
    {
        readonly SeparatorLine  _topLine         = new SeparatorLine();
        readonly SeparatorLine  _bottomLine      = new SeparatorLine();

        public readonly UILabel DescriptionLabel = new UILabel();
        public readonly PlaceholderTextView DescriptionPlaceholder = new PlaceholderTextView();

        public bool IsEditing { get { return DescriptionPlaceholder.IsFirstResponder; } }

        public GroupDescriptionContainer ()
        {
            UIView[] subviews = { DescriptionLabel, _topLine, DescriptionPlaceholder, _bottomLine };
            AddSubviews(subviews);

            Subviews.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;

                this.AddConstraints(
                    v.Width().EqualTo().WidthOf(this),
                    v.WithSameCenterX(this)
                );
            });

            Subviews.Where(v => v is SeparatorLine).ForEach(v => {
                v.AddConstraints(v.Height().EqualTo(0.5f));
                v.BackgroundColor = YuFitStyleKit.Yellow;
            });

            DescriptionLabel.Font = CrudFitEvent.ConfigView.LabelTextFont;
            DescriptionPlaceholder.TextColor = YuFitStyleKit.White;
            DescriptionPlaceholder.Editable = false;

            DescriptionPlaceholder.Font = CrudFitEvent.ConfigView.LabelTextFont;
            DescriptionPlaceholder.TextColor = YuFitStyleKit.Yellow;
            DescriptionPlaceholder.Editable = true;

            DescriptionPlaceholder.KeyboardType = UIKeyboardType.Default;
            DescriptionPlaceholder.AutocorrectionType = UITextAutocorrectionType.Yes;
            DescriptionPlaceholder.AutocapitalizationType = UITextAutocapitalizationType.Sentences;
            DescriptionPlaceholder.BackgroundColor = UIColor.Clear;

            KeyboardToolbar toolbar = new KeyboardToolbar (new CoreGraphics.CGRect(0.0f, 0.0f, Frame.Size.Width, 44.0f));

            toolbar.TintColor = YuFitStyleKit.White;
            toolbar.BarStyle = UIBarStyle.Black;

            toolbar.Translucent = true;

            SetupConstraints();

            var myButton = new UIBarButtonItem (":-)", UIBarButtonItemStyle.Plain, AddBarButtonText);


            toolbar.Items = new UIBarButtonItem[]{ 
                myButton,
                new UIBarButtonItem(":-)", 
                    UIBarButtonItemStyle.Plain, AddBarButtonText),
                new UIBarButtonItem(":-(", 
                    UIBarButtonItemStyle.Plain, AddBarButtonText),
                new UIBarButtonItem(";-)", 
                    UIBarButtonItemStyle.Plain, AddBarButtonText),
                new UIBarButtonItem(":-P", 
                    UIBarButtonItemStyle.Plain, AddBarButtonText),
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate {
                    this.DescriptionPlaceholder.ResignFirstResponder();
                })
            };

            DescriptionPlaceholder.KeyboardAppearance = UIKeyboardAppearance.Dark;
            DescriptionPlaceholder.InputAccessoryView = toolbar;
        }

        public void AddBarButtonText(object sender, EventArgs e)
        {
            var barButtonItem = sender as UIBarButtonItem;
            if (barButtonItem != null) {
                DescriptionPlaceholder.Text += barButtonItem.Title;
                UIDevice.CurrentDevice.PlayInputClick ();
            }
        }

        void SetupConstraints ()
        {
            this.AddConstraints(
                DescriptionLabel.Top().EqualTo().TopOf(this),
                _topLine.Top().EqualTo(1.0f).TopOf(DescriptionLabel),
                DescriptionPlaceholder.Top().EqualTo(4.0f).BottomOf(_topLine),
                DescriptionPlaceholder.Bottom().EqualTo(-4.0f).TopOf(_bottomLine),
                _bottomLine.Bottom().EqualTo(-1.0f).BottomOf(this)
            );
        }

        public override CoreGraphics.CGSize SizeThatFits (CoreGraphics.CGSize size)
        {
            // TODO Replace the 20 with some math (textView.Y - this.Frame.Y) + (this.Frame.Bottom - thexView.Bottom)
            var textViewHeight = DescriptionPlaceholder.SizeThatFits(size).Height;
            return new CGSize(size.Width, textViewHeight + 50);
        }
    }
}

