using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Foundation;
using UIKit;

using YuFit.Core.ViewModels.Associations.Groups;
using YuFit.IOS.Extensions;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Views.Associations.Groups.Members
{

    public class MembersTableView<TViewModel, TCell> : MvxTableViewController where TCell : UITableViewCell, new()
    {
        protected new TViewModel ViewModel { get { return (TViewModel) base.ViewModel; } }
        public MyMultiSectionTableViewSource<TCell> Source { get; set;}

        readonly string _cellKey;

        public MembersTableView (string cellKey)
        {
            _cellKey = cellKey;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            TableView.BackgroundColor = UIColor.Clear;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.RowHeight = 60;

            Source = new MyMultiSectionTableViewSource<TCell>(TableView, _cellKey);

            TableView.Source = Source;
        }

        public override void ViewWillAppear (bool animated)
        {
            TableView.DeselectRow (TableView.IndexPathForSelectedRow, true);
            base.ViewWillAppear(animated);
        }
    }
    
}
