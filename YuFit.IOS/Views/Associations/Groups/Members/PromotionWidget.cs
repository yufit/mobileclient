﻿using System;
using UIKit;
using Foundation;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Associations.Groups.Members
{
    [Register("DownUpBox")]
    public class PromotionWidget : UIView
    {
        public UIColor WidgetColor { get; set; }

        private bool _showUp;
        public bool ShowUp {
            get { return _showUp; }
            set { _showUp = value; SetNeedsDisplay(); }
        }

        public PromotionWidget (IntPtr handle) : base(handle)
        {
            BackgroundColor = UIColor.Clear;
            WidgetColor = YuFitStyleKit.Yellow;
        }

        public PromotionWidget()
        {
            BackgroundColor = UIColor.Clear;
            WidgetColor = YuFitStyleKit.Yellow;
        }

        public override void Draw (CoreGraphics.CGRect rect)
        {
            if (_showUp)    { YuFitStyleKit.DrawDownArrow(Bounds, WidgetColor); }
            else            { YuFitStyleKit.DrawUpArrow(Bounds, WidgetColor); }
        }
    }
}

