﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Foundation;
using UIKit;

using YuFit.Core.ViewModels.Associations.Groups;

using YuFit.IOS.Controls;
using YuFit.IOS.Controls.UserTableView;
using YuFit.IOS.Themes;
using YuFit.IOS.Extensions;

namespace YuFit.IOS.Views.Associations.Groups.Members
{
    public class GroupMemberTableViewCell : UserInfoTableViewCell<MemberViewModel>
    {
        public static readonly NSString Key = new NSString ("GroupMemberTableViewCell");

        readonly UIView             _promotionContainer     = new UIView();
        readonly PromotionWidget    _promotionBox           = new PromotionWidget();
        readonly IconView           _deleteIcon             = new IconView();

        private MemberViewModel ViewModel { get { return (MemberViewModel) DataContext;}}

        ~GroupMemberTableViewCell ()
        {
            System.Console.WriteLine("Delete cell");
        }

        private bool _isManager;
        public bool IsManager {
            get { return _isManager; }
            set { 
                _isManager = value; 
                if (!_isManager) {
//                    ContentColor = YuFitStyleKit.Gray; 
                    _promotionBox.ShowUp = false;
                    _promotionBox.WidgetColor = YuFitStyleKit.Yellow;
                } else {
                    _promotionBox.ShowUp = true;
                    _promotionBox.WidgetColor = YuFitStyleKit.Yellow;
                }
            }
        }

        public bool IsViewerAdmin {
            get { return ViewModel.IsViewerAdmin; }
            set { UpdateWidgetsVisibility(); }
        }

        public bool IsCreator {
            get { return ViewModel.IsCreator; }
            set { UpdateWidgetsVisibility(); }
        }

        public bool IsMe {
            get { return ViewModel.IsMe; }
            set { UpdateWidgetsVisibility(); }
        }

        public GroupMemberTableViewCell ()
        {
            ContentView.AddSubview(_promotionContainer);
            _promotionContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            ContentView.AddConstraints(
                _promotionContainer.Height().EqualTo().HeightOf(ContentView),
                _promotionContainer.CenterY().EqualTo().CenterYOf(ContentView),
                _promotionContainer.Left().EqualTo().LeftOf(ContentView),
                _promotionContainer.Right().EqualTo().LeftOf(PictureView)
            );

            _promotionContainer.AddSubview(_promotionBox);
            _promotionBox.TranslatesAutoresizingMaskIntoConstraints = false;

            _promotionBox.AddConstraints(
                _promotionBox.Height().EqualTo(20.0f),
                _promotionBox.Width().EqualTo(20.0f)
            );

            _promotionContainer.AddConstraints(
                _promotionBox.WithSameCenterY(_promotionContainer),
                _promotionBox.Left().EqualTo(6.0f).LeftOf(_promotionContainer)
            );

            _deleteIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawClose2");
            _deleteIcon.TranslatesAutoresizingMaskIntoConstraints = false;
            _deleteIcon.Hidden = true;
            _deleteIcon.StrokeColor = YuFitStyleKit.YellowDark;
            ContentView.AddSubview(_deleteIcon);

            ContentView.AddConstraints(
                _deleteIcon.Height().EqualTo().HeightOf(ContentView).WithMultiplier(0.35f),
                _deleteIcon.Width().EqualTo().HeightOf(ContentView).WithMultiplier(0.35f),
                _deleteIcon.Right().EqualTo(-6.0f).RightOf(ContentView),
                _deleteIcon.CenterY().EqualTo().CenterYOf(ContentView)
            );

            this.DelayBind(() => {
                var set = this.CreateBindingSet<GroupMemberTableViewCell, MemberViewModel>();
                set.Bind().For(me => me.IsManager).To(vm => vm.IsManager);
                set.Bind().For(me => me.IsViewerAdmin).To(vm => vm.IsViewerAdmin);
                set.Bind().For(me => me.IsMe).To(vm => vm.IsMe);
                set.Bind().For(me => me.IsCreator).To(vm => vm.IsCreator);
                set.Bind(_promotionContainer.Tap()).To(vm => vm.ToggleManagerRoleCommand.Command);
//                set.Bind(_deleteIcon.Tap()).To(vm => vm.DeleteCommand.Command);
                set.Apply ();
            });
        }

        void UpdateWidgetsVisibility()
        {
            // when reloading cells, this could potentially be null for a moment.
            if (ViewModel == null) { return; }
            
            if (ViewModel.IsViewerAdmin && !ViewModel.IsMe && !ViewModel.IsCreator) {
// TODO : enable later when coding booting member from a group
//                _deleteIcon.Hidden = false;
//                _deleteIcon.SetNeedsDisplay();
                _promotionBox.Hidden = false;
                _promotionBox.SetNeedsDisplay();
            } else {
//                _deleteIcon.Hidden = true;
//                _deleteIcon.SetNeedsDisplay();
                _promotionBox.Hidden = true;
                _promotionBox.SetNeedsDisplay();
            }
        }
    }

}

