﻿
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using UIKit;

using YuFit.Core.ViewModels.Associations.Groups;
using YuFit.IOS.Extensions;

namespace YuFit.IOS.Views.Associations.Groups.Members
{
    public class GroupMembersView : BaseViewController<GroupMembersViewModel>
    {
        readonly UIBarButtonItem _addButton = new UIBarButtonItem(UIBarButtonSystemItem.Add);

        class GroupMemberTableView : MembersTableView<MemberViewModel, GroupMemberTableViewCell> {
            public GroupMemberTableView () : base(GroupMemberTableViewCell.Key) { }
        }

        readonly GroupMemberTableView   _tableView  = new GroupMemberTableView();

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            if (ViewModel.IsAdministrator) {
                _addButton.TintColor = Themes.Dashboard.HomeIconColor;
                NavigationItem.RightBarButtonItems = new []{ _addButton };
            }

            SetupViewHierarchy();
            SetupViewConstraints();
            SetupBindings();
        }

        void SetupViewHierarchy ()
        {
            _tableView.BindingContext = BindingContext;
            AddChildViewController(_tableView);
            View.AddSubview(_tableView.View);
        }

        void SetupViewConstraints ()
        {
            _tableView.View.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddConstraints(
                _tableView.View.WithSameHeight(View).WithMultiplier(0.9f),
                _tableView.View.WithSameWidth(View).WithMultiplier(0.9f),
                _tableView.View.WithSameCenterX(View),
                _tableView.View.WithSameCenterY(View)
            );
        }

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<GroupMembersView, GroupMembersViewModel>();
            set.Bind(_tableView.Source).To(vm => vm.Members);
            set.Bind(_tableView.Source).For(s => s.SelectionChangedCommand).To(vm => vm.ShowUserProfileCommand.Command);
            set.Bind(_addButton).For("Clicked").To(vm => vm.AddUserToGroupCommand.Command);
            set.Apply();
            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );
        }

    }
}

