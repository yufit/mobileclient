using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Foundation;
using UIKit;

using YuFit.Core.ViewModels.Associations.Groups;
using YuFit.IOS.Extensions;
using YuFit.IOS.Controls;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Associations.Groups.Members
{
    public class MyMultiSectionTableViewSource<TCell> : TableViewSource<TCell> where TCell : UITableViewCell, new()
    {
        public MyMultiSectionTableViewSource (UITableView tableView, string key) : base(tableView, key)
        {
        }

        public override System.nint NumberOfSections (UITableView tableView)
        {
            return 2;
        }

        public override System.nint RowsInSection (UITableView tableview, System.nint section)
        {
            switch(section) {
                case 0: return ItemsSource.Cast<MemberViewModel>().Count(item => item.IsManager);
                case 1: return ItemsSource.Cast<MemberViewModel>().Count(item => !item.IsManager);
                default: return 0;
            }

        }

        protected override object GetItemAt(NSIndexPath indexPath)
        {
            if (ItemsSource == null)
                return null;

            switch(indexPath.Section) {
                case 0: return ItemsSource.Cast<MemberViewModel>().Where(item => item.IsManager).ElementAt(indexPath.Row);
                case 1: return ItemsSource.Cast<MemberViewModel>().Where(item => !item.IsManager).ElementAt(indexPath.Row);
                default: return null;
            }
        }

        public override System.nfloat GetHeightForHeader (UITableView tableView, System.nint section)
        {
            switch(section) {
                case 0: return 22;
                case 1: return 10;
                default: return 0;
            }
        }

        public override UIView GetViewForHeader (UITableView tableView, System.nint section)
        {
            switch(section) {
                case 0: return AdministratorHeader();
                case 1: return NonAdministratorHeader();
                default: return null;
            }
        }

        UIView AdministratorHeader ()
        {
            UILabel label = new UILabel();
            label.Text = "managed by"; // TODO localize.
            label.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 18);
            label.TextColor = YuFitStyleKit.Yellow;
            return label;
        }

        UIView NonAdministratorHeader ()
        {
            var view = new UIView();

            var separator = new SeparatorLine();
            view.AddSubview(separator);
            separator.BackgroundColor = YuFitStyleKit.Yellow;
            separator.TranslatesAutoresizingMaskIntoConstraints = false;
            view.AddConstraints(
                separator.Height().EqualTo(0.5f),
                separator.WithSameWidth(view),
                separator.WithSameCenterX(view),
                separator.WithSameCenterY(view)
            );

            return view;
        }
    }
    
}
