﻿using System;
using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using CoreGraphics;
using Foundation;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Activity;
using YuFit.Core.ViewModels.Associations.Groups;

using YuFit.IOS.Controls;
using YuFit.IOS.Controls.Buttons;
using YuFit.IOS.Controls.UpcomingActivitiesShortView;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;

using YuFit.WebServices.Interface.Model;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Views.Associations.Groups
{
    public class GroupView : BaseViewController<GroupViewModel>
    {
        #region Upcoming activities Tableview declaration
        const float TableViewHeight = 153.0f;

        // Analysis disable once ConvertToStaticType
        class TableViewCell : UpcomingShortListTableViewCell<UpcomingActivityViewModel> {
            public static readonly NSString Key = new NSString("UpcomingActivitiesTableViewCell");
        }
        class TableView : UpcomingShortListTableView<GroupViewModel, TableViewCell> {
            public TableView () : base(TableViewCell.Key) { }
        }

        class ShortListView : UpcomingShortListView<TableView, TableViewCell> {}
        #endregion

        readonly UIScrollView           _scrollView         = new UIScrollView();
        readonly UIView                 _photoContainerView = new UIView();
        readonly UIImageView            _photoView          = new UIImageView();
        readonly PictureContainer       _avatarView         = new PictureContainer();
        readonly UILabel                _nameLabel          = new UILabel();
        readonly UILabel                _managedByLabel     = new UILabel();
        readonly UILabel                _managedByWho       = new UILabel();

        readonly JoinGroupButton        _joinButton         = new JoinGroupButton();
        readonly LeaveGroupButton       _leaveButton        = new LeaveGroupButton();
        readonly UILabel                _joinLeaveLabel     = new UILabel();

        readonly PlaceholderTextView    _groupDescView      = new PlaceholderTextView();

        readonly DiamondShapeView       _countDiamondView   = new DiamondShapeView();
        readonly UILabel                _countTitleLabel    = new UILabel();
        readonly UILabel                _countLabel         = new UILabel();

        readonly UIView                 _bannerContainer    = new UIView();
        readonly UILabel                _upgradeLabel       = new UILabel();
        readonly IconView               _upgradeInfo        = new IconView(YuFitStyleKit.White, "Question");

        readonly UIView                 _bottomContainer    = new UIView();

        readonly UIView                 _upcomingContainer  = new UIView();
        readonly ShortListView          _upcomingListView   = new ShortListView();

        readonly UIView                 _plusContainer      = new UIView();
        readonly IconView               _websiteButton      = new IconView(YuFitStyleKit.White, "Globe");
        readonly IconView               _facebookButton     = new IconView(YuFitStyleKit.White, "Facebook");
        readonly IconView               _calendarButton     = new IconView(YuFitStyleKit.White, "ActivitiesIcon");

        readonly UIBarButtonItem        _editButton         = new UIBarButtonItem(UIBarButtonSystemItem.Edit);

        public MvxImageViewLoader       ImageViewLoader { get; private set; }

        // Analysis disable ValueParameterNotUsed
        public bool IsAdministrator { get { return ViewModel.IsAdministrator; } set { SetupEditButton(); SetupCreateButton(); } }
        public bool IsUpgradable    { get { return ViewModel.IsUpgradable;    } set { DisplayUpgradeBanner();   } }
        public bool IsPlus          { get { return ViewModel.IsPlus;          } set { UpdatePlusAttributes();   } }
        public bool IsPublic        { get { return ViewModel.IsPublic;        } set { SetupCreateButton();      } }
        public bool IsMember        { get { return ViewModel.IsMember;        } set { ResetJoinOrLeaveButton(); } }
        public bool IsPending       { get { return ViewModel.IsPending;       } set { ResetJoinOrLeaveButton(); } }
        public string Bio           { get { return ViewModel.Description;     } set { UpdateDescription();      } }
        // Analysis restore ValueParameterNotUsed

        NSLayoutConstraint _bioHeightConstraint;
        NSLayoutConstraint _upgradeBannerConstraint;
        NSLayoutConstraint _leaveGroupButtonLeftConstraint;
        NSLayoutConstraint _bottomContainerConstraint;
        NSLayoutConstraint _upcomingContainerConstraint;
        NSLayoutConstraint _plusButtonsContainerConstraint;

        #region UIView override

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            _upcomingListView.SetBindingContext(BindingContext);
            AddChildViewController(_upcomingListView.TableViewCtrl);

            ImageViewLoader = new MvxImageViewLoader(() => _photoView);

            CreateViewHierarchy();
            ConfigureViewSpecifics();
            SetupConstraints();
            SetupBindings();
        }

        public override void ViewWillDisappear (bool animated)
        {
            ImageViewLoader = null;
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidDisappear (bool animated)
        {
            _upcomingListView.SetBindingContext(null);
            base.ViewDidDisappear(animated);
        }

        #endregion

        #region UI setup and layout

        void CreateViewHierarchy ()
        {
            View.AddSubviews(new [] { _bannerContainer, _scrollView, _bottomContainer });

            _scrollView.AddSubviews(new [] { 
                _photoContainerView, _avatarView, _nameLabel, _managedByLabel, _managedByWho, 
                _joinButton, _leaveButton, _joinLeaveLabel, _groupDescView, _countDiamondView, 
                _countTitleLabel 
            });

            _countDiamondView.AddSubview(_countLabel);
            _photoContainerView.AddSubview(_photoView);
            _bottomContainer.AddSubviews(new [] {_upcomingContainer, _plusContainer});
            _upcomingContainer.AddSubview(_upcomingListView);
            _bannerContainer.AddSubviews(new UIView[] {_upgradeLabel, _upgradeInfo});
            _plusContainer.AddSubviews(new [] {_websiteButton, _facebookButton, _calendarButton});
        }

        void ConfigureViewSpecifics()
        {
            _bannerContainer.BackgroundColor = YuFitStyleKit.RedDark;

            _upgradeLabel.TextColor = YuFitStyleKit.White;
            _upgradeLabel.TextAlignment = UITextAlignment.Center;
            _upgradeLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);

            _photoContainerView.BackgroundColor = YuFitStyleKit.Black;
            _photoContainerView.AddSubview(_photoView);
            _photoView.ContentMode = UIViewContentMode.ScaleAspectFit;
            _photoView.ClipsToBounds = true;
            _photoContainerView.ClipsToBounds = true;

            _avatarView.StrokeWidth = 3.0f;
            _avatarView.Label.Font       = UIFont.FromName("HelveticaNeueLTStd-MdCn", 28);
            _avatarView.Label.TextColor  = UIColor.FromRGBA(24.0f/255.0f,24.0f/255.0f,24.0f/255.0f,1.0f);

            _countDiamondView.DiamondColor = YuFitStyleKit.Yellow;
            _countLabel.TextAlignment = UITextAlignment.Center;
            _countLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 28);
            _countLabel.TextColor = YuFitStyleKit.White;
            _countLabel.AdjustsFontSizeToFitWidth = true;
            _countLabel.TextAlignment = UITextAlignment.Center;

            _countTitleLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 14);
            _countTitleLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _countTitleLabel.TextColor = YuFitStyleKit.Yellow;

            _joinLeaveLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 14);
            _joinLeaveLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _joinLeaveLabel.TextColor = YuFitStyleKit.Yellow;
            _joinLeaveLabel.Text = "LEAVE";

            _nameLabel.TextAlignment = UITextAlignment.Center;
            _nameLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 22);
            _nameLabel.TextColor = YuFitStyleKit.White;

            _managedByLabel.TextAlignment = UITextAlignment.Center;
            _managedByLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 14);
            _managedByLabel.TextColor = YuFitStyleKit.Yellow;
            _managedByLabel.AdjustsFontSizeToFitWidth = true;

            _managedByWho.TextAlignment = UITextAlignment.Center;
            _managedByWho.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 14);
            _managedByWho.TextColor = YuFitStyleKit.White;
            _managedByWho.AdjustsFontSizeToFitWidth = true;

            _groupDescView.TextColor = YuFitStyleKit.Gray;
            _groupDescView.TextAlignment = UITextAlignment.Center;
            _groupDescView.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            _groupDescView.Editable = false;
            _groupDescView.BackgroundColor = UIColor.Clear;
            _groupDescView.ScrollEnabled = false;

            _upcomingContainer.BackgroundColor = UIColor.Clear;
        }

        void SetupConstraints()
        {
            View.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            View.AddConstraints(
                _bannerContainer.WithSameLeft(View),
                _bannerContainer.WithSameRight(View),
                _bannerContainer.Top().EqualTo().TopOf(View),

                _scrollView.WithSameLeft(View),
                _scrollView.WithSameRight(View),
                _scrollView.Top().EqualTo().BottomOf(_bannerContainer),
                _scrollView.Bottom().EqualTo().TopOf(_bottomContainer),

                _bottomContainer.WithSameLeft(View),
                _bottomContainer.WithSameRight(View),
                _bottomContainer.Bottom().EqualTo().BottomOf(View)
            );

            _upgradeBannerConstraint = _bannerContainer.Height().EqualTo(0.0f).ToLayoutConstraints().First();
            _bottomContainerConstraint = _bottomContainer.Height().EqualTo(0.0f).ToLayoutConstraints().First();

            _bannerContainer.AddConstraint(_upgradeBannerConstraint);
            _bottomContainer.AddConstraint(_bottomContainerConstraint);

            SetupBannerContainerConstraints();
            SetupScrollViewConstraints();
            SetupBottomContainerConstraints();

            // Once everything is added, we can adjust the scrollview bottom
            // constraint which will devine the amount of scrollable content.
            _scrollView.AddConstraints(
                _groupDescView.Bottom().EqualTo(-Spacing).BottomOf(_scrollView)
            );
        }

        void SetupBannerContainerConstraints()
        {
            _bannerContainer.ClipsToBounds = true;
            _bannerContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _bannerContainer.AddConstraints(
                _upgradeLabel.WithSameCenterX(_bannerContainer),
                _upgradeLabel.WithSameCenterY(_bannerContainer),

                _upgradeInfo.WithSameCenterY(_bannerContainer),
                _upgradeInfo.Left().EqualTo(10.0f).RightOf(_upgradeLabel),
                _upgradeInfo.Height().EqualTo().HeightOf(_bannerContainer).WithMultiplier(0.8f),
                _upgradeInfo.Width().EqualTo().HeightOf(_bannerContainer).WithMultiplier(0.8f)
            );
        }

        void SetupScrollViewConstraints()
        {
            _scrollView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            View.AddConstraints(
                _photoContainerView.Width().EqualTo().WidthOf(View),
                _photoContainerView.Height().EqualTo().WidthOf(View).WithMultiplier(0.5531401f),
                _photoContainerView.WithSameCenterX(View),

                _avatarView.Width().EqualTo(132.0f),
                _avatarView.Height().EqualTo().WidthOf(_avatarView),
                _avatarView.CenterY().EqualTo().BottomOf(_photoContainerView),
                _avatarView.CenterX().EqualTo().CenterXOf(View),

                _countDiamondView.CenterY().EqualTo().BottomOf(_photoContainerView),
                _countDiamondView.Width().EqualTo().WidthOf(_avatarView).WithMultiplier(0.5f),
                _countDiamondView.Height().EqualTo().WidthOf(_avatarView).WithMultiplier(0.5f),
                _countDiamondView.Right().EqualTo().LeftOf(_avatarView),

                _countTitleLabel.Below(_countDiamondView, 2.0f),
                _countTitleLabel.WithSameCenterX(_countDiamondView),

                _joinButton.CenterY().EqualTo().BottomOf(_photoContainerView),
                _joinButton.Width().EqualTo().WidthOf(_avatarView).WithMultiplier(0.5f),
                _joinButton.Height().EqualTo().WidthOf(_avatarView).WithMultiplier(0.5f),
                _joinButton.Left().EqualTo().RightOf(_avatarView),

                _leaveButton.CenterY().EqualTo().BottomOf(_photoContainerView),
                _leaveButton.Width().EqualTo().WidthOf(_avatarView).WithMultiplier(0.5f),
                _leaveButton.Height().EqualTo().WidthOf(_avatarView).WithMultiplier(0.5f),

                _joinLeaveLabel.Below(_joinButton, 2.0f),
                _joinLeaveLabel.WithSameCenterX(_joinButton),

                _nameLabel.WithSameCenterX(_photoContainerView),
                _nameLabel.Width().EqualTo(-40).WidthOf(View),
                _nameLabel.Top().EqualTo(5.0f).BottomOf(_avatarView),

                _managedByLabel.WithSameCenterX(_photoContainerView),
                _managedByLabel.Width().EqualTo(-40).WidthOf(View),
                _managedByLabel.Top().EqualTo(10.0f).BottomOf(_nameLabel),

                _managedByWho.WithSameCenterX(_photoContainerView),
                _managedByWho.Width().EqualTo(-40).WidthOf(View),
                _managedByWho.Top().EqualTo(2.0f).BottomOf(_managedByLabel),

                _groupDescView.Left().EqualTo(Spacing).LeftOf(View),
                _groupDescView.Right().EqualTo(-Spacing).RightOf(View),
                _groupDescView.Top().EqualTo(20.0f).BottomOf(_managedByWho),

                _bottomContainer.WithSameWidth(View),
                _bottomContainer.WithSameCenterX(View),
                _bottomContainer.WithSameBottom(View)
            );

            SetupPictureContainerConstraints();
            SetupCountDiamondViewConstraints();
            SetupJoinLeaveButtonConstraints();
            SetupGroupDescriptionConstraints();

            _scrollView.AddConstraints(
                _photoContainerView.Top().EqualTo().TopOf(_scrollView)
            );

        }

        void SetupPictureContainerConstraints ()
        {
            _photoContainerView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _photoContainerView.AddConstraints(
                _photoView.WithSameWidth(_photoContainerView),
                _photoView.WithSameTop(_photoContainerView),
                _photoView.WithSameCenterX(_photoContainerView),
                _photoView.WithSameHeight(_photoContainerView)
            );
        }

        void SetupCountDiamondViewConstraints ()
        {
            _countDiamondView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _countDiamondView.AddConstraints(
                _countLabel.CenterX().EqualTo().CenterXOf(_countDiamondView),
                _countLabel.CenterY().EqualTo(4.0f).CenterYOf(_countDiamondView),
                _countLabel.Height().EqualTo().HeightOf(_countDiamondView).WithMultiplier(0.6f),
                _countLabel.Width().EqualTo().WidthOf(_countDiamondView).WithMultiplier(0.6f)
            );
        }

        void SetupJoinLeaveButtonConstraints ()
        {
            _leaveGroupButtonLeftConstraint = _leaveButton.Left().EqualTo().RightOf(_avatarView).ToLayoutConstraints().First();
            View.AddConstraint(_leaveGroupButtonLeftConstraint);

            _leaveButton.Hidden = true;
            _joinButton.Hidden = true;
        }

        void SetupGroupDescriptionConstraints ()
        {
            var _desiredHeight = _groupDescView.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
            _bioHeightConstraint = _groupDescView.Height().EqualTo(_desiredHeight).ToLayoutConstraints().First();
            _groupDescView.AddConstraint(_bioHeightConstraint);
        }

        void SetupBottomContainerConstraints ()
        {
            _bottomContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _bottomContainer.AddConstraints(
                _upcomingContainer.WithSameCenterX(_bottomContainer),
                _upcomingContainer.WithSameWidth(_bottomContainer),
                _upcomingContainer.WithSameBottom(_bottomContainer),

                _plusContainer.WithSameCenterX(_bottomContainer),
                _plusContainer.WithSameWidth(_bottomContainer),
                _plusContainer.WithSameBottom(_bottomContainer)
            );

            SetupUpcomingContainerConstraints();
            SetupPlusContainerConstraints();
        }

        void SetupUpcomingContainerConstraints()
        {
            _upcomingContainer.ClipsToBounds = true;
            _upcomingContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _upcomingContainer.AddConstraints(                
                _upcomingListView.Top().EqualTo(6.0f).TopOf(_upcomingContainer),
                _upcomingListView.Left().EqualTo(Spacing).LeftOf(_upcomingContainer),
                _upcomingListView.Bottom().EqualTo(-30).BottomOf(_upcomingContainer),
                _upcomingListView.Right().EqualTo(-Spacing).RightOf(_upcomingContainer)
            );

            _upcomingContainerConstraint = _upcomingContainer.Height().EqualTo(0.0f).ToLayoutConstraints().First();
            _upcomingContainer.AddConstraint(_upcomingContainerConstraint);

        }

        void SetupPlusContainerConstraints ()
        {
            _plusContainer.ClipsToBounds = true;
            _plusContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _plusContainer.AddConstraints(
                _calendarButton.WithSameCenterX(_plusContainer),
                _calendarButton.WithSameCenterY(_plusContainer),
                _calendarButton.Height().EqualTo(35.0f),
                _calendarButton.Width().EqualTo().HeightOf(_calendarButton),

                _websiteButton.WithSameCenterY(_plusContainer),
                _websiteButton.Right().EqualTo(-50.0f).LeftOf(_calendarButton),
                _websiteButton.WithSameHeight(_calendarButton),
                _websiteButton.Width().EqualTo().HeightOf(_websiteButton),

                _facebookButton.WithSameCenterY(_plusContainer),
                _facebookButton.Left().EqualTo(50.0f).RightOf(_calendarButton),
                _facebookButton.WithSameHeight(_calendarButton),
                _facebookButton.Width().EqualTo().HeightOf(_websiteButton)
            );

            _plusButtonsContainerConstraint = _plusContainer.Height().EqualTo(0.0f).ToLayoutConstraints().First();
            _plusContainer.AddConstraint(_plusButtonsContainerConstraint);
        }

        #endregion

        #region Bindings

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<GroupView, GroupViewModel>();
            set.Bind().For(me => me.Title).To(vm => vm.DisplayName);

            set.Bind(_nameLabel).To(vm => vm.DisplayName);
            set.Bind(_managedByLabel).To(vm => vm.ManagedByLabel);
            set.Bind(_managedByWho).To(vm => vm.ManagedBy);
            set.Bind(_countTitleLabel).To(vm => vm.MembersLabel);
            set.Bind(_countLabel).To(vm => vm.NumberOfMembers);
            set.Bind(_joinLeaveLabel).To(vm => vm.JoinLeaveLabel);

            set.Bind(_groupDescView).To(vm => vm.Description);
            set.Bind().For(me => me.Bio).To(vm => vm.Description);

            set.Bind(_upcomingListView.UpcomingTitle).To(vm => vm.UpcomingActivitiesTitle);
            set.Bind(_upcomingListView.OptionalTitle).To(vm => vm.CreateNewActivityTitle);
            set.Bind(_upcomingListView.TableViewCtrl.Source).To(vm => vm.UpcomingActivities);
            set.Bind(_upcomingListView.TableViewCtrl.Source).For(s => s.SelectionChangedCommand).To(vm => vm.ShowActivitySummaryCommand.Command);

            set.Bind(_joinButton.Tap()).To(vm => vm.JoinGroupCommand.Command);
            set.Bind(_leaveButton.Tap()).To(vm => vm.LeaveGroupCommand.Command);
            set.Bind(_countDiamondView.Tap()).To(vm => vm.ViewMembersCommand.Command);

            set.Bind(_avatarView.ImageViewLoader).To(vm => vm.AvatarUrl);

            set.Bind(ImageViewLoader).To(vm => vm.PhotoUrl);
            set.Bind().For(me => me.IsAdministrator).To(vm => vm.IsAdministrator);
            set.Bind().For(me => me.IsMember).To(vm => vm.IsMember);
            set.Bind().For(me => me.IsPending).To(vm => vm.IsPending);
            set.Bind().For(me => me.IsPublic).To(vm => vm.IsPublic);
            set.Bind().For(me => me.IsUpgradable).To(vm => vm.IsUpgradable);
            set.Bind(_editButton).For("Clicked").To(vm => vm.EditGroupCommand.Command);

            set.Bind(_upgradeLabel).To(vm => vm.UpgradeGroupLabel);
            set.Bind().For(me => me.IsPlus).To(vm => vm.IsPlus);
            set.Bind(_bannerContainer.Tap()).To(vm => vm.UpgradeGroupCommand.Command);
            set.Bind(_upgradeInfo.Tap()).To(vm => vm.ShowGroupPlusAdvCommand.Command);

            set.Bind(_upcomingListView.OptionalTitle.Tap()).To(vm => vm.CreateActivityCommand.Command);

            set.Bind(_calendarButton.Tap()).To(vm => vm.ShowCalendarCommand.Command);
            set.Bind(_websiteButton.Tap()).To(vm => vm.OpenWebSiteCommand.Command);
            set.Bind(_facebookButton.Tap()).To(vm => vm.OpenFacebookCommand.Command);

            set.Apply();

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage);
        }

        #endregion

        #region Databinging called method

        void SetupEditButton ()
        {
            if (IsAdministrator)
            {
                UITextAttributes attributes = new UITextAttributes();
                attributes.Font = NavigationBar.TitleFont;
                _editButton.SetTitleTextAttributes(attributes, UIControlState.Normal);
                NavigationItem.RightBarButtonItem = _editButton;
            }
        }

        void DisplayUpgradeBanner ()
        {
            if (IsUpgradable) {
                _upgradeBannerConstraint.Constant = 30.0f;
            } else {
                _upgradeBannerConstraint.Constant = 0.0f;
            }

            View.SetNeedsUpdateConstraints();
            View.LayoutIfNeeded();
            _upgradeInfo.SetNeedsDisplay();
        }

        void SetupCreateButton ()
        {
            //_upcomingListView.OptionalTitle.Hidden = !(ViewModel.IsPublic || ViewModel.IsAdministrator);
            _upcomingListView.OptionalTitle.Hidden = !(ViewModel.IsAdministrator);
        }

        void ResetJoinOrLeaveButton ()
        {
            _leaveButton.Hidden = !IsMember && !IsPending;
            _joinButton.Hidden = IsMember;

            if (IsMember) {
                _leaveGroupButtonLeftConstraint.Constant = 0;
            } else {
                _leaveGroupButtonLeftConstraint.Constant = -1000;
            }
            View.NeedsUpdateConstraints();
        }

        void UpdateDescription()
        {
            _groupDescView.Text = ViewModel.Description;

            _groupDescView.RemoveConstraint(_bioHeightConstraint);

            var _desiredHeight = _groupDescView.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
            _bioHeightConstraint = _groupDescView.Height().EqualTo(_desiredHeight).ToLayoutConstraints().ToArray()[0];

            _groupDescView.AddConstraint(_bioHeightConstraint);

            View.SetNeedsUpdateConstraints();
            View.LayoutIfNeeded();
        }

        void UpdatePlusAttributes()
        {
            UpdateColor();

            if (ViewModel.IsPlus) {
                _plusButtonsContainerConstraint.Constant = 100.0f;
                _upcomingContainerConstraint.Constant = 0.0f;
                _bottomContainerConstraint.Constant = 100.0f;

                _plusContainer.Subviews.ForEach(v => v.SetNeedsDisplay());

            } else {
                _bottomContainerConstraint.Constant = 153.0f;
                _upcomingContainerConstraint.Constant = 153.0f;
                _upcomingListView.SetLayoutConstraints();
            }
            View.SetNeedsUpdateConstraints();
            View.LayoutIfNeeded();
        }

        #endregion


        void UpdateColor ()
        {
            ActivityColors colors = YuFitStyleKitExtension.GetColorGroup("Yellow");
            if (ViewModel.IsPlus) {
                colors = YuFitStyleKitExtension.GetColorGroup("Green");
            }

            _managedByLabel.TextColor = colors.Light;
            _countTitleLabel.TextColor = colors.Light;
            _joinLeaveLabel.TextColor = colors.Light;

            _avatarView.StrokeColor = colors.Base;
            _avatarView.SetNeedsDisplay();

            _joinButton.DiamondColor = colors.Base;
            _joinButton.SetNeedsDisplay();
            _leaveButton.DiamondColor = colors.Base;
            _leaveButton.SetNeedsDisplay();
            _countDiamondView.DiamondColor = colors.Base;
            _countDiamondView.SetNeedsDisplay();
            _upcomingListView.UpcomingSeparator.BackgroundColor = colors.Base;
            _upcomingListView.OptionalTitle.TextColor = colors.Light;
        }

    }
}

