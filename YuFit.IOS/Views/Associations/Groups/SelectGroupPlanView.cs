﻿using System;
using System.Collections.Generic;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Foundation;
using StoreKit;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Associations.Groups;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using YuFit.IOS.Extensions;

namespace YuFit.IOS.Views.Associations.Groups
{
    internal class CustomPaymentObserver : SKPaymentTransactionObserver {
        private InAppPurchaseManager theManager;

        public CustomPaymentObserver(InAppPurchaseManager manager)
        {
            theManager = manager;
        }

        // called when the transaction status is updated
        public override void UpdatedTransactions (SKPaymentQueue queue, SKPaymentTransaction[] transactions)
        {
            Console.WriteLine ("UpdatedTransactions");
            foreach (SKPaymentTransaction transaction in transactions)
            {
                switch (transaction.TransactionState)
                {
                    case SKPaymentTransactionState.Purchased:
                        theManager.CompleteTransaction(transaction);
                        break;
                    case SKPaymentTransactionState.Failed:
                        theManager.FailedTransaction(transaction);
                        break;
                        // Consumable products do not get Restored, so this is not implemented in this sample.
                        //                  case SKPaymentTransactionState.Restored:
                        //                      theManager.RestoreTransaction(transaction);
                        //                      break;
                    default:
                        break;
                }
            }
        }

        public override void PaymentQueueRestoreCompletedTransactionsFinished(SKPaymentQueue queue)
        {
            throw new NotImplementedException("Consumable product purchases do not get Restored, so this is not implemented in this sample.");
        }
    }

    public class InAppPurchaseManager : SKProductsRequestDelegate {
        public static NSString InAppPurchaseManagerProductsFetchedNotification = new NSString("InAppPurchaseManagerProductsFetchedNotification");
        public static NSString InAppPurchaseManagerTransactionFailedNotification  = new NSString("InAppPurchaseManagerTransactionFailedNotification");
        public static NSString InAppPurchaseManagerTransactionSucceededNotification  = new NSString("InAppPurchaseManagerTransactionSucceededNotification");
        public static NSString InAppPurchaseManagerRequestFailedNotification = new NSString("InAppPurchaseManagerRequestFailedNotification");

        SKProductsRequest productsRequest;
        CustomPaymentObserver theObserver;

       // public static NSAction Done {get;set;}

        public InAppPurchaseManager ()
        {
            theObserver = new CustomPaymentObserver(this);

            // Call this once upon startup of in-app-purchase activities
            // This also kicks off the TransactionObserver which handles the various communications
            SKPaymentQueue.DefaultQueue.AddTransactionObserver(theObserver);
        }

        // Verify that the iTunes account can make this purchase for this application
        public bool CanMakePayments()
        {
            return SKPaymentQueue.CanMakePayments;  
        }

        // request multiple products at once
        public void RequestProductData (List<string> productIds)
        {
            var array = new NSString[productIds.Count];
            for (var i = 0; i < productIds.Count; i++) {
                array[i] = new NSString(productIds[i]);
            }
            NSSet productIdentifiers = NSSet.MakeNSObjectSet<NSString>(array);          

            //set up product request for in-app purchase
            productsRequest  = new SKProductsRequest(productIdentifiers);
            productsRequest.Delegate = this; // SKProductsRequestDelegate.ReceivedResponse
            productsRequest.Start();
        }

        public override void ReceivedResponse (SKProductsRequest request, SKProductsResponse response)
        {
            SKProduct[] products = response.Products;

            NSDictionary userInfo = null;
            if (products.Length > 0) {
                NSObject[] productIdsArray = new NSObject[response.Products.Length];
                NSObject[] productsArray = new NSObject[response.Products.Length];
                for (int i = 0; i < response.Products.Length; i++) {
                    productIdsArray[i] = new NSString(response.Products[i].ProductIdentifier);
                    productsArray[i] = response.Products[i];
                }
                userInfo = NSDictionary.FromObjectsAndKeys (productsArray, productIdsArray);
            }
            NSNotificationCenter.DefaultCenter.PostNotificationName(InAppPurchaseManagerProductsFetchedNotification,this,userInfo);

            foreach (string invalidProductId in response.InvalidProducts) {
                Console.WriteLine("Invalid product id: " + invalidProductId );
            }
        }

        public void PurchaseProduct(string appStoreProductId)
        {
            Console.WriteLine("PurchaseProduct " + appStoreProductId);
            //SKMutablePayment payment = SKMutablePayment.PaymentWithProduct (appStoreProductId);
            //payment.Quantity = 4;
            SKPayment payment = SKPayment.PaymentWithProduct (appStoreProductId);
            SKPaymentQueue.DefaultQueue.AddPayment (payment);
        }

        public void CompleteTransaction (SKPaymentTransaction transaction)
        {
            Console.WriteLine("CompleteTransaction " + transaction.TransactionIdentifier);
            var productId = transaction.Payment.ProductIdentifier;
            Console.WriteLine(productId);
            //var qty = transaction.Payment.Quantity;
//            if (productId == ConsumableViewController.Buy5ProductId)
//                CreditManager.Add(5); // 5 * qty
//            else if (productId == ConsumableViewController.Buy10ProductId)
//                CreditManager.Add(10); // 10 * qty
//            else
//                Console.WriteLine ("Shouldn't happen, there are only two products");

            FinishTransaction(transaction, true);
        }
        public void FailedTransaction (SKPaymentTransaction transaction)
        {
            //SKErrorPaymentCancelled == 2
            if (transaction.Error != null) {
                if (transaction.Error?.Code == 2) // user cancelled
                    Console.WriteLine("User CANCELLED FailedTransaction Code=" + transaction.Error.Code + " " + transaction.Error.LocalizedDescription);
                else // error!
                    Console.WriteLine("FailedTransaction Code=" + transaction.Error?.Code + " " + transaction.Error?.LocalizedDescription);
            }

            FinishTransaction(transaction,false);
        }
        public void FinishTransaction(SKPaymentTransaction transaction, bool wasSuccessful)
        {
            Console.WriteLine("FinishTransaction " + wasSuccessful);
            // remove the transaction from the payment queue.
            SKPaymentQueue.DefaultQueue.FinishTransaction(transaction);     // THIS IS IMPORTANT - LET'S APPLE KNOW WE'RE DONE !!!!

            using (var pool = new NSAutoreleasePool()) {
                NSDictionary userInfo = NSDictionary.FromObjectsAndKeys(new NSObject[] {transaction},new NSObject[] { new NSString("transaction")});
                if (wasSuccessful) {
                    // send out a notification that we’ve finished the transaction
                    NSNotificationCenter.DefaultCenter.PostNotificationName(InAppPurchaseManagerTransactionSucceededNotification,this,userInfo);
                } else {
                    // send out a notification for the failed transaction
                    NSNotificationCenter.DefaultCenter.PostNotificationName(InAppPurchaseManagerTransactionFailedNotification,this,userInfo);
                }
            }
        }

        /// <summary>
        /// Probably could not connect to the App Store (network unavailable?)
        /// </summary>
        public override void RequestFailed (SKRequest request, NSError error)
        {
            Console.WriteLine (" ** InAppPurchaseManager RequestFailed() " + error.LocalizedDescription);
            using (var pool = new NSAutoreleasePool()) {
                NSDictionary userInfo = NSDictionary.FromObjectsAndKeys(new NSObject[] {error},new NSObject[] {new NSString("error")});
                // send out a notification for the failed transaction
                NSNotificationCenter.DefaultCenter.PostNotificationName(InAppPurchaseManagerRequestFailedNotification,this,userInfo);
            }
        }
    }

    public class SelectGroupPlanView : BaseViewController<SelectGroupPlanViewModel>
    {
        const float LineMargin = 25.0f;
        const float LineThickness = 1.0f;

        readonly UIView             _topContainer       = new UIView();
        readonly UIView             _titleContainer     = new UIView();
        readonly UILabel            _firstLabel         = new UILabel();
        readonly UIView             _secondRowContainer = new UIView();
        readonly UILabel            _groupLabel         = new UILabel();
        readonly UILabel            _plusLabel          = new UILabel();
        readonly UILabel            _planLabel          = new UILabel();

        readonly UIView             _selectionContainer = new UIView();
        readonly SeparatorLine      _verticalLine       = new SeparatorLine();
        readonly SeparatorLine      _horizontalLine     = new SeparatorLine();

        readonly PlanOptionView     _planOption1        = new PlanOptionView();
        readonly PlanOptionView     _planOption2        = new PlanOptionView();
        readonly PlanOptionView     _planOption3        = new PlanOptionView();
        readonly PlanOptionView     _planOption4        = new PlanOptionView();


        NSObject priceObserver, succeededObserver, failedObserver, requestObserver;
        InAppPurchaseManager iap;
        List<string> products;
        bool pricesLoaded = false;


        public static string Buy5ProductId = "YUFIT_GROUP_PLUS_1M",
        Buy10ProductId = "YUFIT_GROUP_PLUS_3M";

        public SelectGroupPlanView ()
        {
            iap = new InAppPurchaseManager();
            products = new List<string>() { Buy5ProductId, Buy10ProductId };

        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            CreateViewHierarchy();
            ConfigureViewSpecifics();
            SetupConstraints();
            SetupBindings();

            _planOption1.AddGestureRecognizer(new UITapGestureRecognizer((sender) => {
                iap.PurchaseProduct (Buy5ProductId);
            }));

            //RequestProductData(new List<string> { "YUFIT_GROUP_PLUS_1M", "YUFIT_GROUP_PLUS_3M"});
        }

        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear(animated);

            priceObserver = NSNotificationCenter.DefaultCenter.AddObserver (InAppPurchaseManager.InAppPurchaseManagerProductsFetchedNotification, 
                (notification) => {
                var info = notification.UserInfo;
                if (info == null) return;

                var NSBuy5ProductId = new NSString(Buy5ProductId);
                var NSBuy10ProductId = new NSString(Buy10ProductId);

                if (info.ContainsKey(NSBuy5ProductId)) {
                    pricesLoaded = true;

                    var product = (SKProduct) info.ObjectForKey(NSBuy5ProductId);

                    Console.WriteLine("Product id: " + product.ProductIdentifier);
                    Console.WriteLine("Product title: " + product.LocalizedTitle);
                    Console.WriteLine("Product description: " + product.LocalizedDescription);
                    Console.WriteLine("Product price: " + product.Price);
                    //Console.WriteLine("Product l10n price: " + product.LocalizedPrice());   

//                    buy5Button.Enabled = true;
//                    buy5Title.Text = product.LocalizedTitle;
//                    buy5Description.Text = product.LocalizedDescription;
//                    buy5Button.SetTitle(String.Format (Buy, product.LocalizedPrice()), UIControlState.Normal);
                }
                if (info.ContainsKey(NSBuy10ProductId)) {
                    pricesLoaded = true;

                    var product = (SKProduct) info.ObjectForKey(NSBuy10ProductId);

                    Console.WriteLine("Product id: " + product.ProductIdentifier);
                    Console.WriteLine("Product title: " + product.LocalizedTitle);
                    Console.WriteLine("Product description: " + product.LocalizedDescription);
                    Console.WriteLine("Product price: " + product.Price);
                    //Console.WriteLine("Product l10n price: " + product.LocalizedPrice());   

//                    buy10Button.Enabled = true;
//                    buy10Title.Text = product.LocalizedTitle;
//                    buy10Description.Text = product.LocalizedDescription;
//                    buy10Button.SetTitle(String.Format (Buy, product.LocalizedPrice()), UIControlState.Normal);
                }
            });

            // only if we can make payments, request the prices
            if (iap.CanMakePayments()) {
                // now go get prices, if we don't have them already
                if (!pricesLoaded)
                    iap.RequestProductData(products); // async request via StoreKit -> App Store
            } else {
                // can't make payments (purchases turned off in Settings?)
//                buy5Button.SetTitle ("AppStore disabled", UIControlState.Disabled);
//                buy10Button.SetTitle ("AppStore disabled", UIControlState.Disabled);
            }

            succeededObserver = NSNotificationCenter.DefaultCenter.AddObserver (InAppPurchaseManager.InAppPurchaseManagerTransactionSucceededNotification,
                (notification) => {
                Console.WriteLine("SUCCESS");
            });

            failedObserver = NSNotificationCenter.DefaultCenter.AddObserver (InAppPurchaseManager.InAppPurchaseManagerTransactionFailedNotification,
                (notification) => {
                Console.WriteLine ("Transaction Failed");
            });

            requestObserver = NSNotificationCenter.DefaultCenter.AddObserver (InAppPurchaseManager.InAppPurchaseManagerRequestFailedNotification, 
                (notification) => {
                // TODO: 
                Console.WriteLine ("Request Failed");
            });
        }

        public override void ViewWillDisappear (bool animated)
        {
            NSNotificationCenter.DefaultCenter.RemoveObserver (priceObserver);
            NSNotificationCenter.DefaultCenter.RemoveObserver (succeededObserver);
            NSNotificationCenter.DefaultCenter.RemoveObserver (failedObserver);
            NSNotificationCenter.DefaultCenter.RemoveObserver (requestObserver);
            base.ViewWillDisappear(animated);
        }

        void CreateViewHierarchy ()
        {
            View.AddSubviews(new [] { _topContainer, _selectionContainer });
            _topContainer.AddSubviews(new [] { _titleContainer });
            _titleContainer.AddSubviews(new [] { _firstLabel, _secondRowContainer });
            _secondRowContainer.AddSubviews(new [] { _groupLabel, _plusLabel, _planLabel });
            _selectionContainer.AddSubviews(new UIView [] { _verticalLine, _horizontalLine, _planOption1, _planOption2, _planOption3, _planOption4 });
        }

        void ConfigureViewSpecifics ()
        {
            #region This info will come from the itunes connect programatically
            _planOption1.MonthCount.Text = "1";
            _planOption2.MonthCount.Text = "3";
            _planOption3.MonthCount.Text = "6";
            _planOption4.MonthCount.Text = "12";

            _planOption1.SaveLabel.Text  = "no savings";
            _planOption2.SaveLabel.Text  = "save 10%";
            _planOption3.SaveLabel.Text  = "save 20%";
            _planOption4.SaveLabel.Text  = "save 50%";
                                         
            _planOption1.CostLabel.Text  = "12.99$";
            _planOption2.CostLabel.Text  = "24.99$";
            _planOption3.CostLabel.Text  = "49.99$";
            _planOption4.CostLabel.Text  = "99.99$";
            #endregion

            _plusLabel.Text = "+";

            _firstLabel.Font = UIFont.FromName("HelveticaNeueLTStd-UltLtCn", 34);
            _groupLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 28);
            _plusLabel.Font  = UIFont.FromName("HelveticaNeueLTStd-BdCn", 24);
            _planLabel.Font  = UIFont.FromName("HelveticaNeueLTStd-UltLtCn", 34);

            _firstLabel.TextColor = YuFitStyleKit.White;
            _groupLabel.TextColor = YuFitStyleKit.White;
            _plusLabel.TextColor = YuFitStyleKit.Green;
            _planLabel.TextColor = YuFitStyleKit.White;

            _verticalLine.BackgroundColor = YuFitStyleKit.Green;
            _horizontalLine.BackgroundColor = YuFitStyleKit.Green;
        }

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<SelectGroupPlanView, SelectGroupPlanViewModel>();
            set.Bind().For(me => me.Title).To(vm => vm.DisplayName);

            set.Bind(_firstLabel).To(vm => vm.SelectYourTitle);
            set.Bind(_groupLabel).To(vm => vm.GroupTitle);
            set.Bind(_planLabel).To(vm => vm.PlanTitle);

            set.Bind(_planOption1.MonthLabel).To(vm => vm.MonthSingular);
            set.Bind(_planOption2.MonthLabel).To(vm => vm.MonthPlural);
            set.Bind(_planOption3.MonthLabel).To(vm => vm.MonthPlural);
            set.Bind(_planOption4.MonthLabel).To(vm => vm.MonthPlural);

//            set.Bind(_planOption1.Tap()).To(vm => vm.SelectPlan1Command.Command);
//            set.Bind(_planOption2.Tap()).To(vm => vm.SelectPlan2Command.Command);
//            set.Bind(_planOption3.Tap()).To(vm => vm.SelectPlan3Command.Command);
//            set.Bind(_planOption4.Tap()).To(vm => vm.SelectPlan4Command.Command);

            set.Apply ();
            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage);
        }

        #region Constraints

        void SetupConstraints ()
        {
            View.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            View.AddConstraints(
                _topContainer.WithSameWidth(View),
                _topContainer.WithSameCenterX(View),
                _topContainer.Height().EqualTo(105),
                _topContainer.Top().EqualTo().TopOf(View),

                _selectionContainer.WithSameWidth(View),
                _selectionContainer.WithSameCenterX(View),
                _selectionContainer.Top().EqualTo().BottomOf(_topContainer),
                _selectionContainer.Bottom().EqualTo().BottomOf(View)
            );

            SetupTopContainerConstraints();
            SetupBottomContainerConstraints();
        }

        #region TopContainer Constraints

        void SetupTopContainerConstraints ()
        {
            _topContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _topContainer.AddConstraints(
                _titleContainer.WithSameCenterX(_topContainer),
                _titleContainer.WithSameCenterY(_topContainer)
            );

            SetupTitleContainerConstraint();
        }

        void SetupTitleContainerConstraint ()
        {
            _titleContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _titleContainer.AddConstraints(
                _firstLabel.WithSameCenterX(_titleContainer),
                _firstLabel.Top().EqualTo().TopOf(_titleContainer),

                _secondRowContainer.WithSameCenterX(_titleContainer),
                _secondRowContainer.Top().EqualTo(2.0f).BottomOf(_firstLabel),

                _titleContainer.Right().EqualTo().RightOf(_secondRowContainer),
                _titleContainer.Bottom().EqualTo().BottomOf(_secondRowContainer)
            );

            SetupSecondRowContainerConstraint();
        }

        void SetupSecondRowContainerConstraint ()
        {
            _secondRowContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _secondRowContainer.AddConstraints(
                _groupLabel.CenterY().EqualTo(2.0f).CenterYOf(_secondRowContainer),
                _groupLabel.Left().EqualTo().LeftOf(_secondRowContainer),
                _plusLabel.WithSameCenterY(_secondRowContainer),
                _plusLabel.Left().EqualTo().RightOf(_groupLabel),
                _planLabel.WithSameCenterY(_secondRowContainer),
                _planLabel.Left().EqualTo(7.5f).RightOf(_plusLabel),

                _secondRowContainer.Right().EqualTo().RightOf(_planLabel),
                _secondRowContainer.Bottom().EqualTo().BottomOf(_groupLabel)
            );
        }

        #endregion

        void SetupBottomContainerConstraints ()
        {
            _selectionContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _selectionContainer.AddConstraints(
                _verticalLine.Width().EqualTo(LineThickness),
                _verticalLine.WithSameCenterX(_selectionContainer),
                _verticalLine.Top().EqualTo(LineMargin).TopOf(_selectionContainer),
                _verticalLine.Bottom().EqualTo(-LineMargin).BottomOf(_selectionContainer),

                _horizontalLine.Height().EqualTo(LineThickness),
                _horizontalLine.WithSameCenterY(_selectionContainer),
                _horizontalLine.Left().EqualTo(LineMargin).LeftOf(_selectionContainer),
                _horizontalLine.Right().EqualTo(-LineMargin).RightOf(_selectionContainer),

                _planOption1.Left().EqualTo(LineMargin).LeftOf(_selectionContainer),
                _planOption1.Right().EqualTo().CenterXOf(_selectionContainer),
                _planOption1.Top().EqualTo(LineMargin).TopOf(_selectionContainer),
                _planOption1.Bottom().EqualTo().CenterYOf(_selectionContainer),

                _planOption2.Left().EqualTo().CenterXOf(_selectionContainer),
                _planOption2.Right().EqualTo(-LineMargin).RightOf(_selectionContainer),
                _planOption2.Top().EqualTo(LineMargin).TopOf(_selectionContainer),
                _planOption2.Bottom().EqualTo().CenterYOf(_selectionContainer),

                _planOption3.Left().EqualTo(LineMargin).LeftOf(_selectionContainer),
                _planOption3.Right().EqualTo().CenterXOf(_selectionContainer),
                _planOption3.Top().EqualTo().CenterYOf(_selectionContainer),
                _planOption3.Bottom().EqualTo(-LineMargin).BottomOf(_selectionContainer),

                _planOption4.Left().EqualTo().CenterXOf(_selectionContainer),
                _planOption4.Right().EqualTo(-LineMargin).RightOf(_selectionContainer),
                _planOption4.Top().EqualTo().CenterYOf(_selectionContainer),
                _planOption4.Bottom().EqualTo(-LineMargin).BottomOf(_selectionContainer)
            );
        }

        #endregion

        // Verify that the iTunes account can make this purchase for this application
        public bool CanMakePayments()
        {
            return SKPaymentQueue.CanMakePayments;  
        }

        // request multiple products at once
        public void RequestProductData (List<string> productIds)
        {
            var array = new NSString[productIds.Count];
            for (var i = 0; i < productIds.Count; i++) {
                array[i] = new NSString(productIds[i]);
            }
            NSSet productIdentifiers = NSSet.MakeNSObjectSet<NSString>(array);

            //set up product request for in-app purchase
            SKProductsRequest productsRequest  = new SKProductsRequest(productIdentifiers);
            productsRequest.Delegate = new MyDelegate(this); // SKProductsRequestDelegate.ReceivedResponse
            productsRequest.Start();
        }
    }

    public class MyDelegate : SKProductsRequestDelegate
    {
        public static NSString InAppPurchaseManagerProductsFetchedNotification = new NSString("InAppPurchaseManagerProductsFetchedNotification");
        public static NSString InAppPurchaseManagerTransactionFailedNotification = new NSString("InAppPurchaseManagerTransactionFailedNotification");
        public static NSString InAppPurchaseManagerTransactionSucceededNotification = new NSString("InAppPurchaseManagerTransactionSucceededNotification");
        public static NSString InAppPurchaseManagerRequestFailedNotification = new NSString("InAppPurchaseManagerRequestFailedNotification");

        public MyDelegate (SelectGroupPlanView theView)
        {
            
        }

        // received response to RequestProductData - with price,title,description info
        public override void ReceivedResponse (SKProductsRequest request, SKProductsResponse response)
        {
            SKProduct[] products = response.Products;

            NSDictionary userInfo = null;
            if (products.Length > 0) {
                NSObject[] productIdsArray = new NSObject[response.Products.Length];
                NSObject[] productsArray = new NSObject[response.Products.Length];
                for (int i = 0; i < response.Products.Length; i++) {
                    productIdsArray[i] = new NSString(response.Products[i].ProductIdentifier);
                    productsArray[i] = response.Products[i];
                }
                userInfo = NSDictionary.FromObjectsAndKeys (productsArray, productIdsArray);
            }
            NSNotificationCenter.DefaultCenter.PostNotificationName(InAppPurchaseManagerProductsFetchedNotification,this,userInfo);

            foreach (string invalidProductId in response.InvalidProducts) {
                Console.WriteLine("Invalid product id: " + invalidProductId );
            }

            SKProduct a = null;
            foreach (var product in response.Products) {
                Console.WriteLine("product: " + product.LocalizedTitle + " " + product.Price.ToString());
                a = product;
            }

            PurchaseProduct(a.ProductIdentifier);
        }

        public void PurchaseProduct(string appStoreProductId)
        {
            SKPayment   payment = SKPayment.PaymentWithProduct(appStoreProductId);
            SKPaymentQueue.DefaultQueue.AddPayment (payment);
        }
    }
}


//- (void)validateProductIdentifiers:(NSArray *)productIdentifiers
//{
//    SKProductsRequest *productsRequest = [[SKProductsRequest alloc]
//        initWithProductIdentifiers:[NSSet setWithArray:productIdentifiers]];
//
//    // Keep a strong reference to the request.
//    self.request = productsRequest;
//    productsRequest.delegate = self;
//    [productsRequest start];
//}
//
//// SKProductsRequestDelegate protocol method
//- (void)productsRequest:(SKProductsRequest *)request
//didReceiveResponse:(SKProductsResponse *)response
//{
//    self.products = response.products;
//
//    for (NSString *invalidIdentifier in response.invalidProductIdentifiers) {
//        // Handle any invalid product identifiers.
//    }
//
//    [self displayStoreUI]; // Custom method
//}