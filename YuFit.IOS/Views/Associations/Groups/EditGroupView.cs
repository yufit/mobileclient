﻿using System;
using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using CoreGraphics;
using UIKit;

using YuFit.Core.ViewModels.Associations.Groups;

using YuFit.IOS.Controls;
using YuFit.IOS.Controls.Buttons;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;
using YuFit.IOS.Views.Associations.Groups.Edit;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Views.Associations.Groups
{
    public class MyScrollView : UIScrollView
    {
        public bool KeyboardIsUp { get; set; }
        public MyScrollView () {
            DelaysContentTouches = false;
            CanCancelContentTouches = true;
        }

        public override void TouchesBegan (Foundation.NSSet touches, UIEvent evt)
        {
            if (KeyboardIsUp) { Superview.EndEditing(true); }
            else              { base.TouchesBegan(touches, evt); }
        }

        public override bool TouchesShouldCancelInContentView(UIView view) { return true; }
    }

    public class EditGroupView : BaseViewController<EditGroupViewModel>
    {
        readonly MyScrollView               _scrollView                 = new MyScrollView();

        readonly UIView                     _photoContainerView         = new UIView();
        readonly UILabel                    _addCoverLabel              = new UILabel();
        readonly UIImageView                _photoView                  = new UIImageView();

        readonly AddFriendsButton           _addMemberDiamondView       = new AddFriendsButton();
        readonly PictureContainer           _avatarView                 = new PictureContainer();
        readonly DiamondShapeView           _privateView                = new DiamondShapeView();
        readonly UILabel                    _privateLabel               = new UILabel();

        readonly GroupInfoContainer         _groupInfoContainer         = new GroupInfoContainer();
        readonly GroupDescriptionContainer  _groupDescriptionContainer  = new GroupDescriptionContainer();

        readonly ApplyView                  _applyView                  = new ApplyView();

        MvxImageViewLoader                  _photoLoader;

        UIBarButtonItem                     _trashButton = new UIBarButtonItem(UIBarButtonSystemItem.Trash);
        UIBarButtonItem                     _cancelButton = new UIBarButtonItem(UIBarButtonSystemItem.Cancel);

        CGPoint _previousOffset;

        NSLayoutConstraint _descriptionHeightConstraint;

        bool _isCreator;
        public bool IsCreator {
            get { return _isCreator; }
            set { _isCreator = value; SetupDeleteButton(); }
        }

        bool _isEditing;
        public bool IsEditing {
            get { return _isEditing; }
            set { _isEditing = value; SetupCancelButton(); }
        }

        bool _isPrivate;
        public bool IsPrivate {
            get { return _isPrivate; }
            set { _isPrivate = value; UpdatePrivateView(); }
        }

        string _description;
        public string DescriptionText {
            get { return _description; }
            set { 
                _description = value;

                if (!_groupDescriptionContainer.DescriptionPlaceholder.IsFirstResponder) {
                    _groupDescriptionContainer.RemoveConstraint(_descriptionHeightConstraint);
                    var _desiredHeight = _groupDescriptionContainer.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
                    _descriptionHeightConstraint = _groupDescriptionContainer.Height().EqualTo(_desiredHeight).ToLayoutConstraints().ToArray()[0];
                    _groupDescriptionContainer.AddConstraint(_descriptionHeightConstraint);

                    View.SetNeedsUpdateConstraints();
                    View.LayoutIfNeeded();
                }
            }
        }

        public EditGroupView ()
        {
            _photoLoader  = new MvxImageViewLoader(() => _photoView);
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            Title = ViewModel.DisplayName;

            SetupSubviews();
            SetupBindings();

            WatchKeyboard = true;

            _photoContainerView.UserInteractionEnabled = true;
        }

        public override void ViewWillAppear (bool animated)
        {
            _groupInfoContainer.AttachKeyboardMonitoring(ViewModel);
            base.ViewWillAppear(animated);
        }

        public override void ViewWillDisappear (bool animated)
        {
            _groupInfoContainer.DetachKeyboardMonitoring();
            base.ViewWillDisappear(animated);
        }

        void SetupDeleteButton()
        {
            if (!IsCreator) { return; }

            UITextAttributes attributes = new UITextAttributes();
            attributes.Font = NavigationBar.TitleFont;
            _trashButton.SetTitleTextAttributes(attributes, UIControlState.Normal);
            NavigationItem.RightBarButtonItem = _trashButton;
        }

        void SetupCancelButton()
        {
            if (IsEditing) { return; }

            UITextAttributes attributes = new UITextAttributes();
            attributes.Font = NavigationBar.TitleFont;
            attributes.TextColor = NavigationBar.FontColor;
            _cancelButton.SetTitleTextAttributes(attributes, UIControlState.Normal);
            NavigationItem.LeftBarButtonItem = _cancelButton;
        }

        void SetupSubviews()
        {
            SetupScrollView();
            SetupPhotoView();
            SetupAvatarView();
            SetupPrivateView();

            SetupAddMemberContainer();
            SetupGroupInfoContainer();
            SetupDescriptionContainer();
            SetupApplyView();

            View.AddConstraints(
                _scrollView.WithSameWidth(View),
                _scrollView.WithSameCenterX(View),
                _scrollView.Top().EqualTo().TopOf(View),
                _scrollView.Bottom().EqualTo().TopOf(_applyView)
            );

            // Once everything is added, we can adjust the scrollview bottom
            // constraint which will define the amount of scrollable content.
            _scrollView.AddConstraints( 
                _groupDescriptionContainer.Bottom().EqualTo(-Spacing).BottomOf(_scrollView)
            );
        }

        void SetupScrollView ()
        {
            _scrollView.TranslatesAutoresizingMaskIntoConstraints = false;
            Add(_scrollView);

        }

        void SetupPhotoView ()
        {
            _photoContainerView.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_photoContainerView);

            _photoContainerView.BackgroundColor = YuFitStyleKit.Black;
            View.AddConstraints(
                _photoContainerView.Width().EqualTo().WidthOf(View),
                _photoContainerView.Height().EqualTo().WidthOf(View).WithMultiplier(0.5531401f),
                _photoContainerView.Left().EqualTo().LeftOf(View)
            );

            _scrollView.AddConstraints(
                _photoContainerView.Top().EqualTo().TopOf(_scrollView)
            );

            _addCoverLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _photoContainerView.AddSubview(_addCoverLabel);
            _photoContainerView.AddConstraints(
                _addCoverLabel.WithSameCenterX(_photoContainerView),
                _addCoverLabel.CenterY().EqualTo(2.0f).CenterYOf(_photoContainerView)
            );

            _addCoverLabel.Font       = UIFont.FromName("HelveticaNeueLTStd-MdCn", 60);
            _addCoverLabel.TextColor  = UIColor.FromRGBA(24.0f/255.0f,24.0f/255.0f,24.0f/255.0f,1.0f);

            _photoView.TranslatesAutoresizingMaskIntoConstraints = false;
            _photoContainerView.AddSubview(_photoView);
            _photoView.ContentMode = UIViewContentMode.ScaleAspectFit;
            _photoView.ClipsToBounds = true;
            _photoContainerView.ClipsToBounds = true;

            _photoContainerView.AddConstraints(
                _photoView.WithSameWidth(_photoContainerView),
                _photoView.WithSameTop(_photoContainerView),
                _photoView.WithSameCenterX(_photoContainerView),
                _photoView.Height().EqualTo().HeightOf(_photoContainerView)
            );
        }

        void SetupAvatarView()
        {
            _avatarView.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_avatarView);
            _avatarView.StrokeWidth = 3.0f;

            View.AddConstraints(
                _avatarView.Width().EqualTo(132.0f),
                _avatarView.Height().EqualTo().WidthOf(_avatarView),
                _avatarView.Left().EqualTo(29.0f).LeftOf(View),
                _avatarView.CenterY().EqualTo().BottomOf(_photoContainerView)
            );

            _avatarView.Label.Font       = UIFont.FromName("HelveticaNeueLTStd-MdCn", 28);
            _avatarView.Label.TextColor  = UIColor.FromRGBA(24.0f/255.0f,24.0f/255.0f,24.0f/255.0f,1.0f);
        }

        void SetupAddMemberContainer()
        {
            _addMemberDiamondView.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_addMemberDiamondView);

            View.AddConstraints(
                _addMemberDiamondView.Width().EqualTo(66.0f),
                _addMemberDiamondView.Height().EqualTo().WidthOf(_addMemberDiamondView),
                _addMemberDiamondView.Left().EqualTo(8.0f).RightOf(_avatarView),
                _addMemberDiamondView.CenterY().EqualTo().BottomOf(_photoContainerView)
            );

            _addMemberDiamondView.Hidden = true;
        }

        void SetupPrivateView()
        {
            _privateView.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_privateView);
            _privateView.DiamondColor = YuFitStyleKit.Red;

            View.AddConstraints(
                _privateView.Width().EqualTo(46.0f),
                _privateView.Height().EqualTo().WidthOf(_privateView),
                _privateView.Right().EqualTo(-29.0f).RightOf(View),
                _privateView.CenterY().EqualTo().BottomOf(_photoContainerView)
            );

            _privateLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_privateLabel);

            View.AddConstraints(
                _privateLabel.Top().EqualTo(0.0f).BottomOf(_privateView),
                _privateLabel.CenterX().EqualTo().CenterXOf(_privateView)
            );
            _privateLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 14);
        }

        void SetupGroupInfoContainer ()
        {
            _groupInfoContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_groupInfoContainer);
//
//            AddChildViewController(_groupInfoContainer.SelectGroupTypeView);
//            AddChildViewController(_groupInfoContainer.SelectSportKindView);

            View.AddConstraints(
                _groupInfoContainer.Left().EqualTo(Spacing).LeftOf(View),
                _groupInfoContainer.Right().EqualTo(-Spacing).RightOf(View)
            );
            _scrollView.AddConstraints(
                _groupInfoContainer.Top().EqualTo(5.0f).BottomOf(_avatarView)
            );
        }

        protected void SetupApplyView(bool alwaysEnabled = true, bool showLabel = true, bool blur = false)
        {
            _applyView.Layer.ZPosition = 2000;
            Add(_applyView);

            _applyView.Setup(true, true);

            _applyView.ApplyTriangle.PercentFill = 1.0f; 
            _applyView.ApplyTriangle.ShowDownTriangle = false; 
            _applyView.ApplyLabel.Hidden = false;

            _applyView.TranslatesAutoresizingMaskIntoConstraints = false;
            _applyView.AddConstraints(
                _applyView.Height().EqualTo(ViewModel.IsEditing?0.0f:90.0f)
            );
            View.AddConstraints(
                _applyView.WithSameCenterX(View),
                _applyView.WithSameWidth(View),
                _applyView.Bottom().EqualTo().BottomOf(View)
            );
            _applyView.ClipsToBounds = true;
        }

        void SetupDescriptionContainer ()
        {
            _groupDescriptionContainer.DescriptionPlaceholder.ScrollEnabled = false;

            _groupDescriptionContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            _scrollView.Add(_groupDescriptionContainer);

            var _desiredHeight = _groupDescriptionContainer.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
            _descriptionHeightConstraint = _groupDescriptionContainer.Height().EqualTo(_desiredHeight).ToLayoutConstraints().ToArray()[0];
            _groupDescriptionContainer.AddConstraint(_descriptionHeightConstraint);

            View.AddConstraints(
                _groupDescriptionContainer.Left().EqualTo(Spacing).LeftOf(View),
                _groupDescriptionContainer.Right().EqualTo(-Spacing).RightOf(View)
            );
            _scrollView.AddConstraints(
                _groupDescriptionContainer.Top().EqualTo(2.5f).BottomOf(_groupInfoContainer)
            );
        }

        void SetupBindings()
        {
            var set = this.CreateBindingSet<EditGroupView, EditGroupViewModel>();

            set.Bind().For(me => me.Title).To(vm => vm.DisplayName);
            set.Bind(_photoLoader).To(vm => vm.PhotoUrl);

            set.Bind(_trashButton).For("Clicked").To(vm => vm.DeleteCommand.Command);
            set.Bind(_cancelButton).For("Clicked").To(vm => vm.CancelCommand.Command);

            set.Bind(_groupInfoContainer.Name).For("Placeholder").To(vm => vm.GroupNamePlaceholderText);
            set.Bind(_groupInfoContainer.Address).For("Placeholder").To(vm => vm.LocationPlaceholderText);

            set.Bind(_groupInfoContainer.Name).To(vm => vm.GroupName);
            set.Bind(_groupInfoContainer.Address).To(vm => vm.Hometown);


//            set.Bind(_groupInfoContainer.SelectGroupTypeView).For(me => me.SelectItemInListCommand).To(vm => vm.SelectGroupTypeCommand);
//            set.Bind(_groupInfoContainer.SelectGroupTypeView).For(gic => gic.CurrentSelection).To(vm => vm.Type);
//            set.Bind(_groupInfoContainer.SelectSportKindView).For(me => me.SelectItemInListCommand).To(vm => vm.SelectSportKindCommand);
//            set.Bind(_groupInfoContainer.SelectSportKindView).For(gic => gic.CurrentSelection).To(vm => vm.SportKind);
//
            set.Bind(_groupDescriptionContainer.DescriptionPlaceholder).For(l => l.Data).To(vm => vm.Description);
            set.Bind(_groupDescriptionContainer.DescriptionPlaceholder).For("Placeholder").To(vm => vm.DescriptionPlaceholderText);
            set.Bind().For(me => me.DescriptionText).To(vm => vm.Description);

            set.Bind(_addCoverLabel).To(vm => vm.AddPictureCommand.DisplayName);
            set.Bind(_photoContainerView.Tap()).To(vm => vm.AddPictureCommand.Command);
            set.Bind(_photoView).For(p => p.Image).To(vm => vm.GroupPhotoData).WithConversion("InMemoryImage");

            set.Bind(_avatarView.Label).To(vm => vm.AddAvatarCommand.DisplayName);
            set.Bind(_avatarView.Tap()).To(vm => vm.AddAvatarCommand.Command);
            set.Bind(_avatarView.AvatarImage).For(p => p.Image).To(vm => vm.GroupAvatarData).WithConversion("InMemoryImage");
            set.Bind(_avatarView.ImageViewLoader).To(vm => vm.AvatarUrl);

            set.Bind(_addMemberDiamondView.Tap()).To(vm => vm.AddMembersCommand.Command);

            set.Bind(_privateView.Tap()).To(vm => vm.ToggleVisibilityCommand.Command);

            set.Bind().For(me => me.IsPrivate).To(vm => vm.Private);
            set.Bind().For(me => me.IsCreator).To(vm => vm.IsCreator);
            set.Bind().For(me => me.IsEditing).To(vm => vm.IsEditing);

            set.Bind(_applyView.Tap()).To(vm => vm.CreateCommand.Command);
            set.Bind(_applyView.ApplyLabel).To(vm => vm.CreateCommand.DisplayName).WithConversion("ToLowercase");

            set.Apply ();

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage);
        }

        void UpdatePrivateView()
        {
            if (IsPrivate) {
                _privateLabel.Text = ViewModel.PrivateText;
                _privateLabel.TextColor = YuFitStyleKit.Red;
                _privateView.DiamondColor = YuFitStyleKit.Red;
            } else {
                _privateLabel.Text = ViewModel.PublicText;
                _privateLabel.TextColor = YuFitStyleKit.Green;
                _privateView.DiamondColor = YuFitStyleKit.Green;
            }
            _privateView.SetNeedsDisplay();
        }

        protected override void KeyboardWillShow (nfloat keyboardTop, double animationSpeed)
        {
            _scrollView.KeyboardIsUp = true;
            _photoContainerView.UserInteractionEnabled = false;
            _avatarView.UserInteractionEnabled = false;

//            _groupInfoContainer.SelectGroupTypeView.View.UserInteractionEnabled = false;
//            _groupInfoContainer.SelectSportKindView.View.UserInteractionEnabled = false;
//
            _previousOffset = _scrollView.ContentOffset;
            nfloat realContainerBottom;
            if (_groupInfoContainer.IsEditing) {
                realContainerBottom = _groupInfoContainer.Frame.Bottom - _scrollView.ContentOffset.Y;

                var keyboardDelta = realContainerBottom - keyboardTop;
                Console.WriteLine("{0} = {1} - {2}", keyboardDelta, realContainerBottom, keyboardTop);
                if (keyboardDelta > 0) {
                    _scrollView.SetContentOffset(new CGPoint(0, _scrollView.ContentOffset.Y + keyboardDelta), true);
                } 
            } else if (_groupDescriptionContainer.IsEditing) {

                _groupDescriptionContainer.DescriptionPlaceholder.ScrollEnabled = true;

                _groupDescriptionContainer.RemoveConstraint(_descriptionHeightConstraint);
                _descriptionHeightConstraint = _groupDescriptionContainer.Height().EqualTo(keyboardTop).ToLayoutConstraints().ToArray()[0];
                _groupDescriptionContainer.AddConstraint(_descriptionHeightConstraint);

                View.SetNeedsUpdateConstraints();
                View.LayoutIfNeeded();

                Console.WriteLine("{0}", _groupDescriptionContainer.Frame.Top);
                _scrollView.SetContentOffset(new CGPoint(0, _groupDescriptionContainer.Frame.Top), true);
                _scrollView.ScrollEnabled = false;
            }

            base.KeyboardWillShow(keyboardTop, animationSpeed);
        }

        protected override void KeyboardWillHide (double animationSpeed)
        {
            _scrollView.KeyboardIsUp = true;
            _photoContainerView.UserInteractionEnabled = true;
            _avatarView.UserInteractionEnabled = true;
//            _groupInfoContainer.SelectGroupTypeView.View.UserInteractionEnabled = true;
//            _groupInfoContainer.SelectSportKindView.View.UserInteractionEnabled = true;
//
            _groupDescriptionContainer.DescriptionPlaceholder.ScrollEnabled = false;

            _groupDescriptionContainer.RemoveConstraint(_descriptionHeightConstraint);
            var _desiredHeight = _groupDescriptionContainer.SizeThatFits(new CGSize(View.Frame.Width, nfloat.MaxValue)).Height;
            _descriptionHeightConstraint = _groupDescriptionContainer.Height().EqualTo(_desiredHeight).ToLayoutConstraints().ToArray()[0];
            _groupDescriptionContainer.AddConstraint(_descriptionHeightConstraint);

            View.LayoutIfNeeded();
            _scrollView.SetContentOffset(_previousOffset, true);

            if (_scrollView.ContentSize.Height > View.Frame.Height) {
                _scrollView.SetContentOffset(_previousOffset, true);
            } else {
                _scrollView.SetContentOffset(new CGPoint(0.0f, 0.0f), true);
            }

            _scrollView.ScrollEnabled = true;
            base.KeyboardWillHide(animationSpeed);
        }
    }
}

