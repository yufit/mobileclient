﻿using System.Collections.Generic;

using Cirrious.FluentLayouts.Touch;

using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Associations.Groups;

using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Associations.Groups
{
    public class GroupPlusAdvantagesView : BaseViewController<GroupPlusAdvantagesViewModel>
    {
        readonly UIView             _groupTitleContainer        = new UIView();
        readonly UILabel            _groupLabel                 = new UILabel();
        readonly UILabel            _plusLabel                  = new UILabel();
        readonly UILabel            _extendedFeaturesLabel      = new UILabel();

        readonly UIView             _featuresContainer          = new UIView();
        readonly List<UILabel>      _featureLabels              = new List<UILabel>();

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();


            // The view is static, load the string directly, no need to bind.
            ViewModel.Features.ForEach(feature => {
                var label = new UILabel();
                label.Text = feature;
                _featureLabels.Add(label);
            });

            _groupLabel.Text = ViewModel.GroupTitle;
            _plusLabel.Text = "+";
            _extendedFeaturesLabel.Text = ViewModel.ExtendedFeaturesTitle;

            CreateViewHierarchy();
            ConfigureViewSpecifics();
            SetupConstraints();
            SetupKeyboard();
            SetupBindings();
        }

        void CreateViewHierarchy ()
        {
            View.AddSubviews(new UIView[] { _groupTitleContainer, _extendedFeaturesLabel, _featuresContainer});
            _groupTitleContainer.AddSubviews( new UIView[] { _groupLabel, _plusLabel });
            _featuresContainer.AddSubviews(_featureLabels.ToArray());
        }

        void ConfigureViewSpecifics()
        {
            _groupLabel.Font                    = UIFont.FromName("HelveticaNeueLTStd-Cn", 32);
            _plusLabel.Font                     = UIFont.FromName("HelveticaNeueLTStd-Cn", 38);
            _extendedFeaturesLabel.Font         = UIFont.FromName("HelveticaNeueLTStd-UltLtCn", 30);

            _groupLabel.TextColor               = YuFitStyleKit.White;
            _plusLabel.TextColor                = YuFitStyleKit.Green;
            _extendedFeaturesLabel.TextColor    = YuFitStyleKit.Green;

            _featureLabels.ForEach(label => {
                label.Font                      = UIFont.FromName("HelveticaNeueLTStd-UltLtCn", 26);
                label.TextColor                 = YuFitStyleKit.White;
            });
        }

        void SetupConstraints()
        {
            View.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            View.AddConstraints(
                _groupTitleContainer.WithSameCenterX(View),
                _groupTitleContainer.Top().EqualTo(15.0f).TopOf(View),

                _extendedFeaturesLabel.WithSameCenterX(View),
                _extendedFeaturesLabel.Top().EqualTo().BottomOf(_groupTitleContainer),

                _featuresContainer.WithSameCenterX(View),
                _featuresContainer.WithSameWidth(View),
                _featuresContainer.Top().EqualTo(25.0f).BottomOf(_extendedFeaturesLabel)
            );

            SetupGroupTitleContainerConstraint();
            SetupFeaturesContainerConstraint();
        }

        void SetupGroupTitleContainerConstraint ()
        {
            _groupTitleContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _groupTitleContainer.AddConstraints(
                _groupLabel.CenterY().EqualTo(2.0f).CenterYOf(_groupTitleContainer),
                _groupLabel.Left().EqualTo().LeftOf(_groupTitleContainer),
                _plusLabel.WithSameCenterY(_groupTitleContainer),
                _plusLabel.Left().EqualTo().RightOf(_groupLabel),

                _groupTitleContainer.Right().EqualTo().RightOf(_plusLabel),
                _groupTitleContainer.Bottom().EqualTo().BottomOf(_groupLabel)
            );
        }

        void SetupFeaturesContainerConstraint ()
        {
            _featuresContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            var labels = _featureLabels.ToArray();
            UILabel lastLabel = null;
            for (int i=0; i<labels.Length; ++i) {
                var label = labels[i];

                if (lastLabel == null) {
                    _featuresContainer.AddConstraints(
                        label.Top().EqualTo().TopOf(_featuresContainer),
                        label.WithSameCenterX(_featuresContainer)
                    );
                } else {
                    _featuresContainer.AddConstraints(
                        label.Top().EqualTo(5.0f).BottomOf(lastLabel),
                        label.WithSameCenterX(_featuresContainer)
                    );
                }

                lastLabel = label;
            }
            _featuresContainer.AddConstraints(
                _featuresContainer.Bottom().EqualTo().BottomOf(lastLabel)
            );
        }

        void SetupKeyboard()
        {
            
        }

        void SetupBindings()
        {
            
        }
    }
}

