using System;
using System.Collections.Generic;

using Cirrious.FluentLayouts.Touch;

using Foundation;
using StoreKit;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Associations.Groups;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Associations.Groups
{

    public class PlanOptionView : UIView
    {
        private readonly UIView                         _contentView    = new UIView();

        private readonly IconView                       _calendarIcon   = new IconView(YuFitStyleKit.Green, "EmptyCalendar");
        public  readonly LabelWithAdaptiveTextHeight    MonthCount      = new LabelWithAdaptiveTextHeight();
        public  readonly LabelWithAdaptiveTextHeight    MonthLabel      = new LabelWithAdaptiveTextHeight();

        private readonly UIView                         _labelsView     = new UIView();
        public  readonly LabelWithAdaptiveTextHeight    SaveLabel       = new LabelWithAdaptiveTextHeight();
        public  readonly LabelWithAdaptiveTextHeight    CostLabel       = new LabelWithAdaptiveTextHeight();

        public PlanOptionView ()
        {
            MonthCount.Font = UIFont.FromName("HelveticaNeueLTStd-Cn", 36);
            MonthLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 26);
            SaveLabel.Font  = UIFont.FromName("HelveticaNeueLTStd-UltLtCn", 22);
            CostLabel.Font  = UIFont.FromName("HelveticaNeueLTStd-LtCn", 22);

            MonthCount.TextColor = YuFitStyleKit.Black;
            MonthLabel.TextColor = YuFitStyleKit.Black;
            SaveLabel.TextColor = YuFitStyleKit.Green;
            CostLabel.TextColor = YuFitStyleKit.Gray;

            SaveLabel.TextAlignment = UITextAlignment.Center;
            CostLabel.TextAlignment = UITextAlignment.Center;
            MonthLabel.TextAlignment = UITextAlignment.Center;
            MonthCount.TextAlignment = UITextAlignment.Center;

            AddSubviews(new [] { _contentView });
            _contentView.AddSubviews(new UIView[] { _calendarIcon, _labelsView });
            _calendarIcon.AddSubviews(new [] { MonthCount, MonthLabel });
            _labelsView.AddSubviews(new UIView[] { SaveLabel, CostLabel });

            Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            this.AddConstraints(
                _contentView.WithSameCenterX(this),
                _contentView.WithSameCenterY(this),
                _contentView.WithSameWidth(this).WithMultiplier(0.75f),
                _contentView.WithSameHeight(this).WithMultiplier(0.75f)
            );

            _contentView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _contentView.AddConstraints(
                _calendarIcon.WithSameTop(_contentView),
                _calendarIcon.WithSameCenterX(_contentView),
                _calendarIcon.WithSameHeight(_contentView).WithMultiplier(0.63f),
                _calendarIcon.Width().EqualTo().HeightOf(_calendarIcon),

                _labelsView.WithSameBottom(_contentView),
                _labelsView.WithSameCenterX(_contentView),
                _labelsView.WithSameHeight(_contentView).WithMultiplier(0.33f),
                _labelsView.WithSameWidth(_contentView)
            );

            _calendarIcon.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _calendarIcon.AddConstraints(
                MonthLabel.WithSameCenterX(_calendarIcon),
                MonthLabel.Height().EqualTo().HeightOf(_calendarIcon).WithMultiplier(0.25f),
                MonthLabel.Width().EqualTo().WidthOf(_calendarIcon).WithMultiplier(0.9f),
                MonthLabel.Bottom().EqualTo(-3.0f).BottomOf(_calendarIcon),

                MonthCount.WithSameCenterX(_calendarIcon),
                MonthCount.Height().EqualTo().HeightOf(_calendarIcon).WithMultiplier(0.33f),
                MonthCount.Width().EqualTo().WidthOf(_calendarIcon).WithMultiplier(0.9f),
                MonthCount.Bottom().EqualTo().TopOf(MonthLabel)
            );

            _labelsView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _labelsView.AddConstraints(
                SaveLabel.WithSameCenterX(_labelsView),
                SaveLabel.Height().EqualTo().HeightOf(_labelsView).WithMultiplier(0.5f),
                SaveLabel.Width().EqualTo().WidthOf(_labelsView).WithMultiplier(0.9f),
                SaveLabel.Top().EqualTo().TopOf(_labelsView),

                CostLabel.WithSameCenterX(_labelsView),
                CostLabel.Height().EqualTo().HeightOf(_labelsView).WithMultiplier(0.5f),
                CostLabel.Width().EqualTo().WidthOf(_labelsView).WithMultiplier(0.9f),
                CostLabel.Bottom().EqualTo().BottomOf(_labelsView)
            );
        }
    }
    
}

//- (void)validateProductIdentifiers:(NSArray *)productIdentifiers
//{
//    SKProductsRequest *productsRequest = [[SKProductsRequest alloc]
//        initWithProductIdentifiers:[NSSet setWithArray:productIdentifiers]];
//
//    // Keep a strong reference to the request.
//    self.request = productsRequest;
//    productsRequest.delegate = self;
//    [productsRequest start];
//}
//
//// SKProductsRequestDelegate protocol method
//- (void)productsRequest:(SKProductsRequest *)request
//didReceiveResponse:(SKProductsResponse *)response
//{
//    self.products = response.products;
//
//    for (NSString *invalidIdentifier in response.invalidProductIdentifiers) {
//        // Handle any invalid product identifiers.
//    }
//
//    [self displayStoreUI]; // Custom method
//}