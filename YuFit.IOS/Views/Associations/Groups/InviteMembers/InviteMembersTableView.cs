﻿using UIKit;

using MvvmCross.Binding.BindingContext;

using YuFit.Core.ViewModels.CreateActivity;
using YuFit.Core.ViewModels.Associations.Groups;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Views.Associations.Groups.InviteMembers
{
    public class InviteMembersTableView : MvxTableViewController
    {
        protected new SelectParticipantsViewModel ViewModel { get { return (SelectParticipantsViewModel) base.ViewModel; } }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            TableView.BackgroundColor = UIColor.Clear;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.RowHeight = 60;

            var source = new TableViewSource<InviteMembersTableViewCell>(TableView, InviteMembersTableViewCell.Key);
            TableView.Source = source;

            var set = this.CreateBindingSet<InviteMembersTableView, InviteMembersViewModel>();
            set.Bind(source).To(vm => vm.InvitableUsers);
            set.Bind(source).For(s => s.SelectionChangedCommand).To(vm => vm.SelectFriendCommand.Command);
            set.Apply();
        }

        public override void ViewDidDisappear (bool animated)
        {
            base.ViewDidDisappear(animated);

            if (TableView != null) {
                if (TableView.Source != null) {
                    TableView.Source.Dispose();
                    TableView.Source = null;
                }
                TableView.Dispose ();
            }
        }
    }
}

