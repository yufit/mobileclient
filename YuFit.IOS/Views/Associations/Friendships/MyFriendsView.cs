﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Foundation;

using YuFit.Core.ViewModels;

using YuFit.Core.ViewModels.Associations.Friendships;
using YuFit.IOS.Controls;
using YuFit.IOS.Controls.UserTableView;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Associations.Friendships
{
    public class MyFriendsView : BaseViewController<MyFriendsViewModel>
    {
        class MyFriendsTableViewCell : UserInfoTableViewCell<UserInfoViewModel> {
            public static readonly NSString Key = new NSString ("MyFriendsTableViewCell");
        }

        class MyFriendsTableView : UserInfoTableView<MyFriendsViewModel, MyFriendsTableViewCell> {
            public MyFriendsTableView () : base(MyFriendsTableViewCell.Key) { }
        }

        readonly SeparatorLine      _topLine    = new SeparatorLine();
        readonly SeparatorLine      _bottomLine = new SeparatorLine();
        readonly MyFriendsTableView _tableView  = new MyFriendsTableView();
       
        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
//            NavigationController.SetNavigationBarHidden(true, false);

            AddTopLine();
            AddBottomLine();
            AddTableView();

//            _addButton = new UIBarButtonItem(UIBarButtonSystemItem.Add);
//            NavigationItem.RightBarButtonItem = _addButton;
//            _addButton.TintColor = Themes.Dashboard.HomeIconColor;

            SetupBindings();
        }

        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear(animated);
        }

        void AddTopLine()
        {
            
            View.AddSubviews(_topLine);
            _topLine.TranslatesAutoresizingMaskIntoConstraints = false;
            _topLine.AddConstraints(_topLine.Height().EqualTo(1.0f));
            _topLine.BackgroundColor = YuFitStyleKit.Yellow;

            View.AddConstraints(
                _topLine.Left().EqualTo(36.0f).LeftOf(View),
                _topLine.Right().EqualTo(-36.0f).RightOf(View),
                _topLine.Top().EqualTo(16.0f).TopOf(View)
            );
        }

        void AddBottomLine()
        {
            View.AddSubviews(_bottomLine);

            _bottomLine.TranslatesAutoresizingMaskIntoConstraints = false;
            _bottomLine.AddConstraints(_bottomLine.Height().EqualTo(1.0f));
            _bottomLine.BackgroundColor = YuFitStyleKit.Yellow;
            View.AddConstraints(
                _bottomLine.Left().EqualTo(36.0f).LeftOf(View),
                _bottomLine.Right().EqualTo(-36.0f).RightOf(View),
                _bottomLine.Bottom().EqualTo(-36.0f).BottomOf(View)
            );
        }

        void AddTableView()
        {
            _tableView.BindingContext = BindingContext;
            //AddChildViewController(_tableView);
            View.AddSubview(_tableView.View);

            _tableView.View.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddConstraints(
                _tableView.View.Top().EqualTo(8.0f).BottomOf(_topLine),
                _tableView.View.Left().EqualTo().LeftOf(View),
                _tableView.View.Bottom().EqualTo(-8.0f).TopOf(_bottomLine),
                _tableView.View.Right().EqualTo().RightOf(View)
            );
        }

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<MyFriendsView, MyFriendsViewModel>();
            //set.Bind(_addButton).For("Clicked").To(vm => vm.FindFriendsCommand.Command);
            set.Bind(_tableView.Source).To(vm => vm.Friends);
            set.Bind(_tableView.Source).For(s => s.SelectionChangedCommand).To(vm => vm.ShowUserProfileCommand.Command);
            set.Apply();

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );
        }
    }
}

