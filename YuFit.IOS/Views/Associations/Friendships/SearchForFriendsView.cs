﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Foundation;
using UIKit;

using YuFit.Core.ViewModels.Associations.Groups;

using YuFit.IOS.Controls;
using YuFit.IOS.Controls.GroupTableView;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;
using YuFit.Core.ViewModels.Associations.Friendships;
using YuFit.IOS.Controls.UserTableView;
using YuFit.Core.ViewModels;

namespace YuFit.IOS.Views.Associations.Friendships
{
    public class SearchForFriendsView : BaseViewController<SearchForFriendsViewModel>
    {
        // Analysis disable once ConvertToStaticType
        class MyFriendsTableViewCell : UserInfoTableViewCell<UserInfoViewModel> {
            public static readonly NSString Key = new NSString ("MyFriendsTableViewCell");
        }

        class MyFriendsTableView : UserInfoTableView<MyFriendsViewModel, MyFriendsTableViewCell> {
            public MyFriendsTableView () : base(MyFriendsTableViewCell.Key) { }
        }

        readonly UISearchBar        _searchBar  = new UISearchBar();
        readonly SeparatorLine      _topLine    = new SeparatorLine();
        readonly SeparatorLine      _bottomLine = new SeparatorLine();
        readonly MyFriendsTableView  _tableView  = new MyFriendsTableView();

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            AddSearchBar();
            AddTopLine();
            AddBottomLine();
            AddTableView();

            SetupBindings();

            ViewModel.PropertyChanged += ViewModel_PropertyChanged;
        }

        void ViewModel_PropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsBusy" && _searchBar.IsFirstResponder) {
                if (!ViewModel.IsBusy) {
                    _searchBar.ResignFirstResponder();
                }
            }
        }

        public override void ViewDidDisappear (bool animated)
        {
            ViewModel.PropertyChanged -= ViewModel_PropertyChanged;
            base.ViewDidDisappear(animated);
        }

        void AddSearchBar ()
        {
            View.AddSubview(_searchBar);
            _searchBar.BarStyle = UIBarStyle.Black;
            _searchBar.SearchBarStyle = UISearchBarStyle.Default;

            _searchBar.Layer.Opacity = 0.8f;
            _searchBar.Layer.CornerRadius = 6.0f;
            _searchBar.ClipsToBounds = true;
            _searchBar.Layer.BorderWidth = 1.0f;
            _searchBar.KeyboardAppearance = UIKeyboardAppearance.Dark;

            _searchBar.TranslatesAutoresizingMaskIntoConstraints = false;

            View.AddConstraints(
                _searchBar.Left().EqualTo(36.0f).LeftOf(View),
                _searchBar.Right().EqualTo(-36.0f).RightOf(View),
                _searchBar.Top().EqualTo().TopOf(View)
            );
        }

        void AddTopLine()
        {

            View.AddSubviews(_topLine);
            _topLine.TranslatesAutoresizingMaskIntoConstraints = false;
            _topLine.AddConstraints(_topLine.Height().EqualTo(1.0f));
            _topLine.BackgroundColor = YuFitStyleKit.Yellow;

            View.AddConstraints(
                _topLine.Left().EqualTo(36.0f).LeftOf(View),
                _topLine.Right().EqualTo(-36.0f).RightOf(View),
                _topLine.Top().EqualTo(16.0f).BottomOf(_searchBar)
            );
        }

        void AddBottomLine()
        {
            View.AddSubviews(_bottomLine);

            _bottomLine.TranslatesAutoresizingMaskIntoConstraints = false;
            _bottomLine.AddConstraints(_bottomLine.Height().EqualTo(1.0f));
            _bottomLine.BackgroundColor = YuFitStyleKit.Yellow;
            View.AddConstraints(
                _bottomLine.Left().EqualTo(36.0f).LeftOf(View),
                _bottomLine.Right().EqualTo(-36.0f).RightOf(View),
                _bottomLine.Bottom().EqualTo(-36.0f).BottomOf(View)
            );
        }

        void AddTableView()
        {
            _tableView.BindingContext = BindingContext;
            AddChildViewController(_tableView);
            View.AddSubview(_tableView.View);

            _tableView.View.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddConstraints(
                _tableView.View.Top().EqualTo(8.0f).BottomOf(_topLine),
                _tableView.View.Left().EqualTo().LeftOf(View),
                _tableView.View.Bottom().EqualTo(-8.0f).TopOf(_bottomLine),
                _tableView.View.Right().EqualTo().RightOf(View)
            );
        }

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<SearchForFriendsView, SearchForFriendsViewModel>();
            set.Bind(_tableView.Source).To(vm => vm.Users);
            set.Bind(_tableView.Source).For(s => s.SelectionChangedCommand).To(vm => vm.ShowUserProfileCommand.Command);

            set.Bind(_searchBar).To(vm => vm.SearchTerm);
            set.Bind(_searchBar).For("SearchButtonClicked").To(vm => vm.SearchCommand.Command);

            set.Apply();

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );
        }


    }
}

