using System;

using Cirrious.MvvmCross.Binding.BindingContext;

using CoreGraphics;
using UIKit;

using YuFit.Core.Models;
using YuFit.Core.ViewModels.Dashboard;
using YuFit.Core.ViewModels.Dashboard.ActivitySummary;
using YuFit.IOS.Controls;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;
using Coc.MvvmCross.Plugins.Location;

namespace YuFit.IOS.Views.Dashboard.ActivitySummary
{
    [Attributes.PresentViewInContainer("SummaryView")]
    public class ActivitySummaryView : BaseViewController<ActivitySummaryViewModel>
    {
        public string Id { get { return ViewModel.Id; } }
        public CocLocation Location { get { return ViewModel.Location; } }

        private ActivityType _activityType;
        public ActivityType ActivityType {
            get { return _activityType; }
            set { _activityType = value; UpdateContent(); }
        }

        private BaseActivityDetailsViewModelRaw _activityViewModel;
        public BaseActivityDetailsViewModelRaw ActivityViewModel
        {
            get { return _activityViewModel; }
            set { _activityViewModel = value; UpdateSpecifics(); }
        }

        private DarkGradientImageView _backgroundImageView;

        DetailsContainerView        _detailsContainer   = new DetailsContainerView(); 
        readonly SummaryView        _summaryView        = new SummaryView();
        readonly JoinButton         _joinButton         = new JoinButton();
        readonly InscriptionsButton _invitesButton      = new InscriptionsButton();

        public ActivitySummaryView ()
        {
            IsRadialGradientVisible = false;
        }

        /// <summary>
        /// We swap the view of this view controller to the PointyView. It will
        /// have a bottom diamond at the bottom of the view (kind of like a V).
        /// </summary>
        public override void ViewDidLoad ()
        {
            View = new PointyView(View.Bounds);
            base.ViewDidLoad();

            _backgroundImageView = new DarkGradientImageView(UIImage.FromFile ("Media/cycling-road.jpg"), View.Bounds);
            Add(_backgroundImageView);

            Add(_summaryView);
            _summaryView.DiamondButton.StrokeWidth = 0.0f;
            _summaryView.DiamondButton.Inset = 0.0f;

            Add(_joinButton);
            _joinButton.DiamondButton.StrokeWidth = 0.0f;
            _joinButton.DiamondButton.Inset = 0.0f;

            Add(_invitesButton);
            _invitesButton.DiamondButton.StrokeWidth = 1.0f;
            _invitesButton.DiamondButton.Inset = 0.0f;

            var set = this.CreateBindingSet<ActivitySummaryView, ActivitySummaryViewModel>();
//            set.Bind(View.SwipeUp()).To(vm => vm.DismissCommand.Command);
            set.Bind().For(me => me.ActivityType).To(vm => vm.ActivityType);
            set.Bind(_invitesButton).For(w => w.Participants).To(vm => vm.Participants);
            set.Bind(_invitesButton.InvitedLabel).To(vm => vm.InvitedTitle);
            set.Bind(_invitesButton.Tap()).To(vm => vm.ShowInvitedParticipantsCommand.Command);

            set.Bind(_summaryView).For(sv => sv.SubSport).To(vm => vm.SportSubType);
            set.Bind(_summaryView).For(sv => sv.StartTime).To(vm => vm.StartTime);
            set.Bind(_summaryView.SportIconView).For(sv => sv.DrawIcon).To(vm => vm.ActivityType.TypeName);
            set.Bind(_summaryView.Tap()).To(vm => vm.ShowActivityDetailCommand.Command);

            set.Bind(_joinButton).For(jb => jb.JoinString).To(vm => vm.IamInString);
            set.Bind(_joinButton.Tap()).To(vm => vm.JoinCommand.Command);
            set.Bind().For(me => me.ActivityViewModel).To(vm => vm.ActivityViewModel);
            set.Apply();

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, "Fetching info...");
        }


        public void Dismiss()
        {
            ViewModel.DismissCommand.Command.Execute(this);
        }

        void UpdateContent ()
        {
            if (_activityType.TypeName.ToLower() != "cycling" &&
                _activityType.TypeName.ToLower() != "running" &&
                _activityType.TypeName.ToLower() != "swimming") {
                _backgroundImageView.ReplaceImage(UIImage.FromFile ("Media/" + _activityType.TypeName.ToLower() + ".jpg"));
            } else {
                 _backgroundImageView.ReplaceImage(UIImage.FromFile ("Media/" + _activityType.TypeName.ToLower() + "-" + ViewModel.SportSubTypeRaw.ToLower() +".jpg"));
            }

//                public enum CyclingType      { Road, Mtb, Cyclocross, TT, Track, Fixedgear, Fatbike, Spinning };

            ActivityColors colors;

            if (_activityType != null && _activityType.Enabled) {
                colors = YuFitStyleKitExtension.ActivityBackgroundColors(_activityType.TypeName);
            } else {
                colors = YuFitStyleKitExtension.ActivityBackgroundColors("Cycling");
            }


            _summaryView.DiamondButton.DiamondColor = colors.Base;
            _summaryView.SetNeedsDisplay();

            _joinButton.DiamondButton.DiamondColor = colors.Base;
            _joinButton.SetNeedsDisplay();

            _invitesButton.DiamondButton.StrokeColor = colors.Base;
            _invitesButton.SetNeedsDisplay();

            _invitesButton.CountLabelColor = colors.Base;
        }

        void UpdateSpecifics()
        {
            if (_activityViewModel is CyclingActivityDetailsViewModel) {
                _detailsContainer = new CyclingActivitySpecificView(_activityViewModel);
            } else if (_activityViewModel is RunningActivityDetailsViewModel) {
                _detailsContainer = new RunningActivitySpecificView(_activityViewModel);
            } else if (_activityViewModel is SwimmingActivityDetailsViewModel) {
                _detailsContainer = new SwimmingActivitySpecificView(_activityViewModel);
            } else if (_activityViewModel is YogaActivityDetailsViewModel) {
                _detailsContainer = new YogaActivitySpecificView(_activityViewModel);
            }

            Add(_detailsContainer);

            _detailsContainer.Layer.AnchorPoint = new CGPoint(0, 0);
            _detailsContainer.Transform = CGAffineTransform.MakeRotation((float) (-Math.PI/4.0f));
            View.SetNeedsLayout();
        }

        public override void ViewDidLayoutSubviews ()
        {
            _backgroundImageView.Frame = View.Bounds;

            nfloat bigDiamondWidth = View.Bounds.Width * (2.0f/3.0f) - 5;
            _summaryView.Frame = new CGRect {
                X = View.Bounds.Width/2.0f - bigDiamondWidth/2.0f,
                Y = View.Bounds.Height - bigDiamondWidth - 5.0f,
                Width = bigDiamondWidth,
                Height = bigDiamondWidth
            };

            nfloat smallDiamondWidth = View.Bounds.Width * (1.0f/3.0f) - 5;
            _joinButton.Frame = new CGRect {
                X = View.Bounds.Width - smallDiamondWidth - 2.5f,
                Y = View.Bounds.Height - (bigDiamondWidth / 2.0f) - smallDiamondWidth - 7.5f,
                Width = smallDiamondWidth,
                Height = smallDiamondWidth
            };

            _invitesButton.Frame = new CGRect {
                X = View.Bounds.Width / 2.0f + 2.5f,
                Y = View.Bounds.Height - bigDiamondWidth - 5.0f - (smallDiamondWidth/2.0f) - 2.5f,
                Width = smallDiamondWidth,
                Height = smallDiamondWidth
            };

            if (_detailsContainer != null)
            {
                nfloat diamondHalfHeight = View.Bounds.Width / 2.0f;
                nfloat diamondStartY = View.Bounds.Height - diamondHalfHeight;
                nfloat height = (float) Math.Sqrt(Math.Pow(smallDiamondWidth, 2.0f)/2.0f);
                nfloat width = (float) Math.Sqrt(Math.Pow(bigDiamondWidth/2.0f, 2.0f) + Math.Pow(bigDiamondWidth/2.0f, 2.0f)) + height + 2.5f;
                _detailsContainer.Bounds = new CGRect{
                    X = 2.5f,
                    Y = diamondStartY - 2.5f, 
                    Width = width,
                    Height = (nfloat) Math.Round(height)
                };
                _detailsContainer.Center = new CGPoint(2.5f, diamondStartY - 2.5f);

                _detailsContainer.SliderBlockWidth = (float) Math.Sqrt(Math.Pow(bigDiamondWidth/2.0f, 2.0f) + Math.Pow(bigDiamondWidth/2.0f, 2.0f));
                _detailsContainer.SmallBlockWidth = (float) Math.Sqrt(Math.Pow(smallDiamondWidth, 2.0f)/2.0f);
                _detailsContainer.ContentHeight = (height * 0.6f);

            }
            base.ViewDidLayoutSubviews();
        }
    }
}
