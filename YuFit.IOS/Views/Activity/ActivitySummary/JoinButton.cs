using CoreGraphics;
using UIKit;
using YuFit.IOS.Themes;


namespace YuFit.IOS.Views.Dashboard.ActivitySummary
{

    public class JoinButton : DetailsDiamondView
    {
        readonly UILabel _joinLabel = new UILabel();

        public JoinButton () { DoConstruction(); }
        public JoinButton (CGRect frame) : base(frame) { DoConstruction(); }

        public string JoinString { 
            get { return _joinLabel.Text; }
            set { _joinLabel.Text = value; UpdateLabel(); }
        }

        void DoConstruction()
        {
            AddSubview(_joinLabel);
            _joinLabel.TextColor = YuFitStyleKit.White;
            _joinLabel.TextAlignment = UITextAlignment.Center;
            _joinLabel.Font = Themes.Dashboard.SummaryView.JoinButton.TextFont;
            _joinLabel.Lines = 0;
        }

        void UpdateLabel ()
        {
            _joinLabel.Frame = Bounds; 
            _joinLabel.SizeToFit();
            _joinLabel.Center = new CGPoint(Bounds.Width / 2.0f, Bounds.Height / 2.0f + 4.0f);
        }

        public override void LayoutSubviews ()
        {
            UpdateLabel();
            base.LayoutSubviews();
        }
    }
    
}
