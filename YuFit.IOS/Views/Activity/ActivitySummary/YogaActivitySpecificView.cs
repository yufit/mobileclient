using Cirrious.MvvmCross.Binding.BindingContext;

using Fcaico.Controls.ArrowSlider;

using UIKit;

using YuFit.Core.ViewModels.Dashboard.ActivitySummary;
using YuFit.IOS.Themes;
using YuFit.Core.Models.Activities;

namespace YuFit.IOS.Views.Dashboard.ActivitySummary
{

    public class YogaActivitySpecificView : ActivitySpecificsView<YogaActivityDetailsViewModel> {
        readonly ArrowSliderView _durationSlider;

        public YogaActivitySpecificView (BaseActivityDetailsViewModelRaw activityViewModel) : base(activityViewModel) 
        {
            colors = YuFitStyleKitExtension.ActivityBackgroundColors("Yoga");
            _durationSlider = BottomSlider;
            _durationSlider.BackgroundColor = UIColor.Clear;
            _durationSlider.ArrowColor = colors.Base;
            TopSlider.Hidden = true;

            _durationSlider.Values = ViewModel.DurationValues;

            var set = this.CreateBindingSet<YogaActivitySpecificView, YogaActivityDetailsViewModel>();
            set.Bind(_durationSlider).For(slider => slider.CurrentValue).To(vm => vm.Duration);
            set.Apply ();
        }
    }
    
}
