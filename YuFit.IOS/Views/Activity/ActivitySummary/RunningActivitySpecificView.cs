using System;

using Cirrious.MvvmCross.Binding.BindingContext;

using Fcaico.Controls.ArrowSlider;

using CoreGraphics;
using UIKit;

using YuFit.Core.Models;
using YuFit.Core.Models.Activities;
using YuFit.Core.ViewModels.Dashboard;
using YuFit.Core.ViewModels.Dashboard.ActivitySummary;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Dashboard.ActivitySummary
{

    public class RunningActivitySpecificView : ActivitySpecificsView<RunningActivityDetailsViewModel> {
        ArrowSliderView     _durationSlider; 
        ArrowSliderView     _distanceSlider;
        UITextField         _speed;
        UITextField         _speedUnit;

        public RunningActivitySpecificView (BaseActivityDetailsViewModelRaw activityViewModel) : base(activityViewModel) 
        {
            colors = YuFitStyleKitExtension.ActivityBackgroundColors("Running");
            _durationSlider = BottomSlider;
            _distanceSlider = TopSlider;
            _speed = TopLabel;
            _speedUnit = BottomLabel;

            _durationSlider.ArrowColor = colors.Base;
            _durationSlider.Values = ViewModel.DurationValues;
            _distanceSlider.ArrowColor = colors.Base;
            _distanceSlider.Values = ViewModel.DistanceValues;

            _speed.BackgroundColor = UIColor.Clear;
            _speed.TextColor = colors.Base;
            _speed.Font = Themes.Dashboard.SummaryView.SpeedLabels.SpeedTextFont;

            _speedUnit.BackgroundColor = UIColor.Clear;
            _speedUnit.TextColor = YuFitStyleKit.White;
            _speedUnit.Font = Themes.Dashboard.SummaryView.SpeedLabels.SpeedUnitTextFont;

            var set = this.CreateBindingSet<RunningActivitySpecificView, RunningActivityDetailsViewModel>();
            set.Bind(_distanceSlider).For(slider => slider.CurrentValue).To(vm => vm.Distance);
            set.Bind(_durationSlider).For(slider => slider.CurrentValue).To(vm => vm.Duration);
            set.Bind(_speed).To(vm => vm.Speed);
            set.Bind(_speedUnit).To(vm => vm.SpeedUnit);
            set.Apply ();
        }
    }
    
}
