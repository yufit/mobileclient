using Cirrious.MvvmCross.Binding.BindingContext;

using Fcaico.Controls.ArrowSlider;

using UIKit;

using YuFit.Core.ViewModels.Dashboard.ActivitySummary;
using YuFit.IOS.Themes;
using YuFit.Core.Models.Activities;

namespace YuFit.IOS.Views.Dashboard.ActivitySummary
{

    public class SwimmingActivitySpecificView : ActivitySpecificsView<SwimmingActivityDetailsViewModel> {
        readonly ArrowSliderView    _durationSlider; 
        readonly ArrowSliderView    _distanceSlider;

        public SwimmingActivitySpecificView (BaseActivityDetailsViewModelRaw activityViewModel) : base(activityViewModel) 
        {
            colors = YuFitStyleKitExtension.ActivityBackgroundColors("Swimming");
            _durationSlider = BottomSlider;
            _distanceSlider = TopSlider;

            _durationSlider.BackgroundColor = UIColor.Clear;
            _durationSlider.ArrowColor = colors.Base;
            _distanceSlider.BackgroundColor = UIColor.Clear;
            _distanceSlider.ArrowColor = colors.Base;

            _durationSlider.Values = ViewModel.DurationValues;
            _distanceSlider.Values = ViewModel.DistanceValues;

            var set = this.CreateBindingSet<SwimmingActivitySpecificView, SwimmingActivityDetailsViewModel>();
            set.Bind(_distanceSlider).For(slider => slider.CurrentValue).To(vm => vm.Distance);
            set.Bind(_durationSlider).For(slider => slider.CurrentValue).To(vm => vm.Duration);
            set.Apply ();
        }
    }
    
}
