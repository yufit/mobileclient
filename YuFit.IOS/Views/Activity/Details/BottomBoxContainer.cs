using System;
using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using CoreGraphics;

using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.Models;
using YuFit.Core.ViewModels.DisplayActivities;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using YuFit.IOS.Views;

namespace YuFit.IOS.Views.Activity.Details
{

    class BottomBoxContainer : UIView
    {
        public readonly UILabel     SubSportLabel       = new UILabel();

        public readonly UIView      LocationContainer   = new UIView();
        readonly IconView           _locationIcon       = new IconView();
        public readonly UILabel     AddressLabel        = new UILabel();

        readonly UIView             _createdByContainer = new UIView();
        readonly IconView           _createdByIcon      = new IconView();
        public readonly UILabel     CreatedByLabel      = new UILabel();
        public readonly UILabel     CreatorLabel        = new UILabel();

        string _address;
        public string Address {
            get { return _address; }
            set { _address = value; _locationIcon.SetNeedsDisplay(); }
        }

        public BottomBoxContainer ()
        {
            AddSubviews(new UIView[] {SubSportLabel, LocationContainer, _createdByContainer});
            Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            this.AddConstraints(
                LocationContainer.WithSameCenterX(this),
                LocationContainer.CenterY().EqualTo(3.0f).CenterYOf(this),

                SubSportLabel.Top().EqualTo(5.0f).TopOf(this),
                SubSportLabel.WithSameCenterX(this),

                _createdByContainer.Top().EqualTo().BottomOf(LocationContainer),
                _createdByContainer.WithSameCenterX(this)
            );

            ConfigureLocationContainer();
            ConfigureCreatedByContainer();

            SubSportLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 20);
            SubSportLabel.TextColor = YuFitStyleKit.White;
            AddressLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            AddressLabel.TextColor = YuFitStyleKit.White;
            CreatedByLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            CreatedByLabel.TextColor = YuFitStyleKit.White;
            CreatorLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            CreatorLabel.TextColor = YuFitStyleKit.Charcoal;
        }

        void ConfigureLocationContainer ()
        {
            LocationContainer.AddSubviews(new UIView[] {_locationIcon, AddressLabel});
            LocationContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            _locationIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawWhere1");

            LocationContainer.AddConstraints(
                _locationIcon.Left().EqualTo().LeftOf(LocationContainer),
                _locationIcon.WithSameCenterY(LocationContainer),
                _locationIcon.Height().EqualTo(3.0f).HeightOf(AddressLabel),
                _locationIcon.Width().EqualTo(3.0f).HeightOf(AddressLabel),

                AddressLabel.Left().EqualTo(5.0f).RightOf(_locationIcon),
                AddressLabel.CenterY().EqualTo(2.0f).CenterYOf(LocationContainer),

                LocationContainer.Right().EqualTo().RightOf(AddressLabel),
                LocationContainer.Bottom().EqualTo().BottomOf(_locationIcon)
            );
        }

        void ConfigureCreatedByContainer ()
        {
            _createdByContainer.AddSubviews(new UIView[] {_createdByIcon, CreatedByLabel, CreatorLabel});
            _createdByContainer.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            _createdByIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawWho2");

            _createdByContainer.AddConstraints(
                _createdByIcon.Left().EqualTo().LeftOf(_createdByContainer),
                _createdByIcon.WithSameCenterY(_createdByContainer),
                _createdByIcon.Height().EqualTo(3.0f).HeightOf(CreatedByLabel),
                _createdByIcon.Width().EqualTo(3.0f).HeightOf(CreatedByLabel),

                CreatedByLabel.Left().EqualTo(5.0f).RightOf(_createdByIcon),
                CreatedByLabel.CenterY().EqualTo(2.0f).CenterYOf(_createdByContainer),

                CreatorLabel.Left().EqualTo(5.0f).RightOf(CreatedByLabel),
                CreatorLabel.CenterY().EqualTo(2.0f).CenterYOf(_createdByContainer),

                _createdByContainer.Right().EqualTo().RightOf(CreatorLabel),
                _createdByContainer.Bottom().EqualTo().BottomOf(_createdByIcon)
            );
        }
    }
    
}
