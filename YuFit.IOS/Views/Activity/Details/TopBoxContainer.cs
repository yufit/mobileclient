using System;

using Cirrious.FluentLayouts.Touch;
using UIKit;
using YuFit.Core.Extensions;
using YuFit.IOS.Controls;

namespace YuFit.IOS.Views.Activity.Details
{
    class TopBoxContainer : UIView
    {
        public readonly WhenView           WhenView       = new WhenView();
        public readonly ActivityIconView   IconView       = new ActivityIconView();
        public readonly UIView             Specifics      = new UIView();

        DateTime _startTime;
        public DateTime StartTime {
            get { return _startTime; }
            set { _startTime = value; WhenView.Date = _startTime; }
        }

        public TopBoxContainer ()
        {
            AddSubviews(new UIView[] {WhenView, IconView, Specifics});
            Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            this.AddConstraints(
                WhenView.Top().EqualTo(3.0f).TopOf(this),
                WhenView.WithSameBottom(this),
                WhenView.WithSameLeft(this),
                WhenView.Width().EqualTo().WidthOf(this).WithMultiplier(0.33f),

                IconView.WithSameCenterX(this),
                IconView.WithSameCenterY(this),
                IconView.Height().EqualTo().HeightOf(this).WithMultiplier(0.9f),
                IconView.Width().EqualTo().HeightOf(this).WithMultiplier(0.9f),

                Specifics.WithSameTop(this),
                Specifics.WithSameBottom(this),
                Specifics.WithSameRight(this),
                Specifics.Width().EqualTo().WidthOf(this).WithMultiplier(0.33f)
            );
        }
    }
    
}
