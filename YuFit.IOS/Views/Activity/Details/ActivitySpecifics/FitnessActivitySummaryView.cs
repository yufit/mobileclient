﻿using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;


using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.DisplayActivities;

using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Activity.Details.ActivitySpecifics
{

    public class FitnessActivitySummaryView : ActivitySummaryView 
    {
        private readonly UILabel DurationLabel = new UILabel();
        private readonly UILabel IntensityLabel = new UILabel();

        public FitnessActivitySummaryView (BaseActivityDisplayViewModelRaw activityViewModel) : base(activityViewModel)
        {
            AddSubviews(new UIView[] {DurationLabel, IntensityLabel});

            Subviews.Where(v => v is UILabel).ForEach(v => {
                UILabel label = (UILabel) v;
                label.BackgroundColor = UIColor.Clear;
                label.TextAlignment = UITextAlignment.Center;
                label.TextColor = CrudFitEvent.WhenDiamond.TextColor;
                label.TranslatesAutoresizingMaskIntoConstraints = false;
            });

            DurationLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
            IntensityLabel.Font = CrudFitEvent.WhenDiamond.DayLabelTextFont;

            this.AddConstraints(
                DurationLabel.WithSameCenterX(this),
                DurationLabel.Bottom().EqualTo(2.0f).CenterYOf(this),

                IntensityLabel.WithSameCenterX(this),
                IntensityLabel.Top().EqualTo(2.0f).BottomOf(DurationLabel)
            );

            var set = this.CreateBindingSet<FitnessActivitySummaryView, FitnessActivityDisplayViewModel>();
            set.Bind(DurationLabel).To (vm => vm.Duration);
            set.Bind(IntensityLabel).To (vm => vm.Intensity);
            set.Apply ();
        }

    }
}
