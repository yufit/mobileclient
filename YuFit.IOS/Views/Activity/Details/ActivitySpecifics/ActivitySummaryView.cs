using MvvmCross.Binding.BindingContext;
using UIKit;
using YuFit.Core.ViewModels.DisplayActivities;

namespace YuFit.IOS.Views.Activity.Details.ActivitySpecifics
{
    public class ActivitySummaryView : UIView, IMvxBindingContextOwner 
    {

        #region IMvxBindingContextOwner implementation

        public IMvxBindingContext BindingContext { get; set; }

        public virtual object DataContext
        {
            get { return BindingContext.DataContext; }
            set { BindingContext.DataContext = value; }
        }

        #endregion

        public ActivitySummaryView (BaseActivityDisplayViewModelRaw activityViewModel) 
        { 
            this.CreateBindingContext (); 
            DataContext = activityViewModel;
        }
    }
}
