using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;


using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.DisplayActivities;

using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Activity.Details.ActivitySpecifics{

    public class RunningActivitySummaryView : ActivitySummaryView 
    {
        private readonly UILabel DurationLabel  = new UILabel();
        private readonly UILabel DistanceLabel  = new UILabel();
        private readonly UILabel IntensityLabel = new UILabel();

        public RunningActivitySummaryView (BaseActivityDisplayViewModelRaw activityViewModel) : base(activityViewModel)
        {
            AddSubviews(new UIView[] {DurationLabel, DistanceLabel, IntensityLabel});

            Subviews.Where(v => v is UILabel).ForEach(v => {
                UILabel label = (UILabel) v;
                label.BackgroundColor = UIColor.Clear;
                label.TextAlignment = UITextAlignment.Center;
                label.TextColor = CrudFitEvent.WhenDiamond.TextColor;
                label.TranslatesAutoresizingMaskIntoConstraints = false;
            });

            DurationLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
            DistanceLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
            IntensityLabel.Font = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;

            this.AddConstraints(
                DistanceLabel.WithSameCenterX(this),
                DistanceLabel.Bottom().EqualTo(2.0f).TopOf(DurationLabel),

                DurationLabel.WithSameCenterX(this),
                DurationLabel.Bottom().EqualTo(2.0f).CenterYOf(this),

                IntensityLabel.WithSameCenterX(this),
                IntensityLabel.Top().EqualTo(2.0f).BottomOf(DurationLabel)
            );

            var set = this.CreateBindingSet<RunningActivitySummaryView, RunningActivityDisplayViewModel>();
            set.Bind(DistanceLabel).To (vm => vm.Distance);
            set.Bind(DurationLabel).To (vm => vm.Duration);
            set.Bind(IntensityLabel).To (vm => vm.Intensity);
            IntensityLabel.Font = CrudFitEvent.WhenDiamond.DayLabelTextFont;
            set.Apply ();
        }
    }
    
}
