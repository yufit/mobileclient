using System;
using System.Collections.Generic;
using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using CoreGraphics;
using Foundation;
using PHFComposeBarView;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.Models;
using YuFit.Core.ViewModels.Activity.Details;
using YuFit.Core.ViewModels.DisplayActivities;

using YuFit.IOS.Controls;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;
using YuFit.IOS.Views;
using YuFit.IOS.Views.Activity.Comments;
using YuFit.IOS.Views.Activity.Details.ActivitySpecifics;
using YuFit.Core.Messages;
using CoreFoundation;
using MapKit;
using CoreLocation;
using MvvmCross.Plugins.Messenger;
using MvvmCross.Platform;

namespace YuFit.IOS.Views.Activity.Details
{
    public class ViewFitActivityDetailsView : BaseViewController<ViewFitActivityDetailsViewModel>
    {
        private bool _isCanceled;
        public bool IsCanceled {
            get { return _isCanceled; }
            set { _isCanceled = value; ShowCanceledView(value); }
        }

        private bool _isExpired;
        public bool IsExpired {
            get { return _isExpired; }
            set { _isExpired = value; ShowExpiredView(value); }
        }


        readonly TopBoxContainer        _topBox         = new TopBoxContainer();
        readonly SeparatorLine          _separator      = new SeparatorLine();
        readonly BottomBoxContainer     _bottomBox      = new BottomBoxContainer();
        readonly SeparatorLine          _separator2     = new SeparatorLine();
        readonly ActionBar              _actionBar      = new ActionBar();

        readonly CancelledActivityView  _cancelledView      = new CancelledActivityView();
        readonly ExpiredActivityView    _expiredView        = new ExpiredActivityView();

        ComposeBarView composeBarView;

        UITapGestureRecognizer          _addressTap;

        readonly UIBarButtonItem _shareButton            = new UIBarButtonItem(UIBarButtonSystemItem.Action);
        UIBarButtonItem _addCommentButton;
        UIBarButtonItem _editButton;

        readonly ActivityCommentsTableView _commentsTableView    = new ActivityCommentsTableView();

        private ActivityType _activityType;
        public ActivityType ActivityType {
            get { return _activityType; }
            set { _activityType = value; UpdateContent(); }
        }

        private bool _isLoaded;
        public bool IsLoaded {
            get { return _isLoaded; }
            set { _isLoaded = value; LoadUI(); }
        }

        private BaseActivityDisplayViewModelRaw _activityDisplayViewModel;
        public BaseActivityDisplayViewModelRaw ActivityDisplayViewModel {
            get { return _activityDisplayViewModel; }
            set { _activityDisplayViewModel = value; UpdateButton(); }
        }

        private List<NSObject> _keyboardObservers = new List<NSObject>();

        #pragma warning disable 414
        readonly MvxSubscriptionToken _commentAddedMessageToken;
        #pragma warning restore 414

        public ViewFitActivityDetailsView ()
        {
            var messenger = Mvx.Resolve<IMvxMessenger>();
            _commentAddedMessageToken = messenger.Subscribe<EventCommentAddedMessage>(OnCommentAdded);
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            _commentsTableView.BindingContext = BindingContext;
            //AddChildViewController(_commentsTableView);

            _editButton = new UIBarButtonItem(UIBarButtonSystemItem.Edit, null);//(s, e) => ViewModel.EditCommand.Command.Execute(null));
            UITextAttributes attributes = new UITextAttributes();
            attributes.Font = NavigationBar.TitleFont;
            _editButton.SetTitleTextAttributes(attributes, UIControlState.Normal);

            UIImage image; 
            UIGraphics.BeginImageContextWithOptions(new CGSize(22f, 22f), false, 0);
            YuFitStyleKit.DrawMessagesIcon(new CGRect(0, 0, 22F, 22F), Themes.Dashboard.HomeIconColor);
            image = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            _addCommentButton = new UIBarButtonItem(image, UIBarButtonItemStyle.Plain, null);
            _addCommentButton.TintColor = Themes.Dashboard.HomeIconColor;


            var set = this.CreateBindingSet<ViewFitActivityDetailsView, ViewFitActivityDetailsViewModel>();
            set.Bind().For(me => me.IsLoaded).To(vm => vm.IsLoaded);
            set.Apply();

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );

            // Creating ComposeBarView and initializing its properties
            composeBarView = new ComposeBarView (new System.Drawing.RectangleF (0, (float) View.Bounds.Height, (float) View.Bounds.Width, ComposeBarView.InitialHeight)) {
                MaxCharCount = 160,
                MaxLinesCount = 6,
                Placeholder = "Type something...",
                UtilityButtonImage = UIImage.FromBundle ("Camera")
            };

            _addressTap = new UITapGestureRecognizer(() => {
                var location = (Location) ViewModel.Location;
                var latitude = location.GeoCoordinates.Latitude;
                var longitude = location.GeoCoordinates.Longitude;
                var coordinates = new CLLocationCoordinate2D(latitude, longitude);

                var placemark = new MKPlacemark(coordinates, new MKPlacemarkAddress());
                var mapItem = new MKMapItem(placemark);
                mapItem.Name = ViewModel.Name;

                var launchOptions = new MKLaunchOptions();
                mapItem.OpenInMaps(launchOptions);
            });
        }

        public override void ViewWillAppear (bool animated)
        {
            _bottomBox.LocationContainer.AddGestureRecognizer(_addressTap);
            base.ViewWillAppear(animated);
        }

        public override void ViewWillDisappear (bool animated)
        {
            _bottomBox.LocationContainer.RemoveGestureRecognizer(_addressTap);
            base.ViewWillDisappear(animated);
        }

        void LoadUI()
        {
            if (IsLoaded) {
//                var editButton = new UIBarButtonItem(UIBarButtonSystemItem.Edit, (s, e) => ViewModel.EditCommand.Command.Execute(null));
//                UITextAttributes attributes = new UITextAttributes();
//                attributes.Font = NavigationBar.TitleFont; //UIFont.FromName("HelveticaNeueLTStd-LtCn", 19);
//                editButton.SetTitleTextAttributes(attributes, UIControlState.Normal);
//                NavigationItem.RightBarButtonItem = editButton;

                View.AddSubviews(new UIView[] {_topBox, _separator, _bottomBox, _separator2, _actionBar, _commentsTableView.View});

                SetupConstraints();

                _topBox.WhenView.DayLabelFont = CrudFitEvent.WhenDiamond.DayLabelTextFont;
                _topBox.WhenView.MonthAndDateLabelFont = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
                _topBox.WhenView.TimeLabelFont = CrudFitEvent.WhenDiamond.TimeLabelTextFont;

                var set = this.CreateBindingSet<ViewFitActivityDetailsView, ViewFitActivityDetailsViewModel>();
                set.Bind().For(me => me.ActivityType).To(vm => vm.ActivityType);

                set.Bind(_commentsTableView.Source).To(vm => vm.Comments);
                set.Bind(_bottomBox.CreatedByLabel).To(vm => vm.CreatedByLabel);
                set.Bind(_bottomBox.CreatorLabel).To(vm => vm.CreatedBy);
                set.Bind(_bottomBox.AddressLabel).To(vm => vm.Address);
                set.Bind(_bottomBox.SubSportLabel).To(vm => vm.SportSubType);
                set.Bind(_topBox).For(sv => sv.StartTime).To(vm => vm.StartTime);
                set.Bind(_topBox.IconView).For(sv => sv.DrawIcon).To(vm => vm.ActivityType.TypeName);
                set.Bind().For(me => me.ActivityDisplayViewModel).To(vm => vm.ActivityDisplayViewModel);

                set.Bind(composeBarView.PlaceholderLabel).To(vm => vm.AddCommentPlaceholder);
                set.Bind(composeBarView).For("DidPressButton").To(vm => vm.SendCommentCommand.Command);
                set.Bind(_editButton).To(vm => vm.EditCommand.Command);

                set.Bind().For(me => me.IsCanceled).To(vm => vm.IsCanceled);
                set.Bind().For(me => me.IsExpired).To(vm => vm.IsExpired);
                set.Bind(_cancelledView.CancelText).To(vm => vm.CancelString);
                set.Bind(_expiredView.CancelText).To(vm => vm.ExpiredString);

                //set.Bind(_commentsTableView.Source).For(s => s.SelectionChangedCommand).To(vm => vm.ShowCommentsCommand.Command);
                //set.Bind().For(me => me.IsCreator).To(vm => vm.IsCreator);
                set.Apply();
                ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );

                var item0 = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace);
                _shareButton.TintColor = UIColor.White;
                var items = new [] { _addCommentButton, item0, _shareButton };
                _actionBar.SetItems(items, true);
            }
        }

        public override void ViewDidAppear (bool animated)
        {
            _shareButton.Clicked += HandleShareAction;
            _addCommentButton.Clicked += HandleAddComment;
            _keyboardObservers.Add(NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, HandleKeyboardWillShow));
            _keyboardObservers.Add(NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, HandleKeyboardWillHide));
            base.ViewDidAppear(animated);
        }

        public override void ViewDidDisappear (bool animated)
        {
            _shareButton.Clicked -= HandleShareAction;
            _addCommentButton.Clicked -= HandleAddComment;
            _keyboardObservers.ForEach(ko => NSNotificationCenter.DefaultCenter.RemoveObserver(ko));

            var messenger = Mvx.Resolve<IMvxMessenger>();
            messenger.Unsubscribe<EventCommentAddedMessage>(_commentAddedMessageToken);
            base.ViewDidDisappear(animated);
        }

        void SetupConstraints ()
        {
            View.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            _topBox.AddConstraints(_topBox.Height().EqualTo(80.0f));
            _separator.AddConstraints(_separator.Height().EqualTo(0.5f));
            _bottomBox.AddConstraints(_bottomBox.Height().EqualTo(73.5f));
            _separator2.AddConstraints(_separator2.Height().EqualTo(0.5f));
            _actionBar.AddConstraints(_actionBar.Height().EqualTo(40.5f));


            View.AddConstraints(
                _topBox.WithSameWidth(View)    , _topBox.WithSameCenterX(View)   , _topBox.Top().EqualTo().TopOf(View),
                _separator.WithSameWidth(View) , _separator.WithSameCenterX(View), _separator.Top().EqualTo().BottomOf(_topBox),
                _bottomBox.WithSameWidth(View) , _bottomBox.WithSameCenterX(View), _bottomBox.Top().EqualTo().BottomOf(_separator),
                _separator2.WithSameWidth(View), _separator2.WithSameCenterX(View), _separator2.Top().EqualTo().BottomOf(_bottomBox),
                _actionBar.WithSameWidth(View) , _actionBar.WithSameCenterX(View), _actionBar.Top().EqualTo().BottomOf(_separator2),

                _commentsTableView.View.Width().EqualTo().WidthOf(View),
                _commentsTableView.View.Top().EqualTo(15.0f).BottomOf(_actionBar),
                _commentsTableView.View.Bottom().EqualTo().BottomOf(View)
            );


            composeBarView.Hidden = true;
            View.AddSubview (composeBarView);

        }

        void HandleShareAction (object sender, EventArgs e)
        {
            var dummy = new YuFit.IOS.Utilities.ShareActivity(ParentViewController, NavigationController.ViewControllers[0].View);
            dummy.Present();
        }

        void HandleAddComment (object sender, EventArgs e)
        {
//            _topConstraint.Constant -= 100;
//            _bottomConstraint.Constant = -100;
//            View.SetNeedsUpdateConstraints();
//            UIView.AnimateNotify(1.0f, 0.0f, UIViewAnimationOptions.CurveEaseIn, View.LayoutIfNeeded, null);

            composeBarView.BecomeFirstResponder();
//            ViewModel.ShowCommentsCommand.Command.Execute(null);
        }

        void UpdateContent ()
        {
            ActivityColors colors;
            if (_activityType != null && _activityType.Enabled) {
                colors = YuFitStyleKitExtension.ActivityBackgroundColors(_activityType.TypeName);
            } else {
                colors = YuFitStyleKitExtension.ActivityBackgroundColors("Cycling");
            }

            _topBox.BackgroundColor = colors.Base;
            _bottomBox.BackgroundColor = colors.Dark;
            _actionBar.BarTintColor = colors.Base;
            _separator.BackgroundColor = YuFitStyleKit.Charcoal;

            if (ViewModel.IsCreator && !ViewModel.IsCanceled) {
                NavigationItem.RightBarButtonItem = _editButton;
            } else {
                NavigationItem.RightBarButtonItem = null;
            }


        }

        void ShowCanceledView (bool canceled)
        {
            if (canceled) 
            {
                var container = new UIView(View.Frame);
                container.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0.4f);
                container.UserInteractionEnabled = true;
                View.AddSubview(container);

                _cancelledView.TranslatesAutoresizingMaskIntoConstraints = false;
                container.AddSubview(_cancelledView);

                container.AddConstraints(
                    _cancelledView.Height().EqualTo().HeightOf(container).WithMultiplier(.25f),
                    _cancelledView.Width().EqualTo().WidthOf(container),
                    _cancelledView.WithSameCenterX(container),
                    _cancelledView.WithSameCenterY(container).WithMultiplier(.5f));

                ParentViewController.NavigationItem.RightBarButtonItem = null;
            }
        }

        void ShowExpiredView (bool expired)
        {
            if (expired && !ViewModel.IsCanceled) 
            {
                var container = new UIView(View.Frame);
                container.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0.4f);
                container.UserInteractionEnabled = true;
                View.AddSubview(container);

                _expiredView.TranslatesAutoresizingMaskIntoConstraints = false;
                container.AddSubview(_expiredView);

                container.AddConstraints(
                    _expiredView.Height().EqualTo().HeightOf(container).WithMultiplier(.25f),
                    _expiredView.Width().EqualTo().WidthOf(container),
                    _expiredView.WithSameCenterX(container),
                    _expiredView.WithSameCenterY(container).WithMultiplier(.5f));

                ParentViewController.NavigationItem.RightBarButtonItem = null;
            }
        }
        UIView _detailView;
        private void UpdateButton()
        {
            UIView detailView = null;
            if      (_activityDisplayViewModel is CyclingActivityDisplayViewModel)  { detailView = new CyclingActivitySummaryView(_activityDisplayViewModel);  }
            else if (_activityDisplayViewModel is RunningActivityDisplayViewModel)  { detailView = new RunningActivitySummaryView(_activityDisplayViewModel);  }
            else if (_activityDisplayViewModel is SwimmingActivityDisplayViewModel) { detailView = new SwimmingActivitySummaryView(_activityDisplayViewModel); }
            else if (_activityDisplayViewModel is YogaActivityDisplayViewModel)     { detailView = new YogaActivitySummaryView(_activityDisplayViewModel);     }
            else if (_activityDisplayViewModel is FitnessActivityDisplayViewModel)  { detailView = new FitnessActivitySummaryView(_activityDisplayViewModel);     }
            else if (_activityDisplayViewModel is MountainsActivityDisplayViewModel)  { detailView = new MountainsActivitySummaryView(_activityDisplayViewModel);     }

            if (detailView != null) {
                _detailView?.RemoveFromSuperview();
                _topBox.Specifics.AddSubview(detailView);
                detailView.TranslatesAutoresizingMaskIntoConstraints = false;

                _topBox.Specifics.AddConstraints(
                    detailView.WithSameWidth(_topBox.Specifics),
                    detailView.WithSameHeight(_topBox.Specifics),
                    detailView.WithSameCenterX(_topBox.Specifics),
                    detailView.WithSameCenterY(_topBox.Specifics)
                );
                _detailView = detailView;
            }
        }

        void HandleKeyboardWillShow(NSNotification notification)
        {
            // Retrieve keyboard size information.
            NSDictionary info = notification.UserInfo;
            NSObject frameBeginInfo = info[UIKeyboard.FrameBeginUserInfoKey];
            CGSize kbSize = (frameBeginInfo as NSValue).RectangleFValue.Size;

            nfloat keyboardTop = View.Bounds.Height - kbSize.Height;

            CGRect frame = composeBarView.Frame;
            frame.Y = keyboardTop - composeBarView.Frame.Height;
            composeBarView.Frame = frame;
            composeBarView.Hidden = false;
        }

        void HandleKeyboardWillHide(NSNotification notification)
        {
            CGRect frame = composeBarView.Frame;
            frame.Y = View.Frame.Height;
            composeBarView.Frame = frame;
            composeBarView.Hidden = true;
        }

        void OnCommentAdded (EventCommentAddedMessage obj)
        {
            // NOTE: It is necessary to delay the scroll operation or it doesn't scroll
            // properly.
            DispatchQueue.DefaultGlobalQueue.DispatchAfter(
                new DispatchTime(DispatchTime.Now, 50000000), 
                () => BeginInvokeOnMainThread(
                    () => _commentsTableView.TableView.ScrollToRow(
                        NSIndexPath.FromItemSection(ViewModel.Comments.Count - 1, 0), 
                        UITableViewScrollPosition.Bottom, 
                        true
                    )
                )
            );
        }
    }
}
