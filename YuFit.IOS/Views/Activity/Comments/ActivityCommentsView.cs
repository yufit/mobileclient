﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Activity.Comments;
using YuFit.IOS.Extensions;
using PHFComposeBarView;
using UIKit;
using CoreGraphics;
using System;


namespace YuFit.IOS.Views.Activity.Comments
{
    public class ActivityCommentsView : BaseViewController<ActivityCommentsViewModel>
    {
        readonly ActivityCommentsTableView _commentsTableView = new ActivityCommentsTableView();
        ComposeBarView composeBarView;
        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            _commentsTableView.BindingContext = BindingContext;

            AddChildViewController(_commentsTableView);
            View.AddSubviews(new [] {_commentsTableView.View});
            View.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            var set = this.CreateBindingSet<ActivityCommentsView, ActivityCommentsViewModel>();
            set.Bind(_commentsTableView.Source).To(vm => vm.Comments);
            set.Apply();

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );

            View.AddConstraints(
                _commentsTableView.View.Width().EqualTo().WidthOf(View),
                _commentsTableView.View.WithSameCenterX(View),
                _commentsTableView.View.Top().EqualTo().TopOf(View),
                _commentsTableView.View.Bottom().EqualTo().BottomOf(View)
            );

            // Creating ComposeBarView and initializing its properties
            composeBarView = new ComposeBarView (new CGRect (0, View.Bounds.Height - ComposeBarView.InitialHeight, View.Bounds.Width, ComposeBarView.InitialHeight)) {
                MaxCharCount = 160,
                MaxLinesCount = 6,
                Placeholder = "Type something...",
                UtilityButtonImage = UIImage.FromBundle ("Camera")
            };

            // This will handle when the user taps the main button
            composeBarView.DidPressButton += (sender, e) => {
                var composeBar = sender as ComposeBarView;
                Console.WriteLine ("Main button tapped. Text:\n{0}", composeBar.Text);
                InvokeOnMainThread (() => new UIAlertView ("Hello", "Main button tapped. Text:\n" + composeBar.Text, null, "Ok", null).Show ());
                composeBar.Text = string.Empty;
                composeBar.ResignFirstResponder ();
            };

            // This will handle when the user taps the Utility button in this case a CameraButton
            composeBarView.DidPressUtilityButton += (sender, e) => {
                Console.WriteLine ("Utility button tapped");
                InvokeOnMainThread (() => new UIAlertView ("Hello", "Utility button tapped", null, "Ok", null).Show ());
            };

            View.AddSubview (composeBarView);
        }
    }
}

