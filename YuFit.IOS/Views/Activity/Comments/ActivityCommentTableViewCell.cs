﻿
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Foundation;
using UIKit;

using YuFit.Core.Models;
using YuFit.Core.ViewModels.Activity.Comments;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using YuFit.IOS.Extensions;
using YuFit.Core.Interfaces.Services;
using System.Linq;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Platform;

namespace YuFit.IOS.Views.Activity.Comments
{
    public class ActivityCommentTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString ("CommentTableViewCell");

        readonly UILabel        _userNameLabel      = new UILabel();
        readonly UILabel        _postedWhenLabel    = new UILabel();
        readonly UILabel        _commentLabel       = new UILabel();
        readonly IconView       _deleteIcon         = new IconView();

        readonly SeparatorLine  _bottomSeparator    = new SeparatorLine();

        private string _activityTypeName;
        public string ActivityTypeName {
            get { return _activityTypeName; }
            set { _activityTypeName = value; UpdateContent(); }
        }

        private bool _iAmTheOwner;
        public bool IAmTheOwner {
            get { return _iAmTheOwner; }
            set { _iAmTheOwner = value; UpdateDeleteButton(); }
        }

        ~ActivityCommentTableViewCell ()
        {
            System.Console.WriteLine("DELETING CELLS");
        }
        public ActivityCommentTableViewCell ()
        {
            BackgroundColor = UIColor.Clear;

            _userNameLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _userNameLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 16);
            _userNameLabel.Lines = 0;
            _userNameLabel.LineBreakMode = UILineBreakMode.TailTruncation;
            ContentView.AddSubview(_userNameLabel);

            _commentLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _commentLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            _commentLabel.Lines = 0;
            _commentLabel.LineBreakMode = UILineBreakMode.TailTruncation;
            ContentView.AddSubview(_commentLabel);

            _postedWhenLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _postedWhenLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 10);
            _postedWhenLabel.Lines = 0;
            _postedWhenLabel.LineBreakMode = UILineBreakMode.TailTruncation;
            _postedWhenLabel.TextAlignment = UITextAlignment.Right;
            ContentView.AddSubview(_postedWhenLabel);

            _bottomSeparator.TranslatesAutoresizingMaskIntoConstraints = false;
            ContentView.AddSubview(_bottomSeparator);

            _deleteIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawClose2");
            _deleteIcon.TranslatesAutoresizingMaskIntoConstraints = false;
            _deleteIcon.Hidden = true;
            ContentView.AddSubview(_deleteIcon);

            SetupConstraints();

            this.DelayBind(() => {
                var set = this.CreateBindingSet<ActivityCommentTableViewCell, ActivityCommentViewModel>();
                set.Bind().For(me => me.ActivityTypeName).To(vm => vm.ActivityTypeName);
                set.Bind().For(me => me.IAmTheOwner).To(vm => vm.IAmTheOwner);
                set.Bind(_commentLabel).To(vm => vm.Comment);
                set.Bind(_userNameLabel).To(vm => vm.UserName);
                set.Bind(_postedWhenLabel).To(vm => vm.PostedWhen);
                set.Bind(_deleteIcon.Tap()).To(vm => vm.DeleteCommand.Command);
                set.Apply();

//                var ViewModel = DataContext as ActivityCommentViewModel;
//                ViewModel.BindLoadingMessage(ContentView, vm => vm.IsBusy, vm => vm.ProgressMessage);
            });
        }

        void SetupConstraints ()
        {
            _bottomSeparator.AddConstraints(_bottomSeparator.Height().EqualTo(0.5f));

            ContentView.AddConstraints(
                _userNameLabel.Top().EqualTo(5.0f).TopOf(ContentView),
                _userNameLabel.Left().EqualTo(20.0f).LeftOf(ContentView),

                _postedWhenLabel.Top().EqualTo(-1.5f).BottomOf(_userNameLabel),
                _postedWhenLabel.Left().EqualTo(20.0f).LeftOf(ContentView),
//                _postedWhenLabel.WithSameCenterY(_userNameLabel),
//                _postedWhenLabel.Left().EqualTo(10.0f).RightOf(_userNameLabel),

                _deleteIcon.Height().EqualTo().HeightOf(_userNameLabel),
                _deleteIcon.Width().EqualTo().HeightOf(_userNameLabel),
                _deleteIcon.Right().EqualTo(-20.0f).RightOf(ContentView),
                _deleteIcon.CenterY().EqualTo(-2.5f).CenterYOf(_userNameLabel),

                _commentLabel.Top().EqualTo(5.0f).BottomOf(_postedWhenLabel),
                _commentLabel.Left().EqualTo(20.0f).LeftOf(ContentView),
                _commentLabel.Right().EqualTo(-20.0f).RightOf(ContentView),

                _bottomSeparator.Top().EqualTo(5.0f).BottomOf(_commentLabel),
                _bottomSeparator.Left().EqualTo(20.0f).LeftOf(ContentView),
                _bottomSeparator.Right().EqualTo(-20.0f).RightOf(ContentView)
            );

            ContentView.AddConstraints(
                ContentView.Bottom().EqualTo(15.0f).BottomOf(_bottomSeparator)
            );
        }

        void UpdateContent ()
        {
            UIColor color = UIColor.White;

            var categories = Mvx.Resolve<IFitActivityService>().GetEventCategories();
            var category = categories.FirstOrDefault(c => c.Name == _activityTypeName);
            if (category != null) {
                var ccolors = category?.Colors ?? new Colors { 
                    Base = new Color { Red=255,Blue=255,Green=255 },
                    Dark = new Color { Red=255,Blue=255,Green=255 },
                    Light = new Color { Red=255,Blue=255,Green=255 }
                };
                color = new UIColor(ccolors.Light.Red/255.0f, ccolors.Light.Green/255.0f, ccolors.Light.Blue/255.0f, 1.0f);
            } else {
                ActivityColors colors;
                colors = YuFitStyleKitExtension.ActivityBackgroundColors(_activityTypeName);
                color = colors.Light;
            }

            _userNameLabel.TextColor = color;
            _commentLabel.TextColor = color;
            _postedWhenLabel.TextColor = color;
            _bottomSeparator.BackgroundColor = color;
            _deleteIcon.StrokeColor = color;
        }

        void UpdateDeleteButton ()
        {
            _deleteIcon.Hidden = !_iAmTheOwner;
            _deleteIcon.SetNeedsDisplay();
        }
    }
}

