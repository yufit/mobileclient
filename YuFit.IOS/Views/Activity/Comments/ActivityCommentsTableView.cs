﻿using System;

using UIKit;
using YuFit.Core.ViewModels.Activity.Details;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Views.Activity.Comments
{
    public class ActivityCommentsTableView : MvxTableViewController
    {


        public TableViewSource<ActivityCommentTableViewCell> Source { get; set;}

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            TableView.BackgroundColor = UIColor.Clear;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.RowHeight = UITableView.AutomaticDimension;
            TableView.EstimatedRowHeight = 100;
            TableView.AllowsSelection = false;

            Source = new TableViewSource<ActivityCommentTableViewCell>(TableView, ActivityCommentTableViewCell.Key);
            TableView.Source = Source;
        }
    }
}

