using CoreGraphics;
using UIKit;

using YuFit.IOS.Controls;

namespace YuFit.IOS.Views.Activity.Summary
{

    public class DetailsDiamondView : UIView
    {
        public DiamondShapeView DiamondButton { get; private set; }

        public DetailsDiamondView() { DoConstruction(); }
        public DetailsDiamondView(CGRect rect) : base(rect) { DoConstruction(); }

        void DoConstruction ()
        {
            BackgroundColor = UIColor.Clear;
            DiamondButton = new DiamondShapeView();
            Add(DiamondButton);

            UserInteractionEnabled = true;
        }

        public override UIView HitTest (CGPoint point, UIEvent uievent)
        {
            return DiamondButton.HitTest(point, uievent);
        }

        public override void LayoutSubviews ()
        {
            DiamondButton.Frame = Bounds;
            base.LayoutSubviews();
        }
    }
    
}
