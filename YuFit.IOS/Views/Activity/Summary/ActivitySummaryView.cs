using System;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Coc.MvvmCross.Plugins.Location;

using CoreGraphics;
using UIKit;

using YuFit.Core.Models;
using YuFit.Core.ViewModels.Activity.Summary;
using YuFit.Core.ViewModels.Dashboard.ActivitySummary;

using YuFit.IOS.Controls;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Views.Activity.Summary
{
    [Attributes.PresentViewInContainer("SummaryView")]
    public class ActivitySummaryView : BaseViewController<ActivitySummaryViewModel>
    {
        public string Id { get { return ViewModel.Id; } }
        public CocLocation Location { get { return ViewModel.Location; } }
        public MvxImageViewLoader ImageViewLoader { get; private set; }

        private ActivityType _activityType;
        public ActivityType ActivityType {
            get { return _activityType; }
            set { _activityType = value; UpdateContent(); }
        }

        private bool _isCanceled;
        public bool IsCanceled {
            get { return _isCanceled; }
            set { _isCanceled = value; ShowCanceledView(value); }
        }

        private bool _isExpired;
        public bool IsExpired {
            get { return _isExpired; }
            set { _isExpired = value; ShowExpiredView(value); }
        }


//        private bool _isCreator;
//        public bool IsCreator {
//            get { return _isCreator; }
//            set { _isCreator = value; UpdateNavBar(value); }
//        }
//
        private BaseActivityDetailsViewModelRaw _activityViewModel;
        public BaseActivityDetailsViewModelRaw ActivityViewModel
        {
            get { return _activityViewModel; }
            set { _activityViewModel = value; UpdateSpecifics(); }
        }

        private bool _isLoaded;
        public bool IsLoaded {
            get { return _isLoaded; }
            set { _isLoaded = value; LoadUI(); }
        }

        private DarkGradientImageView _backgroundImageView;

        SlidersContainer                _detailsContainer   = new SlidersContainer(); 
        readonly BigDiamondContainer    _summaryView        = new BigDiamondContainer();
        readonly JoinButton             _joinButton         = new JoinButton();
        readonly InscriptionsButton     _invitesButton      = new InscriptionsButton();

        readonly UIBarButtonItem        _shareButton        = new UIBarButtonItem(UIBarButtonSystemItem.Action);
        readonly CancelledActivityView  _cancelledView      = new CancelledActivityView();
        readonly ExpiredActivityView    _expiredView        = new ExpiredActivityView();

        public ActivitySummaryView ()
        {
            IsRadialGradientVisible = false;
        }

        /// <summary>
        /// We swap the view of this view controller to the PointyView. It will
        /// have a bottom diamond at the bottom of the view (kind of like a V).
        /// </summary>
        public override void ViewDidLoad ()
        {
            View = new PointyView(View.Bounds);
            base.ViewDidLoad();

            var set = this.CreateBindingSet<ActivitySummaryView, ActivitySummaryViewModel>();
            set.Bind().For(me => me.IsLoaded).To(vm => vm.IsLoaded);
            set.Apply();

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );
        }

        public override void ViewDidAppear (bool animated)
        {
            _shareButton.Clicked += HandleShareAction;
            base.ViewDidAppear(animated);
        }
        public override void ViewDidDisappear (bool animated)
        {
            _shareButton.Clicked -= HandleShareAction;
            base.ViewDidDisappear(animated);
        }

        void HandleShareAction (object sender, EventArgs e)
        {
            var dummy = new YuFit.IOS.Utilities.ShareActivity(ParentViewController, NavigationController.ViewControllers[0].View);
            dummy.Present();
        }

        void LoadUI()
        {
            if (IsLoaded) {
                _backgroundImageView = new DarkGradientImageView(UIImage.FromFile ("Media/cycling-road.jpg"), View.Bounds);
                if (ViewModel.IsGroupActivity) {
                    ImageViewLoader = new MvxImageViewLoader(() => _backgroundImageView.ImageView);
                }
                Add(_backgroundImageView);

                Add(_summaryView);
                _summaryView.DiamondButton.StrokeWidth = 0.0f;
                _summaryView.DiamondButton.Inset = 0.0f;

                Add(_joinButton);
                _joinButton.DiamondButton.StrokeWidth = 0.0f;
                _joinButton.DiamondButton.Inset = 0.0f;

                Add(_invitesButton);
                _invitesButton.DiamondButton.TransparentBackground = true;
                _invitesButton.DiamondButton.StrokeWidth = 1.0f;
                _invitesButton.DiamondButton.Inset = 0.0f;

                var set = this.CreateBindingSet<ActivitySummaryView, ActivitySummaryViewModel>();
                set.Bind().For(me => me.ActivityType).To(vm => vm.ActivityType);
                set.Bind(_invitesButton).For(w => w.Participants).To(vm => vm.Participants);
                set.Bind(_invitesButton.InvitedLabel).To(vm => vm.InvitedTitle);
                set.Bind(_invitesButton.Tap()).To(vm => vm.ShowInvitedParticipantsCommand.Command);

                set.Bind(_summaryView).For(sv => sv.SubSport).To(vm => vm.SportSubType);
                set.Bind(_summaryView).For(sv => sv.GroupName).To(vm => vm.GroupName).WithConversion("ToUppercase");
                set.Bind(_summaryView).For(sv => sv.StartTime).To(vm => vm.StartTime);
                set.Bind(_summaryView.SportIconView).For(sv => sv.DrawIcon).To(vm => vm.ActivityType.TypeName);
                set.Bind(_summaryView.Tap()).To(vm => vm.ShowActivityDetailCommand.Command);

                set.Bind(_joinButton).For(jb => jb.JoinString).To(vm => vm.IamInString);
                set.Bind(_joinButton.Tap()).To(vm => vm.JoinCommand.Command);
                set.Bind().For(me => me.ActivityViewModel).To(vm => vm.ActivityViewModel);
                set.Bind(ImageViewLoader).To(vm => vm.GroupImage);
//                set.Bind().For(me => me.IsCreator).To(vm => vm.IsCreator);

                set.Bind().For(me => me.IsCanceled).To(vm => vm.IsCanceled);
                set.Bind().For(me => me.IsExpired).To(vm => vm.IsExpired);
                set.Bind(_cancelledView.CancelText).To(vm => vm.CancelString);
                set.Bind(_expiredView.CancelText).To(vm => vm.ExpiredString);
                set.Apply();
            }

        }

        public void Dismiss()
        {
            ViewModel.DismissCommand.Command.Execute(this);
        }

        void UpdateContent ()
        {
            if (ViewModel.GroupImage == null) {
                var sportImage = UIImage.FromFile ("Media/" + _activityType.TypeName.ToLower() + ".jpg");
                if (!string.IsNullOrEmpty(ViewModel.SportSubTypeRaw)) {
                    var subSportImage = UIImage.FromFile ("Media/" + _activityType.TypeName.ToLower() + "-" + ViewModel.SportSubTypeRaw.ToLower() +".jpg");
                    if (subSportImage != null) { sportImage = subSportImage; }
                }

                if (sportImage != null) {
                    _backgroundImageView.ReplaceImage(sportImage);
                }
            }
                
            ActivityColors colors;

            if (_activityType != null && _activityType.Enabled) {
                colors = YuFitStyleKitExtension.ActivityBackgroundColors(_activityType.TypeName);
            } else {
                colors = YuFitStyleKitExtension.ActivityBackgroundColors("Cycling");
            }


            _summaryView.DiamondButton.DiamondColor = colors.Base;
            _summaryView.DiamondButton.SetNeedsDisplay();

            _joinButton.DiamondButton.DiamondColor = colors.Base;
            _joinButton.DiamondButton.SetNeedsDisplay();

            _invitesButton.DiamondButton.StrokeColor = colors.Base;
            _invitesButton.DiamondButton.SetNeedsDisplay();
            Console.WriteLine("Color will be : " + colors.Base);

            ParentViewController.NavigationItem.LeftBarButtonItem = _shareButton;
            _shareButton.TintColor = Themes.Dashboard.HomeIconColor;

            if (!ViewModel.IsCanceled) {
                UIImage image; 
                UIGraphics.BeginImageContextWithOptions(new CGSize(22f, 22f), false, 0);
                YuFitStyleKit.DrawMore(new CGRect(0, 0, 22F, 22F), Themes.Dashboard.HomeIconColor);
                image = UIGraphics.GetImageFromCurrentImageContext();
                UIGraphics.EndImageContext();
                var editButton = new UIBarButtonItem(image, UIBarButtonItemStyle.Plain, (s, e) => ViewModel.ShowActivityDetailCommand.Command.Execute(null));

                //var editButton = new UIBarButtonItem(UIBarButtonSystemItem.Edit, (s, e) => ViewModel.EditCommand.Command.Execute(null));
//                UITextAttributes attributes = new UITextAttributes();
//                attributes.Font = NavigationBar.TitleFont;
//                editButton.SetTitleTextAttributes(attributes, UIControlState.Normal);
                ParentViewController.NavigationItem.RightBarButtonItem = editButton;
            } else {
                ParentViewController.NavigationItem.RightBarButtonItem = null;
            }

            _invitesButton.CountLabelColor = colors.Base;
        }

        void UpdateSpecifics()
        {
            if (_detailsContainer != null) {
                _detailsContainer.RemoveFromSuperview();
            }

            if (_activityViewModel is CyclingActivityDetailsViewModel) {
                _detailsContainer = new CyclingActivitySpecificView(_activityViewModel);
            } else if (_activityViewModel is RunningActivityDetailsViewModel) {
                _detailsContainer = new RunningActivitySpecificView(_activityViewModel);
            } else if (_activityViewModel is SwimmingActivityDetailsViewModel) {
                _detailsContainer = new SwimmingActivitySpecificView(_activityViewModel);
            } else if (_activityViewModel is YogaActivityDetailsViewModel) {
                _detailsContainer = new YogaActivitySpecificView(_activityViewModel);
            } else if (_activityViewModel is FitnessActivityDetailsViewModel) {
                _detailsContainer = new FitnessActivitySpecificView(_activityViewModel);
            } else if (_activityViewModel is MountainsActivityDetailsViewModel) {
                _detailsContainer = new MountainsActivitySpecificView(_activityViewModel);
            }

            Add(_detailsContainer);

            _detailsContainer.Layer.AnchorPoint = new CGPoint(0, 0);
            _detailsContainer.Transform = CGAffineTransform.MakeRotation((float) (-Math.PI/4.0f));
            View.SetNeedsLayout();
        }

        public override void ViewDidLayoutSubviews ()
        {
            if (IsLoaded) {
                _backgroundImageView.Frame = View.Bounds;

                nfloat bigDiamondWidth = View.Bounds.Width * (2.0f/3.0f) - 5;
                _summaryView.Frame = new CGRect {
                    X = View.Bounds.Width/2.0f - bigDiamondWidth/2.0f,
                    Y = View.Bounds.Height - bigDiamondWidth - 5.0f,
                    Width = bigDiamondWidth,
                    Height = bigDiamondWidth
                };

                nfloat smallDiamondWidth = View.Bounds.Width * (1.0f/3.0f) - 5;
                _joinButton.Frame = new CGRect {
                    X = View.Bounds.Width - smallDiamondWidth - 2.5f,
                    Y = View.Bounds.Height - (bigDiamondWidth / 2.0f) - smallDiamondWidth - 7.5f,
                    Width = smallDiamondWidth,
                    Height = smallDiamondWidth
                };

                _invitesButton.Frame = new CGRect {
                    X = View.Bounds.Width / 2.0f + 2.5f,
                    Y = View.Bounds.Height - bigDiamondWidth - 5.0f - (smallDiamondWidth/2.0f) - 2.5f,
                    Width = smallDiamondWidth,
                    Height = smallDiamondWidth
                };

                if (_detailsContainer != null)
                {
                    nfloat diamondHalfHeight = View.Bounds.Width / 2.0f;
                    nfloat diamondStartY = View.Bounds.Height - diamondHalfHeight;
                    nfloat height = (float) Math.Sqrt(Math.Pow(smallDiamondWidth, 2.0f)/2.0f);
                    nfloat width = (float) Math.Sqrt(Math.Pow(bigDiamondWidth/2.0f, 2.0f) + Math.Pow(bigDiamondWidth/2.0f, 2.0f)) + height + 2.5f;
                    _detailsContainer.Bounds = new CGRect{
                        X = 2.5f,
                        Y = diamondStartY - 2.5f, 
                        Width = width,
                        Height = (nfloat) Math.Round(height)
                    };
                    _detailsContainer.Center = new CGPoint(2.5f, diamondStartY - 2.5f);

                    _detailsContainer.SliderBlockWidth = (float) Math.Sqrt(Math.Pow(bigDiamondWidth/2.0f, 2.0f) + Math.Pow(bigDiamondWidth/2.0f, 2.0f));
                    _detailsContainer.SmallBlockWidth = (float) Math.Sqrt(Math.Pow(smallDiamondWidth, 2.0f)/2.0f);
                    _detailsContainer.ContentHeight = (height * 0.6f);

                }

            }
            base.ViewDidLayoutSubviews();
        }

        void ShowCanceledView (bool canceled)
        {
            if (canceled) 
            {
                var container = new UIView(View.Frame);
                container.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0.4f);
                container.UserInteractionEnabled = true;
                View.AddSubview(container);

                _cancelledView.TranslatesAutoresizingMaskIntoConstraints = false;
                container.AddSubview(_cancelledView);

                container.AddConstraints(
                    _cancelledView.Height().EqualTo().HeightOf(container).WithMultiplier(.25f),
                    _cancelledView.Width().EqualTo().WidthOf(container),
                    _cancelledView.WithSameCenterX(container),
                    _cancelledView.WithSameCenterY(container).WithMultiplier(.5f));

                ParentViewController.NavigationItem.RightBarButtonItem = null;
            }
        }

        void ShowExpiredView (bool expired)
        {
            if (expired && !ViewModel.IsCanceled) 
            {
                var container = new UIView(View.Frame);
                container.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0.4f);
                container.UserInteractionEnabled = true;
                View.AddSubview(container);

                _expiredView.TranslatesAutoresizingMaskIntoConstraints = false;
                container.AddSubview(_expiredView);

                container.AddConstraints(
                    _expiredView.Height().EqualTo().HeightOf(container).WithMultiplier(.25f),
                    _expiredView.Width().EqualTo().WidthOf(container),
                    _expiredView.WithSameCenterX(container),
                    _expiredView.WithSameCenterY(container).WithMultiplier(.5f));

                ParentViewController.NavigationItem.RightBarButtonItem = null;
            }
        }
    }
}
