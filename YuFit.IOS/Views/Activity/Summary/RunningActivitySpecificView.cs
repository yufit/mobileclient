using System;

using MvvmCross.Binding.BindingContext;

using Fcaico.Controls.ArrowSlider;

using CoreGraphics;
using UIKit;

using YuFit.Core.Models;
using YuFit.Core.Models.Activities;
using YuFit.Core.ViewModels.Dashboard;
using YuFit.Core.ViewModels.Dashboard.ActivitySummary;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Activity.Summary
{

    public class RunningActivitySpecificView : ActivitySpecificsView<RunningActivityDetailsViewModel> {
        ArrowSliderView         _durationSlider; 
        ArrowSliderView         _distanceSlider;
        readonly UITextField    _speedTextField;
        readonly UITextField    _speedUnit;

        string _speed;
        public string Speed {
            get { return _speed; }
            set { _speed = value; UpdateSpeedControls(); }
        }

        public RunningActivitySpecificView (BaseActivityDetailsViewModelRaw activityViewModel) : base(activityViewModel) 
        {
            colors = YuFitStyleKitExtension.ActivityBackgroundColors("Running");
            _durationSlider = BottomSlider;
            _distanceSlider = TopSlider;
            _speedTextField = TopLabel;
            _speedUnit = BottomLabel;

            _durationSlider.ArrowColor = colors.Base;
            _durationSlider.Values = ViewModel.DurationValues;
            _distanceSlider.ArrowColor = colors.Base;
            _distanceSlider.Values = ViewModel.DistanceValues;

            _speedTextField.BackgroundColor = UIColor.Clear;
            _speedTextField.TextColor = colors.Base;
            _speedTextField.Font = Themes.Dashboard.SummaryView.SpeedLabels.SpeedTextFont;

            _speedUnit.BackgroundColor = UIColor.Clear;
            _speedUnit.TextColor = YuFitStyleKit.White;
            _speedUnit.Font = Themes.Dashboard.SummaryView.SpeedLabels.SpeedUnitTextFont;

            var set = this.CreateBindingSet<RunningActivitySpecificView, RunningActivityDetailsViewModel>();
            set.Bind(_distanceSlider).For(slider => slider.CurrentValue).To(vm => vm.Distance);
            set.Bind(_durationSlider).For(slider => slider.CurrentValue).To(vm => vm.Duration);
            set.Bind().For(me => me.Speed).To(vm => vm.Speed);
            set.Bind(_speedTextField).To(vm => vm.Speed);
            set.Bind(_speedUnit).To(vm => vm.SpeedUnit);
            set.Apply ();
        }

        void UpdateSpeedControls ()
        {
            if (_speed == "00:00") {
                _speedTextField.Hidden = true;
                _speedUnit.Hidden = true;
            } else {
                _speedTextField.Hidden = false;
                _speedUnit.Hidden = false;
            }
        }
    }
    
}
