﻿using System;
using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using Coc.MvvmCross.Plugins.Location;

using CoreGraphics;
using CoreLocation;
using CoreFoundation;

using MapKit;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.Models;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Activity.Summary;
using YuFit.Core.ViewModels.Dashboard.ActivitySummary;

using YuFit.IOS.Utilities;
using YuFit.IOS.Extensions;
using YuFit.IOS.Interfaces;
using YuFit.IOS.Themes;
using YuFit.IOS.Controls;
using YuFit.IOS.Controls.Annotations;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Views.Activity.Summary
{
    public class ActivitySummaryAView : BaseViewController<ActivitySummary2ViewModel>
    {
        public string Id { get { return ViewModel.Id; } }
        public CocLocation Location { get { return ViewModel.Location; } }
        public MvxImageViewLoader ImageViewLoader { get; private set; }

        private ActivityType _activityType;
        public ActivityType ActivityType {
            get { return _activityType; }
            set { _activityType = value; UpdateContent(); }
        }

        private bool _isCanceled;
        public bool IsCanceled {
            get { return _isCanceled; }
            set { _isCanceled = value; ShowCanceledView(value); }
        }

        private bool _isExpired;
        public bool IsExpired {
            get { return _isExpired; }
            set { _isExpired = value; ShowExpiredView(value); }
        }

        //        private bool _isCreator;
        //        public bool IsCreator {
        //            get { return _isCreator; }
        //            set { _isCreator = value; UpdateNavBar(value); }
        //        }
        //
        private BaseActivityDetailsViewModelRaw _activityViewModel;
        public BaseActivityDetailsViewModelRaw ActivityViewModel
        {
            get { return _activityViewModel; }
            set { _activityViewModel = value; UpdateSpecifics(); }
        }

        private bool _isLoaded;
        public bool IsLoaded {
            get { return _isLoaded; }
            set { _isLoaded = value; LoadUI(); }
        }

        private DarkGradientImageView _backgroundImageView;

        SlidersContainer                _detailsContainer   = new SlidersContainer(); 
        readonly BigDiamondContainer    _summaryView        = new BigDiamondContainer();
        readonly JoinButton             _joinButton         = new JoinButton();
        readonly InscriptionsButton     _invitesButton      = new InscriptionsButton();

        readonly UIBarButtonItem        _shareButton        = new UIBarButtonItem(UIBarButtonSystemItem.Action);
        readonly CancelledActivityView  _cancelledView      = new CancelledActivityView();
        readonly ExpiredActivityView    _expiredView        = new ExpiredActivityView();
        UIBarButtonItem                 _showDetailsButton;

        public ActivitySummaryAView ()
        {
            IsRadialGradientVisible = false;
        }

        /// <summary>
        /// We swap the view of this view controller to the PointyView. It will
        /// have a bottom diamond at the bottom of the view (kind of like a V).
        /// </summary>
        public override void ViewDidLoad ()
        {
            View = new PointyView(View.Bounds);
            base.ViewDidLoad();

            UIImage image; 
            UIGraphics.BeginImageContextWithOptions(new CGSize(22f, 22f), false, 0);
            YuFitStyleKit.DrawMore(new CGRect(0, 0, 22F, 22F), Themes.Dashboard.HomeIconColor);
            image = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            _showDetailsButton = new UIBarButtonItem(image, UIBarButtonItemStyle.Plain, null);//(s, e) => ViewModel.ShowActivityDetailCommand.Command.Execute(null));
            ParentViewController.NavigationItem.RightBarButtonItems = new []{ _showDetailsButton, _shareButton };


            var set = this.CreateBindingSet<ActivitySummaryAView, ActivitySummary2ViewModel>();
            set.Bind().For(me => me.IsLoaded).To(vm => vm.IsLoaded);
            set.Apply();

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );
        }

        public override void ViewDidAppear (bool animated)
        {
            _shareButton.Clicked += HandleShareAction;
            base.ViewDidAppear(animated);
        }
        public override void ViewDidDisappear (bool animated)
        {
            _shareButton.Clicked -= HandleShareAction;
            base.ViewDidDisappear(animated);
        }

        void HandleShareAction (object sender, EventArgs e)
        {
            var dummy = new YuFit.IOS.Utilities.ShareActivity(ParentViewController, NavigationController.ViewControllers[0].View);
            dummy.Present();
        }

        void LoadUI()
        {
            if (IsLoaded) {
                _backgroundImageView = new DarkGradientImageView(UIImage.FromFile ("Media/cycling-road.jpg"), View.Bounds);
                if (ViewModel.IsGroupActivity) {
                    ImageViewLoader = new MvxImageViewLoader(() => _backgroundImageView.ImageView);
                }
                Add(_backgroundImageView);

                Add(_summaryView);
                _summaryView.DiamondButton.StrokeWidth = 0.0f;
                _summaryView.DiamondButton.Inset = 0.0f;

                Add(_joinButton);
                _joinButton.DiamondButton.StrokeWidth = 0.0f;
                _joinButton.DiamondButton.Inset = 0.0f;

                Add(_invitesButton);
                _invitesButton.DiamondButton.TransparentBackground = true;
                _invitesButton.DiamondButton.StrokeWidth = 1.0f;
                _invitesButton.DiamondButton.Inset = 0.0f;

                var set = this.CreateBindingSet<ActivitySummaryAView, ActivitySummary2ViewModel>();
                set.Bind().For(me => me.ActivityType).To(vm => vm.ActivityType);
                set.Bind(_invitesButton).For(w => w.Participants).To(vm => vm.Participants);
                set.Bind(_invitesButton.InvitedLabel).To(vm => vm.InvitedTitle);
                set.Bind(_invitesButton.Tap()).To(vm => vm.ShowInvitedParticipantsCommand.Command);

                set.Bind(_summaryView).For(sv => sv.SubSport).To(vm => vm.SportSubType);
                set.Bind(_summaryView).For(sv => sv.GroupName).To(vm => vm.GroupName).WithConversion("ToUppercase");
                set.Bind(_summaryView).For(sv => sv.StartTime).To(vm => vm.StartTime);
                set.Bind(_summaryView.SportIconView).For(sv => sv.DrawIcon).To(vm => vm.ActivityType.TypeName);
                set.Bind(_summaryView.Tap()).To(vm => vm.ShowActivityDetailCommand.Command);

                set.Bind(_joinButton).For(jb => jb.JoinString).To(vm => vm.IamInString);
                set.Bind(_joinButton.Tap()).To(vm => vm.JoinCommand.Command);
                set.Bind().For(me => me.ActivityViewModel).To(vm => vm.ActivityViewModel);
                set.Bind(ImageViewLoader).To(vm => vm.GroupImage);
                //                set.Bind().For(me => me.IsCreator).To(vm => vm.IsCreator);

                set.Bind(_showDetailsButton).To(vm => vm.ShowActivityDetailCommand.Command);
                set.Bind().For(me => me.IsCanceled).To(vm => vm.IsCanceled);
                set.Bind().For(me => me.IsExpired).To(vm => vm.IsExpired);
                set.Bind(_cancelledView.CancelText).To(vm => vm.CancelString);
                set.Bind(_expiredView.CancelText).To(vm => vm.ExpiredString);
                set.Apply();
            }

        }

        void UpdateContent ()
        {
            if (ViewModel.GroupImage == null) {
                var sportImage = UIImage.FromFile ("Media/" + _activityType.TypeName.ToLower() + ".jpg");
                if (!string.IsNullOrEmpty(ViewModel.SportSubTypeRaw)) {
                    var subSportImage = UIImage.FromFile ("Media/" + _activityType.TypeName.ToLower() + "-" + ViewModel.SportSubTypeRaw.ToLower() +".jpg");
                    if (subSportImage != null) { sportImage = subSportImage; }
                }

                if (sportImage != null) {
                    _backgroundImageView.ReplaceImage(sportImage);
                }
            }

            ActivityColors colors;

            if (_activityType != null && _activityType.Enabled) {
                colors = YuFitStyleKitExtension.ActivityBackgroundColors(_activityType.TypeName);
            } else {
                colors = YuFitStyleKitExtension.ActivityBackgroundColors("Cycling");
            }


            _summaryView.DiamondButton.DiamondColor = colors.Base;
            _summaryView.DiamondButton.SetNeedsDisplay();

            _joinButton.DiamondButton.DiamondColor = colors.Base;
            _joinButton.DiamondButton.SetNeedsDisplay();

            _invitesButton.DiamondButton.StrokeColor = colors.Base;
            _invitesButton.DiamondButton.SetNeedsDisplay();
            Console.WriteLine("Color will be : " + colors.Base);

            _invitesButton.CountLabelColor = colors.Base;
        }

        void UpdateSpecifics()
        {
            if (_detailsContainer != null) {
                _detailsContainer.RemoveFromSuperview();
            }

            if (_activityViewModel is CyclingActivityDetailsViewModel) {
                _detailsContainer = new CyclingActivitySpecificView(_activityViewModel);
            } else if (_activityViewModel is RunningActivityDetailsViewModel) {
                _detailsContainer = new RunningActivitySpecificView(_activityViewModel);
            } else if (_activityViewModel is SwimmingActivityDetailsViewModel) {
                _detailsContainer = new SwimmingActivitySpecificView(_activityViewModel);
            } else if (_activityViewModel is YogaActivityDetailsViewModel) {
                _detailsContainer = new YogaActivitySpecificView(_activityViewModel);
            } else if (_activityViewModel is FitnessActivityDetailsViewModel) {
                _detailsContainer = new FitnessActivitySpecificView(_activityViewModel);
            } else if (_activityViewModel is MountainsActivityDetailsViewModel) {
                _detailsContainer = new MountainsActivitySpecificView(_activityViewModel);
            }

            Add(_detailsContainer);

            _detailsContainer.Layer.AnchorPoint = new CGPoint(0, 0);
            _detailsContainer.Transform = CGAffineTransform.MakeRotation((float) (-Math.PI/4.0f));
            View.SetNeedsLayout();
        }

        public override void ViewDidLayoutSubviews ()
        {
            if (IsLoaded) {
                _backgroundImageView.Frame = View.Bounds;

                nfloat bigDiamondWidth = View.Bounds.Width * (2.0f/3.0f) - 5;
                _summaryView.Frame = new CGRect {
                    X = View.Bounds.Width/2.0f - bigDiamondWidth/2.0f,
                    Y = View.Bounds.Height - bigDiamondWidth - 5.0f,
                    Width = bigDiamondWidth,
                    Height = bigDiamondWidth
                };

                nfloat smallDiamondWidth = View.Bounds.Width * (1.0f/3.0f) - 5;
                _joinButton.Frame = new CGRect {
                    X = View.Bounds.Width - smallDiamondWidth - 2.5f,
                    Y = View.Bounds.Height - (bigDiamondWidth / 2.0f) - smallDiamondWidth - 7.5f,
                    Width = smallDiamondWidth,
                    Height = smallDiamondWidth
                };

                _invitesButton.Frame = new CGRect {
                    X = View.Bounds.Width / 2.0f + 2.5f,
                    Y = View.Bounds.Height - bigDiamondWidth - 5.0f - (smallDiamondWidth/2.0f) - 2.5f,
                    Width = smallDiamondWidth,
                    Height = smallDiamondWidth
                };

                if (_detailsContainer != null)
                {
                    nfloat diamondHalfHeight = View.Bounds.Width / 2.0f;
                    nfloat diamondStartY = View.Bounds.Height - diamondHalfHeight;
                    nfloat height = (float) Math.Sqrt(Math.Pow(smallDiamondWidth, 2.0f)/2.0f);
                    nfloat width = (float) Math.Sqrt(Math.Pow(bigDiamondWidth/2.0f, 2.0f) + Math.Pow(bigDiamondWidth/2.0f, 2.0f)) + height + 2.5f;
                    _detailsContainer.Bounds = new CGRect{
                        X = 2.5f,
                        Y = diamondStartY - 2.5f, 
                        Width = width,
                        Height = (nfloat) Math.Round(height)
                    };
                    _detailsContainer.Center = new CGPoint(2.5f, diamondStartY - 2.5f);

                    _detailsContainer.SliderBlockWidth = (float) Math.Sqrt(Math.Pow(bigDiamondWidth/2.0f, 2.0f) + Math.Pow(bigDiamondWidth/2.0f, 2.0f));
                    _detailsContainer.SmallBlockWidth = (float) Math.Sqrt(Math.Pow(smallDiamondWidth, 2.0f)/2.0f);
                    _detailsContainer.ContentHeight = (height * 0.6f);

                }

            }
            base.ViewDidLayoutSubviews();
        }

        void ShowCanceledView (bool canceled)
        {
            if (canceled) 
            {
                var container = new UIView(View.Frame);
                container.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0.4f);
                container.UserInteractionEnabled = true;
                View.AddSubview(container);

                _cancelledView.TranslatesAutoresizingMaskIntoConstraints = false;
                container.AddSubview(_cancelledView);

                container.AddConstraints(
                    _cancelledView.Height().EqualTo().HeightOf(container).WithMultiplier(.25f),
                    _cancelledView.Width().EqualTo().WidthOf(container),
                    _cancelledView.WithSameCenterX(container),
                    _cancelledView.WithSameCenterY(container).WithMultiplier(.5f));

                ParentViewController.NavigationItem.RightBarButtonItem = null;
            }
        }

        void ShowExpiredView (bool expired)
        {
            if (expired && !ViewModel.IsCanceled) 
            {
                var container = new UIView(View.Frame);
                container.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 0.4f);
                container.UserInteractionEnabled = true;
                View.AddSubview(container);

                _expiredView.TranslatesAutoresizingMaskIntoConstraints = false;
                container.AddSubview(_expiredView);

                container.AddConstraints(
                    _expiredView.Height().EqualTo().HeightOf(container).WithMultiplier(.25f),
                    _expiredView.Width().EqualTo().WidthOf(container),
                    _expiredView.WithSameCenterX(container),
                    _expiredView.WithSameCenterY(container).WithMultiplier(.5f));

                ParentViewController.NavigationItem.RightBarButtonItem = null;
            }
        }
    }


//    public class FitActivityAnnotation : MKAnnotation, IFitActivityAnnotation
//    {
//        private CLLocationCoordinate2D _coordinate;
//        public override CLLocationCoordinate2D Coordinate { get { return _coordinate; } }
//        public UIColor Color { get; set; }
//        public new string Title { get; set; }
//        public new string Subtitle { get; set; }
//        public UIImage Image { get; set; }
//        public string AvatarUrl { get; set; }
//
//        public FitActivityAnnotation(CLLocationCoordinate2D newCoordinate, UIColor color) {
//            _coordinate = newCoordinate;
//            Color = color;
//        }
//    }
//
    public class ActivitySummary2View : BaseViewController<ActivitySummary2ViewModel>
    {
        readonly ActivitySummaryAView _summaryView = new ActivitySummaryAView();
        UIView _containerView = new UIView();
        NSLayoutConstraint _activityDetailContainerViewConstraint;

        readonly MKMapView _mapView = new MKMapView();
        readonly AnnotationViewHelper<MapAnnotation> _annotationViewHelper = new AnnotationViewHelper<MapAnnotation>();
        MapAnnotationManager _annotationManager;

        private CocLocation _location;
        public CocLocation Location {
            get { return _location; }
            set { _location = value; RefreshSelectedLocation(); }
        }

        private bool _isReady;
        public bool IsReady {
            get { return _isReady; }
            set { _isReady = value; LoadUI();}
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            _summaryView.DataContext = DataContext;
            AddChildViewController(_summaryView);

            SetupViewHierarchy();
            SetupConstraints();
            SetupBinding();
        }

        void SetupViewHierarchy ()
        {
            View.AddSubviews(new UIView[] {_mapView, _containerView});
            View.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);

            _mapView.PitchEnabled = false;
            _mapView.ShowsUserLocation = true;
            _mapView.GetViewForAnnotation = _annotationViewHelper.GetViewForAnnotation;

            _annotationManager = new MapAnnotationManager(_mapView);
        }

        void SetupConstraints ()
        {
            View.AddConstraints(
                _mapView.WithSameWidth(View), _mapView.WithSameHeight(View), _mapView.WithSameCenterX(View), _mapView.WithSameCenterY(View),
                _containerView.WithSameWidth(View), _containerView.WithSameHeight(View), _containerView.WithSameCenterX(View)
            );
            _activityDetailContainerViewConstraint = _containerView.Bottom().EqualTo().TopOf(View).ToLayoutConstraints().FirstOrDefault();
            View.AddConstraint(_activityDetailContainerViewConstraint);

            _containerView.BackgroundColor = UIColor.Clear;
        }

        void SetupBinding ()
        {
            var set = this.CreateBindingSet<ActivitySummary2View, ActivitySummary2ViewModel>();
            set.Bind().For(me => me.IsReady).To(vm => vm.IsLoaded);
            set.Apply();
            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );
        }

        void LoadUI()
        {
            if (!IsReady) { return; }

            var set = this.CreateBindingSet<ActivitySummary2View, ActivitySummary2ViewModel>();
//            set.Bind(_mapView).For("Region").To(vm => vm.CurrentRegion).WithConversion("RegionToMKCoordinate");
            set.Bind().For(me => me.Location).To(vm => vm.Location);
            set.Bind(_annotationManager).For(my => my.ItemsSource).To(vm => vm.Annotations);
            set.Apply();

            CLLocationCoordinate2D current = new CLLocationCoordinate2D {
                Latitude = ViewModel.CurrentRegion.Center.Latitude,
                Longitude = ViewModel.CurrentRegion.Center.Longitude
            };

            MKCoordinateSpan span = new MKCoordinateSpan(Mapping.MilesToLatitudeDegrees(ViewModel.CurrentRegion.Radius), Mapping.MilesToLongitudeDegrees(ViewModel.CurrentRegion.Radius, current.Latitude));

            _mapView.SetRegion(new MKCoordinateRegion(current, span), false);
            _mapView.DidFinishRenderingMap += _mapView_DidFinishRenderingMap;
        }

        void _mapView_DidFinishRenderingMap (object sender, MKDidFinishRenderingMapEventArgs e)
        {
            DispatchQueue.DefaultGlobalQueue.DispatchAfter(new DispatchTime(DispatchTime.Now, 50000000), () => BeginInvokeOnMainThread(() => {

                CLLocationCoordinate2D center2 = _mapView.CenterCoordinate;
                center2.Latitude += _mapView.Region.Span.LatitudeDelta * -0.45;
                CGPoint point = _mapView.ConvertCoordinate(center2, _mapView);

                CLLocationCoordinate2D center = new CLLocationCoordinate2D{
                    Latitude = ViewModel.Location.Coordinates.Latitude,
                    Longitude = ViewModel.Location.Coordinates.Longitude
                };
                center.Latitude -= _mapView.Region.Span.LatitudeDelta * -0.45;
                _mapView.SetCenterCoordinate(center, false);

                nfloat height = point.Y;
                _containerView.AddSubview(_summaryView.View);
                _summaryView.View.Frame = new CGRect {
                    Width = _mapView.Bounds.Width, Height = height, 
                    X = 0, Y = 0
                };

                View.RemoveConstraint(_activityDetailContainerViewConstraint); 
                _activityDetailContainerViewConstraint = _summaryView.View.Top().EqualTo().TopOf(View).ToLayoutConstraints().FirstOrDefault();
                View.AddConstraint(_activityDetailContainerViewConstraint);

                UIView.AnimateNotify(0.15f, 0.0f, UIViewAnimationOptions.CurveEaseInOut, () => {
                    View.LayoutIfNeeded();
                    View.SetNeedsUpdateConstraints();
                }, finished => {
                    _mapView.DidFinishRenderingMap -= _mapView_DidFinishRenderingMap;
                });
            }));
        }

        void RefreshSelectedLocation ()
        {
            if (Location != null) {
//                var colors = YuFitStyleKitExtension.ActivityBackgroundColors(ViewModel.ActivityType.TypeName);
//                _curPin = new FitActivityAnnotation (Location.Coordinates.ToCLLocationCoordinate2D(), colors.Base );
//                _mapView.AddAnnotation(_curPin);
            }
        }
    }
}

