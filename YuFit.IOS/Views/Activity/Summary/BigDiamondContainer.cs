﻿using System;

using CoreGraphics;
using UIKit;

using YuFit.IOS.Themes;
using YuFit.IOS.Controls;


namespace YuFit.IOS.Views.Activity.Summary
{
    public class BigDiamondContainer : DetailsDiamondView
    {
        public readonly ActivityIconView SportIconView  = new ActivityIconView();
        readonly UILabel                 WhatLabel      = new UILabel();
        readonly UILabel                 GroupNameLabel = new UILabel();
        readonly WhenView                WhenContainer  = new WhenView();

        DateTime _startTime;
        public DateTime StartTime {
            get { return _startTime; }
            set { _startTime = value; WhenContainer.Date = _startTime; }
        }

        public string SubSport {
            get { return WhatLabel.Text; }
            set { WhatLabel.Text = value; }
        }

        public string GroupName {
            get { return GroupNameLabel.Text; }
            set { GroupNameLabel.Text = value; }
        }

        public BigDiamondContainer () { DoConstruction(); }
        public BigDiamondContainer (CGRect frame) : base(frame) { DoConstruction(); }

        void DoConstruction()
        {
            Add(SportIconView);

            WhatLabel.TextColor = YuFitStyleKit.White;
            WhatLabel.TextAlignment = UITextAlignment.Center;
            WhatLabel.Font = CrudFitEvent.WhenDiamond.TimeLabelTextFont;
            WhatLabel.Text = "FOO";
            Add(WhatLabel);

            GroupNameLabel.TextColor = YuFitStyleKit.White;
            GroupNameLabel.TextAlignment = UITextAlignment.Center;
            GroupNameLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 16);
            Add(GroupNameLabel);

            WhenContainer.DayLabelFont = CrudFitEvent.WhenDiamond.DayLabelTextFont;
            WhenContainer.MonthAndDateLabelFont = CrudFitEvent.WhenDiamond.MonthAndDayLabelTextFont;
            WhenContainer.TimeLabelFont = CrudFitEvent.WhenDiamond.TimeLabelTextFont;
            Add(WhenContainer);
        }

        public override void LayoutSubviews ()
        {
            CGRect whenRect = new CGRect();
            whenRect.Width = Bounds.Width;
            whenRect.Height = Bounds.Height/3.0f;
            whenRect.X = 0;
            whenRect.Y = Bounds.Height - (Bounds.Height / 4.0f) - whenRect.Height / 2.0f - 5.0f;
            WhenContainer.Frame = whenRect;
                       
            CGRect iconRect = new CGRect();
            iconRect.Height = Bounds.Height/4.0f;
            iconRect.Width = iconRect.Height;
            iconRect.X = Bounds.Width/2.0f - iconRect.Width/2.0f;
            iconRect.Y = 30.0f;
            SportIconView.Frame = iconRect;

            CGRect whatRect = new CGRect();
            whatRect.Height = Bounds.Height / 10.0f;
            whatRect.Width = Bounds.Width;
            whatRect.X = 0;
            whatRect.Y = iconRect.Bottom - 3.0f;
            WhatLabel.Frame = whatRect;

            CGRect whomRect = new CGRect();
            whomRect.Height = Bounds.Height / 10.0f;
            whomRect.Width = Bounds.Width;
            whomRect.X = 0;
            whomRect.Y = whatRect.Bottom;
            GroupNameLabel.Frame = whomRect;


            base.LayoutSubviews();
        }
    }

}
