using System;

using MvvmCross.Binding.BindingContext;

using Fcaico.Controls.ArrowSlider;

using CoreGraphics;
using UIKit;

using YuFit.Core.Models;
using YuFit.Core.Models.Activities;
using YuFit.Core.ViewModels.Dashboard;
using YuFit.Core.ViewModels.Dashboard.ActivitySummary;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Activity.Summary
{
    public class SlidersContainer : UIView
    {
        protected UIView            SliderBlock     = new UIView();
        protected UIView            SmallBlock      = new UIView();

        protected ArrowSliderView   TopSlider       = new ArrowSliderView(); 
        protected ArrowSliderView   BottomSlider    = new ArrowSliderView();

        protected UITextField       TopLabel        = new UITextField();
        protected UITextField       BottomLabel     = new UITextField();

        public nfloat SliderBlockWidth  { get; set; }
        public nfloat SmallBlockWidth   { get; set; }
        public nfloat ContentHeight     { get; set; }
        public nfloat ContentSpacer     { get { return Bounds.Height - ContentHeight; } }

        public SlidersContainer ()
        {
            DoConstruction();
        }

        void DoConstruction ()
        {
            BackgroundColor = UIColor.Clear;
            SliderBlock.BackgroundColor = UIColor.Clear;
            SmallBlock.BackgroundColor = UIColor.Clear;

            AddSubview(SliderBlock);
            SliderBlock.AddSubview(TopSlider);
            SliderBlock.AddSubview(BottomSlider);

            AddSubview(SmallBlock);
            SmallBlock.AddSubview(TopLabel);
            SmallBlock.AddSubview(BottomLabel);

            TopLabel.VerticalAlignment = UIControlContentVerticalAlignment.Bottom;
            TopLabel.UserInteractionEnabled = false;
            TopLabel.AllowsEditingTextAttributes = false;

            BottomLabel.VerticalAlignment = UIControlContentVerticalAlignment.Top;
            BottomLabel.UserInteractionEnabled = false;
            BottomLabel.AllowsEditingTextAttributes = false;

            TopSlider.IsEnabled = false;
            TopSlider.BackgroundColor = UIColor.Clear;
            TopSlider.Font = Themes.Dashboard.SummaryView.SliderTextFont;
            TopSlider.FontBaselineOffset = Themes.Dashboard.SummaryView.SliderTextOffset;

            BottomSlider.IsEnabled = false;
            BottomSlider.BackgroundColor = UIColor.Clear;
            BottomSlider.Font = Themes.Dashboard.SummaryView.SliderTextFont;
            BottomSlider.FontBaselineOffset = Themes.Dashboard.SummaryView.SliderTextOffset;
        }

        public override void LayoutSubviews ()
        {
            CGRect rect = new CGRect(0, ContentSpacer, SliderBlockWidth, ContentHeight);
            SliderBlock.Bounds = rect;
            SliderBlock.Layer.AnchorPoint = new CGPoint(0, 0);
            SliderBlock.Center = new CGPoint(Center.X, Center.Y + ContentSpacer);

            rect = new CGRect(SliderBlockWidth + 2.5f, 0, SmallBlockWidth, ContentHeight);
            SmallBlock.Bounds = rect;
            SmallBlock.Layer.AnchorPoint = new CGPoint(1.0f, 0);
            SmallBlock.Center = new CGPoint(Bounds.X + Bounds.Width + 1.0f, Bounds.Y + ContentSpacer);

            rect = new CGRect(2, -1.5f, SliderBlockWidth + 3.0f, ContentHeight/2.0f);
            BottomSlider.Bounds = rect;
            BottomSlider.Layer.AnchorPoint = new CGPoint(0, 0);
            BottomSlider.Center = new CGPoint(0, rect.Height + ContentSpacer);

            rect = new CGRect(2, -1.5f, SliderBlockWidth + 3.0f, ContentHeight/2.0f);
            TopSlider.Bounds = rect;
            TopSlider.Layer.AnchorPoint = new CGPoint(0, 0);
            TopSlider.Center = new CGPoint(0, ContentSpacer);

            rect = new CGRect(SliderBlockWidth + 2.5f + 3.5f, 3.5f, SmallBlockWidth, ContentHeight * 0.65f);
            TopLabel.Bounds = rect;
            TopLabel.Layer.AnchorPoint = new CGPoint(0, 0);
            TopLabel.Center = new CGPoint(SliderBlockWidth + 2.5f + 3.5f, 3.5f);

            rect = new CGRect(SliderBlockWidth + 2.5f + 3.5f, ContentHeight * 0.65f, SmallBlockWidth, ContentHeight * 0.35f);
            BottomLabel.Bounds = rect;
            BottomLabel.Layer.AnchorPoint = new CGPoint(0, 0);
            BottomLabel.Center = new CGPoint(SliderBlockWidth + 2.5f + 3.5f, ContentHeight * 0.65f);

            base.LayoutSubviews();
        }
    }
 
    public class ActivitySpecificsView<T> : SlidersContainer, IMvxBindingContextOwner {
        protected ActivityColors colors { get; set; }
        protected T ViewModel { get { return (T) DataContext; }}

        #region IMvxBindingContextOwner implementation

        public IMvxBindingContext BindingContext {
            get;
            set;
        }

        /// <summary>
        /// The data context for the current cell (the view model)
        /// </summary>
        public virtual object DataContext
        {
            get { return BindingContext.DataContext; }
            set { BindingContext.DataContext = value; }
        }

        #endregion

        public ActivitySpecificsView (BaseActivityDetailsViewModelRaw activityViewModel) { 
            this.CreateBindingContext (); 
            DataContext = activityViewModel;

        }
    }


}
