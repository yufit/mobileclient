using System;

using CoreAnimation;
using UIKit;

using YuFit.Core.Models;
using YuFit.Core.ViewModels.Dashboard;

using System.Collections.Generic;
using System.Linq;
using YuFit.Core.Extensions;
using CoreGraphics;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Activity.Summary
{
    public class InscriptionsButton : DetailsDiamondView
    {
        private List<FitActivityParticipant> _participants;
        public List<FitActivityParticipant> Participants {
            get { return _participants; }
            set { _participants = value; UpdateContent(); }
        }

        public UIColor CountLabelColor {
            get { return CountLabel.TextColor; }
            set { CountLabel.TextColor = value; }
        }

        private readonly UIView  LabelContainer = new UIView();
        private readonly UILabel CountLabel     = new UILabel();
        public UILabel InvitedLabel { get; set; }

        public InscriptionsButton () { DoConstruction(); }
        public InscriptionsButton (CGRect frame) : base(frame) { DoConstruction(); }

        void DoConstruction()
        {
            InvitedLabel = new UILabel();
            //LabelContainer.AddSubviews(new [] {CountLabel, InvitedLabel});
            LabelContainer.AddSubviews(new [] {CountLabel});
            AddSubview(LabelContainer);

            LabelContainer.Subviews.Where(v => v is UILabel).ForEach(v => {
                UILabel label = (UILabel) v;
                label.BackgroundColor = UIColor.Clear;
                label.TextAlignment = UITextAlignment.Center;
                label.TextColor = CrudFitEvent.WhenDiamond.TextColor;
            });

            CountLabel.Font = Themes.Dashboard.SummaryView.InvitesButton.CountLabelTextFont;
//            InvitedLabel.Font = Themes.Dashboard.SummaryView.InvitesButton.OtherLabelTextFont;
        }

        public override void LayoutSubviews ()
        {
            LabelContainer.Frame = Bounds.Inset(0, 3);

//            nfloat desiredHeight = (LabelContainer.Frame.Height)/2.0f;
//            nfloat startY = 0.0f;

            CGRect helperRect = LabelContainer.Frame;
//            helperRect.Height = desiredHeight + 3.0f;
//            helperRect.Y = startY + 4.0f + 0.0f;
            CountLabel.Frame = helperRect;

//            helperRect.Y = startY + desiredHeight - 9.0f;
//            InvitedLabel.Frame = helperRect;

            base.LayoutSubviews();
        }

        void UpdateContent ()
        {
            CountLabel.Text = _participants.Count(p => p.IsGoing).ToString();
            LabelContainer.Hidden = false;
        }
    }
    
}
