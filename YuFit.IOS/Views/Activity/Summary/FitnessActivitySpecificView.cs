﻿using MvvmCross.Binding.BindingContext;

using Fcaico.Controls.ArrowSlider;

using UIKit;

using YuFit.Core.ViewModels.Dashboard.ActivitySummary;
using YuFit.IOS.Themes;
using YuFit.Core.Models.Activities;

namespace YuFit.IOS.Views.Activity.Summary
{

    public class FitnessActivitySpecificView : ActivitySpecificsView<FitnessActivityDetailsViewModel> {
        readonly ArrowSliderView _durationSlider;

        public FitnessActivitySpecificView (BaseActivityDetailsViewModelRaw activityViewModel) : base(activityViewModel) 
        {
            colors = YuFitStyleKitExtension.ActivityBackgroundColors("Fitness");
            _durationSlider = BottomSlider;
            _durationSlider.BackgroundColor = UIColor.Clear;
            _durationSlider.ArrowColor = colors.Base;
            TopSlider.Hidden = true;

            _durationSlider.Values = ViewModel.DurationValues;

            var set = this.CreateBindingSet<FitnessActivitySpecificView, FitnessActivityDetailsViewModel>();
            set.Bind(_durationSlider).For(slider => slider.CurrentValue).To(vm => vm.Duration);
            set.Apply ();
        }
    }

}
