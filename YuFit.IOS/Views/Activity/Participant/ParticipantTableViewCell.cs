﻿using MvvmCross.Binding.BindingContext;

using Foundation;

using YuFit.Core.ViewModels.Activity.Participant;
using YuFit.IOS.Controls.UserTableView;
using YuFit.IOS.Themes;

namespace YuFit.IOS.Views.Activity.Participant
{
    public class ParticipantTableViewCell : UserInfoTableViewCell<ParticipantViewModel>
    {
        public static readonly NSString Key = new NSString ("ParticipantTableViewCell");

        private bool _isGoing;
        public bool IsGoing {
            get { return _isGoing; }
            set { _isGoing = value; if (!_isGoing) {ContentColor = YuFitStyleKit.Gray; } }
        }

        public ParticipantTableViewCell ()
        {
            this.DelayBind(() => {
                var set = this.CreateBindingSet<ParticipantTableViewCell, ParticipantViewModel>();
                set.Bind().For(me => me.IsGoing).To(vm => vm.IsGoing);
                set.Apply ();
            });
        }
    }
}

