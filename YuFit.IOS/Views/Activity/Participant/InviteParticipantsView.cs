﻿using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Activity.Participant;

using YuFit.IOS.Controls;
using YuFit.IOS.Extensions;
using YuFit.IOS.Views.CreateActivity.Transitions;

namespace YuFit.IOS.Views.Activity.Participant
{
    public class InviteParticipantsView : BaseViewController<InviteParticipantsViewModel>
    {
        #region Select participant table section begin
        readonly UIButton        _inviteWithFb      = new UIButton();
        readonly UILabel         _tableLabel        = new UILabel();
        readonly SeparatorLine   _tableTopLine      = new SeparatorLine();
        readonly UIView          _tableContainer    = new UIView();
        readonly SeparatorLine   _tableBottomLine   = new SeparatorLine();
        readonly InviteParticipantsTableView _tableView = new InviteParticipantsTableView();
        #endregion

        readonly UIBarButtonItem _addButton = new UIBarButtonItem(UIBarButtonSystemItem.Add);

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            _tableView.BindingContext = BindingContext;
            AddChildViewController(_tableView);

            NavigationItem.RightBarButtonItem = _addButton;
            _addButton.TintColor = Themes.Dashboard.HomeIconColor;

            AddFirstLevelViews();
            AddTableView();
            SetupBindings();
        }

        void AddFirstLevelViews ()
        {
            UIView[] firstLevelViews = { _inviteWithFb, _tableTopLine, _tableContainer, _tableBottomLine };

            View.AddSubviews(firstLevelViews);

            firstLevelViews.ForEach(v => {
                v.TranslatesAutoresizingMaskIntoConstraints = false;
                View.AddConstraints(
                    v.WithSameCenterX(View),
                    v.Width().EqualTo().WidthOf(View).WithMultiplier(0.9f)
                );
            });

            firstLevelViews.Where(v => v is SeparatorLine).ForEach(v => {
                v.AddConstraints(v.Height().EqualTo(1.5f));
                v.BackgroundColor = YuFit.IOS.Themes.YuFitStyleKit.Yellow; //ColorPalette.Base;
            });

            firstLevelViews.Where(v => v is UILabel).ForEach(v => {
                (v as UILabel).Font = Themes.CrudFitEvent.LabelTextFont;
                (v as UILabel).TextColor = Themes.CrudFitEvent.LabelTextColor;
            });

            _inviteWithFb.SetTitle("+ INVITE MORE FRIENDS", UIControlState.Normal);
            _inviteWithFb.LineBreakMode = UILineBreakMode.WordWrap;
            _inviteWithFb.Font = Themes.CrudFitEvent.LabelTextFont;
            _inviteWithFb.TitleLabel.TextColor = Themes.CrudFitEvent.LabelTextColor;
            _inviteWithFb.TitleLabel.TextAlignment = UITextAlignment.Center;

            View.AddConstraints(
                _inviteWithFb.Top().EqualTo(22.0f).TopOf(View),
                _tableTopLine.Top().EqualTo(22.0f).BottomOf(_inviteWithFb),
                _tableContainer.Top().EqualTo(5.0f).BottomOf(_tableTopLine),
                _tableContainer.Bottom().EqualTo(-5.0f).TopOf(_tableBottomLine),
                _tableBottomLine.Bottom().EqualTo(-22.0f).BottomOf(View)
            );
        }

        private void AddTableView()
        {
            _tableContainer.AddSubview(_tableView.View);

            _tableView.View.TranslatesAutoresizingMaskIntoConstraints = false;
            _tableContainer.AddConstraints(
                _tableView.View.Top().EqualTo().TopOf(_tableContainer),
                _tableView.View.Left().EqualTo().LeftOf(_tableContainer),
                _tableView.View.Bottom().EqualTo().BottomOf(_tableContainer),
                _tableView.View.Right().EqualTo().RightOf(_tableContainer)
            );
        }

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<InviteParticipantsView, InviteParticipantsViewModel>();
            set.Bind(_tableLabel).To(vm => vm.TableLabelTitle);
            set.Bind(_inviteWithFb).For("Title").To(vm => vm.InviteText);
            set.Bind(_addButton).For("Clicked").To(vm => vm.InviteMoreFriendsCommand.Command);
            set.Bind(_inviteWithFb).To(vm => vm.InviteMoreFriendsCommand.Command);
            set.Apply();
            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );
        }
    }
}

