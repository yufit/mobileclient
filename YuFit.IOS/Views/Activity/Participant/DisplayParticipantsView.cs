﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using YuFit.Core.ViewModels.Activity.Participant;

using YuFit.IOS.Controls;
using YuFit.IOS.Controls.UserTableView;
using YuFit.IOS.Themes;
using YuFit.IOS.Extensions;
using UIKit;

namespace YuFit.IOS.Views.Activity.Participant
{
    public class DisplayParticipantsView : BaseViewController<DisplayParticipantsViewModel>
    {
        class ParticipantTableView : UserInfoTableView<DisplayParticipantsViewModel, ParticipantTableViewCell> {
            public ParticipantTableView () : base(ParticipantTableViewCell.Key) { }
        }

        readonly SeparatorLine          _topLine    = new SeparatorLine();
        readonly SeparatorLine          _bottomLine = new SeparatorLine();
        readonly ParticipantTableView   _tableView  = new ParticipantTableView();

        readonly UIBarButtonItem        _addButton  = new UIBarButtonItem(UIBarButtonSystemItem.Add);

        // Analysis disable ValueParameterNotUsed
        public string Sport { get { return ViewModel.Sport; } set { UpdateContent(); } }
        public bool CanInvite { get { return ViewModel.CanInvite; } set { UpdateCanInvite(); } }
        // Analysis restore ValueParameterNotUsed


        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            AddTopLine();
            AddBottomLine();
            AddTableView();
            SetupBindings();
        }

        void AddTopLine()
        {
            View.AddSubviews(_topLine);
            _topLine.TranslatesAutoresizingMaskIntoConstraints = false;
            _topLine.AddConstraints(_topLine.Height().EqualTo(1.0f));
            _topLine.BackgroundColor = YuFitStyleKit.Yellow;

            View.AddConstraints(
                _topLine.Left().EqualTo(36.0f).LeftOf(View),
                _topLine.Right().EqualTo(-36.0f).RightOf(View),
                _topLine.Top().EqualTo(36.0f).TopOf(View)
            );
        }

        void AddBottomLine()
        {
            View.AddSubviews(_bottomLine);

            _bottomLine.TranslatesAutoresizingMaskIntoConstraints = false;
            _bottomLine.AddConstraints(_bottomLine.Height().EqualTo(1.0f));
            _bottomLine.BackgroundColor = YuFitStyleKit.Yellow;
            View.AddConstraints(
                _bottomLine.Left().EqualTo(36.0f).LeftOf(View),
                _bottomLine.Right().EqualTo(-36.0f).RightOf(View),
                _bottomLine.Bottom().EqualTo(-36.0f).BottomOf(View)
            );
        }

        void AddTableView()
        {
            _tableView.BindingContext = BindingContext;
            AddChildViewController(_tableView);
            View.AddSubview(_tableView.View);

            _tableView.View.TranslatesAutoresizingMaskIntoConstraints = false;
            View.AddConstraints(
                _tableView.View.Top().EqualTo(8.0f).BottomOf(_topLine),
                _tableView.View.Left().EqualTo().LeftOf(View),
                _tableView.View.Bottom().EqualTo(-8.0f).BottomOf(_bottomLine),
                _tableView.View.Right().EqualTo().RightOf(View)
            );
        }

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<DisplayParticipantsView, DisplayParticipantsViewModel>();
            set.Bind().For(me => me.Sport).To(vm => vm.Sport);
            set.Bind().For(me => me.CanInvite).To(vm => vm.CanInvite);
            set.Bind(_tableView.Source).To(vm => vm.Participants);
            set.Bind(_tableView.Source).For(s => s.SelectionChangedCommand).To(vm => vm.ShowUserProfileCommand.Command);
            set.Bind(_addButton).For("Clicked").To(vm => vm.InviteFriendsCommand.Command);
            set.Apply();
            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );
        }

        void UpdateContent ()
        {
            if (!string.IsNullOrEmpty(Sport)) {
                ActivityColors colors;
                colors = YuFitStyleKitExtension.ActivityBackgroundColors(Sport);
                _bottomLine.BackgroundColor = colors.Base;
                _topLine.BackgroundColor = colors.Dark;
            }
        }

        void UpdateCanInvite ()
        {
            if (ViewModel.CanInvite) {
                UITextAttributes attributes = new UITextAttributes();
                attributes.Font = NavigationBar.TitleFont;
                _addButton.SetTitleTextAttributes(attributes, UIControlState.Normal);
                NavigationItem.RightBarButtonItem = _addButton;
            }
        }

    }
}

