﻿using UIKit;

using MvvmCross.Binding.BindingContext;

using YuFit.Core.ViewModels.Activity.Participant;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Views.Activity.Participant
{
    public class InviteParticipantsTableView : MvxTableViewController
    {
        protected new InviteParticipantsViewModel ViewModel { get { return (InviteParticipantsViewModel) base.ViewModel; } }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            TableView.BackgroundColor = UIColor.Clear;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.RowHeight = 60;

            var source = new TableViewSource<InviteParticipantsTableViewCell>(TableView, InviteParticipantsTableViewCell.Key);
            TableView.Source = source;

            var set = this.CreateBindingSet<InviteParticipantsTableView, InviteParticipantsViewModel>();
            set.Bind(source).To(vm => vm.YuFitFriends);
            set.Bind(source).For(s => s.SelectionChangedCommand).To(vm => vm.ParticipantSelectedCommand.Command);
            set.Apply();
        }

        public override void ViewDidDisappear (bool animated)
        {
            base.ViewDidDisappear(animated);

            if (TableView != null) {
                if (TableView.Source != null) {
                    TableView.Source.Dispose();
                    TableView.Source = null;
                }
                TableView.Dispose ();
            }
        }
    }
}

