﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Foundation;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Activity.Participant;

using YuFit.IOS.Controls;
using YuFit.IOS.Themes;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Views.Activity.Participant
{
    public class InviteParticipantsTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("InviteParticipantTableViewCell");

        readonly UILabel _nameLabel = new UILabel();
        readonly UILabel _locationLabel = new UILabel();
        readonly PictureContainer _pictureView = new PictureContainer(7.0f);
        readonly IconView _locationIcon = new IconView();
        readonly CheckBox _checkbox = new CheckBox();

        public bool IsSelected
        {
            get { return ((InviteParticipantViewModel)DataContext).IsSelected; }
            set {
                UIView.Animate(0.25f, () => {
                    UIColor green = YuFitStyleKit.Green;
                    _locationLabel.TextColor = value ? green : YuFitStyleKit.White;
                    _nameLabel.TextColor = value ? green : YuFitStyleKit.White;
                    _checkbox.IsSelected = value;
                    _locationIcon.StrokeColor = value ? green : YuFitStyleKit.White;
                    _pictureView.StrokeColor = value ? green : YuFitStyleKit.Yellow;
                    _pictureView.SetNeedsDisplay();
                });
            }
        }

        private string _sport;
        public string Sport {
            get { return _sport; }
            set { _sport = value; UpdateContent(); }
        }

        public InviteParticipantsTableViewCell ()
        {
            BackgroundColor = UIColor.Clear;
            SelectionStyle = UITableViewCellSelectionStyle.None;

            ContentView.AddSubviews(new UIView[] {_checkbox, _nameLabel, _locationIcon, _locationLabel, _pictureView});
            _locationIcon.DrawIcon = YuFitStyleKitExtension.GetDrawingMethod("DrawWhere1");
            _pictureView.TransparentBackground = true;

            SetupConstraints();

            this.DelayBind(() => {
                _nameLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 18);
                _nameLabel.TextColor = YuFitStyleKit.Yellow;
                _locationLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 14);
                _locationLabel.TextColor = YuFitStyleKit.Yellow;

                var set = this.CreateBindingSet<InviteParticipantsTableViewCell, InviteParticipantViewModel>();
                set.Bind(_pictureView.ImageViewLoader).To(friend => friend.AvatarUrl);
                set.Bind(_nameLabel).To(friend => friend.Name);
                set.Bind(_locationLabel).To(friend => friend.Location);
                set.Bind(this).For(me => me.IsSelected).To(friend => friend.IsSelected);
                set.Apply();
            });
        }

        void SetupConstraints ()
        {
            ContentView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            _locationIcon.AddConstraints(
                _locationIcon.Height().EqualTo(10.0f),
                _locationIcon.Width().EqualTo(10.0f)
            );

            _checkbox.AddConstraints(
                _checkbox.Height().EqualTo(13.0f),
                _checkbox.Width().EqualTo(19.0f)
            );

            ContentView.AddConstraints(
                _checkbox.WithSameCenterY(ContentView),
                _checkbox.Left().EqualTo(5.0f).LeftOf(ContentView),

                _pictureView.Height().EqualTo().HeightOf(ContentView),
                _pictureView.Width().EqualTo().HeightOf(ContentView),
                _pictureView.WithSameCenterY(ContentView),
                _pictureView.Left().EqualTo(6.0f).RightOf(_checkbox),

                _nameLabel.Left().EqualTo(10.0f).RightOf(_pictureView),
                _nameLabel.Right().EqualTo(-36.0f).RightOf(ContentView),
                _nameLabel.Bottom().EqualTo(2.0f).CenterYOf(ContentView),

                _locationIcon.Left().EqualTo(10.0f).RightOf(_pictureView),
                _locationIcon.Top().EqualTo(4.0f).CenterYOf(ContentView),

                _locationLabel.Left().EqualTo(5.0f).RightOf(_locationIcon),
                _locationLabel.Right().EqualTo(-36.0f).RightOf(ContentView),
                _locationLabel.Top().EqualTo(4.0f).CenterYOf(ContentView)

            );
        }

        void UpdateContent ()
        {
            ActivityColors colors = YuFitStyleKitExtension.ActivityBackgroundColors(_sport);
            _locationLabel.TextColor = colors.Light;
            _nameLabel.TextColor = colors.Light;
            _locationIcon.StrokeColor = colors.Light;
        }

        #if DEBUG_LEAKS
        protected override void Dispose (bool disposing)
        {
        Console.WriteLine("Cell deleted");
        base.Dispose(disposing);
        }
        #endif
    }
}

