﻿using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using Foundation;
using UIKit;

using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Home;
using YuFit.IOS.Themes;
using MvvmCross.Binding.iOS.Views;

namespace YuFit.IOS.Views.HomeMenu
{
    public class EditNotificationsTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("EditNotificationsTableViewCell");

        readonly UILabel _nameLabel = new UILabel();
        readonly UISwitch _isEnabled = new UISwitch();

        public EnableNotificationViewModel ViewModel {
            get { return (EnableNotificationViewModel) DataContext; }
        }

        public bool IsEnabled {
            // Analysis disable ValueParameterNotUsed
            get { return ViewModel.IsEnabled; }
            set { UpdateColors(); }
            // Analysis restore ValueParameterNotUsed
        }

        public EditNotificationsTableViewCell ()
        {
            BackgroundColor = UIColor.Clear;
            ContentView.AddSubviews(new UIView[] {_nameLabel, _isEnabled});

            _nameLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 20);
            _nameLabel.TextColor = YuFitStyleKit.White;

            SetupConstraints();

            LayoutMargins = UIEdgeInsets.Zero;
            PreservesSuperviewLayoutMargins = false;

            this.DelayBind(() => {
                var set = this.CreateBindingSet<EditNotificationsTableViewCell, EnableNotificationViewModel>();
                set.Bind(_nameLabel).To(n => n.NotificationTitle);
                set.Bind(_isEnabled).To(n => n.IsEnabled);
                set.Bind().For(me => me.IsEnabled).To(n => n.IsEnabled);
                set.Apply();
            });
        }

        void SetupConstraints ()
        {
            ContentView.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            ContentView.AddConstraints(
                _nameLabel.Left().EqualTo(5.0f).LeftOf(ContentView),
                _nameLabel.Right().EqualTo(-5.0f).LeftOf(_isEnabled),
                _nameLabel.CenterY().EqualTo(0.0f).CenterYOf(ContentView),

                _isEnabled.Width().EqualTo(50.0f),
                _isEnabled.Right().EqualTo(-5.0f).RightOf(ContentView),
                _isEnabled.CenterY().EqualTo(0.0f).CenterYOf(ContentView)
            );
        }

        void UpdateColors ()
        {
            if (IsEnabled) {
                _nameLabel.TextColor = YuFitStyleKit.Green;
            } else {
                _nameLabel.TextColor = YuFitStyleKit.Red;
            }
        }
    }
}

