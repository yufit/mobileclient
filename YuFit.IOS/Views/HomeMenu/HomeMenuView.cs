using System;
using System.Linq;

using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;

using CoreGraphics;
using UIKit;

using YuFit.Core.ViewModels.Activity;
using YuFit.Core.ViewModels.Home;
using YuFit.Core.Extensions;
using YuFit.IOS.Controls;
using YuFit.IOS.Controls.Buttons;
using YuFit.IOS.Controls.UpcomingActivitiesShortView;
using YuFit.IOS.Extensions;
using YuFit.IOS.Themes;
using Foundation;

namespace YuFit.IOS.Views.HomeMenu
{
    public class HomeMenuView : BaseViewController<HomeMenuViewModel>
    {
        //const float TableViewHeight = 170.0f;

        readonly PictureContainer       _avatar                 = new PictureContainer();
        readonly DiamondButton          _editProfileIcon        = new DiamondButton(YuFitStyleKitExtension.GetDrawingMethod("DrawEditProfileIcon"));
        readonly DiamondButton          _editNotificationsButton= new DiamondButton(YuFitStyleKitExtension.GetDrawingMethod("DrawNotificationSettingsIcon"));
        readonly DiamondButton          _signOutButton          = new DiamondButton(YuFitStyleKitExtension.GetDrawingMethod("DrawSignOutIcon"));

        readonly UILabel                _editNotificationsLabel = new UILabel();
        readonly UILabel                _signOutLabel           = new UILabel();
        readonly UILabel                _nameLabel              = new UILabel();
        readonly UILabel                _homeLabel              = new UILabel();
        readonly UILabel                _bio                    = new UILabel();

        readonly ShortListView          _upcomingListView       = new ShortListView();

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            View.ClipsToBounds = true;

            SetupSubviews();
            SetupBindings();
        }

        void SetupSubviews()
        {
            SetupAvatar();
            SetupEditProfileIcon();
            SetupNameLabel();
            SetupHomeLabel();
            SetupLogoutButton();
            SetupEditNotificationsButton();
            SetupBio();
            SetupUpcomingActivitiesContainer();
        }

        void SetupAvatar()
        {
            _avatar.TranslatesAutoresizingMaskIntoConstraints = false;
            Add(_avatar);

            _avatar.StrokeWidth = 4.0f;
            _avatar.StrokeColor = YuFitStyleKit.YellowDark;
            _avatar.AddConstraints(_avatar.Height().EqualTo().WidthOf(_avatar) );

            View.AddConstraints(
                _avatar.Width().EqualTo().WidthOf(View).WithMultiplier(.7f),
                _avatar.WithSameCenterX(View),
                _avatar.Top().EqualTo(5.0f).TopOf(View)
            );
        }

        void SetupEditProfileIcon()
        {
            _editProfileIcon.DiamondColor = YuFitStyleKit.Yellow;
            _editProfileIcon.StrokeColor = YuFitStyleKit.White;
            _editProfileIcon.TranslatesAutoresizingMaskIntoConstraints = false;
            View.Add(_editProfileIcon);

            _editProfileIcon.AddConstraints(_editProfileIcon.Height().EqualTo().WidthOf(_editProfileIcon) );

            View.AddConstraints(
                _editProfileIcon.Width().EqualTo().WidthOf(_avatar).WithMultiplier(0.25f),
                _editProfileIcon.CenterX().EqualTo().CenterXOf(_avatar),
                _editProfileIcon.Bottom().EqualTo().BottomOf(_avatar)
            );
        }

        void SetupNameLabel ()
        {
            _nameLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            View.Add(_nameLabel);

            _nameLabel.TextAlignment = UITextAlignment.Center;
            _nameLabel.Font = UIFont.FromName("HelveticaNeueLTStd-BdCn", 26);
            _nameLabel.TextColor = YuFitStyleKit.White;
            _nameLabel.AdjustsFontSizeToFitWidth = true;

            View.AddConstraints(
                _nameLabel.WithSameCenterX(View),
                _nameLabel.Top().EqualTo(20.0f).BottomOf(_avatar),
                _nameLabel.Width().EqualTo().WidthOf(View)
            );
        }

        void SetupHomeLabel ()
        {
            _homeLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            View.Add(_homeLabel);

            _homeLabel.TextAlignment = UITextAlignment.Center;
            _homeLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 18);
            _homeLabel.TextColor = YuFitStyleKit.Gray;
            _homeLabel.AdjustsFontSizeToFitWidth = true;

            View.AddConstraints(
                _homeLabel.WithSameCenterX(View),
                _homeLabel.Top().EqualTo(5.0f).BottomOf(_nameLabel),
                _homeLabel.Width().EqualTo().WidthOf(View)
            );
        }

        void SetupLogoutButton()
        {
            _signOutButton.DiamondColor = YuFitStyleKit.Red;
            _signOutButton.StrokeColor = YuFitStyleKit.White;
            _signOutLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            _signOutLabel.TextColor = YuFitStyleKit.Red;
            _signOutLabel.TextAlignment = UITextAlignment.Center;

            View.AddSubviews (new UIView [] { _signOutButton, _signOutLabel });
            View.Subviews.ForEach(sv => sv.TranslatesAutoresizingMaskIntoConstraints = false);

            _signOutButton.AddConstraints(_signOutButton.Height().EqualTo().WidthOf(_signOutButton) );

            View.AddConstraints(
                _signOutButton.Width().EqualTo().WidthOf(_avatar).WithMultiplier(0.25f),
                _signOutButton.CenterX().EqualTo().RightOf(_avatar),
                _signOutButton.Bottom().EqualTo().BottomOf(_avatar),
                _signOutLabel.Top().EqualTo().BottomOf(_signOutButton),
                _signOutLabel.WithSameCenterX(_signOutButton)

            );
        }


        void SetupEditNotificationsButton()
        {
            _editNotificationsButton.DiamondColor = YuFitStyleKit.Green;
            _editNotificationsButton.StrokeColor = YuFitStyleKit.White;

            _editNotificationsLabel.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            _editNotificationsLabel.TextColor = YuFitStyleKit.Green;
            _editNotificationsLabel.TextAlignment = UITextAlignment.Center;

            View.AddSubviews(new UIView [] { _editNotificationsButton, _editNotificationsLabel });
            View.Subviews.ForEach(sv => sv.TranslatesAutoresizingMaskIntoConstraints = false);

            _editNotificationsButton.AddConstraints(_editNotificationsButton.Height().EqualTo().WidthOf(_editNotificationsButton) );

            View.AddConstraints(
                _editNotificationsButton.Width().EqualTo().WidthOf(_avatar).WithMultiplier(0.25f),
                _editNotificationsButton.CenterX().EqualTo().LeftOf(_avatar),
                _editNotificationsButton.Bottom().EqualTo().BottomOf(_avatar),
                _editNotificationsLabel.Top().EqualTo().BottomOf(_editNotificationsButton),
                _editNotificationsLabel.WithSameCenterX(_editNotificationsButton)
            );
        }

        void SetupBio()
        {
            _bio.Font = UIFont.FromName("HelveticaNeueLTStd-LtCn", 16);
            _bio.TextColor = YuFitStyleKit.Yellow;  
            _bio.TextAlignment = UITextAlignment.Left;
            _bio.Lines = 0;

            View.Add(_bio);
            _bio.TranslatesAutoresizingMaskIntoConstraints = false;

            View.AddConstraints(
                _bio.Top().EqualTo(10f).BottomOf(_homeLabel),
                _bio.CenterX().EqualTo().CenterXOf(View),
                _bio.Width().EqualTo().WidthOf(View).WithMultiplier(.7f)
            );
        }

        void SetupUpcomingActivitiesContainer()
        {
            _upcomingListView.SetBindingContext(BindingContext);
            AddChildViewController(_upcomingListView.TableViewCtrl);
            _upcomingListView.SetLayoutConstraints();

            _upcomingListView.TranslatesAutoresizingMaskIntoConstraints = false;

            View.AddSubview(_upcomingListView);

            View.AddConstraints(
                _bio.Bottom().LessThanOrEqualTo().TopOf(_upcomingListView),                    
                _upcomingListView.Height().EqualTo().HeightOf(View).WithMultiplier(.25f),
                _upcomingListView.Left().EqualTo(Spacing).LeftOf(View),
                _upcomingListView.Bottom().EqualTo(-8.0f).BottomOf(View),
                _upcomingListView.Right().EqualTo(-Spacing).RightOf(View)
            );
        }
        public override void ViewDidDisappear (bool animated)
        {
            _upcomingListView.SetBindingContext(null);
            base.ViewDidDisappear(animated);
        }

        void SetupBindings()
        {
            var set = this.CreateBindingSet<HomeMenuView, HomeMenuViewModel>();
            set.Bind().For(me => me.Title).To(vm => vm.DisplayName);
            set.Bind(_avatar.ImageViewLoader).To(vm => vm.AvatarUrl);
            set.Bind(_nameLabel).To(vm => vm.RealName);
            set.Bind(_homeLabel).To(vm => vm.HomeAddress);
            set.Bind(_signOutLabel).To(vm => vm.SignOutCommand.DisplayName);
            set.Bind(_editNotificationsLabel).To(vm => vm.EditNotificationsCommand.DisplayName);
            set.Bind(_bio).To(vm => vm.Bio);

            set.Bind(_upcomingListView.UpcomingTitle).To(vm => vm.UpcomingActivitiesTitle);
            set.Bind(_upcomingListView.TableViewCtrl.Source).To(vm => vm.UpcomingActivities);
            set.Bind(_upcomingListView.TableViewCtrl.Source).For(s => s.SelectionChangedCommand).To(vm => vm.ShowActivitySummaryCommand.Command);

            set.Bind(_editNotificationsButton.Tap()).To(vm => vm.EditNotificationsCommand.Command);
            set.Bind(_editProfileIcon.Tap()).To(vm => vm.EditProfileCommand.Command);
            set.Bind(_signOutButton.Tap()).To(vm => vm.SignOutCommand.Command);
            set.Apply ();

            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage);
        }
    }

    class TableViewCell : UpcomingShortListTableViewCell<UpcomingActivityViewModel> {
        public static readonly NSString Key = new NSString("UpcomingActivitiesTableViewCell");
    }

    class TableView : UpcomingShortListTableView<HomeMenuViewModel, TableViewCell> {
        public TableView () : base(TableViewCell.Key) { }
    }
    class ShortListView : UpcomingShortListView<TableView, TableViewCell> {
    }

}

