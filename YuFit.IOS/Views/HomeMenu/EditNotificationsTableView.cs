﻿using System;
using YuFit.Core.ViewModels.Home;
using UIKit;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;

namespace YuFit.IOS.Views.HomeMenu
{
    public class EditNotificationsTableView : MvxTableViewController
    {
        protected new EditNotificationsViewModel ViewModel { get { return (EditNotificationsViewModel) base.ViewModel; } }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();
            TableView.BackgroundColor = UIColor.Clear;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            TableView.RowHeight = 60;

            var source = new TableViewSource<EditNotificationsTableViewCell>(TableView, EditNotificationsTableViewCell.Key);
            TableView.Source = source;

            TableView.AllowsSelection = false;
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
            TableView.SeparatorColor =  YuFit.IOS.Themes.YuFitStyleKit.Yellow;

            // This alongs with settings LayoutMargins and 
            // PreservesSuperviewLayoutmargins in the cell class, ensure the separator
            // line does not have an inset.
            TableView.SeparatorInset = UIEdgeInsets.Zero;

            // This is to not show empty cell at the bottom.
            TableView.TableFooterView = new UIView();
            TableView.TableFooterView.Hidden = true;

            var set = this.CreateBindingSet<EditNotificationsTableView, EditNotificationsViewModel>();
            set.Bind(source).To(vm => vm.EnabledNotifications);
            set.Apply();
        }

        public override void ViewDidDisappear (bool animated)
        {
            base.ViewDidDisappear(animated);

            if (TableView != null) {
                if (TableView.Source != null) {
                    TableView.Source.Dispose();
                    TableView.Source = null;
                }
                TableView.Dispose ();
            }
        }
    }
}











  