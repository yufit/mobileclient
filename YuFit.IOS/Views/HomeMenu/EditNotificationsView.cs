﻿using System;
using YuFit.Core.ViewModels.Home;
using MvvmCross.Binding.BindingContext;
using YuFit.IOS.Extensions;
using UIKit;
using YuFit.Core.Extensions;
using Cirrious.FluentLayouts.Touch;

namespace YuFit.IOS.Views.HomeMenu
{
    public class EditNotificationsView : BaseViewController<EditNotificationsViewModel>
    {
        readonly EditNotificationsTableView _tableView = new EditNotificationsTableView();

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            _tableView.BindingContext = BindingContext;
            AddChildViewController(_tableView);

            CreateViewHierarchy();
            ConfigureViewSpecifics();
            SetupConstraints();
            SetupBindings();
        }

        void CreateViewHierarchy ()
        {
            View.AddSubviews(new [] {_tableView.View});
        }

        void ConfigureViewSpecifics ()
        {
        }

        void SetupConstraints ()
        {
            View.Subviews.ForEach(v => v.TranslatesAutoresizingMaskIntoConstraints = false);
            View.AddConstraints(
                _tableView.View.Top().EqualTo().TopOf(View),
                _tableView.View.Left().EqualTo(30.0f).LeftOf(View),
                _tableView.View.Bottom().EqualTo().BottomOf(View),
                _tableView.View.Right().EqualTo(-30.0f).RightOf(View)
            );
        }

        void SetupBindings ()
        {
            var set = this.CreateBindingSet<EditNotificationsView, EditNotificationsViewModel>();
            set.Apply();
            ViewModel.BindLoadingMessage(View, vm => vm.IsBusy, vm => vm.ProgressMessage );
        }
    }
}

