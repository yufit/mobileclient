﻿using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Util;
using Android.Views;

namespace YuFit.Droid.Controls.CustomViews
{
    public class ArrowView : View
    {
        Color _color;
//        int _direction;
        float _thickness = 2.0f;

        public ArrowView (Context context) :
            base(context)
        {
            Initialize();
        }

        public ArrowView (Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            Initialize(attrs);
        }

        public ArrowView (Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            Initialize(attrs);
        }

        void Initialize (IAttributeSet attrs = null)
        {
            if (attrs != null) {
                TypedArray a = Context.Theme.ObtainStyledAttributes(
                    attrs,
                    Resource.Styleable.ArrowView,
                    0, 0);

                try {
                    _color = a.GetColor(Resource.Styleable.ArrowView_arrowcolor, Color.Blue);
//                    _direction = a.GetInteger(Resource.Styleable.ArrowView_direction, 0);
                    _thickness = a.GetFloat(Resource.Styleable.ArrowView_arrowthickness, 0);
                } finally {
                    a.Recycle();
                }
            }
        }

        protected override void OnDraw (Canvas canvas)
        {
            base.OnDraw(canvas);

            Paint p = new Paint();
            p.Color = _color;
            p.StrokeWidth = _thickness;
            p.AntiAlias = true;
            p.Dither = true;
            p.SetStyle(Paint.Style.Stroke);
            p.StrokeJoin = Paint.Join.Round;

            canvas.DrawLine (0, 0, Width, Height/2, p);
            canvas.DrawLine (Width, Height/2, 0, Height, p);
        }
    }
}

