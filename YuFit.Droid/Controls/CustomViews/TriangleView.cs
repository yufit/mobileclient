﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Content.Res;

namespace YuFit.Droid.Controls.CustomViews
{
    public class TriangleView : View
    {
        Color _color;

        public TriangleView (Context context) :
            base(context)
        {
            Initialize();
        }

        public TriangleView (Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
            Initialize(attrs);
        }

        public TriangleView (Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
            Initialize(attrs);
        }

        void Initialize (IAttributeSet attrs = null)
        {
            if (attrs != null) {
                TypedArray a = Context.Theme.ObtainStyledAttributes(
                    attrs,
                    Resource.Styleable.TriangleView,
                    0, 0);

                try {
                    _color = a.GetColor(Resource.Styleable.TriangleView_trianglecolor, Color.Blue);
                } finally {
                    a.Recycle();
                }
            }
        }

        protected override void OnDraw (Canvas canvas)
        {
            base.OnDraw(canvas);

//            int w = Width / 2;
//
//            Path path = new Path();
//            path.MoveTo( w, 0);
//            path.LineTo( 2 * w , 0);
//            path.LineTo( 2 * w , w);
//            path.LineTo( w , 0);
//            path.Close();
//

            Path path = new Path();
            path.MoveTo( 0, 0);
            path.LineTo( Width, Height / 2 );
            path.LineTo( 0, Height);
            path.LineTo( 0, 0);
            path.Close();


            Paint p = new Paint();
            p.Color = _color;

            canvas.DrawPath(path, p);
        }
    }
}

