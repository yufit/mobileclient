﻿using System.Collections.Generic;

using Android.Content;

using Coc.MvvmCross.Plugins.Facebook;
using Coc.MvvmCross.Plugins.Facebook.Droid;

using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Platform;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using MvvmCross.Platform.Platform;
using MvvmCross.Platform.Plugins;

using YuFit.Core;
using YuFit.Core.Interfaces.Services;
using YuFit.Droid.Services;

namespace YuFit.Droid
{
    /// <summary>
    /// Setup class.  Part of the MvvmFramework stuff.
    /// </summary>
    /// ---------------------------------------------------------------------------------------------------
    public class Setup : MvxAndroidSetup
	{
        Context _applicationContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:YuFit.Droid.Setup"/> class.
        /// </summary>
        /// <param name="applicationContext">Application context.</param>
        /// ---------------------------------------------------------------------------------------------------
		public Setup(Context applicationContext) : base(applicationContext)
		{
            _applicationContext = applicationContext;
		}

        /// <summary>
        /// Creates the app.
        /// </summary>
        /// <returns>The app.</returns>
        /// ---------------------------------------------------------------------------------------------------
		protected override IMvxApplication CreateApp()
		{
			return new App();
		}

        /// <summary>
        /// Creates the debug trace.
        /// </summary>
        /// <returns>The debug trace.</returns>
        /// ---------------------------------------------------------------------------------------------------
		protected override IMvxTrace CreateDebugTrace()
		{
			return new DebugTrace();
		}

        /// <summary>
        /// Initializes the first chance.
        /// </summary>
        /// <returns>The first chance.</returns>
        /// ---------------------------------------------------------------------------------------------------
		protected override void InitializeFirstChance()
		{
			base.InitializeFirstChance();
			CreatableTypes().EndingWith("Service").AsInterfaces().RegisterAsLazySingleton();

            // TODO remove this and use 
            // _context = CrossCurrentActivity.Current.Activity.ApplicationContext;
            ((AndroidStringLoaderService)Mvx.Resolve<IStringLoaderService>()).Context = _applicationContext;
		}

        /// <summary>
        /// Initializes the plugin framework.
        /// </summary>
        /// <returns>The plugin framework.</returns>
        /// ---------------------------------------------------------------------------------------------------
        protected override IMvxPluginManager InitializePluginFramework ()
        {
            var p = base.InitializePluginFramework ();

            // TODO Move this logic in the ViewModel.
            // For the ios version, have the Init method have the appid hardcoded in it.
            var facebookService = Mvx.Resolve<IFacebookService> ();
            var droidFacebookService = (DroidFacebookService) facebookService;
            droidFacebookService.InitializeAppInfo();

            return p;
        }

        protected override IDictionary<string, string> ViewNamespaceAbbreviations
        {
            get
            {
                var abbreviations = base.ViewNamespaceAbbreviations;
                abbreviations["Abv"] = "yufit.droid.controls.customViews";
                return abbreviations;
            }
        }
	}
}