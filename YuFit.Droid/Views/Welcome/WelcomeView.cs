﻿using Android.App;
using Android.Content.PM;
using Android.OS;

using YuFit.Core.ViewModels.Welcome;
using Android.Views;
using Android.Support.V4.View;
using Android.Support.V4.App;
using Android.Graphics;
using Android.Content;
using Android.Widget;
using YuFit.Droid.Controls.CustomViews;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using Xamarin.Facebook;
using System;
using Android.Runtime;
using Xamarin.Facebook.Login;
using MvvmCross.Platform;

[assembly:Permission (Name = Android.Manifest.Permission.Internet)]
[assembly:Permission (Name = Android.Manifest.Permission.WriteExternalStorage)]
[assembly:MetaData ("com.facebook.sdk.ApplicationId", Value ="@string/app_id")]
[assembly:MetaData ("com.facebook.sdk.ApplicationName", Value ="@string/app_name")]

namespace YuFit.Droid.Views.Welcome
{
	[Activity (Label = "WelcomeView", ScreenOrientation = ScreenOrientation.Portrait)]
	public class WelcomeView : MvxFragmentActivity //BaseViewController<WelcomeViewModel>
	{
        ViewPager _pager;
        PagerAdapter _pagerAdapter;

        ICallbackManager callbackManager;

		protected override void OnCreate (Bundle bundle)
		{
            Acr.UserDialogs.UserDialogs.Init (this);
			base.OnCreate (bundle);

            //Remove title bar
            RequestWindowFeature(WindowFeatures.NoTitle);

            //Remove notification bar
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);

			SetContentView(Resource.Layout.WelcomeView);
			
            // Create your application here
            _pager = (ViewPager) FindViewById(Resource.Id.pager);
            _pagerAdapter = new ScreenSlidePagerAdapter(ViewModel, SupportFragmentManager);
            _pager.Adapter = _pagerAdapter;
            _pager.PageSelected += (object sender, ViewPager.PageSelectedEventArgs e) => {
                InvalidateOptionsMenu();
            };

            //FacebookSdk.SdkInitialize (this.ApplicationContext);

            //callbackManager = CallbackManagerFactory.Create ();

            //var loginCallback = new FacebookCallback<LoginResult> {
            //    HandleSuccess = loginResult => {
            //        Mvx.Trace("Success");
            //        //HandlePendingAction ();
            //        //UpdateUI ();
            //    },
            //    HandleCancel = () => {
            //        Mvx.Trace("Cancel");
            //        //if (pendingAction != PendingAction.NONE) {
            //        //    ShowAlert (
            //        //        GetString (Resource.String.cancelled),
            //        //        GetString (Resource.String.permission_not_granted));
            //        //    pendingAction = PendingAction.NONE;
            //        //}
            //        //UpdateUI ();                        
            //    },
            //    HandleError = loginError => {
            //        Mvx.Trace("Error");
            //        //if (pendingAction != PendingAction.NONE
            //        //    && loginError is FacebookAuthorizationException) {
            //        //    ShowAlert (
            //        //        GetString (Resource.String.cancelled),
            //        //        GetString (Resource.String.permission_not_granted));
            //        //    pendingAction = PendingAction.NONE;
            //        //}
            //        //UpdateUI ();
            //    }
            //};

            //LoginManager.Instance.RegisterCallback (callbackManager, loginCallback);


            //((Coc.MvvmCross.Plugins.Facebook.Droid.DroidFacebookService)MvvmCross.Platform.Mvx.Resolve<Coc.MvvmCross.Plugins.Facebook.IFacebookService> ()).AuthenticateAsync2();

		}

        protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult (requestCode, resultCode, data);
            ((Coc.MvvmCross.Plugins.Facebook.Droid.DroidFacebookService)MvvmCross.Platform.Mvx.Resolve<Coc.MvvmCross.Plugins.Facebook.IFacebookService> ()).OnActivityResult(requestCode, (int) resultCode, data);

            //callbackManager.OnActivityResult (requestCode, (int)resultCode, data);
        }

        public override bool OnCreateOptionsMenu (IMenu menu)
        {
//            if (!base.OnCreateOptionsMenu(menu)) {return false;}

            MenuInflater.Inflate(Resource.Menu.WelcomeScreenMenu, menu);

            var item = menu.Add(
                Menu.None, Resource.Id.action_next, 
                Menu.None, (_pager.CurrentItem == _pagerAdapter.Count-1) ? "Finish" : "Next"
            );

            item.SetShowAsAction(ShowAsAction.IfRoom | ShowAsAction.WithText);

            return true;
        }

        public override bool OnOptionsItemSelected (IMenuItem item)
        {
            switch (item.ItemId) {
                case Android.Resource.Id.Home:
                    //NavUtils.NavigateUpTo(this, new Android.Content.Intent(this, MainActivity.))
                    return true;

                case Resource.Id.action_previous:
                    _pager.CurrentItem = _pager.CurrentItem - 1;
                    return true;

                case Resource.Id.action_next:
                    _pager.CurrentItem = _pager.CurrentItem + 1;
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }
	}

    class ScreenSlidePageFragment : MvxFragment
    {
        Button postStatusUpdateButton;

        string _message;
        public string Message {
            get { return _message; }
            set { 
                _message = value; 
            }
        }

//        int pageNumber;
        public static ScreenSlidePageFragment Create(BasePageViewModel vm, int pageNumber) {
            var fragment = new ScreenSlidePageFragment();
//            fragment.pageNumber = pageNumber;
            fragment.DataContext = vm;
            return fragment;
        }

        public override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

        }

        public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            this.EnsureBindingContextIsSet (savedInstanceState);
            var set = this.CreateBindingSet<ScreenSlidePageFragment, IntroViewModel>();
            set.Bind().For(me => me.Message).To(vm => vm.Messages);
            set.Apply();

            ViewGroup rootView = (ViewGroup) inflater.Inflate(Resource.Layout.FirstView, container, false);
            //rootView.SetBackgroundColor(Android.Graphics.Color.Purple);

            var v = (RelativeLayout) rootView.FindViewById(Resource.Id.container_diamond);

            Typeface tf = Typeface.CreateFromAsset (Application.Context.Assets, "fonts/HelveticaNeueLTStd-Cn.otf");
            TextView tv1=(TextView) rootView.FindViewById(Resource.Id.pagenum);
            tv1.SetTypeface(tf, TypefaceStyle.Normal);

            var metrics = Resources.DisplayMetrics;
            var width = metrics.WidthPixels;
            var height = metrics.HeightPixels;

            v.LayoutParameters.Height = (int) (height * 0.39f);
            v.LayoutParameters.Width = (int) (v.LayoutParameters.Height/2.0f);

            ArrowView arrow = (ArrowView)rootView.FindViewById(Resource.Id.arrow);
            arrow.LayoutParameters.Height = (int) (v.LayoutParameters.Height * 0.25f);
            arrow.LayoutParameters.Width = (int) (arrow.LayoutParameters.Height/2.0f);

            postStatusUpdateButton = rootView.FindViewById<Button> (Resource.Id.postStatusUpdateButton);
            postStatusUpdateButton.Click += (sender, e) => {
                ((Coc.MvvmCross.Plugins.Facebook.Droid.DroidFacebookService)MvvmCross.Platform.Mvx.Resolve<Coc.MvvmCross.Plugins.Facebook.IFacebookService> ()).AuthenticateAsync();

                //string [] PERMISSIONS = new [] { "publish_actions" };
                //LoginManager.Instance.LogInWithPublishPermissions (this, PERMISSIONS);
            };

            return rootView;
        }

        public override void OnActivityResult (int requestCode, int resultCode, Intent data)
        {
            base.OnActivityResult (requestCode, resultCode, data);
            ((Coc.MvvmCross.Plugins.Facebook.Droid.DroidFacebookService)MvvmCross.Platform.Mvx.Resolve<Coc.MvvmCross.Plugins.Facebook.IFacebookService> ()).OnActivityResult(requestCode, resultCode, data);
        }

        private int ConvertPixelsToDp(float pixelValue)
        {
            var dp = (int) ((pixelValue)/Resources.DisplayMetrics.Density);
            return dp;
        }
    }

    class ScreenSlidePagerAdapter : FragmentPagerAdapter {
        WelcomeViewModel _viewModel;
        public ScreenSlidePagerAdapter (IMvxViewModel viewModel, Android.Support.V4.App.FragmentManager fm) : base(fm)
        {
            _viewModel = (WelcomeViewModel) viewModel;
        }

        public ScreenSlidePagerAdapter (Android.Support.V4.App.FragmentManager fm) : base(fm)
        {
            
        }

        public override Android.Support.V4.App.Fragment GetItem (int position)
        {
            return ScreenSlidePageFragment.Create(_viewModel.Pages[position], position);
        }

        public override int Count
        {
            get
            {
                return _viewModel.Pages.Count;
            }
        }
    }
    class FacebookCallback<TResult> : Java.Lang.Object, IFacebookCallback where TResult : Java.Lang.Object
    {
        public Action HandleCancel { get; set; }
        public Action<FacebookException> HandleError { get; set; }
        public Action<TResult> HandleSuccess { get; set; }

        public void OnCancel ()
        {
            var c = HandleCancel;
            if (c != null)
                c ();
        }

        public void OnError (FacebookException error)
        {
            var c = HandleError;
            if (c != null)
                c (error);
        }

        public void OnSuccess (Java.Lang.Object result)
        {
            var c = HandleSuccess;
            if (c != null)
                c (result.JavaCast<TResult> ());
        }
    }
}

