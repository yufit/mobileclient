﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Views;
using YuFit.Core.ViewModels;

namespace YuFit.Droid.Views
{
	public class BaseViewController<T> : MvxActivity where T : NamedViewModel
	{
		protected override void OnCreate (Bundle bundle)
		{
            UserDialogs.Init(this);
			base.OnCreate (bundle);

			// Create your application here
		}
	}
}

