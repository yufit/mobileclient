using MvvmCross.Platform.Plugins;

namespace YuFit.Droid.Bootstrap
{
    public class FilePluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.File.PluginLoader>
    {
    }
}