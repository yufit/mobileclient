﻿using MvvmCross.Platform.Plugins;


namespace YuFit.Droid.Bootstrap
{
    public class FacebookPluginBootstrap
        : MvxLoaderPluginBootstrapAction<Coc.MvvmCross.Plugins.Facebook.PluginLoader, Coc.MvvmCross.Plugins.Facebook.Droid.Plugin>
    {
        public FacebookPluginBootstrap ()
        {
        }
    }
}
