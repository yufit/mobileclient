﻿using System;
using YuFit.Core.Interfaces.Services;

namespace YuFit.Droid.Services
{
    public class AndroidNetworkMonitoringService : INetworkMonitoringService
    {
        #region INetworkMonitoringService implementation

        public void NetworkIsGoingActive ()
        {
        }

        public void NetworkIsGoingIdle ()
        {
        }

        public void NetworkSeemsToBeSlow ()
        {
            throw new NotImplementedException();
        }

        public void NetworkSeemsToHaveRecovered ()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}

