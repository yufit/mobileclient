﻿using System;
using Android.Content;
using YuFit.Core.Interfaces.Services;

namespace YuFit.Droid.Services
{
    public class AndroidStringLoaderService : IStringLoaderService
    {
        public Context Context { get; set; }

        #region IStringLoaderService implementation

        /// <summary>
        /// Return the localized string.
        /// </summary>
        /// <returns>The string.</returns>
        /// <param name="baseString">Base string.</param>
        public string GetString(string baseString)
        {
            try {
                int text_id = Context
                    .Resources
                    .GetIdentifier(baseString, "string", Context.PackageName);

                return Context.GetString(text_id);
            } catch (Exception /* ex */) {
                return "TRANSLATION MISSING";
            }
        }

        /// <summary>
        /// Gets the string. NO-OP not used.
        /// </summary>
        /// <returns>The string.</returns>
        /// <param name="baseString">Base string.</param>
        /// <param name="table">Table.</param>
        public string GetString(string baseString, string table)
        {
            return "";
        }

        #endregion
    }
}

