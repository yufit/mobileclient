﻿using System;

namespace YuFit.Core.Interfaces.Services
{
    public interface INetworkMonitoringService
    {
        void NetworkIsGoingActive();
        void NetworkIsGoingIdle();
        void NetworkSeemsToBeSlow();
        void NetworkSeemsToHaveRecovered();
    }
}

