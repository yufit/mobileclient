﻿using System;
using System.Threading.Tasks;
using System.IO;

namespace YuFit.Core.Interfaces.Services
{
    public interface IImagePickerService
    {
        void ChoosePictureFromLibrary(int percentQuality, object uiHint, Action<Stream> pictureAvailable, Action assumeCancelled);
        void TakePicture(int percentQuality, object uiHint, Action<Stream> pictureAvailable, Action assumeCancelled);

        /// <summary>
        /// Returns null if cancelled
        /// </summary>
        Task<Stream> ChoosePictureFromLibrary(object uiHint);

        /// <summary>
        /// Returns null if cancelled
        /// </summary>
        Task<Stream> TakePicture(object uiHint);
    }
}

