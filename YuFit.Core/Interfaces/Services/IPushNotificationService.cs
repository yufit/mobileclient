﻿using System.Collections.Generic;
using System.Threading.Tasks;
using YuFit.Core.Models;
using System.Threading;

namespace YuFit.Core.Interfaces.Services
{

    public interface IPushNotificationService
    {
        string ActivityIdToDisplay { get; set; }

        // Mobile api to registers device to received notification.
        void SetupNotificationsHandler();

        // Api to back end server to get notifications sent by server in the
        // past 7 days.
        Task<IList<NotificationRecord>> GetNotifications(CancellationToken token = default(CancellationToken));
        Task<bool> AckNotification(string notificationId, CancellationToken token = default(CancellationToken));
        Task<bool> AckAllNotifications(CancellationToken token = default(CancellationToken));
    }
}

