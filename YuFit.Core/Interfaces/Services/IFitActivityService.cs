﻿using System.Threading.Tasks;
using System.Collections.Generic;
using YuFit.Core.Models;
using System.Threading;

namespace YuFit.Core.Interfaces.Services
{
    public interface IFitActivityService
    {
        List<ActivityType> AvailableActivities{ get; }

        Task<List<FitActivity>> GetAllActivities(CancellationToken token = default(CancellationToken));
        Task<FitActivity> GetActivity(string activityId, CancellationToken token = default(CancellationToken));
        Task<FitActivity> CreateActivity(FitActivity fitActivity, CancellationToken token = default(CancellationToken));
        Task<FitActivity> UpdateActivity(FitActivity fitActivity, CancellationToken token = default(CancellationToken));
        Task DeleteActivity(FitActivity fitActivity, CancellationToken token = default(CancellationToken));

        Task<List<FitActivity>> SearchForActivities(Coordinate coordinates, double range = 10.0f, CancellationToken token = default(CancellationToken));
        Task<List<FitActivity>> MyUpcomingActivities(int limit = -1, CancellationToken token = default(CancellationToken));
        Task<List<FitActivity>> MyInvitedActivities(CancellationToken token = default(CancellationToken));
        Task<List<FitActivity>> MyCreatedActivities(CancellationToken token = default(CancellationToken));

        Task<List<FitActivityComment>> GetActivityComments(string activityId, CancellationToken token = default(CancellationToken));
        Task<FitActivityComment> AddComment (string activityId, string comment, CancellationToken token = default(CancellationToken));
        Task<bool> DeleteComment (string activityId, string commentId, CancellationToken token = default(CancellationToken));

        Task SetGoing(string Id, bool going);


        List<EventCategory> GetEventCategories();
    }
}

