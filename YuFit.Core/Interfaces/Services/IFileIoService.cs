﻿namespace YuFit.Core.Interfaces.Services
{
    public interface IFileIoService
    {
        string Read(string fileName);
        T Read<T>(string fileName) where T : new();

        void Write(string fileName, string data);
        void Write<T>(string fileName, T data);

        void RemoveFile(string fileName);
    }
}

