﻿using System.Collections.Generic;
using System.Threading.Tasks;
using YuFit.Core.Models;
using Coc.MvvmCross.Plugins.Location;

namespace YuFit.Core.Interfaces.Services
{
    public class Placemark {
        public Coordinate Coordinates;
        public string SubThoroughfare;
        public string Thoroughfare;
        public string Locality;
        public string SubLocality;
        public string PostalCode;
        public string AdministrativeArea;
        public string SubAdministrativeArea;
        public string Country;

    }

    public class Region {
        public Coordinate Center;
        public double Radius;
        public string Identifier;
    }

    public interface IGeoLocationConverterService
    {
        Task<Placemark> ReverseGeocodeLocationAsync(CocLocation location, bool keepLocation = true);
        Task<Placemark> ReverseGeocodeLocationAsync(Coordinate location, bool keepLocation = true);
        Task<List<Placemark>> GeocodeAddressAsync(string address);
        Task<List<Placemark>> GeocodeAddressAsync(string address, Region region);
    }
}

