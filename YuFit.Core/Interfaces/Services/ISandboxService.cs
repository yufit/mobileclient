﻿namespace YuFit.Core.Interfaces.Services
{
    public interface ISandboxService
    {
        void SetItem<T>(string key, T item);
        T GetItem<T>(string key);
    }
}

