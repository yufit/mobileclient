using YuFit.Core.Services.Notifications;


namespace YuFit.Core.Interfaces.Services
{
    public enum PushNotificationResult
    {
        NewData,
        NoData,
        Failed
    }

    public interface IPushNotification
    {
        PushNotificationCategory Category { get; }
        string Message { get; }

        bool AppIsInForeground { get; set; }

        void HandleReceivedNotificationWhileRunning(System.Action<PushNotificationResult> completionHandler);
        void HandleReceivedNotificationWhileStarting();
        void HandleNotificationAction(string actionIdentifier, System.Action completionHandler);

        int? UpdatedBadgeCount { get; set; }
    }
}
