﻿using System.Collections.Generic;
using System.Threading.Tasks;

using YuFit.Core.Models;
using YuFit.WebServices.Interface.Model;
using System.IO;
using System.Threading;

namespace YuFit.Core.Interfaces.Services
{
    public interface IUserService
    {
        bool                    NeedsAuthentication { get; }
        User                    LoggedInUser        { get; set; }

        Task                    RefreshCurrentUserInfo          (CancellationToken token = default(CancellationToken));
        Task                    UpdateUserCurrentPosition       (ILocation location, CancellationToken token = default(CancellationToken));
        Task                    SendUserCurrentPosition         (CancellationToken token = default(CancellationToken));

        Task<bool>              AuthenticateWithFacebook        (string token, string email, CancellationToken ct = default(CancellationToken));
        Task<bool>              AuthenticateWithEmail           (string email, string password, CancellationToken token = default(CancellationToken));
        Task<bool>              RegisterWithEmail               (string name, string email, string password, CancellationToken token = default(CancellationToken));

        Task<IUser>             FetchUserInfo                   (string id, CancellationToken token = default(CancellationToken));
        Task<IUser>             UpdateUserInfo                  (IUser user, CancellationToken token = default(CancellationToken));
        Task<IUser>             GetLoggedInUserInfo             (bool refresh = false, CancellationToken token = default(CancellationToken));
        Task<IList<IUserGroup>> GetGroups                       (bool refresh = true, CancellationToken token = default(CancellationToken));
        Task<IUser>             SetFilters                      (IEventFilter filters, CancellationToken token = default(CancellationToken));
        Task<IUser>             UpdateAvatarImage               (IUser user, string contentType, Stream image, CancellationToken token = default(CancellationToken));

        Task<IList<IUser>>      SearchUser                      (string query, int pageNum, int pageSize, CancellationToken token = default(CancellationToken));

        bool                    AmITheCreatorOfThisActivity     (FitActivity activity);
        bool                    AmIInvitedToThisActivity        (FitActivity activity);

        bool                    IsThisMe                        (string userId);

        Task<bool>              AddFriend                       (string friendId, CancellationToken token = default(CancellationToken));
        Task<bool>              UnFriend                        (string friendId, CancellationToken token = default(CancellationToken));
        Task<IList<IUser>>      GetMyFriends                    (CancellationToken token = default(CancellationToken));
        Task<IList<IUser>>      GetFriendsOf                    (string userId, CancellationToken token = default(CancellationToken));
        Task<IList<IUserGroup>> GetGroupsOf                     (string userId, CancellationToken token = default(CancellationToken));


        void Signout();
    }
}

