﻿using System;

namespace YuFit.Core.Interfaces.Services
{
	public interface IStringLoaderService
	{
        string GetString(string baseString);
        string GetString(string baseString, string table);
	}
}

