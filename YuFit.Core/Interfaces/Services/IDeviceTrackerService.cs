﻿using System.Threading.Tasks;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Interfaces.Services
{
    /// <summary>
    /// Takes care of keeping the device notification token in sync
    /// with the server for push notification.
    /// </summary>
    public interface IDeviceTrackerService
    {
        void SetNotificationToken(DeviceOs os, string tokenId);
        Task<bool> PurgeDeviceInfo();
    }
}

