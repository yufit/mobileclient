﻿using System.Threading.Tasks;
using System.Collections.Generic;
using YuFit.WebServices.Interface.Model;
using System.IO;
using YuFit.Core.Models;
using System.Threading;

namespace YuFit.Core.Interfaces.Services
{
    public interface IGroupService
    {
        Task<IList<IUserGroup>>     GetAllGroups            (CancellationToken token = default(CancellationToken));
        Task<IList<IUserGroup>>     GetMyGroups             (CancellationToken token = default(CancellationToken));

        Task<IUserGroup>            GetGroup                (string groupId, CancellationToken token = default(CancellationToken));
        Task<IUserGroup>            CreateGroup             (IUserGroup group, CancellationToken token = default(CancellationToken));
        Task<IUserGroup>            UpdateGroup             (IUserGroup group, CancellationToken token = default(CancellationToken));
        Task                        DeleteGroup             (string groupId, CancellationToken token = default(CancellationToken));

        Task<IUserGroup>            UpdateGroupImage        (IUserGroup group, string contentType, Stream image, CancellationToken token = default(CancellationToken));
        Task<IUserGroup>            UpdateAvatarImage       (IUserGroup group, string contentType, Stream image, CancellationToken token = default(CancellationToken));

        Task<IList<IUserGroup>>     SearchGroup             (string query, int pageNum, int pageSize, CancellationToken token = default(CancellationToken));

        Task<IList<IGroupMember>>   GetMembers              (string groupId, CancellationToken token = default(CancellationToken));
        Task                        SetMemberAdminStatus    (string groupId, string memberId, bool isAdmin, CancellationToken token = default(CancellationToken));
        Task                        JoinGroup               (string groupId, CancellationToken token = default(CancellationToken));
        Task                        LeaveGroup              (string groupId, string memberId, CancellationToken token = default(CancellationToken));

        Task<IUserGroup>            InviteUserToGroup       (string groupId, IList<string> usersId, CancellationToken token = default(CancellationToken));
        Task                        RemoveUserFromGroup     (string groupId, string memberId, CancellationToken token = default(CancellationToken));

        Task<List<FitActivity>>     MyUpcomingActivities    (string groupId, CancellationToken token = default(CancellationToken));
    }
}

