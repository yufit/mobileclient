﻿using System;

namespace YuFit.Core.Interfaces.Services
{
    public interface IBrowserTask
    {
        void ShowWebPage(string url);
    }
}

