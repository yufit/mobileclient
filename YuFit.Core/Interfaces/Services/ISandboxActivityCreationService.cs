﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using YuFit.Core.Models;
using YuFit.Core.Models.Activities;

namespace YuFit.Core.Interfaces.Services
{
    public interface ISandboxActivityCreationService
    {
        FitActivity SandboxedActivity { get; }
        void Update(Location where);
        void Update(ActivitySettings activitySettings);
        void Update(EventSchedule schedule);
        void Update(List<FitActivityParticipant> who);
        void Update(string what);
        void ResetCurrentActivity ();

        Task<bool> SaveCurrentSandboxedActivity (CancellationToken token = default(CancellationToken));
        Task BeginEditingActivity(string activityId, CancellationToken token = default(CancellationToken));
        Task DeleteCurrentActivity (CancellationToken token = default(CancellationToken));
    }
}

