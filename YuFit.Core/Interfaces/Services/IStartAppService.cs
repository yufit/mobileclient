﻿using System;

namespace YuFit.Core.Interfaces.Services
{
    public interface IStartAppService
    {
        void OpenUrl(Uri uri);
    }
}

