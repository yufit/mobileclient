﻿using System;
using YuFit.Core.ViewModels;

namespace YuFit.Core.Interfaces
{
    public interface IContextCommand1and2
    {
        CommandViewModel ContextCommand1 { get; }
        CommandViewModel ContextCommand2 { get; }
    }
}

