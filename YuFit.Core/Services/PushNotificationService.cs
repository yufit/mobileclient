﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;

using YuFit.WebServices.Interface;
using YuFit.WebServices.Interface.WebService;
using System.Threading;

namespace YuFit.Core.Services
{
    public abstract class PushNotificationService : IPushNotificationService
    {
        private readonly INotificationWebService _notificationEndpoint;

        protected PushNotificationService ()
        {
            _notificationEndpoint = Mvx.Resolve<IYuFitClient>().NotificationService;
        }

        #region IPushNotificationService implementation

        public string ActivityIdToDisplay { get; set; }

        public async Task<IList<NotificationRecord>> GetNotifications(CancellationToken token = default(CancellationToken))
        {
            var userService = Mvx.Resolve<IUserService>();
            var loggedInUser = userService.LoggedInUser;
            if (loggedInUser == null) { throw new Exception("user is not logged in"); }

            var notifications = await _notificationEndpoint.GetNotificationsForUser(loggedInUser.Id, 0, 100);
            var castedNotifications = notifications.Cast<NotificationRecord>().ToList();
            castedNotifications.ForEach(notification => notification.PushNotification = CreatePlatformNotificationFromNotificationRecord(notification));
            return castedNotifications;
        }

        public async Task<bool> AckNotification(string notificationId, CancellationToken token = default(CancellationToken))
        {
            var userService = Mvx.Resolve<IUserService>();
            var loggedInUser = userService.LoggedInUser;
            if (loggedInUser == null) { throw new Exception("user is not logged in"); }

            return await _notificationEndpoint.AckNotificationForUser(loggedInUser.Id, notificationId);
        }

        public async Task<bool> AckAllNotifications(CancellationToken token = default(CancellationToken))
        {
            var userService = Mvx.Resolve<IUserService>();
            var loggedInUser = userService.LoggedInUser;
            if (loggedInUser == null) { throw new Exception("user is not logged in"); }

            return await _notificationEndpoint.AckAllNotificationsForUser(loggedInUser.Id);
        }

        public virtual void SetupNotificationsHandler ()
        {
            throw new NotImplementedException();
        }

        #endregion

        protected abstract IPushNotification CreatePlatformNotificationFromNotificationRecord(NotificationRecord notification);
    }
}
