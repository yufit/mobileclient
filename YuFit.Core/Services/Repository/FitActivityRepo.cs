﻿using System.Collections.Generic;
using System.Linq;


using YuFit.Core.Messages;
using YuFit.Core.Models;
using YuFit.Core.Models.Activities;

using YuFit.WebServices.Interface.Model;
using MvvmCross.Plugins.Messenger;
using MvvmCross.Platform;

namespace YuFit.Core.Services.Repository
{
    public class FitActivityRepo
    {
        private FitActivity _sandboxActivity;

            private readonly IMvxMessenger _messenger;

        public FitActivity SandboxedActivity { get { return _sandboxActivity; } }

        public FitActivityRepo ()
        {
            _sandboxActivity = new FitActivity();
            _messenger = Mvx.Resolve<IMvxMessenger>();
        }

        #region Sandboxed Activity

        public bool IsSandboxCreating()
        {
            if (_sandboxActivity == null) { return false; }
            return string.IsNullOrEmpty(_sandboxActivity.Id);
        }

        public void LoadActivityInSandbox (FitActivity activity)
        {
            _sandboxActivity = activity;
        }

        public void Update (Location where)
        {
            _sandboxActivity.Location = where;
            _messenger.Publish(new SandboxedActivityUpdatedMessage(this, SandboxedActivity));
        }

        public void Update (EventSchedule schedule)
        {
            _sandboxActivity.EventSchedule = schedule;
            _messenger.Publish(new SandboxedActivityUpdatedMessage(this, SandboxedActivity));
        }

        public void Update (List<FitActivityParticipant> who)
        {
            _sandboxActivity.Participants = who.Cast<IFitEventParticipant>().ToList();
            _messenger.Publish(new SandboxedActivityUpdatedMessage(this, SandboxedActivity));
        }

        public void Update (string what)
        {
            _sandboxActivity.Sport = what;
            _messenger.Publish(new SandboxedActivityUpdatedMessage(this, SandboxedActivity));
        }

        public void Update (ActivitySettings activitySettings)
        {
            _sandboxActivity.ActivitySettings = activitySettings;
            _messenger.Publish(new SandboxedActivityUpdatedMessage(this, SandboxedActivity));
        }

        public void ResetSandboxedActivity ()
        {
            _sandboxActivity = new FitActivity();
            _messenger.Publish(new SandboxedActivityUpdatedMessage(this, SandboxedActivity));
        }

        #endregion
    }
}

