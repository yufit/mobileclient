﻿using System.Collections.Generic;
using System.Threading.Tasks;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;

using YuFit.WebServices.Interface;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;
using YuFit.Core.Models;
using System.Linq;
using System.Threading;

namespace YuFit.Core.Services
{
    public class GroupService : IGroupService
    {
//        private IList<IUserGroup> _groups = new [] 
//        {
//            new Group { Description = "This is a description for Group 1", Id = "0", Name = "Group One", Location = "Rochester, NY", Type = "Group Type One", Website = new Uri("http://www.yufit.com"), ManagedBy = "Frank Caico" },
//            new Group { Description = "This is a description for Group 2", Id = "1", Name = "Group Two", Location = "Rochester, NY", Type = "Group Type Two", Website = new Uri("http://www.yufit.com"), ManagedBy = "Pat LaPlante" },
//            new Group { Description = "This is a description for Group 3", Id = "2", Name = "Group Three", Location = "Rochester, NY", Type = "Group Type Three", Website = new Uri("http://www.yufit.com"), ManagedBy = "Frank Caico, Pat LaPlante"  }
//        };
//
        #region Data members

        private readonly IUserGroupWebService _userGroupWebService;
        private readonly IUserGroupMemberWebService _userGroupMemberWebService;
        //private readonly IYuFitSession _session;

        #endregion


        public GroupService (IYuFitClient client)
        {
            //_session = client.Session;
            _userGroupWebService = client.UserGroupService;
            _userGroupMemberWebService = client.UserGroupMemberService;
        }

        public async Task<IList<IUserGroup>> GetAllGroups (CancellationToken token = default(CancellationToken))
        {
            IList<IUserGroup> groups = await _userGroupWebService.GetGroups(0, 100);
            return groups;
        }

        public async Task<IList<IUserGroup>> GetMyGroups (CancellationToken token = default(CancellationToken))
        {
            return await _userGroupWebService.GetMyGroups(0, 100);
        }

        public async Task<IUserGroup> GetGroup(string groupId, CancellationToken token = default(CancellationToken))
        {
            return await _userGroupWebService.GetGroup(groupId);
        }

        public async Task<IUserGroup> CreateGroup (IUserGroup group, CancellationToken token = default(CancellationToken))
        {
            group.Id = null;
            IUserGroup newGroup =  await _userGroupWebService.CreateGroup(group);
            Mvx.Resolve<IMvxMessenger>().Publish(new GroupCreatedMessage(this, newGroup));
            return newGroup;
        }

        public async Task<IUserGroup> UpdateGroup (IUserGroup group, CancellationToken token = default(CancellationToken))
        {
            IUserGroup updatedGroup = await _userGroupWebService.UpdateGroup(group);
            Mvx.Resolve<IMvxMessenger>().Publish(new GroupUpdatedMessage(this, updatedGroup));
            return updatedGroup;
        }

        public async Task DeleteGroup (string groupid, CancellationToken token = default(CancellationToken))
        {
            await _userGroupWebService.DeleteGroup(groupid);
            Mvx.Resolve<IMvxMessenger>().Publish(new GroupDeletedMessage(this, groupid));
        }

        public async Task<IUserGroup> UpdateGroupImage (IUserGroup group, string contentType, System.IO.Stream image, CancellationToken token = default(CancellationToken))
        {
            IUserGroup updatedGroup = await _userGroupWebService.UpdateGroupImage(group, "", image);
            Mvx.Resolve<IMvxMessenger>().Publish(new GroupUpdatedMessage(this, updatedGroup));
            return updatedGroup;
        }

        public async Task<IUserGroup> UpdateAvatarImage (IUserGroup group, string contentType, System.IO.Stream image, CancellationToken token = default(CancellationToken))
        {
            IUserGroup updatedGroup = await _userGroupWebService.UpdateAvatarImage(group, "", image);
            Mvx.Resolve<IMvxMessenger>().Publish(new GroupUpdatedMessage(this, updatedGroup));
            return updatedGroup;
        }

        public async Task<IList<IUserGroup>> SearchGroup (string query, int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            return await _userGroupWebService.SearchGroup(query, pageNum, pageSize);
        }

        public async Task<IList<IGroupMember>> GetMembers (string groupId, CancellationToken token = default(CancellationToken))
        {
            IList<IGroupMember> members = await _userGroupMemberWebService.GetGroupMembers(groupId, 0, 100);
            return members;
        }

        public async Task SetMemberAdminStatus (string groupId, string memberId, bool isAdmin, CancellationToken token = default(CancellationToken))
        {
            await _userGroupMemberWebService.SetMemberAdminStatus(groupId, memberId, isAdmin);
            // fire notification
        }

        public async Task JoinGroup (string groupId, CancellationToken token = default(CancellationToken))
        {
            await _userGroupWebService.JoinGroup(groupId);
            var updatedGroup = await _userGroupWebService.GetGroup(groupId);
            Mvx.Resolve<IMvxMessenger>().Publish(new JoinedGroupMessage(this, updatedGroup));
        }

        public async Task LeaveGroup (string groupId, string memberId, CancellationToken token = default(CancellationToken))
        {
            await _userGroupWebService.LeaveGroup(groupId, memberId);
            var updatedGroup = await _userGroupWebService.GetGroup(groupId);
            Mvx.Resolve<IMvxMessenger>().Publish(new LeftGroupMessage(this, updatedGroup));
        }

        public async Task<IUserGroup> InviteUserToGroup(string groupId, IList<string> usersId, CancellationToken token = default(CancellationToken))
        {
            return await _userGroupWebService.InviteUserToGroup(groupId, usersId);
        }

        public async Task RemoveUserFromGroup (string groupId, string memberId, CancellationToken token = default(CancellationToken))
        {
            await _userGroupWebService.RemoveUserFromGroup(groupId, memberId);
        }

        public async Task<List<FitActivity>> MyUpcomingActivities(string groupId, CancellationToken token = default(CancellationToken))
        {
            IList<IFitEvent> wsFitEvents = await _userGroupWebService.UpcomingEvents(groupId, 0, 100);
            List<FitActivity> activities = wsFitEvents.Cast<FitActivity>().ToList();
            return activities;
        }

    }
}

