﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Models;
using YuFit.Core.Models.Activities;
using YuFit.Core.Services.Repository;

using YuFit.WebServices.Interface;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;
using Newtonsoft.Json;
using MvvmCross.Platform;
using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.Services
{
    public class FitActivityService : IFitActivityService, ISandboxActivityCreationService
    {
        const string eventTypeJSON = "[{\"Colors\":{\"Base\":{\"Blue\":46,\"Green\":56,\"Red\":223},\"Dark\":{\"Blue\":29,\"Green\":40,\"Red\":166},\"Light\":{\"Blue\":69,\"Green\":104,\"Red\":233}},\"EventKinds\":[\"street\",\"trail\",\"obstacles\",\"track\",\"extreme\"],\"IconName\":\"Running\",\"Kind\":\"Running\",\"Name\":\"Running\"},{\"Colors\":{\"Base\":{\"Blue\":0,\"Green\":203,\"Red\":248},\"Dark\":{\"Blue\":4,\"Green\":175,\"Red\":213},\"Light\":{\"Blue\":88,\"Green\":215,\"Red\":246}},\"EventKinds\":[\"road\",\"track\",\"cross_country\",\"downhill\",\"fatbike\",\"bmx\",\"endurance\",\"raid\",\"grandfond\",\"everyone\"],\"IconName\":\"Cycling\",\"Kind\":\"Cycling\",\"Name\":\"Cycling\"},{\"Colors\":{\"Base\":{\"Blue\":3,\"Green\":128,\"Red\":233},\"Dark\":{\"Blue\":8,\"Green\":100,\"Red\":181},\"Light\":{\"Blue\":43,\"Green\":147,\"Red\":236}},\"EventKinds\":[\"crossfit\",\"bootcamp\",\"crosstraining\"],\"IconName\":\"Outdoor\",\"Kind\":\"Outdoor\",\"Name\":\"Outdoor\"},{\"Colors\":{\"Base\":{\"Blue\":141,\"Green\":110,\"Red\":1},\"Dark\":{\"Blue\":112,\"Green\":88,\"Red\":4},\"Light\":{\"Blue\":161,\"Green\":137,\"Red\":48}},\"EventKinds\":[\"crosscountry_ski\",\"crosscountry_ski_skate\",\"snow_showing\",\"downhill\",\"fatbike\"],\"IconName\":\"Winter\",\"Kind\":\"Winter\",\"Name\":\"Winter\"},{\"Colors\":{\"Base\":{\"Blue\":0,\"Green\":155,\"Red\":134},\"Dark\":{\"Blue\":7,\"Green\":122,\"Red\":105},\"Light\":{\"Blue\":41,\"Green\":170,\"Red\":155}},\"EventKinds\":[\"duathlon\",\"pentathlon\",\"aquathlon\",\"triathlon\"],\"IconName\":\"Triathlon\",\"Kind\":\"Triathlon\",\"Name\":\"Triathlon\"}]";
        List<EventCategory> _eventCategories;

        #region Data members

        private readonly IFitEventWebService _fitEventEndpoint;
        private readonly IFitEventCommentWebService _fitEventCommentEndpoint;
        private readonly FitActivityRepo _fitActivityRepo = new FitActivityRepo();

        #endregion

        #region Construction/Initialization

        public FitActivityService ()
        {
            _fitEventEndpoint = Mvx.Resolve<IYuFitClient>().FitEventService;
            _fitEventCommentEndpoint = Mvx.Resolve<IYuFitClient>().FitEventCommentService;

            // TODO Get rid of this index if we can.  It is useless and was put there as a test.
            int i = 0;
            AvailableActivities = new List<ActivityType>();
            AvailableActivities.Add(new ActivityType { TypeName = "Running"  , Index = i++, Enabled = true  });
            AvailableActivities.Add(new ActivityType { TypeName = "Yoga"     , Index = i++, Enabled = true  });
            AvailableActivities.Add(new ActivityType { TypeName = "Cycling"  , Index = i++, Enabled = true  });
            AvailableActivities.Add(new ActivityType { TypeName = "Swimming" , Index = i++, Enabled = true  });
            AvailableActivities.Add(new ActivityType { TypeName = "Mountains", Index = i++, Enabled = true  });
            AvailableActivities.Add(new ActivityType { TypeName = "Fitness"  , Index = i++, Enabled = true  });
            AvailableActivities.Add(new ActivityType { TypeName = "Team"     , Index = i++, Enabled = false });
        }

        #endregion

        #region ISandboxActivityCreationService implementation

        public FitActivity SandboxedActivity { get { return _fitActivityRepo.SandboxedActivity; } }
        public void Update (Location where                      ) { _fitActivityRepo.Update(where);     }
        public void Update (EventSchedule schedule              ) { _fitActivityRepo.Update(schedule);  }
        public void Update (List<FitActivityParticipant> who    ) { _fitActivityRepo.Update(who);       }
        public void Update (string what                         ) { _fitActivityRepo.Update(what);      }
        public void Update (ActivitySettings activitySettings   ) { _fitActivityRepo.Update(activitySettings);}

        public async Task<bool> SaveCurrentSandboxedActivity(CancellationToken token = default(CancellationToken)) 
        {
            IFitActivityService fitActivityService = Mvx.Resolve<IFitActivityService>();
            if (_fitActivityRepo.IsSandboxCreating()) {
                await fitActivityService.CreateActivity(_fitActivityRepo.SandboxedActivity, token);
            } else {
                await fitActivityService.UpdateActivity(_fitActivityRepo.SandboxedActivity, token);
            }
            _fitActivityRepo.ResetSandboxedActivity();
            return true;
        }

        public void ResetCurrentActivity () {
            _fitActivityRepo.ResetSandboxedActivity();
        }

        public async Task BeginEditingActivity(string activityId, CancellationToken token = default(CancellationToken)) {
            _fitActivityRepo.LoadActivityInSandbox(await GetActivity(activityId, token));
        }

        public async Task DeleteCurrentActivity (CancellationToken token = default(CancellationToken)) {
            if (!_fitActivityRepo.IsSandboxCreating()) {
                await DeleteActivity(_fitActivityRepo.SandboxedActivity);
            }
            _fitActivityRepo.ResetSandboxedActivity();
        }

        #endregion

        #region IActivityEventService implementation

        public List<EventCategory> GetEventCategories ()
        {
            if (_eventCategories == null) {
                _eventCategories =  JsonConvert.DeserializeObject<List<EventCategory>>(eventTypeJSON);
            }
            return _eventCategories;
        }

        public List<ActivityType> AvailableActivities { get; private set; }

        #pragma warning disable 1998
        /// <summary>
        /// Gets all activities.  Be careful with this as it returns everything
        /// in the database!  I could take forever.
        /// </summary>
        /// <returns>The all activities.</returns>
        public async Task<List<FitActivity>> GetAllActivities (CancellationToken token = default(CancellationToken))
        {
            IList<IFitEvent> wsFitEvents = await _fitEventEndpoint.GetAllEvents(0, 100);
            List<FitActivity> activities = wsFitEvents.Cast<FitActivity>().ToList();
            return activities;
        }

        /// <summary>
        /// Gets the activity.
        /// </summary>
        /// <returns>The activity.</returns>
        /// <param name="activityId">Activity identifier.</param>
        /// <param name = "token"></param>
        public async Task<FitActivity> GetActivity (string activityId, CancellationToken token = default(CancellationToken))
        {
            var objectId = activityId;
            IFitEvent result = await _fitEventEndpoint.GetEvent(objectId);
            return result as FitActivity;
        }

        /// <summary>
        /// Creates the activity.
        /// </summary>
        /// <returns>The activity.</returns>
        /// <param name="fitActivity">Fit activity.</param>
        /// <param name = "token"></param>
        public async Task<FitActivity> CreateActivity (FitActivity fitActivity, CancellationToken token = default(CancellationToken))
        {
            fitActivity.Id = null;
            fitActivity.Normalize();
            IFitEvent wsFitEvent = await _fitEventEndpoint.CreateEvent(fitActivity);

            await SetGoing(wsFitEvent.Id, true);

            var addedActivity = wsFitEvent as FitActivity;
            var message = new EventCreatedMessage(this, addedActivity);
            Mvx.Resolve<IMvxMessenger>().Publish(message);

            return addedActivity;
        }

        /// <summary>
        /// Updates the activity.
        /// </summary>
        /// <returns>The activity.</returns>
        /// <param name="fitActivity">Fit activity.</param>
        /// <param name = "token"></param>
        public async Task<FitActivity> UpdateActivity (FitActivity fitActivity, CancellationToken token = default(CancellationToken))
        {
            fitActivity.Normalize();
            IFitEvent wsFitEvent = await _fitEventEndpoint.UpdateEvent(fitActivity);

            var updatedActivity = wsFitEvent as FitActivity;
            var message = new EventUpdatedMessage(this, updatedActivity);
            Mvx.Resolve<IMvxMessenger>().Publish(message);
            return updatedActivity;
        }

        /// <summary>
        /// Deletes the activity.
        /// </summary>
        /// <returns>The activity.</returns>
        /// <param name="fitActivity">Fit activity.</param>
        /// <param name = "token"></param>
        public async Task DeleteActivity (FitActivity fitActivity, CancellationToken token = default(CancellationToken))
        {
            await _fitEventEndpoint.DeleteEvent(fitActivity);
            var message = new EventDeletedMessage(this, fitActivity);
            Mvx.Resolve<IMvxMessenger>().Publish(message);
        }

        /// <summary>
        /// Searchs for activities.
        /// </summary>
        /// <returns>The for activities.</returns>
        /// <param name="coordinates">Coordinates.</param>
        /// <param name="range">Range.</param>
        /// <param name = "token"></param>
        public async Task<List<FitActivity>> SearchForActivities (
            Coordinate coordinates, 
            double range = 10.0,
            CancellationToken token = default(CancellationToken)
        ) {
            IList<IFitEvent> wsFitEvents = await _fitEventEndpoint.SearchForEvents(0, 100, coordinates, range, token);
            List<FitActivity> activities = wsFitEvents.Cast<FitActivity>().ToList();
            return activities;
        }

        public async Task<List<FitActivity>> MyUpcomingActivities(int limit = -1, CancellationToken token = default(CancellationToken))
        {
            IList<IFitEvent> wsFitEvents = await _fitEventEndpoint.MyUpcomingEvents(0, limit!=-1?limit:100);
            List<FitActivity> activities = wsFitEvents.Cast<FitActivity>().ToList();
            return activities;
        }

        public async Task<List<FitActivity>> MyInvitedActivities(CancellationToken token = default(CancellationToken))
        {
            IList<IFitEvent> wsFitEvents = await _fitEventEndpoint.MyInvitedEvents(0, 100);
            List<FitActivity> activities = wsFitEvents.Cast<FitActivity>().ToList();
            return activities;
        }

        public async Task<List<FitActivity>> MyCreatedActivities(CancellationToken token = default(CancellationToken))
        {
            IList<IFitEvent> wsFitEvents = await _fitEventEndpoint.MyCreatedEvents(0, 100);
            List<FitActivity> activities = wsFitEvents.Cast<FitActivity>().ToList();
            return activities;
        }

        public async Task SetGoing(string id, bool going)
        {
            await _fitEventEndpoint.SetGoing(id, going);
            Mvx.Resolve<IMvxMessenger>().Publish(new EventRemovedFromInvitationMessage(this));
        }

        public async Task<List<FitActivityComment>> GetActivityComments(string activityId, CancellationToken token = default(CancellationToken))
        {
            var comments = await _fitEventCommentEndpoint.GetComments(new FitActivity { 
                Id = activityId
            }, 0, 100);
            return comments.Cast<FitActivityComment>().ToList();
        }

        public async Task<FitActivityComment> AddComment (string activityId, string comment, CancellationToken token = default(CancellationToken))
        {
            var addedComment = await _fitEventCommentEndpoint.AddComment(new FitActivity { 
                Id = activityId
            }, new FitActivityComment {
                Comment = comment
            });
            Mvx.Resolve<IMvxMessenger>().Publish(new EventCommentAddedMessage(this, activityId, (FitActivityComment) addedComment));
            return (FitActivityComment) addedComment;
        }

        public async Task<bool> DeleteComment (string activityId, string commentId, CancellationToken token = default(CancellationToken))
        {
            await _fitEventCommentEndpoint.DeleteComment(new FitActivity { 
                Id = activityId
            }, new FitActivityComment {
                Id = commentId
            });

            Mvx.Resolve<IMvxMessenger>().Publish(new EventCommentDeletedMessage(this, activityId, commentId));
            return true;
        }


        #pragma warning restore 1998

        #endregion

        #region Methods

        #endregion
    }
}

