﻿using YuFit.Core.Interfaces.Services;
using System.Collections.Generic;

namespace YuFit.Core.Services
{
    public class SandboxService : ISandboxService
    {
        readonly Dictionary<string, object> _sandboxItems = new Dictionary<string, object>();

        #region ISandboxService implementation

        public void SetItem<T> (string key, T item)
        {
            _sandboxItems[key] = item;
        }

        public T GetItem<T> (string key)
        {
            T result = default(T);
            object value;
            if (_sandboxItems.TryGetValue(key, out value))
            {
                result = (T) value;
            }

            return result;
        }

        #endregion
    }
}

