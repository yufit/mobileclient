﻿
namespace YuFit.Core.Services.Notifications
{
    public class EventPromotedNotification : BaseEventPushNotification
    {
        public string EventIconUrl  { get { return GetStringValueForKey("event_icon_url"); } }

        public EventPromotedNotification () 
        {
            Category = PushNotificationCategory.EventPromoted;
        }
    }

}
