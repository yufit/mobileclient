﻿using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.Services.Notifications
{
    public class BaseGroupPushNotification : BasePushNotification
    {
        public string GroupId           { get { return GetStringValueForKey("user_group_id");          } }
        public string GroupName         { get { return GetStringValueForKey("user_group_name");        } }
        public string UserId            { get { return GetStringValueForKey("acting_user_id");         } }
        public string UserName          { get { return GetStringValueForKey("acting_user_name");       } }
        public string GroupAvatarUrl    { get { return GetStringValueForKey("user_group_avatar_url");  } }

        public override void HandleReceivedNotificationWhileRunning (System.Action<PushNotificationResult> completionHandler)
        {
            if (AppIsInForeground) { 
                base.HandleReceivedNotificationWhileRunning(completionHandler);
            } else {
                var message = new DisplayGroupMessage(this, GroupId);
                Mvx.Resolve<IMvxMessenger>().Publish(message);
            }
            completionHandler(PushNotificationResult.NewData);
        }

    }
}

