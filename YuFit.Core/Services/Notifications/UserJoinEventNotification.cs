
namespace YuFit.Core.Services.Notifications
{
    public class UserJoinEventNotification : BaseEventPushNotification
    {
        public UserJoinEventNotification () 
        {
            Category = PushNotificationCategory.UserJoined;
        }
    }

}
