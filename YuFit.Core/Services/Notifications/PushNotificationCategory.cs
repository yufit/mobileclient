using YuFit.Core.Attributes;
using System;
using System.Reflection;

namespace YuFit.Core.Services.Notifications
{
    public enum PushNotificationCategory {
        [StringValue("invitation")]             Invitation,
        [StringValue("user_left_event")]        UserLeft,
        [StringValue("user_joined_event")]      UserJoined,
        [StringValue("event_modified")]         EventModified,
        [StringValue("event_canceled")]         EventCancelled,
        [StringValue("event_comment_added")]    EventCommentPosted,
        [StringValue("event_promoted")]         EventPromoted,
        [StringValue("general_message")]        GeneralMessage,
        [StringValue("user_joined_group")]      UserJoinedGroup,
        [StringValue("user_invited_to_group")]  UserInvitedToGroup,
        [StringValue("friend_added")]           FriendAdded,
        [StringValue("friend_joined")]          FriendJoined,

        [StringValue("unknown")]                Unknown
    }

    public static class PushNotificationCategoryEx
    {
        public static PushNotificationCategory GetValueFromStringValue(string description)
        {
            foreach (PushNotificationCategory val in Enum.GetValues(typeof(PushNotificationCategory)))
            {
                Type t = val.GetType();
                FieldInfo fieldInfo = t.GetRuntimeField(val.ToString());

                // Get the stringvalue attributes
                StringValueAttribute[] attributes = fieldInfo.GetCustomAttributes(
                    typeof(StringValueAttribute), false) as StringValueAttribute[];

                if (attributes != null && attributes.Length > 0) {
                    var attribute = attributes[0];
                    if (attribute != null) {
                        if (attribute.StringValue == description) {
                            return val;
                        }
                    }
                }
            }

            return PushNotificationCategory.Unknown;
        }
    }
}
