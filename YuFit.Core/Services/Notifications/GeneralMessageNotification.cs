﻿namespace YuFit.Core.Services.Notifications
{
    public class GeneralMessageNotification : BasePushNotification
    {
        public GeneralMessageNotification () 
        {
            Category = PushNotificationCategory.GeneralMessage;
        }
    }
}

