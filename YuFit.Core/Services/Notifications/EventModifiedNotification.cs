
namespace YuFit.Core.Services.Notifications
{
    public class EventModifiedNotification : BaseEventPushNotification
    {
        public EventModifiedNotification () 
        {
            Category = PushNotificationCategory.EventModified;
        }
    }

}
