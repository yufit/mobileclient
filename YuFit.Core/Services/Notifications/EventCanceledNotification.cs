namespace YuFit.Core.Services.Notifications
{
    public class EventCanceledNotification : BaseEventPushNotification
    {
        public EventCanceledNotification () 
        {
            Category = PushNotificationCategory.EventCancelled;
        }
    }

}
