

namespace YuFit.Core.Services.Notifications
{
    public class UserLeftEventNotification : BaseEventPushNotification
    {
        public UserLeftEventNotification () 
        {
            Category = PushNotificationCategory.UserLeft;
        }
    }

}
