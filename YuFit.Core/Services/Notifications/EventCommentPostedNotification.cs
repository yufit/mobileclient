﻿using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using YuFit.Core.Messages;
using YuFit.Core.Interfaces.Services;

namespace YuFit.Core.Services.Notifications
{
    public class EventCommentPostedNotification : BaseEventPushNotification
    {
        public EventCommentPostedNotification () 
        {
            Category = PushNotificationCategory.EventCommentPosted;
        }

        public override void HandleReceivedNotificationWhileRunning (System.Action<PushNotificationResult> completionHandler)
        {
            if (AppIsInForeground) {
                base.HandleReceivedNotificationWhileRunning(completionHandler);
            } else {
                var message = new DisplayDetailActivityMessage(this, EventId);
                Mvx.Resolve<IMvxMessenger>().Publish(message);
            }
        }
    }
}

