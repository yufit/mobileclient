﻿using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.Services.Notifications
{
    public class BaseFriendPushNotification : BasePushNotification
    {
        public string UserId            { get { return GetStringValueForKey("user_id");         } }
        public string UserName          { get { return GetStringValueForKey("user_name");       } }
        public string UserAvatarUrl     { get { return GetStringValueForKey("user_avatar_url"); } }

        public override void HandleReceivedNotificationWhileRunning (System.Action<PushNotificationResult> completionHandler)
        {
            if (AppIsInForeground) { 
                base.HandleReceivedNotificationWhileRunning(completionHandler);
            } else {
                var message = new DisplayUserProfileMessage(this, UserId);
                Mvx.Resolve<IMvxMessenger>().Publish(message);
            }
            completionHandler(PushNotificationResult.NewData);
        }
    }
}

