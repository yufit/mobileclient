﻿using System;

namespace YuFit.Core.Services.Notifications
{
    public class FriendJoinedNotification : BaseFriendPushNotification
    {
        public FriendJoinedNotification ()
        {
            Category = PushNotificationCategory.FriendAdded;
        }
    }
}

