using System;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Interfaces.Services;

namespace YuFit.Core.Services.Notifications
{

    public abstract class InvitedToEventNotification : BaseEventPushNotification
    {
        const string inviteJoinAction = @"Join";
        const string inviteDismissAction = @"Dismiss";

        protected InvitedToEventNotification () 
        {
            Category = PushNotificationCategory.Invitation;
        }

        public override async void HandleNotificationAction(string actionIdentifier, System.Action completionHandler) 
        {
            var fitActivityService = Mvx.Resolve<IFitActivityService>();

            try {
                if (actionIdentifier == inviteJoinAction) { 
                    await fitActivityService.SetGoing(EventId, true); 
                }
                else if (actionIdentifier == inviteDismissAction) { 
                    await fitActivityService.SetGoing(EventId, false); 
                }
            } catch (Exception) {
                
            } finally {
                completionHandler();
            }
        }
    }

}
