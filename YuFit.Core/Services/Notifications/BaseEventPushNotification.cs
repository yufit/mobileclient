﻿using System;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;

namespace YuFit.Core.Services.Notifications
{
    public class BaseEventPushNotification : BasePushNotification
    {
        public string EventId           { get { return GetStringValueForKey("event_id");           } }
        public string Sport             { get { return GetStringValueForKey("sport");              } }
        public string SubSport          { get { return GetStringValueForKey("sub_sport");           } }
        public string CreatorAvatarUrl  { get { return GetStringValueForKey("creator_avatar_url"); } }

        public BaseEventPushNotification ()
        {
        }

        public override void HandleReceivedNotificationWhileRunning (System.Action<PushNotificationResult> completionHandler)
        {
            if (AppIsInForeground) {
                base.HandleReceivedNotificationWhileRunning(completionHandler);
            } else {
                var message = new InvitedToActivityMessage(this, EventId, !AppIsInForeground);
                Mvx.Resolve<IMvxMessenger>().Publish(message);
            }
            completionHandler(PushNotificationResult.NewData);
        }

        public override void HandleReceivedNotificationWhileStarting()
        {
            Mvx.Trace("++++++++++++++++++++++++++ Storing activityid to display in sandbox");
            Mvx.Resolve<ISandboxService>().SetItem<string>("ActivityIdToDisplay", EventId);
        }
    }
}
 
