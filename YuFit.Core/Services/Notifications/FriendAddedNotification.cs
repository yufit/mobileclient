﻿using System;

namespace YuFit.Core.Services.Notifications
{
    public class FriendAddedNotification : BaseFriendPushNotification
    {
        public FriendAddedNotification ()
        {
            Category = PushNotificationCategory.FriendAdded;
        }
    }
}

