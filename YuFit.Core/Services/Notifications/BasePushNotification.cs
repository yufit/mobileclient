﻿using System;
using System.Collections.Generic;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;

namespace YuFit.Core.Services.Notifications
{
    public class AlertInformation
    {
        public string[] Values      { get; set; }
        public string   Key         { get; set; }
        public string   AlertExtra  { get; set; }

        public string Message {
            get {
                var stringLoaderService = Mvx.Resolve<IStringLoaderService> ();

                if (!string.IsNullOrEmpty(Key) && Values != null && Values.Length > 0) {
                    var localizedString = stringLoaderService.GetString(Key + "_CSHARP");
                    return string.Format(localizedString, Values);
                }

                if (!string.IsNullOrEmpty(Key) && (Values == null || Values.Length == 0)) {
                    return stringLoaderService.GetString(Key);
                }

                return AlertExtra;
            }
        }

        public AlertInformation ()
        {
            AlertExtra = string.Empty;
        }
    }


    public abstract class BasePushNotification : IPushNotification
    {
        public int? UpdatedBadgeCount { get; set; }

        public AlertInformation AlertInformation { get; set; }
        public dynamic Info { get; set; }

        public bool AppIsInForeground { get; set; }

        public PushNotificationCategory Category { get; protected set; }
        public string Message { get { return AlertInformation != null ? AlertInformation.Message : string.Empty; } }

//        public T GetNotificationValue<T>(string key)
//        {
//            object value;
//            if (Info.TryGetValue(key, out value)) {
//                // Analysis disable once EmptyGeneralCatchClause
//                try { return (T) value; }
//                catch (Exception /*e*/) { }
//            }
//
//            return default(T);
//        }
//
//        public bool TryGetNotificationValue<T>(string key, out T value)
//        {
//            object genericValue;
//            value = default(T);
//
//            if (Info.TryGetValue(key, out genericValue)) {
//                // Analysis disable once EmptyGeneralCatchClause
//                try { 
//                    value = (T) genericValue; 
//                    return true; 
//                }
//                catch (Exception /*e*/) {
//                }
//            }
//
//            return false;
//        }

        #region IRemoteNotification implementation

        public virtual void HandleReceivedNotificationWhileRunning (Action<PushNotificationResult> completionHandler)
        {
            if (AppIsInForeground) {
                var message = new PushNotificationMessage(this, this);
                Mvx.Resolve<IMvxMessenger>().Publish(message);
            }

            completionHandler(PushNotificationResult.NewData);
        }

        public virtual void HandleReceivedNotificationWhileStarting ()
        {
        }

        public virtual void HandleNotificationAction (string actionIdentifier, Action completionHandler)
        {
            completionHandler();
        }

        #endregion

        protected string GetStringValueForKey(string key)
        {
            return Info != null && Info[key] != null ? Info[key].ToString() : string.Empty;
        }

        public virtual void SetupNotificationsHandler ()
        {
            throw new NotImplementedException();
        }
    }
}

