﻿
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using YuFit.Core.Interfaces.Services;

namespace YuFit.Core.Services.Notifications
{
    public class UserInvitedToGroupNotification : BaseGroupPushNotification
    {
        public const string InviteJoinAction = @"JoinGroup";
        public const string InviteDismissAction = @"DismissGroup";

        public UserInvitedToGroupNotification () 
        {
            Category = PushNotificationCategory.UserInvitedToGroup;
        }

        public override async void HandleNotificationAction (string actionIdentifier, System.Action completionHandler)
        {
            var groupService = Mvx.Resolve<IGroupService>();

            try {
                if (actionIdentifier == InviteJoinAction) { 
                    await groupService.JoinGroup(GroupId); 
                }
                else if (actionIdentifier == InviteDismissAction) { 
                    // no-op
                }
            } catch (System.Exception) {
                
            } finally {
                completionHandler();
            }
        }
    }
}

