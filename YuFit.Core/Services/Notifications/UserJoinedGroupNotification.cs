﻿namespace YuFit.Core.Services.Notifications
{
    public class UserJoinedGroupNotification : BaseGroupPushNotification
    {
        public UserJoinedGroupNotification () 
        {
            Category = PushNotificationCategory.UserJoinedGroup;
        }
    }
}

