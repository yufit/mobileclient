﻿using System;
using System.IO;
using System.Text;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.File;

using Newtonsoft.Json;

using YuFit.Core.Interfaces.Services;

namespace YuFit.Core.Services
{
    public class FileIoService : IFileIoService
    {
        private readonly IMvxFileStore _fileStore;
        public static object Locker = new object();

        public FileIoService(IMvxFileStore fileStore)
        {
            _fileStore = fileStore;
        }

        public string Read(string fileName)
        {
            lock (Locker)
            {
                string data = string.Empty;

                if (_fileStore.Exists(fileName))
                {
                    try
                    {
                        using(Stream reader = _fileStore.OpenRead(fileName))
                        {
                            byte[] rawContent = new byte[reader.Length];
                            reader.Read(rawContent, 0, rawContent.Length);
                            data = Encoding.UTF8.GetString(rawContent, 0, rawContent.Length);
                        }
                    }
                    catch (Exception ex)
                    {
                        Mvx.Trace("Failed to read data from local disk: {0}", ex.Message);
                        _fileStore.DeleteFile(fileName);
                    }
                }

                return data;
            }
        }


        public T Read<T>(string fileName) where T : new()
        {
            lock (Locker)
            {
                T data = new T();

                if (_fileStore.Exists(fileName))
                {
                    string fileContents;

                    // read file into a string and deserialize JSON to a type
                    if (_fileStore.TryReadTextFile(fileName, out fileContents))
                    {
                        data = JsonConvert.DeserializeObject<T>(fileContents);
                    }
                    else
                    {
                        Mvx.Trace("Failed to read data from local file system. Deleting file.");
                    }
                }

                return data;
            }
        }

        public void EnsureFileDoesNotExist(string fileName) {
            if (_fileStore.Exists(fileName)) { 
                _fileStore.DeleteFile(fileName); 
            }
        }

        public void Write(string fileName, string data)
        {
            lock (Locker)
            {
                try
                {
                    // TODO PAT ENSURE THIS WORK
                    EnsureFileDoesNotExist(fileName);
                    using(Stream writer = _fileStore.OpenWrite(fileName))
                    {
                        byte[] array = Encoding.UTF8.GetBytes(data);
                        writer.Write(array, 0, array.Length);
                        writer.Flush();
                    }
                }
                catch (Exception ex)
                {
                    Mvx.Trace("Failed to write data to local disk: {0}", ex.Message);
                }
            }
        }

        public void Write<T>(string fileName, T data)
        {
            lock (Locker)
            {
                try
                {
                    // TODO PAT ENSURE THIS WORK
                    EnsureFileDoesNotExist(fileName);
                    using(Stream fs = _fileStore.OpenWrite(fileName))
                    using(StreamWriter writer = new StreamWriter(fs))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(writer, data);
                        writer.Flush();
                    }
                }
                catch (Exception ex)
                {
                    Mvx.Trace("Failed to write data to local disk: {0}", ex.Message);
                }
            }
        }

        public void RemoveFile(string fileName)
        {
            lock (Locker)
            {
                if (_fileStore.Exists(fileName))
                {
                    _fileStore.DeleteFile(fileName);
                }
            }
        }
    }
}

