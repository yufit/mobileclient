﻿using System;
using System.Threading.Tasks;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using Xamarin;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Models;

using YuFit.WebServices.Interface;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;

namespace YuFit.Core.Services
{
    class DeviceInfoCache
    {
        public string UserId { get; set; }
        public IDeviceInformation DevInfo { get; set; }

        public bool UserIdValid { get { return !string.IsNullOrEmpty(UserId); } }

        public bool IsThisTokenNewer(string newToken) {
            if (DevInfo == null) { return true; }
            return newToken == DevInfo.NotificationToken;
        }
    }

    public class DeviceTrackerService : IDeviceTrackerService
    {
        #pragma warning disable 414
        readonly MvxSubscriptionToken   _userSignedInToken;
        readonly MvxSubscriptionToken   _userSignedOutToken;
        readonly MvxSubscriptionToken   _userInfoReloadedToken;
        #pragma warning restore 414

        readonly IMvxMessenger          _messenger;
        readonly IDeviceWebService      _deviceWebService;
        readonly IFileIoService         _fileService;

        DeviceInfoCache                 _deviceInfoCache;
        DeviceInformation               _newDeviceInfo;

        // ---------------------------------------------------------------------------------------
        /// <summary>
        /// Initializes a new instance of the <see cref="YuFit.Core.Services.DeviceTrackerService"/> class.
        /// </summary>
        /// <param name="messenger">Messenger.</param>
        /// <param name="client">Client.</param>
        /// <param name="fileService">File service.</param>
        public DeviceTrackerService (
            IMvxMessenger   messenger, 
            IYuFitClient    client,
            IFileIoService  fileService
        )
        {
            _messenger              = messenger;
            _deviceWebService       = client.DeviceWebService;
            _fileService            = fileService;

            _userSignedInToken      = _messenger.Subscribe<UserLoggedOnMessage>     (OnUserSignIn);
            _userSignedOutToken     = _messenger.Subscribe<UserLoggedOutMessage>    (OnUserSignOut);
            _userInfoReloadedToken  = _messenger.Subscribe<UserInfoReloadedMessage> (OnUserInfoReloaded);

            ReadDeviceInfo();
        }

        #region IDeviceTrackerService implementation
        // ---------------------------------------------------------------------------------------
        /// <summary>
        /// Sets the notification token.  It is assumed that the cached one already been loaded.
        /// </summary>
        /// <returns>The notification token.</returns>
        /// <param name="os">Os.</param>
        /// <param name="tokenId">Token identifier.</param>
        public void SetNotificationToken(DeviceOs os, string tokenId)
        {
            Mvx.Trace("Calling set notification with {0}, {1}", os, tokenId);

            // We may or may not be ready to send it yet.  It depend if we have refreshed the user
            // info from the server of if the user has logged in.

            // The sequence when the app load or start, is: 
            //   If a user is logged in (we have a session on disk), we always refresh its info
            //   from the server before doing anything else.

            // Scenarios are:
            //  We always get a token on app start from scratch, not from reload from mem.
            //   We get a new token back from the push server and there are no logged in user.
            //   We get a new token and the user is logged in and its info has been refreshed.
            //   We get a new token and we haven't refresh the user info from the server yet.

            var tokenIsNewer = _deviceInfoCache != null && _deviceInfoCache.IsThisTokenNewer(tokenId);
            if (tokenIsNewer) {
                _newDeviceInfo = new DeviceInformation {
                    DeviceOs = DeviceOs.IOS,
                    NotificationToken = tokenId
                };
                Mvx.Trace("Token is newer");
            }

            // If we have a user id, we are good to post the info to the server right.
            if (_deviceInfoCache.UserIdValid) {
                Mvx.Trace("User is valid when setting token");
                PushNotificationTokenToServer();
            }
        }

        // ---------------------------------------------------------------------------------------
        /// <summary>
        /// Purges the device info.  This public api must be called when logging out the user to 
        /// ensure that the device will no longer received notification.
        /// </summary>
        /// <returns>The device info.</returns>
        public async Task<bool> PurgeDeviceInfo ()
        {
            Mvx.Trace("Purging device info");

            if (_deviceInfoCache != null || _deviceInfoCache.DevInfo == null) {
                Mvx.Trace("No info to purge");
                return false;
            }

            _fileService.RemoveFile("deviceInfo");

            // We do not try/catch this, since this is called by our app and we want it to bubble
            // up.
            return await _deviceWebService.RemoveDevice(_deviceInfoCache.UserId, _deviceInfoCache.DevInfo.DeviceId);
        }
        #endregion

        // ---------------------------------------------------------------------------------------
        /// <summary>
        /// Called when a user signed in.  Since they just signed in, we must send their device
        /// notification token.  It is possible that token hasn't been received by the device yet.
        /// 
        /// We assume this is a first run for a user.  If it isn't the server should be able to
        /// handle that scenario.
        /// </summary>
        /// <param name="obj">Object.</param>
        void OnUserSignIn (UserLoggedOnMessage obj)
        {
            Mvx.Trace("User signed in message");
            _deviceInfoCache.UserId = obj.AuthenticatedUser.Id;
            PushNotificationTokenToServer();
        }

        // ---------------------------------------------------------------------------------------
        /// <summary>
        /// Raises the user info reloaded event.
        /// </summary>
        /// <param name="obj">Object.</param>
        void OnUserInfoReloaded (UserInfoReloadedMessage obj)
        {
            Mvx.Trace("User info reloaded message");
            _deviceInfoCache.UserId = obj.User.Id;
            PushNotificationTokenToServer();
        }

        // ---------------------------------------------------------------------------------------
        /// <summary>
        /// Raises the user sign out event.
        /// </summary>
        /// <param name="obj">Object.</param>
        void OnUserSignOut (UserLoggedOutMessage obj)
        {
        //    Mvx.Trace("User signed out message");
        //    _fileService.RemoveFile("deviceInfo");
        //    PurgeDeviceFromServer();
        }

        // ---------------------------------------------------------------------------------------
        /// <summary>
        /// Pushs the notification token to server if need to.
        /// </summary>
        async void PushNotificationTokenToServer ()
        {
            // We must decide what to do here.  The possible scenarios are:
            //   There was no token stored locally and we have a new token
            //      add it to the server
            //   There was no token stored locally and we have no token to send
            //      do nothing
            //   There was a token stored locally and do not have a new token
            //      do nothing
            //   There was a token stored locally and we have a new one
            //      update the stored token on the server with the new one

            bool addToken = _deviceInfoCache.DevInfo == null && _newDeviceInfo != null;
            bool updateToken = _deviceInfoCache.DevInfo != null && _newDeviceInfo != null;

            Mvx.Trace("add token {0}, update token {1}", addToken, updateToken);

            // We must try/catch this since this gets called from an outside context and we won't
            // catch the exception in our code otherwise.
            try {
                IDeviceInformation info = null;
                if (addToken) {
                    info = await _deviceWebService.AddNewDevice(_deviceInfoCache.UserId, _newDeviceInfo);
                } else if (updateToken) {
                    // _newDeviceInfo won't contain the device record id needed for the update
                    // we have to put it in.
                    _newDeviceInfo.DeviceId = _deviceInfoCache.DevInfo.DeviceId;
                    info = await _deviceWebService.UpdateDevice(_deviceInfoCache.UserId, _newDeviceInfo);
                }

                if (info != null) {
                    _deviceInfoCache.DevInfo = info;
                    _newDeviceInfo = null;
                    SaveDeviceInfo();
                }
            } catch (Exception ex) {
                Insights.Report(ex);
            }
        }

        // ---------------------------------------------------------------------------------------
        /// <summary>
        /// [private helper method]
        /// Reloads the token from store.
        /// </summary>
        void ReadDeviceInfo ()
        {
            _deviceInfoCache = _fileService.Read<DeviceInfoCache>("deviceInfo");
        }

        // ---------------------------------------------------------------------------------------
        /// <summary>
        /// [private helper method]
        /// Saves the token to store.
        /// </summary>
        void SaveDeviceInfo ()
        {
            _fileService.Write<DeviceInfoCache>("deviceInfo", _deviceInfoCache);
        }
    }
}

