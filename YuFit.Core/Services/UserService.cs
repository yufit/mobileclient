﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using Coc.MvvmCross.Plugins.Facebook;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Models;

using YuFit.WebServices.Interface;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Interface.WebService;
using YuFit.Core.Extensions;
using System.Threading;
using YuFit.Core.Utils;

namespace YuFit.Core.Services
{
    public class UserService : IUserService
    {
        readonly IYuFitClient       _yuFitClient;
        readonly IUserWebService    _userWebService;
        readonly IFriendsWebService _friendsWebService;
        readonly IMvxMessenger      _messenger;

        public User LoggedInUser { get; set; }

        IDeviceInformation _deviceInfo;
        public IDeviceInformation DeviceInfo {
            get { return _deviceInfo; }
            set { _deviceInfo = value; }//TransmitDevToken(); }
        }

        public bool NeedsAuthentication { 
            get { return Session == null || Session.NeedsAuthentication; } 
        }

        public IYuFitSession Session { get; private set; }

        public UserService (IYuFitClient client, IMvxMessenger messenger)
        {
            _yuFitClient = client;
            _messenger = messenger;

            Session = Mvx.Resolve<IYuFitClient>().Session;
            _userWebService = _yuFitClient.UserService;
            _friendsWebService = _yuFitClient.FriendsWebService;
        }

        #region IAuthenticationService implementation

        public async Task<bool> AuthenticateWithFacebook (string token, string email, CancellationToken ct = default(CancellationToken))
        {
            IAuthenticationCredentials creds = YuFit.WebServices.Model.AuthenticationCredentials.CreateWithToken(ServiceType.Facebook, token);
            LoggedInUser = null;
            Session = null;

            Session = await _yuFitClient.OpenSession(creds);
            if (Session != null && Session.AuthenticatedUser != null)
            {
                LoggedInUser = (User) await FetchUserInfo(Session.AuthenticatedUser.Id);
                IMvxMessenger messenger = Mvx.Resolve<IMvxMessenger>();
                messenger.Publish(new UserLoggedOnMessage(this, LoggedInUser));

                return true;
            }

            return false;
        }

        public async Task<bool> AuthenticateWithEmail (string email, string password, CancellationToken token = default(CancellationToken))
        {
            IAuthenticationCredentials creds = YuFit.WebServices.Model.AuthenticationCredentials.CreateWithCreds(ServiceType.Yufit, email, password);
            LoggedInUser = null;
            Session = null;

            Session = await _yuFitClient.OpenSession(creds);
            if (Session != null && Session.AuthenticatedUser != null)
            {
                LoggedInUser = (User) await FetchUserInfo(Session.AuthenticatedUser.Id);
                IMvxMessenger messenger = Mvx.Resolve<IMvxMessenger>();
                messenger.Publish(new UserLoggedOnMessage(this, LoggedInUser));

                return true;
            }

            return false;
        }

        public async Task<bool> RegisterWithEmail (string name, string email, string password, CancellationToken token = default(CancellationToken))
        {
            IAuthenticationCredentials creds = YuFit.WebServices.Model.AuthenticationCredentials.CreateWithCreds(ServiceType.Yufit, name, email, password);
            LoggedInUser = null;
            Session = null;

            Session = await _yuFitClient.OpenSession(creds);
            if (Session != null && Session.AuthenticatedUser != null)
            {
                LoggedInUser = (User) await FetchUserInfo(Session.AuthenticatedUser.Id);
                IMvxMessenger messenger = Mvx.Resolve<IMvxMessenger>();
                messenger.Publish(new UserLoggedOnMessage(this, LoggedInUser));

                return true;
            }

            return false;
        }

        public async Task<IUser> UpdateAvatarImage (IUser user, string contentType, System.IO.Stream image, CancellationToken token = default(CancellationToken))
        {
            IUser updatedUser = await _userWebService.UpdateAvatarImage(user.Id, "", image);
            Mvx.Resolve<IMvxMessenger>().Publish(new UserProfileUpdatedMessage(this, (User) updatedUser));
            return updatedUser;
        }

        public async Task<IList<IUserGroup>> GetGroups(bool refresh = true, CancellationToken token = default(CancellationToken))
        {
            if (Session == null) { throw new Exception("no session"); }

            if (refresh) {
                var groupService = Mvx.Resolve<IGroupService>();
                // Eventually we will want to do this:
                //var groups = await _userWebService.GetGroups();

                // UPDATE THE DATA STORE.
                return await groupService.GetMyGroups();
            }

            return null;
        }

        public async Task<IUser> FetchUserInfo(string id, CancellationToken token = default(CancellationToken))
        {
            IUser user = await _userWebService.GetUserInformation(id);
            user.Id  = id;
            return user;
        }

        public async Task<IUser> GetLoggedInUserInfo(bool refresh = false, CancellationToken token = default(CancellationToken))
        {
            if (Session == null) { throw new Exception("no session"); }

            if (refresh) {
                await RefreshCurrentUserInfo();
            }

            return LoggedInUser;
        }

        public async Task<IUser> UpdateUserInfo(IUser user, CancellationToken token = default(CancellationToken))
        {
            var result = await _userWebService.UpdateUserInformation(user);
            LoggedInUser = (User) result;
            LoggedInUser.Id = Session.AuthenticatedUser.Id;

            _messenger.Publish(new UserProfileUpdatedMessage(this, LoggedInUser));
            return LoggedInUser;
        }

        public async Task RefreshCurrentUserInfo(CancellationToken token = default(CancellationToken))
        {
            if (Session == null) { throw new Exception("no session"); }

            var result = await FetchUserInfo(Session.AuthenticatedUser.Id);
            LoggedInUser = (User) result;
            LoggedInUser.Id = Session.AuthenticatedUser.Id;
            _messenger.Publish(new UserInfoReloadedMessage(this, LoggedInUser));
        }

        public async Task<IList<IUser>> SearchUser (string query, int pageNum, int pageSize, CancellationToken token = default(CancellationToken))
        {
            return await _userWebService.SearchUser(query, pageNum, pageSize);
        }

        public async Task UpdateUserCurrentPosition(ILocation location, CancellationToken token = default(CancellationToken))
        {
            if (Session == null) { throw new Exception("no session"); }
            LoggedInUser.SetCurrentLocation(location);
            await UpdateUserInfo(LoggedInUser);
        }

        public async Task SendUserCurrentPosition(CancellationToken token = default(CancellationToken))
        {
            // TODO DEBUG THIS
            var locationProvider = Mvx.Resolve<Coc.MvvmCross.Plugins.Location.ICocLocationProvider>();
            var locationConverterService = Mvx.Resolve<IGeoLocationConverterService>();
            if (LoggedInUser != null) {
                var currentLocation = await locationProvider.TryGetLocationFromDeviceApi();
                if (currentLocation != null) {
                    var address = await locationConverterService.ReverseGeocodeLocationAsync(currentLocation);
                    if (address != null) {
                        await UpdateUserCurrentPosition(address.ToLocation());
                    }
                }
            }
        }

        public async Task<IUser> SetFilters (IEventFilter filters, CancellationToken token = default(CancellationToken))
        {
            if (Session == null) { throw new Exception("no session"); }

            LoggedInUser = (User) await _userWebService.SetEventFilter(Session.AuthenticatedUser.Id, filters);
            LoggedInUser.Id = Session.AuthenticatedUser.Id;

            _messenger.Publish(new UserFiltersUpdatedMessage(this, filters));

            return LoggedInUser;
        }


        public async void Signout()
        {
            var facebookService = Mvx.Resolve<IFacebookService>();
            facebookService.Logout();

            try {
                await new CancelableRequest().ExecuteRequest(async (token, tryCount) => {
                    await Mvx.Resolve<IDeviceTrackerService>().PurgeDeviceInfo();
                    _yuFitClient.CloseActiveSession();
                    Session = null;
                    LoggedInUser = null;

                    _messenger.Publish(new UserLoggedOutMessage(this));
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }


        }

        public bool AmITheCreatorOfThisActivity(FitActivity activity)
        {
            if (Session == null) { throw new Exception("no session"); }
            return activity.CreatedBy.Id == Session.AuthenticatedUser.Id;
        }

        public bool IsThisMe(string userId)
        {
            return Session.AuthenticatedUser.Id == userId;
        }

        public bool AmIInvitedToThisActivity(FitActivity activity)
        {
            return !AmITheCreatorOfThisActivity(activity) && activity.Participants.Any(p => p.UserId == Session.AuthenticatedUser.Id);
        }

        #endregion

        #region Friends IUserService methods impl

        public async Task<bool> AddFriend(string friendId, CancellationToken token = default(CancellationToken))
        {
            await _friendsWebService.AddFriend(friendId);
            _messenger.Publish(new FriendAddedMessage(this, friendId));
            return true;
        }

        public async Task<bool> UnFriend(string friendId, CancellationToken token = default(CancellationToken))
        {
            await _friendsWebService.UnFriend(friendId);
            _messenger.Publish(new FriendRemovedMessage(this, friendId));
            return true;
        }

        public async Task<IList<IUser>> GetMyFriends(CancellationToken token = default(CancellationToken))
        {
            return await _friendsWebService.MyFriends(0, 100);
        }

        public async Task<IList<IUser>> GetFriendsOf(string friendId, CancellationToken token = default(CancellationToken))
        {
            return await _friendsWebService.GetFriendsOf(friendId, 0, 100);
        }

        public async Task<IList<IUserGroup>> GetGroupsOf(string friendId, CancellationToken token = default(CancellationToken))
        {
            return await _friendsWebService.GetGroupsOf(friendId, 0, 100);
        }

        #endregion
    }
}

