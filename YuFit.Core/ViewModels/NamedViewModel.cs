﻿using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;

namespace YuFit.Core.ViewModels
{
    public class NamedViewModel : MvxViewModel
	{
        private IStringLoaderService StringLoaderService { get; set; }

        public  string DisplayName { get; set; }

        public NamedViewModel () 
        {
            StringLoaderService = Mvx.Resolve<IStringLoaderService> (); 
            DisplayName = string.Empty;
        }

        public NamedViewModel (string displayName) 
        { 
            StringLoaderService = Mvx.Resolve<IStringLoaderService> ();
            DisplayName = Localized(displayName); 
        }

//        #if DEBUG_LEAKS
        ~NamedViewModel ()
        {
            Mvx.Trace("Destroy NamedViewModel {0}", GetType().ToString());
        }
//        #endif

        protected string Localized(string text)
        {
            return StringLoaderService.GetString(text);
        }

        protected string Localized(string text, string table)
        {
            return StringLoaderService.GetString(text, table);
        }

        protected string LocalizedSubsport(FitActivity activity)
        {       
            string localizedSubsport = string.Empty;
            if (!string.IsNullOrEmpty(activity.Sport) && !string.IsNullOrEmpty(activity.SubSport))
            {
                localizedSubsport = Localized(string.Format("activity.{0}.subsport.value.{1}", activity.Sport, activity.SubSport.ToLower()));
            }
            return localizedSubsport;
        }

        #region IDisposable/Memory management stuff

        public void Close() { OnClosing(); }
        protected virtual void OnClosing() {}

        #endregion
	}
}

