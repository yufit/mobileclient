﻿
namespace YuFit.Core.ViewModels
{
    public class InvitationTableItemViewModel : ActivitySummarySubViewModel
    {
        public string InvitationId { get; set; }

        public CommandViewModel IgnoreInvitationCommand { get; set; }
        public CommandViewModel AcceptInvitationCommand { get; set; }
    }
}

