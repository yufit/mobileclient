﻿
using YuFit.Core.Interfaces.Services;
using MvvmCross.Platform;

namespace YuFit.Core.ViewModels
{
    public class BaseFitActivityViewModel : BaseViewModel
    {
        protected string ActivityId { get; private set; }
        protected string ActivityTypeName { get; set; }
        protected IFitActivityService FitActivityService { get; private set; }

        public class Navigation { 
            public string ActivityId { get; set; } 
            public string ActivityTypeName { get; set; }
        }

        public virtual void Init(Navigation navParams)
        {
            ActivityId = navParams.ActivityId;
            ActivityTypeName = navParams.ActivityTypeName;
            LoadViewModel();
        }

        public BaseFitActivityViewModel(string displayName)
        {
            DisplayName = Localized(displayName);
            FitActivityService = Mvx.Resolve<IFitActivityService>();
        }

        protected virtual void LoadViewModel() {}
    }
}

