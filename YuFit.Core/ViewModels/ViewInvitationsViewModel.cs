﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using Coc.MvvmCross.Plugins.Location;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.Models.Activities;
using YuFit.Core.ViewModels.Activity.Summary;
using YuFit.Core.ViewModels.Event;

namespace YuFit.Core.ViewModels
{
    public class ViewInvitationsViewModel : BaseViewModel, IContextCommand1and2
    {
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly IFitActivityService _fitActivityService;
        private readonly ICocLocationProvider _locationProvider;
        private Region _currentRegion;

        private ObservableCollection<string> _categories;
        public ObservableCollection<string> Categories
        {
            get { return _categories; }
            set { _categories = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// Backing store for the <see cref="Invitations"/> property
        /// </summary>
        private ObservableCollection<InvitationTableItemViewModel> _invitations;
        public ObservableCollection<InvitationTableItemViewModel> Invitations {
            get { return _invitations; }
            set { _invitations = value; RaisePropertyChanged(); }
        }

        public CommandViewModel ContextCommand2 { get; private set; }
        public CommandViewModel ContextCommand1 { get; private set; }
        public CommandViewModel ShowActivitySummaryCommand { get; private set; }

        public bool FirstTimeRunnningApp   { get; set; }

        public int WhichInvitations { get; set; }
        public string InformationText { get; set; }


        public async void OnWhichInvitationsChanged()
        {
            await DisplayInvitations();
        }

        public ViewInvitationsViewModel(
            ICocLocationProvider locationProvider, 
            IUserService userService,
            IFitActivityService fitActivityService,
            IFileIoService fileIo)
        {
            DisplayName = Localized("view.invitations.title");
            _fitActivityService = fitActivityService;
            _locationProvider = locationProvider;
            WhichInvitations = 0;

            ShowActivitySummaryCommand = new CommandViewModel(new MvxCommand<InvitationTableItemViewModel>(ShowActivitySummary));

            Categories = new ObservableCollection<string> { 
                Localized("view.invitations.category.invitations.label"), 
                Localized("view.invitations.category.around.me.label"), 
                Localized("view.invitations.category.mine.label") };

            var accessInfo      = fileIo.Read<AccessInformation>(AccessInformation.Key);
            FirstTimeRunnningApp = accessInfo.NumberOfAccesses == 1;

        }

        async void DeclineInvitation (string inviteId)
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    await _fitActivityService.SetGoing(inviteId, false);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void DeleteInvitation (string inviteId)
        {
            UserDialogs.Alert (
                Localized("view.invitations.context.coming.soon.label"), 
                null, 
                Localized("view.invitations.context.coming.soon.title"));
        }

        async void ToggleGoing (string inviteId)
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    await _fitActivityService.SetGoing(inviteId, true);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        public override async void Start ()
        {
            base.Start();

            CocLocation currentLocation = _locationProvider.TryGetLocationUsingTimezone();
            _currentRegion = new Region {
                Center = new Coordinate(currentLocation.Coordinates.Latitude, currentLocation.Coordinates.Longitude),
                Radius = 20
            };

            try 
            {
                await UpdateCurrentLocation();
                await DisplayInvitations();
            }
            catch (Exception ex)
            {
                Mvx.Trace(ex.Message);
            }
        }


        private async Task UpdateCurrentLocation()
        {
            CocLocation currentLocation = null;
            // Analysis disable EmptyGeneralCatchClause
            try { currentLocation = await _locationProvider.TryGetLocationFromDeviceApi(); }
            catch (Exception /*ex*/) { }
            // Analysis restore EmptyGeneralCatchClause

            if (currentLocation != null) {
                _currentRegion = new Region {
                    Center = new Coordinate(currentLocation.Coordinates.Latitude, currentLocation.Coordinates.Longitude),
                    Radius = 20
                };
            }
        }


        private async Task DisplayInvitations()
        {
            switch (WhichInvitations)
            {
                case 0:
                    await GetInvitations();
                    break;

                case 1:
                    await GetActivitiesAroundMe();
                    break;

                case 2:
                    await GetMyActivities();
                    break;
            }
        }

            

        private string GetDistanceValue(string activityType, int? distance)
        {
            string distanceValue = string.Empty;
            if (distance.HasValue)
            {
                distanceValue = DistanceValues.ForSport(activityType).TextForValue(distance.Value);
            }
            return distanceValue;
        }

        private string GetDurationValue(string activityType, int? duration)
        {
            string durationValue = string.Empty;
            if (duration.HasValue)
            {
                durationValue = DurationValues.ForSport(activityType).TextForValue(duration.Value);
            }
            return durationValue;
        }

        private void CreateInvitations(List<FitActivity> activities, string label)
        {
            ObservableCollection<InvitationTableItemViewModel> invitations = new ObservableCollection<InvitationTableItemViewModel>();

            activities.ForEach(activity =>
            {
                if (activity.IsAPromotedEvent) {

                    invitations.Add(new InvitationTableItemViewModel {
                        ActivityType = new ActivityType { TypeName = activity.Sport, Enabled = true },
                        DisplayName = Localized(string.Format("{0}.{1}", activity.Sport, activity.SubSport).ToLower()),
                        InvitationId = activity.Id,
                        When = ((EventSchedule) activity.EventSchedule).LocalStartTime,
                        CreatedBy = activity.CreatedBy.Name,
                        CreatedByLabel = label,
                        StreetAddress = ((Location)activity.Location).FullAddress(),
                        Details = GetDetailsForActivity(activity),
                        IsCanceled = activity.IsCanceled.HasValue && activity.IsCanceled.Value,
                        IsPromoted = true
                    });
                } else {
                    invitations.Add(new InvitationTableItemViewModel {
                        DisplayName = LocalizedSubsport(activity),
                        InvitationId = activity.Id,
                        ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(a => a.TypeName == activity.Sport),
                        When = ((EventSchedule) activity.EventSchedule).LocalStartTime,
                        CreatedBy = activity.CreatedBy.Name,
                        CreatedByLabel = label,
                        StreetAddress = ((Location)activity.Location).FullAddress(),
                        Details = GetDetailsForActivity(activity),
                        IsCanceled = activity.IsCanceled.HasValue && activity.IsCanceled.Value
                    });
                }
            });
            
            Invitations = invitations;
        }

        private async Task GetInvitations()
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    ContextCommand1 = new CommandViewModel(Localized("view.invitations.context.accept.command"), new MvxCommand<string>(ToggleGoing));
                    ContextCommand2 = new CommandViewModel(Localized("view.invitations.context.ignore.command"), new MvxCommand<string>(DeclineInvitation));
                    List<FitActivity> activities = await _fitActivityService.MyInvitedActivities(token);
                    var orderedList = activities.OrderBy(x => x.EventSchedule.StartTime).ToList();

                    InformationText = activities.Count == 0 
                        ? InformationText = Localized("view.invitations.no.invites.label")
                        : string.Empty;

                    CreateInvitations(orderedList, Localized ("view.invitations.invited.by.label"));
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        private async Task GetActivitiesAroundMe()
        {
            try {
                ContextCommand1 = new CommandViewModel(Localized("view.invitations.context.join.command"), new MvxCommand<string>(ToggleGoing));
                ContextCommand2 = new CommandViewModel(Localized("view.invitations.context.ignore.command"), new MvxCommand<string>(DeclineInvitation));

                double radius = _currentRegion.Radius;
                if (radius > 300.0) { radius = 300.0; }
                if (radius < 5.0  ) { radius = 5.0;   }
                List<FitActivity> activities = await _fitActivityService.SearchForActivities(_currentRegion.Center, radius);
                var orderedList = activities.OrderBy(x => x.EventSchedule.StartTime).ToList();
                CreateInvitations(orderedList, Localized ("view.invitations.created.by.label"));

                InformationText = string.Empty;
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        private async Task GetMyActivities()
        {
            try {
                ContextCommand1 = new CommandViewModel(Localized("view.invitations.context.leave.command"), new MvxCommand<string>(ToggleGoing));
                ContextCommand2 = new CommandViewModel(Localized("view.invitations.context.delete.command"), new MvxCommand<string>(DeleteInvitation));

                List<FitActivity> activities = await _fitActivityService.MyCreatedActivities();
                var orderedList = activities.OrderBy(x => x.EventSchedule.StartTime).ToList();
                CreateInvitations(orderedList, Localized ("view.invitations.created.by.label"));

                InformationText = activities.Count == 0 
                    ? InformationText = Localized("view.invitations.none.created.label")
                    : string.Empty;
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        private List<string> GetDetailsForActivity(FitActivity activity)
        {
            List<string> details = new List<string>();

            if (activity.Duration.HasValue)
            {
                string durationFormat = Localized(string.Format("activity.{0}.duration.format", activity.Sport));
                details.Add(string.Format(durationFormat, activity.Duration.Value / 60, activity.Duration.Value % 60));
            }

            if (activity.Distance.HasValue)
            {
                string durationFormat = Localized(string.Format("activity.{0}.distance.format", activity.Sport));
                details.Add(string.Format(durationFormat, activity.Distance.Value));
            }

            if (activity.IntensityLevel.HasValue)
            {
                details.Add(Localized(string.Format("activity.intensity.value.{0}", activity.Intensity.ToString().ToLower())));
            }

            return details;
        }

        void ShowActivitySummary (InvitationTableItemViewModel obj)
        {
            if (obj.IsPromoted) {
                ShowViewModel<EventViewModel>(new EventViewModel.Nav { Id = obj.InvitationId });
            } else {
                ShowViewModel<ActivitySummary2ViewModel>(new ActivitySummary2ViewModel.Nav { Id = obj.InvitationId });
            }
//            var message = new InvitedToActivityMessage(this, obj.InvitationId, true);
//            Mvx.Resolve<IMvxMessenger>().Publish(message);
        }

    }
}

