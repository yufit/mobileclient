﻿using System;
using System.Threading.Tasks;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using Coc.MvvmCross.Plugins.Facebook;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.ViewModels.Dashboard;

namespace YuFit.Core.ViewModels.Login
{
    
    public class LoginViewModel : BaseViewModel
    {
        readonly IUserService       _userService;
        readonly IFacebookService   _facebookService;

        #region String Properties
        public string LoginTitleString              { get; set; }
        public string LoginMethodString             { get; set; }
        public string EmailLabelPlaceholder         { get; set; }
        public string PasswordLabelPlaceholder      { get; set; }
        public string GoString                      { get; set; }
        public string OrString                      { get; set; }
        public string SignUpString                  { get; set; }
        public string IfYouDontHaveAnAccountString  { get; set; }
        public string ResetPasswordString           { get; set; }

        public bool   FacebookChosen                { get; set; }
        public bool   TwitterChosen                 { get; set; }
        public bool   YufitChosen                   { get; set; }
        public bool   InstagramChosen               { get; set; }
        #endregion

        #region Data Properties
        public string Email                         { get; set; }
        public string Password                      { get; set; }
        #endregion

        #region Commands
        public CommandViewModel SignUpCommand             { get; private set; }
        public CommandViewModel ChooseFacebookCommand     { get; private set; }
        public CommandViewModel ChooseTwitterCommand      { get; private set; }
        public CommandViewModel ChooseYufitCommand        { get; private set; }
        public CommandViewModel ChooseInstagramCommand    { get; private set; }
        public CommandViewModel AuthenticateCommand       { get; private set; }
        public CommandViewModel ResetPasswordCommand      { get; private set; }
        #endregion

        public LoginViewModel (
            IUserService userService,
            IFacebookService facebookService
        )
        {
            _userService = userService;
            _facebookService = facebookService;

            LoginTitleString                = Localized("login.title");
            LoginMethodString               = Localized("login.method.label");
            EmailLabelPlaceholder           = Localized("login.method.yufit.email.placeholder");
            PasswordLabelPlaceholder        = Localized("login.method.yufit.password.placeholder");
            GoString                        = Localized("login.go.button");
            OrString                        = Localized("login.method.yufit.or.label");
            SignUpString                    = Localized("login.method.yufit.signup.button");
            IfYouDontHaveAnAccountString    = Localized("login.method.yufit.if.you.dont.have.an.account.label");
            ProgressMessage                 = Localized("login.signing.up.progress.message");
            ResetPasswordString             = Localized("login.method.yufit.resetpassword");

            SignUpCommand                   = new CommandViewModel(new MvxCommand(SignUp));
            AuthenticateCommand             = new CommandViewModel(new MvxCommand(Authenticate));
            ChooseFacebookCommand           = new CommandViewModel(new MvxCommand(ChooseFacebook));
            ChooseTwitterCommand            = new CommandViewModel(new MvxCommand(ChooseTwitter));
            ChooseYufitCommand              = new CommandViewModel(new MvxCommand(ChooseYufit));
            ChooseInstagramCommand          = new CommandViewModel(new MvxCommand(ChooseInstagram));
            ResetPasswordCommand            = new CommandViewModel(new MvxCommand(ResetPassword));
            
        }

        //------------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------

        void SignUp ()
        {
            ShowViewModel<SignupViewModel>();
        }

        void ChooseFacebook()
        {
            LoginMethodString = Localized("login.method.facebook.label");
            FacebookChosen = true;
            TwitterChosen = YufitChosen = InstagramChosen = false;
        }

        void ChooseTwitter()
        {
            LoginMethodString = Localized("login.method.twitter.label");
            TwitterChosen = true;
            FacebookChosen = YufitChosen = InstagramChosen = false;
        }

        void ChooseYufit()
        {
            LoginMethodString = Localized("login.method.yufit.label");
            YufitChosen = true;
            TwitterChosen = FacebookChosen = InstagramChosen = false;
        }

        void ChooseInstagram()
        {
            LoginMethodString = Localized("login.method.instagram.label");
            InstagramChosen = true;
            TwitterChosen = YufitChosen = FacebookChosen = false;
        }

        async void LoginWithFacebook ()
        {
            try {
                if (!await _facebookService.AuthenticateAsync()) { 
                    Alert("login.error.facebook.generic"); 
                    return;
                }

                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    await _userService.AuthenticateWithFacebook(_facebookService.AuthToken, string.Empty, token);
                    ShowViewModel<DashboardViewModel>();
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        async void LoginWithYufit ()
        {
            try {
                if (string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(Password)) {
                    Alert("login.error.noemail.or.password.specified"); 
                    return;
                }

                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    await _userService.AuthenticateWithEmail(Email, Password, token);
                    ShowViewModel<DashboardViewModel>();
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void LoginWithTwitter ()
        {
            Alert ("login.error.twitter.unavailable", null, "Error");
        }

        void LoginWithInstagram()
        {
            Alert ("login.error.instagram.unavailable", null, "Error");
        }
       
        void Authenticate()
        {
            if (YufitChosen     ) { LoginWithYufit();               }
            if (FacebookChosen  ) { LoginWithFacebook();            }
            if (InstagramChosen ) { LoginWithInstagram();           }
            if (TwitterChosen   ) { LoginWithTwitter();             }
        }

        void ResetPassword()
        {
            Mvx.Resolve<IStartAppService>().OpenUrl(new System.Uri("https://staging.api.yu-fit.com:8443/email/forgot"));
        }

        protected async override void HandleWebServiceUnauthorizedException (Exception ex)
        {
            if (YufitChosen) {
                bool resetPassword = await UserDialogs.ConfirmAsync(
                    Localized("login.error.invalid.password.message"), 
                    Localized("login.error.invalid.password.title"), 
                    Localized("login.error.invalid.password.reset.button"),
                    Localized("login.error.invalid.password.cancel.button")
                );

                if (resetPassword) { ResetPassword(); }
            }

            if (FacebookChosen) {
                Alert("login.error.facebook.generic");
            }
        }
    }

}

