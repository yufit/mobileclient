﻿using System;


using YuFit.Core.Interfaces.Services;
using YuFit.Core.ViewModels.Dashboard;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;

namespace YuFit.Core.ViewModels.Login
{
    public class SignupViewModel : BaseViewModel
    {
        readonly IUserService       _userService;

        #region String Properties
        public string SignUpTitleString             { get; set; }
        public string FillThisString                { get; set; }
        public string YouWillReceiveConfString      { get; set; }
        public string NameLabelPlaceholder          { get; set; }
        public string EmailLabelPlaceholder         { get; set; }
        public string PasswordLabelPlaceholder      { get; set; }
        public string PasswordAgainLabelPlaceholder { get; set; }
        #endregion

        #region Data Properties
        public string Name                          { get; set; }
        public string Email                         { get; set; }
        public string Password                      { get; set; }
        public string PasswordRepeat                { get; set; }
        #endregion

        #region Commands
        public CommandViewModel CreateMyAccountCommand   { get; private set; }
        #endregion

        public SignupViewModel (
            IUserService userService)
        {
            _userService = userService;

            SignUpTitleString               = Localized("login.signup.title");
            FillThisString                  = Localized("login.signup.fill.this.label");
            YouWillReceiveConfString        = Localized("login.signup.confirmation.label");

            NameLabelPlaceholder            = Localized("login.signup.name.placeholder");
            EmailLabelPlaceholder           = Localized("login.signup.email.placeholder");
            PasswordLabelPlaceholder        = Localized("login.signup.password.placeholder");
            PasswordAgainLabelPlaceholder   = Localized("login.signup.password2.placeholder");

            ProgressMessage                 = Localized("login.signup.creating.progress.message");

            CreateMyAccountCommand          = new CommandViewModel(Localized("login.signup.sign.up.button"), new MvxCommand(CreateMyAccount));
        }

        async void CreateMyAccount ()
        {
            if (Password != PasswordRepeat) {
                Alert("login.signup.error.passwords.dont.match");
                return;
            }

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    await _userService.RegisterWithEmail(Name, Email, Password, token);
                    ShowViewModel<DashboardViewModel>();
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        protected override void HandleWebServiceUnauthorizedException (Exception ex)
        {
            Alert("login.signup.error.account.already.exists");
        }
    }
}

