﻿using System;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using MvvmCross.Plugins.DownloadCache;

namespace YuFit.Core.ViewModels
{
    public class DisplayReceivedNotificationViewModel : BaseViewModel
    {
        MvxFileDownloadCache.Timer _timer;

        public CommandViewModel DismissCommand { get; private set; }

        public DisplayReceivedNotificationViewModel ()
        {
            DismissCommand = new CommandViewModel(new MvxCommand(OnTapped));
        }

        public override void Start ()
        {
            base.Start();
            _timer = new MvxFileDownloadCache.Timer(NotificationExpired, null, TimeSpan.FromSeconds(3), TimeSpan.FromSeconds(3));
        }

        private void NotificationExpired(object state)
        {
            DismissMe();
        }

        private void DismissMe()
        {
            _timer.Cancel();
            Close(this);
        }

        protected virtual void OnTapped()
        {
            DismissMe();
        }
    }
}

