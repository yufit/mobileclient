﻿using YuFit.Core.Services.Notifications;
using Newtonsoft.Json;
using System.Text;
using YuFit.Core.Interfaces.Services;

namespace YuFit.Core.ViewModels
{
    public class PushNotificationViewModel : DisplayReceivedNotificationViewModel
    {   
        public PushNotificationCategory Category { get; set; }
        public string Message { get; set; }
        public string Sport { get; set; }

        BasePushNotification Notification { get; set; }

        public class Nav 
        {
            public string NotificationJSON { get; set; }

            public Nav () { }

            public Nav (IPushNotification notification)
            {
                NotificationJSON = JsonConvert.SerializeObject(notification, Formatting.None, new JsonSerializerSettings()
                {
                    TypeNameHandling = TypeNameHandling.Objects,
                    TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
                });
            }

            public T GetNotification<T>()
            {
                return (T) JsonConvert.DeserializeObject(NotificationJSON, typeof(T), new JsonSerializerSettings() {
                    TypeNameHandling = TypeNameHandling.Objects
                });
            }
        }

        public virtual void Init(Nav navigation)
        {
            Notification = navigation.GetNotification<BasePushNotification>();

            Category = Notification.Category;
            Message = Notification.Message;

            var baseEventPushNotification = Notification as BaseEventPushNotification;
            if (baseEventPushNotification != null) {
                Sport = baseEventPushNotification.Sport;
            }
        }

        protected override void OnTapped ()
        {
            base.OnTapped();
            Notification.AppIsInForeground = false;
            Notification.HandleReceivedNotificationWhileRunning(done => {});
        }
    }
}

