﻿using YuFit.Core.Models;

namespace YuFit.Core.ViewModels.MapAnnotations
{
    public class EventMapAnnotationViewModel : FitActivityMapAnnotationViewModel
    {
        #region Properties
        public string           AvatarUrl       { get; set; }
        public Colors           Colors          { get; set; }
        #endregion

        public override MapAnnotationViewModel Copy (MapAnnotationViewModel vm)
        {
            var copy = (EventMapAnnotationViewModel) base.Copy(vm);
            copy.AvatarUrl = ((EventMapAnnotationViewModel)vm).AvatarUrl;
            return copy;
        }

        public override void Reload (FitActivity fitActivity)
        {
            base.Reload(fitActivity);
            AvatarUrl = fitActivity.UserGroupAvatarUrl;
        }
    }
}

