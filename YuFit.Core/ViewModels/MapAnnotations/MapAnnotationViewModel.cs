﻿using System;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using YuFit.Core.Models;

namespace YuFit.Core.ViewModels.MapAnnotations
{
    public abstract class MapAnnotationViewModel : BaseViewModel
    {
        public const float EPSILON = 0.1f;

        #region Properties
        public string           Id                  { get; set; }
        public Coordinate       Location            { get; set; }
        //public CommandViewModel ViewDetailsCommand  { get; private set; }
        #endregion

        protected MapAnnotationViewModel()
        {
            //ViewDetailsCommand = new CommandViewModel(new MvxCommand(ViewDetails));
        }

        #region Methods

        /// <summary>
        /// Views the details.
        /// </summary>
        protected abstract void ViewDetails ();

        /// <summary>
        /// Determines whether this instance is equals the specified vm.
        /// </summary>
        /// <returns><c>true</c> if this instance is equals the specified vm; otherwise, <c>false</c>.</returns>
        /// <param name="vm">Vm.</param>
        public virtual bool IsEquals(MapAnnotationViewModel vm)
        {
            return  Id == vm.Id &&
                DisplayName == vm.DisplayName &&
                Math.Abs(Location.Latitude - vm.Location.Latitude) < EPSILON &&
                Math.Abs(Location.Longitude - vm.Location.Longitude) < EPSILON;
        }

        /// <summary>
        /// Copy the specified vm.
        /// </summary>
        /// <param name="vm">Vm.</param>
        public virtual MapAnnotationViewModel Copy(MapAnnotationViewModel vm)
        {
            Id = vm.Id;
            DisplayName = vm.DisplayName;
            Location = vm.Location;

            return this;
        }

        /// <summary>
        /// Reload the specified fitActivity.
        /// </summary>
        /// <param name="fitActivity">Fit activity.</param>
        public virtual void Reload (FitActivity fitActivity)
        {
            DisplayName = fitActivity.Name;
            Location = ((Location)fitActivity.Location).GeoCoordinates;
        }

        #endregion
    }
}

