﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

using YuFit.Core.Extensions;
using YuFit.Core.Models;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using YuFit.Core.Interfaces.Services;

namespace YuFit.Core.ViewModels.MapAnnotations
{
    public class MapAnnotationObservableCollection : ObservableCollection<MapAnnotationViewModel>
    {
        /// <summary>
        /// Syncs the collection.
        /// For performance, first handle the remove cases.  Then the modified, then the added.
        /// </summary>
        /// <param name="events">Events.</param>
        public void SyncCollection(List<FitActivity> events)
        {
            var incomingList = new List<MapAnnotationViewModel>();
            events.ForEach(ev => incomingList.Add(CreateFrom(ev)));

            var removed = this.Except(incomingList, new ComparerById()).ToList();
            removed.ForEach(ev => Remove(ev));

            var modified = incomingList.Intersect(this, new ComparerToFindModifiedPin()).ToList();
            modified.ForEach(updatedEv => this.FirstOrDefault(ev => ev.Id == updatedEv.Id).Copy(updatedEv));

            var added = incomingList.Except(this, new ComparerById()).ToList();
            added.ForEach(Add);
        }

        /// <summary>
        /// Creates a vm from the kind of activity.  This will work only if the stuff added
        /// on the map is an activity.  If we ever add something else, we will have to tweaks this
        /// and move the logic outside.
        /// </summary>
        /// <returns>The from.</returns>
        /// <param name="fitActivity">Fit activity.</param>
        public static MapAnnotationViewModel CreateFrom(FitActivity fitActivity) 
        {
            if (!string.IsNullOrEmpty(fitActivity.UserGroupId)) {
                return new GroupFitActivityMapAnnotationViewModel
                {
                    Id = fitActivity.Id,
                    DisplayName = fitActivity.Name,
                    Location = ((Location)fitActivity.Location).GeoCoordinates,
                    ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(activity => activity.TypeName == fitActivity.Sport),
                    Invited = Mvx.Resolve<IUserService>().AmIInvitedToThisActivity(fitActivity),
                    AvatarUrl = fitActivity.UserGroupAvatarUrl
                };
            }

            if (fitActivity.IsAPromotedEvent) {
                var categories = Mvx.Resolve<IFitActivityService>().GetEventCategories();

                var category = categories.FirstOrDefault(c => c.Name == fitActivity.Sport);
                var colors = category?.Colors ?? new Colors { 
                    Base = new Color { Red=255,Blue=255,Green=255 },
                    Dark = new Color { Red=255,Blue=255,Green=255 },
                    Light = new Color { Red=255,Blue=255,Green=255 }
                };

                return new EventMapAnnotationViewModel
                {
                    Id = fitActivity.Id,
                    DisplayName = fitActivity.Name,
                    Location = ((Location)fitActivity.Location).GeoCoordinates,
                    Colors = colors,
                    Invited = false,
                    AvatarUrl = fitActivity.IconUrl
                };
            }

            return new FitActivityMapAnnotationViewModel
            {
                Id = fitActivity.Id,
                DisplayName = fitActivity.Name,
                Location = ((Location)fitActivity.Location).GeoCoordinates,
                ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(activity => activity.TypeName == fitActivity.Sport),
                Invited = Mvx.Resolve<IUserService>().AmIInvitedToThisActivity(fitActivity)
            };
        }

        public void Reset() 
        {
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        #region Comparer classes

        class ComparerById : IEqualityComparer<MapAnnotationViewModel>
        {
            // Analysis disable once MemberHidesStaticFromOuterClass
            public bool Equals(MapAnnotationViewModel x, MapAnnotationViewModel y)
            {
                if (Object.ReferenceEquals(x, y)) { return true; }
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null)) { return false; }
                return string.Equals(x.Id, y.Id);
            }

            // If Equals() returns true for a pair of objects  
            // then GetHashCode() must return the same value for these objects. 
            public int GetHashCode(MapAnnotationViewModel product)
            {
                return Object.ReferenceEquals(product, null) ? 0 : product.Id.GetHashCode();
            }

        }

        class ComparerToFindModifiedPin : IEqualityComparer<MapAnnotationViewModel>
        {
            // Analysis disable once MemberHidesStaticFromOuterClass
            public bool Equals(MapAnnotationViewModel x, MapAnnotationViewModel y)
            {
                return string.Equals(x.Id, y.Id) && !x.IsEquals(y);
            }

            // If Equals() returns true for a pair of objects  
            // then GetHashCode() must return the same value for these objects. 
            public int GetHashCode(MapAnnotationViewModel product)
            {
                return Object.ReferenceEquals(product, null) ? 0 : product.Id.GetHashCode();
            }

        }

        #endregion
    }
}

