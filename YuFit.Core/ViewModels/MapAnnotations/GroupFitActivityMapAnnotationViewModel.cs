﻿using YuFit.Core.Models;

namespace YuFit.Core.ViewModels.MapAnnotations
{
    public class GroupFitActivityMapAnnotationViewModel : FitActivityMapAnnotationViewModel
    {
        #region Properties
        public string           AvatarUrl       { get; set; }
        #endregion

        public override MapAnnotationViewModel Copy (MapAnnotationViewModel vm)
        {
            var copy = (GroupFitActivityMapAnnotationViewModel) base.Copy(vm);
            copy.AvatarUrl = ((GroupFitActivityMapAnnotationViewModel)vm).AvatarUrl;
            return copy;
        }

        public override void Reload (FitActivity fitActivity)
        {
            base.Reload(fitActivity);
            AvatarUrl = fitActivity.UserGroupAvatarUrl;
        }
    }
}

