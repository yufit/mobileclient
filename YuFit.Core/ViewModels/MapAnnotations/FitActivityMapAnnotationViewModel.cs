﻿using System;
using System.Linq;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.ViewModels.Activity.Details;

namespace YuFit.Core.ViewModels.MapAnnotations
{
    public class FitActivityMapAnnotationViewModel : MapAnnotationViewModel
    {
        #region Properties
        public ActivityType     ActivityType    { get; set; }
        public bool             Invited         { get; set; }
        #endregion

        protected override void ViewDetails ()
        {
            ShowViewModel<ViewFitActivityDetailsViewModel>(new ViewFitActivityDetailsViewModel.Navigation { ActivityId = this.Id });
        }

        public override MapAnnotationViewModel Copy (MapAnnotationViewModel vm)
        {
            var copy = (FitActivityMapAnnotationViewModel) base.Copy(vm);
            copy.ActivityType = ((FitActivityMapAnnotationViewModel)vm).ActivityType;
            copy.Invited = ((FitActivityMapAnnotationViewModel)vm).Invited;
            return copy;
        }

        public override void Reload (FitActivity fitActivity)
        {
            base.Reload(fitActivity);
            ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(activity => activity.TypeName == fitActivity.Sport);
            Invited = Mvx.Resolve<IUserService>().AmIInvitedToThisActivity(fitActivity);
        }
    }
}

