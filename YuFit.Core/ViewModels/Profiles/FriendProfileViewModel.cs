﻿using System.Collections.Generic;
using System.Linq;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Activity;
using YuFit.Core.ViewModels.Activity.Summary;
using YuFit.Core.ViewModels.Associations.Friendships;
using YuFit.Core.ViewModels.Associations;
using System;
using YuFit.Core.ViewModels.Event;

namespace YuFit.Core.ViewModels.Profiles
{
    public class FriendProfileViewModel : BaseProfileViewModel
    {

        public bool     ThisIsMe                { get; set; }
        public bool     IsFriend                { get; set; }
        public string   ActivityCreatedCount    { get; set; }
        public string   UpcomingActivitiesTitle { get; set; }
        public SilentObservableCollection<UpcomingActivityViewModel> UpcomingActivities { get; private set; } 

        #region Commands

        public CommandViewModel ViewFriendsCommand          { get; private set; }
        public CommandViewModel ShowActivitySummaryCommand  { get; private set; }
        public CommandViewModel AddAsFriendCommand          { get; private set; }
        public CommandViewModel UnFriendCommand             { get; private set; }

        #endregion

        public string CreatedCountLabel { get; set; }

        #region Construction/Destruction

        public FriendProfileViewModel (IUserService userService) : base(userService)
        {
            CreatedCountLabel = Localized("friend.profile.created.count.label");

            ViewFriendsCommand  = new CommandViewModel(new MvxCommand(ViewFriends));
            AddAsFriendCommand  = new CommandViewModel(new MvxCommand(AddAsFriend));
            UnFriendCommand     = new CommandViewModel(new MvxCommand(UnFriend));

            UpcomingActivities = new SilentObservableCollection<UpcomingActivityViewModel>();
            UpcomingActivitiesTitle = Localized("activity.upcoming.label");
            ShowActivitySummaryCommand = new CommandViewModel(new MvxCommand<UpcomingActivityViewModel>(ShowActivitySummary));
        }

        protected override void PopulateProfileFromUserData (User user)
        {
            base.PopulateProfileFromUserData(user);

            ThisIsMe = UserService.IsThisMe(UserId);
            IsFriend = user.IsFriend;
            ActivityCreatedCount = user.CreatedEventsCount.ToString();

            var tmp = new List<UpcomingActivityViewModel>();
            user.ShortUpcomingEventList.ForEach(act => tmp.Add(new UpcomingActivityViewModel((FitActivity) act)));
            var orderedList = tmp.OrderBy(x => x.When).ToList();
            UpcomingActivities.Clear();
            UpcomingActivities.AddMultiple(orderedList);
        }

        #endregion

        void ViewFriends ()
        {
            ShowViewModel<FriendAssociationsViewModel>(new FriendAssociationsViewModel.Navigation {UserId = UserId, Name = DisplayName});
            //ShowViewModel<FriendsOfFriendViewModel>(new FriendsOfFriendViewModel.Navigation {UserId = UserId});
        }

        async void AddAsFriend ()
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    await UserService.AddFriend(UserId, token);
                    IsFriend = true;
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        async void UnFriend ()
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    await UserService.UnFriend(UserId, token);
                    IsFriend = true;
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void ShowActivitySummary (UpcomingActivityViewModel obj)
        {
            if (obj.IsPromoted) {
                ShowViewModel<EventViewModel>(new EventViewModel.Nav { Id = obj.Id });
            } else {
                ShowViewModel<ActivitySummary2ViewModel>(new ActivitySummary2ViewModel.Nav { Id = obj.Id });
            }
        }
    }
}


//--Bio
//    Defriend button
//    Link to friends
//-number of activity it has been in
//-Sex
//-Avatar
//-Age
//-City
//-Interests
//-Interests color thingy
//-Real name


