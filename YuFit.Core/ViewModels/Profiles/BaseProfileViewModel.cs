﻿using System;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;

namespace YuFit.Core.ViewModels.Profiles
{
    /// <summary>
    /// We have a base class for the FriendProfileViewModel class.  At some point, we had a public
    /// and a private view (if someone is a friend or not). Now that we no longer required to have
    /// friendship confirmed, there is no point of having public vs private profile view.  However
    /// it was decided to keep this class anyway, and not move the logic in the only derived class,
    /// in case we decide to add that public/private logic in the future.
    /// </summary>
    public class BaseProfileViewModel : BaseViewModel
    {
        protected readonly IUserService UserService;
       
        public string   UserId          { get; private set; }
        public string   RealName        { get; set; }
        public string   HomeAddress     { get; set; }
        public Uri      AvatarUrl       { get; set; }
        public string   Bio             { get; set; }


        #region Commands

        public CommandViewModel SendMessageCommand { get; private set; }

        #endregion

        #region Construction/Destruction

        public BaseProfileViewModel (IUserService userService) : base("profile.fetch.progress.message")
        {
            UserService = userService;
        }

        public class Navigation { public string UserId { get; set; } public string AvatarUrl {get; set; } }
        public virtual void Init(Navigation navParams)
        {
            FetchUserInfo(navParams.UserId, navParams.AvatarUrl);
        }

        protected virtual void PopulateProfileFromUserData(User user)
        {
            DisplayName = user.Name;
            RealName = user.Name;

            if (user.UserProfile != null) 
            {
                AvatarUrl = user.UserProfile.AvatarUri;
                Bio       = user.UserProfile.Bio;
            }

            Location home = user.GetHomeLocation();
            if (home != null) 
            {
                HomeAddress = home.CityStateString();
            }

            if (HomeAddress == null) 
            {
                HomeAddress = Localized("user.info.location.not.provided.text");
            }
        }

        async void FetchUserInfo (string userId, string avatarUrl)
        {
            UserId = userId;

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var user = (User) await UserService.FetchUserInfo(UserId, token);
                    PopulateProfileFromUserData(user);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        #endregion
    }
}

