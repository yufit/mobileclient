﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

using MvvmCross.Platform;
using Coc.MvvmCross.Plugins.Facebook;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;

using YuFit.WebServices.Interface.Model;
using MvvmCross.Core.ViewModels;

namespace YuFit.Core.ViewModels.Profiles.Edit
{
    public class EditProfileViewModel : BaseViewModel
    {
        IUser _currentUser;
        Location _homelocation;

        readonly IUserService _userService;
        readonly IGeoLocationConverterService _locationConverterService;

        #region Notifiable properties

        public bool IsChanged { get; set; }
        public string RealName { get; set; }
        public string EmailAddress { get; set; }

        public string Hometown { get; set; }
        public int Radius { get; set; }
        public bool UseMetrics { get; set; }
        public DateTime BirthDay { get; set; }
        public Gender Gender { get; set; }
        public Uri AvatarUrl { get; set; }
        public string Bio { get; set; }
        public int SelectedSegment { get; set; }
        public IList<string> FavoriteActivities { get; set; }

        MemoryStream _avatarStream;
        public byte[] AvatarData             { get; set; }

        public ObservableCollection<ActivitySelectViewModel> Activities { get; private set; }

        #endregion

        public string BirthdayPlaceholderText { get; private set; }
        public string NamePlaceholderText     { get; private set; }
        public string EmailPlaceholderText    { get; private set; }
        public string AddressPlaceholderText  { get; private set; }
        public string BioPlaceholderText      { get; private set; }
        public string InterestPlaceholderText { get; private set; }

        public CommandViewModel ToggleActivityCommand { get; private set; }
        public CommandViewModel SearchHometownCommand { get; private set; }
        public CommandViewModel SaveCommand           { get; private set; }
        public CommandViewModel AddAvatarCommand      { get; private set; }

        public EditProfileViewModel(
            IUserService userService,
            IGeoLocationConverterService locationConverterService,
            IFacebookService facebookService
        ) : base("profile.edit.title")
        {
            _userService                = userService;
            _locationConverterService   = locationConverterService;
            IsChanged                   = false;

            BirthdayPlaceholderText     = Localized("profile.edit.birthday.placeholder");
            NamePlaceholderText         = Localized("profile.edit.name.placeholder");
            EmailPlaceholderText        = Localized("profile.edit.email.placeholder");
            AddressPlaceholderText      = Localized("profile.edit.hometown.placeholder");
            BioPlaceholderText          = Localized("profile.edit.bio.placeholder");
            InterestPlaceholderText     = Localized("profile.edit.interest.placeholder");


            ToggleActivityCommand       = new CommandViewModel(new MvxCommand<ActivitySelectViewModel>(ToggleActivity));
            SearchHometownCommand       = new CommandViewModel(new MvxCommand(Search));
            SaveCommand                 = new CommandViewModel(new MvxCommand(Save));
            AddAvatarCommand            = new CommandViewModel(Localized("group.action.add.avatar"), new MvxCommand(AddAvatar));

            Activities = new ObservableCollection<ActivitySelectViewModel>();
            Mvx.Resolve<IFitActivityService>().AvailableActivities
                .Where(act => act.Enabled)
                .ForEach(activity => Activities.Add(new ActivitySelectViewModel(activity)));
        }

        public override void Start ()
        {
            base.Start();

            FetchUserInfo();
        }

        void ToggleActivity (ActivitySelectViewModel activityVm)
        {
            activityVm.IsSelected = !activityVm.IsSelected;
            IsChanged = true;
        }

        protected override void OnClosing ()
        {
            if (IsChanged) { Save(); }
            base.OnClosing();
        }

        async void Save()
        {
            IsChanged = true;

            _currentUser.Email = EmailAddress;
            if (_homelocation != null) {
                ((User) _currentUser).SetHomeLocation(_homelocation);
            }

            _currentUser.UserProfile = _currentUser.UserProfile ?? new UserProfile();
            _currentUser.UserProfile.AvatarUri = AvatarUrl;
            _currentUser.UserProfile.Birthday = BirthDay;
            _currentUser.UserProfile.FavoriteActivities = new List<string>();
            _currentUser.UserProfile.Kilometers = SelectedSegment == 0;
            _currentUser.UserProfile.Bio = Bio;
            _currentUser.UserProfile.Radius = 100;
            _currentUser.UserProfile.Interests = "";

            Activities
                .Where(act => act.IsSelected)
                .ForEach(activity => _currentUser.UserProfile.FavoriteActivities.Add(activity.DisplayName));

            _currentUser.Devices = null;

            ProgressMessage = Localized("profile.edit.save.progress.message");

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    _currentUser = await _userService.UpdateUserInfo(_currentUser, token);

                    if (_avatarStream != null) {
                        _currentUser = await _userService.UpdateAvatarImage(_currentUser, "", _avatarStream);
                        _avatarStream.Dispose();
                    }

                    IsChanged = false;
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        async void FetchUserInfo ()
        {
            ProgressMessage = Localized("profile.edit.fetch.progress.message");
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var currentUser = await _userService.GetLoggedInUserInfo();
                    RealName = currentUser.Name;
                    EmailAddress = currentUser.Email;

                    if (currentUser.UserProfile != null) {
                        Radius              = currentUser.UserProfile.Radius        ?? 100;
                        BirthDay            = currentUser.UserProfile.Birthday      ?? new DateTime(1980, 01, 01);
                        AvatarUrl           = currentUser.UserProfile.AvatarUri;

                        if (currentUser.UserProfile.Kilometers == null) {
                            UseMetrics = true;
                        } else {
                            UseMetrics = (bool) currentUser.UserProfile.Kilometers;
                        }

                        Gender              = currentUser.UserProfile.Gender ?? Gender.Male;
                        FavoriteActivities  = currentUser.UserProfile.FavoriteActivities;
                        Bio                 = currentUser.UserProfile.Bio;
                    } else {
                        Radius              = 100;
                        UseMetrics          = true;
                        BirthDay            = new DateTime(1980, 01, 01);
                        Gender              = Gender.Male;
                    }

                    BirthDay = BirthDay.ToUniversalTime();

                    if (FavoriteActivities != null) {
                        Activities.ForEach(a => a.IsSelected = false);
                        FavoriteActivities.ForEach(activity => {
                            ActivitySelectViewModel vm = Activities.FirstOrDefault(a => a.ActivityType == activity); 
                            if (vm != null) { vm.IsSelected = true; }
                        });   
                    }

                    SelectedSegment = (UseMetrics?0:1);

                    _homelocation = ((User) currentUser).GetHomeLocation();
                    if (_homelocation != null) {
                        Hometown = _homelocation.CityStateString();
                    }

                    _currentUser = currentUser;
                    IsChanged = false;
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        async void Search()
        {
            if (string.IsNullOrEmpty(Hometown)) { return; }

            try {
                _homelocation = null;

                ProgressMessage = Localized("profile.edit.location.search.progress.message");
                IsBusy = true;
                var addresses = await _locationConverterService.GeocodeAddressAsync(Hometown);
                if (addresses.Count > 0) { 
                    Placemark pl = addresses[0];
                    _homelocation = pl.ToLocation();
                    Hometown = _homelocation.City + ", " + _homelocation.StateProvince;
                }
            } catch (Exception /*ex*/) {
                Hometown = Localized("profile.edit.error.invalid.location.message");
            } finally {
                IsBusy = false;
            }
        }

        void AddAvatar()
        {
            var task = Mvx.Resolve<IImagePickerService>();
            task.ChoosePictureFromLibrary(90, (object) "avatar",
                stream => {
                if (_avatarStream != null) { _avatarStream.Dispose(); }
                _avatarStream = new MemoryStream();
                stream.CopyTo(_avatarStream);
                _avatarStream.Seek(0, SeekOrigin.Begin);

                AvatarData = _avatarStream.ToArray();
            },
                () => {
                // perform any cancelled operation
            });
        }
    }
}

