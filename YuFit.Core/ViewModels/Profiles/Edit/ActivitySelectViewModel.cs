﻿using System;
using YuFit.Core.Models;

namespace YuFit.Core.ViewModels.Profiles.Edit
{
    public class ActivitySelectViewModel : BaseViewModel
    {
        public string ActivityType { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsSelected { get; set; }

        public ActivitySelectViewModel (ActivityType activityType)
        {
            DisplayName = activityType.TypeName;
            ActivityType = activityType.TypeName;
            IsEnabled = activityType.Enabled;
            IsSelected = true;
        }
    }
}

