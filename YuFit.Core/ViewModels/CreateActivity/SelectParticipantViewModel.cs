﻿using System.Collections.Generic;
using System.Linq;

using Coc.MvvmCross.Plugins.Facebook;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.Utils;

using YuFit.WebServices.Interface.Model;
using System;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;

namespace YuFit.Core.ViewModels.CreateActivity
{
    public class ParticipantViewModel : NamedViewModel
    {
        public string FavSport      { get; set; }
        public string Id            { get; set; }
        public string AvatarUrl     { get; set; }
        public string Location      { get; set; }
        public bool   IsSelected    { get; set; }

        public ParticipantViewModel (IUser friend) : base(friend.Name)
        {
            Id = friend.Id;
            var homelocation = ((User) friend).GetHomeLocation();
            if (homelocation != null) {
                Location = homelocation.CityStateString();
            }

            if (friend.UserProfile != null && friend.UserProfile.AvatarUri != null) {
                AvatarUrl = friend.UserProfile.AvatarUri.ToString();
            }

            if (string.IsNullOrEmpty(Location)) {
                Location = Localized("user.info.location.not.provided.text");
            }

            FavSport = "Cycling";
            Mvx.Trace("Creating userinfovm for {0}", DisplayName);
        }

        #if DEBUG_LEAKS
        ~ParticipantViewModel ()
        {
            Mvx.Trace("deleting userinfovm for {0}", DisplayName);
        }
        #endif
    }

    public class SelectParticipantsViewModel : BaseViewModel
    {
        #region Data Members
        readonly IUserService _userService;
        readonly ISandboxActivityCreationService _sandboxedActivityCreationService;

        #endregion

        #region Properties

        public ActivityType ActivityType { get; set; }

        public string TableLabelTitle { get; set; }
        public string SearchPlaceholderText { get; set; }
        public string SearchText { get; set; }
        public string InviteText { get; set; }

        public SilentObservableCollection<ParticipantViewModel> YuFitFriends { get; private set; }

        public CommandViewModel ParticipantSelectedCommand { get; private set; }
        public CommandViewModel InviteMoreFriendsCommand { get; private set; }

        #endregion

        #region Construction / Destruction

        public SelectParticipantsViewModel(
            IUserService userService,
            ISandboxActivityCreationService sandboxedActivityCreationService
        ) : base("activity.invite.participants.title")
        {
            _userService = userService;
            _sandboxedActivityCreationService = sandboxedActivityCreationService;

            YuFitFriends = new SilentObservableCollection<ParticipantViewModel>();
            InviteText = Localized("activity.invite.participants.invite.text");

            ParticipantSelectedCommand = new CommandViewModel("", new MvxCommand<ParticipantViewModel>(ParticipantSelected));
            InviteMoreFriendsCommand = new CommandViewModel(new MvxCommand(InviteFriends));//ShowViewModel<FindFriendsViewModel>()));

            ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(activity => activity.TypeName == _sandboxedActivityCreationService.SandboxedActivity.Sport);

        }

        #endregion

        #region Methods

        public override void Start ()
        {
            base.Start();
            FetchFriendsList();
        }

        protected override void OnClosing ()
        {
            base.OnClosing();
            Save();
        }

        protected void Save ()
        {
            List<FitActivityParticipant> selectedParticipants = new List<FitActivityParticipant>();

            var selectedParticipantsVM = YuFitFriends.Where(participant => participant.IsSelected);
            selectedParticipantsVM.ForEach(participant => selectedParticipants.Add(new FitActivityParticipant {
                UserId = participant.Id,
                Invited = true
            }));

            _sandboxedActivityCreationService.Update(selectedParticipants);
        }

        #endregion

        void ParticipantSelected (ParticipantViewModel participant)
        {
            participant.IsSelected = !participant.IsSelected;
        }

        async void FetchFriendsList ()
        {
            ProgressMessage = Localized("activity.invite.participants.fetch.progress.message");

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var friends = await _userService.GetMyFriends(token);
                    if (friends != null) {
                        var tmp = new List<ParticipantViewModel>();
                        friends.ForEach(friend => tmp.Add(new ParticipantViewModel(friend)));

                        var orderedList = tmp.OrderBy(x => x.DisplayName).ToList();

                        YuFitFriends.Clear();
                        YuFitFriends.AddMultiple(orderedList);
                    }

                    var selectedParticipants = _sandboxedActivityCreationService.SandboxedActivity.Participants;
                    selectedParticipants.ForEach(sp => YuFitFriends.Where(p => p.Id == sp.UserId).ForEach(p => p.IsSelected = true));

                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        IFacebookFriendInvitor invitor;
        void InviteFriends()
        {
            var fbService = Mvx.Resolve<IFacebookService>();
            invitor = fbService.GetInvitor();
            invitor.OperationComplete = (canceled, error) => {
                invitor.OperationComplete = null;
                invitor = null;
            };
            invitor.InviteFacebookFriends();

        }
    }
}

