﻿
namespace YuFit.Core.ViewModels.CreateActivity
{
    public class CreateFitActivityViewModel : BaseCreateActivityViewModel
    {
        public CreateFitActivityViewModel() : base("activity.create.title")
        {
        }
    }
}

