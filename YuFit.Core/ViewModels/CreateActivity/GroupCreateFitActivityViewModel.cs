﻿using System.Collections.Generic;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.Models.Activities;

using YuFit.WebServices.Interface.Model;
using System;
using Xamarin;
using YuFit.Core.Messages;
using System.Threading.Tasks;

namespace YuFit.Core.ViewModels.CreateActivity
{
    public class GroupCreateFitActivityViewModel : BaseCreateActivityViewModel
    {
        private string _groupId;
        readonly IGroupService _groupService;

        public GroupCreateFitActivityViewModel (
            IGroupService groupService
        ) : base("group.create.activity.title")
        {
            _groupService = groupService;
            FriendText = Localized("group.create.activity.who.friends.label");
            InvitedText  = Localized("group.create.activity.who.invited.label");
        }

        public class CreateFitActivityVMNavigation { public string GroupId { get; set; } }
        public void Init(CreateFitActivityVMNavigation navParams)
        {
            _groupId = navParams.GroupId;
            LoadViewModel();
        }

        protected async void LoadViewModel ()
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var group = await _groupService.GetGroup(_groupId, token);
                    Mvx.Trace(group.Name);

                    var members = await _groupService.GetMembers(_groupId, token);
                    var selectedParticipants = new List<FitActivityParticipant>();

                    members.ForEach(participant => selectedParticipants.Add(new FitActivityParticipant {
                        UserId = participant.User.Id,
                        Invited = true
                    }));

                    SandboxedActivityCreationService.Update(selectedParticipants);

                    SandboxedActivityCreationService.SandboxedActivity.UserGroupId = _groupId;
                    SandboxedActivityCreationService.SandboxedActivity.UserGroupAvatarUrl = group.GroupAvatar.ToString();;

                    var lastLocation = Mvx.Resolve<IFileIoService>().Read<Location>(string.Format("groupid.{0}.Location", _groupId));
                    if (lastLocation != null) {SandboxedActivityCreationService.Update(lastLocation);}

                    var lastSport = Mvx.Resolve<IFileIoService>().Read(string.Format("groupid.{0}.Sport", _groupId));
                    var lastActivityInfo = Mvx.Resolve<IFileIoService>().Read(string.Format("groupid.{0}.Activity", _groupId));


                    if (!string.IsNullOrEmpty(lastSport)) {
                        const string ActSettingsVMFormat = "YuFit.Core.Models.Activities.{0}ActivitySettings";
                        string className = string.Format(ActSettingsVMFormat, lastSport);
                        Type classType = Type.GetType(className);

                        if (classType != null) {
                            var activitySettings = Newtonsoft.Json.JsonConvert.DeserializeObject(lastActivityInfo, classType);
                            SandboxedActivityCreationService.Update((ActivitySettings) activitySettings);
                            SandboxedActivityCreationService.Update(lastSport);
                        }
                    }
                });
            } catch (Exception exception) {
                Insights.Report(exception);
                Mvx.Trace(exception.ToString());
            }
        }
            
        protected override async Task Save ()
        {
            try
            {
                var sandboxedActivity = SandboxedActivityCreationService.SandboxedActivity;
                sandboxedActivity.UserGroupId = _groupId;

                Mvx.Resolve<IFileIoService>().Write<ILocation>(string.Format("groupid.{0}.Location", _groupId), sandboxedActivity.Location);
                Mvx.Resolve<IFileIoService>().Write(string.Format("groupid.{0}.Sport", _groupId), sandboxedActivity.Sport);
                Mvx.Resolve<IFileIoService>().Write<ActivitySettings>(string.Format("groupid.{0}.Activity", _groupId), sandboxedActivity.ActivitySettings);

                await base.Save();

                Messenger.Publish(new GroupEventCreatedMessage(this, _groupId));
            }
            catch (Exception exception)
            {
                Mvx.Trace(exception.ToString());
            }
        }

        protected override void OnClosing ()
        {
            SandboxedActivityCreationService.ResetCurrentActivity();
            base.OnClosing();
        }

    }
}

