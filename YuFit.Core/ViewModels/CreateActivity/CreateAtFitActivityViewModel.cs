﻿using System;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using Coc.MvvmCross.Plugins.Location;
using Newtonsoft.Json;
using Xamarin;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;

namespace YuFit.Core.ViewModels.CreateActivity
{
    public class CreateAtFitActivityViewModel : BaseCreateActivityViewModel
    {
        readonly IGeoLocationConverterService _locationConverterService;

        CocLocation _location;

        public CreateAtFitActivityViewModel (
            IGeoLocationConverterService locationConverterService
        ) : base("activity.create.title")
        {
            _locationConverterService = locationConverterService;
        }
    
        public class CreateAtFitActivityVMNavigation { 
            public string LocationJSON { get; set; }

            public CreateAtFitActivityVMNavigation ()
            {
            }

            public CreateAtFitActivityVMNavigation (CocLocation location)
            {
                LocationJSON = JsonConvert.SerializeObject(location, Formatting.None, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Objects,
                    TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
                });
            }

            public CocLocation GetLocation()
            {
                return (CocLocation) JsonConvert.DeserializeObject(LocationJSON, typeof(CocLocation), new JsonSerializerSettings {
                    TypeNameHandling = TypeNameHandling.Objects
                });
            }
        }

        public void Init(CreateAtFitActivityVMNavigation navParams)
        {
            Mvx.Trace("Init VIEWMODEL for CREATE");
            _location = navParams.GetLocation();
            LoadViewModel();
        }

        protected async void LoadViewModel ()
        {
            Mvx.Trace("Loading VIEWMODEL for CREATE");

            try {
                var pl = await _locationConverterService.ReverseGeocodeLocationAsync(_location);
                if (pl != null) {
                    SandboxedActivityCreationService.Update(pl.ToLocation());
                }
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }
    }
}

