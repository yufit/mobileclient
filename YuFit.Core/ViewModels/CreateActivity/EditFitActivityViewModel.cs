﻿using System.Linq;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.ViewModels.Activity.Participant;
using System;
using System.Threading.Tasks;

namespace YuFit.Core.ViewModels.CreateActivity
{
    public class EditFitActivityViewModel : BaseCreateActivityViewModel
    {
        protected string ActivityId { get; private set; }

        public CommandViewModel DeleteActivityCommand     { get; private set; }

        public EditFitActivityViewModel () : base("activity.edit.title")
        {
            DeleteActivityCommand       = new CommandViewModel(new MvxCommand(DeleteActivity       ));
            FriendText                  = Localized("activity.edit.are.in.label");
        }

        public class Navigation { public string ActivityId { get; set; } }

        public virtual void Init(Navigation navParams)
        {
            ActivityId = navParams.ActivityId;
            LoadViewModel();
        }

        async void DeleteActivity() 
        {
            if (await UserDialogs.ConfirmAsync(
                Localized("activity.edit.delete.message.text"), 
                Localized("activity.edit.delete.message.title"), 
                Localized("activity.edit.delete.message.ok.button"),
                Localized("activity.edit.delete.message.cancel.button")))
            {
                ProgressMessage = Localized("activity.edit.delete.progress.message");

                try {
                    await ExecuteCancelableWebRequestAsync(async (token, tryCount) => await SandboxedActivityCreationService.DeleteCurrentActivity(token));
                } catch (Exception exception) {
                    Mvx.Trace(exception.ToString());
                }

                Close(this);
            }
        }

        protected override void OnClosing ()
        {
            SandboxedActivityCreationService.ResetCurrentActivity();
            base.OnClosing();
        }

        protected async void LoadViewModel ()
        {
            try
            {
                await ExecuteCancelableWebRequestAsync(async (token, tryNum) => {
                    await SandboxedActivityCreationService.BeginEditingActivity(ActivityId, token);

                    var sandboxedActivity = SandboxedActivityCreationService.SandboxedActivity;

                    Schedule = ((EventSchedule) sandboxedActivity.EventSchedule);
                    StartTime = Schedule.LocalStartTime;
                    ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(activity => activity.TypeName == sandboxedActivity.Sport);

                    Location = sandboxedActivity.Location;
                    Participants = sandboxedActivity.Participants.Cast<FitActivityParticipant>().ToList();
                    Description = sandboxedActivity.Description;
                    Private = sandboxedActivity.IsPrivate.HasValue && sandboxedActivity.IsPrivate.Value;
                    UserGroupId = sandboxedActivity.UserGroupId;

                    ParticipantsCount = Participants.Count(p => p.IsGoing).ToString();
                    InvitedText = "";

                    UpdateActivityViewModel();
                });
            }
            catch (Exception ex)
            {
                Mvx.Trace(ex.ToString());
            }

        }

        protected override void SelectParticipants ()
        {
            ShowViewModel<DisplayParticipantsViewModel>(new DisplayParticipantsViewModel.Nav { Id = ActivityId });
        }

        protected override async Task Save ()
        {
            var sandboxedActivity = SandboxedActivityCreationService.SandboxedActivity;
            sandboxedActivity.Description = Description;

            ProgressMessage = Localized("activity.edit.save.progress.message");

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => await SandboxedActivityCreationService.SaveCurrentSandboxedActivity(token));
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }

            Close(this);
        }
    }
}

