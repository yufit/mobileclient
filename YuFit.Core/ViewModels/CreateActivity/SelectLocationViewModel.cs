﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Coc.MvvmCross.Plugins.Location;

using Xamarin;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.ViewModels.MapAnnotations;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;

namespace YuFit.Core.ViewModels.CreateActivity
{
    /// <summary>
    /// Select location view model.  This is the view  model dictating the work
    /// flow of the activity location selection.
    /// </summary>
    public class SelectLocationViewModel : BaseViewModel
    {
        #region Data Members

        readonly IGeoLocationConverterService _locationConverterService;
        readonly ICocLocationProvider _locationProvider;
        readonly ISandboxActivityCreationService _sandboxedActivityCreationService;

        #endregion

        #region Properties
        FitActivityMapAnnotationViewModel _pinVm;

        public MapAnnotationObservableCollection Annotations { get; private set; }
        public ActivityType ActivityType { get; set; }

        private Location _selectedLocation;
        public Location SelectedLocation {
            get { return _selectedLocation; }
            set { _selectedLocation = value; RaisePropertyChanged(); }
        }

        /// <summary>
        /// The current region.  As of this moment, this is not being  used for
        /// anything.  The idea is to eventually use this to  filter the search
        /// results to the map current radius (ie if a user searches for Dunkin
        /// Donuts, instead of a valid  address, we  would only search for some
        /// in the current map visible region.
        /// </summary>
        private Region _currentRegion;
        public Region CurrentRegion
        {
            get { return _currentRegion; }
            set { _currentRegion = value;  RaisePropertyChanged(); }
        }

        /// <summary>
        /// The selected location.
        /// </summary>
        private CocLocation _selectedCoordinates;
        public CocLocation SelectedCoordinates
        {
            get { return _selectedCoordinates; }
            set { 
                _selectedCoordinates = value;  
                _pinVm.Location = new Coordinate(value.Coordinates.Latitude, value.Coordinates.Longitude);
                RaisePropertyChanged();  
                UpdateAddress();  
            }
        }

        /// <summary>
        /// The search term.  This is databound to a search widget. We use this
        /// to show the currently selected address, as well  as the text string
        /// used to search for a specific location matching the search term.
        /// </summary>
        private string _searchTerm;
        public string SearchTerm
        {
            get { return _searchTerm; }
            set { _searchTerm = value; RaisePropertyChanged(); }
        }

        public CommandViewModel SearchCommand { get; private set; }

        #endregion

        #region Construction / Destruction

        /// <summary>
        /// Initializes a new instance of the <see cref="YuFit.Core.ViewModels.CreateActivity.SelectLocationViewModel"/> class.
        /// We only use the locationWatcher to get the current location in case
        /// no event were selected.
        /// </summary>
        /// <param name="locationConverterService">Location converter service.</param>
        /// <param name="locationManager"></param>
        /// <param name="locationProvider"></param>
        /// <param name="sandboxedActivityCreationService"></param>
        public SelectLocationViewModel(
            IGeoLocationConverterService locationConverterService,
            ICocLocationManager locationManager,
            ICocLocationProvider locationProvider,
            ISandboxActivityCreationService sandboxedActivityCreationService
        ) : base("activity.create.select.location.title")
        {
            _locationProvider = locationProvider;
            _locationConverterService = locationConverterService;
            _sandboxedActivityCreationService = sandboxedActivityCreationService;

            Annotations = new MapAnnotationObservableCollection();

            ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(activity => activity.TypeName == _sandboxedActivityCreationService.SandboxedActivity.Sport);

            _searchTerm = string.Empty;
            SearchCommand = new CommandViewModel(new MvxCommand(Search));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Start this instance. When starting, our radius decision is based on
        /// wether or not there is a selected location already specified in the
        /// sandboxed activity.  If there is, we set the radius to really small
        /// value to have the user zoomed in on the  location, otherwise we set
        /// the map center to the current user location along with a fairly big
        /// radius.
        /// </summary>
        public override void Start ()
        {
            base.Start();

            var activityLocation = _sandboxedActivityCreationService.SandboxedActivity.Location;
            _selectedLocation = (Location) activityLocation;

            if (_selectedLocation != null && _selectedLocation.Coordinates != null) {
                Coordinate coordinates = _selectedLocation.GeoCoordinates;
                _searchTerm = _selectedLocation.ToString();
                _selectedCoordinates = new CocLocation { Coordinates = new CocCoordinates { Latitude = coordinates.Latitude, Longitude = coordinates.Longitude } };
                _currentRegion = new Region {
                    Center = new Coordinate(_selectedCoordinates.Coordinates.Latitude, _selectedCoordinates.Coordinates.Longitude),
                    Radius = 10
                };
            } else {
                var currentRegion = Mvx.Resolve<ISandboxService>().GetItem<Region>("currentRegion");
                if (currentRegion != null)
                {
                    _currentRegion = new Region {
                        Center = currentRegion.Center,
                        Radius = currentRegion.Radius
                    };
                }
                else {
                    CocLocation currentLocation = _locationProvider.TryGetLocationUsingTimezone();
                    _currentRegion = new Region {
                        Center = new Coordinate(currentLocation.Coordinates.Latitude, currentLocation.Coordinates.Longitude),
                        Radius = 1000
                    };
                    Task.Factory.StartNew(() => InvokeOnMainThread(CenterOnCurrentLocation));
                }
                _selectedCoordinates = null;
            }

            var sport = "Cycling";
            if (!string.IsNullOrEmpty(_sandboxedActivityCreationService.SandboxedActivity.Sport)) {
                sport = _sandboxedActivityCreationService.SandboxedActivity.Sport;
            }
            var activityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(activity => activity.TypeName == sport);

            var isGroupActivity = !string.IsNullOrEmpty(_sandboxedActivityCreationService.SandboxedActivity.UserGroupId);

            if (isGroupActivity) {
                _pinVm = new GroupFitActivityMapAnnotationViewModel
                {
                    Id = _sandboxedActivityCreationService.SandboxedActivity.Id,
                    DisplayName = _sandboxedActivityCreationService.SandboxedActivity.Name,
                    Location = _selectedLocation?.GeoCoordinates,
                    ActivityType = activityType,
                    Invited = false,
                    AvatarUrl = _sandboxedActivityCreationService.SandboxedActivity.UserGroupAvatarUrl
                };
            } else {
                _pinVm = new FitActivityMapAnnotationViewModel
                {
                    Id = _sandboxedActivityCreationService.SandboxedActivity.Id,
                    DisplayName = _sandboxedActivityCreationService.SandboxedActivity.Name,
                    Location = _selectedLocation?.GeoCoordinates,
                    ActivityType = activityType,
                    Invited = false
                };
            }

            Annotations.Add(_pinVm);
        }

        async void CenterOnCurrentLocation ()
        {
            await Task.Delay(500);
            CocLocation currentLocation = null;
            try
            {
                currentLocation = await _locationProvider.TryGetLocationFromDeviceApi();
            }
            catch (Exception /*ex*/)
            {
                //currentLocation = await _locationProvider.TryGetLocationUsingIpAddress();
            }

            if (currentLocation != null) {
                CurrentRegion = new Region {
                    Center = new Coordinate(currentLocation.Coordinates.Latitude, currentLocation.Coordinates.Longitude),
                    Radius = 0.25
                };
            }
        }

        protected override void OnClosing ()
        {
            base.OnClosing();
            Save();
        }

        /// <summary>
        /// Save this instance.  Gets called when the  user is ok  with his/her
        /// selected location.  We update the sandboxed activity.
        /// </summary>
        protected void Save ()
        {
            if (_selectedCoordinates != null) { _sandboxedActivityCreationService.Update(SelectedLocation); }
        }

        /// <summary>
        /// Search for the current search term content.
        /// 
        /// TODO This needs to tell the user somehow that we are searching, via
        /// spinners, or a string in the search term...
        /// </summary>
        async void Search ()
        {
            try {
                var addresses = await _locationConverterService.GeocodeAddressAsync(SearchTerm);
                if (addresses.Count > 0) { 

                    Placemark pl = addresses[0];
                    _selectedLocation = pl.ToLocation();
                    SearchTerm = _selectedLocation.ToString();

                    SelectedCoordinates = new CocLocation { Coordinates = new CocCoordinates { Latitude = pl.Coordinates.Latitude, Longitude = pl.Coordinates.Longitude } };
                    _currentRegion.Center = new Coordinate(SelectedCoordinates.Coordinates.Latitude, SelectedCoordinates.Coordinates.Longitude);
                    _currentRegion.Radius = 0.25;
                    RaisePropertyChanged(() => CurrentRegion);
                }
            } catch (Exception /*ex*/) {
                SearchTerm = string.Empty;
            }
        }

        /// <summary>
        /// When the selected location is updated via the view data binding, we
        /// issue a reverse geo search to get  an address mapping to  the newly
        /// selected coordinate (latitude, longitude).
        /// 
        /// This will update the searchTerm string to show the found address.
        /// </summary>
        async void UpdateAddress ()
        {
            SearchTerm = Localized("activity.Location.search.progress.message");
            try {
                var address = await _locationConverterService.ReverseGeocodeLocationAsync(SelectedCoordinates);
                _selectedLocation = address?.ToLocation();
                SearchTerm = _selectedLocation?.ToString();
            } catch (Exception ex) {
                SearchTerm = string.Empty;
                Insights.Report(ex);
            }
        }
        #endregion
    }
}

