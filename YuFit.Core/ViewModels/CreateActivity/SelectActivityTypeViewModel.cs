﻿using System;
using System.Collections.ObjectModel;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.ViewModels.Activity;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;

namespace YuFit.Core.ViewModels.CreateActivity
{

    public class SelectActivityTypeViewModel : BaseViewModel
    {
        string _selectedActivityType;
        readonly ISandboxActivityCreationService _sandboxedActivityCreationService;

        #region Properties

        public CommandViewModel ConfigureActivityCommand { get; private set; }
        public ObservableCollection<ActivityTypeCellViewModel> Activities { get; private set; }

        #endregion

        #region Construction / Destruction

        public SelectActivityTypeViewModel(
            ISandboxActivityCreationService sandboxedActivityCreationService
        ) : base ("activity.create.select.type.title")
        {
            Activities = new ObservableCollection<ActivityTypeCellViewModel>();
            _sandboxedActivityCreationService = sandboxedActivityCreationService;
            _selectedActivityType = _sandboxedActivityCreationService.SandboxedActivity.Sport;

            Mvx.Resolve<IFitActivityService>().AvailableActivities.ForEach((activity) => 
                Activities.Add(new ActivityTypeCellViewModel {
                    TypeName = activity.TypeName,
                    IsEnabled = activity.Enabled,
                    IsSelected = _selectedActivityType == activity.TypeName
                }
            ));

            ConfigureActivityCommand = new CommandViewModel(new MvxCommand<ActivityTypeCellViewModel>(ConfigureActivity));
        }

        #endregion

        #region Methods

        void ConfigureActivity (ActivityTypeCellViewModel activityType)
        {
            const string ActivityTypeVMFormat = "YuFit.Core.ViewModels.CreateActivity.ConfigureActivities.{0}ActivityConfigureViewModel";
            string className = string.Format(ActivityTypeVMFormat, activityType.TypeName);
            Type activityViewModelClassType = Type.GetType(className);

            if   (activityViewModelClassType != null) { ShowViewModel(activityViewModelClassType); }
        }

        #endregion
    }
}

