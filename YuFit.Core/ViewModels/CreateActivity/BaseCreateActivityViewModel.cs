﻿using System.Linq;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Models;
using System.Collections.Generic;
using YuFit.Core.ViewModels.DisplayActivities;
using System;
using YuFit.WebServices.Interface.Model;
using System.Threading.Tasks;
using MvvmCross.Plugins.Messenger;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;

namespace YuFit.Core.ViewModels.CreateActivity
{
    /// <summary>
    /// This View Model defines the workflow for the top level "Create Event" 
    /// functionality.
    /// </summary>
    public abstract class BaseCreateActivityViewModel : BaseViewModel
    {
        #pragma warning disable 414
        private readonly MvxSubscriptionToken _sandboxedActivityUpdatedToken;
        #pragma warning restore 414

        protected ISandboxActivityCreationService SandboxedActivityCreationService { get; private set; }
        protected IFitActivityService FitActivityService { get; private set; }

        #region Properties

        public List<FitActivityParticipant>     Participants             { get; set; }
        public BaseActivityDisplayViewModelRaw  ActivityDisplayViewModel { get; set; }

        public string           Title                       { get; set; }
        public string           Description                 { get; set; }
        public string           DescriptionPlaceholder      { get; set; }
        public DateTime         StartTime                   { get; set; }
        public EventSchedule    Schedule                    { get; set; }
        public ILocation        Location                    { get; set; }
        public string           PrivateText                 { get; set; }
        public string           PublicText                  { get; set; }
        public bool             Private                     { get; set; }
        public string           FriendText                  { get; set; }
        public string           InvitedText                 { get; set; }
        public string           ParticipantsCount           { get; set; }
        public string           UserGroupId                 { get; set; }

        public CommandViewModel SelectParticipantsCommand   { get; private set; }
        public CommandViewModel SelectActivityTypeCommand   { get; private set; }
        public CommandViewModel SelectLocationCommand       { get; private set; }
        public CommandViewModel SelectTimeDateCommand       { get; private set; }
        public CommandViewModel ToggleVisibilityCommand     { get; private set; }

        #endregion

        public ActivityType ActivityType { get; set; }

        public CommandViewModel SaveCommand { get; private set; }

        protected BaseCreateActivityViewModel (string displayName) : base(displayName)
        {
            SandboxedActivityCreationService = Mvx.Resolve<ISandboxActivityCreationService>();
            FitActivityService = Mvx.Resolve<IFitActivityService>();

            _sandboxedActivityUpdatedToken = Messenger.Subscribe<SandboxedActivityUpdatedMessage>(OnSandboxedActivityUpdated);

            ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(activity => activity.TypeName == SandboxedActivityCreationService.SandboxedActivity.Sport);

            SaveCommand = new CommandViewModel("", new MvxCommand(async () => await Save()));

            Title                       = Localized("activity.create.title");
            DescriptionPlaceholder      = Localized("activity.create.description.placeholder");
            PublicText                  = Localized("activity.create.privacy.value.public");
            PrivateText                 = Localized("activity.create.privacy.value.private");
            FriendText                  = Localized("activity.create.who.friends.label");
            InvitedText                 = Localized("activity.create.who.invited.label");

            SelectParticipantsCommand   = new CommandViewModel(new MvxCommand(SelectParticipants   ));
            SelectActivityTypeCommand   = new CommandViewModel(new MvxCommand(SelectActivity       ));
            SelectLocationCommand       = new CommandViewModel(new MvxCommand(SelectLocation       ));
            SelectTimeDateCommand       = new CommandViewModel(new MvxCommand(SelectTimeAndDate    ));
            ToggleVisibilityCommand     = new CommandViewModel(Localized("activity.create.toggle.visibility.command"), new MvxCommand(ToggleVisibility));

            var sandboxedActivity       = SandboxedActivityCreationService.SandboxedActivity;

            Schedule        = ((EventSchedule) sandboxedActivity.EventSchedule);
            StartTime       = Schedule.LocalStartTime;
            ActivityType    = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(activity => activity.TypeName == sandboxedActivity.Sport);

            Location        = sandboxedActivity.Location;
            Participants    = sandboxedActivity.Participants.Cast<FitActivityParticipant>().ToList();
            Description     = sandboxedActivity.Description;
            Private         = sandboxedActivity.IsPrivate.HasValue && sandboxedActivity.IsPrivate.Value;
            UpdateActivityViewModel();

            if (sandboxedActivity.IsReady) {
                SaveCommand.DisplayName = Localized("activity.create.its.on.label");
            }

        }

        protected override void OnClosing ()
        {
            Messenger.Unsubscribe<SandboxedActivityUpdatedMessage>(_sandboxedActivityUpdatedToken);
        }

        #region Methods

        protected virtual void SelectActivity     () { ShowViewModel<SelectActivityTypeViewModel>(); }
        protected virtual void SelectLocation     () { ShowViewModel<SelectLocationViewModel>    (); }
        protected virtual void SelectTimeAndDate  () { ShowViewModel<SelectActivityTimeViewModel>(); }
        protected virtual void SelectParticipants () { if (string.IsNullOrEmpty(UserGroupId)) { ShowViewModel<SelectParticipantsViewModel>(); }}

        protected virtual async Task Save ()
        {
            try
            {
                var sandboxedActivity = SandboxedActivityCreationService.SandboxedActivity;
                sandboxedActivity.Description = Description;

                if (sandboxedActivity.IsReady)
                {
                    ProgressMessage = Localized("activity.create.progress.message");

                    await ExecuteCancelableWebRequestAsync(async (token, tryCount) => await SandboxedActivityCreationService.SaveCurrentSandboxedActivity(token));
                }

                Close(this);
            }
            catch (Exception exception)
            {
                Mvx.Trace(exception.ToString());
            }

        }

        protected virtual void OnSandboxedActivityUpdated (SandboxedActivityUpdatedMessage message)
        {
            ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(activity => activity.TypeName == message.Activity.Sport);

            Schedule = ((EventSchedule) message.Activity.EventSchedule);
            StartTime = Schedule.LocalStartTime;
            ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(activity => activity.TypeName == message.Activity.Sport);
            Location = message.Activity.Location;
            Participants = message.Activity.Participants.Cast<FitActivityParticipant>().ToList();
            ParticipantsCount = Participants.Count.ToString();
            UserGroupId = message.Activity.UserGroupId;

            if (message.Activity.IsReady) {
                SaveCommand.DisplayName = Localized("activity.create.its.on.label");
            }

            UpdateActivityViewModel();
        }

        protected void UpdateActivityViewModel ()
        {
            if (ActivityType != null) {

                var sandboxedActivity = SandboxedActivityCreationService.SandboxedActivity;

                const string ActivityTypeVMFormat = "YuFit.Core.ViewModels.DisplayActivities.{0}ActivityDisplayViewModel";
                string className = string.Format(ActivityTypeVMFormat, ActivityType.TypeName);
                Type activityViewModelClassType = Type.GetType(className);

                if (activityViewModelClassType != null) {
                    if (ActivityDisplayViewModel == null || ActivityDisplayViewModel.GetType() != activityViewModelClassType) {
                        ActivityDisplayViewModel = (BaseActivityDisplayViewModelRaw)Activator.CreateInstance(activityViewModelClassType, sandboxedActivity.ActivitySettings);
                    } else {
                        ActivityDisplayViewModel.Update();
                    }
                } else {
                    ActivityDisplayViewModel = null;
                }
            } else {
                ActivityDisplayViewModel = null;
            }
        }

        public void ToggleVisibility()
        {
            var sandboxedActivity = SandboxedActivityCreationService.SandboxedActivity;
            Private = !Private;
            sandboxedActivity.IsPrivate = Private;
        }

        #endregion
    }
}

