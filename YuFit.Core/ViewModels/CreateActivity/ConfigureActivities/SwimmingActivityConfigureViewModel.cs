﻿using YuFit.Core.Models.Activities;
using System.Collections.Generic;
using System;
using YuFit.Core.ViewModels.Helpers;

namespace YuFit.Core.ViewModels.CreateActivity.ConfigureActivities
{
    public class SwimmingActivityConfigureViewModel : BaseActivityConfigureViewModel<SwimmingActivitySettings>
    {
        private const string localPrefixKey = "activity.Swimming.";

        public int Distance
        {
            get { return ActivitySettings.Distance; }
            set { ActivitySettings.Distance = value; RaisePropertyChanged(); }
        }

        public int Duration
        {
            get { return ActivitySettings.Duration; }
            set { ActivitySettings.Duration = value; RaisePropertyChanged(() => Duration); }
        }

        public SwimmingIntensity Intensity
        {
            get { return ActivitySettings.IntensityLevel; }
            set { ActivitySettings.IntensityLevel = value; RaisePropertyChanged(() => Intensity); }
        }

        public SwimmingType SwimmingType
        {
            get { return ActivitySettings.SwimmingType; }
            set { ActivitySettings.SwimmingType = value; RaisePropertyChanged(() => SwimmingType); }
        }

        public List<Tuple<string, object>> DurationValues { get { return YuFit.Core.Models.Activities.DurationValues.Swimming; } }
        public List<Tuple<string, object>> DistanceValues { get { return YuFit.Core.Models.Activities.DistanceValues.Swimming; } }

        public SelectItemInListCommandViewModel<string, SwimmingIntensity> SelectIntensityCommand { get; private set; }
        public SelectItemInListCommandViewModel<string, SwimmingType> SelectTypeCommand { get; private set; }

        public string SwimmingTypeTitle { get; private set; }
        public string DistanceTitle     { get; private set; }
        public string DurationTitle     { get; private set; }
        public string TrainingTypeTitle { get; private set; }
        public string DistanceUnit      { get; private set; }
        public string DurationUnit      { get; private set; }

        public SwimmingActivityConfigureViewModel () : base(localPrefixKey + "get.fit.label")
        {
            SwimmingTypeTitle = Localized(localPrefixKey + "subsport.label");
            DistanceTitle     = Localized(localPrefixKey + "distance.label");
            DurationTitle     = Localized(localPrefixKey + "duration.label");
            TrainingTypeTitle = Localized(localPrefixKey + "intensity.label");

            SelectIntensityCommand = CreateSelectFromEnumCommand(Intensity, "activity.intensity.value.{0}", SelectIntensity);
            SelectTypeCommand = CreateSelectFromEnumCommand(SwimmingType, localPrefixKey + "subsport.value.{0}", SelectType);

            DistanceUnit = "M";
            DurationUnit = "H";
        }

        void SelectIntensity (SwimmingIntensity intensity) { Intensity = intensity; }
        void SelectType      (SwimmingType type)           { SwimmingType = type; }
    }
}

