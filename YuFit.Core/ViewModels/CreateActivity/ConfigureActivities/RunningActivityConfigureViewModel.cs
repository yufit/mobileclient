﻿using YuFit.Core.Models.Activities;
using System.Collections.Generic;
using System;
using YuFit.Core.ViewModels.Helpers;


namespace YuFit.Core.ViewModels.CreateActivity.ConfigureActivities
{
    public class RunningActivityConfigureViewModel : BaseActivityConfigureViewModel<RunningActivitySettings>
    {
        private const string localPrefixKey = "activity.Running.";

        public int Distance
        {
            get { return ActivitySettings.Distance; }
            set { ActivitySettings.Distance = value; RaisePropertyChanged(() => Distance); RaisePropertyChanged(() => Speed); }
        }

        public int Duration
        {
            get { return ActivitySettings.Duration; }
            set { ActivitySettings.Duration = value; RaisePropertyChanged(() => Duration); RaisePropertyChanged(() => Speed); }
        }

        public RunningIntensity Intensity
        {
            get { return ActivitySettings.IntensityLevel; }
            set { ActivitySettings.IntensityLevel = value; RaisePropertyChanged(() => Intensity); }
        }

        public RunningType RunningType
        {
            get { return ActivitySettings.RunningType; }
            set { ActivitySettings.RunningType = value; RaisePropertyChanged(() => RunningType); }
        }

        public string Speed { 
            get {
                int minutes = (int) ActivitySettings.Speed;
                int seconds = (int) ((ActivitySettings.Speed - minutes) * 60);

                return string.Format("{0:00}:{1:00}", minutes, seconds); 
            } 
        }

        public string RunningTypeTitle  { get; private set; }
        public string DistanceTitle     { get; private set; }
        public string DurationTitle     { get; private set; }
        public string TrainingTypeTitle { get; private set; }
        public string SpeedUnit         { get; private set; }
        public string DistanceUnit      { get; private set; }
        public string DurationUnit      { get; private set; }

        public List<Tuple<string, object>> DurationValues { get { return YuFit.Core.Models.Activities.DurationValues.Running; } }
        public List<Tuple<string, object>> DistanceValues { get { return YuFit.Core.Models.Activities.DistanceValues.Running; } }


        public SelectItemInListCommandViewModel<string, RunningIntensity> SelectIntensityCommand { get; private set; }
        public SelectItemInListCommandViewModel<string, RunningType>      SelectRunningTypeCommand { get; private set; }

        public RunningActivityConfigureViewModel () : base(localPrefixKey + "get.fit.label")
        {
            RunningTypeTitle  = Localized(localPrefixKey + "subsport.label");
            DistanceTitle     = Localized(localPrefixKey + "distance.label");
            DurationTitle     = Localized(localPrefixKey + "duration.label");
            TrainingTypeTitle = Localized(localPrefixKey + "intensity.label");

            SelectIntensityCommand = CreateSelectFromEnumCommand(Intensity, "activity.intensity.value.{0}", SelectIntensity);
            SelectRunningTypeCommand = CreateSelectFromEnumCommand(RunningType, localPrefixKey + "subsport.value.{0}", SelectType);

            // TODO This needs to be using the system local to use mph or km
            SpeedUnit = "MIN/KM";
            DistanceUnit = "KM";
            DurationUnit = "H";
        }

        void SelectIntensity (RunningIntensity intensity) { Intensity = intensity; }
        void SelectType      (RunningType type)           { RunningType = type; }
    }
}

