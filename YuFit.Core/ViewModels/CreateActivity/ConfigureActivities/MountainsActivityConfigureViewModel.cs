﻿using YuFit.Core.Models.Activities;
using System.Collections.Generic;
using System;
using YuFit.Core.ViewModels.Helpers;


namespace YuFit.Core.ViewModels.CreateActivity.ConfigureActivities
{
    public class MountainsActivityConfigureViewModel : BaseActivityConfigureViewModel<MountainsActivitySettings>
    {
        private const string localPrefixKey = "activity.Mountains.";

        public int Distance
        {
            get { return ActivitySettings.Distance; }
            set { ActivitySettings.Distance = value; RaisePropertyChanged(() => Distance); RaisePropertyChanged(() => Speed); }
        }

        public int Duration
        {
            get { return ActivitySettings.Duration; }
            set { ActivitySettings.Duration = value; RaisePropertyChanged(() => Duration); RaisePropertyChanged(() => Speed); }
        }

        public MountainsIntensity Intensity
        {
            get { return ActivitySettings.IntensityLevel; }
            set { ActivitySettings.IntensityLevel = value; RaisePropertyChanged(() => Intensity); }
        }

        public MountainsType MountainsType
        {
            get { return ActivitySettings.MountainsType; }
            set { ActivitySettings.MountainsType = value; RaisePropertyChanged(() => MountainsType); }
        }

        public string Speed { 
            get {
                int minutes = (int) ActivitySettings.Speed;
                int seconds = (int) ((ActivitySettings.Speed - minutes) * 60);

                return string.Format("{0:00}:{1:00}", minutes, seconds); 
            } 
        }

        public string MountainsTypeTitle  { get; private set; }
        public string DistanceTitle     { get; private set; }
        public string DurationTitle     { get; private set; }
        public string TrainingTypeTitle { get; private set; }
        public string SpeedUnit         { get; private set; }
        public string DistanceUnit      { get; private set; }
        public string DurationUnit      { get; private set; }

        public List<Tuple<string, object>> DurationValues { get { return YuFit.Core.Models.Activities.DurationValues.Mountains; } }
        public List<Tuple<string, object>> DistanceValues { get { return YuFit.Core.Models.Activities.DistanceValues.Mountains; } }


        public SelectItemInListCommandViewModel<string, MountainsIntensity> SelectIntensityCommand { get; private set; }
        public SelectItemInListCommandViewModel<string, MountainsType>      SelectMountainsTypeCommand { get; private set; }

        public MountainsActivityConfigureViewModel () : base(localPrefixKey + "get.fit.label")
        {
            MountainsTypeTitle  = Localized(localPrefixKey + "subsport.label");
            DistanceTitle     = Localized(localPrefixKey + "distance.label");
            DurationTitle     = Localized(localPrefixKey + "duration.label");
            TrainingTypeTitle = Localized(localPrefixKey + "intensity.label");

            SelectIntensityCommand = CreateSelectFromEnumCommand(Intensity, "activity.intensity.value.{0}", SelectIntensity);
            SelectMountainsTypeCommand = CreateSelectFromEnumCommand(MountainsType, localPrefixKey + "subsport.value.{0}", SelectType);

            // TODO This needs to be using the system local to use mph or km
            SpeedUnit = "KM/H";
            DistanceUnit = "KM";
            DurationUnit = "H";
        }

        void SelectIntensity (MountainsIntensity intensity) { Intensity = intensity; }
        void SelectType      (MountainsType type)           { MountainsType = type; }
    }
}

