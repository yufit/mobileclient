﻿using YuFit.Core.Models.Activities;
using System.Collections.Generic;
using System;
using YuFit.Core.ViewModels.Helpers;

namespace YuFit.Core.ViewModels.CreateActivity.ConfigureActivities
{
    public class YogaActivityConfigureViewModel : BaseActivityConfigureViewModel<YogaActivitySettings>
    {
        private const string localPrefixKey = "activity.Yoga.";

        public int Duration
        {
            get { return ActivitySettings.Duration; }
            set { ActivitySettings.Duration = value; RaisePropertyChanged(() => Duration); }
        }

        public YogaIntensity Intensity
        {
            get { return ActivitySettings.IntensityLevel; }
            set { ActivitySettings.IntensityLevel = value; RaisePropertyChanged(() => Intensity); }
        }

        public YogaType YogaType
        {
            get { return ActivitySettings.YogaType; }
            set { ActivitySettings.YogaType = value; RaisePropertyChanged(() => YogaType); }
        }

        public List<Tuple<string, object>> DurationValues { get { return YuFit.Core.Models.Activities.DurationValues.Yoga; } }

        public SelectItemInListCommandViewModel<string, YogaIntensity> SelectIntensityCommand { get; private set; }
        public SelectItemInListCommandViewModel<string, YogaType> SelectTypeCommand { get; private set; }

        public string YogaTypeTitle     { get; private set; }
        public string DurationTitle     { get; private set; }
        public string TrainingTypeTitle { get; private set; }
        public string DurationUnit      { get; private set; }

        public YogaActivityConfigureViewModel () : base(localPrefixKey + "get.fit.label")
        {
            YogaTypeTitle     = Localized(localPrefixKey + "subsport.label");
            DurationTitle     = Localized(localPrefixKey + "duration.label");
            TrainingTypeTitle = Localized(localPrefixKey + "intensity.label");

            SelectIntensityCommand = CreateSelectFromEnumCommand(Intensity, "activity.intensity.value.{0}", SelectIntensity);
            SelectTypeCommand      = CreateSelectFromEnumCommand(YogaType, localPrefixKey + "subsport.value.{0}", SelectType);
            DurationUnit = "H";
        }

        void SelectIntensity (YogaIntensity intensity) { Intensity = intensity; }
        void SelectType      (YogaType type)           { YogaType = type; }
    }
}


