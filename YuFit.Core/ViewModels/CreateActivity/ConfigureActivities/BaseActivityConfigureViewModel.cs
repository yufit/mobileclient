﻿using System;
using System.Collections.Generic;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models.Activities;
using YuFit.Core.ViewModels.Helpers;

namespace YuFit.Core.ViewModels.CreateActivity.ConfigureActivities
{
    /// <summary>
    /// This is needed in the presenter.  We need to do logic based on the type
    /// of view model and  BaseActivityConfigureViewModel being templatized was
    /// causing me grief, so i've created this intermediate dummy class to help.
    /// </summary>
    public abstract class BaseActivityConfigureVM : BaseViewModel {
        protected BaseActivityConfigureVM (string displayName) : base(displayName)
        {
            
        }
    };

    /// <summary>
    /// Base activity configure view model.
    /// </summary>
    public class BaseActivityConfigureViewModel<T> : BaseActivityConfigureVM where T: ActivitySettings, new()
    {
        public T ActivitySettings { get; private set; }
        protected ISandboxActivityCreationService SandboxedActivityCreationService { get; private set; }

        /// <summary>
        /// Initializes a new instance of the BaseActivityConfigureViewModel class.
        /// </summary>
        /// <param name="displayName">Display name.</param>
        public BaseActivityConfigureViewModel (string displayName) : base(displayName)
        {
            SandboxedActivityCreationService = Mvx.Resolve<ISandboxActivityCreationService>();
            ActivitySettings = SandboxedActivityCreationService.SandboxedActivity.ActivitySettings as T; 
            if (ActivitySettings == null) {
                // Load from the cache first to get last selected settings.
                // If there is nothing cached to disk, the read will return a new instance
                // which will be setup to default values.
                ActivitySettings = Mvx.Resolve<IFileIoService>().Read<T>(typeof(T).ToString());

                // This could happen if the content of the cache is corrupted.
                if (ActivitySettings == null) {
                    ActivitySettings = new T();
                }
            }
        }

        protected override void OnClosing ()
        {
            base.OnClosing();
            Save();
        }

        /// <summary>
        /// Save this instance into the sandboxed activity and close the vm.
        /// </summary>
        protected void Save ()
        {
            SandboxedActivityCreationService.Update(ActivitySettings);
            Mvx.Resolve<IFileIoService>().Write<T>(typeof(T).ToString(), ActivitySettings);
        }

    }
}

