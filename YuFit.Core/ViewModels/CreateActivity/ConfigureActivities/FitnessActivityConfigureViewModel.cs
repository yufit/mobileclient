﻿using YuFit.Core.Models.Activities;
using System.Collections.Generic;
using System;
using YuFit.Core.ViewModels.Helpers;

namespace YuFit.Core.ViewModels.CreateActivity.ConfigureActivities
{
    public class FitnessActivityConfigureViewModel : BaseActivityConfigureViewModel<FitnessActivitySettings>
    {
        private const string localPrefixKey = "activity.Fitness.";

        public int Duration
        {
            get { return ActivitySettings.Duration; }
            set { ActivitySettings.Duration = value; RaisePropertyChanged(() => Duration); }
        }

        public FitnessIntensity Intensity
        {
            get { return ActivitySettings.IntensityLevel; }
            set { ActivitySettings.IntensityLevel = value; RaisePropertyChanged(() => Intensity); }
        }

        public FitnessType FitnessType
        {
            get { return ActivitySettings.FitnessType; }
            set { ActivitySettings.FitnessType = value; RaisePropertyChanged(() => FitnessType); }
        }

        public List<Tuple<string, object>> DurationValues { get { return YuFit.Core.Models.Activities.DurationValues.Fitness; } }

        public SelectItemInListCommandViewModel<string, FitnessIntensity> SelectIntensityCommand { get; private set; }
        public SelectItemInListCommandViewModel<string, FitnessType> SelectTypeCommand { get; private set; }

        public string FitnessTypeTitle     { get; private set; }
        public string DurationTitle     { get; private set; }
        public string TrainingTypeTitle { get; private set; }
        public string DurationUnit      { get; private set; }

        public FitnessActivityConfigureViewModel () : base(localPrefixKey + "get.fit.label")
        {
            FitnessTypeTitle     = Localized(localPrefixKey + "subsport.label");
            DurationTitle     = Localized(localPrefixKey + "duration.label");
            TrainingTypeTitle = Localized(localPrefixKey + "intensity.label");

            SelectIntensityCommand = CreateSelectFromEnumCommand(Intensity, "activity.intensity.value.{0}", SelectIntensity);
            SelectTypeCommand      = CreateSelectFromEnumCommand(FitnessType, localPrefixKey + "subsport.value.{0}", SelectType);
            DurationUnit = "H";
        }

        void SelectIntensity (FitnessIntensity intensity) { Intensity = intensity; }
        void SelectType      (FitnessType type)           { FitnessType = type; }
    }
}


