﻿using YuFit.Core.Models.Activities;
using System.Collections.Generic;
using System;
using YuFit.Core.ViewModels.Helpers;

namespace YuFit.Core.ViewModels.CreateActivity.ConfigureActivities
{
    public class CyclingActivityConfigureViewModel : BaseActivityConfigureViewModel<CyclingActivitySettings>
    {
        private const string localPrefixKey = "activity.Cycling.";

        public int Distance
        {
            get { return ActivitySettings.Distance; }
            set { ActivitySettings.Distance = value; RaisePropertyChanged(() => Distance); RaisePropertyChanged(() => Speed); }
        }

        public int Duration
        {
            get { return ActivitySettings.Duration; }
            set { ActivitySettings.Duration = value; RaisePropertyChanged(() => Duration); RaisePropertyChanged(() => Speed); }
        }

        public CyclingIntensity Intensity
        {
            get { return ActivitySettings.IntensityLevel; }
            set { ActivitySettings.IntensityLevel = value; RaisePropertyChanged(() => Intensity); }
        }

        public CyclingType CyclingType
        {
            get { return ActivitySettings.CyclingType; }
            set { ActivitySettings.CyclingType = value; RaisePropertyChanged(() => CyclingType); }
        }

        public string Speed { get { return string.Format("{0:0.00}", ActivitySettings.Speed); } }

        public string CyclingTypeTitle  { get; private set; }
        public string DistanceTitle     { get; private set; }
        public string DurationTitle     { get; private set; }
        public string TrainingTypeTitle { get; private set; }
        public string SpeedUnit         { get; private set; }
        public string DistanceUnit      { get; private set; }
        public string DurationUnit      { get; private set; }

        public List<Tuple<string, object>> DurationValues { get { return YuFit.Core.Models.Activities.DurationValues.Cycling; } }
        public List<Tuple<string, object>> DistanceValues { get { return YuFit.Core.Models.Activities.DistanceValues.Cycling; } }

        public SelectItemInListCommandViewModel<string, CyclingIntensity> SelectIntensityCommand { get; private set; }
        public SelectItemInListCommandViewModel<string, CyclingType> SelectCyclingTypeCommand { get; private set; }

        public CyclingActivityConfigureViewModel () : base(localPrefixKey + "get.fit.label")
        {
            CyclingTypeTitle  = Localized(localPrefixKey + "subsport.label");
            DistanceTitle     = Localized(localPrefixKey + "distance.label");
            DurationTitle     = Localized(localPrefixKey + "duration.label");
            TrainingTypeTitle = Localized(localPrefixKey + "intensity.label");

            SelectIntensityCommand = CreateSelectFromEnumCommand(Intensity, "activity.intensity.value.{0}", SelectIntensity);
            SelectCyclingTypeCommand = CreateSelectFromEnumCommand(CyclingType, localPrefixKey + "subsport.value.{0}", SelectType);

            // TODO This needs to be using the system local to use mph or km
            SpeedUnit = "KM/H";
            DistanceUnit = "KM";
            DurationUnit = "H";
        }

        void SelectIntensity (CyclingIntensity intensity) { Intensity = intensity; }
        void SelectType      (CyclingType type)           { CyclingType = type; }
    }
}

