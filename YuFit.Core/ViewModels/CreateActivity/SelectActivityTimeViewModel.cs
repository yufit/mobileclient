﻿using System;

using YuFit.Core.Extensions;
using YuFit.Core.Models;
using YuFit.Core.ViewModels.Helpers;

using YuFit.WebServices.Interface.Model;
using YuFit.Core.Interfaces.Services;
using System.Linq;
using MvvmCross.Platform;

namespace YuFit.Core.ViewModels.CreateActivity
{
    public class SelectActivityTimeViewModel : BaseViewModel
	{
        #region Constants
        private const string localPrefixKey = "activity.Time.";
        readonly ISandboxActivityCreationService _sandboxedActivityCreationService;
        #endregion

		#region Properties

        public ActivityType ActivityType { get; set; }

        public DateTime Today { get { return DateTime.Now; } }

        private DateTime _selectedDate;
        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set {
                _selectedDate = value;

                SetSelectedTime();

                RaisePropertyChanged();
                UpdateSchedule();
            }
        }


        private DateTime _selectedTime;
        public DateTime SelectedTime
        {
            get { return _selectedTime; }
            set { _selectedTime = value; RaisePropertyChanged(); UpdateSchedule(); }
        }

        public DateTime MinTime { get; set; }
        public DateTime MaxTime { get; set; }
        public EventRecurrence Recurrence { get; set; }
        public EventSchedule Schedule { get; set; }

        public SelectItemInListCommandViewModel<string, EventRecurrence> SelectRepeatIntervalCommand { get; private set; }

        #endregion

        #region Construction / Destruction

        public SelectActivityTimeViewModel(
            ISandboxActivityCreationService sandboxedActivityCreationService
        ) : base("activity.create.select.time.title")
        {
            _sandboxedActivityCreationService = sandboxedActivityCreationService;
            Schedule = (EventSchedule) _sandboxedActivityCreationService.SandboxedActivity.EventSchedule;
            Recurrence = Schedule.Recurrence;

            var tmpDate = Schedule.LocalStartTime;
            if (tmpDate == default(DateTime)) { tmpDate = RoundUp(DateTime.Now, TimeSpan.FromMinutes(60)); }

            _selectedDate = tmpDate.Date;
            _selectedTime = default(DateTime).Add(tmpDate.TimeOfDay);

            SelectRepeatIntervalCommand = CreateSelectFromEnumCommand(Recurrence, localPrefixKey + "repeat.interval.value.{0}", SelectRepeatInterval);

            SetSelectedTime();

            ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(activity => activity.TypeName == _sandboxedActivityCreationService.SandboxedActivity.Sport);

        }

        DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            return new DateTime(((dt.Ticks + d.Ticks - 1) / d.Ticks) * d.Ticks);
        }

        #endregion

        #region Methods

        private void UpdateSchedule() 
        {
            Schedule.LocalStartTime =  _selectedDate.Add(_selectedTime.TimeOfDay);
        }

        private void SelectRepeatInterval(EventRecurrence interval)
        {
            Schedule.Recurrence = interval;
        }

        private void SetSelectedTime()
        {
            SetMinTime();
            SetMaxTime();

            DateTime newSelectedTime = new DateTime(SelectedDate.Year, SelectedDate.Month, SelectedDate.Day, 
                                           SelectedTime.Hour, SelectedTime.Minute, SelectedTime.Second);

            if (newSelectedTime < MinTime)
            {
                newSelectedTime = MinTime;
            }
            SelectedTime = newSelectedTime;
        }

        private void SetMinTime()
        {
            MinTime = SelectedDate.IsToday()
                ? new DateTime(SelectedDate.Year, SelectedDate.Month, SelectedDate.Day, DateTime.Now.AddHours(1).Hour, 0, 0)
                : new DateTime(SelectedDate.Year, SelectedDate.Month, SelectedDate.Day, 0, 0, 0);
        }

        private void SetMaxTime()
        {
            MaxTime = new DateTime(SelectedDate.Year, SelectedDate.Month, SelectedDate.Day, 23, 45, 0);
        }

        protected override void OnClosing ()
        {
            base.OnClosing();
            Save();
        }

        protected void Save ()
        {
            _sandboxedActivityCreationService.Update(Schedule);
        }

        #endregion
    }
}

