﻿using System;
using YuFit.Core.Models;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.ViewModels
{
    public class UserInfoViewModel : BaseViewModel
    {
        public string Age       { get; set; }
        public string AvatarUrl { get; set; }
        public string Id        { get; set; }
        public string Name      { get; set; }
        public string Location  { get; set; }
        public string Sport     { get; set; }

        public UserInfoViewModel (User user)
        {
            Id                  = user.Id;
            Name                = user.Name;
            Sport               = "Cycling";

            var homeLocation    = user.GetHomeLocation();
            Location            = homeLocation?.CityStateString() ?? Localized("user.info.location.not.provided.text");
            AvatarUrl           = user.UserProfile?.AvatarUri?.ToString();

            CalculateAge(user.UserProfile?.Birthday, DateTime.Now);

            Mvx.Trace("Creating userinfovm for {0}", Name);
        }

        #if DEBUG_LEAKS
        ~UserInfoViewModel () { Mvx.Trace("Deleting userinfovm for {0}", Name); }
        #endif

        void CalculateAge(DateTime? birthDate, DateTime now)
        {
            if (birthDate != null && birthDate.HasValue) {
                var bday = birthDate.Value;
                int age = now.Year - bday.Year;
                if (now.Month < bday.Month || (now.Month == bday.Month && now.Day < bday.Day)) age--;
                Age = age.ToString();
            }
        }
    }
}

