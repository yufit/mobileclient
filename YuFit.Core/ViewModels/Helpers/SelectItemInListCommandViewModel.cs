using System.Collections.Generic;
using System.Windows.Input;

namespace YuFit.Core.ViewModels.Helpers
{
    public class SelectItemInListCommandViewModel<T, TEnum> : CommandViewModel {

        public List<T> SelectableItems { get; private set; }
        
        public TEnum DefaultSelection { get; private set; }

        public SelectItemInListCommandViewModel(List<T> items, TEnum defaultSelection, ICommand command) : base(command)
        {
            SelectableItems = items;
            DefaultSelection = defaultSelection;
        }
           
    }

}
