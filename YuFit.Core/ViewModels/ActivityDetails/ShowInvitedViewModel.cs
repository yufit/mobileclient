﻿using System;
using Cirrious.MvvmCross.ViewModels;

namespace YuFit.Core.ViewModels.Dashboard.ActivitySummary
{
    public class ShowInvitedViewModel : BaseViewModel
    {
        public CommandViewModel ShowUserProfileCommand { get; private set; }

        public ShowInvitedViewModel ()
        {
            DisplayName = Localized("invited");
            ShowUserProfileCommand = new CommandViewModel(new MvxCommand(ShowUserProfile));
        }

        void ShowUserProfile ()
        {
            ShowViewModel<Profiles.DisplayFriendProfileViewModel>();
        }
    }
}

