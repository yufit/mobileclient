﻿using YuFit.Core.Models.Activities;
using System.Collections.Generic;
using System;

namespace YuFit.Core.ViewModels.Dashboard.ActivitySummary
{
    public class YogaActivityDetailsViewModel : BaseActivityDetailsViewModel<YogaActivitySettings>
    {
        private const string localPrefixKey = "crudDisplayInfoYoga - ";

        private int _duration;
        public int Duration { 
            get { return _duration; } 
            set { _duration = value; RaisePropertyChanged(() => Duration); }
        }

        public List<Tuple<string, object>> DurationValues { get { return YuFit.Core.Models.Activities.DurationValues.Yoga; } }

        public YogaActivityDetailsViewModel (YogaActivitySettings settings) : base("", settings)
        {
            UpdateValues();
        }

        public override void Update ()
        {
            UpdateValues();
            base.Update();
        }

        void UpdateValues()
        {
            Duration = ActivitySettings.Duration;
        }
    }
}

