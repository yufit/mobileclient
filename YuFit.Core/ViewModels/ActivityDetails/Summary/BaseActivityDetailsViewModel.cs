﻿using YuFit.Core.Models.Activities;

namespace YuFit.Core.ViewModels.Dashboard.ActivitySummary
{

    public class BaseActivityDetailsViewModelRaw : BaseFitActivityViewModel {
        public BaseActivityDetailsViewModelRaw(string displayName) : base(displayName) {}
        public virtual void Update() {}
    }

    public class BaseActivityDetailsViewModel<T> : BaseActivityDetailsViewModelRaw where T : ActivitySettings, new()
    {
        public T ActivitySettings { get; set; }
        public BaseActivityDetailsViewModel (string displayName, T settings) : base(displayName)
        {
            ActivitySettings = settings;
        }
    }
}

