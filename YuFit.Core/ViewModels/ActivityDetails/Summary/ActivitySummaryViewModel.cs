﻿using System;
using System.Collections.Generic;
using System.Linq;

using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;

using Coc.MvvmCross.Plugins.Location;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.ViewModels.Dashboard.ActivitySummary;
using YuFit.Core.ViewModels.DisplayActivities;

namespace YuFit.Core.ViewModels.Dashboard.ActivitySummary
{
    public class ActivitySummaryViewModel : BaseViewModel
    {
        #region Data Models

        readonly IFitActivityService _fitActivityService;

        #endregion

        public string Id { get; set; }
        public CocLocation Location { get; set; }

        #region Properties

        #region Notifiable properties

        private ActivityType _activityType;
        public ActivityType ActivityType
        {
            get { return _activityType; }
            set { _activityType = value; RaisePropertyChanged(); }
        }

        private List<FitActivityParticipant> _participants;
        public List<FitActivityParticipant> Participants
        {
            get { return _participants; }
            set { _participants = value; RaisePropertyChanged(); }
        }

        private BaseActivityDetailsViewModelRaw _activityViewModel;
        public BaseActivityDetailsViewModelRaw ActivityViewModel
        {
            get { return _activityViewModel; }
            set { _activityViewModel = value; RaisePropertyChanged(); }
        }

        private DateTime _startTime;
        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; RaisePropertyChanged(); }
        }

        private bool _iamin;
        public bool IamIn
        {
            get { return _iamin; }
            set { _iamin = value; RaisePropertyChanged(); RaisePropertyChanged(() => IamInString); }
        }

        string _subSport;
        public string SportSubType
        {
            get { return _subSport; }
            set { _subSport = value; RaisePropertyChanged(); }
        }

        string _subSportRaw;
        public string SportSubTypeRaw
        {
            get { return _subSportRaw; }
            set { _subSportRaw = value; RaisePropertyChanged(); }
        }

        #endregion

        #region Localized String properties

        public string JoinTitle { get; private set; }
        public string IAmInTitle { get; private set; }
        public string InvitedTitle { get; private set; }
        public string IamInString { get { return _iamin ? IAmInTitle : JoinTitle; } }

        #endregion

        #region Commands VM

        public CommandViewModel DismissCommand { get; private set; }
        public CommandViewModel JoinCommand { get; private set; }
        public CommandViewModel ShowInvitedParticipantsCommand { get; private set; }
        public CommandViewModel ShowActivityDetailCommand { get; private set; }

        #endregion

        #endregion

        #region Construction/Initialization
        public ActivitySummaryViewModel (IFitActivityService fitActivityService)
        {
            _fitActivityService = fitActivityService;

            JoinTitle = StringLoaderService.GetString("join label title");
            IAmInTitle = StringLoaderService.GetString("i am in label title");
            InvitedTitle = StringLoaderService.GetString("invited label title");

            DismissCommand = new CommandViewModel(new MvxCommand(Dismiss));
            JoinCommand = new CommandViewModel(new MvxCommand(Join));
            ShowInvitedParticipantsCommand = new CommandViewModel(new MvxCommand(ShowInvitedParticipants));
            ShowActivityDetailCommand = new CommandViewModel(new MvxCommand(ShowActivityDetail));
        }

        public class Nav { public string Id { get; set; } }
        public async void Init(Nav navigationData)
        {
            Id = navigationData.Id;

            await ExecuteWebServiceRequestAsync(async () => {
                // Get the info from the server api (which may cache thing)
                FitActivity activity = await _fitActivityService.GetActivity(Id);
                Location = new CocLocation { Coordinates = new CocCoordinates {Latitude = ((Location) activity.Location).GeoCoordinates.Latitude, Longitude = ((Location) activity.Location).GeoCoordinates.Longitude } };
                ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(a => a.TypeName == activity.Sport);
                Participants = activity.Participants.Cast<FitActivityParticipant>().ToList();
                StartTime = activity.LocalStartTime;

                if (!string.IsNullOrEmpty(activity.SubSport)) {
                    _subSport = StringLoaderService.GetString(activity.SubSport.ToLower());
                }

                SportSubTypeRaw = activity.SubSport;

                UpdateActivityViewModel(activity);

//                List<FitActivity> activities = await _fitActivityService.GetMyActivities(false, true);
//                IamIn = activities.Any(a => a.Id.ObjectId == Id);

                Mvx.Trace(activity.ToString());
            });
        }
        #endregion


        #region Methods

        #region Command handlers

        void Dismiss ()
        {
            Close(this);
        }

        async void Join ()
        {
            Mvx.Trace("JOIN");
            await ExecuteWebServiceRequestAsync(async () => {
                await _fitActivityService.ToggleGoing(Id);
                IamIn = !IamIn;

                FitActivity activity = await _fitActivityService.GetActivity(Id, false);
                Participants = activity.Participants.Cast<FitActivityParticipant>().ToList();
            });
        }

        void ShowInvitedParticipants ()
        {
            Mvx.Trace("SHOW");
            ShowViewModel<ShowInvitedViewModel>();
        }

        void ShowActivityDetail ()
        {
            ShowViewModel<ViewFitActivityDetailsViewModel>(new ViewFitActivityDetailsViewModel.Navigation { ActivityId = Id });
        }

        #endregion

        void UpdateActivityViewModel (FitActivity activity)
        {
            const string ActivityTypeVMFormat = "YuFit.Core.ViewModels.Dashboard.ActivitySummary.{0}ActivityDetailsViewModel";//{0}ActivityDetailsViewModel";
            string className = string.Format(ActivityTypeVMFormat, ActivityType.TypeName);
            Type activityViewModelClassType = Type.GetType(className);

            if (activityViewModelClassType != null)
            {
                ActivityViewModel = (BaseActivityDetailsViewModelRaw) Activator.CreateInstance(activityViewModelClassType, activity.ActivitySettings);
            }
        }

        #endregion

    }
}

