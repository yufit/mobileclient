﻿using System;
using System.Collections.Generic;

using YuFit.Core.Models;

namespace YuFit.Core.ViewModels
{
    public class ActivitySummarySubViewModel : NamedViewModel
    {
        public ActivityType     ActivityType    { get; set; }
        public DateTime         When            { get; set; }
        public string           CreatedBy       { get; set; }
        public string           CreatedByLabel  { get; set; }
        public int?             Duration        { get; set; }
        public string           StreetAddress   { get; set; }
        public List<string>     Details         { get; set; }
        public bool             IsCanceled      { get; set; }
        public string           CancelString    { get; set; }
        public bool             IsPromoted      { get; set; }

        public ActivitySummarySubViewModel ()
        {
            Details = new List<string>();
            CancelString = Localized("activity.canceled.label");
        }
        
    }
}

