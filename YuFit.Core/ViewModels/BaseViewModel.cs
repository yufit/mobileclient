﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Xamarin;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Helpers;
using YuFit.Core.ViewModels.Login;
using YuFit.WebServices.Exceptions;
using MvvmCross.Platform;
using MvvmCross.Plugins.Messenger;
using MvvmCross.Core.ViewModels;
using Acr.UserDialogs;

namespace YuFit.Core.ViewModels
{
    public class BaseViewModel : NamedViewModel
	{
		#region Data members

        private readonly CancelableRequest _pendingRequest = new CancelableRequest();

		#endregion

		#region Properties

        public      string                      ProgressMessage { get; set; }
        public      bool                        IsBusy { get; set; }

        protected   IUserDialogs                UserDialogs { get; private set; }
		protected   IMvxMessenger               Messenger { get; private set; }
        protected   INetworkMonitoringService   NetworkMonitoringService { get; private set; }

		#endregion

		#region Construction

		public BaseViewModel()                                       { DoCommonConstruction(); }
        public BaseViewModel(string displayName) : base(displayName) { DoCommonConstruction(); }

        private void DoCommonConstruction()
        {
            UserDialogs = Mvx.Resolve<IUserDialogs> ();
            Messenger = Mvx.Resolve<IMvxMessenger>();
            NetworkMonitoringService = Mvx.Resolve<INetworkMonitoringService>();
            ProgressMessage = string.Empty;
        }

		#endregion

		#region Available API

        /// <summary>
        /// Executes the cancelable web request async.
        /// </summary>
        /// <returns>The cancelable web request async.</returns>
        /// <param name="operation">Operation.</param>
        /// --------------------------------------------------------------------------------------------
        protected async Task ExecuteCancelableWebRequestAsync(Func<CancellationToken, int, Task> operation) 
        {
            Mvx.Resolve<INetworkMonitoringService>().NetworkIsGoingActive();
            IsBusy = true;

            try {
                await _pendingRequest.ExecuteRequest(async (token, tryCount) => {
                    if (tryCount > 1) { ProgressMessage = Localized("base.timed.out.retrying.progress.message"); }
                    await operation(token, tryCount);
                });
            }

            catch (Exception ex) when (ex is NoActiveSessionException || ex is UnauthorizedException) {
                HandleWebServiceUnauthorizedException(ex);
            }

            catch (ConnectionTimeoutException ex) {
                HandleWebServiceConnectionTimeoutException(ex);
            }

            catch (ServerNotReachableException ex) {
                HandleServerNotReachableException(ex);
            }

            finally {
                Mvx.Resolve<INetworkMonitoringService>().NetworkIsGoingIdle();
                IsBusy = false;
            }
        }

        /// <summary>
        /// Determines whether this instance cancel request in progress.
        /// </summary>
        /// <returns><c>true</c> if this instance cancel request in progress; otherwise, <c>false</c>.</returns>
        /// --------------------------------------------------------------------------------------------
        protected async void CancelRequestInProgress()
        {
            // Analysis disable EmptyGeneralCatchClause
            try { await _pendingRequest.CancelRequest(); }
            catch(Exception) { }
            // Analysis restore EmptyGeneralCatchClause
        }

        /// <summary>
        /// Alert the specified message, doneAction, title and okButton.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <param name="doneAction">Done action.</param>
        /// <param name="title">Title.</param>
        /// <param name="okButton">Ok button.</param>
        /// --------------------------------------------------------------------------------------------
        public virtual void Alert(string message, Action doneAction = null, string title = "", string okButton = "base.ok.button")
        {
            UserDialogs.Alert(Localized(message), Localized(title), Localized(okButton));
        }

        #endregion

        #region Default webservices exception handler method

        /// <summary>
        /// Handles the web service connection timeout exception.
        /// </summary>
        /// <returns>The web service connection timeout exception.</returns>
        /// <param name="ex">Ex.</param>
        protected virtual void HandleWebServiceConnectionTimeoutException (ConnectionTimeoutException ex)
        {
            Alert ("base.error.connection.timed.out.message", null, "base.error.connection.timed.out.title");
        }

        /// <summary>
        /// Handles the web service unauthorized exception.
        /// </summary>
        /// <returns>The web service unauthorized exception.</returns>
        /// <param name="ex">Ex.</param>
        protected virtual void HandleWebServiceUnauthorizedException (Exception ex)
        {
            Mvx.Resolve<IUserService>().Signout();
            ShowViewModel<LoginViewModel> ();
        }

        /// <summary>
        /// Handles the server not reachable exception.
        /// </summary>
        /// <returns>The server not reachable exception.</returns>
        /// <param name="ex">Ex.</param>
        protected virtual void HandleServerNotReachableException (ServerNotReachableException ex)
        {
            Alert ("base.error.server.not.reachable.message", null, "base.error.server.not.reachable.title");
        }

        protected virtual void HandleGeneralException(Exception ex)
        {
            #if DEBUG
            Alert ("base.error.generic.message", null, "base.error.generic.title");
            #endif
        }

		#endregion

        /// <summary>
        /// Creates the select from enum command.  This  return a command view
        /// model specialized to pick a value from an enum.
        /// </summary>
        /// <returns>The select from enum command.</returns>
        /// <param name="enumValue">Enum value.</param>
        /// <param name="localizationFormat">Localization format.</param>
        /// <param name="command">Command.</param>
        /// <typeparam name="TEnum">The 1st type parameter.</typeparam>
        public SelectItemInListCommandViewModel<string, TEnum> CreateSelectFromEnumCommand<TEnum>(TEnum enumValue, string localizationFormat, Action<TEnum> command) {

            List<string> localized = new List<string>();
            Enum.GetNames(typeof(TEnum)).ForEach(name =>
                localized.Add(Localized(string.Format(localizationFormat, name.ToLower())))
            );

            return new SelectItemInListCommandViewModel<string, TEnum>(localized, enumValue, new MvxCommand<TEnum>(command));
        } 
	}
}

