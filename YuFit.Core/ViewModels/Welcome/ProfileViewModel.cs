﻿namespace YuFit.Core.ViewModels.Welcome
{
    public class ProfileViewModel : BasePageViewModel
    {
        public string RealName { get; set; }
        public string HomeAddress { get; set; }
        public string Bio { get; set; }

        public ProfileViewModel () : base("welcome.profile.title")
        {
            SubTitle1 = Localized("welcome.profile.body.1.text");
            SubTitle2 = Localized("welcome.profile.body.2.text");
            SubTitle3 = Localized("welcome.profile.body.3.text");

            RealName = Localized("welcome.profile.sample.name.text");
            HomeAddress = Localized("welcome.profile.sample.address.text");
            Bio = Localized("welcome.profile.sample.bio.text");
        }
    }
}

