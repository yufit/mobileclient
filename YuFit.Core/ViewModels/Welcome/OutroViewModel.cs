﻿using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.ViewModels.Login;

namespace YuFit.Core.ViewModels.Welcome
{
    public class OutroViewModel : BasePageViewModel
    {
        public CommandViewModel ContinueCommand { get; private set; }
        public string Label1 { get; set; }
        public string Label2 { get; set; }
        public string Label3 { get; set; }

        public OutroViewModel () : base("welcome.outro.title")
        {
            ContinueCommand = new CommandViewModel(new MvxCommand(ContinueOn));
            Label1 = Localized("welcome.outro.body.1.text");
            Label2 = Localized("welcome.outro.body.2.text");
            Label3 = Localized("welcome.outro.body.3.text");
        }

        public void ContinueOn()
        {
            ShowViewModel<LoginViewModel>();
        }
    }
}

