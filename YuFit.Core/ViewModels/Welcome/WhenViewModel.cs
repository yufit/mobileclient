﻿namespace YuFit.Core.ViewModels.Welcome
{
    public class WhenViewModel : BasePageViewModel
    {
        public WhenViewModel () : base("welcome.when.title")
        {
            SubTitle1 = Localized("welcome.when.body.1.text");
            SubTitle2 = Localized("welcome.when.body.2.text");
            SubTitle3 = Localized("welcome.when.body.3.text");
        }
    }
}

