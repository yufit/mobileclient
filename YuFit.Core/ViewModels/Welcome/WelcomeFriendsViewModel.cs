﻿using System.Collections.Generic;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using YuFit.Core.Utils;

namespace YuFit.Core.ViewModels.Welcome
{
    public class ParticipantViewModel : MvxViewModel
    {
        public string AvatarUrl  { get; set; }
        public string Name       { get; set; }
        public string Location   { get; set; }
        public bool   IsSelected { get; set; }

        ~ParticipantViewModel ()
        {
            Mvx.Trace("Cleaning VM for {0}", Name);
        }
    }

    public class WelcomeFriendsViewModel : BasePageViewModel
    {
        public SilentObservableCollection<ParticipantViewModel> YuFitFriends { get; private set; }

        public WelcomeFriendsViewModel () : base("welcome.friends.title")
        {
            SubTitle1 = Localized("welcome.friends.body.1.text");
            SubTitle2 = Localized("welcome.friends.body.2.text");
            SubTitle3 = Localized("welcome.friends.body.3.text");
            YuFitFriends = new SilentObservableCollection<ParticipantViewModel>();

            // Create dummy friend.
            var tmp = new List<ParticipantViewModel>();
            tmp.Add(new ParticipantViewModel { Name = Localized("welcome.friends.sample.name.1.text"), Location = Localized("welcome.friends.sample.location.1.text"), IsSelected = true,  AvatarUrl = "AvatarClubRunner.jpg"});
            tmp.Add(new ParticipantViewModel { Name = Localized("welcome.friends.sample.name.2.text"), Location = Localized("welcome.friends.sample.location.2.text"), IsSelected = true,  AvatarUrl = "AvatarClubYoga.jpg"});
            tmp.Add(new ParticipantViewModel { Name = Localized("welcome.friends.sample.name.3.text"), Location = Localized("welcome.friends.sample.location.3.text"), IsSelected = false, AvatarUrl = "AvatarJUJU1.png"});
            tmp.Add(new ParticipantViewModel { Name = Localized("welcome.friends.sample.name.4.text"), Location = Localized("welcome.friends.sample.location.4.text"), IsSelected = false, AvatarUrl = "AvatarJUJU2.png"});
            tmp.Add(new ParticipantViewModel { Name = Localized("welcome.friends.sample.name.5.text"), Location = Localized("welcome.friends.sample.location.5.text"), IsSelected = true,  AvatarUrl = "AvatarJP1.png"});
            tmp.Add(new ParticipantViewModel { Name = Localized("welcome.friends.sample.name.6.text"), Location = Localized("welcome.friends.sample.location.6.text"), IsSelected = true,  AvatarUrl = "AvatarAngel.png"});
            tmp.Add(new ParticipantViewModel { Name = Localized("welcome.friends.sample.name.7.text"), Location = Localized("welcome.friends.sample.location.7.text"), IsSelected = false, AvatarUrl = "AvatarVincent.jpg"});
            tmp.Add(new ParticipantViewModel { Name = Localized("welcome.friends.sample.name.8.text"), Location = Localized("welcome.friends.sample.location.8.text"), IsSelected = true,  AvatarUrl = "AvatarPAT2.png"});
            YuFitFriends.AddMultiple(tmp);
        }

    }
}

