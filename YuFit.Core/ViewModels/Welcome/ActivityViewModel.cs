﻿using System;
using System.Collections.Generic;

namespace YuFit.Core.ViewModels.Welcome
{
    public class ActivityViewModel : BasePageViewModel
    {
        public string ActivityType { get; set; }
        public string ParticipantCount { get; set; }
        public DateTime StartTime { get; set; }

        public string JoinTitle { get; private set; }
        public string GoingTitle { get; private set; }
        public string Speed { get; private set; }
        public string SpeedUnit { get; private set; }

        public int Distance { get; private set; }
        public int Duration { get; private set; }

        public List<Tuple<string, object>> DurationValues { get { return YuFit.Core.Models.Activities.DurationValues.Running; } }
        public List<Tuple<string, object>> DistanceValues { get { return YuFit.Core.Models.Activities.DistanceValues.Running; } }

        public ActivityViewModel () : base("welcome.activity.title")
        {
            SubTitle1 = Localized("welcome.activity.body.1.text");
            SubTitle2 = Localized("welcome.activity.body.2.text");
            SubTitle3 = Localized("welcome.activity.body.3.text");

            GoingTitle = Localized("welcome.activity.going.label");
            ActivityType = Localized("welcome.activity.sport");
            ParticipantCount = Localized("welcome.activity.participant.count.label");
            JoinTitle = Localized("welcome.activity.join.label");
            StartTime = new DateTime(2020, 05, 17, 21, 15, 0);

            SpeedUnit = "KM/H";

            Distance = 30;
            Duration = 120;
            Speed = (((float)Distance)/((float)Duration) * 60.0f).ToString();
        }
    }
}

