﻿using System.Collections.ObjectModel;
using MvvmCross.Core.ViewModels;

namespace YuFit.Core.ViewModels.Welcome
{
    public class WelcomeViewModel : BaseViewModel
    {
        public int CurrentPage                                  { get; set; }
        public CommandViewModel NextPageCommand                 { get; private set; }
        public ObservableCollection<BasePageViewModel> Pages    { get; private set; }

        public WelcomeViewModel () : base ("welcome.title")
        {
            Pages = new ObservableCollection<BasePageViewModel>();
            Pages.Add(new IntroViewModel());
            Pages.Add(new ProfileViewModel());
            Pages.Add(new ActivityViewModel());
            Pages.Add(new LocationViewModel());
            Pages.Add(new WelcomeFriendsViewModel());
            Pages.Add(new WhenViewModel());
            Pages.Add(new OutroViewModel());

            CurrentPage = 0;
            NextPageCommand = new CommandViewModel(new MvxCommand(NextPage));
        }

        void NextPage ()
        {
            if (CurrentPage < Pages.Count) {CurrentPage++;}
        }
    }
}

