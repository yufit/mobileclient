﻿namespace YuFit.Core.ViewModels.Welcome
{
    public class LocationViewModel : BasePageViewModel
    {
        public LocationViewModel () : base("welcome.location.title")
        {
            SubTitle1 = Localized("welcome.location.body.1.text");
            SubTitle2 = Localized("welcome.location.body.2.text");
            SubTitle3 = Localized("welcome.location.body.3.text");
        }
    }
}

