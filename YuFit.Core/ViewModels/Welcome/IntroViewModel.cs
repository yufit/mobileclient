﻿namespace YuFit.Core.ViewModels.Welcome
{
    public class IntroViewModel : BasePageViewModel
    {
        public string Messages { get; set; }
        public IntroViewModel () : base("welcome.intro.title")
        {
            Messages = Localized("welcome.intro.body.1.text");
        }
    }
}

