﻿namespace YuFit.Core.ViewModels.Welcome
{
    public class BasePageViewModel : BaseViewModel
    {
        public string SubTitle1 { get; set; }
        public string SubTitle2 { get; set; }
        public string SubTitle3 { get; set; }

        public BasePageViewModel () { }
        public BasePageViewModel (string displayName) : base(displayName) { }
    }
}

