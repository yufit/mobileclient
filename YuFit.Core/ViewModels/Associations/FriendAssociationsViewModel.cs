﻿using System.Collections.ObjectModel;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.ViewModels.Associations.Friendships;
using YuFit.Core.ViewModels.Associations.Groups;

namespace YuFit.Core.ViewModels.Associations
{
    public class FriendAssociationsViewModel : BaseViewModel
    {
        readonly FriendsOfFriendViewModel _friendsOf;
        readonly GroupsOfFriendViewModel _groupsOf;

        public ObservableCollection<AssociationViewModel> Pages { get; private set; }

        private int _selectedPage;
        public int SelectedPage { 
            get { return _selectedPage; }
            set 
            {
                CanAdd = Pages[value].CanAdd;
                CanSearch = Pages[value].CanSearch; 
                _selectedPage = value;
            }
        }


        public bool CanAdd { get; set; }
        public bool CanSearch { get; set; }

        public class Navigation { public string UserId { get; set; } public string Name {get; set; } }
        public virtual void Init(Navigation navParams)
        {
            _friendsOf.Init(new FriendsOfFriendViewModel.Navigation {UserId = navParams.UserId});
            _groupsOf.Init(new GroupsOfFriendViewModel.Navigation {UserId = navParams.UserId});
            DisplayName = navParams.Name;
        }


        public FriendAssociationsViewModel ()
        {
            DisplayName = Localized("associations.title");

            Pages = new ObservableCollection<AssociationViewModel>();

            _friendsOf = Mvx.IocConstruct<FriendsOfFriendViewModel>();
            _groupsOf  = Mvx.IocConstruct<GroupsOfFriendViewModel>();

            Pages.Add(_friendsOf);
            Pages.Add(_groupsOf);

            _selectedPage = 0;
            CanAdd = Pages[_selectedPage].CanAdd;
            CanSearch = Pages[_selectedPage].CanSearch; 
        }
    }
}

