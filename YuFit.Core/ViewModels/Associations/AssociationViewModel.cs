﻿namespace YuFit.Core.ViewModels.Associations
{
    public abstract class AssociationViewModel : BaseViewModel
    {
        public abstract void Add ();
        public abstract bool CanAdd { get; }

        public abstract void Search ();
        public abstract bool CanSearch { get; }

        public AssociationViewModel (string displayName) : base(displayName)
        {
        }

    }
}

