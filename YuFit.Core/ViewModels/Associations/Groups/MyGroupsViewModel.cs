﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Utils;

using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.ViewModels.Associations.Groups
{
    public class MyGroupsViewModel : BaseGroupsViewModel
    {
        #pragma warning disable 414
        readonly MvxSubscriptionToken _groupCreatedMessageToken;
        readonly MvxSubscriptionToken _groupUpdatedMessageToken;
        readonly MvxSubscriptionToken _groupDeletedMessageToken;
        readonly MvxSubscriptionToken _joinedGroupMessageToken;
        readonly MvxSubscriptionToken _leftGroupMessageToken;
        #pragma warning restore 414

        public override bool CanAdd { get { return true; } }
        public override bool CanSearch { get { return true; } }

        public MyGroupsViewModel (
            IUserService userService, 
            IMvxMessenger messengerService
        ) : base(userService, "groups.title")
        {
            _groupCreatedMessageToken = Messenger.Subscribe<GroupCreatedMessage>(OnGroupCreated);
            _groupUpdatedMessageToken = Messenger.Subscribe<GroupUpdatedMessage>(OnGroupUpdated);
            _groupDeletedMessageToken = Messenger.Subscribe<GroupDeletedMessage>(OnGroupDeleted);
            _joinedGroupMessageToken  = Messenger.Subscribe<JoinedGroupMessage> (OnJoinedGroup);
            _leftGroupMessageToken    = Messenger.Subscribe<LeftGroupMessage>   (OnLeftGroup);
        }

        void OnGroupCreated (GroupCreatedMessage obj)
        {
            Groups.Insert(0, new GroupInfoViewModel(obj.Group));
        }

        void OnGroupUpdated (GroupUpdatedMessage obj)
        {
            var GroupVM = Groups.FirstOrDefault(group => group.Id == obj.Group.Id);
            if (GroupVM != null) { GroupVM.Update(obj.Group); }
        }

        void OnGroupDeleted (GroupDeletedMessage obj)
        {
            var GroupVM = Groups.FirstOrDefault(group => group.Id == obj.GroupId);
            if (GroupVM != null) { Groups.Remove(GroupVM); }
        }

        void OnJoinedGroup (JoinedGroupMessage obj)
        {
            Groups.Insert(0, new GroupInfoViewModel(obj.Group));
        }

        void OnLeftGroup (LeftGroupMessage obj)
        {
            var GroupVM = Groups.FirstOrDefault(group => group.Id == obj.Group.Id);
            if (GroupVM != null) { Groups.Remove(GroupVM); }
        }

        protected override async Task<IList<IUserGroup>> FetchGroups()
        {
            return await _userService.GetGroups();
        }

        public override void Add ()
        {
            CreateGroupAndEdit();
        }

        public override void Search()
        {
            SearchForGroup();
        }

        private void CreateGroupAndEdit()
        {
            ShowViewModel<EditGroupViewModel>();
        }

        private void SearchForGroup()
        {
            ShowViewModel<SearchForGroupsViewModel>();
        }

        protected override void OnClosing ()
        {
            Messenger.Unsubscribe<GroupDeletedMessage>(_groupDeletedMessageToken);
            Messenger.Unsubscribe<GroupCreatedMessage>(_groupCreatedMessageToken);
            Messenger.Unsubscribe<GroupUpdatedMessage>(_groupUpdatedMessageToken);
            Messenger.Unsubscribe<JoinedGroupMessage> (_joinedGroupMessageToken);
            Messenger.Unsubscribe<LeftGroupMessage>   (_leftGroupMessageToken);
            base.OnClosing();
        }
    }
}

