﻿using System.Collections.Generic;
using System.Linq;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Utils;
using System;

namespace YuFit.Core.ViewModels.Associations.Groups
{
    public class SearchForGroupsViewModel : BaseViewModel
    {
        private readonly IGroupService _groupService;

        public string SearchTerm { get; set; }

        public CommandViewModel SearchCommand { get; private set; }
        public CommandViewModel LoadMoreCommand { get; private set; }
        public CommandViewModel ShowGroupCommand { get; private set; }

        public SilentObservableCollection<GroupInfoViewModel> Groups { get; private set; } 

        public SearchForGroupsViewModel (
            IGroupService groupService
        ) : base("groups.search.title")
        {
            _groupService = groupService;

            SearchTerm = string.Empty;
            SearchCommand = new CommandViewModel(new MvxCommand(Search));
            LoadMoreCommand = new CommandViewModel(new MvxCommand(LoadMore));
            ShowGroupCommand = new CommandViewModel(new MvxCommand<GroupInfoViewModel>(ShowGroup));

            Groups = new SilentObservableCollection<GroupInfoViewModel>();
        }

        async void Search ()
        {
            ProgressMessage = Localized("groups.search.progress.message");

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    Mvx.Trace("Search term {0}", SearchTerm);
                    var groups = await _groupService.SearchGroup(SearchTerm, 0, 10, token);
                    if (groups != null) 
                    {
                        var tmp = new List<GroupInfoViewModel>();
                        groups.ForEach(group => tmp.Add(new GroupInfoViewModel(group)));

                        var orderedList = tmp.OrderBy(x => x.DisplayName).ToList();
                        Groups.Clear();
                        Groups.AddMultiple(orderedList);
                    }
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void LoadMore ()
        {
        }

        void ShowGroup (GroupInfoViewModel group)
        {
            ShowViewModel<GroupViewModel>(new GroupViewModel.Navigation {GroupId = group.Id, Name = group.DisplayName});
        }
    }
}

