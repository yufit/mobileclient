﻿using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Activity;
using YuFit.WebServices.Interface.Model;
using System;
using YuFit.Core.Models;

namespace YuFit.Core.ViewModels.Associations.Groups
{
    /// <summary>
    /// An Abbreviated representation of a group used for
    /// lists of groups or group summaries
    /// </summary>
    public class GroupInfoViewModel : BaseViewModel
    {
        public Uri          AvatarUrl               { get; set; }
        public string       Id                      { get; set; }
        public string       ManagedByLabel          { get; set; }
        public string       ManagedBy               { get; set; }
        public string       Description             { get; set; }
        public string       MembersLabel            { get; set; }
        public int          NumberOfMembers         { get; set; }
        public string       Location                { get; set; }

        public string       ActivityCreatedCount    { get; set; }
        public string       UpcomingActivitiesTitle { get; set; }

        public SilentObservableCollection<UpcomingActivityViewModel> UpcomingActivities { get; private set; } 

        #region Commands

        public CommandViewModel ViewMembersCommand { get; private set; }
        public CommandViewModel ShowActivitySummaryCommand { get; private set; }

        #endregion

        public GroupInfoViewModel (IUserGroup group) { Update(group); }

        public void Update (IUserGroup group)
        {
            Id          = group.Id;
            DisplayName = group.Name;
            Description = group.Description;
            AvatarUrl   = group.GroupAvatar;

            var homeLocation = group.Location;
            if (homeLocation != null) {
                Location = ((Location)homeLocation).CityStateString();
            }
            if (string.IsNullOrEmpty(Location)) {
                Location = Localized("user.info.location.not.provided.text");
            }
        }
    }
}

