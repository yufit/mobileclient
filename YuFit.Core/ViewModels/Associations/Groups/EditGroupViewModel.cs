﻿using System;
using System.IO;
using System.Threading.Tasks;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;

using YuFit.WebServices.Interface.Model;
using YuFit.Core.ViewModels.Helpers;
using YuFit.Core.Extensions;

namespace YuFit.Core.ViewModels.Associations.Groups
{
    public class EditGroupViewModel : BaseViewModel
    {
        readonly IGroupService                  _groupService;
        readonly IUserService                   _userService;
        readonly IGeoLocationConverterService   _locationConverterService;

        string      _groupId;
        bool        _wasDeleted;
        ILocation   _homelocation;

        public CommandViewModel AddPictureCommand       { get; private set; }
        public CommandViewModel AddAvatarCommand        { get; private set; }
        public CommandViewModel ToggleVisibilityCommand { get; private set; }
        public CommandViewModel AddMembersCommand       { get; private set; }
        public CommandViewModel DeleteCommand           { get; private set; }
        public CommandViewModel CreateCommand           { get; private set; }
        public CommandViewModel CancelCommand           { get; private set; }
        public CommandViewModel SearchLocationCommand   { get; private set; }

        public SelectItemInListCommandViewModel<string, GroupType> SelectGroupTypeCommand { get; private set; }
        public SelectItemInListCommandViewModel<string, SportKind> SelectSportKindCommand { get; private set; }

        public bool         IsChanged                   { get; set; }
        public bool         IsEditing                   { get; set; }
        public bool         IsCreator                   { get; set; }

        public string       GroupName                   { get; set; }
        public string       Description                 { get; set; }
        public string       Hometown                    { get; set; }
        public GroupType    Type                        { get; set; }
        public SportKind    SportKind                   { get; set; }
        public string       ManagedBy                   { get; set; }
        public Uri          PhotoUrl                    { get; set; }
        public Uri          AvatarUrl                   { get; set; }
        public bool         Private                     { get; set; }

        public string       AddMembersLabel             { get; set; }
        public string       GroupNamePlaceholderText    { get; set; }
        public string       LocationPlaceholderText     { get; set; }
        public string       DescriptionPlaceholderText  { get; set; }
        public string       PublicText                  { get; set; }
        public string       PrivateText                 { get; set; }

        public byte[]       GroupPhotoData              { get; set; }
        public byte[]       GroupAvatarData             { get; set; }

        MemoryStream _imageStream;
        MemoryStream _avatarStream;

        public EditGroupViewModel (
            IGroupService groupService, 
            IGeoLocationConverterService locationConverterService,
            IUserService userService
        )
        {
            _userService                = userService;
            _locationConverterService   = locationConverterService;
            _groupService               = groupService;

            DisplayName                 = Localized("group.create.title");
            AddMembersLabel             = Localized("group.edit.add.members.label");
            GroupNamePlaceholderText    = Localized("group.edit.name.placeholder");
            LocationPlaceholderText     = Localized("group.edit.location.placeholder");
            DescriptionPlaceholderText  = Localized("group.edit.description.placeholder");
            PublicText                  = Localized("group.edit.privacy.value.public");
            PrivateText                 = Localized("group.edit.privacy.value.private");

            AddPictureCommand           = new CommandViewModel(Localized("group.edit.add.cover.command"), new MvxCommand(AddPicture));
            AddAvatarCommand            = new CommandViewModel(Localized("group.edit.add.avatar.command"), new MvxCommand(AddAvatar));
            ToggleVisibilityCommand     = new CommandViewModel(Localized("group.edit.togglevisibility.command"), new MvxCommand(ToggleVisibility));
            CreateCommand               = new CommandViewModel(Localized("group.edit.create.command"), new MvxCommand(CreateGroup));
            AddMembersCommand           = new CommandViewModel(new MvxCommand(AddMembers));
            DeleteCommand               = new CommandViewModel(new MvxCommand(DeleteGroup));
            CancelCommand               = new CommandViewModel(new MvxCommand(Cancel));
            SearchLocationCommand       = new CommandViewModel(new MvxCommand(SearchLocation));

            SelectGroupTypeCommand      = CreateSelectFromEnumCommand(Type,         "group.type.{0}", SelectGroup);
            SelectSportKindCommand      = CreateSelectFromEnumCommand(SportKind,    "group.sport.{0}", SelectSport);
        }

        public class Navigation { public string GroupId { get; set; } }
        public virtual async void Init (Navigation nav)
        {
            if (string.IsNullOrEmpty(nav.GroupId)) { return; }

            IsEditing = true;
            _groupId = nav.GroupId;
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    IUserGroup group = await _groupService.GetGroup(_groupId, token);

                    if (group != null) {
                        DisplayName     = Localized("group.edit.title");
                        GroupName       = group.Name; 
                        Description     = group.Description; 
                        //                    Type            = group.Type; 
                        //                    SportKind       = group.SportKind;
                        PhotoUrl        = group.GroupPhoto;
                        IsCreator       = group.CreatedBy.Id == _userService.LoggedInUser.Id;
                        AvatarUrl       = group.GroupAvatar;
                        Private         = !group.Private.HasValue || group.Private.Value;

                        _homelocation   = group.Location;
                        if (_homelocation != null) {
                            Hometown = ((Location) _homelocation).CityStateString();
                        }

                        IsChanged       = false;
                    }
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        protected override async void OnClosing ()
        {
            if (!_wasDeleted && IsChanged && IsEditing) { 
                await SaveIt(result => {}); 
                base.OnClosing();
            } else {
                base.OnClosing();
            }
            if (_avatarStream != null) { _avatarStream.Dispose(); }
            if (_imageStream != null) { _imageStream.Dispose(); }
        }

        async void CreateGroup()
        {
            await SaveIt(result => {
                if (result) {
                    Close(this); 
                }
            });
        }

        async Task SaveIt(Action<bool> completion)
        {
            // Validation
            if (string.IsNullOrEmpty(GroupName)) {
                GroupName = "random name";
                completion(false);
//                Alert(Localized("You must give your group a name!"));
//                return;
            }

            UserGroup group = new UserGroup {
                Id          = _groupId,
                Name        = GroupName ,
                Description = Description,
                SportKind   = "dummy",
                Type        = "dummy",
                Private     = Private,
            };

            if (_homelocation != null) { group.Location = _homelocation; }

            ProgressMessage = Localized("group.edit.save.progress.message");
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    IUserGroup updatedGroup;
                    if   (_groupId == null) { updatedGroup = await _groupService.CreateGroup(group, token); } 
                    else                    { updatedGroup = await _groupService.UpdateGroup(group, token); }

                    if (_imageStream != null) {
                        updatedGroup = await _groupService.UpdateGroupImage(updatedGroup, "", _imageStream);
                        _imageStream.Dispose();
                    }

                    if (_avatarStream != null) {
                        updatedGroup = await _groupService.UpdateAvatarImage(updatedGroup, "", _avatarStream);
                        _avatarStream.Dispose();
                    }

                    IsChanged = false;
                    completion(true);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        async void DeleteGroup()
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    await _groupService.DeleteGroup(_groupId);
                    _wasDeleted = true;
                    Close(this);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void Cancel()
        {
            Close(this);
        }

        void SelectGroup(GroupType type) { Type = type; }
        void SelectSport(SportKind kind) { SportKind = kind; }


        public void AddMembers()
        {
            ShowViewModel<InviteMembersViewModel>(new GroupMembersViewModel.Navigation { GroupId = _groupId });
        }

        public void AddPicture()
        {
            var task = Mvx.Resolve<IImagePickerService>();
            task.ChoosePictureFromLibrary(90, (object) "groupPhoto",
                stream => {
                if (_imageStream != null) { _imageStream.Dispose(); }
                _imageStream = new MemoryStream();
                stream.CopyTo(_imageStream);
                _imageStream.Seek(0, SeekOrigin.Begin);

                GroupPhotoData = _imageStream.ToArray();
            },
                () => {
                // perform any cancelled operation
            });
//            var task = Mvx.Resolve<IMvxPictureChooserTask>();
//            task.ChoosePictureFromLibrary(500, 90,
//                stream => {
//                    if (_imageStream != null) { _imageStream.Dispose(); }
//                    _imageStream = new MemoryStream();
//                    stream.CopyTo(_imageStream);
//                    _imageStream.Seek(0, SeekOrigin.Begin);
//
//                    GroupPhotoData = _imageStream.ToArray();
//                },
//                () => {
//                    // perform any cancelled operation
//                });
        }

        public void AddAvatar()
        {
            var task = Mvx.Resolve<IImagePickerService>();
            task.ChoosePictureFromLibrary(90, (object) "avatar",
                stream => {
                    if (_avatarStream != null) { _avatarStream.Dispose(); }
                    _avatarStream = new MemoryStream();
                    stream.CopyTo(_avatarStream);
                    _avatarStream.Seek(0, SeekOrigin.Begin);

                    GroupAvatarData = _avatarStream.ToArray();
                },
                () => {
                    // perform any cancelled operation
                });
        }

        public void ToggleVisibility()
        {
            Private = !Private;
        }

        async void SearchLocation()
        {
            if (string.IsNullOrEmpty(Hometown)) { return; }

            try {
                _homelocation = null;

                ProgressMessage = Localized("group.edit.location.search.progress.message");
                IsBusy = true;
                var addresses = await _locationConverterService.GeocodeAddressAsync(Hometown);
                if (addresses.Count > 0) { 
                    Placemark pl = addresses[0];
                    _homelocation = pl.ToLocation();
                    Hometown = _homelocation.City + ", " + _homelocation.StateProvince;
                }
            } catch (Exception /*ex*/) {
                Hometown = Localized("group.edit.location.invalid.text");
            } finally {
                IsBusy = false;
            }
        }
    }
}

