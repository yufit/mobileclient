﻿using System;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Interfaces.Services;
using YuFit.WebServices.Interface.Model;


namespace YuFit.Core.ViewModels.Associations.Groups
{
    public class SelectGroupPlanViewModel : BaseViewModel
    {
        string                  _groupId;
        readonly IGroupService  _groupService;

        public string SelectYourTitle               { get; private set; }
        public string GroupTitle                    { get; private set; }
        public string PlanTitle                     { get; private set; }
        public string MonthSingular                 { get; private set; }
        public string MonthPlural                   { get; private set; }

        #region Commands
        public CommandViewModel SelectPlan1Command { get; private set; }
        public CommandViewModel SelectPlan2Command { get; private set; }
        public CommandViewModel SelectPlan3Command { get; private set; }
        public CommandViewModel SelectPlan4Command { get; private set; }
        #endregion

        public class Navigation
        {
            public string Name { get; set; }
            public string GroupId { get; set; }
        }

        public void Init (Navigation nav)
        {
            _groupId = nav.GroupId;
            DisplayName = nav.Name;
        }

        public SelectGroupPlanViewModel (IGroupService groupService) : base("group.upgrade.select.plan.title")
        {
            _groupService           = groupService;

            SelectYourTitle         = Localized("group.upgrade.selectyour.title"    );
            GroupTitle              = Localized("group.upgrade.group.title"         );
            PlanTitle               = Localized("group.upgrade.plan.title"          );
            MonthSingular           = Localized("group.upgrade.month.singular"      );
            MonthPlural             = Localized("group.upgrade.month.plural"        );

            SelectPlan1Command      = new CommandViewModel( new MvxCommand( SelectPlan1 ));
            SelectPlan2Command      = new CommandViewModel( new MvxCommand( SelectPlan2 ));
            SelectPlan3Command      = new CommandViewModel( new MvxCommand( SelectPlan3 ));
            SelectPlan4Command      = new CommandViewModel( new MvxCommand( SelectPlan4 ));
        }

        async void SelectPlan (string planName, DateTime expirationDate)
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    IUserGroup group = await _groupService.GetGroup(_groupId, token);
                    group.Plus = true;
                    group.PlusSubscriptionLevel = planName;
                    group.PlusExpiresAt = expirationDate;
                    await _groupService.UpdateGroup(group, token);
                    Close(this);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void SelectPlan1 ()
        {
            var now = DateTime.Now;
            var expiration = now.AddMonths(1);
            SelectPlan("monthly", expiration);
        }

        void SelectPlan2 ()
        {
            var now = DateTime.Now;
            var expiration = now.AddMonths(3);
            SelectPlan("trimonthly", expiration);
        }

        void SelectPlan3 ()
        {
            var now = DateTime.Now;
            var expiration = now.AddMonths(6);
            SelectPlan("biyearly", expiration);
        }

        void SelectPlan4 ()
        {
            var now = DateTime.Now;
            var expiration = now.AddYears(1);
            SelectPlan("yearly", expiration);
        }
    }
}

