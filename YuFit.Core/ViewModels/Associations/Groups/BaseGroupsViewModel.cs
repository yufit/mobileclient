﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using Coc.MvvmCross.Plugins.Facebook;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Profiles;

using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.ViewModels.Associations.Groups
{
    public abstract class BaseGroupsViewModel : AssociationViewModel
    {
        protected readonly IUserService _userService;

        public SilentObservableCollection<GroupInfoViewModel> Groups { get; private set; } 

        public CommandViewModel ShowGroupCommand { get; private set; }
        public override bool CanAdd { get { return false; } }
        public override bool CanSearch { get { return false; } }

        public BaseGroupsViewModel (
            IUserService userService,
            string displayName
        ) : base(displayName)
        {
            _userService = userService;

            ShowGroupCommand = new CommandViewModel(new MvxCommand<GroupInfoViewModel>(ShowGroup));
            Groups = new SilentObservableCollection<GroupInfoViewModel>();
        }

        public override void Start ()
        {
            base.Start();
            PopulateGroups();
        }

        protected async void PopulateGroups()
        {
            ProgressMessage = Localized("groups.fetch.progress.message");
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var groups = await FetchGroups();
                    if (groups != null)
                    {
                        var tmp = new List<GroupInfoViewModel>();
                        groups.ForEach(group => tmp.Add(new GroupInfoViewModel(group)));

                        var orderedList = tmp.OrderBy(x => x.DisplayName).ToList();

                        Groups.Clear();
                        Groups.AddMultiple(orderedList);
                    }
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void ShowGroup (GroupInfoViewModel group)
        {
            ShowViewModel<GroupViewModel>(new GroupViewModel.Navigation {GroupId = group.Id, Name = group.DisplayName});
        }


        public override void Add ()
        {
            throw new NotImplementedException();
        }

        public override void Search ()
        {
            throw new NotImplementedException();
        }           

        protected abstract Task<IList<IUserGroup>> FetchGroups();
    }
}


