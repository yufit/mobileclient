﻿using System;
using System.Collections.Generic;
using System.Linq;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Profiles;

using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.ViewModels.Associations.Groups
{
    /// <summary>
    /// Members view model.  Represent a member.
    /// </summary>
    public class MemberViewModel : UserInfoViewModel, IComparable
    {
        public bool     IsCreator       { get; set; }
        public bool     IsManager       { get; set; }
        public bool     IsViewerAdmin   { get; set; }
        public bool     IsMe            { get; set; }
        public string   UserId          { get; private set; }
        public bool     IsInvited       { get; private set; }

        public Action<MemberViewModel>  ManagerToggledAction        { get; set; }
        public CommandViewModel         ToggleManagerRoleCommand    { get; private set; }

        public MemberViewModel (IGroupMember member, bool isViewerAdmin, string myId, string creatorId) : base(member.User as User)
        {
            Id              = member.Id;
            UserId          = member.User.Id;
            IsManager       = member.IsManager.HasValue && member.IsManager.Value;
            IsViewerAdmin   = isViewerAdmin;
            IsMe            = member.User.Id == myId;
            IsCreator       = member.User.Id == creatorId;

            ToggleManagerRoleCommand = new CommandViewModel(new MvxCommand<MemberViewModel>(ToggleManagerRole));
        }

        void ToggleManagerRole (MemberViewModel obj)
        {
            if (ManagerToggledAction != null) { ManagerToggledAction(this); }
        }

        #region IComparable implementation

        public int CompareTo (object obj)
        {
            var toCompareWith = (MemberViewModel) obj;
            if (IsManager && !toCompareWith.IsManager) { return -1; }
            if (!IsManager && toCompareWith.IsManager) { return  1; }

            return string.Compare(Name, toCompareWith.Name, StringComparison.Ordinal);
        }

        #endregion
    }

    /// <summary>
    /// Group members view model.
    /// The list of members in the group.
    /// </summary>
    public class GroupMembersViewModel : BaseViewModel
    {
        public class MemberCollection : SortedObservableCollection<MemberViewModel> {};

        string _groupId;

        readonly IGroupService _groupService;
        readonly IUserService _userService;

        public MemberCollection     Members             { get; private set; } 
        public bool                 IsAdministrator     { get; set; }

        public CommandViewModel     ShowUserProfileCommand { get; private set; }
        public CommandViewModel     AddUserToGroupCommand { get; private set; }

        public GroupMembersViewModel (
            IUserService userService, 
            IGroupService groupService
        ) : base("groupmembers.title")
        {
            _userService = userService;
            _groupService = groupService;

            ShowUserProfileCommand = new CommandViewModel(new MvxCommand<MemberViewModel>(ShowUserProfile));
            AddUserToGroupCommand = new CommandViewModel(new MvxCommand(InviteUsers));
            Members = new MemberCollection();
        }

        public class Navigation { public string GroupId { get; set; } public bool IsAdministrator { get; set; } }
        public virtual async void Init (Navigation nav)
        {
            _groupId = nav.GroupId;
            IsAdministrator = nav.IsAdministrator;

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var group = await _groupService.GetGroup(_groupId, token);

                    IList<IGroupMember> members = await _groupService.GetMembers(_groupId);
                    var isAdministrator = members.Any(member => member.User.Id == _userService.LoggedInUser.Id && member.IsManager == true);

                    var tmp = new List<MemberViewModel>();
                    members.ForEach(p => tmp.Add(new MemberViewModel(p, isAdministrator, _userService.LoggedInUser.Id, group.CreatedBy.Id)));

                    var orderedList = tmp.OrderBy(x => !x.IsManager).ThenBy(x => x.Name).ToList();

                    Members.Clear();
                    Members.AddMultiple(orderedList);
                    Members.ForEach(vm => vm.ManagerToggledAction = UpdateMemberStatus);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        async void UpdateMemberStatus (MemberViewModel updatedMembersVM)
        {
            if (!IsAdministrator) { return; }

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    await _groupService.SetMemberAdminStatus(_groupId, updatedMembersVM.Id, !updatedMembersVM.IsManager, token);

                    updatedMembersVM.IsManager = !updatedMembersVM.IsManager;
                    Members.Remove(updatedMembersVM);
                    Members.Insert(0, updatedMembersVM);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        protected override void OnClosing ()
        {
            Members.ForEach(vm => vm.ManagerToggledAction = null);
            base.OnClosing();
        }

        void ShowUserProfile (MemberViewModel entry)
        {
            ShowViewModel<FriendProfileViewModel>(new FriendProfileViewModel.Navigation {UserId = entry.UserId});
        }

        void InviteUsers()
        {
            ShowViewModel<InviteMembersViewModel>(new GroupMembersViewModel.Navigation { GroupId = _groupId });
        }
    }
}

