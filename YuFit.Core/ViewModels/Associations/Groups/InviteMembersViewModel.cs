﻿using System;
using System.Collections.Generic;
using System.Linq;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Profiles;

using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.ViewModels.Associations.Groups
{
    public class InvitableUserViewModel : UserInfoViewModel, IComparable
    {
        public CommandViewModel InviteCommand   { get; private set; }
        public bool             IsSelected      { get; set; }

        public InvitableUserViewModel (IUser user) : base(user as User)
        {
            InviteCommand = new CommandViewModel(new MvxCommand<InvitableUserViewModel>(Invite));
        }

        void Invite (InvitableUserViewModel obj)
        {
        }

        #region IComparable implementation

        public int CompareTo (object obj)
        {
            var toCompareWith = (MemberViewModel) obj;
//            if (IsManager && !toCompareWith.IsManager) { return -1; }
//            if (!IsManager && toCompareWith.IsManager) { return  1; }

            return string.Compare(Name, toCompareWith.Name, StringComparison.Ordinal);
        }

        #endregion
    }

    public class InviteMembersViewModel : BaseViewModel
    {
        string _groupId;

        private readonly IGroupService _groupService;
        private readonly IUserService _userService;

        public string SearchTerm { get; set; }
        public CommandViewModel SearchCommand { get; private set; }
        public CommandViewModel LoadMoreCommand { get; private set; }
        public CommandViewModel SelectFriendCommand { get; private set; }

        public SilentObservableCollection<InvitableUserViewModel> InvitableUsers { get; private set; } 

        public InviteMembersViewModel (
            IGroupService groupService, 
            IUserService userService
        ) : base("group.invite.members.title")
        {
            _groupService = groupService;
            _userService = userService;

            SearchTerm = string.Empty;
            SearchCommand = new CommandViewModel(new MvxCommand(Search));
            LoadMoreCommand = new CommandViewModel(new MvxCommand(LoadMore));
            SelectFriendCommand = new CommandViewModel(new MvxCommand<InvitableUserViewModel>(SelectFriend));

            InvitableUsers = new SilentObservableCollection<InvitableUserViewModel>();
        }

        public class Navigation { public string GroupId { get; set; } }
        public virtual async void Init (Navigation nav)
        {
            _groupId = nav.GroupId;

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var friends = await _userService.GetMyFriends(token);

                    var tmp = new List<InvitableUserViewModel>();
                    friends.ForEach(pp => tmp.Add(new InvitableUserViewModel(pp)));

                    var orderedList = tmp.OrderBy(x => x.Name).ToList();

                    InvitableUsers.Clear();
                    InvitableUsers.AddMultiple(orderedList);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        async void Search ()
        {
            ProgressMessage = Localized("group.invite.members.search.progress.message");

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    Mvx.Trace("Search term {0}", SearchTerm);
                    var users = await _userService.SearchUser(SearchTerm, 0, 10, token);
                    if (users != null) 
                    {
                        var tmp = new List<InvitableUserViewModel>();
                        users.ForEach(u => tmp.Add(new InvitableUserViewModel(u)));

                        var orderedList = tmp.OrderBy(x => x.DisplayName).ToList();
                        InvitableUsers.Clear();
                        InvitableUsers.AddMultiple(orderedList);
                    }
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void LoadMore ()
        {
        }

        void SelectFriend (InvitableUserViewModel entry)
        {
            entry.IsSelected = !entry.IsSelected;
        }

        protected async override void OnClosing ()
        {
            try {
                var selectedFriends = InvitableUsers.Where(iu => iu.IsSelected).Select(x => x.Id);
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => await _groupService.InviteUserToGroup(_groupId, selectedFriends.ToList(), token));
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }

            base.OnClosing();
        }


    }
}

