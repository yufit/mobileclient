﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using Coc.MvvmCross.Plugins.Facebook;

using PropertyChanged;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Activity;

using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Exceptions;
using YuFit.Core.ViewModels.CreateActivity;
using YuFit.Core.Extensions;
using YuFit.Core.ViewModels.Activity.Summary;

namespace YuFit.Core.ViewModels.Associations.Groups
{
    public class GroupViewModel : BaseViewModel
    {
        private readonly IGroupService _groupService;
        private readonly IUserService _userService;
        private string _groupId;

        public Uri          PhotoUrl                        { get; set; }
        public Uri          AvatarUrl                       { get; set; }
        public string       Type                            { get; set; }
        public SportKind    Sport                           { get; set; }
        public string       ManagedByLabel                  { get; set; }
        public string       ManagedBy                       { get; set; }
        public string       Description                     { get; set; }
        public string       MembersLabel                    { get; private set; }
        public string       UpgradeGroupLabel               { get; private set; }
        public int          NumberOfMembers                 { get; private set; }

        public bool         IsPublic                        { get; private set; }
        public bool         IsCreator                       { get; private set; }
        public bool         IsAdministrator                 { get; private set; }

        [AlsoNotifyFor("JoinLeaveLabel")]
        public bool         IsMember                        { get; private set; }
        public string       JoinLeaveLabel                  { get { return IsMember ? Localized("group.leave") : Localized("group.join"); } }

        public bool         IsPending                       { get; private set; }
        public string       MemberId                        { get; private set; }

        public int          ActivityCreatedCount            { get; set; }
        public string       UpcomingActivitiesTitle         { get; set; }
        public string       CreateNewActivityTitle          { get; set; }

        public bool         IsPlus                          { get; set; }
        public string       PlusSubscription                { get; set; }
        public DateTime     PlusSubscriptionExpiration      { get; set; }

        public bool         IsUpgradable                    { get; set; }

        public SilentObservableCollection<UpcomingActivityViewModel> UpcomingActivities { get; private set; } 

        #region Commands
        public CommandViewModel ViewMembersCommand          { get; private set; }
        public CommandViewModel EditGroupCommand            { get; private set; }
        public CommandViewModel JoinGroupCommand            { get; private set; }
        public CommandViewModel LeaveGroupCommand           { get; private set; }

        public CommandViewModel CreateActivityCommand       { get; private set; }
        public CommandViewModel ShowActivitySummaryCommand  { get; private set; }

        public CommandViewModel UpgradeGroupCommand         { get; private set; }
        public CommandViewModel ShowGroupPlusAdvCommand     { get; private set; }

        public CommandViewModel ShowCalendarCommand         { get; private set; }
        public CommandViewModel OpenWebSiteCommand          { get; private set; }
        public CommandViewModel OpenFacebookCommand         { get; private set; }
        #endregion

        #pragma warning disable 414
        readonly MvxSubscriptionToken _groupUpdatedMessageToken;
        readonly MvxSubscriptionToken _groupDeletedMessageToken;
        readonly MvxSubscriptionToken _groupEventCreatedToken;
        #pragma warning restore 414

        public class Navigation
        {
            public string Name { get; set; }
            public string GroupId { get; set; }
        }

        public GroupViewModel (IGroupService groupService, IUserService userService)
        {
            _groupService               = groupService;
            _userService                = userService;

            ManagedByLabel              = Localized("group.managed.by.label");
            Description                 = Localized("group.description.placeholder");
            MembersLabel                = Localized("group.members.label");
            UpgradeGroupLabel           = Localized("group.upgrade.label");
            UpcomingActivitiesTitle     = Localized("activity.upcoming.label");
            CreateNewActivityTitle      = Localized("activity.create.label");

            NumberOfMembers             = 0;

            ViewMembersCommand          = new CommandViewModel( new MvxCommand( ViewMembers           ));
            EditGroupCommand            = new CommandViewModel( new MvxCommand( EditGroup             ));
            JoinGroupCommand            = new CommandViewModel( new MvxCommand( JoinGroup             ));
            LeaveGroupCommand           = new CommandViewModel( new MvxCommand( LeaveGroup            ));
            CreateActivityCommand       = new CommandViewModel( new MvxCommand( CreateActivity        ));
            UpgradeGroupCommand         = new CommandViewModel( new MvxCommand( UpgradeGroup          ));
            ShowGroupPlusAdvCommand     = new CommandViewModel( new MvxCommand( ShowGroupPlusAdv      ));

            ShowCalendarCommand         = new CommandViewModel( new MvxCommand( ShowCalendar          ));
            OpenWebSiteCommand          = new CommandViewModel( new MvxCommand( OpenWebSite           ));
            OpenFacebookCommand         = new CommandViewModel( new MvxCommand( OpenFacebook          ));

            ShowActivitySummaryCommand  = new CommandViewModel( new MvxCommand<UpcomingActivityViewModel>( ShowActivitySummary   ));

            UpcomingActivities          = new SilentObservableCollection<UpcomingActivityViewModel>();

            _groupUpdatedMessageToken   = Messenger.Subscribe<GroupUpdatedMessage>(async (message) => await OnGroupUpdated(message));
            _groupDeletedMessageToken   = Messenger.Subscribe<GroupDeletedMessage>(OnGroupDeleted);
            _groupEventCreatedToken     = Messenger.Subscribe<GroupEventCreatedMessage>(async (message) => await OnGroupEventCreated(message));

        }

        void OnGroupDeleted (GroupDeletedMessage obj)
        {
            Task.Factory.StartNew(() => InvokeOnMainThread(async () => {
                await Task.Delay(200);
                Close(this);
            }));
        }

        async Task OnGroupUpdated (GroupUpdatedMessage obj)
        {
            // We need to re-fetch the info as the group passed in the message won't include
            // bits like if the user is admin... 
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    IUserGroup group = await _groupService.GetGroup(_groupId, token);
                    PopulateGroup(group);
                    await GetNextTraining();
                });
            } catch (NotFoundException /* exception */) {
                Alert("group.error.group.no.longer.exists", () => Close(this));
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        async Task OnGroupEventCreated(GroupEventCreatedMessage message)
        {
            if (message.GroupId != _groupId) {
                return;
            }

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    await GetNextTraining(); 
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        protected override void OnClosing ()
        {
            Messenger.Unsubscribe<GroupUpdatedMessage>(_groupUpdatedMessageToken);
            Messenger.Unsubscribe<GroupDeletedMessage>(_groupDeletedMessageToken);
        }

        public virtual async void Init (Navigation nav)
        {
            _groupId = nav.GroupId;

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    IUserGroup group = await _groupService.GetGroup(_groupId, token);
                    PopulateGroup(group);
                    await GetNextTraining();
                });
            } catch (NotFoundException /* exception */) {
                Alert("group.error.group.no.longer.exists", () => Close(this));
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void PopulateGroup(IUserGroup group) 
        {
            if (group == null) { return; }

            DisplayName                 = group.Name;
            Type                        = Localized(string.Format("group.type.{0}", group.Type).ToLower());
            PhotoUrl                    = group.GroupPhoto;
            AvatarUrl                   = group.GroupAvatar;
            ManagedBy                   = group.CreatedBy.Name;
            IsCreator                   = group.CreatedBy.Id == _userService.LoggedInUser.Id;
            Description                 = group.Description;
            IsAdministrator             = group.IsCurrentUserManager.HasValue && group.IsCurrentUserManager.Value;
            NumberOfMembers             = group.MemberCount;
            IsMember                    = group.IsCurrentUserAMember.HasValue && group.IsCurrentUserAMember.Value;
            IsPending                   = group.IsCurrentUserPending.HasValue && group.IsCurrentUserPending.Value;
            MemberId                    = group.CurrentMemberUserMemberId;
            IsPublic                    = !(group.Private.HasValue && group.Private.Value);
//            IsPlus                      = group.Plus.HasValue && group.Plus.Value;
//            PlusSubscription            = group.PlusSubscriptionLevel;
//            PlusSubscriptionExpiration  = group.PlusExpiresAt;
//            IsUpgradable                = !IsPlus && IsAdministrator;
        }

        private async Task GetNextTraining()
        {
            List<YuFit.Core.Models.FitActivity> incomingActivities = await _groupService.MyUpcomingActivities(_groupId);
            if (incomingActivities != null)
            {
                var tmp = new List<UpcomingActivityViewModel>();
                incomingActivities.ForEach(act => tmp.Add(new UpcomingActivityViewModel(act)));
                var orderedList = tmp.OrderBy(x => x.When).ToList();
                UpcomingActivities.Clear();
                UpcomingActivities.AddMultiple(orderedList);
            }
        }

        private void ViewMembers ()
        {
            ShowViewModel<GroupMembersViewModel>(new GroupMembersViewModel.Navigation { GroupId = _groupId, IsAdministrator = IsAdministrator });
        }

        private void EditGroup()
        {
            ShowViewModel<EditGroupViewModel>(new EditGroupViewModel.Navigation { GroupId = _groupId });
        }

        private void CreateActivity()
        {
            ShowViewModel<GroupCreateFitActivityViewModel>(new GroupCreateFitActivityViewModel.CreateFitActivityVMNavigation { GroupId = _groupId });
        }

        async void JoinGroup()
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    await _groupService.JoinGroup(_groupId, token);
                    IsMember = true;

                    var members = await _groupService.GetMembers(_groupId);
                    var meInList = members.ToList().FirstOrDefault(m => m.User.Id == Mvx.Resolve<IUserService>().LoggedInUser.Id);
                    if (meInList != null) { MemberId = meInList.Id; }
                    NumberOfMembers++;
                });
            } catch (NotFoundException /* exception */) {
                Alert("group.error.group.no.longer.exists", () => Close(this));
            } catch (ServerException exception) {
                if (exception.StatusCode == HttpStatusCode.Forbidden) {
                    Alert("group.error.not.authorized");
                }
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        async void LeaveGroup()
        {
            if (!IsMember) { return; }
            if (IsCreator) { WarnCreatorHeCantLeave(); return; }

            bool leaveGroup = await UserDialogs.ConfirmAsync(
                Localized("group.leave.message.text"), 
                Localized("group.leave.message.title"), 
                Localized("group.leave.message.yes.button"),
                Localized("group.leave.message.no.button")
            );

            if (leaveGroup) {
                try {
                    await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                        await _groupService.LeaveGroup(_groupId, MemberId, token);
                        IsMember = false;
                        MemberId = string.Empty;
                        NumberOfMembers--;
                    });
                } catch (NotFoundException /* exception */) {
                    Alert("group.error.group.no.longer.exists", () => Close(this));
                } catch (ServerException exception) {
                    if (exception.StatusCode == HttpStatusCode.Forbidden) {
                        Alert("group.error.only.administrator");
                    }
                } catch (Exception exception) {
                    Mvx.Trace(exception.ToString());
                }
            }
        }

        private void UpgradeGroup()
        {
            ShowViewModel<SelectGroupPlanViewModel>(new SelectGroupPlanViewModel.Navigation {
                Name = DisplayName,
                GroupId = _groupId
            });
        }

        void ShowGroupPlusAdv()
        {
//            ShowCalendar();
            ShowViewModel<GroupPlusAdvantagesViewModel>();
        }

        void ShowActivitySummary(UpcomingActivityViewModel obj)
        {
            ShowViewModel<ActivitySummary2ViewModel>(new ActivitySummary2ViewModel.Nav { Id = obj.Id });
        }

        void ShowCalendar ()
        {
            ShowViewModel<GroupCalendarViewModel>(new GroupCalendarViewModel.Nav { 
                Name = DisplayName,
                Id = _groupId 
            });
        }

        void OpenWebSite  ()
        {
            var browserTask = Mvx.Resolve<IBrowserTask>();
            browserTask.ShowWebPage("http://digg.com");
        }

        void OpenFacebook ()
        {
            var fbService = Mvx.Resolve<IFacebookService>();
            fbService.OpenPage("https://www.facebook.com/Aerial-Tree-Experts-327024197430816/");
        }

        void WarnCreatorHeCantLeave()
        {
            Alert("group.error.creator.leaving");
        }
    }
}

