﻿using System.Collections.Generic;
using System.Threading.Tasks;

using YuFit.Core.Interfaces.Services;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.ViewModels.Associations.Groups
{
    public class GroupsOfFriendViewModel : BaseGroupsViewModel
    {
        string _userId;

        public GroupsOfFriendViewModel (
            IUserService userService
        ) : base(userService, "groups.of.friend.title")
        {
        }

        public class Navigation { public string UserId { get; set; } public string AvatarUrl {get; set; } }
        public virtual void Init(Navigation navParams)
        {
            _userId = navParams.UserId;
            PopulateGroups();
        }

        protected override async Task<IList<IUserGroup>> FetchGroups()
        {
            return await _userService.GetGroupsOf(_userId);
        }
    }
}

