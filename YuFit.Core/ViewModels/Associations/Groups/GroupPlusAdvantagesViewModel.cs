﻿using System;
using YuFit.Core.Utils;
using System.Collections.Generic;

namespace YuFit.Core.ViewModels.Associations.Groups
{
    public class GroupPlusAdvantagesViewModel : BaseViewModel
    {
        public string GroupTitle                { get; private set; }
        public string ExtendedFeaturesTitle     { get; private set; }
        public string FirstInSearchTitle        { get; private set; }
        public string CustomIconOnMapTitle      { get; private set; }
        public string AdvancedCalendarTitle     { get; private set; }
        public string RecuringActivitiesTitle   { get; private set; }
        public string EntryFeeActivitiesTitle   { get; private set; }
        public string WebsiteLinkTitle          { get; private set; }
        public string NoAdvertisingTitle        { get; private set; }
        public string BiggerCoverPicture        { get; private set; }

        public List<string> Features            { get; private set; }

        public GroupPlusAdvantagesViewModel ()
        {
            GroupTitle                = Localized("group.plus.advantages.group.title"               );
            ExtendedFeaturesTitle     = Localized("group.plus.advantages.extendedfeatures.title"    );
            FirstInSearchTitle        = Localized("group.plus.advantages.firstinsearch.title"       );
            CustomIconOnMapTitle      = Localized("group.plus.advantages.customicononmap.title"     );
            AdvancedCalendarTitle     = Localized("group.plus.advantages.advancedcalendar.title"    );
            RecuringActivitiesTitle   = Localized("group.plus.advantages.recuringactivities.title"  );
            EntryFeeActivitiesTitle   = Localized("group.plus.advantages.entryfeeactivities.title"  );
            WebsiteLinkTitle          = Localized("group.plus.advantages.websitelink.title"         );
            NoAdvertisingTitle        = Localized("group.plus.advantages.noadvertising.title"       );
            BiggerCoverPicture        = Localized("group.plus.advantages.biggercoverpicture.title"  );

            Features = new List<string>();
            Features.Add(FirstInSearchTitle);
            Features.Add(CustomIconOnMapTitle);
            Features.Add(AdvancedCalendarTitle);
            Features.Add(RecuringActivitiesTitle);
            Features.Add(EntryFeeActivitiesTitle);
            Features.Add(WebsiteLinkTitle);
            Features.Add(NoAdvertisingTitle);
            Features.Add(BiggerCoverPicture);
        }
    }
}

