﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using Coc.MvvmCross.Plugins.Facebook;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Profiles;

using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.ViewModels.Associations.Friendships
{
    public abstract class BaseFriendsViewModel : AssociationViewModel
    {
        protected readonly IUserService _userService;
        protected IFacebookFriendInvitor _invitor;

        public SilentObservableCollection<UserInfoViewModel> Friends { get; private set; } 

        public CommandViewModel ShowUserProfileCommand { get; private set; }
        public override bool CanAdd { get { return false; } }
        public override bool CanSearch { get { return false; } }

       protected BaseFriendsViewModel (
            IUserService userService, 
            IFacebookService facebookService,
            string displayName) : base(displayName)
        {
            _userService = userService;
            _invitor = facebookService.GetInvitor();

            ShowUserProfileCommand = new CommandViewModel(new MvxCommand<UserInfoViewModel>(ShowUserProfile));
            Friends = new SilentObservableCollection<UserInfoViewModel>();
        }

        public override void Start ()
        {
            base.Start();
            PopulateFriends();
        }

        protected async void PopulateFriends()
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var friends = await FetchFriends();
                    if (friends != null)
                    {
                        var tmp = new List<UserInfoViewModel>();
                        friends.ForEach(friend => tmp.Add(new UserInfoViewModel((User) friend)));

                        var orderedList = tmp.OrderBy(x => x.Name).ToList();

                        Friends.Clear();
                        Friends.AddMultiple(orderedList);
                    }
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
                Xamarin.Insights.Report(exception);
            }
        }

        protected void ShowUserProfile (UserInfoViewModel entry)
        {
            ShowViewModel<FriendProfileViewModel>(
                new FriendProfileViewModel.Navigation {UserId = entry.Id}
            );
        }


        public override void Add ()
        {
            throw new NotImplementedException();
        }

        public override void Search ()
        {
            throw new NotImplementedException();
        }           

        protected abstract Task<IList<IUser>> FetchFriends();
    }
}

