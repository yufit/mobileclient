﻿using System.Collections.Generic;
using System.Threading.Tasks;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using Coc.MvvmCross.Plugins.Facebook;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;

using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.ViewModels.Associations.Friendships
{
    public class MyFriendsViewModel : BaseFriendsViewModel
    {
        public override bool CanAdd { get { return true; } }
        public override bool CanSearch { get { return true; } }

        #pragma warning disable 414
        readonly MvxSubscriptionToken _friendAddedMessageToken;
        readonly MvxSubscriptionToken _friendRemoveddMessageToken;
        #pragma warning restore 414


        public MyFriendsViewModel (
            IUserService userService, 
            IFacebookService facebookService) : base(userService, facebookService, "friends.title")
        {
            _friendAddedMessageToken = Messenger.Subscribe<FriendAddedMessage>(OnFriendAdded);
            _friendRemoveddMessageToken = Messenger.Subscribe<FriendRemovedMessage>(OnFriendRemoved);
        }

        void InviteFriends()
        {
            _invitor.OperationComplete = (canceled, error) => {
                _invitor.OperationComplete = null;
                //_invitor = null;
            };

            _invitor.InviteFacebookFriends();
        }

        public override void Add ()
        {
            InviteFriends();
        }

        public override void Search ()
        {
            SearchForFriends();
        }

        private void SearchForFriends()
        {
            ShowViewModel<SearchForFriendsViewModel>();
        }

        private void OnFriendAdded(FriendAddedMessage message)
        {
            PopulateFriends();
        }

        private void OnFriendRemoved(FriendRemovedMessage message)
        {
            PopulateFriends();
        }

        protected override void OnClosing ()
        {
            Messenger.Unsubscribe<FriendAddedMessage>(_friendAddedMessageToken);
            Messenger.Unsubscribe<FriendRemovedMessage>(_friendRemoveddMessageToken);
            base.OnClosing();
        }

        protected override async Task<IList<IUser>> FetchFriends()
        {
            return await _userService.GetMyFriends();
        }
    }
}

