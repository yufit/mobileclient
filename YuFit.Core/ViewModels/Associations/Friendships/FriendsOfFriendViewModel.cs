﻿using System.Collections.Generic;
using System.Threading.Tasks;


using Coc.MvvmCross.Plugins.Facebook;

using YuFit.Core.Interfaces.Services;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.ViewModels.Associations.Friendships
{
    public class FriendsOfFriendViewModel : BaseFriendsViewModel
    {
        string _userId;

        public FriendsOfFriendViewModel (
            IUserService userService, 
            IFacebookService facebookService) : base(userService, facebookService, "friends.of.friend.title")
        {
        }

        public class Navigation { public string UserId { get; set; } public string AvatarUrl {get; set; } }
        public virtual void Init(Navigation navParams)
        {
            _userId = navParams.UserId;
            PopulateFriends();
        }

        protected override async Task<IList<IUser>> FetchFriends()
        {
            return await _userService.GetFriendsOf(_userId);
        }
    }
}

