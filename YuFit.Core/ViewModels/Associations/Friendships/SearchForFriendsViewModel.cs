﻿using System.Collections.Generic;
using System.Linq;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Profiles;
using System;

namespace YuFit.Core.ViewModels.Associations.Friendships
{
    public class SearchForFriendsViewModel : BaseViewModel
    {
        private readonly IUserService _userService;

        public string SearchTerm { get; set; }

        public CommandViewModel SearchCommand { get; private set; }
        public CommandViewModel LoadMoreCommand { get; private set; }
        public CommandViewModel ShowUserProfileCommand { get; private set; }

        public SilentObservableCollection<UserInfoViewModel> Users { get; private set; } 

        public SearchForFriendsViewModel (
            IUserService userService
        ) : base("friends.search.title")
        {
            _userService = userService;

            SearchTerm = string.Empty;
            SearchCommand = new CommandViewModel(new MvxCommand(Search));
            LoadMoreCommand = new CommandViewModel(new MvxCommand(LoadMore));

            ShowUserProfileCommand = new CommandViewModel(new MvxCommand<UserInfoViewModel>(ShowUserProfile));
            Users = new SilentObservableCollection<UserInfoViewModel>();
        }

        async void Search ()
        {
            ProgressMessage = Localized("friends.search.progress.message");

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    Mvx.Trace("Search term {0}", SearchTerm);

                    var users = await _userService.SearchUser(SearchTerm, 0, 10, token);
                    if (users != null) 
                    {
                        var tmp = new List<UserInfoViewModel>();
                        users.ForEach(user => tmp.Add(new UserInfoViewModel((User)user)));

                        var orderedList = tmp.OrderBy(x => x.DisplayName).ToList();
                        Users.Clear();
                        Users.AddMultiple(orderedList);
                    }
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void LoadMore ()
        {
        }

        void ShowUserProfile (UserInfoViewModel user)
        {
            ShowViewModel<FriendProfileViewModel>(new FriendProfileViewModel.Navigation { UserId = user.Id });
        }
    }
}

