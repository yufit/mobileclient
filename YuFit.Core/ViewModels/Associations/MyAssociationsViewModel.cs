﻿using System.Collections.ObjectModel;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.ViewModels.Associations.Friendships;
using YuFit.Core.ViewModels.Associations.Groups;

namespace YuFit.Core.ViewModels.Associations
{
    public class MyAssociationsViewModel : BaseViewModel
    {
        public ObservableCollection<AssociationViewModel> Pages { get; private set; }

        private int _selectedPage;
        public int SelectedPage { 
            get { return _selectedPage; }
            set 
            {
                CanAdd = Pages[value].CanAdd;
                CanSearch = Pages[value].CanSearch; 
                _selectedPage = value;
            }
        }

        public bool CanAdd { get; set; }
        public bool CanSearch { get; set; }

        public CommandViewModel AddCommand { get; private set; }
        public CommandViewModel SearchCommand { get; private set; }

        public MyAssociationsViewModel ()
        {
            DisplayName = Localized("associations.title");

            Pages = new ObservableCollection<AssociationViewModel>();

            Pages.Add(Mvx.IocConstruct<MyFriendsViewModel>());
            Pages.Add(Mvx.IocConstruct<MyGroupsViewModel>());

            AddCommand = new CommandViewModel(new MvxCommand(AddSomething));
            SearchCommand = new CommandViewModel(new MvxCommand(SearchForSomething));

            _selectedPage = 0;
            CanAdd = Pages[_selectedPage].CanAdd;
            CanSearch = Pages[_selectedPage].CanSearch; 
        }

        void AddSomething()
        {
            var currentPage = Pages[SelectedPage];
            currentPage.Add();
        }

        void SearchForSomething()
        {
            var currentPage = Pages[SelectedPage];
            currentPage.Search();
        }
    }
}

