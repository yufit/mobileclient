﻿using System;
using System.Collections.Generic;
using System.Linq;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using Humanizer;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.Services.Notifications;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Activity.Details;
using YuFit.Core.ViewModels.Activity.Summary;
using YuFit.Core.ViewModels.Associations.Groups;
using YuFit.Core.ViewModels.Profiles;
using YuFit.Core.ViewModels.Event;

namespace YuFit.Core.ViewModels.Notifications
{
    public class NotificationViewModel : BaseViewModel
    {
        public PushNotificationCategory Category { get; set; }
        public IPushNotification PushNotification { get; private set; }

        public string   Message         { get; private set; }
        public DateTime When            { get; private set; }
        public bool     IsAcked         { get; private set; }
        public string   WhenAsString    { get; private set; }

        public NotificationViewModel (NotificationRecord record)
        {
            Category         = record.PushNotification.Category;
            Message          = record.PushNotification.Message;
            When             = record.CreatedAt;
            WhenAsString     = record.CreatedAt.Humanize();
            IsAcked          = record.IsAcked;
            PushNotification = record.PushNotification;
        }

        #if DEBUG_LEAKS
        ~NotificationViewModel () { Mvx.Trace("Deleting NotificationViewModel"); }
        #endif

        public virtual void HandleNotification() {}
    }

    public class EventNotificationViewModel : NotificationViewModel
    {
        public string   Sport               { get; private set; }
        public string   SubSport            { get; private set; }
        public Uri      CreatorAvatarUri    { get; private set; }

        public EventNotificationViewModel (NotificationRecord record) : base(record)
        {
            BaseEventPushNotification notification = record.PushNotification as BaseEventPushNotification;
            Sport = notification.Sport;
            SubSport = notification.SubSport;
            if (!string.IsNullOrEmpty(notification.CreatorAvatarUrl)) {
                CreatorAvatarUri = new Uri(notification.CreatorAvatarUrl);
            }
        }

        public override void HandleNotification ()
        {
            bool promoted = IsThisAPromotedEventNotification();
            if (PushNotification is EventCommentPostedNotification) {
                if (!promoted) {
                    ShowViewModel<ViewFitActivityDetailsViewModel>(new ViewFitActivityDetailsViewModel.Navigation {ActivityId = ((BaseEventPushNotification)PushNotification).EventId});
                } else {
                    ShowViewModel<EventMessagesViewModel>(new EventMessagesViewModel.Navigation { EventId = ((BaseEventPushNotification)PushNotification).EventId, EventCategory = Sport});
                }
            } else {
                if (!promoted) {
                    ShowViewModel<ActivitySummary2ViewModel>(new ActivitySummary2ViewModel.Nav {Id = ((BaseEventPushNotification)PushNotification).EventId});
                } else {
                    ShowViewModel<EventViewModel>(new EventViewModel.Nav { Id = ((BaseEventPushNotification)PushNotification).EventId});
                }
            }
        }

        // THIS IS A REAL HACK.  FOR THE MOMENT, THERE IS NO WAY TO KNOW IF AN EVENT WAS FOR A PROMOTED EVENT OR NOT.
        // TODO ASK JEFF IF HE CAN PUT A FLAG IN THE NOTIFICATION INFO STRUCTURE  FOR NOW WE USE SPORT AND SUBSPORT.
        // EVEN IF SOME ARE THE SAME AS YUFIT ACTIVITY, THEY ARE STORED all lowercase in event and Camelcase for yufit.
        protected bool IsThisAPromotedEventNotification()
        {
            bool isThisAPromotedEventNotification = false;

            var categories = Mvx.Resolve<IFitActivityService>().GetEventCategories();
            var category = categories.FirstOrDefault(c => c.Name == Sport);
            if (category != null) {
                var subsport = category.EventKinds.FirstOrDefault(ss => ss == SubSport);
                isThisAPromotedEventNotification = subsport != null;
            }
            return isThisAPromotedEventNotification;
        }
    }

    public class PromotedEventNotificationViewModel : EventNotificationViewModel
    {
        public Uri      EventIconUrl        { get; private set; }

        public PromotedEventNotificationViewModel (NotificationRecord record) : base(record)
        {
            EventPromotedNotification notification = record.PushNotification as EventPromotedNotification;
            if (!string.IsNullOrEmpty(notification.EventIconUrl)) {
                EventIconUrl = new Uri(notification.EventIconUrl);
            }
        }

        public override void HandleNotification ()
        {
            ShowViewModel<EventViewModel>(new EventViewModel.Nav { Id = ((BaseEventPushNotification)PushNotification).EventId});
        }
    }

    public class GroupNotificationViewModel : NotificationViewModel
    {
        public Uri GroupAvatarUri { get; private set; }

        public GroupNotificationViewModel (NotificationRecord record) : base(record)
        {
            BaseGroupPushNotification notification = record.PushNotification as BaseGroupPushNotification;
            if (!string.IsNullOrEmpty(notification.GroupAvatarUrl)) {
                GroupAvatarUri = new Uri(notification.GroupAvatarUrl);
            }
        }

        public override void HandleNotification ()
        {
            ShowViewModel<GroupViewModel>(new GroupViewModel.Navigation {GroupId = ((BaseGroupPushNotification)PushNotification).GroupId});
        }
    }

    public class FriendNotificationViewModel : NotificationViewModel
    {
        public Uri AvatarUri { get; private set; }

        public FriendNotificationViewModel (NotificationRecord record) : base(record)
        {
            BaseFriendPushNotification notification = record.PushNotification as BaseFriendPushNotification;
            if (!string.IsNullOrEmpty(notification.UserAvatarUrl)) {
                AvatarUri = new Uri(notification.UserAvatarUrl);
            }
        }

        public override void HandleNotification ()
        {
            ShowViewModel<FriendProfileViewModel>(new FriendProfileViewModel.Navigation {UserId = ((BaseFriendPushNotification)PushNotification).UserId});
        }
    }

    public class NotificationsViewModel : BaseViewModel
    {
        readonly IPushNotificationService _pushNotificationService;

        public SilentObservableCollection<NotificationViewModel> Notifications { get; private set; }
        public CommandViewModel HandleNotificationCommand { get; private set; }

        public NotificationsViewModel (IPushNotificationService pushNotificationService)
        {
            _pushNotificationService = pushNotificationService;
            Notifications = new SilentObservableCollection<NotificationViewModel>();
            HandleNotificationCommand = new CommandViewModel(new MvxCommand<NotificationViewModel>(HandleNotification));
            DisplayName = Localized("notifications.title");
        }

        public override void Start ()
        {
            LoadViewModel();
            base.Start();
        }

        private async void LoadViewModel()
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var notifications = await _pushNotificationService.GetNotifications(token);

                    var tmp = new List<NotificationViewModel>();
                    notifications.ForEach(p => {
                        if      (p.PushNotification is BaseEventPushNotification) { tmp.Add(new EventNotificationViewModel(p));             }
                        else if (p.PushNotification is EventPromotedNotification) { tmp.Add(new PromotedEventNotificationViewModel(p));     }
                        else if (p.PushNotification is BaseGroupPushNotification) { tmp.Add(new GroupNotificationViewModel(p));             }
                        else if (p.PushNotification is BaseFriendPushNotification){ tmp.Add(new FriendNotificationViewModel(p));            }
                        else if (p.PushNotification != null)                      { tmp.Add(new NotificationViewModel(p));                  }
                    });
                    var orderedList = tmp.OrderByDescending(x => x.When).ToList();

                    Notifications.Clear();
                    Notifications.AddMultiple(orderedList);

                    await _pushNotificationService.AckAllNotifications();
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void HandleNotification (NotificationViewModel entry) {
            entry.HandleNotification();
        }
    }
}

