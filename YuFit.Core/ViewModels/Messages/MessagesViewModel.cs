﻿using System;

namespace YuFit.Core.ViewModels.Messages
{
    public class MessagesViewModel : CommingSoonViewModel
    {
        public MessagesViewModel () : base("send.messages.title") 
        {
            ComingSoonMessage = Localized("send.messages.message");
        }
    }
}

