﻿using YuFit.Core.Models.Activities;
using System.Collections.Generic;
using System;

namespace YuFit.Core.ViewModels.Dashboard.ActivitySummary
{
    public class RunningActivityDetailsViewModel : BaseActivityDetailsViewModel<RunningActivitySettings>
    {
        private const string localPrefixKey = "crudDisplayInfoRunning - ";

        private int _distance;
        public int Distance { 
            get { return _distance; } 
            set { _distance = value; RaisePropertyChanged(() => Distance); }
        }

        private int _duration;
        public int Duration { 
            get { return _duration; } 
            set { _duration = value; RaisePropertyChanged(() => Duration); }
        }

        public string Speed { get { return string.Format("{0:0.00}", ActivitySettings.Speed); } }
        public string SpeedUnit { get; private set; }

        public List<Tuple<string, object>> DurationValues { get { return YuFit.Core.Models.Activities.DurationValues.Running; } }
        public List<Tuple<string, object>> DistanceValues { get { return YuFit.Core.Models.Activities.DistanceValues.Running; } }

        public RunningActivityDetailsViewModel (RunningActivitySettings settings) : base("", settings)
        {
            UpdateValues();
        }

        public override void Update ()
        {
            UpdateValues();
            base.Update();
        }

        void UpdateValues()
        {

            Distance = ActivitySettings.Distance;
            Duration = ActivitySettings.Duration;

            // TODO This needs to be using the system local to use mph or km
            SpeedUnit = "KM/H";
        }
    }
}

