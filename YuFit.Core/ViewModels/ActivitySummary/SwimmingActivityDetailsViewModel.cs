﻿using YuFit.Core.Models.Activities;
using System.Collections.Generic;
using System;

namespace YuFit.Core.ViewModels.Dashboard.ActivitySummary
{
    public class SwimmingActivityDetailsViewModel : BaseActivityDetailsViewModel<SwimmingActivitySettings>
    {
        private const string localPrefixKey = "crudDisplayInfoSwimming - ";

        private int _distance;
        public int Distance { 
            get { return _distance; } 
            set { _distance = value; RaisePropertyChanged(() => Distance); }
        }

        private int _duration;
        public int Duration { 
            get { return _duration; } 
            set { _duration = value; RaisePropertyChanged(() => Duration); }
        }

        private SwimmingType _swimmingType;
        public SwimmingType SwimmingType
        {
            get { return _swimmingType; }
            set { _swimmingType = value; RaisePropertyChanged(() => SwimmingType); }
        }

        public List<Tuple<string, object>> DurationValues { get { return YuFit.Core.Models.Activities.DurationValues.Swimming; } }
        public List<Tuple<string, object>> DistanceValues { get { return YuFit.Core.Models.Activities.DistanceValues.Swimming; } }


        public SwimmingActivityDetailsViewModel (SwimmingActivitySettings settings) : base("", settings)
        {
            UpdateValues();
        }

        public override void Update ()
        {
            UpdateValues();
            base.Update();
        }

        void UpdateValues()
        {
            Distance = ActivitySettings.Distance;
            Duration = ActivitySettings.Duration;
            SwimmingType = ActivitySettings.SwimmingType;
        }
    }
}

