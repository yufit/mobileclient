﻿using System;

namespace YuFit.Core.ViewModels
{
    public class CommingSoonViewModel : BaseViewModel
    {
        public string ComingSoonMessage { get; protected set; }

        public CommingSoonViewModel() : base("coming.soon.title")
        {
            ComingSoonMessage = Localized("coming.soon.message");
        }

        public CommingSoonViewModel (string title) : base(title)
        {
            ComingSoonMessage = Localized("coming.soon.message");
        }
    }
}

