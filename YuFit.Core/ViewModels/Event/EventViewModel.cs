﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using Coc.MvvmCross.Plugins.Facebook;
using Coc.MvvmCross.Plugins.Location;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Models;
using YuFit.Core.ViewModels.Activity.Participant;

using YuFit.WebServices.Exceptions;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.ViewModels.Event
{
    public class EventViewModel : BaseViewModel
    {
        #region Data Models

        readonly IFitActivityService _fitActivityService;
        readonly IUserService _userService;

        #pragma warning disable 414
        readonly MvxSubscriptionToken _activityUpdatedMessageToken;
        readonly MvxSubscriptionToken _activityDeletedMessageToken;
        #pragma warning restore 414

        #endregion

        public string Id { get; set; }
        public ILocation Location { get; set; }

        #region Properties

        #region Notifiable properties

        public bool IsLoaded { get; set; }
        public bool IsCreator { get; set; }
        public DateTime StartTime { get; set; }
        public string Sport { get; set; }
        public string SportSubType { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsExpired { get; set; }
        public Uri Image { get; set; }
        public Uri Avatar { get; set; }
        public Colors Colors { get; set; }
        public List<FitActivityParticipant> Participants { get; set; }
        public bool IamIn { get; set; }
        public string WebsiteUrl { get; protected set; }
        public string FacebookUrl { get; protected set; }
        public string Address { get; set; }
        public string GoingCount { get; set; }
        public string Description { get; private set; }

        #endregion

        #region Localized String properties

        public string JoinTitle { get; private set; }
        public string IAmInTitle { get; private set; }
        public string InvitedTitle { get; private set; }
        public string IamInString { get { return IamIn ? IAmInTitle : JoinTitle; } }
        public string CancelString { get; set; }
        public string ExpiredString { get; set; }

        #endregion

        #region Commands VM

        public CommandViewModel JoinCommand                     { get; private set; }
        public CommandViewModel ShowInvitedParticipantsCommand  { get; private set; }
        public CommandViewModel OpenWebSiteCommand              { get; private set; }
        public CommandViewModel OpenFacebookCommand             { get; private set; }
        public CommandViewModel ShowCommentsCommand             { get; private set; }

        #endregion

        #endregion

        #region Construction/Initialization
        public EventViewModel (
            IUserService userService, 
            IFitActivityService fitActivityService)
        {
            _fitActivityService = fitActivityService;
            _userService = userService;
            Participants = new List<FitActivityParticipant>();

            JoinTitle = Localized("activity.summary.join.label");
            IAmInTitle = Localized("activity.summary.im.in.label");
            InvitedTitle = Localized("activity.summary.invited.label");
            CancelString = Localized("activity.summary.canceled.label");
            ExpiredString = Localized("activity.summary.expired.label");

            ShowCommentsCommand             = new CommandViewModel(new MvxCommand(ShowComments));
            JoinCommand                     = new CommandViewModel(new MvxCommand(Join));
            ShowInvitedParticipantsCommand  = new CommandViewModel(new MvxCommand(ShowInvitedParticipants));
            OpenWebSiteCommand              = new CommandViewModel(new MvxCommand(OpenWebSite));
            OpenFacebookCommand             = new CommandViewModel(new MvxCommand(OpenFacebook));

            CancelString = Localized("activity.summary.canceled.label");

            _activityUpdatedMessageToken = Messenger.Subscribe<EventUpdatedMessage>(OnActivityUpdated);
            _activityDeletedMessageToken = Messenger.Subscribe<EventDeletedMessage>(OnActivityDeleted);
        }

        public class Nav { public string Id { get; set; } }
        public async void Init(Nav navigationData)
        {
            Id = navigationData.Id;
            string meId = _userService.LoggedInUser.Id;

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    // Get the info from the server api (which may cache thing)
                    FitActivity activity = await _fitActivityService.GetActivity(Id, token);
                    if (activity != null) {
                        var categories = Mvx.Resolve<IFitActivityService>().GetEventCategories();
                        var category = categories.FirstOrDefault(c => c.Name == activity.Sport);
                        Colors = category?.Colors ?? new Colors { 
                            Base = new Color { Red=255,Blue=255,Green=255 },
                            Dark = new Color { Red=255,Blue=255,Green=255 },
                            Light = new Color { Red=255,Blue=255,Green=255 }
                        };

                        Sport = Localized(activity.Sport.ToLower());
                        SportSubType = Localized(string.Format("{0}.{1}", activity.Sport, activity.SubSport).ToLower());
                        //Location = new CocLocation { Coordinates = new CocCoordinates {Latitude = ((Location) activity.Location).GeoCoordinates.Latitude, Longitude = ((Location) activity.Location).GeoCoordinates.Longitude } };
                        Location = activity.Location;
                        Participants = activity.Participants.Cast<FitActivityParticipant>().ToList();
                        StartTime = ((EventSchedule) activity.EventSchedule).LocalStartTime;
                        IsCreator = Mvx.Resolve<IUserService>().AmITheCreatorOfThisActivity(activity);
                        IsCanceled = activity.IsCanceled.HasValue && activity.IsCanceled.Value;
                        IsExpired = activity.EventSchedule.StartTime < DateTime.UtcNow;
                        Image = CreateUri(activity.EventPhotoUrl);
                        Avatar = CreateUri(activity.IconUrl);
                        WebsiteUrl = activity.WebsiteUrl;
                        FacebookUrl = activity.FacebookUrl;
                        DisplayName = activity.Name;
                        Address = ((Location) activity.Location).FullAddress();
                        GoingCount = Participants.Count.ToString();
                        Description = activity.Description;

                        var me = Participants.FirstOrDefault(p => p.UserId == meId);
                        if (me != null) {
                            IamIn = me.Going.HasValue && me.Going == true;
                        } else {
                            IamIn = false;
                        }

                        Mvx.Trace(activity.ToString());
                    } else {
                        Mvx.Trace(string.Format("Error: got a bad event id: {0}", Id));
                    }

                    IsLoaded = true;
                });
            } catch (NotFoundException /*exception*/) {
                Alert ("activity.unavailable", null, "Error");
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }
        #endregion

        Uri CreateUri(string url)
        {
            try {
                return new Uri(url);
            } catch (Exception /*ex*/) {
                return null;
            }
        }

        void OnActivityUpdated (EventUpdatedMessage obj)
        {
            if (Id != obj.Activity.Id) { return; }

            var activity = obj.Activity;
            Location = activity.Location;
            Participants = activity.Participants.Cast<FitActivityParticipant>().ToList();
            StartTime = ((EventSchedule) activity.EventSchedule).LocalStartTime;
            IsCreator = Mvx.Resolve<IUserService>().AmITheCreatorOfThisActivity(activity);
            SportSubType = LocalizedSubsport(activity);
        }

        void OnActivityDeleted (EventDeletedMessage obj)
        {
            Task.Factory.StartNew(() => InvokeOnMainThread(async () => {
                await Task.Delay(200);
                Close(this);
            }));
        }


        #region Methods

        #region Command handlers

        void ShowComments ()
        {
            ShowViewModel<EventMessagesViewModel>(new EventMessagesViewModel.Navigation { EventId = Id, EventCategory = Sport });
        }

        async void Join ()
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    IamIn = !IamIn;
                    await _fitActivityService.SetGoing(Id, IamIn);

                    FitActivity activity = await _fitActivityService.GetActivity(Id);
                    Participants = activity.Participants.Cast<FitActivityParticipant>().ToList();

                    GoingCount = Participants.Count.ToString();
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void ShowInvitedParticipants ()
        {
            ShowViewModel<DisplayParticipantsViewModel>(new DisplayParticipantsViewModel.Nav { Id = Id });
        }

        void OpenWebSite  ()
        {
            if (string.IsNullOrEmpty(WebsiteUrl)) { 
                Alert("event.summary.no.website.available");
                return; 
            }
            string url = WebsiteUrl;
            if (!url.StartsWith("http://", StringComparison.OrdinalIgnoreCase)) {
                url = "http://" + url;
            }

            var browserTask = Mvx.Resolve<IBrowserTask>();
            browserTask.ShowWebPage(url);
        }

        void OpenFacebook ()
        {
            if (string.IsNullOrEmpty(FacebookUrl)) { 
                Alert("event.summary.no.facebook.available");
                return; 
            }
            string url = FacebookUrl;
            if (!url.StartsWith("https://", StringComparison.OrdinalIgnoreCase)) {
                url = "https://" + url;
            }

            var fbService = Mvx.Resolve<IFacebookService>();
            fbService.OpenPage(url);
        }

        #endregion

        #endregion

        protected override void OnClosing ()
        {
            Messenger.Unsubscribe<EventUpdatedMessage>(_activityUpdatedMessageToken);
            Messenger.Unsubscribe<EventDeletedMessage>(_activityDeletedMessageToken);
        }


    }
}

