﻿using System;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using Humanizer;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;

namespace YuFit.Core.ViewModels.Event
{
    public class EventMessageViewModel : BaseViewModel
    {
        public string           EventId             { get; set; }
        public string           Id                  { get; set; }
        public string           Comment             { get; set; }
        public DateTime         UpdatedAt           { get; set; }
        public string           UserId              { get; set; }
        public string           UserName            { get; set; }
        public Uri              UserAvatar          { get; set; }
        public string           PostedWhen          { get; set; }
        public bool             IAmTheOwner         { get; set; }
        public Colors           Colors              { get; private set; }

        public CommandViewModel DeleteCommand       { get; private set; }

        public EventMessageViewModel (string activityId, FitActivityComment activityComment, Colors colors, bool iAmTheOwner = false) : base("")
        {
            EventId = activityId;
            Id = activityComment.Id;
            Comment = activityComment.Comment;
            UpdatedAt = activityComment.UpdatedAt;
            UserId = activityComment.UserId;
            UserName = activityComment.UserName;
            UserAvatar = activityComment.UserAvatar;
            PostedWhen = UpdatedAt.Humanize();
            IAmTheOwner = iAmTheOwner;
            Colors = colors;

            DeleteCommand = new CommandViewModel(new MvxCommand(DeleteComment));
        }

        async void DeleteComment ()
        {
            Mvx.Trace("DELETE");
            if (await UserDialogs.ConfirmAsync(
                Localized("activity.comments.delete.message"), 
                Localized("activity.comments.delete.title"), 
                Localized("activity.comments.delete.ok.button"),
                Localized("activity.comments.delete.cancel.button")))
            {
                var FitActivityService = Mvx.Resolve<IFitActivityService>();
                try {
                    await ExecuteCancelableWebRequestAsync(async (token, tryCount) => await FitActivityService.DeleteComment(EventId, Id, token));
                } catch (Exception exception) {
                    Mvx.Trace(exception.ToString());
                }
            }
        }
    }
}

