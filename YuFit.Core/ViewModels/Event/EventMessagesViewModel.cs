﻿using System;
using System.Collections.Generic;
using System.Linq;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Models;
using YuFit.Core.Utils;

namespace YuFit.Core.ViewModels.Event
{
    public class EventMessagesViewModel : BaseViewModel
    {
        readonly IFitActivityService _fitActivityService;
        readonly IUserService        _userService;

        public string AddCommentPlaceholder { get; set; }

        protected string EventId { get; private set; }
        protected string EventCategory { get; set; }

        public SilentObservableCollection<EventMessageViewModel> Comments { get; private set; }

        public CommandViewModel SendCommentCommand { get; private set; }
        public Colors           Colors { get; private set; }

        #pragma warning disable 414
        readonly MvxSubscriptionToken _commentDeletedMessageToken;
        #pragma warning restore 414

        public EventMessagesViewModel (IFitActivityService eventService, IUserService userService) : base("activity.comments.title")
        {
            _fitActivityService = eventService;
            _userService = userService;

            AddCommentPlaceholder = Localized("activity.view.details.comment.placeholder");

            Comments = new SilentObservableCollection<EventMessageViewModel>();
            SendCommentCommand = new CommandViewModel(new MvxCommand<string>(SendComment));
            _commentDeletedMessageToken = Messenger.Subscribe<EventCommentDeletedMessage>(OnCommentDeleted);
        }

        public class Navigation { 
            public string EventId { get; set; } 
            public string EventCategory { get; set; }
        }

        public virtual async void Init(Navigation navParams)
        {
            EventId = navParams.EventId;
            EventCategory = navParams.EventCategory;

            var categories = _fitActivityService.GetEventCategories();
            var category = categories.FirstOrDefault(c => c.Name == EventCategory);
            Colors = category?.Colors ?? new Colors { 
                Base = new Color { Red=255,Blue=255,Green=22 },
                Dark = new Color { Red=255,Blue=255,Green=255 },
                Light = new Color { Red=255,Blue=255,Green=222 }
            };

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var activityComments = await _fitActivityService.GetActivityComments(EventId, token);

                    var tmp = new List<EventMessageViewModel>();
                    activityComments.ForEach(comment => Mvx.Trace (comment.Comment));
                    activityComments.ForEach(comment => tmp.Add(new EventMessageViewModel(EventId, comment, Colors, _userService.IsThisMe(comment.UserId))));

                    var orderedList = tmp.OrderBy(x => x.UpdatedAt).ToList();
                    Comments.AddMultiple(orderedList);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        protected override void OnClosing ()
        {
            var messenger = Mvx.Resolve<IMvxMessenger>();
            messenger.Unsubscribe<EventCommentAddedMessage>(_commentDeletedMessageToken);

            base.OnClosing ();
        }

        async void SendComment(string comment)
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var addedComment = await _fitActivityService.AddComment(EventId, comment, token);
                    var vm = new EventMessageViewModel(EventId, addedComment, Colors, true);
                    Comments.Add(vm);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void OnCommentDeleted (EventCommentDeletedMessage obj)
        {
            var deletedComment = Comments.FirstOrDefault(c => c.Id == obj.CommentId);
            if (deletedComment != null) { Comments.Remove(deletedComment); }
        }
    }}

