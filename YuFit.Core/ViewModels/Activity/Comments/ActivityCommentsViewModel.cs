﻿using System;
using System.Collections.Generic;
using System.Linq;

using MvvmCross.Platform;

using YuFit.Core.Extensions;
using YuFit.Core.Utils;

namespace YuFit.Core.ViewModels.Activity.Comments
{
    public class ActivityCommentsViewModel : BaseFitActivityViewModel
    {
        public SilentObservableCollection<ActivityCommentViewModel> Comments { get; private set; }

        public ActivityCommentsViewModel () : base("activity.comments.title")
        {
            Comments = new SilentObservableCollection<ActivityCommentViewModel>();
        }

        protected async override void LoadViewModel ()
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var activityComments = await FitActivityService.GetActivityComments(ActivityId, token);

                    var tmp = new List<ActivityCommentViewModel>();
                    activityComments.ForEach(comment => Mvx.Trace (comment.Comment));
                    activityComments.ForEach(comment => tmp.Add(new ActivityCommentViewModel(ActivityId, comment, ActivityTypeName)));

                    var orderedList = tmp.OrderBy(x => x.UpdatedAt).ToList();
                    Comments.AddMultiple(orderedList);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
            base.LoadViewModel();
        }
    }
}

