﻿using System;
using Humanizer;
using YuFit.Core.Models;
using MvvmCross.Platform;

using YuFit.Core.Interfaces.Services;
using MvvmCross.Core.ViewModels;

namespace YuFit.Core.ViewModels.Activity.Comments
{
    public class ActivityCommentViewModel : BaseViewModel
    {
        public string           ActivityTypeName    { get; set; }
        public string           ActivityId          { get; set; }
        public string           Id                  { get; set; }
        public string           Comment             { get; set; }
        public DateTime         UpdatedAt           { get; set; }
        public string           UserId              { get; set; }
        public string           UserName            { get; set; }
        public Uri              UserAvatar          { get; set; }
        public string           PostedWhen          { get; set; }
        public bool             IAmTheOwner         { get; set; }

        public CommandViewModel DeleteCommand       { get; private set; }

        public ActivityCommentViewModel (string activityId, FitActivityComment activityComment, string activityTypeName, bool iAmTheOwner = false)
        {
            ActivityId = activityId;
            ActivityTypeName = activityTypeName;
            Id = activityComment.Id;
            Comment = activityComment.Comment;
            UpdatedAt = activityComment.UpdatedAt;
            UserId = activityComment.UserId;
            UserName = activityComment.UserName;
            UserAvatar = activityComment.UserAvatar;
            PostedWhen = UpdatedAt.Humanize();
            IAmTheOwner = iAmTheOwner;

            DeleteCommand = new CommandViewModel(new MvxCommand(DeleteComment));
        }

        public ActivityCommentViewModel (string activityTypeName, string comment, string userName)
        {
            ActivityTypeName = activityTypeName;
            Comment = comment;
            UserName = userName;
            PostedWhen = string.Empty;
            IAmTheOwner = false;
        }

        async void DeleteComment ()
        {
            Mvx.Trace("DELETE");
            if (await UserDialogs.ConfirmAsync(
                Localized("activity.comments.delete.message"), 
                Localized("activity.comments.delete.title"), 
                Localized("activity.comments.delete.ok.button"),
                Localized("activity.comments.delete.cancel.button")))
            {
                var FitActivityService = Mvx.Resolve<IFitActivityService>();
                try {
                    await ExecuteCancelableWebRequestAsync(async (token, tryCount) => await FitActivityService.DeleteComment(ActivityId, Id, token));
                } catch (Exception exception) {
                    Mvx.Trace(exception.ToString());
                }
            }
        }
    }
}

