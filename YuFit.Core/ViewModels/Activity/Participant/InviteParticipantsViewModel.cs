﻿using System.Collections.Generic;
using System.Linq;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using Coc.MvvmCross.Plugins.Facebook;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.Utils;

using YuFit.WebServices.Interface.Model;
using System;

namespace YuFit.Core.ViewModels.Activity.Participant
{
    public class InviteParticipantViewModel : UserInfoViewModel
    {
        public string FavSport          { get; set; }
        public bool   IsSelected        { get; set; }
        public bool   IsGoing           { get; set; }
        public bool   AlreadyInvited    { get; set; }

        public InviteParticipantViewModel (User friend) : base(friend)
        {
            Id = friend.Id;
            Name = friend.Name;
            var homelocation = ((User) friend).GetHomeLocation();
            if (homelocation != null) {
                Location = homelocation.CityStateString();
            }

            if (friend.UserProfile != null && friend.UserProfile.AvatarUri != null) {
                AvatarUrl = friend.UserProfile.AvatarUri.ToString();
            }

            if (string.IsNullOrEmpty(Location)) {
                Location = Localized("user.info.location.not.provided.text");
            }

            FavSport = "Cycling";
            Mvx.Trace("Creating userinfovm for {0}", Name);
        }

        #if DEBUG_LEAKS
        ~InviteParticipantViewModel ()
        {
            Mvx.Trace("deleting userinfovm for {0}", Name);
        }
        #endif
    }

    public class InviteParticipantsViewModel : BaseViewModel
    {
        #region Data Members
        readonly IUserService _userService;
        string _activityId;

        #endregion

        #region Properties

        public ActivityType ActivityType { get; set; }

        public string TableLabelTitle { get; set; }
        public string SearchPlaceholderText { get; set; }
        public string SearchText { get; set; }
        public string InviteText { get; set; }

        public SilentObservableCollection<InviteParticipantViewModel> YuFitFriends { get; private set; }

        public CommandViewModel ParticipantSelectedCommand { get; private set; }
        public CommandViewModel InviteMoreFriendsCommand { get; private set; }

        #endregion

        #region Construction / Destruction

        public InviteParticipantsViewModel(
            IUserService userService
        ) : base("activity.invite.participants.title")
        {
            _userService = userService;

            YuFitFriends = new SilentObservableCollection<InviteParticipantViewModel>();
            InviteText = Localized("activity.invite.participants.invite.text");

            ParticipantSelectedCommand = new CommandViewModel("", new MvxCommand<InviteParticipantViewModel>(ParticipantSelected));
            InviteMoreFriendsCommand = new CommandViewModel(new MvxCommand(InviteFriends));
        }

        #endregion

        #region Methods

        public class Navigation { public string ActivityId { get; set; } }
        public virtual void Init(Navigation navParams)
        {
            _activityId = navParams.ActivityId;
            FetchFriendsList();
        }

        protected override void OnClosing ()
        {
            base.OnClosing();
            Save();
        }

        protected async void Save ()
        {
            List<IFitEventParticipant> selectedParticipants = new List<IFitEventParticipant>();

            var selectedParticipantsVM = YuFitFriends.Where(participant => participant.IsSelected);
            selectedParticipantsVM.ForEach(participant => selectedParticipants.Add(new FitActivityParticipant {
                UserId = participant.Id,
                Invited = true
            }));

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var activity = await Mvx.Resolve<IFitActivityService>().GetActivity(_activityId, token);
                    activity.Participants = selectedParticipants;
                    await Mvx.Resolve<IFitActivityService>().UpdateActivity(activity, token);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        #endregion

        void ParticipantSelected (InviteParticipantViewModel participant)
        {
            participant.IsSelected = !participant.IsSelected;
        }

        async void FetchFriendsList ()
        {
            ProgressMessage = Localized("activity.invite.participants.fetch.progress.message");
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var friends = await _userService.GetMyFriends(token);
                    if (friends != null) {
                        var tmp = new List<InviteParticipantViewModel>();
                        friends.ForEach(friend => tmp.Add(new InviteParticipantViewModel((User) friend)));

                        var orderedList = tmp.OrderBy(x => x.Name).ToList();

                        YuFitFriends.Clear();
                        YuFitFriends.AddMultiple(orderedList);
                    }

                    var activity = await Mvx.Resolve<IFitActivityService>().GetActivity(_activityId);
                    var selectedParticipants = activity.Participants;

                    foreach (var sp in selectedParticipants) {
                        var matches = YuFitFriends.Where(p => p.Id == sp.UserId);
                        matches.ForEach(m => {
                            m.IsSelected = true;
                            m.IsGoing = sp.Going.HasValue && sp.Going.Value;
                            m.AlreadyInvited = true;
                        });
                    }

                    ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(fitActivity => fitActivity.TypeName == activity.Sport);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        IFacebookFriendInvitor invitor;
        void InviteFriends()
        {
            var fbService = Mvx.Resolve<IFacebookService>();
            invitor = fbService.GetInvitor();
            invitor.OperationComplete = (canceled, error) => {
                invitor.OperationComplete = null;
                invitor = null;
            };
            invitor.InviteFacebookFriends();

        }
    }}

