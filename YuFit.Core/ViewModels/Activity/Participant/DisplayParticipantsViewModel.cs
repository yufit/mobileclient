﻿using System;
using System.Collections.Generic;
using System.Linq;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Profiles;

using YuFit.WebServices.Exceptions;

namespace YuFit.Core.ViewModels.Activity.Participant
{
    public class ParticipantViewModel : UserInfoViewModel
    {
        public bool IsGoing { get; set; }

        public ParticipantViewModel (FitActivityParticipant participant, string sport) : base(participant)
        {
            Id = participant.UserId;
            IsGoing = participant.IsGoing;
            Sport = sport;

        }
    }

    public class DisplayParticipantsViewModel : BaseViewModel
    {
        readonly IFitActivityService _fitActivityService;
        string _activityId;

        public string Sport { get; set; }
        public bool CanInvite { get; set; }

        public CommandViewModel ShowUserProfileCommand { get; private set; }
        public CommandViewModel InviteFriendsCommand { get; private set; }

        public SilentObservableCollection<ParticipantViewModel> Participants { get; private set; }

        public DisplayParticipantsViewModel (IFitActivityService fitActivityService) : base("activity.participants.title")
        {
            _fitActivityService = fitActivityService;
            ShowUserProfileCommand = new CommandViewModel(new MvxCommand<ParticipantViewModel>(ShowUserProfile));
            InviteFriendsCommand = new CommandViewModel(new MvxCommand(InviteFriends));
            Participants = new SilentObservableCollection<ParticipantViewModel>();
        }

        public class Nav { public string Id { get; set; } }
        public async void Init(Nav navigationData)
        {
            _activityId = navigationData.Id;

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    // Get the info from the server api (which may cache thing)
                    FitActivity activity = await _fitActivityService.GetActivity(_activityId, token);

                    var isPrivate = activity.IsPrivate.HasValue && activity.IsPrivate.Value;
                    var isCreator = Mvx.Resolve<IUserService>().AmITheCreatorOfThisActivity(activity);
                    var isNotAGroupActivity = string.IsNullOrEmpty(activity.UserGroupId);
                    CanInvite = (!isPrivate || isCreator) && isNotAGroupActivity;

                    if (activity.Participants != null) {
                        var tmp = new List<ParticipantViewModel>();

                        var participantCast = activity.Participants.Cast<FitActivityParticipant>().ToList();
                        participantCast.Where(p => (p.IsGoing || p.HasNotAnswered)).ForEach(p => tmp.Add(new ParticipantViewModel(p, activity.Sport)));

                        var orderedList = tmp.OrderBy(x => !x.IsGoing).ThenBy(x => x.Name).ToList();

                        Participants.Clear();
                        Participants.AddMultiple(orderedList);
                    }
                    Sport = activity.Sport;
                });
            } catch (NotFoundException /*exception*/) {
                Alert ("activity.unavailable", null, "Error");
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void ShowUserProfile (ParticipantViewModel entry)
        {
            ShowViewModel<FriendProfileViewModel>(new FriendProfileViewModel.Navigation {UserId = entry.Id});
        }

        void InviteFriends ()
        {
            Mvx.Trace("Inviting new friends");
            ShowViewModel<InviteParticipantsViewModel>(new InviteParticipantsViewModel.Navigation {ActivityId = _activityId});
        }

    }
}

