﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MvvmCross.Platform;

using Coc.MvvmCross.Plugins.Location;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Models;
using YuFit.Core.ViewModels.Activity.Details;
using YuFit.Core.ViewModels.Activity.Participant;
using YuFit.Core.ViewModels.CreateActivity;
using YuFit.Core.ViewModels.Dashboard.ActivitySummary;

using YuFit.WebServices.Exceptions;
using MvvmCross.Plugins.Messenger;
using MvvmCross.Core.ViewModels;

namespace YuFit.Core.ViewModels.Activity.Summary
{
    public class ActivitySummaryViewModel : BaseViewModel
    {
        #region Data Models

        readonly IFitActivityService _fitActivityService;
        readonly IGroupService _groupService;
        readonly IUserService _userService;

        #pragma warning disable 414
        readonly MvxSubscriptionToken _activityUpdatedMessageToken;
        readonly MvxSubscriptionToken _activityDeletedMessageToken;
        #pragma warning restore 414

        #endregion

        public string Id { get; set; }
        public CocLocation Location { get; set; }

        #region Properties

        #region Notifiable properties

        public bool IsLoaded { get; set; }
        public bool IsCreator { get; set; }
        public ActivityType ActivityType { get; set; }
        public List<FitActivityParticipant> Participants { get; set; }
        public BaseActivityDetailsViewModelRaw ActivityViewModel { get; set; }
        public DateTime StartTime { get; set; }
        public bool IamIn { get; set; }
        public string SportSubType { get; set; }
        public string SportSubTypeRaw { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsExpired { get; set; }
        public string CancelString { get; set; }
        public bool IsGroupActivity { get; set; }
        public Uri GroupImage { get; set; }
        public string GroupName { get; set; }

        #endregion

        #region Localized String properties

        public string JoinTitle { get; private set; }
        public string IAmInTitle { get; private set; }
        public string InvitedTitle { get; private set; }
        public string IamInString { get { return IamIn ? IAmInTitle : JoinTitle; } }
        public string ExpiredString { get; set; }

        #endregion

        #region Commands VM

        public CommandViewModel DismissCommand { get; private set; }
        public CommandViewModel JoinCommand { get; private set; }
        public CommandViewModel ShowInvitedParticipantsCommand { get; private set; }
        public CommandViewModel ShowActivityDetailCommand { get; private set; }
        public CommandViewModel EditCommand { get; private set; }

        #endregion

        #endregion

        #region Construction/Initialization
        public ActivitySummaryViewModel (
            IUserService userService, 
            IGroupService groupService, 
            IFitActivityService fitActivityService)
        {
            _fitActivityService = fitActivityService;
            _userService = userService;
            _groupService = groupService;
            ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(a => a.TypeName == "Cycling");
            Participants = new List<FitActivityParticipant>();

            JoinTitle = Localized("activity.summary.join.label");
            IAmInTitle = Localized("activity.summary.im.in.label");
            InvitedTitle = Localized("activity.summary.invited.label");
            CancelString = Localized("activity.summary.canceled.label");
            ExpiredString = Localized("activity.summary.expired.label");

            DismissCommand = new CommandViewModel(new MvxCommand(Dismiss));
            JoinCommand = new CommandViewModel(new MvxCommand(Join));
            ShowInvitedParticipantsCommand = new CommandViewModel(new MvxCommand(ShowInvitedParticipants));
            ShowActivityDetailCommand = new CommandViewModel(new MvxCommand(ShowActivityDetail));
            EditCommand = new CommandViewModel(new MvxCommand(Edit));

            _activityUpdatedMessageToken = Messenger.Subscribe<EventUpdatedMessage>(OnActivityUpdated);
            _activityDeletedMessageToken = Messenger.Subscribe<EventDeletedMessage>(OnActivityDeleted);
        }

        public class Nav { public string Id { get; set; } }
        public async void Init(Nav navigationData)
        {
            Id = navigationData.Id;
            string meId = _userService.LoggedInUser.Id;

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    // Get the info from the server api (which may cache thing)
                    FitActivity activity = await _fitActivityService.GetActivity(Id, token);
                    if (activity != null) {

                        SportSubTypeRaw = activity.SubSport;
                        Location = new CocLocation { Coordinates = new CocCoordinates {Latitude = ((Location) activity.Location).GeoCoordinates.Latitude, Longitude = ((Location) activity.Location).GeoCoordinates.Longitude } };
                        Participants = activity.Participants.Cast<FitActivityParticipant>().ToList();
                        StartTime = ((EventSchedule) activity.EventSchedule).LocalStartTime;
                        IsCreator = Mvx.Resolve<IUserService>().AmITheCreatorOfThisActivity(activity);
                        IsCanceled = activity.IsCanceled.HasValue && activity.IsCanceled.Value;
                        IsExpired = activity.EventSchedule.StartTime < DateTime.UtcNow;
                        IsGroupActivity = !string.IsNullOrEmpty(activity.UserGroupId);
                        SportSubType = LocalizedSubsport(activity);

                        var me = Participants.FirstOrDefault(p => p.UserId == meId);
                        if (me != null) {
                            IamIn = me.Going.HasValue && me.Going == true;
                        } else {
                            IamIn = false;
                        }

                        ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(a => a.TypeName == activity.Sport);
                        UpdateActivityViewModel(activity);

                        if (IsGroupActivity) {
                            var groupInfo = await _groupService.GetGroup(activity.UserGroupId);
                            GroupImage = groupInfo.GroupPhoto;
                            GroupName = groupInfo.Name;
                        }

                        Mvx.Trace(activity.ToString());
                    } else {
                        Mvx.Trace(string.Format("Error: got a bad event id: {0}", Id));
                    }

                    IsLoaded = true;
                });
            } catch (NotFoundException /*exception*/) {
                Alert ("activity.unavailable", null, "Error");
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }
        #endregion


        void OnActivityUpdated (EventUpdatedMessage obj)
        {
            if (Id != obj.Activity.Id) { return; }

            var activity = obj.Activity;
            SportSubTypeRaw = activity.SubSport;
            Location = new CocLocation { Coordinates = new CocCoordinates {Latitude = ((Location) activity.Location).GeoCoordinates.Latitude, Longitude = ((Location) activity.Location).GeoCoordinates.Longitude } };
            Participants = activity.Participants.Cast<FitActivityParticipant>().ToList();
            StartTime = ((EventSchedule) activity.EventSchedule).LocalStartTime;
            IsCreator = Mvx.Resolve<IUserService>().AmITheCreatorOfThisActivity(activity);
            SportSubType = LocalizedSubsport(activity);

            ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(a => a.TypeName == activity.Sport);
            UpdateActivityViewModel(activity);
        }

        void OnActivityDeleted (EventDeletedMessage obj)
        {
            Task.Factory.StartNew(() => InvokeOnMainThread(async () => {
                await Task.Delay(200);
                Close(this);
            }));
        }


        #region Methods

        #region Command handlers

        void Dismiss ()
        {
            Close(this);
        }

        async void Join ()
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    IamIn = !IamIn;
                    await _fitActivityService.SetGoing(Id, IamIn);

                    FitActivity activity = await _fitActivityService.GetActivity(Id);
                    Participants = activity.Participants.Cast<FitActivityParticipant>().ToList();
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void ShowInvitedParticipants ()
        {
            ShowViewModel<DisplayParticipantsViewModel>(new DisplayParticipantsViewModel.Nav { Id = Id });
        }

        void ShowActivityDetail ()
        {
            ShowViewModel<ViewFitActivityDetailsViewModel>(new ViewFitActivityDetailsViewModel.Navigation { ActivityId = Id });
        }

        void Edit () 
        {
            ShowViewModel<EditFitActivityViewModel>(new BaseFitActivityViewModel.Navigation { ActivityId = Id });
        }

        #endregion

        void UpdateActivityViewModel (FitActivity activity)
        {
            const string ActivityTypeVMFormat = "YuFit.Core.ViewModels.Dashboard.ActivitySummary.{0}ActivityDetailsViewModel";//{0}ActivityDetailsViewModel";
            string className = string.Format(ActivityTypeVMFormat, ActivityType.TypeName);
            Type activityViewModelClassType = Type.GetType(className);

            if (activityViewModelClassType != null)
            {
                ActivityViewModel = (BaseActivityDetailsViewModelRaw) Activator.CreateInstance(activityViewModelClassType, activity.ActivitySettings);
            }
        }

        #endregion

        protected override void OnClosing ()
        {
            Messenger.Unsubscribe<EventUpdatedMessage>(_activityUpdatedMessageToken);
            Messenger.Unsubscribe<EventDeletedMessage>(_activityDeletedMessageToken);
        }


    }
}

