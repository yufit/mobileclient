﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using Coc.MvvmCross.Plugins.Location;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Models;
using YuFit.Core.ViewModels.Activity.Details;
using YuFit.Core.ViewModels.Activity.Participant;
using YuFit.Core.ViewModels.CreateActivity;
using YuFit.Core.ViewModels.Dashboard.ActivitySummary;

using YuFit.WebServices.Exceptions;
using YuFit.Core.ViewModels.MapAnnotations;

namespace YuFit.Core.ViewModels.Activity.Summary
{
    public class ActivitySummary2ViewModel : BaseViewModel
    {
        #region Data Models

        public string Id { get; set; }

        readonly IFitActivityService    _fitActivityService;
        readonly IUserService           _userService;
        readonly IGroupService          _groupService;

        #pragma warning disable 414
        readonly MvxSubscriptionToken   _activityUpdatedMessageToken;
        readonly MvxSubscriptionToken   _activityDeletedMessageToken;
        #pragma warning restore 414

        #endregion

        #region Properties

        FitActivityMapAnnotationViewModel _pinVm;
        public MapAnnotationObservableCollection Annotations { get; private set; }

        #region Notifiable properties

        public bool             IsLoaded        { get; set; }
        public bool             IsCreator       { get; set; }
        public ActivityType     ActivityType    { get; set; }
        public DateTime         StartTime       { get; set; }
        public bool             IamIn           { get; set; }
        public string           SportSubType    { get; set; }
        public string           SportSubTypeRaw { get; set; }
        public bool             IsCanceled      { get; set; }
        public bool             IsExpired       { get; set; }
        public CocLocation      Location        { get; set; }
        public Region           CurrentRegion   { get; set; }
        public bool             IsGroupActivity { get; set; }
        public Uri              GroupImage      { get; set; }
        public string           GroupName       { get; set; }

        public BaseActivityDetailsViewModelRaw  ActivityViewModel   { get; set; }
        public List<FitActivityParticipant>     Participants        { get; set; }

        #endregion

        #region Localized String properties

        public string JoinTitle     { get; private set; }
        public string IAmInTitle    { get; private set; }
        public string InvitedTitle  { get; private set; }
        public string IamInString   { get { return IamIn ? IAmInTitle : JoinTitle; } }
        public string CancelString  { get; private set; }
        public string ExpiredString { get; private set; }

        #endregion

        #region Commands VM

        public CommandViewModel JoinCommand                     { get; private set; }
        public CommandViewModel ShowInvitedParticipantsCommand  { get; private set; }
        public CommandViewModel ShowActivityDetailCommand       { get; private set; }
        public CommandViewModel EditCommand                     { get; private set; }

        #endregion

        #endregion

        #region Construction/Initialization
        public ActivitySummary2ViewModel (
            IUserService        userService, 
            IGroupService       groupService,
            IFitActivityService fitActivityService
        ) : base()
        {
            _fitActivityService = fitActivityService;
            _userService        = userService;
            _groupService       = groupService;

            ActivityType        = _fitActivityService.AvailableActivities.FirstOrDefault(a => a.TypeName == "Cycling");
            Participants        = new List<FitActivityParticipant>();

            LoadStrings();
            CreateCommands();

            _activityUpdatedMessageToken    = Messenger.Subscribe<EventUpdatedMessage>(OnActivityUpdated);
            _activityDeletedMessageToken    = Messenger.Subscribe<EventDeletedMessage>(OnActivityDeleted);

            Annotations = new MapAnnotationObservableCollection();
        }

        void LoadStrings()
        {
            JoinTitle    = Localized("activity.summary.join.label");
            IAmInTitle   = Localized("activity.summary.im.in.label");
            InvitedTitle = Localized("activity.summary.invited.label");
            CancelString = Localized("activity.summary.canceled.label");
            ExpiredString = Localized("activity.summary.expired.label");
        }

        void CreateCommands()
        {
            JoinCommand                     = new CommandViewModel(new MvxCommand(Join));
            ShowInvitedParticipantsCommand  = new CommandViewModel(new MvxCommand(ShowInvitedParticipants));
            ShowActivityDetailCommand       = new CommandViewModel(new MvxCommand(ShowActivityDetail));
            EditCommand                     = new CommandViewModel(new MvxCommand(Edit));
        }

        public class Nav { public string Id { get; set; } }
        public async void Init(Nav navigationData)
        {
            Id = navigationData.Id;

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    FitActivity activity = await _fitActivityService.GetActivity(Id, token);
                    PopulateVM(activity);
                    IsLoaded = true;
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        protected override void OnClosing ()
        {
            Messenger.Unsubscribe<EventUpdatedMessage>(_activityUpdatedMessageToken);
            Messenger.Unsubscribe<EventDeletedMessage>(_activityDeletedMessageToken);
        }
        #endregion

        #region Methods

        #region Message handlers

        void OnActivityUpdated (EventUpdatedMessage obj) { PopulateVM(obj.Activity); }
        void OnActivityDeleted (EventDeletedMessage obj) { IsCanceled = true; }

        #endregion

        #region Command handlers

        async void Join ()
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    IamIn = !IamIn;
                    await _fitActivityService.SetGoing(Id, IamIn);

                    FitActivity activity = await _fitActivityService.GetActivity(Id, token);
                    Participants = activity.Participants.Cast<FitActivityParticipant>().ToList();
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void ShowInvitedParticipants () { ShowViewModel<DisplayParticipantsViewModel>(new DisplayParticipantsViewModel.Nav { Id = Id }); }
        void ShowActivityDetail      () { ShowViewModel<ViewFitActivityDetailsViewModel>(new ViewFitActivityDetailsViewModel.Navigation { ActivityId = Id }); }
        void Edit                    () { ShowViewModel<EditFitActivityViewModel>(new BaseFitActivityViewModel.Navigation { ActivityId = Id }); }

        #endregion

        #region Helpers

        async void PopulateVM(FitActivity activity)
        {
            if (Id != activity.Id) { return; }

            string meId = _userService.LoggedInUser.Id;

            ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(a => a.TypeName == activity.Sport);
            UpdateActivityViewModel(activity);

            SportSubTypeRaw = activity.SubSport;
            Location        = new CocLocation { Coordinates = new CocCoordinates {Latitude = ((Location) activity.Location).GeoCoordinates.Latitude, Longitude = ((Location) activity.Location).GeoCoordinates.Longitude } };
            Participants    = activity.Participants.Cast<FitActivityParticipant>().ToList();
            StartTime       = ((EventSchedule) activity.EventSchedule).LocalStartTime;
            IsCreator       = Mvx.Resolve<IUserService>().AmITheCreatorOfThisActivity(activity);
            IsCanceled      = activity.IsCanceled.HasValue && activity.IsCanceled.Value;
            IsExpired = activity.EventSchedule.StartTime < DateTime.UtcNow;
            IsGroupActivity = !string.IsNullOrEmpty(activity.UserGroupId);
            SportSubType    = LocalizedSubsport(activity);

            var me = Participants.FirstOrDefault(p => p.UserId == meId);
            if (me != null) {
                IamIn = me.Going.HasValue && me.Going == true;
            } else {
                IamIn = false;
            }

            CurrentRegion = new Region {
                Center = new Coordinate(Location.Coordinates.Latitude, Location.Coordinates.Longitude),
                Radius = 10
            };

            if (IsGroupActivity) {
                try {
                    await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                        var groupInfo = await _groupService.GetGroup(activity.UserGroupId, token);
                        GroupImage = groupInfo.GroupPhoto;
                        GroupName = groupInfo.Name;
                    });
                } catch (Exception exception) {
                    Mvx.Trace(exception.ToString());
                }
            }

            var location = new Coordinate {Latitude = ((Location) activity.Location).GeoCoordinates.Latitude, Longitude = ((Location) activity.Location).GeoCoordinates.Longitude };

            if (IsGroupActivity) {
                _pinVm = new GroupFitActivityMapAnnotationViewModel
                {
                    Id = Id,
                    DisplayName = "",
                    Location = location,
                    ActivityType = ActivityType,
                    Invited = false,
                    AvatarUrl = activity.UserGroupAvatarUrl
                };
            } else {
                _pinVm = new FitActivityMapAnnotationViewModel
                {
                    Id = Id,
                    DisplayName = "",
                    Location = location,
                    ActivityType = ActivityType,
                    Invited = false
                };
            }

            Annotations.Add(_pinVm);
        }

        void UpdateActivityViewModel (FitActivity activity)
        {
            const string ActivityTypeVMFormat = "YuFit.Core.ViewModels.Dashboard.ActivitySummary.{0}ActivityDetailsViewModel";//{0}ActivityDetailsViewModel";
            string className = string.Format(ActivityTypeVMFormat, ActivityType.TypeName);
            Type activityViewModelClassType = Type.GetType(className);

            if (activityViewModelClassType != null)
            {
                ActivityViewModel = (BaseActivityDetailsViewModelRaw) Activator.CreateInstance(activityViewModelClassType, activity.ActivitySettings);
            }
        }
        #endregion

        #endregion

    }
}

