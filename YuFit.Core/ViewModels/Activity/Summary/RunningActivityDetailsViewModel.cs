﻿using YuFit.Core.Models.Activities;
using System.Collections.Generic;
using System;

namespace YuFit.Core.ViewModels.Dashboard.ActivitySummary
{
    public class RunningActivityDetailsViewModel : BaseActivityDetailsViewModel<RunningActivitySettings>
    {
        private const string localPrefixKey = "activity.Running.";

        public int Distance { get; set; }
        public int Duration { get; set; }

        public string Speed { 
            get {
                int minutes = (int) ActivitySettings.Speed;
                int seconds = (int) ((ActivitySettings.Speed - minutes) * 60);

                return string.Format("{0:00}:{1:00}", minutes, seconds); 
            } 
        }

        public string SpeedUnit { get; private set; }

        public List<Tuple<string, object>> DurationValues { get { return YuFit.Core.Models.Activities.DurationValues.Running; } }
        public List<Tuple<string, object>> DistanceValues { get { return YuFit.Core.Models.Activities.DistanceValues.Running; } }

        public RunningActivityDetailsViewModel (RunningActivitySettings settings) : base("", settings)
        {
            UpdateValues();
        }

        public override void Update ()
        {
            UpdateValues();
            base.Update();
        }

        void UpdateValues()
        {

            Distance = ActivitySettings.Distance;
            Duration = ActivitySettings.Duration;

            // TODO This needs to be using the system local to use mph or km
            SpeedUnit = "MIN/KM";
        }
    }
}

