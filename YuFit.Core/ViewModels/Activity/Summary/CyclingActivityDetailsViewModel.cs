﻿using YuFit.Core.Models.Activities;
using System.Collections.Generic;
using System;

namespace YuFit.Core.ViewModels.Dashboard.ActivitySummary
{
    public class CyclingActivityDetailsViewModel : BaseActivityDetailsViewModel<CyclingActivitySettings>
    {
        private const string localPrefixKey = "activity.Cycling.";

        public int Distance { get; set; } 
        public int Duration { get; set; }

        public List<Tuple<string, object>> DurationValues { get { return YuFit.Core.Models.Activities.DurationValues.Cycling; } }
        public List<Tuple<string, object>> DistanceValues { get { return YuFit.Core.Models.Activities.DistanceValues.Cycling; } }

        public string Speed { get { return string.Format("{0:00.00}", ActivitySettings.Speed); } }
        public string SpeedUnit         { get; private set; }

        public CyclingActivityDetailsViewModel (CyclingActivitySettings settings) : base("", settings)
        {
            UpdateValues();
        }

        public override void Update ()
        {
            UpdateValues();
            base.Update();
        }

        void UpdateValues()
        {
            Distance = ActivitySettings.Distance;
            Duration = ActivitySettings.Duration;

            // TODO This needs to be using the system local to use mph or km
            SpeedUnit = "KM/H";
        }
    }
}

