﻿using YuFit.Core.Models.Activities;
using System.Collections.Generic;
using System;

namespace YuFit.Core.ViewModels.Dashboard.ActivitySummary
{
    public class SwimmingActivityDetailsViewModel : BaseActivityDetailsViewModel<SwimmingActivitySettings>
    {
        private const string localPrefixKey = "activity.Swimming.";

        public int Distance { get; set; }
        public int Duration { get; set; }
        public SwimmingType SwimmingType { get; set; }

        public List<Tuple<string, object>> DurationValues { get { return YuFit.Core.Models.Activities.DurationValues.Swimming; } }
        public List<Tuple<string, object>> DistanceValues { get { return YuFit.Core.Models.Activities.DistanceValues.Swimming; } }


        public SwimmingActivityDetailsViewModel (SwimmingActivitySettings settings) : base("", settings)
        {
            UpdateValues();
        }

        public override void Update ()
        {
            UpdateValues();
            base.Update();
        }

        void UpdateValues()
        {
            Distance = ActivitySettings.Distance;
            Duration = ActivitySettings.Duration;
            SwimmingType = ActivitySettings.SwimmingType;
        }
    }
}

