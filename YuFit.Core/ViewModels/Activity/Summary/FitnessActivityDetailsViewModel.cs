﻿using YuFit.Core.Models.Activities;
using System.Collections.Generic;
using System;

namespace YuFit.Core.ViewModels.Dashboard.ActivitySummary
{
    public class FitnessActivityDetailsViewModel : BaseActivityDetailsViewModel<FitnessActivitySettings>
    {
        private const string localPrefixKey = "activity.Yoga.";

        public int Duration { get; set; }

        public List<Tuple<string, object>> DurationValues { get { return YuFit.Core.Models.Activities.DurationValues.Yoga; } }

        public FitnessActivityDetailsViewModel (FitnessActivitySettings settings) : base("", settings)
        {
            UpdateValues();
        }

        public override void Update ()
        {
            UpdateValues();
            base.Update();
        }

        void UpdateValues()
        {
            Duration = ActivitySettings.Duration;
        }
    }
}

