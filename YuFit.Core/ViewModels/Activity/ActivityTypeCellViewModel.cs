using PropertyChanged;


namespace YuFit.Core.ViewModels.Activity
{
    public class ActivityTypeCellViewModel : BaseViewModel
    {
        [DoNotCheckEqualityAttribute] public string TypeName   { get; set; }
        [DoNotCheckEqualityAttribute] public bool   IsSelected { get; set; }
        [DoNotCheckEqualityAttribute] public bool   IsEnabled  { get; set; }
    }
    
}
