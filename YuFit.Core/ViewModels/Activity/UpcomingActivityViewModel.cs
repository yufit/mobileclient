﻿using System;
using YuFit.Core.Models;

namespace YuFit.Core.ViewModels.Activity
{
    public class UpcomingActivityViewModel : BaseViewModel
    {
        public string   Id          { get; set; }
        public DateTime When        { get; set; }
        public string   Sport       { get; set; }
        public string   Subsport    { get; set; }
        public string   Location    { get; set; }
        public bool     IsCanceled  { get; set; }
        public bool     IsPromoted  { get; set; }

        public UpcomingActivityViewModel (FitActivity activity)
        {
            Id          = activity.Id;
            When        = ((EventSchedule) activity.EventSchedule).LocalStartTime;
            Sport       = activity.Sport;
            Subsport    = activity.SubSport;
            Location    = ((Location) activity.Location).CityStateString();
            IsCanceled  = activity.IsCanceled.HasValue && activity.IsCanceled.Value;
            IsPromoted  = activity.IsAPromotedEvent;
        }
    }
}

