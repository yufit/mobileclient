﻿using YuFit.Core.Models.Activities;

namespace YuFit.Core.ViewModels.DisplayActivities
{
    public class YogaActivityDisplayViewModel : BaseActivityDisplayViewModel<YogaActivitySettings>
    {
        private const string localPrefixKey = "activity.Yoga.";

        public string Distance { get; set; }
        public string Duration { get; set; }
        public string Intensity { get; set; }

        public YogaActivityDisplayViewModel (YogaActivitySettings settings) : base("", settings)
        {
            UpdateValues();
        }

        public override void Update ()
        {
            UpdateValues();
            base.Update();
        }

        void UpdateValues()
        {
            string durationFormat = Localized(localPrefixKey + "duration.format");
            Duration = string.Format(durationFormat, ActivitySettings.Duration / 60 , ActivitySettings.Duration % 60);
            Intensity = Localized(string.Format("activity.intensity.value.{0}", 
                ActivitySettings.IntensityLevel.ToString().ToLower()));
        }
    }
}

