﻿using YuFit.Core.Models.Activities;

namespace YuFit.Core.ViewModels.DisplayActivities
{
    public class BaseActivityDisplayViewModelRaw : BaseFitActivityViewModel {
        public BaseActivityDisplayViewModelRaw(string displayName) : base(displayName) {}
        public virtual void Update() {}
    }

    public class BaseActivityDisplayViewModel<T> : BaseActivityDisplayViewModelRaw where T : ActivitySettings, new()
    {
        public T ActivitySettings { get; private set; }

        public BaseActivityDisplayViewModel (string displayName, T settings) : base(displayName)
        {
            ActivitySettings = settings;
        }

    }
}

