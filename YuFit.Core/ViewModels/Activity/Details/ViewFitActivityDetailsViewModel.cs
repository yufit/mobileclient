﻿using System;
using System.Collections.Generic;
using System.Linq;


using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Models;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Activity.Comments;
using YuFit.Core.ViewModels.DisplayActivities;

using YuFit.WebServices.Exceptions;
using YuFit.WebServices.Interface.Model;
using YuFit.Core.ViewModels.CreateActivity;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using MvvmCross.Platform;

namespace YuFit.Core.ViewModels.Activity.Details
{
    public class ViewFitActivityDetailsViewModel : BaseFitActivityViewModel
    {
        public bool IsLoaded { get; set; }

        public ActivityType ActivityType { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public int? Duration { get; set; }
        public int? IntensityLevel { get; set; }
        public string Sport { get; set; }
        public string SportSubType { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime LastUpdate { get; set; }
        public string Visibility { get; set; }
        public ILocation Location { get; set; }
        public string Address { get; set; }
        public bool IsCreator { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsExpired { get; set; }
        public bool IsPrivate { get; set; }

        public string CancelString  { get; private set; }
        public string ExpiredString { get; private set; }

        public string CreatedByLabel { get; set; }

        public CommandViewModel EditCommand { get; set; }

        public CommandViewModel ShowCommentsCommand { get; private set; }
        public CommandViewModel SendCommentCommand { get; private set; }

        public SilentObservableCollection<ActivityCommentViewModel> Comments { get; private set; }
        public BaseActivityDisplayViewModelRaw ActivityDisplayViewModel { get; set; }

        public string AddCommentPlaceholder { get; set; }

        #pragma warning disable 414
        readonly MvxSubscriptionToken _commentDeletedMessageToken;
        readonly MvxSubscriptionToken _activityUpdatedMessageToken;
        #pragma warning restore 414

        public ViewFitActivityDetailsViewModel () : base("activity.view.details.title")
        {
            CreatedByLabel = Localized("activity.view.details.created.by.label");
            CancelString = Localized("activity.summary.canceled.label");
            ExpiredString = Localized("activity.summary.expired.label");

            EditCommand = new CommandViewModel(Localized("activity.view.details.edit.command"), new MvxCommand(EditFitActivity));
            ShowCommentsCommand = new CommandViewModel(new MvxCommand(ShowComments));
            SendCommentCommand = new CommandViewModel(new MvxCommand<string>(SendComment));
            Comments = new SilentObservableCollection<ActivityCommentViewModel>();

            AddCommentPlaceholder = Localized("activity.view.details.comment.placeholder");

            _commentDeletedMessageToken = Messenger.Subscribe<EventCommentDeletedMessage>(OnCommentDeleted);
            _activityUpdatedMessageToken = Messenger.Subscribe<EventUpdatedMessage>(OnActivityUpdated);
        }

        public override async void Init (Navigation navParams)
        {
            base.Init(navParams);

            // get fit event details 
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    FitActivity activity = await FitActivityService.GetActivity(ActivityId, token);

                    if (activity != null) {
                        DisplayName = activity.Name;
                        CreatedBy = activity.CreatedBy.Name;
                        Description = activity.Description;
                        Duration = activity.Duration;
                        IntensityLevel = activity.IntensityLevel;
                        Sport = activity.Sport;
                        SportSubType = activity.SubSport;
                        Location = activity.Location;
                        StartTime = ((EventSchedule)activity.EventSchedule).LocalStartTime;
                        Visibility = activity.Visibility;
                        Name = activity.Name;
                        CreatedBy = activity.CreatedBy.Name;
                        Address = ((Location) Location).FullAddress();
                        ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(a => a.TypeName == activity.Sport);
                        IsCreator = Mvx.Resolve<IUserService>().AmITheCreatorOfThisActivity(activity);
                        IsCanceled = activity.IsCanceled.HasValue && activity.IsCanceled.Value;
                        IsExpired = activity.EventSchedule.StartTime < DateTime.UtcNow;
                        IsPrivate = activity.IsPrivate.HasValue && activity.IsPrivate.Value;

                        UpdateActivityViewModel(activity);
                        if (activity.IsAPromotedEvent) {
                            SportSubType = Localized(String.Format("{0}.{1}", Sport, SportSubType).ToLower());
                        } else {
                            SportSubType = LocalizedSubsport(activity);
                        }

                        if (!string.IsNullOrEmpty(Description)) {
                            var comment = new ActivityCommentViewModel(ActivityType.TypeName, Description, string.Empty);
                            Comments.Add(comment);
                        }

                        var UserService = Mvx.Resolve<IUserService>();
                        var activityComments = await FitActivityService.GetActivityComments(ActivityId);
                        var tmp = new List<ActivityCommentViewModel>();
                        activityComments.ForEach(comment => Mvx.Trace (comment.Comment));
                        activityComments.ForEach(comment => tmp.Add(new ActivityCommentViewModel(ActivityId, comment, ActivityType.TypeName, UserService.IsThisMe(comment.UserId))));
                        var orderedList = tmp.OrderBy(x => x.UpdatedAt).ToList();
                        Comments.AddMultiple(orderedList);

                    } else {
                        Mvx.Trace(string.Format("Error: got a bad event id: {0}", ActivityId));
                    }

                    IsLoaded = true;
                });
            } catch (NotFoundException /*exception*/) {
                Alert ("activity.unavailable", null, "Error");
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }

        }

        void EditFitActivity ()
        {
            ShowViewModel<EditFitActivityViewModel>(new BaseFitActivityViewModel.Navigation { ActivityId = ActivityId });
        }

        void ShowComments ()
        {
            ShowViewModel<ActivityCommentsViewModel>(new BaseFitActivityViewModel.Navigation {
                ActivityId = ActivityId,
                ActivityTypeName = ActivityType.TypeName
            });
        }

        async void SendComment(string comment)
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var addedComment = await FitActivityService.AddComment(ActivityId, comment, token);
                    var vm = new ActivityCommentViewModel(ActivityId, addedComment, ActivityType.TypeName, true);
                    Comments.Add(vm);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        void UpdateActivityViewModel (FitActivity activity)
        {
            try
            {
                if (!activity.IsAPromotedEvent) {
                    if (ActivityType != null) {

                        const string ActivityTypeVMFormat = "YuFit.Core.ViewModels.DisplayActivities.{0}ActivityDisplayViewModel";
                        string className = string.Format(ActivityTypeVMFormat, ActivityType.TypeName);
                        Type activityViewModelClassType = Type.GetType(className);

                        if (activityViewModelClassType != null) {
                            if (ActivityDisplayViewModel == null || ActivityDisplayViewModel.GetType() != activityViewModelClassType) {
                                ActivityDisplayViewModel = (BaseActivityDisplayViewModelRaw)Activator.CreateInstance(activityViewModelClassType, activity.ActivitySettings);
                            } else {
                                ActivityDisplayViewModel.Update();
                            }
                        } else {
                            ActivityDisplayViewModel = null;
                        }
                    } else {
                        ActivityDisplayViewModel = null;
                    }
                } else {
                    ActivityDisplayViewModel = null;
                    IsCreator = false;
                }
            }
            catch (Exception /*ex*/)
            {
            }
        }

        void OnCommentDeleted (EventCommentDeletedMessage obj)
        {
            var deletedComment = Comments.FirstOrDefault(c => c.Id == obj.CommentId);
            if (deletedComment != null) { Comments.Remove(deletedComment); }
        }

        async void OnActivityUpdated (EventUpdatedMessage obj)
        {
            if (ActivityId != obj.Activity.Id) { return; }

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    FitActivity activity = await FitActivityService.GetActivity(ActivityId, token);
                    Comments.Clear();
                    if (activity != null) {
                        DisplayName = activity.Name;
                        CreatedBy = activity.CreatedBy.Name;
                        Description = activity.Description;
                        Duration = activity.Duration;
                        IntensityLevel = activity.IntensityLevel;
                        Sport = activity.Sport;
                        SportSubType = activity.SubSport;
                        Location = activity.Location;
                        StartTime = ((EventSchedule)activity.EventSchedule).LocalStartTime;
                        Visibility = activity.Visibility;
                        Name = activity.Name;
                        CreatedBy = activity.CreatedBy.Name;
                        Address = ((Location) Location).FullAddress();
                        ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(a => a.TypeName == activity.Sport);
                        IsCreator = Mvx.Resolve<IUserService>().AmITheCreatorOfThisActivity(activity);
                        IsCanceled = activity.IsCanceled.HasValue && activity.IsCanceled.Value;
                        IsPrivate = activity.IsPrivate.HasValue && activity.IsPrivate.Value;

                        UpdateActivityViewModel(activity);
                        SportSubType = LocalizedSubsport(activity);

                        if (!string.IsNullOrEmpty(Description)) {
                            var comment = new ActivityCommentViewModel(ActivityType.TypeName, Description, string.Empty);
                            Comments.Add(comment);
                        }

                        var UserService = Mvx.Resolve<IUserService>();
                        var activityComments = await FitActivityService.GetActivityComments(ActivityId);
                        var tmp = new List<ActivityCommentViewModel>();
                        activityComments.ForEach(comment => Mvx.Trace (comment.Comment));
                        activityComments.ForEach(comment => tmp.Add(new ActivityCommentViewModel(ActivityId, comment, ActivityType.TypeName, UserService.IsThisMe(comment.UserId))));
                        var orderedList = tmp.OrderBy(x => x.UpdatedAt).ToList();
                        Comments.AddMultiple(orderedList);

                    } else {
                        Mvx.Trace(string.Format("Error: got a bad event id: {0}", ActivityId));
                    }

                    IsLoaded = true;
                });
            } catch (NotFoundException /*exception*/) {
                Alert ("activity.unavailable", null, "Error");
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }
    }
}

