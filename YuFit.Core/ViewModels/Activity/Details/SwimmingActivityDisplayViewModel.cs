﻿using YuFit.Core.Models.Activities;

namespace YuFit.Core.ViewModels.DisplayActivities
{
    public class SwimmingActivityDisplayViewModel : BaseActivityDisplayViewModel<SwimmingActivitySettings>
    {
        private const string localPrefixKey = "activity.Swimming.";

        public string Duration { get; set; }
        public string Distance { get; set; }
        public string Intensity { get; set; }
        public SwimmingType SwimmingType { get; set; }

        public SwimmingActivityDisplayViewModel (SwimmingActivitySettings settings) : base("", settings)
        {
            UpdateValues();
        }

        public override void Update ()
        {
            UpdateValues();
            base.Update();
        }

        void UpdateValues()
        {
            string durationFormat = Localized(localPrefixKey + "duration.format");
            string distanceFormat = Localized(localPrefixKey + "distance.format");

            Duration = string.Format(durationFormat, ActivitySettings.Duration / 60 , ActivitySettings.Duration % 60);
            Distance = string.Format(distanceFormat, ActivitySettings.Distance);
            SwimmingType = ActivitySettings.SwimmingType;
            Intensity = Localized(string.Format("activity.intensity.value.{0}", 
                ActivitySettings.IntensityLevel.ToString().ToLower()));

        }
    }
}

