﻿using YuFit.Core.Models.Activities;

namespace YuFit.Core.ViewModels.DisplayActivities
{
    public class CyclingActivityDisplayViewModel : BaseActivityDisplayViewModel<CyclingActivitySettings>
    {
        private const string localPrefixKey = "activity.Cycling.";

        public string Distance { get; set; }
        public string Duration { get; set; }
        public string Intensity { get; set; }

        public CyclingActivityDisplayViewModel (CyclingActivitySettings settings) : base("", settings)
        {
            UpdateValues();
        }

        public override void Update ()
        {
            UpdateValues();
            base.Update();
        }

        void UpdateValues()
        {
            string durationFormat = Localized(localPrefixKey + "duration.format");

            Intensity = Localized(ActivitySettings.IntensityLevel.ToString().ToLower());
            Distance = ActivitySettings.Distance + " KM";
            Duration = string.Format(durationFormat, ActivitySettings.Duration / 60 , ActivitySettings.Duration % 60);
        }
    }
}

