﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Models;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Activity;
using YuFit.Core.ViewModels.Profiles.Edit;
using YuFit.Core.ViewModels.Activity.Summary;
using YuFit.Core.ViewModels.Event;
using MvvmCross.Plugins.Messenger;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;

namespace YuFit.Core.ViewModels.Home
{
    public class HomeMenuViewModel : BaseViewModel
    {
        private IUserService _userService;
        private IFitActivityService _fitActivityService;

        public string   RealName                { get; set; }
        public string   HomeAddress             { get; set; }
        public Uri      AvatarUrl               { get; set; }
        public string   Bio                     { get; set; }
        public string   UpcomingActivitiesTitle { get; set; }
        public SilentObservableCollection<UpcomingActivityViewModel> UpcomingActivities { get; private set; } 

        public CommandViewModel EditProfileCommand { get; private set; }
        public CommandViewModel EditNotificationsCommand { get; private set; }
        public CommandViewModel SignOutCommand { get; private set; }
        public CommandViewModel ShowActivitySummaryCommand { get; private set; }

        #pragma warning disable 414
        readonly MvxSubscriptionToken _userProfileUpdatedMessageToken;
        #pragma warning restore 414


        public HomeMenuViewModel (
            IFitActivityService fitActivityService, 
            IUserService userService
        ) : base(string.Empty)
        {
            DisplayName = Localized("home.title");
            _userService = userService;
            _fitActivityService = fitActivityService;

            UpcomingActivities      = new SilentObservableCollection<UpcomingActivityViewModel>();
            UpcomingActivitiesTitle = Localized("activity.upcoming.label");

            EditProfileCommand          = new CommandViewModel(new MvxCommand(() => ShowViewModel<EditProfileViewModel>()));
            EditNotificationsCommand    = new CommandViewModel(Localized("home.edit.notifications.label"), new MvxCommand(EditNotifications));
            SignOutCommand              = new CommandViewModel(Localized("home.sign.out.label"), new MvxCommand(SignOut));
            ShowActivitySummaryCommand  = new CommandViewModel(new MvxCommand<UpcomingActivityViewModel>(ShowActivitySummary));

            _userProfileUpdatedMessageToken = Messenger.Subscribe<UserProfileUpdatedMessage>(OnProfileUpdated);
        }

        public async override void Start ()
        {
            base.Start();

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    IsBusy = true;
                    User currentUser = (User) await _userService.GetLoggedInUserInfo(true, token);

                    RealName = currentUser?.Name;
                    AvatarUrl = currentUser?.UserProfile?.AvatarUri;
                    Bio = currentUser?.UserProfile?.Bio;

                    var home = currentUser.GetHomeLocation();
                    HomeAddress = home?.CityStateString() ?? Localized("user.info.location.not.provided.text");

                    await GetNextTraining();
                    IsBusy = false;
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        private async Task GetNextTraining()
        {
            var incomingActivities = await _fitActivityService.MyUpcomingActivities(limit: 10);

            var tmp = new List<UpcomingActivityViewModel>();
            incomingActivities.ForEach(act => tmp.Add(new UpcomingActivityViewModel(act)));
            var orderedList = tmp.OrderBy(x => x.When).ToList();
            UpcomingActivities.Clear();
            UpcomingActivities.AddMultiple(orderedList);
        }

        void EditNotifications()
        {
            ShowViewModel<EditNotificationsViewModel>();
        }

        void ShowActivitySummary (UpcomingActivityViewModel obj)
        {
            if (obj.IsPromoted) {
                ShowViewModel<EventViewModel>(new EventViewModel.Nav { Id = obj.Id });
            } else {
                ShowViewModel<ActivitySummary2ViewModel>(new ActivitySummary2ViewModel.Nav { Id = obj.Id });
            }
//            var message = new InvitedToActivityMessage(this, obj.Id, true);
//            Mvx.Resolve<IMvxMessenger>().Publish(message);
        }

        void OnProfileUpdated (UserProfileUpdatedMessage obj)
        {
            var currentUser = obj.User;
            RealName = currentUser.Name;

            if (currentUser.UserProfile != null) {
                AvatarUrl = currentUser.UserProfile.AvatarUri;
                Bio = currentUser.UserProfile.Bio;
            }

            var home = currentUser.GetHomeLocation();
            if (home != null) {
                HomeAddress = home.CityStateString();
            }

            if (HomeAddress == null) {
                HomeAddress = Localized("user.info.location.not.provided.text");
            }
        }

        protected override void OnClosing ()
        {
            Messenger.Unsubscribe<UserProfileUpdatedMessage>(_userProfileUpdatedMessageToken);
            CancelRequestInProgress();
        }

        protected void SignOut()
        {
            ProgressMessage = Localized("home.sign.out.progress.message");

            Task.Run(async () => 
            {
                IsBusy = true;    
                await Task.Delay(500);
                _userService.Signout();
                IsBusy = false;
            });
        }
    }
}

