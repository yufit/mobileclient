﻿using System;
using Cirrious.MvvmCross.ViewModels;
using YuFit.Core.ViewModels.CreateActivity;
using System.Windows.Input;

namespace YuFit.Core.ViewModels.Home
{
    public class HomeMenuItemViewModel : MvxViewModel
    {
        public string Icon { get; set; }
        public string MenuText { get; set; }
        public int BadgeNumber { get; set; }

        public ICommand Command { get; set; }
    }
}

