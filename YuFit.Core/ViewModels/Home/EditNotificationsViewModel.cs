﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Attributes;
using YuFit.Core.Extensions;
using YuFit.Core.Services.Notifications;
using YuFit.Core.Utils;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.ViewModels.Home
{
    public class EnableNotificationViewModel : BaseViewModel
    {
        public bool IsEnabled { get; set; }
        public string NotificationTitle { get; set; }
        public string CategoryName { get; set; }

        public EnableNotificationViewModel (PushNotificationCategory category, bool isEnabled)
        {
            Type t = category.GetType();
            FieldInfo fieldInfo = t.GetRuntimeField(category.ToString());
            var attribute =  (StringValueAttribute) fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false).First();
            CategoryName = attribute.StringValue;
            NotificationTitle = Localized(String.Format("home.edit.notifications.title.{0}", CategoryName));

            IsEnabled = isEnabled;
        }
    }

    public class EditNotificationsViewModel : BaseViewModel
    {
        private readonly IUserService _userService;

        public SilentObservableCollection<EnableNotificationViewModel> EnabledNotifications { get; private set; }

        public EditNotificationsViewModel (
            IUserService userService
        ) : base("home.edit.notifications.title") 
        {
            _userService = userService;
            EnabledNotifications = new SilentObservableCollection<EnableNotificationViewModel>();
        }

        public override void Start ()
        {
            base.Start();

            // Fetch the info for the user id.
            var user = _userService.LoggedInUser;
            var setNotifications = user.UserProfile?.NotificationPreferences;

            var tmp = new List<EnableNotificationViewModel>();

            foreach (PushNotificationCategory val in Enum.GetValues(typeof(PushNotificationCategory)))
            {
                var skipCategory = val == PushNotificationCategory.GeneralMessage || val == PushNotificationCategory.Unknown;
                if (!skipCategory) {
                    Type t = val.GetType();
                    FieldInfo fieldInfo = t.GetRuntimeField(val.ToString());
                    var attribute = (StringValueAttribute) fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false).First();
                    var categoryName = attribute.StringValue;

                    var setNotification = setNotifications?.FirstOrDefault(n => n.CategoryName == categoryName);
                    tmp.Add(new EnableNotificationViewModel(val, setNotification == null || !setNotification.IsDisabled));
                }
            }

            EnabledNotifications.Clear();
            EnabledNotifications.AddMultiple(tmp);

            base.Start();
        }

        protected override async void OnClosing ()
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    var notificationPreferences = new List<INotificationPreference>();
                    EnabledNotifications.ForEach(notVm => notificationPreferences.Add(new NotificationPreference {
                        CategoryName = notVm.CategoryName,
                        IsDisabled = !notVm.IsEnabled
                    }));

                    var user = _userService.LoggedInUser;
                    user.UserProfile = user.UserProfile ?? new UserProfile();
                    user.UserProfile.NotificationPreferences = notificationPreferences;

                    await _userService.UpdateUserInfo(user, token);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }

            base.OnClosing();
        }
    }
}

