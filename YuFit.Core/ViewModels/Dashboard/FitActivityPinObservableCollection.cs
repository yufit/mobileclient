﻿//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Linq;
//using YuFit.Core.Extensions;
//using YuFit.Core.Models;
//using System.Collections.Specialized;
//
//namespace YuFit.Core.ViewModels.Dashboard
//{
//	/// <summary>
//	/// Fit event view model observable collection.
//	/// </summary>
//	public class FitActivityPinObservableCollection : ObservableCollection<FitActivityPinViewModel>
//	{
//		/// <summary>
//		/// Syncs the collection.
//		/// For performance, first handle the remove cases.  Then the modified, then the added.
//		/// </summary>
//		/// <param name="events">Events.</param>
//		public void SyncCollection(List<FitActivity> events)
//		{
//			List<FitActivityPinViewModel> incomingList = new List<FitActivityPinViewModel>();
//			events.ForEach(ev => incomingList.Add(FitActivityPinViewModel.CreateFrom(ev)));
//
//			List<FitActivityPinViewModel> removed = this.Except(incomingList, new ComparerById()).ToList();
//			removed.ForEach(ev => Remove(ev));
//
//			List<FitActivityPinViewModel> modified = incomingList.Intersect(this, new ComparerToFindModifiedPin()).ToList();
//			modified.ForEach(updatedEv => this.FirstOrDefault(ev => ev.Id == updatedEv.Id).Copy(updatedEv));
//
//			List<FitActivityPinViewModel> added = incomingList.Except(this, new ComparerById()).ToList();
//			added.ForEach(Add);
//		}
//
//        public void Moved (FitActivityPinViewModel activityVm, FitActivity activity)
//        {
//            var newVm = FitActivityPinViewModel.CreateFrom(activity);
//            Remove(activityVm);
//            Add(newVm);
//            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, newVm, activityVm));
//        }
//
//        public void Reset() 
//        {
//            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
//        }
//
//		#region Comparer classes
//
//		class ComparerById : IEqualityComparer<FitActivityPinViewModel>
//		{
//			// Analysis disable once MemberHidesStaticFromOuterClass
//			public bool Equals(FitActivityPinViewModel x, FitActivityPinViewModel y)
//			{
//				if (Object.ReferenceEquals(x, y)) { return true; }
//				if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null)) { return false; }
//				return string.Equals(x.Id, y.Id);
//			}
//
//			// If Equals() returns true for a pair of objects  
//			// then GetHashCode() must return the same value for these objects. 
//			public int GetHashCode(FitActivityPinViewModel product)
//			{
//				return Object.ReferenceEquals(product, null) ? 0 : product.Id.GetHashCode();
//			}
//
//		}
//
//		class ComparerToFindModifiedPin : IEqualityComparer<FitActivityPinViewModel>
//		{
//			// Analysis disable once MemberHidesStaticFromOuterClass
//			public bool Equals(FitActivityPinViewModel x, FitActivityPinViewModel y)
//			{
//				return string.Equals(x.Id, y.Id) && !x.IsEquals(y);
//			}
//
//			// If Equals() returns true for a pair of objects  
//			// then GetHashCode() must return the same value for these objects. 
//			public int GetHashCode(FitActivityPinViewModel product)
//			{
//				return Object.ReferenceEquals(product, null) ? 0 : product.Id.GetHashCode();
//			}
//
//		}
//
//		#endregion
//
//	}
//}
//
