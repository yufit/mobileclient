using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Coc.MvvmCross.Plugins.Location;

using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Plugins.Messenger;

using Xamarin;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Models;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Activity.Details;
using YuFit.Core.ViewModels.Activity.Summary;
using YuFit.Core.ViewModels.Associations;
using YuFit.Core.ViewModels.Associations.Groups;
using YuFit.Core.ViewModels.CreateActivity;
using YuFit.Core.ViewModels.Event;
using YuFit.Core.ViewModels.Home;
using YuFit.Core.ViewModels.MapAnnotations;
using YuFit.Core.ViewModels.Messages;
using YuFit.Core.ViewModels.Notifications;

using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.ViewModels.Dashboard
{
    public class FlipFromRightHint : MvxPresentationHint {}
    public class FlipFromLeftHint : MvxPresentationHint {}

    public class DashboardViewModel : BaseViewModel
    {
        bool _ready;
        #region Data members

        readonly IFitActivityService _fitActivityService;
        readonly IUserService _userService;

        readonly ICocLocationProvider _locationProvider;
        readonly IGeoLocationConverterService _locationConverterService;

        bool _viewingSummary;

        #endregion

        #pragma warning disable 414
        readonly MvxSubscriptionToken _activityCreatedMessageToken;
        readonly MvxSubscriptionToken _activityUpdatedMessageToken;
        readonly MvxSubscriptionToken _activityDeletedMessageToken;
        readonly MvxSubscriptionToken _userSignedOutToken;

        readonly MvxSubscriptionToken _invitedToActivityToken;
        readonly MvxSubscriptionToken _filtersUpdatedMessageToken;
        readonly MvxSubscriptionToken _appEnteredForegroundMessageToken;
        readonly MvxSubscriptionToken _eventRemovedFromInvitationMessageToken;
        readonly MvxSubscriptionToken _pushNotificationMessageToken;

        readonly MvxSubscriptionToken _showGroupVmMessageToken;
        readonly MvxSubscriptionToken _showActivityDetailsVmMessageToken;

        #pragma warning restore 414

        #region Properties

        public bool     FirstTimeRunnning   { get; set; }
        public Region   CurrentRegion       { get; set; }
        public string   SearchTerm          { get; set; }
        public IUser    LoggedInUser        { get; set; }
        bool _isInstanceValid = true;

        public MapAnnotationViewModel SelectedPinVM { get; private set; }
        public MapAnnotationObservableCollection FitActivities { get; private set; }

        public void OnCurrentRegionChanged()
        {
            Mvx.Resolve<ISandboxService>().SetItem("currentRegion", CurrentRegion);
            UpdateActivityCollection();
        }

        public int NumberOfNewInvitations { get; private set; }
        public int NumberOfNewNotifications { get; private set; }

        #region Commands

        public CommandViewModel BringBackToWhereIAmCommand              { get; private set; }

        public CommandViewModel DisplayFriendsCommand                   { get; private set; }
        public CommandViewModel DisplayFiltersCommand                   { get; private set; }
        public CommandViewModel DisplayMessagesCommand                  { get; private set; }
        public CommandViewModel DisplayInvitesCommand                   { get; private set; }
        public CommandViewModel DisplayNotificationsCommand             { get; private set; }
        public CommandViewModel CreateFitActivityCommand                { get; private set; }
        public CommandViewModel CreateFitActivityAtCommand              { get; private set; }

        public CommandViewModel ShowHomeMenuCommand                     { get; private set; }

        public CommandViewModel DisplayActivitySummaryCommand           { get; private set; }
        public CommandViewModel DisplayNextActivitySummaryCommand       { get; private set; }
        public CommandViewModel DisplayPreviousActivitySummaryCommand   { get; private set; }
        public CommandViewModel DismissActivitySummaryCommand           { get; private set; }

        public CommandViewModel SearchCommand                           { get; private set; }

        #endregion

        #endregion

        #region Construction/Destruction

        /// <summary>
        /// Initializes a new instance of the <see cref="YuFit.Core.ViewModels.Dashboard.DashboardViewModel"/> class.
        /// </summary>
        /// ------------------------------------------------------------------------------------
        public DashboardViewModel (
            IFitActivityService             fitActivityService, 
            IUserService                    userService,
            IGeoLocationConverterService    locationConverterService,
            ICocLocationManager             locationManager,
            ICocLocationProvider            locationProvider, 
            IFileIoService                  fileService
        ) : base("dashboard.title")
        {
            _ready                      = false;
            _fitActivityService         = fitActivityService;
            _userService                = userService;
            _locationProvider           = locationProvider;
            _locationConverterService   = locationConverterService;

            var accessInfo      = fileService.Read<AccessInformation>(AccessInformation.Key);
            FirstTimeRunnning   = accessInfo.IsFirstTimeUser();
            accessInfo.Update();
            fileService.Write<AccessInformation>(AccessInformation.Key, accessInfo);

            _activityCreatedMessageToken            = Messenger.Subscribe<EventCreatedMessage>              (OnActivityCreated);
            _activityUpdatedMessageToken            = Messenger.Subscribe<EventUpdatedMessage>              (OnActivityUpdated);
            _activityDeletedMessageToken            = Messenger.Subscribe<EventDeletedMessage>              (OnActivityDeleted);
            _userSignedOutToken                     = Messenger.Subscribe<UserLoggedOutMessage>             (OnUserSignedOut);
            _invitedToActivityToken                 = Messenger.Subscribe<InvitedToActivityMessage>         (OnInvitedToActivity);
            _filtersUpdatedMessageToken             = Messenger.Subscribe<UserFiltersUpdatedMessage>        (OnFiltersUpdated);
            _appEnteredForegroundMessageToken       = Messenger.Subscribe<AppEnteredForegroundMessage>      (OnAppEnteredForeground);
            _eventRemovedFromInvitationMessageToken = Messenger.Subscribe<EventRemovedFromInvitationMessage>(OnEventRemovedFromInvitation);
            _showGroupVmMessageToken                = Messenger.Subscribe<DisplayGroupMessage>              (OnShowGroupVM);
            _showActivityDetailsVmMessageToken      = Messenger.Subscribe<DisplayDetailActivityMessage>     (OnShowDetailActivityVM);
            _pushNotificationMessageToken           = Messenger.Subscribe<PushNotificationMessage>          (OnPushNotificationReceived);

            FitActivities = new MapAnnotationObservableCollection();

            BringBackToWhereIAmCommand              = new CommandViewModel(new MvxCommand(BringBackToWhereIAm));
            CreateFitActivityAtCommand              = new CommandViewModel(new MvxCommand<CocLocation>(CreateFitActivityAt));
            CreateFitActivityCommand                = new CommandViewModel(new MvxCommand(CreateFitActivity));
            DismissActivitySummaryCommand           = new CommandViewModel(new MvxCommand(DismissSummary));
            DisplayFriendsCommand                   = new CommandViewModel(new MvxCommand(DisplayFriends));
            DisplayFiltersCommand                   = new CommandViewModel(new MvxCommand(DisplayFilters));
            DisplayMessagesCommand                  = new CommandViewModel(new MvxCommand(DisplayMessages));
            DisplayInvitesCommand                   = new CommandViewModel(new MvxCommand(DisplayInvites));
            DisplayNotificationsCommand             = new CommandViewModel(new MvxCommand(DisplayNotifications));
            DisplayActivitySummaryCommand           = new CommandViewModel(new MvxCommand<MapAnnotationViewModel>(DisplayActivitySummary));
            DisplayNextActivitySummaryCommand       = new CommandViewModel(new MvxCommand(DisplayNextActivitySummary));
            DisplayPreviousActivitySummaryCommand   = new CommandViewModel(new MvxCommand(DisplayPreviousActivitySummary));
            SearchCommand                           = new CommandViewModel(new MvxCommand(Search));
            ShowHomeMenuCommand                     = new CommandViewModel(new MvxCommand(ShowHomeMenu));

            NumberOfNewInvitations = 0;
        }

        #endregion

        #region Methods

        #region Mvx override

        public override void Start ()
        {
            base.Start();

            // MOVE THAT CODE IN THE LOCATION PLUGIN?
            Region lastKnownLocation = Mvx.Resolve<IFileIoService>().Read<Region>("lastKnownLocation");
            if (Math.Abs(lastKnownLocation.Radius) < 0.01f)
            {
                CocLocation currentLocation = _locationProvider.TryGetLocationUsingTimezone();
                CurrentRegion = new Region {
                    Center = new Coordinate(currentLocation.Coordinates.Latitude, currentLocation.Coordinates.Longitude),
                    Radius = 1000
                };
            }
            else
            {
                // This is to avoid the zoomin factor when starting for a far.  If you have run
                // the app at least once, we will have something to start with.
                CurrentRegion = lastKnownLocation;
            }

            // TODO Add a mechanism to have set of classes executed whenever 
            StartSequence();
        }

        #endregion

        async void StartSequence(bool centerMapOnPosition = true) {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    await _userService.RefreshCurrentUserInfo(token);
                    LoggedInUser = _userService.LoggedInUser;

                    if (LoggedInUser != null) {
                        var manyInfos = new Dictionary<string, string> {
                            {"Email", LoggedInUser.Email} ,
                            {"Name", LoggedInUser.Name }
                        };
                        Mvx.Trace(">>>>>>>>>>>>>>>>> {0}", LoggedInUser.Name);
                        Insights.Identify(LoggedInUser.Name, manyInfos);
                    }

                    var activityToDisplay = Mvx.Resolve<ISandboxService>().GetItem<string>("ActivityIdToDisplay");
                    if (!string.IsNullOrEmpty(activityToDisplay)) {
                        Mvx.Trace("++++++++++++++++ SHOWING ACTIVITY!!!");
                        await Task.Delay(500);
                        ShowInvitation();
                    } else if (centerMapOnPosition) {
                        CenterOnCurrentLocation();
                    }

                    var pushSvc = Mvx.Resolve<IPushNotificationService>();
                    var notifications = await pushSvc.GetNotifications(token);
                    NumberOfNewNotifications = notifications.Count(notification => !notification.IsAcked);
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }

        }

        void ShowInvitation() {
//            var activityToDisplay = Mvx.Resolve<ISandboxService>().GetItem<string>("ActivityIdToDisplay");
//            if (!string.IsNullOrEmpty(activityToDisplay)) {
//
//                try
//                {
//                    await updateActivities.ExecuteRequest(async (token, tryNum) => {
//                        var activity = await _fitActivityService.GetActivity(activityToDisplay, token);
//                        var activityVm = MapAnnotationObservableCollection.CreateFrom(activity);
//                        FitActivities.Add(activityVm);
//
//                        SelectedPinVM = activityVm;
//
//                        ShowViewModel<ActivitySummaryViewModel>(
//                            new ActivitySummaryViewModel.Nav { Id = activityToDisplay }
//                        );
//                    });
//                }
//                catch (Exception ex)
//                {
//                    Mvx.Trace(ex.ToString());
//                }
//            }

            _ready = true;
            UpdateActivityCollection();
        }

        //IFacebookFriendInvitor invitor;
        async void CenterOnCurrentLocation()
        {
            CocLocation currentLocation = null;

            try {
                currentLocation = await _locationProvider.TryGetLocationFromDeviceApi();
            } catch (Exception ex) {
//                currentLocation = await _locationProvider.TryGetLocationUsingIpAddress();
                Mvx.Trace(ex.ToString());
            }

            if (currentLocation != null) {
                CurrentRegion = new Region {
                    Center = new Coordinate(currentLocation.Coordinates.Latitude, currentLocation.Coordinates.Longitude),
                    Radius = 20
                };
            }

            Mvx.Resolve<IFileIoService>().Write<Region>("lastKnownLocation", CurrentRegion);

            _ready = true;
            UpdateActivityCollection();
            //UpdateInvitationCount();

            if (FirstTimeRunnning) {
                // Ask user about enabling notifications.
                await UserDialogs.AlertAsync(
                    Localized("dashboard.notifications.registration.message"), 
                    Localized("dashboard.notifications.registration.title"), 
                    Localized("dashboard.notifications.registration.ok.button"));

                // TODO MOVE THIS TO THE FRIEND SCREEN ON FIRST RUN.
//                bool doItNow = await UserInteractionService.ConfirmAsync(
//                    Localized("dashboard.notifications.invite.friends.message"), 
//                    Localized("dashboard.notifications.invite.friends.title"), 
//                    Localized("dashboard.notifications.invite.friends.now.button"),
//                    Localized("dashboard.notifications.invite.friends.later.button")
//                );
//
//                if (doItNow) {
//                    var fbService = Mvx.Resolve<IFacebookService>();
//                    invitor = fbService.GetInvitor();
//                    invitor.OperationComplete = (canceled, error) => {
//                        invitor.OperationComplete = null;
//                        invitor = null;
//                    };
//                    invitor.InviteFacebookFriends();
//                }
//
                FirstTimeRunnning = false;
            }

            Mvx.Resolve<IPushNotificationService>().SetupNotificationsHandler();

            Messenger.Publish(new AppStartedMessage(this));
        }

        #region Command handlers

        void BringBackToWhereIAm   () { CenterOnCurrentLocation();                      }
        void CreateFitActivity     () { ShowViewModel<CreateFitActivityViewModel>();    }
        void DisplayFriends        () { ShowViewModel<MyAssociationsViewModel>();       }
        void DisplayFilters        () { ShowViewModel<MapFiltersViewModel>();           }
        void DisplayMessages       () { ShowViewModel<MessagesViewModel>();             }
        void DisplayInvites        () { ShowViewModel<ViewInvitationsViewModel>();      }
        void DisplayNotifications  () { 
            ShowViewModel<NotificationsViewModel>();
            // TODO CHANGE THIS TO BE UPDATED FROM MONITORING THE NOTIFICATIONS FROM THE SERVICE.
            NumberOfNewNotifications = 0;
        }
        void ShowHomeMenu          () { 
//            var task = Mvx.Resolve<IMvxPictureChooserTask>();
//            task.ChoosePictureFromLibrary(500, 90,
//                stream => {
//                // use the stream
//                // expect the stream to be disposed after immediately this method returns.
//            },
//                () => {
//                // perform any cancelled operation
//            });
            ShowViewModel<HomeMenuViewModel>();
        }

        void CreateFitActivityAt(CocLocation location) {
            ShowViewModel<CreateAtFitActivityViewModel>(
                new CreateAtFitActivityViewModel.CreateAtFitActivityVMNavigation(location)
            );
        }

        void DisplayActivitySummary (MapAnnotationViewModel obj)
        {
            if (obj is EventMapAnnotationViewModel) {
                ShowViewModel<EventViewModel>(
                    new EventViewModel.Nav { Id = obj.Id }
                );
            } else {
                _ready = false;
                _viewingSummary = true;
                SelectedPinVM = obj;

                ShowViewModel<ActivitySummaryViewModel>(
                    new ActivitySummaryViewModel.Nav { Id = obj.Id }
                );
            }
        }

        void DisplayNextActivitySummary ()
        {
            if (SelectedPinVM == null) { return; }

            var result = FitActivities.WithContext().Single(i => i.Current.Id == SelectedPinVM.Id);
            var next = result.Next ?? FitActivities.First();
            while (next is EventMapAnnotationViewModel) { 
                result = FitActivities.WithContext().Single(i => i.Current.Id == next.Id);
                next = result.Next ?? FitActivities.First(); 
            }

            ChangePresentation(new FlipFromRightHint());
            DisplayActivitySummary(next);
        }

        void DisplayPreviousActivitySummary ()
        {
            if (SelectedPinVM == null) { return; }

            var result = FitActivities.WithContext().Single(i => i.Current.Id == SelectedPinVM.Id);
            var previous = result.Previous ?? FitActivities.Last();
            while (previous is EventMapAnnotationViewModel) { 
                result = FitActivities.WithContext().Single(i => i.Current.Id == previous.Id);
                previous = result.Previous ?? FitActivities.Last(); 
            }

            ChangePresentation(new FlipFromLeftHint());
            DisplayActivitySummary(previous);
        }

        void DismissSummary()
        {
            _viewingSummary = false;
            _ready = true;
        }

        async void Search ()
        {
            try {
                var addresses = await _locationConverterService.GeocodeAddressAsync(SearchTerm);
                if (addresses.Count > 0) { 
                    Placemark pl = addresses[0];
                    CurrentRegion = new Region {
                        Center = pl.Coordinates,
                        Radius = 0.5
                    };
                }
            } catch (Exception /*ex*/) {
                SearchTerm = string.Empty;
            }
        }

        #endregion

        void OnUserSignedOut (UserLoggedOutMessage obj)
        {
            ShowViewModel<YuFit.Core.ViewModels.Login.LoginViewModel>();
        }

        void OnAppEnteredForeground (AppEnteredForegroundMessage obj)
        {
            // TODO Add a mechanism to have set of classes executed whenever 
            StartSequence(false);

//            //UpdateInvitationCount();
//            CenterOnCurrentLocation();
//
//            await ExecuteWebServiceRequestAsync(async () => await _userService.RefreshCurrentUserInfo(), ex => Mvx.Trace(ex.ToString()));
        }

        void OnActivityCreated (EventCreatedMessage obj)
        {
            CurrentRegion = new Region {
                Center = ((Location)obj.Activity.Location).GeoCoordinates,
                Radius = 5.0
            };
            UpdateActivityCollection();
        }

        void OnActivityUpdated (EventUpdatedMessage obj)
        {
            // Are we showing the updated activity vm already?
            var activityVm = FitActivities.FirstOrDefault(a => a.Id == obj.Activity.Id);
            if (activityVm == null) { return; }

            // We have this activity in our pin lists.  Are we displaying it in a summary view?
            // If we are, don't bother.  The summary vm will be notified of the change and update
            // itself.
            activityVm.Reload(obj.Activity);
            //UpdateActivityCollection();

            // This is kind of lame.  Problem is that if the event location was modified,
            // we want to move the pin.  To achieve this, we must remove it and re-add it.
            //var newVm = FitActivityPinViewModel.CreateFrom(obj.Activity);
            //FitActivities.Remove(activityVm);
            //FitActivities.Add(newVm);

//            FitActivities.Moved(activityVm, obj.Activity);
            //FitActivities.Reset();
        }

        void OnActivityDeleted (EventDeletedMessage obj)
        {
            // Are we showing the deleted activity vm already?
            var activityVm = FitActivities.FirstOrDefault(a => a.Id == obj.Activity.Id);
            if (activityVm == null) { return; }

            // We are showing it.  If the summary view is open, the summary vm will trap the\
            // delete as well and close itself.
            //if (activityVm == SelectedPinVM) {}

            // Now delete from the collection.
            FitActivities.Remove(activityVm);
        }

        void OnEventRemovedFromInvitation (EventRemovedFromInvitationMessage obj)
        {
            //if (NumberOfNewInvitations > 0) { NumberOfNewInvitations--; }
        }

        void OnShowGroupVM(DisplayGroupMessage message)
        {
            ShowViewModel<GroupViewModel>(new GroupViewModel.Navigation{GroupId = message.GroupId});
        }

        void OnShowDetailActivityVM(DisplayDetailActivityMessage message)
        {
            ShowViewModel<ViewFitActivityDetailsViewModel>(new BaseFitActivityViewModel.Navigation {ActivityId = message.EventId});
        }

        void OnFiltersUpdated (UserFiltersUpdatedMessage obj)
        {
            UpdateActivityCollection();
        }

        void OnInvitedToActivity (InvitedToActivityMessage obj)
        {
//            var activityVm = FitActivities.FirstOrDefault(a => a.Id == obj.ActivityId);
//
//            if (activityVm == null) {
//                await ExecuteWebServiceRequestAsync(async () => {
//                    var activity = await _fitActivityService.GetActivity(obj.ActivityId, false);
//
//                    activityVm = FitActivityPinViewModel.CreateFrom(activity);
//                    FitActivities.Add(activityVm);
//
//                    if (obj.MustShowSummary) {
//                        _ready = false;
//                        SelectedPinVM = activityVm;
//                        ShowViewModel<ActivitySummaryViewModel>(
//                            new ActivitySummaryViewModel.Nav { Id = SelectedPinVM.Id }
//                        );
//                    } else {
//                        //NumberOfNewInvitations++;
//                    }
//
//                }, error => {
//                    if (error is NotFoundException) {
//                        Alert ("activity.unavailable", null, "Error");
//                    }
//                });
//            } else {
//                if (obj.MustShowSummary) {
//                    _ready = false;
//                    SelectedPinVM = activityVm;
//                    ShowViewModel<ActivitySummaryViewModel>(
//                        new ActivitySummaryViewModel.Nav { Id = SelectedPinVM.Id }
//                    );
//                } else {
//                    //NumberOfNewInvitations++;
//                }
//            }
        }

        void OnPushNotificationReceived(PushNotificationMessage message)
        {
            ShowViewModel<PushNotificationViewModel>(new PushNotificationViewModel.Nav(message.Notification) );
            if (message.Notification.UpdatedBadgeCount.HasValue) {
                NumberOfNewNotifications = message.Notification.UpdatedBadgeCount.Value;
            }
        }

        protected override void OnClosing ()
        {
            Messenger.Unsubscribe<EventCreatedMessage>(_activityCreatedMessageToken);
            Messenger.Unsubscribe<EventUpdatedMessage>(_activityUpdatedMessageToken);
            Messenger.Unsubscribe<EventDeletedMessage>(_activityDeletedMessageToken);
            Messenger.Unsubscribe<UserLoggedOutMessage>(_userSignedOutToken);
            Messenger.Unsubscribe<InvitedToActivityMessage>(_invitedToActivityToken);
            Messenger.Unsubscribe<UserFiltersUpdatedMessage>(_filtersUpdatedMessageToken);
            Messenger.Unsubscribe<AppEnteredForegroundMessage>(_appEnteredForegroundMessageToken);
            Messenger.Unsubscribe<EventRemovedFromInvitationMessage>(_eventRemovedFromInvitationMessageToken);
            Messenger.Unsubscribe<PushNotificationMessage>(_pushNotificationMessageToken);
            Messenger.Unsubscribe<DisplayGroupMessage>(_showGroupVmMessageToken);
            Messenger.Unsubscribe<DisplayDetailActivityMessage>(_showActivityDetailsVmMessageToken);
            _isInstanceValid = false;
        }

        private void AddPrivateActivities (List<FitActivity> target, List<FitActivity> source)
        {
            source.ForEach(activity => {
                if (activity.IsPrivate.HasValue && activity.IsPrivate.Value)
                {
                    target.Add(activity);
                }
            });
        }


        private async Task<List<FitActivity>> GetVisibleActivitiesAroundMe(Coordinate coord, double radius, CancellationToken token)
        {
            List<FitActivity> activities;
            Mvx.Trace("!!!!!!!!!!!!!!!!!! making the call");
            activities = await _fitActivityService.SearchForActivities(coord, radius, token);
//            AddPrivateActivities(activities, await _fitActivityService.MyCreatedActivities(token));
//            AddPrivateActivities(activities, await _fitActivityService.MyInvitedActivities(token));
//            AddPrivateActivities(activities, await _fitActivityService.MyUpcomingActivities(token: token));
            return activities;
        }

        CancelableRequest updateActivities = new CancelableRequest();

        private async void UpdateActivityCollection ()
        {
            if (!_ready) { return; }

//            var previousCts = this.cts;
//            var newCts = CancellationTokenSource.CreateLinkedTokenSource(token);
//            this.cts = newCts;
//
//            if (previousCts != null)
//            {
//                Mvx.Trace("!!!!!!!!!!!!!!!!!! issuing a cancel");
//                // cancel the previous session and wait for its termination
//                previousCts.Cancel();
//                try { 
//                    Mvx.Trace("!!!!!!!!!!!!!!!!!! Waiting for it to cancel");
//                    await this.pendingTask; 
//                } catch { }
//                Mvx.Trace("!!!!!!!!!!!!!!!!!! canceled");
//            }

            Mvx.Trace("IS INSTANCE VALID {0}", _isInstanceValid);
            if (!_viewingSummary && _isInstanceValid) {
                try
                {
                    await updateActivities.ExecuteRequest(async (token, tryNum) => {
                        if (tryNum > 1) {
                            ProgressMessage = Localized("base.timed.out.retrying.progress.message");
                        }
                    //await ExecuteWebServiceRequestAsync(async () => {
                        double radius = CurrentRegion.Radius;
                        if (radius > 300.0) { radius = 300.0; }
                        if (radius < 5.0  ) { radius = 5.0;   }

                        List<FitActivity> activities = await GetVisibleActivitiesAroundMe(CurrentRegion.Center, radius, token);
                        var orderedList = activities.OrderByDescending(x => x.EventSchedule.StartTime).ToList();

                        Mvx.Trace("!!!!!!!!!!!!!!!!!! call done");

                        FitActivities.SyncCollection(orderedList);
                    });
                }
                catch (Exception ex)
                {
                    Mvx.Trace(ex.ToString());
                }
            }
        }

        private async void UpdateInvitationCount ()
        {
            if (!_ready) { return; }

            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => {
                    List<FitActivity> activities = await _fitActivityService.MyInvitedActivities(token);
                    NumberOfNewInvitations = activities.Count;
                });
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }

        #endregion
	}
}

