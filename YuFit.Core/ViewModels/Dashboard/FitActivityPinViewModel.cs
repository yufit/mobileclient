﻿//using System;
//using System.Linq;
//
//using Cirrious.CrossCore;
//using Cirrious.MvvmCross.ViewModels;
//
//using YuFit.Core.Interfaces.Services;
//using YuFit.Core.Models;
//using YuFit.Core.ViewModels.DisplayActivities;
//using YuFit.Core.ViewModels.Activity.Details;
//
//namespace YuFit.Core.ViewModels.Dashboard
//{
//    public class FitActivityPinViewModel : BaseViewModel
//    {
//        public const float EPSILON = 0.1f;
//
//        #region Properties
//
//        public string           Id              { get; set; }
//        public Coordinate       Location        { get; set; }
//        public ActivityType     ActivityType    { get; set; }
//        public bool             Invited         { get; set; }
//        public string           AvatarUrl       { get; set; }
//
//        public CommandViewModel ViewDetailsCommand { get; private set; }
//        #endregion
//
//        public FitActivityPinViewModel()
//        {
//            ViewDetailsCommand = new CommandViewModel(new MvxCommand(ViewFitActivity));
//            AvatarUrl = String.Empty;
//        }
//
//
//        #region Methods
//
//        void ViewFitActivity ()
//        {
//            ShowViewModel<ViewFitActivityDetailsViewModel>(new ViewFitActivityDetailsViewModel.Navigation { ActivityId = this.Id });
//        }
//
//        public bool IsEquals(FitActivityPinViewModel vm)
//        {
//            return 	Id == vm.Id &&
//                DisplayName == vm.DisplayName &&
//                Math.Abs(Location.Latitude - vm.Location.Latitude) < EPSILON &&
//                Math.Abs(Location.Longitude - vm.Location.Longitude) < EPSILON;
//        }
//
//        public FitActivityPinViewModel Copy(FitActivityPinViewModel vm)
//        {
//            Id = vm.Id;
//            DisplayName = vm.DisplayName;
//            Location = vm.Location;
//            Invited = vm.Invited;
//            AvatarUrl = vm.AvatarUrl;
//
//            return this;
//        }
//
//        public static FitActivityPinViewModel CreateFrom(FitActivity fitActivity)
//        {
//            FitActivityPinViewModel newActivity = new FitActivityPinViewModel
//            {
//                Id = fitActivity.Id,
//                DisplayName = fitActivity.Name,
//                Location = ((Location)fitActivity.Location).GeoCoordinates,
//                ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(activity => activity.TypeName == fitActivity.Sport),
//                Invited = Mvx.Resolve<IUserService>().AmIInvitedToThisActivity(fitActivity),
//                AvatarUrl = fitActivity.UserGroupAvatarUrl
//            };
//
//            return newActivity;
//        }
//
//        public void Reload (FitActivity fitActivity)
//        {
//            DisplayName = fitActivity.Name;
//            Location = ((Location)fitActivity.Location).GeoCoordinates;
//            ActivityType = Mvx.Resolve<IFitActivityService>().AvailableActivities.FirstOrDefault(activity => activity.TypeName == fitActivity.Sport);
//            Invited = Mvx.Resolve<IUserService>().AmIInvitedToThisActivity(fitActivity);
//            AvatarUrl = fitActivity.UserGroupAvatarUrl;
//        }
//
//        #endregion
//    }
//}
//
