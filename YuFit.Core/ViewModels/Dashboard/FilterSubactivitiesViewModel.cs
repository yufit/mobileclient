﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Models;

using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.ViewModels.Dashboard
{
    public class SubactivityViewModel : MvxViewModel
    {
        public string DisplayName { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
        public string ParentActivity { get; set; }
    };


    public class FilterSubactivitiesViewModel : BaseViewModel
    {
        private IEventFilter _eventFilters;
        private ISportFilter _sportFilter;

        public string TableLabelTitle { get; set; }

        public String Activity { get; set; }

        public CommandViewModel SubactivitySelectedCommand { get; private set; }

        public class Navigation
        {
            public string ActivityType { get; set; }
        };
            
        public ObservableCollection<SubactivityViewModel> Subactivities { get; private set; }

        public FilterSubactivitiesViewModel(IStringLoaderService stringService) : base("map.filters.choose.activities.title")
        {
            Subactivities = new ObservableCollection<SubactivityViewModel>();
            TableLabelTitle = Localized("map.filters.choose.activities.table.title");
            SubactivitySelectedCommand = new CommandViewModel(new MvxCommand<SubactivityViewModel>(SubactivitySelected));
        }

        /// <summary>
        /// Some assumption: This vm is always loaded from the MapFiltersViewModel.  Therefore
        /// _eventFilters.SportFilters will never be null.
        /// </summary>
        /// <param name="nav">Nav.</param>
        public void Init (Navigation nav)
        {
            Activity = nav.ActivityType;

            _eventFilters = Mvx.Resolve<ISandboxService>().GetItem<IEventFilter>("userFilters");
            _sportFilter = _eventFilters.SportFilters.FirstOrDefault(sf => sf.Sport == Activity);

            string subType = "YuFit.Core.Models.Activities." + Activity + "Type";
            Type t = Type.GetType(subType);
            foreach (var e in Enum.GetValues(t))
            {
                Subactivities.Add(new SubactivityViewModel {
                    DisplayName = Localized(e.ToString().ToLower()),
                    Name = e.ToString(),
                    IsSelected = false,
                    ParentActivity = Activity
                });
            }

            if (_sportFilter == null) {
                Subactivities.ForEach(sa => sa.IsSelected = true);
            } else {
                bool excluded = _sportFilter.IsExcluded.HasValue && _sportFilter.IsExcluded.Value;
                if ((_sportFilter == null || _sportFilter.SubSports == null) && !excluded) {
                    Subactivities.ForEach(sa => sa.IsSelected = true);
                } else if (_sportFilter.SubSports != null && !excluded) {
                    Subactivities.ForEach(sa => sa.IsSelected = _sportFilter.SubSports.Contains(sa.Name));
                }
            }
        }

        private void SubactivitySelected(SubactivityViewModel subactivity)
        {
            subactivity.IsSelected = !subactivity.IsSelected;
        }

        protected override void OnClosing ()
        {
            base.OnClosing();
            SaveIt();
        }

        private void SaveIt()
        {
            int selectionCount = Subactivities.Count(sa => sa.IsSelected);
            bool nothingSelected = selectionCount == 0;
            bool allSelected = selectionCount == Subactivities.Count;
            bool someItemsChecked = selectionCount != 0 && !allSelected;

            // Remove existing filter for that sport type.
            if (_sportFilter != null ) { _eventFilters.SportFilters.Remove(_sportFilter); }

            ISportFilter sportFilter = new SportFilter(Activity);
            sportFilter.IsExcluded = nothingSelected;

            if (someItemsChecked) {
                sportFilter.SubSports = new List<string>();
                Subactivities.Where(subactivity => subactivity.IsSelected).ForEach(a => sportFilter.SubSports.Add(a.Name));
            }

            _eventFilters.SportFilters.Add(sportFilter);

            Mvx.Resolve<ISandboxService>().SetItem("userFilters", _eventFilters);
            IMvxMessenger messenger = Mvx.Resolve<IMvxMessenger>();
            messenger.Publish(new UserFiltersUpdatedMessage(this, _eventFilters));
        }
    }
}

