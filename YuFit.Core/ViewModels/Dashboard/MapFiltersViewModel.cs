﻿using System;
using System.Collections.Generic;
using System.Linq;

using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Messages;
using YuFit.Core.Models;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Activity;

using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.ViewModels.Dashboard
{
    public class MapFiltersViewModel : BaseViewModel
    {
        readonly IUserService _userService;
        readonly IEventFilter _eventFilters;
        readonly MvxSubscriptionToken _filtersUpdatedMessageToken;

        public int WithinDays { 
            get { return _eventFilters.WithinDays; }
            set { _eventFilters.WithinDays = value; }
        }

        public int Radius { 
            get { return _eventFilters.Radius; }
            set { _eventFilters.Radius = value; }
        }

        public string ActivityFilterLabel { get; set; }
        public SilentObservableCollection<ActivityTypeCellViewModel> Activities { get; private set; }
        public string WithinDaysLabel { get; set; }
        public List<Tuple<string, object>> WithinDaysOptionsList { get; private set; }
        public CommandViewModel ShowSubActivitiesFilterCommand { get; private set; }

        public MapFiltersViewModel (IStringLoaderService stringLoader, IUserService userService) : base("map.filters.title")
        {
            _userService = userService;
            _filtersUpdatedMessageToken  = Messenger.Subscribe<UserFiltersUpdatedMessage>(OnFiltersUpdated);

            Activities = new SilentObservableCollection<ActivityTypeCellViewModel>();

            Mvx.Resolve<IFitActivityService>().AvailableActivities.Where(act => act.Enabled).ForEach(activity => 
                Activities.Add(new ActivityTypeCellViewModel {
                    TypeName = activity.TypeName,
                    IsEnabled = true,
                    IsSelected = false
                }
            ));

            _eventFilters = _userService.LoggedInUser.EventFilter;
            if (_eventFilters == null || _eventFilters.SportFilters == null || _eventFilters.SportFilters.Count == 0) {
                _eventFilters = EventFilter.Default();
            }

            // If new sports were added they won't be in the filter.  To make sure they are enabled,
            // we reset the filters.
            // TODO Find a better way to handle this.
            Activities.ForEach(activity => {
                var sportFilter = _eventFilters.SportFilters.Any(filter => filter.Sport == activity.TypeName);
                if (!sportFilter) {
                    _eventFilters.SportFilters.Add(new SportFilter(activity.TypeName));
                }
            });


            Mvx.Resolve<ISandboxService>().SetItem("userFilters", _eventFilters);

            ShowSubActivitiesFilterCommand = new CommandViewModel(new MvxCommand<ActivityTypeCellViewModel>(SelectActivitiesFilter));

            WithinDaysOptionsList = new List<Tuple<string, object>> { 
                new Tuple<string, object>(Localized("map.filters.time.values.day"), 1),
                new Tuple<string, object>(Localized("map.filters.time.values.2.days"), 2),
                new Tuple<string, object>(Localized("map.filters.time.values.4.days"), 4),
                new Tuple<string, object>(Localized("map.filters.time.values.week"), 7),
                new Tuple<string, object>(Localized("map.filters.time.values.2.weeks"), 14),
                new Tuple<string, object>(Localized("map.filters.time.values.4.weeks"), 28)
            };

            int[] options = {1, 2, 4, 7, 14, 28};
            if(!options.Contains(WithinDays)) {
                WithinDays = 28;
            }

            ActivityFilterLabel = Localized("map.filters.activities.label");
            WithinDaysLabel = Localized("map.filters.time.label");

            _eventFilters.SportFilters.ForEach(sport => Activities.Where(a => a.TypeName == sport.Sport && (!sport.IsExcluded.HasValue || (sport.IsExcluded.HasValue && !sport.IsExcluded.Value))).ForEach(a => a.IsSelected = true));
        }

        void SelectActivitiesFilter (ActivityTypeCellViewModel activityType)
        {
            ShowViewModel<FilterSubactivitiesViewModel>(
                    new FilterSubactivitiesViewModel.Navigation { ActivityType = activityType.TypeName });
        }

        void OnFiltersUpdated (UserFiltersUpdatedMessage obj)
        {
            WithinDays = obj.Filters.WithinDays;

            Activities.ForEach(activityvm => {
                var sportFilter = obj.Filters.SportFilters.FirstOrDefault(sf => sf.Sport == activityvm.TypeName);
                if (sportFilter != null) {
                    bool notExcluded = (!sportFilter.IsExcluded.HasValue) || sportFilter.IsExcluded.HasValue && !sportFilter.IsExcluded.Value;
                    activityvm.IsSelected = sportFilter != null && notExcluded;
                    //                    (obj.Filters.SportFilters.FirstOrDefault(sf => sf.Sport == activityvm.TypeName) != null &&
                    //                        obj.Filters.SportFilters.FirstOrDefault(sf => sf.Sport == activityvm.TypeName).SubSports.Count > 0);
                }
            });
        }

        protected override void OnClosing ()
        {
            base.OnClosing();
            Messenger.Unsubscribe<UserFiltersUpdatedMessage>(_filtersUpdatedMessageToken);
            SaveIt();
        }

        private async void SaveIt()
        {
            try {
                await ExecuteCancelableWebRequestAsync(async (token, tryCount) => await _userService.SetFilters(_eventFilters, token));
            } catch (Exception exception) {
                Mvx.Trace(exception.ToString());
            }
        }
    }
}

