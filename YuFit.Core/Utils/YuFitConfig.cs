﻿using System;

using YuFit.Core.Models;

using YuFit.WebServices.Interface;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Utils
{
    public class YuFitConfig : IYuFitConfig
    {
        public string           ClientVersionNumber { get; private set; }
        public Uri              ServiceEndpoint     { get; private set; }
        public ISessionStore    SessionStore        { get; private set; }
        public ILogger          Logger              { get; private set; }

        public YuFitConfig (ISessionStore sessionStore, ILogger logger, string clientVersionNumber)
        {
            SessionStore = sessionStore;
            ClientVersionNumber = clientVersionNumber;

            #if PRODUCTION
                ServiceEndpoint = new UriBuilder("https", "production.api.yu-fit.com", 8443).Uri;
            #elif STAGING
                ServiceEndpoint = new UriBuilder("https", "staging.api.yu-fit.com", 8443).Uri;
            #elif LOCAL
                ServiceEndpoint = new UriBuilder("http", "localhost", 3000).Uri;
            #endif

            Logger = logger;
        }

        public object Create(Type objectType)
        {
            if (objectType == typeof(ICoordinate))              { return new Coordinate();              }
            if (objectType == typeof(IFitEvent))                { return new FitActivity();             }
            if (objectType == typeof(IFitEventComment))         { return new FitActivityComment();      }
            if (objectType == typeof(IFitEventParticipant))     { return new FitActivityParticipant();  }
            if (objectType == typeof(IServiceIdentity))         { return new ServiceIdentity();         }
            if (objectType == typeof(IUser))                    { return new User();                    }
            if (objectType == typeof(IDeviceInformation))       { return new DeviceInformation();       }
            if (objectType == typeof(IUserProfile))             { return new UserProfile();             }
            if (objectType == typeof(ILocation))                { return new Location();                }
            if (objectType == typeof(Location))                 { return new Location();                }
            if (objectType == typeof(IEventSchedule))           { return new EventSchedule();           }
            if (objectType == typeof(ISportFilter))             { return new SportFilter();             }
            if (objectType == typeof(IEventFilter))             { return new EventFilter();             }
            if (objectType == typeof(INotification))            { return new NotificationRecord();      }
            if (objectType == typeof(IUserGroup))               { return new UserGroup();               }
            if (objectType == typeof(IGroupMember))             { return new GroupMember();             }
            if (objectType == typeof(INotificationPreference))  { return new NotificationPreference();  }

            return null;
        }

    }
}

