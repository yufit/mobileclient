﻿using YuFit.WebServices.Interface;
using MvvmCross.Plugins.File;
using MvvmCross.Platform;

namespace YuFit.Core.Utils
{
	public class SessionStore : ISessionStore
	{
		private readonly IMvxFileStore _fileStore;
		private readonly string _filePath;

		public SessionStore ()
		{
			_fileStore = Mvx.Resolve<IMvxFileStore>();
			_filePath = _fileStore.PathCombine("Session", "Session.txt");
			_fileStore.EnsureFolderExists("Session");
		}

		#region ISessionStore implementation

		public void StoreSessionInformation (string information)
		{
			Mvx.Trace ("info {0}", information);
			_fileStore.WriteFile(_filePath, information);   
		}

		public string LoadSessionInformation ()
		{
            string sessionData = string.Empty;
            if (_fileStore.TryReadTextFile(_filePath, out sessionData))
			{
                Mvx.Trace("Loaded {0}", sessionData);
			}
			return sessionData;
		}

		#endregion
	}
}

