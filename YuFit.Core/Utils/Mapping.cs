﻿using System;

namespace YuFit.Core.Utils
{
    /// <summary>
    /// Static mapping utility methods
    /// </summary>
    public static class Mapping
    {
        /// <summary>
        /// Converts miles to latitudinal degrees
        /// </summary>
        /// <returns>The latitude in degrees.</returns>
        /// <param name="miles">number of miles to convert</param>
        public static double MilesToLatitudeDegrees(double miles)
        {
            const double earthRadius = 3960.0; // in miles
            const double radiansToDegrees = 180.0 / Math.PI;
            return (miles/earthRadius) * radiansToDegrees;
        }

        public static double LatitudeDegreesToMiles(double lattitudeDelta)
        {
            const double earthRadius = 3960.0; // in miles
            const double radiansToDegrees = 180.0 / Math.PI;
            return (lattitudeDelta / radiansToDegrees) * earthRadius;
        }

        /// <summary>
        /// Converts miles to longitudinal degrees at a specified latitude
        /// </summary>
        /// <returns>The to longitude degrees.</returns>
        /// <param name="miles">Miles.</param>
        /// <param name="atLatitude">At latitude.</param>
        public static double MilesToLongitudeDegrees(double miles, double atLatitude)
        {
            const double earthRadius = 3960.0; // in miles
            const double degreesToRadians = Math.PI / 180.0;
            const double radiansToDegrees = 180.0 / Math.PI;

            // derive the earth's radius at that point in latitude
            double radiusAtLatitude = earthRadius * Math.Cos(atLatitude * degreesToRadians);
            return (miles / radiusAtLatitude) * radiansToDegrees;
        }


    }
}

