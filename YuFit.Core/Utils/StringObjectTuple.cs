using System;

namespace YuFit.Core.Utils
{
    public class StringObjectTuple : Tuple<string, object>
    {
        public StringObjectTuple(string text, object value) : base(text, value)
        {
        }
    }

}

