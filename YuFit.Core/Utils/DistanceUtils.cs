﻿using System;
using System.Collections.Generic;
using YuFit.Core.Interfaces.Services;
using MvvmCross.Platform;

namespace YuFit.Core.Utils
{
    public static class DistanceUtils
    {
        public static List<Tuple<string, object>> CreateLinearDistanceValues(
            string units, 
            int smallestDistance, 
            int largestDistance, 
            int increment, 
            bool includeNone = true)
        {
            List<Tuple<string, object>> values = new List<Tuple<string, object>>();

            if (includeNone && smallestDistance > 0)
            {
                IStringLoaderService stringLoader = Mvx.Resolve<IStringLoaderService>();
                values.Add(new StringObjectTuple(stringLoader.GetString("activity.distance.none"), 0));
            }

            if (smallestDistance == 1)
            {
                values.Add(new StringObjectTuple(string.Format("{0} {1}", smallestDistance, units), smallestDistance));
                smallestDistance = increment;
            }

            for (int i = smallestDistance; i <= largestDistance; i += increment)
            {
                values.Add(new StringObjectTuple(string.Format("{0} {1}", i, units), i));
            }

            return values;
        }
    }
}

