﻿using System;
using System.Collections.Generic;
using YuFit.Core.Interfaces.Services;
using MvvmCross.Platform;

namespace YuFit.Core.Utils
{
    public static class DurationUtils
    {
        public static List<Tuple<string, object>> CreateLinearDurationValues(int smallestDuration, int largestDuration, int increment, bool includeNone = true)
        {
            List<Tuple<string, object>> values = new List<Tuple<string, object>>();

            int hours = 0;
            int minutes = smallestDuration;
            int totalMinutes = smallestDuration;

            if (includeNone && smallestDuration > 0)
            {
                IStringLoaderService stringLoader = Mvx.Resolve<IStringLoaderService>();
                values.Add(new StringObjectTuple(stringLoader.GetString("activity.duration.none"), 0));
            }

            while (totalMinutes <= largestDuration)
            {
                string textVal = string.Format("{0:D2}:{1:D2} H", hours, minutes);
                values.Add(new StringObjectTuple(textVal, totalMinutes));

                minutes += increment;
                totalMinutes += increment;

                if (minutes >= 60)
                {
                    hours++;
                    minutes = 0;
                }
            }
            return values;
        }
    }
}

