﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using YuFit.WebServices.Exceptions;
using Xamarin;

namespace YuFit.Core.Utils
{
    public class CancelableRequest
    {
        CancellationTokenSource         _cancellationTokenSource    = null;
        Task                            _pendingTask                = null;
        readonly CancellationToken      _token                      = new CancellationToken(false);

        public bool IsInProgress { get { return _cancellationTokenSource != null; }}

        public async Task CancelRequest()
        {
            if (!IsInProgress) { return; }

            // Analysis disable EmptyGeneralCatchClause
            try {
                Mvx.Trace("!!!!!!!!!!!!!!!!!! issuing a cancel");
                _cancellationTokenSource?.Cancel();
                Mvx.Trace("!!!!!!!!!!!!!!!!!! Waiting for it to cancel");
                await _pendingTask;
                Mvx.Trace("!!!!!!!!!!!!!!!!!! done waiting canceled");
            } 
            catch (Exception /*ex*/) {
            } 
            // Analysis restore EmptyGeneralCatchClause
            Mvx.Trace("!!!!!!!!!!!!!!!!!! canceled");
        }

        public async Task ExecuteRequest(Func<CancellationToken, int, Task> operation)
        {
            const int retryCount = 3;
            int retryLeft = 3;
            bool done;

            do {
                try {
                    await CancelRequest();
                    _cancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(_token);

                    _pendingTask = operation(_cancellationTokenSource.Token, (retryCount-retryLeft) + 1); 
                    await _pendingTask;

                    Mvx.Trace("!!!!!!!!!!!!!!!!!! done waiting for task");
                    done = true;
                } 

                catch (ConnectionTimeoutException /*ex*/) { 
                    done = --retryLeft == 0;
                    Mvx.Trace("!!!!!!!!!!!!!!!!!! timedout: retry left {0}", retryLeft);
                }

                catch (TaskCanceledException /*taskCanceledException*/) {
                    // no-op.
                    Mvx.Trace("!!!!!!!!!!!!!!!!!! task canceled");
                    done = true;
                } 

                catch (Exception /*ex*/) {
                    // Stop trying and propagate exception up.
                    done = true;
                    throw;
                }

                finally {
                    _cancellationTokenSource?.Dispose();
                    _cancellationTokenSource = null;
                }
            } while(!done);

            if (retryLeft == 0) {
                throw new ConnectionTimeoutException();
            }
        }
    }
}

