﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using YuFit.Core.Extensions;

namespace YuFit.Core.Utils
{
    public class SilentObservableCollection<T> : ObservableCollection<T>
    {
        public bool IsSilent { get; set; }

        public void AddMultiple(List<T> items)
        {
            IsSilent = true;
            int startingIndex = Count;
            items.ForEach(Add);
            IsSilent = false;

            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, items, startingIndex));
        }

        public void RemoveMultiple(List<T> items)
        {
            IsSilent = true;
            int startingIndex = Count;
            items.ForEach(item => Remove(item));
            IsSilent = false;

            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, items, startingIndex));
        }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (!IsSilent) { base.OnCollectionChanged(e); }
        }
    }
}

