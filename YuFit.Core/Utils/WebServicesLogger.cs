﻿using System;
using YuFit.WebServices.Interface;
using MvvmCross.Platform;

namespace YuFit.Core.Utils
{
	public class WebServicesLogger : ILogger
	{
		public void LogDiagnostic(string message)
		{
            #if DEBUG
            Mvx.Trace(message);
            #endif
		}
	}
}

