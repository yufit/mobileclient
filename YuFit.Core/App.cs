using MvvmCross.Core;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;

namespace YuFit.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {

            Mvx.LazyConstructAndRegisterSingleton<IFileIoService, FileIoService>();
            Mvx.LazyConstructAndRegisterSingleton<ISandboxService, SandboxService>();
            Mvx.LazyConstructAndRegisterSingleton<IUserService, UserService>();
            Mvx.LazyConstructAndRegisterSingleton<IGroupService, GroupService>();

            Mvx.RegisterSingleton<IFitActivityService>(GetFitActivityService);
            Mvx.RegisterSingleton<ISandboxActivityCreationService>(GetFitActivityService);

            RegisterAppStart(new AppVm());
        }

        private FitActivityService FitActivityService = null;
        private FitActivityService GetFitActivityService()
        {
            if (FitActivityService == null)
            {
                FitActivityService = Mvx.IocConstruct<FitActivityService>();
            }
            return FitActivityService;
        }
    }
}
