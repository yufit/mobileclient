﻿using System;
using MvvmCross.Plugins.Messenger;
using YuFit.Core.Models;

namespace YuFit.Core.Messages
{
    public class UserProfileUpdatedMessage : MvxMessage {

        public User User { get; private set; }

        public UserProfileUpdatedMessage(object sender, User user) : base(sender) {
            User = user;
        }
    }
}

