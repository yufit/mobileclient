﻿using MvvmCross.Plugins.Messenger;
using YuFit.Core.Models;
using YuFit.WebServices.Interface.Model;


namespace YuFit.Core.Messages
{
    public class UserFiltersUpdatedMessage : MvxMessage {

        public IEventFilter Filters { get; private set; }

        public UserFiltersUpdatedMessage(object sender, IEventFilter filters) : base(sender) {
            Filters = filters;
        }
    }
}


