﻿using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.Messages
{
    public class UserNeedToAuthenticateMessage : MvxMessage {
        public UserNeedToAuthenticateMessage (object sender) : base(sender) {
        }
    }
}

