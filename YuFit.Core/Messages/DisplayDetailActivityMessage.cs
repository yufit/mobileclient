﻿using System;
using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.Messages
{
    public class DisplayDetailActivityMessage : MvxMessage
    {
        public string EventId { get; private set; }

        public DisplayDetailActivityMessage (object sender, string eventId)
            : base(sender)
        {
            EventId = eventId;
        }

    }
}

