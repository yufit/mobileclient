﻿using System;
using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.Messages
{
    public class DisplayGroupMessage : MvxMessage
    {
        public string GroupId { get; private set; }

        public DisplayGroupMessage (object sender, string groupId)
            : base(sender)
        {
            GroupId = groupId;
        }
    }
}

