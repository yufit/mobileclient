﻿using System;
using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.Messages
{
    public class AppEnteredForegroundMessage : MvxMessage {
        public AppEnteredForegroundMessage(object sender) : base(sender) {}
    }
}

