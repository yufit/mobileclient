﻿using MvvmCross.Plugins.Messenger;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Messages
{
    public class UserInfoReloadedMessage : MvxMessage {
        public IUser User { get; private set; }

        public UserInfoReloadedMessage (object sender, IUser user) : base(sender)
        {
            User = user;
        }
    }
}

