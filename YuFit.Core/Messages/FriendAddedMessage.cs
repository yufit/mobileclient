﻿using System;
using MvvmCross.Plugins.Messenger;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Messages
{
    public class FriendAddedMessage : MvxMessage 
    {
        public string UserId { get; private set; }

        public FriendAddedMessage(object sender, string userId) : base(sender) {
            UserId = userId;
        }
    }
}

