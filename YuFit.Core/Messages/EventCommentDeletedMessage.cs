﻿using System;
using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.Messages
{
    public class EventCommentDeletedMessage : MvxMessage
    {
        public string ActivityId { get; private set; }
        public string CommentId { get; private set; }

        public EventCommentDeletedMessage (object sender, string activityId, string commentId) : base(sender) 
        {
            ActivityId = activityId;
            CommentId = commentId;
        }
    }
}


