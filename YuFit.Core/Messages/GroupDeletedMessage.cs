﻿using MvvmCross.Plugins.Messenger;

using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Messages
{
    public class GroupDeletedMessage : MvxMessage {

        public string GroupId { get; private set; }

        public GroupDeletedMessage(object sender, string groupId) : base(sender) {
            GroupId = groupId;
        }
    }
}

