﻿using MvvmCross.Plugins.Messenger;
using YuFit.Core.Models;


namespace YuFit.Core.Messages
{
    public class SandboxedActivityUpdatedMessage : MvxMessage {

        public FitActivity Activity { get; private set; }

        public SandboxedActivityUpdatedMessage(object sender, FitActivity activity) : base(sender) {
            Activity = activity;
        }
    }
}


