﻿using MvvmCross.Plugins.Messenger;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Messages
{
    public class LeftGroupMessage : MvxMessage {

        public IUserGroup Group { get; private set; }

        public LeftGroupMessage(object sender, IUserGroup group) : base(sender) {
            Group = group;
        }
    }
}

