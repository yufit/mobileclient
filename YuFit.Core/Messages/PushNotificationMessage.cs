﻿using System;
using MvvmCross.Plugins.Messenger;
using YuFit.Core.Interfaces.Services;

namespace YuFit.Core.Messages
{
    public class PushNotificationMessage: MvxMessage {

        public IPushNotification Notification { get; set; }

        public PushNotificationMessage(object sender, IPushNotification notification) : base(sender) {
            Notification = notification;
        }
    }
}

