﻿using MvvmCross.Plugins.Messenger;
using YuFit.Core.Models;

namespace YuFit.Core.Messages
{
    public class EventUpdatedMessage : MvxMessage {

        public FitActivity Activity { get; private set; }

        public EventUpdatedMessage(object sender, FitActivity activity) : base(sender) {
            Activity = activity;
        }
    }
}

