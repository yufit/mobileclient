﻿using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.Messages
{
    public class EventRemovedFromInvitationMessage : MvxMessage {
        public EventRemovedFromInvitationMessage(object sender) : base(sender) {}
    }
}

