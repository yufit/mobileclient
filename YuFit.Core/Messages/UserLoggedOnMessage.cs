﻿using MvvmCross.Plugins.Messenger;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Messages
{
    public class UserLoggedOnMessage : MvxMessage {
        public IUser AuthenticatedUser { get; private set; }

        public UserLoggedOnMessage (object sender, IUser authUser) : base(sender)
        {
            AuthenticatedUser = authUser;
        }
    }
}

