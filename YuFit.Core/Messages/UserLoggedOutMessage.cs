﻿using MvvmCross.Plugins.Messenger;
using YuFit.Core.Models;

namespace YuFit.Core.Messages
{
	public class UserLoggedOutMessage : MvxMessage {
		public UserLoggedOutMessage (object sender) : base(sender)
		{
		}
	}
}

