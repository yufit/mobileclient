﻿using YuFit.Core.Models;
using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.Messages
{
    public class EventDeletedMessage : MvxMessage {

        public FitActivity Activity { get; private set; }

        public EventDeletedMessage(object sender, FitActivity activity) : base(sender) {
            Activity = activity;
        }
    }
}

