﻿using System;
using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.Messages
{
    public class DisplayUserProfileMessage : MvxMessage
    {
        public string UserId { get; private set; }

        public DisplayUserProfileMessage (object sender, string groupId)
            : base(sender)
        {
            UserId = groupId;
        }
    }
}

