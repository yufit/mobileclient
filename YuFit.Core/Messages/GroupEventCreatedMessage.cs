﻿using MvvmCross.Plugins.Messenger;


namespace YuFit.Core.Messages
{
    public class GroupEventCreatedMessage : MvxMessage {
        public string GroupId { get; set; }

        public GroupEventCreatedMessage(object sender, string groupId) : base(sender) {
            GroupId = groupId;
        }
    }
}

