﻿using System;
using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.Messages
{
    public class InvitedToActivityMessage : MvxMessage {

        public string ActivityId { get; private set; }
        public bool MustShowSummary { get; private set; }

        public InvitedToActivityMessage(object sender, string activityId, bool mustShowSummary = true) : base(sender) {
            ActivityId = activityId;
            MustShowSummary = mustShowSummary;
        }
    }
}

