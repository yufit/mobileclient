﻿using MvvmCross.Plugins.Messenger;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Messages
{
    public class JoinedGroupMessage : MvxMessage {

        public IUserGroup Group { get; private set; }

        public JoinedGroupMessage(object sender, IUserGroup group) : base(sender) {
            Group = group;
        }
    }
}

