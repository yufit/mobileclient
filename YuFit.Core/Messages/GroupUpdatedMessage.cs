﻿using MvvmCross.Plugins.Messenger;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Messages
{
    public class GroupUpdatedMessage : MvxMessage {

        public IUserGroup Group { get; private set; }

        public GroupUpdatedMessage(object sender, IUserGroup group) : base(sender) {
            Group = group;
        }
    }
}

