﻿using System;
using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.Messages
{
    public class GroupsUpdatedMessage : MvxMessage {

        public GroupsUpdatedMessage(object sender) : base(sender) {
        }
    }
}

