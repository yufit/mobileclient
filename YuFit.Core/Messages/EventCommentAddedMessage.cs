﻿using System;
using MvvmCross.Plugins.Messenger;
using YuFit.Core.Models;

namespace YuFit.Core.Messages
{
    public class EventCommentAddedMessage : MvxMessage
    {
        public string ActivityId { get; private set; }
        public FitActivityComment Comment { get; private set; }

        public EventCommentAddedMessage (object sender, string activityId, FitActivityComment comment) : base(sender) 
        {
            ActivityId = activityId;
            Comment = comment;
        }
    }
}


