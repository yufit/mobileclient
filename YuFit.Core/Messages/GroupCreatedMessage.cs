﻿using MvvmCross.Plugins.Messenger;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Messages
{
    public class GroupCreatedMessage : MvxMessage {

        public IUserGroup Group { get; private set; }

        public GroupCreatedMessage(object sender, IUserGroup group) : base(sender) {
            Group = group;
        }
    }
}

