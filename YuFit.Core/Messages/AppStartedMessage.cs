﻿using System;
using MvvmCross.Plugins.Messenger;

namespace YuFit.Core.Messages
{
    public class AppStartedMessage : MvxMessage {
        public AppStartedMessage(object sender) : base(sender) {}
    }
}

