﻿using System;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Models
{
    public class DeviceInformation : IDeviceInformation
    {
        public DeviceInformation ()
        {
        }

        #region IDeviceInformation implementation

        public string   DeviceId            { get; set; }
        public DeviceOs DeviceOs            { get; set; }
        public string   NotificationToken   { get; set; }

        #endregion
    }
}

