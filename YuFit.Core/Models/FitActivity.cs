﻿using System;
using System.Collections.Generic;
using YuFit.Core.Models.Activities;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Models
{
    public enum Intensity  { Easy = 0, Moderate = 1, High = 2, NoIntensity = -1 };

    /// <summary>
    /// Fit activity.  This is what gets sent and retrieve from the server.
    /// Our realized class add some extra helper accessors.
    /// </summary>
    public class FitActivity : IFitEvent
    {
        #region IFitEvent interface

        public string                   Id                  { get; set; }
        public DateTime                 CreatedAt           { get; set; }
        public IUser                    CreatedBy           { get; set; }
        public string                   UserGroupId         { get; set; }
        public string                   UserGroupName       { get; set; }
        public string                   UserGroupAvatarUrl  { get; set; }
        public DateTime                 LastUpdate          { get; set; }
        public string                   Visibility          { get; set; }
        public bool?                    IsCanceled          { get; set; }
        public bool?                    IsPrivate           { get; set; }

        // What
        public string                   Name                { get; set; }
        public string                   Description         { get; set; }
        public int?                     Duration            { get; set; }
        public int?                     Distance            { get; set; }
        public int?                     IntensityLevel      { get; set; }
        public string                   Sport               { get; set; }
        public string                   SubSport            { get; set; }

        // Where
        public ILocation                Location            { get; set; }

        // When
        public IEventSchedule           EventSchedule       { get; set; }

        // Who
        public IList<IFitEventParticipant> Participants     { get; set; }

        public DateTime?                PromotionStartingOn { get; set; }
        public Boolean?                 IsPromoted          { get; set; }

        public string                   IconUrl             { get; set; }
        public string                   EventPhotoUrl       { get; set; }

        public string                   WebsiteUrl          { get; set; }
        public string                   FacebookUrl         { get; set; }
        public int                      NumViews            { get; set; }

        #endregion

        #region Helper Data Properties

        // What
        private ActivitySettings _activitySettings;
        public ActivitySettings ActivitySettings {
            get {
                if (string.IsNullOrEmpty(Sport)) { return null; }

                const string ActSettingsVMFormat = "YuFit.Core.Models.Activities.{0}ActivitySettings";
                string className = string.Format(ActSettingsVMFormat, Sport);
                Type classType = Type.GetType(className);

                if (classType != null) {
                    if (_activitySettings == null || _activitySettings.GetType() != classType) {
                        _activitySettings = (ActivitySettings) Activator.CreateInstance(classType);
                        _activitySettings.LoadFrom(this);
                    }
                }

                return _activitySettings;
            }
            set {
                _activitySettings = value;
                _activitySettings.StoreInto(this);
            }
        }

        #endregion

        public FitActivity ()
        {
            Participants    = new ParticipantList();
            Location        = new Location();
            EventSchedule   = new EventSchedule();
        }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Name)) {
                Name = string.Format("{0} - {1}", Sport, SubSport);
            }

            // Server requires us not to send the list of participants.
            if (!string.IsNullOrEmpty(UserGroupId)) { Participants = null; }
        }

        #region Object current state properties

        /// <summary>
        /// Gets a value indicating whether this instance has what been specified.
        /// </summary>
        /// <value><c>true</c> if this instance has what been specified; otherwise, <c>false</c>.</value>
        public bool HasWhatBeenSpecified {
            get { return ActivitySettings != null && ActivitySettings.IsConfigurationValid(); }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has when been specified.
        /// </summary>
        /// <value><c>true</c> if this instance has when been specified; otherwise, <c>false</c>.</value>
        public bool HasWhenBeenSpecified {
            get { return ((EventSchedule) EventSchedule).IsValid; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has where been specified.
        /// </summary>
        /// <value><c>true</c> if this instance has where been specified; otherwise, <c>false</c>.</value>
        public bool HasWhereBeenSpecified {
            get { return ((Location) Location).IsValid; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has whom been specified.
        /// </summary>
        /// <value><c>true</c> if this instance has whom been specified; otherwise, <c>false</c>.</value>
        public bool HasWhomBeenSpecified {
            get { return true; }
        }

        public Intensity Intensity { 
            get { return IntensityLevel.HasValue ? (Intensity) IntensityLevel.Value : Intensity.NoIntensity; }
        }

        public bool IsReady {
            get { return HasWhatBeenSpecified && HasWhenBeenSpecified && HasWhereBeenSpecified && HasWhomBeenSpecified; }
        }

        public bool IsAPromotedEvent {
            get { return IsPromoted.HasValue ? IsPromoted.Value : false; }
        }

        #endregion
    }
}

