﻿using YuFit.WebServices.Interface.Model;
using System.Collections.Generic;
using System.Linq;

namespace YuFit.Core.Models
{
    public class FitActivityParticipant : User, IFitEventParticipant
    {
        #region IFitEventInvite implementation

        public string UserId { get; set; }
        public bool? Invited { get; set; }
        public bool? Going { get; set; }

        #endregion

        public bool IsGoing { get {
                return Going != null && Going == true;
            }
        }

        public bool IsNotGoing { get {
                return Going != null && Going == false;
            }
        }

        public bool IsInvited { get {
                return Invited != null && Invited == true;
            }
        }

        public bool IsNotInvited { get {
                return Invited != null && Invited == false;
            }
        }

        public bool HasNotAnswered { get {
                return Going == null;
            }
        }

        public FitActivityParticipant () {}
        public FitActivityParticipant (string userid) { UserId = userid; }
    }

    public class ParticipantList : List<IFitEventParticipant> {}
}

