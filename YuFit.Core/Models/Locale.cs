﻿using System;

namespace YuFit.Core.Models
{
    public enum LocalSystem
    {
        English,
        Metric
    }
}

