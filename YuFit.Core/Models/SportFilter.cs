﻿using System;
using System.Collections.Generic;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Models
{
    public class SportFilter : ISportFilter
    {
        #region ISportFilters implementation
        public bool?            IsExcluded  { get; set; }
        public string           Sport       { get; set; }
        public IList<string>    SubSports   { get; set; }

        #endregion

        public SportFilter()
        {
            // this is used during creation by reflection. 
            // dont set any values
        }

        public SportFilter (string activity)
        {
            Sport = activity;
            IsExcluded = false;
        }
    }
}

