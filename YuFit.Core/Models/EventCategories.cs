﻿using System;
using System.Collections.Generic;

namespace YuFit.Core.Models
{
    public class EventCategory
    {
        public IList<string> EventKinds { get; set; }

        public string   Name        { get; set; }
        public string   IconName    { get; set; }
        public Colors   Colors      { get; set; }
    }

    public class Color
    {
        public int  Red     { get; set; }
        public int  Green   { get; set; }
        public int  Blue    { get; set; }
    }

    public class Colors
    {
        public Color Base   { get; set; }
        public Color Light  { get; set; }
        public Color Dark   { get; set; }
    }
}

