﻿using YuFit.WebServices.Interface.Model;


namespace YuFit.Core.Models
{
	public class Coordinate : ICoordinate
	{
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public Coordinate ()
        {
            
        }
        public Coordinate(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public Coordinate(double[] location)
        {
            Longitude = location[0];
            Latitude = location[1];
        }

        public double[] ToArray()
        {
            return new [] {Longitude, Latitude};
        }
	}
}

