
namespace YuFit.Core.Models
{
    public class ActivityType
    {
        public string TypeName { get; set; }
        public int Index { get; set; }
        public bool Enabled { get; set; }
    }
}
