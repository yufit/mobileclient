﻿using System;

using YuFit.WebServices.Interface.Model;
using YuFit.Core.Interfaces.Services;

namespace YuFit.Core.Models
{
    // Server side notification object.  The yufit-api will return a list of them.
    // They represent notification that were pushed at some point.
    //
    // The Data part is platform specific.
    public class NotificationRecord : INotification
    {
        #region INotification implementation

        public string   Id          { get; set; }
        public dynamic  Data        { get; set; }
        public bool     IsAcked     { get; set; }
        public DateTime CreatedAt   { get; set; }

        #endregion

        public IPushNotification PushNotification { get; set; }
    }
}

