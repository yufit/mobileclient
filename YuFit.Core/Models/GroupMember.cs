﻿using YuFit.WebServices.Interface.Model;
using System;

namespace YuFit.Core.Models
{
    public class GroupMember : IGroupMember
    {
        #region IGroupMember implementation

        public string       Id          { get; set; }
        public IUser        User        { get; set; }
        public Boolean?     IsManager   { get; set; }

        #endregion
    }
}

