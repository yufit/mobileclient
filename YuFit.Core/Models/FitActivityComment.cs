﻿using System;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Models
{
    public class FitActivityComment : IFitEventComment
    {
        public string           Id          { get; set; }
        public string           Comment     { get; set; }
        public DateTime         UpdatedAt   { get; set; }
        public string           UserId      { get; set; }
        public string           UserName    { get; set; }
        public Uri              UserAvatar  { get; set; }
    }
}

