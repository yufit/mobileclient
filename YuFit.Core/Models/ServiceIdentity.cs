﻿using System;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Models
{
    public class ServiceIdentity : IServiceIdentity
    {
        #region IServiceIdentity implementation

        public ServiceType ServiceType { get; set; }
        public bool IsLoginIdentity { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        #endregion
    }
}

