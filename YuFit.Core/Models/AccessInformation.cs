﻿using System;

namespace YuFit.Core.Models
{
    public class AccessInformation
    {
        public const string Key = "AccessInformation";

        public int NumberOfAccesses { get; set; }
        public DateTime LastAccessed { get; set; }

        public bool IsFirstTimeUser() { return NumberOfAccesses == 0; }

        public void Update()
        {
            NumberOfAccesses++;
            LastAccessed = DateTime.Now;
        }
    }
}

