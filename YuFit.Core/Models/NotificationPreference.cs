﻿using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Models
{
    public class NotificationPreference : INotificationPreference
    {
        #region INotificationPreference implementation
        public string CategoryName { get; set; }
        public bool IsDisabled { get; set; }
        #endregion
    }
}

