﻿using System;
using System.Collections.Generic;
using YuFit.Core.Utils;
using System.Linq;

namespace YuFit.Core.Models.Activities
{
    internal static class DurationValues
    {
        public static List<Tuple<string, object>> Cycling;
        public static List<Tuple<string, object>> Running;
        public static List<Tuple<string, object>> Mountains;
        public static List<Tuple<string, object>> Swimming;
        public static List<Tuple<string, object>> Yoga;
        public static List<Tuple<string, object>> Fitness;

        static DurationValues()
        {
            Cycling = DurationUtils.CreateLinearDurationValues(15, 360, 15);
            Running = DurationUtils.CreateLinearDurationValues(15, 240, 15);
            Mountains = DurationUtils.CreateLinearDurationValues(15, 240, 15);
            Swimming = DurationUtils.CreateLinearDurationValues(15, 120, 5);
            Yoga = DurationUtils.CreateLinearDurationValues(15, 240, 5);
            Fitness = DurationUtils.CreateLinearDurationValues(15, 240, 5);
        }

        public static void Relocalize()
        {
            // nothing to do here yet.
        }

        public static List<Tuple<string, object>> ForSport(string sport)
        {
            switch (sport) {
                case "Cycling": return Cycling;
                case "Running": return Running;
                case "Swimming": return Swimming;
                case "Yoga": return Yoga;
                case "Fitness": return Fitness;
                case "Mountains": return Mountains;
                default: return new List<Tuple<string, object>>();
            }
        }

        public static string TextForValue(this List<Tuple<string, object>> list, object val)
        {
            return list.FirstOrDefault(tuple => tuple.Item2.Equals(val)).Item1;
        }
    }
}

