using System;

namespace YuFit.Core.Models.Activities
{
    public enum RunningIntensity  { Easy, Moderate, High };
    public enum RunningType       { Street, CrossCountry, Track };

    public class RunningActivitySettings : ActivitySettings
    {
        public new string       Sport              { get { return "Running"; } }
        public int              Duration           { get; set; }
        public int              Distance           { get; set; }
        public RunningIntensity IntensityLevel     { get; set; }
        public RunningType      RunningType        { get; set; }

        public float Speed { 
            get { 
                return (Duration == 0 || Distance == 0) ? 0 : ((float) Duration / (float) Distance); 
            } 
        }

        public RunningActivitySettings ()
        {
            Duration        = 120;
            Distance        = 20;
            IntensityLevel  = RunningIntensity.Easy;
            RunningType     = RunningType.Street;
        }

        public override bool IsConfigurationValid() { return true; }

        public override void LoadFrom(FitActivity activity)
        {
            Duration = activity.Duration ?? 0;
            Distance = activity.Distance ?? 0;
            IntensityLevel = (RunningIntensity) activity.IntensityLevel;
            if (!string.IsNullOrEmpty(activity.SubSport)) {
                RunningType = (RunningType) Enum.Parse(typeof(RunningType), activity.SubSport);
            }
        }

        public override void StoreInto(FitActivity activity)
        {
            activity.Duration = Duration;
            activity.Distance = Distance;
            activity.IntensityLevel = (int?) IntensityLevel;
            activity.SubSport = RunningType.ToString();
            activity.Sport = Sport;
        }

    }
    
}
