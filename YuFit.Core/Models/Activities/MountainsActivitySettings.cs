﻿using System;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using YuFit.Core.Interfaces.Services;

namespace YuFit.Core.Models.Activities
{
    public enum MountainsIntensity { Easy, Moderate, High };
    public enum MountainsType      { SnowShoes, Hicking, CrosscountrySkiingClassic, CrosscountrySkiingSkate };

    public class MountainsActivitySettings : ActivitySettings
    {
        public new string           Sport              { get { return "Mountains"; } }
        public int                  Duration           { get; set; }
        public int                  Distance           { get; set; }
        public MountainsIntensity   IntensityLevel     { get; set; }
        public MountainsType        MountainsType        { get; set; }

        public float Speed { 
            get { 
                return (Duration == 0 || Distance == 0) ? 0 : Distance / (Duration / 60.0f); 
            } 
        }

        public MountainsActivitySettings ()
        {
            Duration        = 60;
            Distance        = 60;
            IntensityLevel  = MountainsIntensity.Easy;
            MountainsType   = MountainsType.SnowShoes;
        }

        public override bool IsConfigurationValid() { return true; }

        public override void LoadFrom(FitActivity activity)
        {
            Duration = activity.Duration ?? 0;
            Distance = activity.Distance ?? 0;
            IntensityLevel = (MountainsIntensity) activity.IntensityLevel;

            if (!string.IsNullOrEmpty(activity.SubSport)) {
                MountainsType = (MountainsType) Enum.Parse(typeof(MountainsType), activity.SubSport);
            }
        }

        public override void StoreInto(FitActivity activity)
        {
            activity.Sport = Sport;
            activity.Duration = Duration;
            activity.Distance = Distance;
            activity.IntensityLevel = (int?) IntensityLevel;
            activity.SubSport = MountainsType.ToString();
        }

    }

}
