using System;

namespace YuFit.Core.Models.Activities
{
    public enum CyclingIntensity { Easy, Moderate, High };
    public enum CyclingType      { Road, Mtb, Cyclocross, TT, Track, Fixedgear, Fatbike, Spinning };

    public class CyclingActivitySettings : ActivitySettings
    {
        public new string       Sport              { get { return "Cycling"; } }
        public int              Duration           { get; set; }
        public int              Distance           { get; set; }
        public CyclingIntensity IntensityLevel     { get; set; }
        public CyclingType      CyclingType        { get; set; }

        public float Speed { 
            get { 
                return (Duration == 0 || Distance == 0) ? 0 : Distance / (Duration / 60.0f); 
            } 
        }

        public CyclingActivitySettings ()
        {
            Duration        = 60;
            Distance        = 60;
            IntensityLevel  = CyclingIntensity.Easy;
            CyclingType     = CyclingType.Mtb;
        }

        public override bool IsConfigurationValid() { return true; }

        public override void LoadFrom(FitActivity activity)
        {
            Duration = activity.Duration ?? 0;
            Distance = activity.Distance ?? 0;
            IntensityLevel = (CyclingIntensity) activity.IntensityLevel;

            if (!string.IsNullOrEmpty(activity.SubSport)) {
                CyclingType = (CyclingType) Enum.Parse(typeof(CyclingType), activity.SubSport);
            }
        }

        public override void StoreInto(FitActivity activity)
        {
            activity.Sport = Sport;
            activity.Duration = Duration;
            activity.Distance = Distance;
            activity.IntensityLevel = (int?) IntensityLevel;
            activity.SubSport = CyclingType.ToString();
        }

    }

}
