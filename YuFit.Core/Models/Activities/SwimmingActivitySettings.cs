using System;

namespace YuFit.Core.Models.Activities
{
    public enum SwimmingIntensity  { Easy, Moderate, High };
    public enum SwimmingType       { OpenWater, Pool };

    public class SwimmingActivitySettings : ActivitySettings
    {
        public new string        Sport              { get { return "Swimming"; } }
        public int               Duration           { get; set; }
        public int               Distance           { get; set; }
        public SwimmingIntensity IntensityLevel     { get; set; }
        public SwimmingType      SwimmingType       { get; set; }

        public SwimmingActivitySettings ()
        {
            Duration        = 60;
            Distance        = 1000;
            IntensityLevel  = SwimmingIntensity.Easy;
            SwimmingType    = SwimmingType.OpenWater;
        }

        public override bool IsConfigurationValid() { return true; }

        public override void LoadFrom(FitActivity activity)
        {
            Duration = activity.Duration ?? 0;
            Distance = activity.Distance ?? 0;
            IntensityLevel = (SwimmingIntensity) activity.IntensityLevel;
            if (!string.IsNullOrEmpty(activity.SubSport)) {
                SwimmingType = (SwimmingType) Enum.Parse(typeof(SwimmingType), activity.SubSport);
            }
        }

        public override void StoreInto(FitActivity activity)
        {
            activity.IntensityLevel = (int?) IntensityLevel;
            activity.SubSport = SwimmingType.ToString();
            activity.Duration = Duration;
            activity.Distance = Distance;
            activity.Sport = Sport;
        }
    }
    
}
