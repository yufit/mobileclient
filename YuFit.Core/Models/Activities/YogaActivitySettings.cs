using System;

namespace YuFit.Core.Models.Activities
{
    /// <summary>
    /// Yoga intensity.
    /// </summary>
    public enum YogaIntensity  { Easy, Moderate, High };

    /// <summary>
    /// Yoga type.
    /// </summary>
    public enum YogaType { 
        Ashtanga, Bikram, ChildrenYoga, HathaYoga, Kundalini, PowerYoga, 
        HotYoga, Moksha, PrePostNatalYoga, Sup, TherapeuticYoga, ViniYoga, Vinyasa, 
        Yin, Yogalate, Yogaball, Yoga, Pilates
    };

    /// <summary>
    /// Yoga activity settings.
    /// </summary>
    public class YogaActivitySettings : ActivitySettings
    {
        public new string       Sport              { get { return "Yoga"; } }
        public int              Duration           { get; set; }
        public YogaIntensity    IntensityLevel     { get; set; }
        public YogaType         YogaType           { get; set; }

        public YogaActivitySettings ()
        {
            Duration        = 60;
            IntensityLevel  = YogaIntensity.Easy;
            YogaType        = YogaType.Yoga;
        }

        public override bool IsConfigurationValid() { return true; }

        public override void LoadFrom(FitActivity activity)
        {
            Duration = activity.Duration ?? 0;
            IntensityLevel = (YogaIntensity) activity.IntensityLevel;
            if (!string.IsNullOrEmpty(activity.SubSport)) {
                YogaType = (YogaType) Enum.Parse(typeof(YogaType), activity.SubSport);
            }
        }

        public override void StoreInto(FitActivity activity)
        {
            activity.Duration = Duration;
            activity.IntensityLevel = (int?) IntensityLevel;
            activity.SubSport = YogaType.ToString();
            activity.Sport = Sport;
        }
    }
    
}
