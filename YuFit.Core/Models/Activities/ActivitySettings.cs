
namespace YuFit.Core.Models.Activities
{
    public enum ActivityKind { Cycling, Running, Swimming, Yoga, Mountains, Fitness };

//    public enum RepeatInterval { 
//        Never, 
//        Daily, 
//        Weekly, 
//        BiWeekly, 
//        Monthly
//    };

    public class ActivitySettings {
        public string Sport { get { return string.Empty; } }

        public virtual void LoadFrom(FitActivity activity) {}
        public virtual void StoreInto(FitActivity activity) {}
        public virtual bool IsConfigurationValid() { return false; }
    }

}
