﻿using System;
using System.Collections.Generic;
using YuFit.Core.Utils;
using System.Linq;

namespace YuFit.Core.Models.Activities
{
    internal static class DistanceValues
    {
        public static List<Tuple<string, object>> Cycling { get; private set; }
        public static List<Tuple<string, object>> Running { get; private set; }
        public static List<Tuple<string, object>> Mountains { get; private set; }
        public static List<Tuple<string, object>> Swimming { get; private set; }
        public static LocalSystem System { get; private set; }

        static DistanceValues()
        {
            System = LocalSystem.Metric;  
            Initialize();
        }

        private static void Initialize()
        {
            Cycling = DistanceUtils.CreateLinearDistanceValues(System == LocalSystem.Metric ? "km" : "miles", 1, 200, 5);
            Running = DistanceUtils.CreateLinearDistanceValues(System == LocalSystem.Metric ? "km" : "miles", 1, 42, 1);
            Mountains = DistanceUtils.CreateLinearDistanceValues(System == LocalSystem.Metric ? "km" : "miles", 1, 42, 1);
            Swimming = DistanceUtils.CreateLinearDistanceValues(System == LocalSystem.Metric ? "m" : "yards", 0, 5000, 25);
        }

        public static void Relocalize(LocalSystem system)
        {
            System = LocalSystem.Metric;  
            Initialize();
        }

        public static List<Tuple<string, object>> ForSport(string sport)
        {
            switch (sport) {
                case "Cycling": return Cycling;
                case "Running": return Running;
                case "Swimming": return Swimming;
                default: return new List<Tuple<string, object>>();
            }
        }
    }
}

