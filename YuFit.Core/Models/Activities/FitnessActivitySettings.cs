﻿using System;

namespace YuFit.Core.Models.Activities
{
    public enum FitnessIntensity  { Easy, Moderate, High };
    public enum FitnessType       { 
        Aerobic, CrossTraining, KeltelBell, StrengthTraining, Crossfit, Bootcamp
    };

    public class FitnessActivitySettings : ActivitySettings
    {
        public new string       Sport              { get { return "Fitness"; } }
        public int              Duration           { get; set; }
        public FitnessIntensity IntensityLevel     { get; set; }
        public FitnessType      FitnessType        { get; set; }

        public FitnessActivitySettings ()
        {
            Duration        = 120;
            IntensityLevel  = FitnessIntensity.Easy;
            FitnessType     = FitnessType.Aerobic;
        }

        public override bool IsConfigurationValid() { return true; }

        public override void LoadFrom(FitActivity activity)
        {
            Duration = activity.Duration ?? 0;
            IntensityLevel = (FitnessIntensity) activity.IntensityLevel;
            if (!string.IsNullOrEmpty(activity.SubSport)) {
                FitnessType = (FitnessType) Enum.Parse(typeof(FitnessType), activity.SubSport);
            }
        }

        public override void StoreInto(FitActivity activity)
        {
            activity.Duration = Duration;
            activity.IntensityLevel = (int?) IntensityLevel;
            activity.SubSport = FitnessType.ToString();
            activity.Sport = Sport;
        }

    }

}
