﻿using System;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Models
{
    public class UserGroup : IUserGroup
    {
        #region IUserGroup implementation
        public string       Id                          { get; set; }
        public string       Name                        { get; set; }
        public string       Description                 { get; set; }
        public Uri          Website                     { get; set; }
        public string       Type                        { get; set; }
        public string       SportKind                   { get; set; }
        public DateTime     CreatedAt                   { get; set; }
        public DateTime     UpdatedAt                   { get; set; }
        public Uri          GroupAvatar                 { get; set; }
        public Uri          GroupPhoto                  { get; set; }
        public IUser        CreatedBy                   { get; set; }
        public Boolean?     Private                     { get; set; }
        public ILocation    Location                    { get; set; }
        public Boolean?     IsCurrentUserManager        { get; set; }
        public int          MemberCount                 { get; set; }

        public Boolean?     IsCurrentUserAMember        { get; set; }
        public Boolean?     IsCurrentUserPending        { get; set; }
        public string       CurrentMemberUserMemberId   { get; set; }

        public Boolean?     Plus                        { get; set; }
        public string       PlusSubscriptionLevel       { get; set; }
        public DateTime     PlusExpiresAt               { get; set; }

        #endregion

        public UserGroup ()
        {
        }
    }
}

