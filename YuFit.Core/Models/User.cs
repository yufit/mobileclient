﻿using System;
using System.Collections.Generic;
using System.Linq;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Models
{
    public class User : IUser
    {
        public const string HomeLocationId      = "Home";
        public const string CurrentLocationId   = "Current";

        public string                       Id                      { get; set; }
        public string                       Name                    { get; set; }
        public string                       Email                   { get; set; }
        public IUserProfile                 UserProfile             { get; set; }
        public IList<IDeviceInformation>    Devices                 { get; set; }
        public IList<IServiceIdentity>      ServiceIdentities       { get; set; }
        public DateTime                     CreatedAt               { get; set; }
        public DateTime                     UpdatedAt               { get; set; }
        public IList<ILocation>             Locations               { get; set; }
        public IEventFilter                 EventFilter             { get; set; }
        public int?                         CreatedEventsCount      { get; set; }
        public IList<IFitEvent>             ShortUpcomingEventList  { get; set; }
        public bool                         IsFriend                { get; set; }

        public IServiceIdentity LogonIdentity {
            get { return ServiceIdentities.FirstOrDefault(si => si.IsLoginIdentity); }
        }

        public string FirstName { get { return Name; } }
        public string LastName  { get { return Name; } }

        public void SetCurrentLocation(ILocation location) {
            SetLocation(CurrentLocationId, location);
        }

        public void SetHomeLocation(ILocation location) {
            SetLocation(HomeLocationId, location);
        }

        public Location GetHomeLocation() {
            return (Location) GetLocation(HomeLocationId);
        }

        public void SetLocation(string id, ILocation location) {
            if (Locations == null) {Locations = new List<ILocation>();}

            var currentLocation = GetLocation(id);
            if (currentLocation != null) {
                Locations.Remove(currentLocation);
            }
            location.Name = id;
            Locations.Add(location);
        }

        public ILocation GetLocation(string id) {
            if (Locations == null) {return null;}
            return Locations.FirstOrDefault(l => l.Name.Equals(id));
        }
    }
}

