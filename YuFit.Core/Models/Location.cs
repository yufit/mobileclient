﻿using System;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Model;

namespace YuFit.Core.Models
{
    public class Location : ILocation
    {
        #region ILocation implementation

        public string   Id              { get; set; }
        public string   Name            { get; set; }
        public string   StreetAddress   { get; set; }
        public string   City            { get; set; }
        public string   StateProvince   { get; set; }
        public string   Country         { get; set; }
        public double[] Coordinates     { get; set; }

        #endregion

        public Coordinate GeoCoordinates {
            get { return Coordinates != null ? new Coordinate(Coordinates) : null; }
            set { Coordinates = value.ToArray(); }
        }

        public Location ()
        {
            
        }
        public override string ToString ()
        {
            return string.Format ("{0}, {1}, {2}, {3}]", StreetAddress, City, StateProvince, Country);
        }

        public string FullAddress ()
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(StreetAddress)) {
                result += StreetAddress;
            }

            if (!string.IsNullOrEmpty(City)) {
                if (!string.IsNullOrEmpty(result)) {
                    result += ",";
                }

                result += City;
            }

            if (!string.IsNullOrEmpty(StateProvince)) {
                if (!string.IsNullOrEmpty(result)) {
                    result += ", ";
                }

                result += StateProvince;
            }

            return result;
        }


        public string CityStateString ()
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(City)) {
                result += City;
            }

            if (!string.IsNullOrEmpty(StateProvince)) {
                if (!string.IsNullOrEmpty(result)) {
                    result += ", ";
                }

                result += StateProvince;
            }

            if (!string.IsNullOrEmpty(Country)) {
                if (!string.IsNullOrEmpty(result)) {
                    result += ", ";
                }

                result += Country;
            }
            return result;
        }

        public bool IsValid {
            get { return Coordinates != null; }
        }
    }
}

