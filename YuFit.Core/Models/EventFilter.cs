﻿using System;
using System.Collections.Generic;
using System.Linq;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;
using YuFit.Core.Extensions;
using YuFit.Core.Interfaces.Services;
using YuFit.WebServices.Interface.Model;

namespace YuFit.Core.Models
{
    public class EventFilter : IEventFilter
    {
        #region IEventFilters implementation

        public int                  Radius          { get; set; }
        public int                  WithinDays      { get; set; }
        public IList<ISportFilter>  SportFilters    { get; set; }

        #endregion

        public class SubActivities : List<string> {}
        public class Activities : Dictionary<string, SubActivities> {}

        public DateTime FromTime { set; get; }
        public DateTime ToTime { set; get; }

        public List<DayOfWeek> DaysOfTheWeek { get; set; }
        public Activities SelectedActivities { get; set; }

        public static EventFilter Default()
        {
            EventFilter filters = new EventFilter();
            filters.Radius = 50; 
            filters.WithinDays = 28;

            filters.SportFilters = new List<ISportFilter>();

            Mvx.Resolve<IFitActivityService>().AvailableActivities.Where(a => a.Enabled)
                .ForEach(activity => 
                    filters.SportFilters.Add(new SportFilter(activity.TypeName)));
            return filters;
        }

    }
}

