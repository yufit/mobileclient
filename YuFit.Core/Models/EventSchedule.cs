﻿using System;
using YuFit.WebServices.Interface.Model;
using YuFit.WebServices.Model;

namespace YuFit.Core.Models
{
    public class EventSchedule : IEventSchedule
    {
        #region IEventSchedule implementation

        public string           Id                  { get; set; }
        public DateTime         StartTime           { get; set; }
        public EventRecurrence  Recurrence          { get; set; }
        public int?             Interval            { get; set; }
        public DateTime?        NextOccurence       { get; set; }
        public int?             NumberOfOccurences  { get; set; }
        public DateTime?        ExpiresAt           { get; set; }

        #endregion

        public DateTime LocalStartTime { 
            get { return StartTime.ToLocalTime(); } 
            set { StartTime = value.ToUniversalTime(); }
        }

        public bool IsValid {
            get { return LocalStartTime > DateTime.Now; }
        }
    }
}

