﻿using System;

using YuFit.Core.Interfaces.Services;
using YuFit.Core.Utils;
using YuFit.Core.ViewModels.Dashboard;
using YuFit.Core.ViewModels.Login;

using YuFit.WebServices;
using YuFit.WebServices.Interface;
using YuFit.Core.Models;
using YuFit.Core.ViewModels;
using YuFit.Core.ViewModels.Welcome;
using YuFit.Core.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using Acr.UserDialogs;

namespace YuFit.Core
{
	/// <summary>
	/// Our own implementation of the IMvxAppStart.  This takes care of showing
	/// the login screen if required.
	/// </summary>
    public class AppVm : MvxNavigatingObject, IMvxAppStart
    {
        public void Start(object hint = null)
        {
            ISessionStore sessionStore = new SessionStore ();
            ILogger logger = new WebServicesLogger();
            //IYuFitConfig config = new YuFitConfig(sessionStore, logger, (bool)(hint ?? false));
            IYuFitConfig config = new YuFitConfig(sessionStore, logger, (string) hint);

            Mvx.RegisterSingleton<ISessionStore>(sessionStore);
            Mvx.RegisterSingleton<ILogger>(logger);
            Mvx.RegisterSingleton<IYuFitConfig>(config);
            Mvx.LazyConstructAndRegisterSingleton<IYuFitClient, YuFitClient>();
            Mvx.RegisterSingleton<IDeviceTrackerService>(Mvx.IocConstruct<DeviceTrackerService>());
            Mvx.RegisterSingleton (() => UserDialogs.Instance);

            IFileIoService fileStore = Mvx.Resolve<IFileIoService>();
            var accessInfo = fileStore.Read<AccessInformation>(AccessInformation.Key);

            if (accessInfo.IsFirstTimeUser()) {
                ShowViewModel<WelcomeViewModel>();
            } else {
                IUserService authService = Mvx.Resolve<IUserService>();
                if (authService.NeedsAuthentication) {
                    ShowViewModel<LoginViewModel>();
                } else {
                    ShowViewModel<DashboardViewModel>();
                }
            }
        }
    }
}

