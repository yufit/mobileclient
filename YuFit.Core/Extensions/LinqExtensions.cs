using System.Collections.Generic;
using System.Linq;
using System;

namespace YuFit.Core.Extensions
{

    public static class LinqExtensions
    {
        // Analysis disable PossibleMultipleEnumeration
        public static IEnumerable<ElementWithContext<T>> 
        WithContext<T>(this IEnumerable<T> source)
        {
            T previous = default(T);
            T current = source.FirstOrDefault();

            foreach (T next in source.Union(new[] { default(T) }).Skip(1))
            {
                yield return new ElementWithContext<T>(current, previous, next);
                previous = current;
                current = next;
            }
        }

        public static void Each<T>( this IEnumerable<T> ie, Action<T, int> action )
        {
            var i = 0;
            foreach ( var e in ie ) action( e, i++ );
        }
        // Analysis restore PossibleMultipleEnumeration
    }
}
