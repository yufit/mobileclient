﻿using System;
using YuFit.Core.Interfaces.Services;
using YuFit.Core.Models;

namespace YuFit.Core.Extensions
{
    public static class PlacemarkExtension
    {
        public static Location ToLocation(this Placemark placemark)
        {
            var location = new Location {
                City            = placemark.Locality,
                Country         = placemark.Country,
                StreetAddress   = placemark.SubThoroughfare + ", " + placemark.Thoroughfare,
                StateProvince   = placemark.AdministrativeArea,
                Coordinates     = placemark.Coordinates.ToArray()
            };
            return location;
        }
    }
}

