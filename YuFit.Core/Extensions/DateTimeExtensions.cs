﻿using System;

namespace YuFit.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsToday(this DateTime value)
        {
            return (value.Year == DateTime.Now.Year &&
            value.Month == DateTime.Now.Month &&
            value.Day == DateTime.Now.Day);
        }
    }
}

