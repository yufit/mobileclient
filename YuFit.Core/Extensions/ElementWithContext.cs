
namespace YuFit.Core.Extensions
{
    /// <summary>
    /// This is used by the LinqExtension module.  It enables us to walk through
    /// an enumerator using next and previous.
    /// </summary>
    public class ElementWithContext<T>
    {
        public T Previous { get; private set; }
        public T Next { get; private set; }
        public T Current { get; private set; }

        public ElementWithContext(T current, T previous, T next)
        {
            Current = current;
            Previous = previous;
            Next = next;
        }
    }
    
}
