// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Coc.ImagePicker.Touch.Test
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView PictureView { get; set; }

		[Action ("UIButton3_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void UIButton3_TouchUpInside (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (PictureView != null) {
				PictureView.Dispose ();
				PictureView = null;
			}
		}
	}
}
