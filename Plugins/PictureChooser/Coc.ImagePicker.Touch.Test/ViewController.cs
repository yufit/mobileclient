﻿using System;

using UIKit;
using CoreGraphics;

namespace Coc.ImagePicker.Touch.Test
{
    public partial class ViewController : UIViewController
    {
        public ImagePicker _imagePicker;

        public UIImage Image {
            get { return PictureView.Image; }
            set { PictureView.Image = value; }
        }
        public ViewController (IntPtr handle) : base(handle)
        {
        }

        partial void UIButton3_TouchUpInside (UIButton sender)
        {
            var icov = new ImageCropOverlayView();
            icov.CropSize = new CGSize(View.Frame.Width, 200);

//            var icov = new ImageDiamondCropOverlayView();//ImageCropOverlayView();
//            icov.CropSize = new CGSize(200, 200);
            icov.CropRect = new CGRect(100, 100, 200, 200);
            _imagePicker = new ImagePicker(icov);
            //_imagePicker.CropSize = new CGSize(View.Frame.Width, 200);
            _imagePicker.Delegate = new PickerDelegate(this);

            PresentViewController(_imagePicker.ImagePickerController, true, null);
        }

        public void HidePicker()
        {
            _imagePicker.DismissController();
            _imagePicker.Delegate = null;
        }
    }

    public class PickerDelegate : ImagePickerDelegate
    {
        WeakReference _controller;
        ViewController ViewController {
            get { return (ViewController) _controller.Target; }
            set { _controller = new WeakReference(value);}
        }

        public PickerDelegate (ViewController viewController)
        {
            ViewController = viewController;
        }

        #region ImagePickerDelegate implementation

        public void ImagePicked (ImagePicker picker, UIImage pickedImage)
        {
            UIGraphics.BeginImageContext(new CGSize(200, 200));
            pickedImage.Draw(new CGRect(0, 0, 200, 200));

            ViewController.Image = UIGraphics.GetImageFromCurrentImageContext();;
            ViewController.HidePicker();
            UIGraphics.EndImageContext();
        }

        public void ImagePickedDidCancel (ImagePicker picker)
        {
            ViewController.HidePicker();
        }

        #endregion

//        -(UIImage*)scaleToSize:(CGSize)size
//        {
//            // Create a bitmap graphics context
//            // This will also set it as the current context
//            UIGraphicsBeginImageContext(size);
//
//            // Draw the scaled image in the current context
//            [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
//
//            // Create a new image from current context
//            UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
//
//            // Pop the current context from the stack
//            UIGraphicsEndImageContext();
//
//            // Return our new scaled image
//            return scaledImage;
//        }

    }
}

