﻿using System;
using UIKit;
using System.Drawing;
using CoreGraphics;

namespace Coc.ImagePicker.Touch
{
    public interface ImageCropControllerDelegate
    {
        void FinishedCroppingImage(ImageCropViewController controller, UIImage croppedImage);
    }

    public class ImageCropViewController : UIViewController
    {
        public UIImage SourceImage { get; set; }
        public ImageCropControllerDelegate Delegate { get; set; }
        public CGSize CropSize { get; set; }

        public ImageCropView ImageCropView { get; set; }

        readonly UIToolbar _toolbar = new UIToolbar(CGRect.Empty);
        readonly UIButton _useButton = new UIButton();
        readonly UIButton _cancelButton = new UIButton();

        ImageCropOverlayView _overlayView;

        private UIImage _croppedImage;

        public string ViewTitle {
            get { return Title; }
            set { Title = value; }
        }

        private string _useText = "use";
        public string UseText {
            get { return _useText; }
            set { _useText = value; _useButton.SetTitle(value, UIControlState.Normal); }
        }

        private string _cancelText = "cancel";
        public string CancelText {
            get { return _cancelText; }
            set { _cancelText = value; _cancelButton.SetTitle(value, UIControlState.Normal); }
        }

        public ImageCropViewController ()
        {
        }

        public ImageCropViewController (ImageCropOverlayView overlayView)
        {
            _overlayView = overlayView;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad();

            AutomaticallyAdjustsScrollViewInsets = false;

            SetupCropView();
            SetupToolbar();

            if (NavigationController != null) {
                NavigationController.NavigationBarHidden = true;
            }
        }

        public override void ViewWillLayoutSubviews ()
        {
            base.ViewWillLayoutSubviews();
            ImageCropView.Frame = View.Bounds;
            _toolbar.Frame = new CGRect(0, View.Frame.Height - 54, View.Frame.Width, 54);
        }

        void SetupCropView ()
        {
            ImageCropView = new ImageCropView(_overlayView, View.Bounds);
            ImageCropView.ImageToCrop = SourceImage;
            //ImageCropView.CropSize = CropSize;
            View.AddSubview(ImageCropView);
        }

        void SetupToolbar ()
        {
            _toolbar.Translucent = true;
            _toolbar.BarStyle = UIBarStyle.Black;
            View.AddSubview(_toolbar);

            SetupCancelButton();
            SetupUseButton();

            var cancel = new UIBarButtonItem(_cancelButton);
            var flex = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace);
            var use = new UIBarButtonItem(_useButton);
            _toolbar.SetItems(new UIBarButtonItem[]{cancel, flex, use}, false);
        }

        void SetupCancelButton ()
        {
            _cancelButton.TitleLabel.Font = UIFont.BoldSystemFontOfSize(16);
            _cancelButton.TitleLabel.ShadowOffset = new CGSize(0, -1);
            _cancelButton.Frame = new CGRect(0, 0, 58, 30);
            _cancelButton.SetTitle("cancel", UIControlState.Normal);
            _cancelButton.TouchUpInside += cancelButton_TouchUpInside;
        }

        void SetupUseButton ()
        {
            _useButton.TitleLabel.Font = UIFont.BoldSystemFontOfSize(16);
            _useButton.TitleLabel.ShadowOffset = new CGSize(0, -1);
            _useButton.Frame = new CGRect(0, 0, 58, 30);
            _useButton.SetTitle("use", UIControlState.Normal);
            _useButton.TouchUpInside += useButton_TouchUpInside;
        }

        void cancelButton_TouchUpInside (object sender, EventArgs e)
        {
            if (NavigationController != null) {
                NavigationController.PopViewController(true);
            }
        }

        void useButton_TouchUpInside (object sender, EventArgs e)
        {
            _croppedImage = ImageCropView.CroppedImage();
            if (Delegate != null) {
                Delegate.FinishedCroppingImage(this, _croppedImage);
            }
        }
    }
}

