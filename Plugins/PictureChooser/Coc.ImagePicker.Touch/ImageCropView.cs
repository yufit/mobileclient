﻿using System;
using UIKit;
using CoreGraphics;
using System.Drawing;

namespace Coc.ImagePicker.Touch
{
    class ScrollView : UIScrollView
    {
        public ScrollView (CGRect frame) : base(frame) {}

        public override void LayoutSubviews ()
        {
            var zoomView = Delegate.ViewForZoomingInScrollView(this);
            if (zoomView != null) {
                var boundsSize = Bounds.Size;
                var frameToCenter = zoomView.Frame;

                // center horizontally
                if (frameToCenter.Size.Width < boundsSize.Width) {
                    frameToCenter.X = (boundsSize.Width - frameToCenter.Size.Width) / 2;
                } else {
                    frameToCenter.X = 0;
                }

                // center vertically
                if (frameToCenter.Size.Height < boundsSize.Height) {
                    frameToCenter.Y = (boundsSize.Height - frameToCenter.Size.Height) / 2;
                } else {
                    frameToCenter.Y = 0;
                }

                zoomView.Frame = frameToCenter;
            }
        }
    }

    public sealed class ImageCropView : UIView
    {
        readonly UIScrollView _scrollView;
        readonly UIImageView _imageView;
        ImageCropOverlayView _cropOverlayView;
        float _xOffset;
        float _yOffset;

        public static CGRect ScaleRect(CGRect rect, float scale)
        {
            return new CGRect(
                rect.X * scale,
                rect.Y * scale,
                rect.Width * scale,
                rect.Height * scale
            );
        }

        public UIImage ImageToCrop { 
            get { return _imageView.Image; } 
            set { _imageView.Image = value; }
        }

        public CGSize CropSize {
            get { return _cropOverlayView.CropSize; }
            set {
                var view = _cropOverlayView;
                if (view != null) {
                    view.CropSize = value;
                } else {
                    _cropOverlayView = new ImageCropOverlayView(Bounds);
                    _cropOverlayView.CropSize = value;
                    AddSubview(_cropOverlayView);
                }
            }
        }

        public ImageCropView (CGRect frame) : base(frame)
        {
            UserInteractionEnabled = true;
            BackgroundColor = UIColor.Black;
            _scrollView = new ScrollView(Frame);
            _scrollView.ShowsHorizontalScrollIndicator = false;
            _scrollView.ShowsVerticalScrollIndicator = false;
            _scrollView.ClipsToBounds = false;
            _scrollView.DecelerationRate = 0;
            _scrollView.BackgroundColor = UIColor.Clear;
            AddSubview(_scrollView);

            _scrollView.ViewForZoomingInScrollView = GetZoomView;

            _imageView = new UIImageView(_scrollView.Frame);
            _imageView.ContentMode = UIViewContentMode.ScaleAspectFit;
            _imageView.BackgroundColor = UIColor.Black;
            _scrollView.AddSubview(_imageView);

            _scrollView.MinimumZoomScale = _scrollView.Frame.Width / _scrollView.Frame.Height;
            _scrollView.MaximumZoomScale = 20;
            _scrollView.SetZoomScale(1.0f, false);
        }

        public ImageCropView (ImageCropOverlayView overlayView, CGRect frame) : base(frame)
        {
            UserInteractionEnabled = true;
            BackgroundColor = UIColor.Black;
            _scrollView = new ScrollView(Frame);
            _scrollView.ShowsHorizontalScrollIndicator = false;
            _scrollView.ShowsVerticalScrollIndicator = false;
            _scrollView.ClipsToBounds = false;
            _scrollView.DecelerationRate = 0;
            _scrollView.BackgroundColor = UIColor.Clear;
            AddSubview(_scrollView);

            _scrollView.ViewForZoomingInScrollView = GetZoomView;

            _imageView = new UIImageView(_scrollView.Frame);
            _imageView.ContentMode = UIViewContentMode.ScaleAspectFit;
            _imageView.BackgroundColor = UIColor.Black;
            _scrollView.AddSubview(_imageView);

            _scrollView.MinimumZoomScale = _scrollView.Frame.Width / _scrollView.Frame.Height;
            _scrollView.MaximumZoomScale = 20;
            _scrollView.SetZoomScale(1.0f, false);
            _cropOverlayView = overlayView;
            AddSubview(_cropOverlayView);
        }

        UIView GetZoomView (UIScrollView scrollView)
        {
            return _imageView;
        }

        public override void LayoutSubviews ()
        {
            base.LayoutSubviews();
            var size = CropSize;
            const int toolbarSize = 54;
            _xOffset = (float) Math.Floor((Bounds.Width - size.Width) * 0.5);
            _yOffset = (float) Math.Floor((Bounds.Height - toolbarSize - size.Height) * 0.5);

            var height = ImageToCrop.Size.Height;
            var width = ImageToCrop.Size.Width;

            nfloat factor;
            nfloat factoredHeight;
            nfloat factoredWidth;

            if (width > height) {
                factor = width / size.Width;
                factoredWidth = size.Width;
                factoredHeight =  height / factor;
            } else {
                factor = height / size.Height;
                factoredWidth = width / factor;
                factoredHeight = size.Height;
            }

            // TODO MAKE IMAGE WIDTH OR HEIGHT TO BE FULL WIDTH OR HEIGHT OF SCROLL TO AVOID BLACK BORDER ON SIDE OR TOP.
            _cropOverlayView.Frame = Bounds;
            _scrollView.Frame = new CGRect(_xOffset, _yOffset, size.Width, size.Height);
            _scrollView.ContentSize = new CGSize(size.Width, size.Height);
            _imageView.Frame = new CGRect(0, Math.Floor((size.Height - factoredHeight) * 0.5),
                factoredWidth, factoredHeight);
        }

        public UIImage CroppedImage()
        {
            var visibleRect = CalcVisibleRectForCropArea();
            var rectTransform = OrientationTransformedRectOfImage(ImageToCrop);
            visibleRect = CGAffineTransform.CGRectApplyAffineTransform(visibleRect, rectTransform);

            // finally crop image
//            UIGraphics.BeginImageContextWithOptions(CropSize, false, 0);
//            var resultImage = UIGraphics.GetImageFromCurrentImageContext();
            var imageRef = ImageToCrop.CGImage.WithImageInRect(visibleRect);
            if (imageRef != null) {
                var result = new UIImage(imageRef, (nfloat) ImageToCrop.CurrentScale, ImageToCrop.Orientation);
                return result;
            } else {
                // TODO LOG THE ERROR.
                return null;
            }
        }

        CGRect CalcVisibleRectForCropArea()
        {
            // scaled width/height in regards of real width to crop width
            var scaleWidth = ImageToCrop.Size.Width / CropSize.Width;
            var scaleHeight = ImageToCrop.Size.Height / CropSize.Height;
            float scale;

            if (CropSize.Width == CropSize.Height) {
                scale = (float) Math.Max(scaleWidth, scaleHeight);
            } else if (CropSize.Width > CropSize.Height) {
                scale = ImageToCrop.Size.Width < ImageToCrop.Size.Height ?
                    (float) Math.Max(scaleWidth, scaleHeight) :
                    (float) Math.Min(scaleWidth, scaleHeight);
            } else {
                scale = ImageToCrop.Size.Width < ImageToCrop.Size.Height ?
                    (float) Math.Min(scaleWidth, scaleHeight) :
                    (float) Math.Max(scaleWidth, scaleHeight);
            }

            Console.WriteLine (ImageToCrop.Size);
            Console.WriteLine (scale);
            // extract visible rect from scrollview and scale it
            var visibleRect = _scrollView.ConvertRectToView(_scrollView.Bounds, _imageView);
            Console.WriteLine (visibleRect);
            visibleRect = ImageCropView.ScaleRect(visibleRect, scale);
            Console.WriteLine (visibleRect);
            return visibleRect;
        }

        CGAffineTransform OrientationTransformedRectOfImage(UIImage image)
        {
            var rectTransform = CGAffineTransform.MakeIdentity();

            switch (image.Orientation) {
                case UIImageOrientation.Left:
                    rectTransform = CGAffineTransform.Translate(
                        CGAffineTransform.MakeRotation((nfloat) (Math.PI/2)), 0, -image.Size.Height);
                    break;
                case UIImageOrientation.Right:
                    rectTransform = CGAffineTransform.Translate(
                        CGAffineTransform.MakeRotation(-((nfloat) (Math.PI/2))), -image.Size.Width, 0);
                    break;
                case UIImageOrientation.Down:
                    rectTransform = CGAffineTransform.Translate(
                        CGAffineTransform.MakeRotation((nfloat) (-Math.PI)), -image.Size.Width, -image.Size.Height);
                    break;
            }

            return CGAffineTransform.Scale(rectTransform, image.CurrentScale, image.CurrentScale);
        }
    }
}

