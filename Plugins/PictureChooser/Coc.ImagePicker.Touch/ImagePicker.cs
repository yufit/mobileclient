﻿using System;
using UIKit;
using System.Drawing;
using CoreGraphics;

namespace Coc.ImagePicker.Touch
{
    public interface ImagePickerDelegate
    {
        void ImagePicked(ImagePicker picker, UIImage pickedImage);
        void ImagePickedDidCancel(ImagePicker picker);
    }

    public class ImagePicker : UINavigationControllerDelegate, ImageCropControllerDelegate
    {
        public UIImagePickerController ImagePickerController { get; private set; }
        public ImagePickerDelegate Delegate { get; set; }
        public CGSize CropSize { get; set; }

        ImageCropOverlayView _overlayView;

        public ImagePicker ()
        {
            CropSize = new SizeF(320, 320);
            ImagePickerController = new UIImagePickerController();

            ImagePickerController.FinishedPickingMedia += Picker_FinishedPickingMedia;
            ImagePickerController.FinishedPickingImage += Picker_FinishedPickingImage;
            ImagePickerController.Canceled += Picker_Canceled;

        }

        public ImagePicker (ImageCropOverlayView overlayView)
        {
//            CropSize = new SizeF(320, 320);
            ImagePickerController = new UIImagePickerController();

            ImagePickerController.FinishedPickingMedia += Picker_FinishedPickingMedia;
            ImagePickerController.FinishedPickingImage += Picker_FinishedPickingImage;
            ImagePickerController.Canceled += Picker_Canceled;

            _overlayView = overlayView;

        }

        public void DismissController()
        {
            ImagePickerController.FinishedPickingMedia -= Picker_FinishedPickingMedia;
            ImagePickerController.FinishedPickingImage -= Picker_FinishedPickingImage;
            ImagePickerController.Canceled -= Picker_Canceled;

            ImagePickerController.DismissViewController(true, null);
        }

        void Picker_FinishedPickingMedia (object sender, UIImagePickerMediaPickedEventArgs e)
        {
            var picker = (UIImagePickerController) sender;

            var cropController = new ImageCropViewController(_overlayView);
            cropController.SourceImage = e.OriginalImage;
            //cropController.CropSize = CropSize;
            cropController.Delegate = this;
            picker.PushViewController(cropController, true);
        }

        void Picker_FinishedPickingImage (object sender, UIImagePickerImagePickedEventArgs e)
        {
        }

        void Picker_Canceled (object sender, EventArgs e)
        {
            if (Delegate != null) {
                Delegate.ImagePickedDidCancel(this);
            } else {
                DismissController();
            }
        }

        #region ImageCropControllerDelegate implementation

        public void FinishedCroppingImage (ImageCropViewController controller, UIImage croppedImage)
        {
            if (Delegate != null) {
                Delegate.ImagePicked(this, croppedImage);
            }
        }

        #endregion
    }
}

