﻿using System;
using System.Drawing;
using UIKit;
using CoreGraphics;

namespace Coc.ImagePicker.Touch
{
    public class ImageCropOverlayView : UIView
    {
        public CGRect CropRect { get; set; }
        public CGSize CropSize { get; set; }

        public ImageCropOverlayView ()
        {
            BackgroundColor = UIColor.Clear;
            UserInteractionEnabled = false;
        }

        public ImageCropOverlayView (CGRect frame) : base(frame)
        {
            BackgroundColor = UIColor.Clear;
            UserInteractionEnabled = false;
        }

        public override void Draw (CGRect rect)
        {
            base.Draw(rect);

            const int toolbarSize = 54;
            var width = Frame.Width;
            var height = Frame.Height - toolbarSize;

            var heightSpan = Math.Floor(height / 2.0f - CropSize.Height/ 2.0f);
            var widthSpan = Math.Floor(width / 2.0f - CropSize.Width/ 2.0f);

            // fill outer rect
            UIColor.FromRGBA(0,0,0,0.5f).SetFill();
            UIGraphics.RectFill(Bounds);

            // fill inner border
            UIColor.FromRGBA(1,1,1,0.5f).SetFill();
            UIGraphics.RectFill(new CGRect(widthSpan-2, heightSpan-2, CropSize.Width + 4, CropSize.Height + 4));

            // fill inner rect
            UIColor.Clear.SetFill();
            UIGraphics.RectFill(new CGRect(widthSpan, heightSpan, CropSize.Width, CropSize.Height));
        }
    }

    public class ImageDiamondCropOverlayView : ImageCropOverlayView
    {
        public ImageDiamondCropOverlayView ()
        {
            BackgroundColor = UIColor.Clear;
            UserInteractionEnabled = false;
        }

        public ImageDiamondCropOverlayView (CGRect frame) : base(frame)
        {
            BackgroundColor = UIColor.Clear;
            UserInteractionEnabled = false;
        }

        protected CGRect DiamondRect {
            get {
                var width = Frame.Width;
                var height = Frame.Height - 54;
                var heightSpan = Math.Floor(height / 2.0f - CropSize.Height/ 2.0f);
                var widthSpan = Math.Floor(width / 2.0f - CropSize.Width/ 2.0f);

                var rect = new CGRect(widthSpan, heightSpan, CropSize.Width, CropSize.Height);
                if (rect.Width > rect.Height) { 
                    rect.Width = rect.Height; 
                    rect.X = (Bounds.Width-rect.Width)/2.0f;
                } else if (rect.Height > rect.Width) { 
                    rect.Height = rect.Width; 
                    rect.Y = (Bounds.Height-rect.Height)/2.0f;
                }

                if (Math.Abs(rect.Width) < 0.1f || Math.Abs(rect.Height) < 0.1f) {
                    return rect;
                }
                return rect;
            }
        }

        public override void Draw (CGRect rect)
        {
            //const int toolbarSize = 54;
            //var width = Frame.Width;
            //var height = Frame.Height - toolbarSize;

            // fill outer rect
            UIColor.FromRGBA(0,0,0,0.5f).SetFill();
            UIGraphics.RectFill(Bounds);

            CGRect frame = DiamondRect;
            UIBezierPath _diamondShapePath = new UIBezierPath();
            _diamondShapePath.MoveTo    (new CGPoint(frame.GetMinX() + 0.49942f * frame.Width, frame.GetMinY() + 1.00000f * frame.Height));
            _diamondShapePath.AddLineTo (new CGPoint(frame.GetMinX() + 1.00000f * frame.Width, frame.GetMinY() + 0.49942f * frame.Height));
            _diamondShapePath.AddLineTo (new CGPoint(frame.GetMinX() + 0.50058f * frame.Width, frame.GetMinY() + 0.00000f * frame.Height));
            _diamondShapePath.AddLineTo (new CGPoint(frame.GetMinX() + 0.00000f * frame.Width, frame.GetMinY() + 0.50058f * frame.Height));
            _diamondShapePath.AddLineTo (new CGPoint(frame.GetMinX() + 0.49942f * frame.Width, frame.GetMinY() + 1.00000f * frame.Height));
            _diamondShapePath.ClosePath();
            UIColor.FromRGBA(1,1,1,1.0f).SetFill();
            _diamondShapePath.Fill();

            UIGraphics.GetCurrentContext().SetBlendMode(CGBlendMode.Clear);
            UIColor.Clear.SetFill();
            frame = DiamondRect;
            frame = frame.Inset(4, 4);
            _diamondShapePath = new UIBezierPath();
            _diamondShapePath.MoveTo    (new CGPoint(frame.GetMinX() + 0.49942f * frame.Width, frame.GetMinY() + 1.00000f * frame.Height));
            _diamondShapePath.AddLineTo (new CGPoint(frame.GetMinX() + 1.00000f * frame.Width, frame.GetMinY() + 0.49942f * frame.Height));
            _diamondShapePath.AddLineTo (new CGPoint(frame.GetMinX() + 0.50058f * frame.Width, frame.GetMinY() + 0.00000f * frame.Height));
            _diamondShapePath.AddLineTo (new CGPoint(frame.GetMinX() + 0.00000f * frame.Width, frame.GetMinY() + 0.50058f * frame.Height));
            _diamondShapePath.AddLineTo (new CGPoint(frame.GetMinX() + 0.49942f * frame.Width, frame.GetMinY() + 1.00000f * frame.Height));
            _diamondShapePath.ClosePath();
            _diamondShapePath.UsesEvenOddFillRule = true;
            _diamondShapePath.Fill();

        }
    }
}

