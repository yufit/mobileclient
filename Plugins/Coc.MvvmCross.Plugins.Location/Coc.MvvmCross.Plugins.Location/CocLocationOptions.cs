﻿using System;

namespace Coc.MvvmCross.Plugins.Location
{
    public enum CocLocationAccuracy { Fine, Coarse }

    public class CocLocationOptions
    {
        public CocLocationAccuracy Accuracy { get; set; }
        /// <summary>
        /// Use TimeSpan.Zero for most frequent updates
        /// </summary>
        public TimeSpan TimeBetweenUpdates { get; set; }
        /// <summary>
        /// Use 0 threshold for most frequent updates
        /// </summary>
        public int MovementThresholdInM { get; set; }
    }
}

