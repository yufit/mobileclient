﻿using System;

namespace Coc.MvvmCross.Plugins.Location
{
    public class CocLocation
    {
        public CocCoordinates Coordinates { get; set; }
        public DateTimeOffset Timestamp { get; set; }

        public CocLocation() { 
            Coordinates = new CocCoordinates();
        }
    }
}

