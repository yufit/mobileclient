﻿using System.Threading.Tasks;

namespace Coc.MvvmCross.Plugins.Location
{
    public interface ICocLocationManager
    {
        bool LocationServicesEnabled { get; }
        bool IsAuthorized { get; }
        bool HasNotBeenAuthorized { get; }

        Task<bool> RequestAuthorizationAsync(bool always = false);
        Task<CocLocation> GetCurrentLocationAsync();
    }
}

#if bkupforfuture
namespace Coc.MvvmCross.Plugins.Location
{
public class LocationChangedEventArgs : EventArgs 
{
// class members
}

public class LocationErrorEventArgs : EventArgs 
{
// class members
}

public class AuthorizationChangedEventArgs : EventArgs 
{
// class members
}

public interface ICocLocationManager
{
event EventHandler LocationChanged;
event EventHandler LocationError;
event EventHandler AuthorizationChanged;

bool LocationServicesEnabled { get; }
bool IsAuthorized { get; }
bool HasNotBeenAuthorized { get; }

void RequestAuthorization(bool always = false);
Task<bool> RequestAuthorizationAsync(bool always = false);

bool Tracking { get; }
void StartTracking(CocLocationOptions options);
void StopTrackings();

CocLocation LastSeenLocation { get; }
Task<CocLocation> GetCurrentLocationAsync();
}
}


#endif