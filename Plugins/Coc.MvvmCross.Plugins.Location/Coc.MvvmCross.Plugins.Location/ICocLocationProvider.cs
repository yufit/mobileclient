﻿using System;
using System.Threading.Tasks;

namespace Coc.MvvmCross.Plugins.Location
{
    public interface ICocLocationProvider
    {
        /// <summary>
        /// MOST ACCURATE.  REQUIRE USER'S PERMISSION.
        /// Tries the get location from device API.  It will check to see if
        /// the user was ever asked and it will ask the user for permission.
        /// </summary>
        /// <returns>The get location from device API.</returns>
        Task<CocLocation> TryGetLocationFromDeviceApi();

        /// <summary>
        /// SOMEWHAT ACCURATE.  DOESN'T REQUIRE ANY PERMISSION
        /// Tries the get location using ip address.
        /// </summary>
        /// <returns>The get location using ip address.</returns>
        Task<CocLocation> TryGetLocationUsingIpAddress();

        /// <summary>
        /// NOT REALLY ACCURATE.  DOESN'T REQUIRE ANY PERMISSION
        /// LONGITUDE SHOULD BE 
        /// Tries the get location using timezone.
        /// </summary>
        /// <returns>The get location using timezone.</returns>
        CocLocation TryGetLocationUsingTimezone();
    }
}

