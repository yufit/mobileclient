using System;

namespace Coc.MvvmCross.Plugins.Location.Properties
{

    public enum CocLocationAccuracy
    {
        Fine,
        Coarse
    }
    
}
