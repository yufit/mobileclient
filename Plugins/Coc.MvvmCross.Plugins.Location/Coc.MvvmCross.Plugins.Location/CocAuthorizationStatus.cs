﻿using System;

namespace Coc.MvvmCross.Plugins.Location
{
    public enum CocAuthorizationStatus
    {
        NotDetermined  = 0,
        Restricted,
        Denied,
        Authorized,
        AuthorizedAlways  = Authorized,
        AuthorizedWhenInUse 
    }
}

