﻿using System;

namespace Coc.MvvmCross.Plugins.Location
{
    public enum CocLocationErrorCode
    {
        ServiceUnavailable,
        PermissionDenied,
        PositionUnavailable,
        Timeout
    }

    public class CocLocationError
    {
        public CocLocationErrorCode Code { get; private set; }
        public CocLocationError(CocLocationErrorCode code) { Code = code; }
    }
}

