using MvvmCross.Platform.Plugins;
using MvvmCross.Platform;

namespace Coc.MvvmCross.Plugins.Location.Touch
{
    public class Plugin : IMvxPlugin
    {
        public void Load()
        {
            CocTouchLocationManager locationManager = new CocTouchLocationManager();
            Mvx.RegisterSingleton<ICocLocationManager>(() => locationManager);
            Mvx.RegisterSingleton<ICocLocationProvider>(() => new CocLocationProvider(locationManager));
        }
    }
}
