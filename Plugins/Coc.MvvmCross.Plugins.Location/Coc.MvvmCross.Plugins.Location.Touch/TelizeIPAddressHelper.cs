using System;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Coc.MvvmCross.Plugins.Location.Touch
{

    public class TelizeIPAddressHelper
    {
        //http://ip4.telize.com
        //http://www.telize.com/geoip/74.69.71.167

        public async Task<string> GetIPAddress() 
        {
            using (HttpClient client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync("http://ip4.telize.com"))
            using (HttpContent content = response.Content)
            {
                string result = await content.ReadAsStringAsync();
                return result;
            }

        }

        public async Task<CocLocation> GetCoordinates(string ipAddress)
        {
            using (HttpClient client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync("http://www.telize.com/geoip/" + ipAddress))
            using (HttpContent content = response.Content)
            {
                string result = await content.ReadAsStringAsync();
                //JsonSerializer serializer = new JsonSerializer();
                var o = JsonConvert.DeserializeObject<JObject>(result);
                var longitude = Convert.ToDouble(o["longitude"]);
                var latitude = Convert.ToDouble(o["latitude"]);
                return new CocLocation { Coordinates = new CocCoordinates { Longitude = longitude, Latitude = latitude }};
            }
        }

        /*
        {
            "longitude": -77.4494,
            "latitude": 43.2186,
            "asn": "AS11351",
            "offset": "-4",
            "ip": "74.69.71.167",
            "area_code": "0",
            "continent_code": "NA",
            "dma_code": "0",
            "city": "Webster",
            "timezone": "America/New_York",
            "region": "New York",
            "country_code": "US",
            "isp": "Time Warner Cable Internet LLC",
            "postal_code": "14580",
            "country": "United States",
            "country_code3": "USA",
            "region_code": "NY"
        }
        */
    }
    
}
