﻿using System;
using System.Threading.Tasks;

namespace Coc.MvvmCross.Plugins.Location.Touch
{
    public class CocLocationProvider : ICocLocationProvider
    {
        readonly ICocLocationManager _locationWatcher;
        public CocLocationProvider (ICocLocationManager locationWatcher)
        {
            _locationWatcher = locationWatcher;
        }

        #region ICocLocationProvider implementation

        public async Task<CocLocation> TryGetLocationFromDeviceApi ()
        {
            EnsureLocationServicesIsEnabled();
            await EnsureLocationTrackingIsAuthorised();
            return await _locationWatcher.GetCurrentLocationAsync();
        }

        public async Task<CocLocation> TryGetLocationUsingIpAddress ()
        {
            TelizeIPAddressHelper h = new TelizeIPAddressHelper();
            string ip = await h.GetIPAddress();
            return await h.GetCoordinates(ip);
        }

        public CocLocation TryGetLocationUsingTimezone ()
        {
            DateTime now = DateTime.Now;

            DateTime winter = new DateTime(2000, 1, 1);
            DateTime summer = new DateTime(2000, 6, 1);

            var winterOffset = TimeZone.CurrentTimeZone.GetUtcOffset(winter).TotalSeconds;
            var summerOffset = TimeZone.CurrentTimeZone.GetUtcOffset(summer).TotalSeconds;
            var nowOffset = TimeZone.CurrentTimeZone.GetUtcOffset(now).TotalHours;

            var longitude = nowOffset * 15;
            int latitude = 0;
            if (winterOffset < summerOffset) latitude = 45;
            else if (winterOffset > summerOffset) latitude = -45;

            return new CocLocation { Coordinates = new CocCoordinates { Longitude = longitude, Latitude = latitude }};
        }

        #endregion

        void EnsureLocationServicesIsEnabled ()
        {
            if (!_locationWatcher.LocationServicesEnabled) { 
                throw new Exception(); 
            }
        }

        async Task EnsureLocationTrackingIsAuthorised ()
        {
            if (_locationWatcher.HasNotBeenAuthorized) { 
                if (!await _locationWatcher.RequestAuthorizationAsync()) {
                    throw new Exception(); 
                }
            } else {
                if (!_locationWatcher.IsAuthorized) {
                    throw new Exception(); 
                }
            }
        }
    }


}
    

