using System;
using System.Threading.Tasks;

using CoreLocation;
using Foundation;

namespace Coc.MvvmCross.Plugins.Location.Touch
{
    static class NSDateExtensions 
    {
        public static DateTime NSDateToDateTime(this NSDate date)
        {
            DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime( 
                new DateTime(2001, 1, 1, 0, 0, 0) );
            return reference.AddSeconds(date.SecondsSinceReferenceDate);
        }

        public static NSDate DateTimeToNSDate(this DateTime date)
        {
            DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(
                new DateTime(2001, 1, 1, 0, 0, 0) );
            return NSDate.FromTimeIntervalSinceReferenceDate(
                (date - reference).TotalSeconds);
        }
    }

    public class CocTouchLocationManager : ICocLocationManager
    {
        readonly CLLocationManager _locationManager = new CLLocationManager();

        #region ICocLocationWatcher implementation

        public bool LocationServicesEnabled { 
            get { return CLLocationManager.LocationServicesEnabled; } 
        }

        public bool IsAuthorized { 
            get { return TrackingAuthorized(CLLocationManager.Status); }
        }

        public bool HasNotBeenAuthorized { 
            get { return CLLocationManager.Status == CLAuthorizationStatus.NotDetermined; }
        }

        public CocTouchLocationManager ()
        {
        }

        /// <summary>
        /// Start the specified options.
        /// </summary>
        /// <param name="options">Options.</param>
        public void StartTracking (CocLocationOptions options)
        {
            ApplyOptions(options);
            _locationManager.StartUpdatingLocation();
        }

        /// <summary>
        /// Stop this instance.
        /// </summary>
        public void StopTrackings ()
        {
            _locationManager.StopUpdatingLocation();
            if (CLLocationManager.HeadingAvailable) { _locationManager.StopUpdatingHeading(); }
        }

        public async Task<bool> RequestAuthorizationAsync (bool always = false)
        {
            var t = new TaskCompletionSource<bool>();
            TryRequestAuthorization(always, authorized => t.TrySetResult(authorized));
            return await t.Task;
        }

        public async Task<CocLocation> GetCurrentLocationAsync()
        {
            var t = new TaskCompletionSource<CocLocation>();
            TryGettingCurrentLocation(location => t.TrySetResult(location));
            return await t.Task;
        }

        #endregion

        bool TrackingAuthorized(CLAuthorizationStatus status)
        {
            return (status == CLAuthorizationStatus.Authorized || 
                status == CLAuthorizationStatus.AuthorizedAlways || 
                status == CLAuthorizationStatus.AuthorizedWhenInUse);
        }

        void TryRequestAuthorization(bool always, Action<bool> completion)
        {
            EventHandler<CLAuthorizationChangedEventArgs> handler = null;
            handler = (s, e) => {
                _locationManager.AuthorizationChanged -= handler;
                completion(TrackingAuthorized(e.Status));
            };
            _locationManager.AuthorizationChanged += handler;

            if (always) { _locationManager.RequestAlwaysAuthorization(); }
            else        { _locationManager.RequestWhenInUseAuthorization(); }
        }

        void TryGettingCurrentLocation(Action<CocLocation> completion)
        {
            CocLocationOptions locaOptions = new CocLocationOptions();
            locaOptions.Accuracy = CocLocationAccuracy.Fine;
            locaOptions.TimeBetweenUpdates = TimeSpan.Zero;
            locaOptions.MovementThresholdInM = 1;

            EventHandler<CLLocationsUpdatedEventArgs> handler = null;
            handler = (s, e) => {
                var mostRecent = e.Locations[e.Locations.Length - 1];
                var converted = CreateLocation(mostRecent);
                _locationManager.LocationsUpdated -= handler;
                StopTrackings();
                completion(converted);
            };
            _locationManager.LocationsUpdated += handler;

            StartTracking(locaOptions);
        }

        #region LocationManagerHandlers


        #endregion

        /// <summary>
        /// Applies the options.
        /// </summary>
        /// <param name="options">Options.</param>
        void ApplyOptions (CocLocationOptions options)
        {
            if (options.MovementThresholdInM > 0) {
                _locationManager.DistanceFilter = options.MovementThresholdInM;
            } else {
                _locationManager.DistanceFilter = CLLocationDistance.FilterNone;
            }

            _locationManager.DesiredAccuracy = options.Accuracy == CocLocationAccuracy.Fine ? CLLocation.AccuracyBest : CLLocation.AccuracyKilometer;

            if (CLLocationManager.HeadingAvailable) {
                _locationManager.StartUpdatingHeading();
            }
        }

        /// <summary>
        /// Creates the location.
        /// </summary>
        /// <returns>The location.</returns>
        /// <param name="location">Location.</param>
        /// <param name="heading">Heading.</param>
        private static CocLocation CreateLocation(CLLocation location, CLHeading heading = null)
        {
            var position = new CocLocation { Timestamp = location.Timestamp.NSDateToDateTime() };
            var coords = position.Coordinates;

            coords.Altitude = location.Altitude;
            coords.Latitude = location.Coordinate.Latitude;
            coords.Longitude = location.Coordinate.Longitude;
            coords.Speed = location.Speed;
            coords.Accuracy = location.HorizontalAccuracy;
            coords.AltitudeAccuracy = location.VerticalAccuracy;
            if (heading != null)
            {
                coords.Heading = heading.TrueHeading;
                coords.HeadingAccuracy = heading.HeadingAccuracy;
            }

            return position;
        }
    }
}

#if later

using System;
using System.Threading.Tasks;

using CoreLocation;
using Foundation;

namespace Coc.MvvmCross.Plugins.Location.Touch
{
static class NSDateExtensions 
{
public static DateTime NSDateToDateTime(this NSDate date)
{
DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime( 
new DateTime(2001, 1, 1, 0, 0, 0) );
return reference.AddSeconds(date.SecondsSinceReferenceDate);
}

public static NSDate DateTimeToNSDate(this DateTime date)
{
DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(
new DateTime(2001, 1, 1, 0, 0, 0) );
return NSDate.FromTimeIntervalSinceReferenceDate(
(date - reference).TotalSeconds);
}
}

public class CocTouchLocationManager : ICocLocationManager
{
readonly CLLocationManager _locationManager = new CLLocationManager();
readonly object _locker = new object();

#region ICocLocationWatcher implementation

public event EventHandler LocationChanged;
public event EventHandler LocationError;
public event EventHandler AuthorizationChanged;

public CocLocation LastSeenLocation { get; private set; }
public bool Tracking { get; private set; }

public bool LocationServicesEnabled { 
    get { return CLLocationManager.LocationServicesEnabled; } 
}

public bool IsAuthorized { 
    get { return TrackingAuthorized(CLLocationManager.Status); }
}

public bool HasNotBeenAuthorized { 
    get { return CLLocationManager.Status == CLAuthorizationStatus.NotDetermined; }
}

public CocTouchLocationManager ()
{
    _locationManager.Failed += HandleFailed;
    _locationManager.MonitoringFailed += HandleMonitoringFailed;
    _locationManager.AuthorizationChanged += HandleAuthorizationChanged;
    _locationManager.LocationsUpdated += HandleLocationsUpdated;
}

/// <summary>
/// Start the specified options.
/// </summary>
/// <param name="options">Options.</param>
public void StartTracking (CocLocationOptions options)
{
    if (Tracking) { StopTrackings(); }

    lock (_locker) {
        //RequestAuthorization();
        ApplyOptions(options);
        _locationManager.StartUpdatingLocation();
    }
}

/// <summary>
/// Stop this instance.
/// </summary>
public void StopTrackings ()
{
    lock (_locker) {
        _locationManager.StopUpdatingLocation();
        if (CLLocationManager.HeadingAvailable) { _locationManager.StopUpdatingHeading(); }
    }
}

/// <summary>
/// Requests the authorization location tracking.
/// </summary>
public void RequestAuthorization (bool always = false)
{
    lock (_locker)  {
        if (always) { _locationManager.RequestAlwaysAuthorization(); }
        else        { _locationManager.RequestWhenInUseAuthorization(); }
    }
}

public async Task<bool> RequestAuthorizationAsync (bool always = false)
{
    var t = new TaskCompletionSource<bool>();
    TryRequestAuthorization(always, authorized => t.TrySetResult(authorized));
    return await t.Task;
}

public async Task<CocLocation> GetCurrentLocationAsync()
{
    var t = new TaskCompletionSource<CocLocation>();
    TryGettingCurrentLocation(location => t.TrySetResult(location));
    return await t.Task;
}

#endregion

bool TrackingAuthorized(CLAuthorizationStatus status)
{
    return (status == CLAuthorizationStatus.Authorized || 
        status == CLAuthorizationStatus.AuthorizedAlways || 
        status == CLAuthorizationStatus.AuthorizedWhenInUse);
}

void TryRequestAuthorization(bool always, Action<bool> completion)
{
    EventHandler<CLAuthorizationChangedEventArgs> handler = null;
    handler = (s, e) => {
        _locationManager.AuthorizationChanged -= handler;
        completion(TrackingAuthorized(e.Status));
    };
    _locationManager.AuthorizationChanged += handler;

    if (always) { _locationManager.RequestAlwaysAuthorization(); }
    else        { _locationManager.RequestWhenInUseAuthorization(); }
}

void TryGettingCurrentLocation(Action<CocLocation> completion)
{
    if (!Tracking) {
        CocLocationOptions locaOptions = new CocLocationOptions();
        locaOptions.Accuracy = CocLocationAccuracy.Fine;
        locaOptions.TimeBetweenUpdates = TimeSpan.Zero;
        locaOptions.MovementThresholdInM = 1;

        EventHandler<CLLocationsUpdatedEventArgs> handler = null;
        handler = (s, e) => {
            var mostRecent = e.Locations[e.Locations.Length - 1];
            var converted = CreateLocation(mostRecent);
            _locationManager.LocationsUpdated -= handler;
            StopTrackings();
            completion(converted);
        };
        _locationManager.LocationsUpdated += handler;

        StartTracking(locaOptions);
    }

}

#region LocationManagerHandlers

/// <summary>
/// Handles the locations updated.
/// </summary>
/// <param name="sender">Sender.</param>
/// <param name="e">E.</param>
void HandleLocationsUpdated (object sender, CLLocationsUpdatedEventArgs e)
{
    if (e.Locations.Length == 0)
    {
        //                MvxTrace.Error("iOS has passed LocationsUpdated an empty array - this should never happen");
        return;
    }

    var mostRecent = e.Locations[e.Locations.Length - 1];
    var converted = CreateLocation(mostRecent);
    SendLocation(converted);

}

/// <summary>
/// Handles the authorization changed.
/// </summary>
/// <param name="sender">Sender.</param>
/// <param name="e">E.</param>
void HandleAuthorizationChanged (object sender, CLAuthorizationChangedEventArgs e)
{

}

/// <summary>
/// Handles the monitoring failed.
/// </summary>
/// <param name="sender">Sender.</param>
/// <param name="e">E.</param>
void HandleMonitoringFailed (object sender, CLRegionErrorEventArgs e)
{

}

/// <summary>
/// Handles the failed.
/// </summary>
/// <param name="sender">Sender.</param>
/// <param name="e">E.</param>
void HandleFailed (object sender, Foundation.NSErrorEventArgs e)
{

}

#endregion

/// <summary>
/// Applies the options.
/// </summary>
/// <param name="options">Options.</param>
void ApplyOptions (CocLocationOptions options)
{
    if (options.MovementThresholdInM > 0) {
        _locationManager.DistanceFilter = options.MovementThresholdInM;
    } else {
        _locationManager.DistanceFilter = CLLocationDistance.FilterNone;
    }

    _locationManager.DesiredAccuracy = options.Accuracy == CocLocationAccuracy.Fine ? CLLocation.AccuracyBest : CLLocation.AccuracyKilometer;

    if (options.TimeBetweenUpdates > TimeSpan.Zero)
    {
        //                    Mvx.Warning("TimeBetweenUpdates specified for MvxLocationOptions - but this is not supported in iOS");
    }

    if (CLLocationManager.HeadingAvailable) {
        _locationManager.StartUpdatingHeading();

    }
}

/// <summary>
/// Creates the location.
/// </summary>
/// <returns>The location.</returns>
/// <param name="location">Location.</param>
/// <param name="heading">Heading.</param>
private static CocLocation CreateLocation(CLLocation location, CLHeading heading = null)
{
    var position = new CocLocation { Timestamp = location.Timestamp.NSDateToDateTime() };
    var coords = position.Coordinates;

    coords.Altitude = location.Altitude;
    coords.Latitude = location.Coordinate.Latitude;
    coords.Longitude = location.Coordinate.Longitude;
    coords.Speed = location.Speed;
    coords.Accuracy = location.HorizontalAccuracy;
    coords.AltitudeAccuracy = location.VerticalAccuracy;
    if (heading != null)
    {
        coords.Heading = heading.TrueHeading;
        coords.HeadingAccuracy = heading.HeadingAccuracy;
    }

    return position;
}

/// <summary>
/// Sends the location.
/// </summary>
/// <param name="location">Location.</param>
void SendLocation (CocLocation location)
{
    LastSeenLocation = location;
    var handler = LocationChanged;
    if (handler != null) { handler(this, new LocationChangedEventArgs()); }
}

/// <summary>
/// Sends the location error.
/// </summary>
/// <param name="error">Error.</param>
void SendLocationError (CocLocationError error)
{
    var handler = LocationError;
    if (handler != null) { handler(this, new LocationErrorEventArgs()); }
}

}
}


#endif