using System;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Coc.MvvmCross.Plugins.Location.Touch
{

    public class TelizeGeoLocationInfo
    {
        public double longitude {get; set;}
        public double latitude {get; set;}
        public string asn {get; set;}
        public double offset {get; set;}
        public string ip {get; set;}
        public string areaCode {get; set;}
        public string continent_code {get; set;}
        public string dma_code {get; set;}
        public string city {get; set;}
        public string time_zone {get; set;}
        public string region {get; set;}
        public string country_code {get; set;}
        public string isp {get; set;}
        public string postal_code {get; set;}
        public string country {get; set;}
        public string country_code3 {get; set; }
    }
}
