// MvxTouchFileStore.cs
// (c) Copyright Cirrious Ltd. http://www.cirrious.com
// MvvmCross is licensed using Microsoft Public License (Ms-PL)
// Contributions and inspirations noted in readme.md and license.txt
// 
// Project Lead - Stuart Lodge, @slodge, me@slodge.com

using System;
using System.IO;
using Foundation;
using UIKit;
using MvvmCross.Platform;

namespace Cirrious.MvvmCross.Plugins.File.Touch
{
    public class MvxTouchFileStore : MvxFileStore
    {
        public const string ResScheme = "res:";

        protected override string FullPath(string path)
        {
            if (path.StartsWith(ResScheme, StringComparison.InvariantCulture)) {
                
                return path.Substring(ResScheme.Length);
            }

//            Mvx.Trace("accessing file {0}", path);
            return Path.Combine(DocsDir(), path);
        }

        public string DocsDir ()
        {
            var version = int.Parse(UIDevice.CurrentDevice.SystemVersion.Split('.')[0]);
            string docsDir = "";
            if (version>=8) {
                var docs = NSFileManager.DefaultManager.GetUrls (NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomain.User) [0];
                docsDir = docs.Path;
//                Console.WriteLine("ios 8++ "+docsDir);
            } else {
                docsDir = Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments);
//                Console.WriteLine("ios 7.1-- " + docsDir);
            }
            return docsDir;
        }

        public static void SetFileProtection(string filePath, bool completeProtection = true)
        {
            NSMutableDictionary dict = new NSMutableDictionary ();
            var protection = new NSString ((completeProtection ? "NSFileProtectionCompleteUnlessOpen" : "NSFileProtectionNone"));

            dict.Add((new NSString("NSFileProtectionKey") as NSObject), (protection as NSObject));
            NSError error;
            NSFileManager.DefaultManager.SetAttributes(dict, filePath, out error);

            if (error != null)
                Mvx.Trace("SetFileProtection Error: {0}",  error.Description);
        }
    }
}

// TODO - credits needed!
