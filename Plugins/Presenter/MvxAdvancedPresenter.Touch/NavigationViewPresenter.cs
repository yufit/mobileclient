// Copyright (C) 2014 Pat Laplante
//
// Permission is hereby granted, free of charge, to  any person obtaining a copy 
// of this software and associated documentation files (the "Software"), to deal 
// in the Software without  restriction, including without limitation the rights 
// to use, copy,  modify,  merge, publish,  distribute,  sublicense, and/or sell 
// copies of the  Software,  and  to  permit  persons  to   whom the Software is 
// furnished to do so, subject to the following conditions:
//
// The above  copyright notice  and this permission notice shall be included all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT  WARRANTY OF  ANY KIND,  EXPRESS OR
// IMPLIED, INCLUDING  BUT NOT  LIMITED TO  THE WARRANTIES  OF  MERCHANTABILITY,  
// FITNESS  FOR  A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
// 
// IN NO EVENT SHALL  THE AUTHORS OR COPYRIGHT  HOLDERS BE  LIABLE FOR ANY CLAIM
// DAMAGES  OR  OTHER  LIABILITY, WHETHER  IN  AN  ACTION  OF  CONTRACT, TORT OR 
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
// OR OTHER DEALINGS IN THE SOFTWARE.
// -----------------------------------------------------------------------------
using UIKit;
using System.Linq;
using MvvmCross.iOS.Views;
using MvvmCross.Core.ViewModels;

namespace Coc.MvxAdvancedPresenter.Touch
{
	public class NavigationViewPresenter : BaseTouchViewPresenter
	{
		private UINavigationController _navViewController;
        public override void ShowFirstView (IMvxIosView view)
		{
			_navViewController = CreateNavController(view);
			RootViewController = _navViewController;
		}

        public override void Show (IMvxIosView view)
		{
			_navViewController.PushViewController(view as UIViewController, true);
		}

		public override void Close (IMvxViewModel viewModel)
		{
			_navViewController.PopViewController(true);
		}

        protected virtual UINavigationController CreateNavController(IMvxIosView view)
		{
			return new UINavigationController(view as UIViewController);
		}

		public override bool IsPresentingSameViewModel (System.Type vmType)
		{
            try {
                if (_navViewController.TopViewController == null) { return false; }
                return ((IMvxIosView)_navViewController.TopViewController).ViewModel.GetType() == vmType;
            } catch {
                return false;
            }
		}

        protected override void WillDetachedFromWindow (UIWindow window)
        {
            var vcs = _navViewController.ViewControllers;
            _navViewController.ViewControllers = new UIViewController[] {};
            vcs.ToList().ForEach(vc => vc.RemoveFromParentViewController ());
            base.WillDetachedFromWindow(window);
        }

	}
}
