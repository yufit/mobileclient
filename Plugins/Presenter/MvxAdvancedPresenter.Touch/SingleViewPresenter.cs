// Copyright (C) 2014 Pat Laplante
//
// Permission is hereby granted, free of charge, to  any person obtaining a copy 
// of this software and associated documentation files (the "Software"), to deal 
// in the Software without  restriction, including without limitation the rights 
// to use, copy,  modify,  merge, publish,  distribute,  sublicense, and/or sell 
// copies of the  Software,  and  to  permit  persons  to   whom the Software is 
// furnished to do so, subject to the following conditions:
//
// The above  copyright notice  and this permission notice shall be included all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT  WARRANTY OF  ANY KIND,  EXPRESS OR
// IMPLIED, INCLUDING  BUT NOT  LIMITED TO  THE WARRANTIES  OF  MERCHANTABILITY,  
// FITNESS  FOR  A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
// 
// IN NO EVENT SHALL  THE AUTHORS OR COPYRIGHT  HOLDERS BE  LIABLE FOR ANY CLAIM
// DAMAGES  OR  OTHER  LIABILITY, WHETHER  IN  AN  ACTION  OF  CONTRACT, TORT OR 
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
// OR OTHER DEALINGS IN THE SOFTWARE.
// -----------------------------------------------------------------------------
using UIKit;
using System;
using MvvmCross.iOS.Views;
using MvvmCross.Core.ViewModels;

namespace Coc.MvxAdvancedPresenter.Touch
{
	public class SingleViewPresenter : BaseTouchViewPresenter
	{
		public override void ShowFirstView (IMvxIosView view)
		{
			RootViewController = CreateRootViewController(view);
		}

        public override void Show (IMvxIosView view)
		{
		}

		public override void Close (IMvxViewModel viewModel)
		{
		}

        public virtual UIViewController CreateRootViewController(IMvxIosView view) 
		{
			return view as UIViewController;
		}

		public override bool IsPresentingSameViewModel (System.Type vmType)
		{
			if (RootViewController == null) { return false; }
            var mvxTouchView = RootViewController as IMvxIosView;
			if (mvxTouchView != null && mvxTouchView.ViewModel != null) {
				return mvxTouchView.ViewModel.GetType() == vmType;
			}
			return false;
		}

//		protected override void FreeManagedResources ()
//		{
//			RootViewController.Dispose();
//			RootViewController = null;
//
//			base.FreeManagedResources ();
//		}
	}
}
