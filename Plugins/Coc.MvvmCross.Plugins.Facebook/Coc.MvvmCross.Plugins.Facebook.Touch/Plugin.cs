// Plugin.cs
// (c) Copyright Cirrious Ltd. http://www.cirrious.com
// MvvmCross is licensed using Microsoft Public License (Ms-PL)
// Contributions and inspirations noted in readme.md and license.txt
// 
// Project Lead - Stuart Lodge, @slodge, me@slodge.com
using MvvmCross.Platform.Plugins;
using MvvmCross.Platform;

namespace Coc.MvvmCross.Plugins.Facebook.Touch
{
    public class Plugin : IMvxPlugin

    {
        public void Load()
        {
            Mvx.RegisterSingleton<IFacebookService>(() => new IOSFacebookService());
//            #pragma warning disable 618 // 618 is that this interface is obsolete
//            // ReSharper disable CSharpWarnings::CS0612
//            Mvx.RegisterSingleton<IMvxGeoLocationWatcher>(() => new MvxTouchGeoLocationWatcher());
//            // ReSharper restore CSharpWarnings::CS0612
//            #pragma warning restore 618 // 618 is that this interface is obsolete
        }
    }
}