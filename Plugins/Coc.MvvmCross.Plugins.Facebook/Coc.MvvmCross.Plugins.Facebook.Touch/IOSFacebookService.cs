using System;
using System.Threading.Tasks;

using Social;

using Facebook.CoreKit;
using Facebook.LoginKit;
using Facebook.ShareKit;

using Foundation;
using Accounts;
using Xamarin;
using UIKit;

namespace Coc.MvvmCross.Plugins.Facebook.Touch
{
//    public class MyFBGraphObject :  FBGraphObject
//    {
//        public MyFBGraphObject (System.IntPtr ptr) : base(ptr)
//        {
//            
//        }
//    }

    public class IOSFacebookService : IFacebookService
    {
        readonly string [] ExtendedPermissions = { 
            "user_about_me", "user_friends", "user_location", 
            "user_hometown"
        };

        readonly NSUrl appLinkUrl = new NSUrl ("https://fb.me/739255952862948");
        readonly NSUrl previewImageUrl = new NSUrl ("http://www.yu-fit.com/images/fb_hero_banner.png");


        public string AuthToken { get; private set; }
        LoginManager login = new LoginManager ();

        #region IFacebookService implementation

        public void InitializeAppInfo(string appId, string displayName)
        {
            Settings.AppID = "510471792408033";
            Settings.DisplayName = "YuFit";
        }

        private async Task<bool> TryAuthenticatingUsingSocialAPI() 
        {
            var store = new ACAccountStore();
            var type = store.FindAccountType(ACAccountType.Facebook);

            var options = new AccountStoreOptions();
            options.FacebookAppId = Settings.AppID;
            options.SetPermissions(ACFacebookAudience.Friends, new string[] {"email", "user_about_me", "user_friends", "user_location", "user_hometown"});

            var result = await store.RequestAccessAsync(type, options);
            if (result.Item1) {
                var accounts = store.FindAccounts(type);

                //it will always be the last object with single sign on
                var facebookAccount = accounts[accounts.Length-1];
                try {
                    // This will force to get new token and avoid using a stale one.
                    // We should probably only do that if it is expired but since we are 
                    // not doing any facebook call in the app itself, we have to renew it here.
                    await store.RenewCredentialsAsync(facebookAccount);
                } catch (Exception ex) {
                    // This will happen if somehow the app gets marked as not available in the
                    // facebook settings in ios.  one needs to uncheck the switch.  run yufit
                    // close yufit then turn the switch back on. 
                    Insights.Report(ex);
                }

                var facebookCredential = facebookAccount.Credential;
                AuthToken = facebookCredential.OAuthToken;
            }

            return result.Item1;
        }
        public async Task<bool> AuthenticateAsync()
        {
            if (!await TryAuthenticatingUsingSocialAPI()) {
                LoginManagerLoginResult loginResult = await login.LogInWithReadPermissionsAsync(ExtendedPermissions);
                if (loginResult != null && loginResult.Token != null) {
                    if (!string.IsNullOrEmpty(loginResult.Token.TokenString)) {
                        Console.WriteLine ("{0}", loginResult.Token.TokenString);
                        Console.WriteLine(@"Session opened");
                        AuthToken = loginResult.Token.TokenString;
                        return true;
                    }
                }

                return false;
            }

            return true;
        }

        public void Logout()
        {
            //login.LogOut();
        }

        // Go to https://graph.facebook.com/PageName to get your page id
//        func openFacebookPage() {
//            let facebookURL = NSURL(string: "fb://profile/PageId")!
//                if UIApplication.sharedApplication().canOpenURL(facebookURL) {
//                    UIApplication.sharedApplication().openURL(facebookURL)
//                } else {
//                    UIApplication.sharedApplication().openURL(NSURL(string: "https://www.facebook.com/PageName")!)
//                }
//                }

        public void OpenPage(string pageId)
        {
            var url = new NSUrl(pageId);
            if (UIApplication.SharedApplication.CanOpenUrl(url)) {
                UIApplication.SharedApplication.OpenUrl(url);
            }
        }

        public IFacebookFriendInvitor GetInvitor()
        {
            return new IOSFacebookFriendInvitor(appLinkUrl, previewImageUrl);
        }


        #endregion
    }

    class IOSFacebookFriendInvitor : NSObject, IFacebookFriendInvitor, IAppInviteDialogDelegate
    {
        readonly NSUrl _appLinkUrl;
        readonly NSUrl _previewImageUrl;


        public Action<bool, string> OperationComplete { get; set; }

        public IOSFacebookFriendInvitor (NSUrl appLinkUrl, NSUrl previewImageUrl)
        {
            _appLinkUrl = appLinkUrl;
            _previewImageUrl = previewImageUrl;
        }

        #region IFacebookFriendInvitor implementation

        public void InviteFacebookFriends ()
        {
            var content = new AppInviteContent { AppLinkURL = _appLinkUrl, PreviewImageURL = _previewImageUrl };
            AppInviteDialog.Show (content, this);
        }

        #endregion

        public void DidComplete (AppInviteDialog appInviteDialog, NSDictionary results)
        {
            bool canceled = true;
            if (results != null) {
                if (results ["didComplete"] != null && results["didComplete"].ToString() == "1" && results ["completionGesture"] == null)  {
                    canceled = false;
                }
            }

            if (OperationComplete != null) {
                OperationComplete(canceled, string.Empty);
            }
        }

        public void DidFail (AppInviteDialog appInviteDialog, NSError error)
        {
            if (OperationComplete != null) {
                OperationComplete(true, error.Description);
            }
        }

    }
}
