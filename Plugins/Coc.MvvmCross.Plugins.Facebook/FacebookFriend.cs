﻿using System;

namespace Coc.MvvmCross.Plugins.Facebook
{
    public class FacebookAvatar
    {
        public bool IsSilouhette { get; set; }
        public string Url { get; set; }
    }

    public class FacebookGeoCoordinate
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class FacebookFriend
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public FacebookAvatar Avatar { get; set; }
        public FacebookGeoCoordinate Location { get; set; }
        public FacebookGeoCoordinate Hometown { get; set; }
    }
}

