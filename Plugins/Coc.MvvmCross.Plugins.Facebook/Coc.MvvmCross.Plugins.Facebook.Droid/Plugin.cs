﻿// Plugin.cs
// (c) Copyright Cirrious Ltd. http://www.cirrious.com
// MvvmCross is licensed using Microsoft Public License (Ms-PL)
// Contributions and inspirations noted in readme.md and license.txt
// 
// Project Lead - Stuart Lodge, @slodge, me@slodge.com
using MvvmCross.Platform.Plugins;
using MvvmCross.Platform;

namespace Coc.MvvmCross.Plugins.Facebook.Droid
{
    public class Plugin : IMvxPlugin

    {
        public void Load()
        {
            Mvx.RegisterSingleton<IFacebookService>(() => new DroidFacebookService());
        }
    }
}