﻿using System;
using System.Threading.Tasks;

using Android.Content;
using Android.Runtime;

using Plugin.CurrentActivity;

using Xamarin.Facebook;
using Xamarin.Facebook.Login;

using FacebookSdkAuthorizationException = Xamarin.Facebook.FacebookAuthorizationException;
using FacebookSdkException = Xamarin.Facebook.FacebookException;

namespace Coc.MvvmCross.Plugins.Facebook.Droid
{
    /// <summary>
    /// Droid facebook service.
    /// </summary>
    /// ---------------------------------------------------------------------------------------------------
    public class DroidFacebookService : IFacebookService
    {
        readonly string [] ExtendedPermissions = { "user_about_me", "user_friends", "user_location", "user_hometown" };

        Context                     _context;
        ICallbackManager            _callbackManager;
        TaskCompletionSource<bool>  _pendingAuthenticationTask;

        #region IFacebookService implementation

        /// <summary>
        /// Gets the auth token.  Set after a successful authentication.
        /// </summary>
        /// <value>The auth token.</value>
        /// ---------------------------------------------------------------------------------------------------
        public string AuthToken { get; private set; }

        /// <summary>
        /// Authenticates against facebook.
        /// </summary>
        /// <returns>The async.</returns>
        /// ---------------------------------------------------------------------------------------------------
        public Task<bool> AuthenticateAsync()
        {
            _pendingAuthenticationTask = new TaskCompletionSource<bool>();
            LoginManager.Instance.LogInWithReadPermissions(CrossCurrentActivity.Current.Activity, ExtendedPermissions);

            return _pendingAuthenticationTask.Task;
        }

        /// <summary>
        /// Logout this instance.
        /// </summary>
        /// ---------------------------------------------------------------------------------------------------
        public void Logout()
        {
        }

        /// <summary>
        /// Opens the specified page inside facebook.
        /// </summary>
        /// <returns>The page.</returns>
        /// <param name="pageId">Page identifier.</param>
        /// ---------------------------------------------------------------------------------------------------
        public void OpenPage(string pageId)
        {
        }

        /// <summary>
        /// Returns an invitor object, usefull to invite people to try the app.
        /// </summary>
        /// <returns>The invitor.</returns>
        /// ---------------------------------------------------------------------------------------------------
        public IFacebookFriendInvitor GetInvitor()
        {
            return null;
            //return new IOSFacebookFriendInvitor(appLinkUrl, previewImageUrl);
        }

        #endregion

        #region Droid specific shit

        /// <summary>
        /// Initializes the app info.  For droid, the info is stored in the app context.
        /// </summary>
        /// <returns>The app info.</returns>
        /// ---------------------------------------------------------------------------------------------------
        public void InitializeAppInfo ()
        {
            _context = CrossCurrentActivity.Current.Activity.ApplicationContext;
            FacebookSdk.SdkInitialize (_context);

            _callbackManager = CallbackManagerFactory.Create ();

            var loginCallback = new FacebookCallback<LoginResult> {
                HandleSuccess = loginResult => {
                    AuthToken = loginResult?.AccessToken?.Token;
                    _pendingAuthenticationTask?.SetResult(true);
                    _pendingAuthenticationTask = null;
                },
                HandleCancel = () => {
                    _pendingAuthenticationTask?.SetResult(false);
                    _pendingAuthenticationTask = null;
                },
                HandleError = loginError => {
                    _pendingAuthenticationTask?.SetException(ErrorToException(loginError));
                    _pendingAuthenticationTask = null;
                }
            };

            LoginManager.Instance.RegisterCallback (_callbackManager, loginCallback);
        }

        /// <summary>
        /// On droid, the result of a facebook call is passed via the current activity.
        /// The activity will take care of passing it to us when required.
        /// </summary>
        /// <returns>The activity result.</returns>
        /// <param name="requestCode">Request code.</param>
        /// <param name="resultCode">Result code.</param>
        /// <param name="data">Data.</param>
        /// ---------------------------------------------------------------------------------------------------
        public void OnActivityResult (int requestCode, int resultCode, Intent data)
        {
            _callbackManager.OnActivityResult (requestCode, resultCode, data);
        }
        #endregion

        #region Helpers

        /// <summary>
        /// Convert droid facebook exception to our exception.
        /// </summary>
        /// <returns>The to exception.</returns>
        /// <param name="loginError">Login error.</param>
        /// ---------------------------------------------------------------------------------------------------
        Exception ErrorToException (FacebookSdkException loginError)
        {
            if (loginError is FacebookSdkAuthorizationException) {
                return new FacebookAuthorizationException();
            }

            return new FacebookException ();
        }

        #endregion
    }

    /// <summary>
    /// Facebook callback.
    /// Droids works with callbacks to handle responses from the facebook app.
    /// </summary>
    /// ---------------------------------------------------------------------------------------------------
    class FacebookCallback<TResult> : Java.Lang.Object, IFacebookCallback where TResult : Java.Lang.Object
    {
        public Action                       HandleCancel    { get; set; }
        public Action<FacebookSdkException> HandleError     { get; set; }
        public Action<TResult>              HandleSuccess   { get; set; }

        public void OnCancel    ()                              { if (HandleCancel != null  ) { HandleCancel (); } }
        public void OnError     (FacebookSdkException error)    { if (HandleError != null   ) { HandleError (error); } }
        public void OnSuccess   (Java.Lang.Object result)       { if (HandleSuccess != null ) { HandleSuccess (result.JavaCast<TResult> ()); } }
    }

    //class IOSFacebookFriendInvitor : IFacebookFriendInvitor, IAppInviteDialogDelegate
    //{
    //    readonly NSUrl _appLinkUrl;
    //    readonly NSUrl _previewImageUrl;


    //    public Action<bool, string> OperationComplete { get; set; }

    //    public IOSFacebookFriendInvitor (NSUrl appLinkUrl, NSUrl previewImageUrl)
    //    {
    //        _appLinkUrl = appLinkUrl;
    //        _previewImageUrl = previewImageUrl;
    //    }

    //    #region IFacebookFriendInvitor implementation

    //    public void InviteFacebookFriends ()
    //    {
    //        var content = new AppInviteContent { AppLinkURL = _appLinkUrl, PreviewImageURL = _previewImageUrl };
    //        AppInviteDialog.Show (content, this);
    //    }

    //    #endregion

    //    public void DidComplete (AppInviteDialog appInviteDialog, NSDictionary results)
    //    {
    //        bool canceled = true;
    //        if (results != null) {
    //            if (results ["didComplete"] != null && results["didComplete"].ToString() == "1" && results ["completionGesture"] == null)  {
    //                canceled = false;
    //            }
    //        }

    //        if (OperationComplete != null) {
    //            OperationComplete(canceled, string.Empty);
    //        }
    //    }

    //    public void DidFail (AppInviteDialog appInviteDialog, NSError error)
    //    {
    //        if (OperationComplete != null) {
    //            OperationComplete(true, error.Description);
    //        }
    //    }

//    }
}

// Go to https://graph.facebook.com/PageName to get your page id
//        func openFacebookPage() {
//            let facebookURL = NSURL(string: "fb://profile/PageId")!
//                if UIApplication.sharedApplication().canOpenURL(facebookURL) {
//                    UIApplication.sharedApplication().openURL(facebookURL)
//                } else {
//                    UIApplication.sharedApplication().openURL(NSURL(string: "https://www.facebook.com/PageName")!)
//                }
//                }



//readonly NSUrl appLinkUrl = new NSUrl ("https://fb.me/739255952862948");
//readonly NSUrl previewImageUrl = new NSUrl ("http://www.yu-fit.com/images/fb_hero_banner.png");
