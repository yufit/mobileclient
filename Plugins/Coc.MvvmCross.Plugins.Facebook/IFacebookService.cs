﻿using System;
using System.Threading.Tasks;

namespace Coc.MvvmCross.Plugins.Facebook
{
    public interface IFacebookService
    {
        Task<bool> AuthenticateAsync();
        void Logout();
        string AuthToken { get; }
        IFacebookFriendInvitor GetInvitor();
        void OpenPage(string pageId);
    }

    public interface IFacebookFriendInvitor
    {
        void InviteFacebookFriends();
        Action<bool, string> OperationComplete { get; set; }
    }
}
