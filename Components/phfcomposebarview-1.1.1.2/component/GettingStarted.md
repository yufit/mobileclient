This is a precise reconstruction of the compose bar from the iOS Messages.app, mimicking the behaviors and graphics while also allowing you to customize many aspects of it.

It basically consists of a text view, a placeholder label, a utility button located to the left of the text view, and a main button located to the right of the text view.

- title of main button (the one on the right) can be changed
- tint color of main button can be changed
- title of the placeholder can be changed
- placeholder is exposed as a property for further customization
- text view is exposed as a property for further customization
- utility button (the one on the left) can be shown by setting the utility
  button image (best results for white images on transparent background with up
  to 32pt side length)
- optional character counter when specifying a max character count (similar to
  typing an SMS in Messages.app; the max char count limit is not imposed)
- uses delegation to notify of button presses
- forwards delegation methods from the text view
- automatically grows when text wraps
- posts notifications and sends delegate messages about frame changes before and
  after the change so you can adjust your view setup
- by default grows upwards, alternatively downwards
- max height for growth can be specified in terms of points or line numbers

Here's an example on how to display a simple Compose bar View:

```csharp
using PHFComposeBarView;
// ...

ComposeBarView composeBarView;
public override void ViewDidLoad ()
{
	base.ViewDidLoad ();

	// Creating ComposeBarView and initializing its properties
	composeBarView = new ComposeBarView (new RectangleF (0, 100, View.Bounds.Width, ComposeBarView.InitialHeight)) {
		MaxCharCount = 160,
		MaxLinesCount = 6,
		Placeholder = "Type something...",
		UtilityButtonImage = UIImage.FromBundle ("Camera")
	};

	// This will handle when the user taps the main button
	composeBarView.DidPressButton += (sender, e) => {
		var composeBar = sender as ComposeBarView;
		Console.WriteLine ("Main button tapped. Text:\n{0}", composeBar.Text);
		InvokeOnMainThread (() => new UIAlertView ("Hello", "Main button tapped. Text:\n" + composeBar.Text, null, "Ok", null).Show ());
		composeBar.Text = string.Empty;
		composeBar.ResignFirstResponder ();
	};

	// This will handle when the user taps the Utility button in this case a CameraButton
	composeBarView.DidPressUtilityButton += (sender, e) => {
		Console.WriteLine ("Utility button tapped");
		InvokeOnMainThread (() => new UIAlertView ("Hello", "Utility button tapped", null, "Ok", null).Show ());
	};

	View.AddSubview (composeBarView);
}
```