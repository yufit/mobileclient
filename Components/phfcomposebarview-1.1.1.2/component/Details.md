This is a precise reconstruction of the compose bar from the iOS Messages.app, mimicking the behaviors and graphics while also allowing you to customize many aspects of it.

It basically consists of a text view, a placeholder label, a utility button located to the left of the text view, and a main button located to the right of the text view.

Here's an example on how to display a simple Compose bar View:

```csharp
using PHFComposeBarView;
// ...

ComposeBarView composeBarView;
public override void ViewDidLoad ()
{
	base.ViewDidLoad ();

	// Creating ComposeBarView and initializing its properties
	composeBarView = new ComposeBarView (new RectangleF (0, 100, View.Bounds.Width, ComposeBarView.InitialHeight)) {
		MaxCharCount = 160,
		MaxLinesCount = 6,
		Placeholder = "Type something...",
		UtilityButtonImage = UIImage.FromBundle ("Camera")
	};

	// This will handle when the user taps the main button
	composeBarView.DidPressButton += (sender, e) => {
		var composeBar = sender as ComposeBarView;
		Console.WriteLine ("Main button tapped. Text:\n{0}", composeBar.Text);
		InvokeOnMainThread (() => new UIAlertView ("Hello", "Main button tapped. Text:\n" + composeBar.Text, null, "Ok", null).Show ());
		composeBar.Text = string.Empty;
		composeBar.ResignFirstResponder ();
	};

	// This will handle when the user taps the Utility button in this case a CameraButton
	composeBarView.DidPressUtilityButton += (sender, e) => {
		Console.WriteLine ("Utility button tapped");
		InvokeOnMainThread (() => new UIAlertView ("Hello", "Utility button tapped", null, "Ok", null).Show ());
	};

	View.AddSubview (composeBarView);
}
```

*Some component screenshots assembled with [PlaceIt](http://placeit.breezi.com/).*
