#pragma warning disable 1591
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.42000
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

[assembly: Android.Runtime.ResourceDesignerAttribute("XamSvgTests.Resource", IsApplication=true)]

namespace XamSvgTests
{
	
	
	[System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Android.Build.Tasks", "1.0.0.0")]
	public partial class Resource
	{
		
		static Resource()
		{
			global::Android.Runtime.ResourceIdManager.UpdateIdValues();
		}
		
		public static void UpdateIdValues()
		{
			global::XamSvg.Resource.Attribute.colorMapping = global::XamSvgTests.Resource.Attribute.colorMapping;
			global::XamSvg.Resource.Attribute.colorMappingSelected = global::XamSvgTests.Resource.Attribute.colorMappingSelected;
			global::XamSvg.Resource.Attribute.svg = global::XamSvgTests.Resource.Attribute.svg;
			global::XamSvg.Resource.Attribute.useCache = global::XamSvgTests.Resource.Attribute.useCache;
			global::XamSvg.Resource.Styleable.SvgImageView = global::XamSvgTests.Resource.Styleable.SvgImageView;
			global::XamSvg.Resource.Styleable.SvgImageView_colorMapping = global::XamSvgTests.Resource.Styleable.SvgImageView_colorMapping;
			global::XamSvg.Resource.Styleable.SvgImageView_colorMappingSelected = global::XamSvgTests.Resource.Styleable.SvgImageView_colorMappingSelected;
			global::XamSvg.Resource.Styleable.SvgImageView_svg = global::XamSvgTests.Resource.Styleable.SvgImageView_svg;
			global::XamSvg.Resource.Styleable.SvgImageView_useCache = global::XamSvgTests.Resource.Styleable.SvgImageView_useCache;
		}
		
		public partial class Attribute
		{
			
			// aapt resource value: 0x7f010001
			public const int colorMapping = 2130771969;
			
			// aapt resource value: 0x7f010002
			public const int colorMappingSelected = 2130771970;
			
			// aapt resource value: 0x7f010000
			public const int svg = 2130771968;
			
			// aapt resource value: 0x7f010003
			public const int useCache = 2130771971;
			
			static Attribute()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Attribute()
			{
			}
		}
		
		public partial class Color
		{
			
			// aapt resource value: 0x7f060001
			public const int dark_grey = 2131099649;
			
			// aapt resource value: 0x7f060000
			public const int white = 2131099648;
			
			static Color()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Color()
			{
			}
		}
		
		public partial class Dimension
		{
			
			// aapt resource value: 0x7f070000
			public const int dimen10 = 2131165184;
			
			// aapt resource value: 0x7f070001
			public const int dimen12 = 2131165185;
			
			// aapt resource value: 0x7f070002
			public const int dimen16 = 2131165186;
			
			static Dimension()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Dimension()
			{
			}
		}
		
		public partial class Drawable
		{
			
			// aapt resource value: 0x7f020000
			public const int Icon = 2130837504;
			
			static Drawable()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Drawable()
			{
			}
		}
		
		public partial class Id
		{
			
			// aapt resource value: 0x7f090000
			public const int bk = 2131296256;
			
			// aapt resource value: 0x7f09000c
			public const int btnGoEmpty = 2131296268;
			
			// aapt resource value: 0x7f09000b
			public const int btnNextImage = 2131296267;
			
			// aapt resource value: 0x7f090005
			public const int btn_lvl1 = 2131296261;
			
			// aapt resource value: 0x7f090007
			public const int btn_lvl2 = 2131296263;
			
			// aapt resource value: 0x7f090008
			public const int btn_msg = 2131296264;
			
			// aapt resource value: 0x7f090009
			public const int content = 2131296265;
			
			// aapt resource value: 0x7f09000a
			public const int icon = 2131296266;
			
			// aapt resource value: 0x7f090003
			public const int imageView1 = 2131296259;
			
			// aapt resource value: 0x7f090004
			public const int imageView6 = 2131296260;
			
			// aapt resource value: 0x7f090006
			public const int mail = 2131296262;
			
			// aapt resource value: 0x7f090002
			public const int medaillon = 2131296258;
			
			// aapt resource value: 0x7f090001
			public const int zone = 2131296257;
			
			static Id()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Id()
			{
			}
		}
		
		public partial class Layout
		{
			
			// aapt resource value: 0x7f030000
			public const int empty = 2130903040;
			
			// aapt resource value: 0x7f030001
			public const int Main = 2130903041;
			
			static Layout()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Layout()
			{
			}
		}
		
		public partial class Raw
		{
			
			// aapt resource value: 0x7f040000
			public const int bend_demo = 2130968576;
			
			// aapt resource value: 0x7f040001
			public const int buddyadd = 2130968577;
			
			// aapt resource value: 0x7f040002
			public const int buddycheck = 2130968578;
			
			// aapt resource value: 0x7f040003
			public const int edit_camera = 2130968579;
			
			// aapt resource value: 0x7f040004
			public const int edit_edit = 2130968580;
			
			// aapt resource value: 0x7f040005
			public const int ic_lock = 2130968581;
			
			// aapt resource value: 0x7f040006
			public const int ios_app_buttons = 2130968582;
			
			// aapt resource value: 0x7f040007
			public const int location = 2130968583;
			
			// aapt resource value: 0x7f040008
			public const int mail = 2130968584;
			
			static Raw()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Raw()
			{
			}
		}
		
		public partial class String
		{
			
			// aapt resource value: 0x7f050002
			public const int FontName1 = 2131034114;
			
			// aapt resource value: 0x7f050003
			public const int FontName2 = 2131034115;
			
			// aapt resource value: 0x7f050004
			public const int FontName3 = 2131034116;
			
			// aapt resource value: 0x7f050005
			public const int FontName4 = 2131034117;
			
			// aapt resource value: 0x7f050001
			public const int app_name = 2131034113;
			
			// aapt resource value: 0x7f050000
			public const int hello = 2131034112;
			
			static String()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private String()
			{
			}
		}
		
		public partial class Style
		{
			
			// aapt resource value: 0x7f080000
			public const int TextFont1Bold = 2131230720;
			
			// aapt resource value: 0x7f080001
			public const int TextFont3Light = 2131230721;
			
			// aapt resource value: 0x7f080002
			public const int TextFont3Medium = 2131230722;
			
			// aapt resource value: 0x7f080003
			public const int TextFont4Bold = 2131230723;
			
			static Style()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Style()
			{
			}
		}
		
		public partial class Styleable
		{
			
			public static int[] SvgImageView = new int[]
			{
					2130771968,
					2130771969,
					2130771970,
					2130771971};
			
			// aapt resource value: 1
			public const int SvgImageView_colorMapping = 1;
			
			// aapt resource value: 2
			public const int SvgImageView_colorMappingSelected = 2;
			
			// aapt resource value: 0
			public const int SvgImageView_svg = 0;
			
			// aapt resource value: 3
			public const int SvgImageView_useCache = 3;
			
			static Styleable()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Styleable()
			{
			}
		}
	}
}
#pragma warning restore 1591
