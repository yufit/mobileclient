# XamSvg
Stop adding PNG or JPG image files at different resolutions to support all devices !

Use SVG image files instead. SVG are standard small vector graphic files that autoscale to the device resolution and to any size without any stair step artifact. 
You can use them for nearly all images in your Xamarin application (native or Forms).

Because XamSvg uses only hardware accelerated graphics functions, it has no performance hit.

This package supports Xamarin Forms (iOS+Android+WP81), Xamarin iOS, Xamarin Android, and Windows Phone 8.1 projects.
It is delivered with a private nuget server and full source code.

**Important**: svg images that don't have any color set will not display as default color is transparent. This changes from other svg renderers where the default color is black.

## Svg Control

The package provides a ready and easy to use **SvgImage** control on all platforms. 

**Xamarin Forms xaml**

	<svg:SvgImage Svg="res:images.hand" HorizontalOptions="Center" HeightRequest="20" 
                  Clicked="OnSvgClicked" ClickedCommand="{Binding SvgClickedCommand}"
                  ColorMappingSelected="ffffff=00ff00" />

All properties are bindable and can be dynamically changed.

**Native iOS**

			var svgImage = new UISvgImageView()

**Native Android in axml** (also available in code)

            <XamSvg.SvgImageView
                local:svg="@raw/hand"
                android:layout_width="wrap_content"
                android:layout_height="27dp" />

**Windows Phone xaml**

    <svg:Svg Source="ms-appx:///Assets/svg/images.hand.svg" HorizontalAlignment="Stretch" VerticalAlignment="Center" />


##Usage

Svg image files can be stored either as .NET resources in a PCL library, or as native Android / iOS resources, or can be fetched from strings or streams. 

The control respects the aspect ratio of the svg bounding box if you set only one of the dimensions.

###Xamarin Forms

This example displays the image *hand.svg* stored in the */Images* folder of your PCL assembly.

	<svg:SvgImage Svg="res:images.hand" HorizontalOptions="Center" HeightRequest="20" />

<br/>
This example displays the image *hand.svg* stored in the */Assets/Images* folder of your PCL assembly.  
When pressed, white color (#FFFFFF) is replaced by green (#00FF00) and black color (#000000) is replaced by dark grey (#333333).  
When tapped, the OnSvgClicked handler is invoked.

	<svg:SvgImage Svg="res:assets.images.hand" HorizontalOptions="Center" HeightRequest="20"
					Clicked="OnSvgClicked" ColorMappingSelected="ffffff=00ff00;000000=333333" />

<br/>
This example uses the same hand.svg file but with other colors. It dynamically update the colors when loading the file.

	<svg:SvgImage Svg="res:images.hand" HorizontalOptions="Center" HeightRequest="20"
					ColorMapping="ffffff=00ff00;000000=333333" />

<br/>
This example invokes the YourCommand command when the image is tapped. See chapter [From Data Bindings to MVVM](https://developer.xamarin.com/guides/cross-platform/xamarin-forms/user-interface/xaml-basics/data_bindings_to_mvvm/) for more information.

	<svg:SvgImage Svg="res:images.hand" HorizontalOptions="Center" HeightRequest="20"
					ClickedCommand="{Binding YourCommand}" />

###Native Android

* Display myimage.svg:

        <?xml version="1.0" encoding="utf-8"?>
        <RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
            xmlns:local="http://schemas.android.com/apk/res-auto"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            >
                <XamSvg.SvgImageView
                    local:svg="@raw/myimage"
                    android:layout_width="wrap_content"
                    android:layout_height="50dp" />
        </RelativeLayout>

If <code>layout_width</code> or <code>layout_height</code> is not specified or is equal to <code>wrap_content</code>, then its value is computed from the other dimension and the svg bounding box ratio.

This is the recommanded way to use the widget: specify only one dimension and set the other to <code>wrap_content</code>.
The svg image will fill the available space and keep its aspect ratio.

* Display myimage.svg in another color:

        <XamSvg.SvgImageView
            local:svg="@raw/myimage"
            local:colorMapping="EAEDF1=494949;FFFFFF=000000;e9f1f7=ffFFFFFE;646464=FFFFFE"
            android:layout_width="wrap_content"
            android:layout_height="50dp" />

In this example the color EAEDF1, expressed as RRGGBB, is replaced by color 494949.

###Native iOS

* Replace the standard back button with cross.svg in ViewDidLoad:

        var btnBackImage = new UISvgImageView("vectors/cross.svg", fillHeightPixels: NavigationController.NavigationBar.Bounds.Height - 20);
        var backButton = new UIBarButtonItem(btnBackImage);
        NavigationItem.LeftBarButtonItem = backButton;

* Use a color mapping:

        var hands = new UISvgImageView("vectors/hands.svg", fillHeightPixels: 80, colorMapper: SvgColorMapperFactory.FromDic(new Dictionary<string, string> { { "000000", "2471B2" } }));

##Complete example projects and NuGet support

Complete example projects with source code are provided. Open and try them, they are easy to understand !

###Very easy to use

Simply add the widget markup to you axml layout in Android, or some lines of code in iOS.

This package contains a nuget script so you can deploy the library as a nuget package on your private nuget server.


###Mvvmcross binding support

Use the new "Svg" virtual property ! Add the MvxImageViewSvgDrawableTargetBinding file to your project, and register it from your mvvmcross Setup class:

        public class Setup : MvxAndroidSetup
        {
            ...

            protected override void FillTargetFactories(IMvxTargetBindingFactoryRegistry registry)
            {
                base.FillTargetFactories(registry);
                MvxImageViewSvgDrawableTargetBinding.Register(registry);
            }
        }

####Android

        <XamSvg.SvgImageView
            local:MvxBind="Svg MyStringProperty"
            android:layout_width="wrap_content"
            android:layout_height="50dp" />

####iOS

        set.Bind(icon).For("Svg").To(vm => vm.MyStringProperty);

###Optimizing SVG image files

You can use the free svg image editor [Inkscape](https://inkscape.org) to modify and generate optimized svg files, 
to change the bounding box (which changes the aspect ratio), or to simplify the SVG if it is not displayed correctly.

You can then use Internet Explorer to verify if your optimized svg still appears correctly.

Note that XamSvg does not set a default color if none is specified in the svg file. So either set a color, or use a color mapping from #00000000 (transparent) to something non transparent.

SVg files can also be exported from Photoshop and Illustrator, as long as they don't contain bitmaps. 
Bitmap extensions of svg will not render. You can check for bitmap data in a svg file by opening it and looking for tons of raw bytes.

### Features

The idea behind XamSvg was to include only svg features that can draw fast on all platforms. This matches most svg files. 
Some uncommon or non-native svg features are not implemented: svg containing bitmaps are not rendered (they can not scale smoothly).
If you are not sure if your svg file contains a bitmap, open it in any text editor and check for raw bytes data.

The svg images can be loaded from .NET embedded resources (see the Xamarin Forms chapter in this document), the resource bundle on iOS, the raw resource folder on Android, or a string.

### Performance
Svg parsing performance is good (svg files are very small), and the svg drawing code is very fast: it uses exclusively the native Path object which is hardware accelerated on all platforms.

Additionally, the Android version includes a thread safe cache of the rendered SVGs. This appears to be a good feature at start, but is in fact unnecessary.
You can disable the cache by adding <code>local:useCache="false"</code>. The cache is also disabled if both the width and height are 0 (native size).

##Supported platforms

Android api level 15+ (Android 4.0.3+)  
iOS 7+ with ios 64bits projects  
WP 8.1

The library requires PCL (Portable Class Library) support available from Visual Studio 2012 or Xamarin Studio.

iOS versions supports AutoLayout and storyboards (storyboard support is unofficial though).

The demo version supports only Android projects.

## Source code

This package includes the full source code of the library and the samples.

##Get in touch!

* [Propose and vote for new features](http://xamsvg.uservoice.com)
* [Support forum](http://xamsvg.uservoice.com)
* [FAQ](http://xamsvg.uservoice.com)

## Testimonials

This library is already used in these commercial apps :

- [Shapr](http://shapr.net/)
- [VfrTracks Flight Recorder](https://itunes.apple.com/fr/app/vfrtracks-flight-recorder/id948028963?mt=8) (Axel Charpentier)
- [KVMR Radio](http://bit.ly/kvmrios) (Matt Harrington)
- [Chabal 7M7M](http://www.fr.sogeti.com/accedez-a-linformation/actualites/7m7m-premiere-demonstration/)
- NFC by TagZi (Creapption)
- Inside (Creapption)
- DangerEx (Vapolia)
- ...
