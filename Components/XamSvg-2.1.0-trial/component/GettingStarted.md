﻿#Getting Started

## Svg file name

The svg control takes a string as input. If this string starts with "svg:", the control search the image in the .net embedded resources. If this string starts with "string:", the control reads the svg image from the string. Otherwise it will use native platform resources (in raw/xml for Android, in bundle resources for iOS).

## res: scheme (.NET embedded resources)

For the res: scheme, don't add the svg suffix and prefix the file name with the directory path relative to the root of your library, and replace / by dots. 

For exemple if you have this file:

(project root)/MyFiles/Images/Vectors/tiger_hood.svg

Use this string: "res:MyFiles.Images.Vectors.tiger_hood"

**Don't forget to set the build action of tiger_hood.svg to Embedded Resource**

Note: the res: scheme is case insensitive. You can use "res:myfiles.images.vectors.tiger_hood" if you prefer.

## string: scheme

You can dynamically load a svg string like this:

    string svgString = @"string:<svg width=""100"" height=""100""><circle cx=""50"" cy=""50"" r=""50"" style=""fill:#ff0000"" /></svg>"; 

For a customer, we created an animated multicolor progress circle control easily using this feature.

## other

The control will search the file in the native platform resources. 

On Android: /raw/xml

On iOS: in the bundle resources folder

## Xamarin Forms

Checklist:

- Add your svg files to the PCL project in any folder
- Set their build action to **Embedded resources** (right clic on the file > Properties > Build Action )
- Add an SvgImage tag to a xaml page (and its namespace)

Namespace is xmlns:svg="clr-namespace:XamSvg.XamForms;assembly=XamSvg.XamForms"  
Tag is svg:SvgImage

    <?xml version="1.0" encoding="utf-8" ?>
    <ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
		         xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
                xmlns:svg="clr-namespace:XamSvg.XamForms;assembly=XamSvg.XamForms"
			     x:Class="XamSvg.Demo.MainPage"
                 BackgroundColor="#E08080">
      <ContentView Padding="10, 40, 40, 10">
        <StackLayout Orientation="Vertical" VerticalOptions="Start">

           <Label Text="Clicking the vector image will select it and trigger both the command and event. Get more svg images on http://www.flaticon.com/" HorizontalOptions="Center" />

   	        <svg:SvgImage Svg="res:images.hand" HorizontalOptions="Start" HeighRequest="32"
                        ColorMapping="00000000=ff000000" ColorMappingSelected="00000000=ff000000;ffffff=00ff00" />

        </StackLayout>
      </ContentView>
    </ContentPage>

In the Svg property, set the name of the svg file, without its extension, prefixed with res: and the directory names where the SVG file is (separated by a dot).
If the Svg file is /images/hand.svg, the svg property should be set to "res:images.hand"
Note that as only HeighRequest was set, the SvgImage object will compute its width according to the SVG aspect ratio automatically.



- In App.cs, set the ResourceAssembly property to the assembly containing your SVG resources:


        public class App : Application
        {
            public App()
            {
                var assembly = typeof (App).GetTypeInfo().Assembly;
                XamSvg.Shared.Config.ResourceAssembly = assembly;





- In the iOS Forms project, open the AppDelegate.cs file and add a call to InitializeForms():

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            SvgImageRenderer.InitializeForms();            
            ...
        }



- In the Android Forms project, open MainActivity.cs file and add a call to InitializeForms():

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
              
            SvgImageRenderer.InitializeForms();
            
            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());
        }

### Additional usages

For Xamarin Forms toolbars images, you can't set a SvgImage.
That's where the SvgImageSource class comes handy:

The static SvgImageSource.Create() method will return an ImageSource object containing the rendered SVG at the specified size.

    ImageSource SvgImageSource.Create(string svg, double width=0, double height=0, string colorMapping=null)

You have to set either the width or the height or both.
There is only one optional color mapping, as ImageSource does not provide pushed state / click events.


##Android 3.1+ (api 12)

- Open your Android project, open nuget, then add "XamSvg". The nuget package has been copied in your local cache by the component installer.

- Add a folder called `raw` under the standard `resources` folder

- Add your svg images to this `raw` folder. Make sure they have the AndroidResource build action.

- In your Application or MainActivity class, initialize the Svg lib by calling XamSvg.Setup.InitSvgLib();
This is very quick and binds the cross platform color helper with the specific android version.

        [Application]
        public class MainApp : Application
        {
            public MainApp(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
            {
                XamSvg.Setup.InitSvgLib();
            }
        }



To display myimage.svg image:

        <?xml version="1.0" encoding="utf-8"?>
        <RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
            xmlns:local="http://schemas.android.com/apk/res-auto"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            >
                <XamSvg.SvgImageView
                    local:svg="@raw/myimage"
                    android:layout_width="wrap_content"
                    android:layout_height="50dp" />
        </RelativeLayout>

##iOS 7+

Note: 32 bit support (ios 6) has been removed.

### New way: using SVG as a shared .NET embedded resource

- Open your iOS project, open nuget, then add "XamSvg". The nuget package has been copied in your local cache by the component installer.

- Add your svg images to your project (ios or PCL). Make sure they have the **EmbeddedResource** build action.

- In your AppDelegate, initialize the Svg lib by calling XamSvg.Setup.InitSvgLib() 
This simply binds the cross platform color helper with the specific platform version. It is very fast.
Then set the XamSvg.Shared.Config.ResourceAssembly property to the assembly where XamSvg will search for embedded resources.

        [Register("AppDelegate")]
        public partial class AppDelegate : UIApplicationDelegate
        {
            private UIWindow window;
 
            public override bool FinishedLaunching(UIApplication app, NSDictionary options)
            {
                //Initialize the cross platform color helper
                XamSvg.Setup.InitSvgLib();
            
                //Tells XamSvg in which assembly to search for svg embedded resources
                XamSvg.Shared.Config.ResourceAssembly = typeof(AClassInTheAssemblyWhereSVGResourcesAre).GetTypeInfo().Assembly;
 
                window = new UIWindow(UIScreen.MainScreen.Bounds) {RootViewController = new MyViewController()};
                window.MakeKeyAndVisible();
                return true;
            }
        }


To display (project root)/myassets/images/myimage.svg:

        var myimage = new UISvgImageView("res:myassets.images.myimage", fillHeightPixels: 80);
        Add(myimage);
        //You can add autolayout constraints. UISvgImageView will have an intrisic size.



### Old way: SVG as a native iOS bundle resource

- Open your iOS project, open nuget, then add "XamSvg". The nuget package has been copied in your local cache by the component installer.

- Add your svg images to the `resources` folder or any of its subfolder. Suggested is a `svg` folder. Make sure they have the **BundleResource** build action.

- In your AppDelegate, initialize the Svg lib by calling XamSvg.Setup.InitSvgLib();
This simply binds the cross platform color helper with the specific platform version. It is very fast.

        [Register("AppDelegate")]
        public partial class AppDelegate : UIApplicationDelegate
        {
            private UIWindow window;
 
            public override bool FinishedLaunching(UIApplication app, NSDictionary options)
            {
                //Initialize the cross platform color helper
                XamSvg.Setup.InitSvgLib();

                window = new UIWindow(UIScreen.MainScreen.Bounds) {RootViewController = new MyViewController()};
                window.MakeKeyAndVisible();
                return true;
            }
        }


To display myimage.svg image:

        var myimage = new UISvgImageView("svg/myimage.svg", fillHeightPixels: 80);
        Add(myimage);
        //You can add autolayout constraints. UISvgImageView will have an intrisic size.


#Reference

## Color Mapping

A color mapping is the transformation of an existing color inside the svg file to another color.
This is useful to easily provide a feedback for the pressed state, or to reuse the same svg file using alternate colors.

Colors are specified using the standard HTML syntax: AARRGGBB  
where AA is the alpha level (00=transparent, FF=opaque), and RGB the standard red/green/blue levels.

**Strings**

You can specify mappings using strings.  

Syntax:  
OLDCOLOR=NEWCOLOR  

Multiple mappings syntax:  
OLDCOLOR=NEWCOLOR;OLDCOLOR=NEWCOLOR;OLDCOLOR=NEWCOLOR

Examples:  
ColorMapping="00000000=ff000000"  
ColorMappingSelected="00000000=ff000000;ffffff=00ff00"

#Full example projects

Complete example projects with source code are provided. Open and try them, they are easy to understand !

New! Sample projects are available on github [here](https://github.com/softlion/XamSvg-Samples).


#Get in touch!

* [Propose and vote for new features](http://xamsvg.uservoice.com)
* [Support forum](http://xamsvg.uservoice.com)
* [FAQ](http://xamsvg.uservoice.com)
